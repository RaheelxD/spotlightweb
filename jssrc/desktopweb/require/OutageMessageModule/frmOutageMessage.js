define("OutageMessageModule/frmOutageMessage", function() {
    return function(controller) {
        function addWidgetsfrmOutageMessage() {
            this.setDefaultUnit(kony.flex.DP);
            var flxOutageMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxOutageMsg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOutageMsg.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblServiceOutageMessages\")"
                    },
                    "mainHeader": {
                        "left": 0,
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxNoOutageMsgWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "89.40%",
                "id": "flxNoOutageMsgWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "106dp",
                "zIndex": 1
            }, {}, {});
            flxNoOutageMsgWrapper.setDefaultUnit(kony.flex.DP);
            var flxNoFAQCreated = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350px",
                "id": "flxNoFAQCreated",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxNoFAQCreated.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "height": "340px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": "200px",
                        "text": "ADD OUTAGE POLICY",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "noStaticData": {
                        "left": "0px",
                        "right": "35px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoFAQCreated.add(noStaticData);
            flxNoOutageMsgWrapper.add(flxNoFAQCreated);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "106dp",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var FlexSearchBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "FlexSearchBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            FlexSearchBar.setDefaultUnit(kony.flex.DP);
            var search = new com.adminConsole.header.search({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSearch": {
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "width": "351px"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": 36,
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "top": "viz.val_cleared",
                        "width": "350px"
                    },
                    "flxSubHeader": {
                        "right": "0px"
                    },
                    "imgSearchCancel": {
                        "src": "close_blue.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "search": {
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "width": "100%"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "25dp",
                "id": "flxSelectOptionsHeader",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "30px",
                "width": "110dp",
                "zIndex": 111
            }, {}, {});
            flxSelectOptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblListIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblListIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon12px",
                "text": "",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActions",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessage.Actions\")",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDownArrow = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblDownArrow",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknfontIconDescDownArrow12px",
                "text": "",
                "top": "0",
                "width": "12dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectOptionsHeader.add(lblListIcon, lblActions, lblDownArrow);
            FlexSearchBar.add(search, flxSelectOptionsHeader);
            var FlexUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "CopyCopyCopyslFbox",
                "top": "80px",
                "zIndex": 1
            }, {}, {});
            FlexUsers.setDefaultUnit(kony.flex.DP);
            var FlexUserTable = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": false,
                "height": "300dp",
                "horizontalScrollIndicator": true,
                "id": "FlexUserTable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "pagingEnabled": false,
                "right": "35px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "Copysknflxffffffop0e23e7fddf7bd4b",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            FlexUserTable.setDefaultUnit(kony.flex.DP);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "100px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxSelectOptionsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptionsInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelectOptionsInner.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxEdit.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(lblIconOption2, lblOption1);
            var flxPauseOutage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPauseOutage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i1d238dfcce748",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxPauseOutage.setDefaultUnit(kony.flex.DP);
            var lblIconPause = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconPause",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PauseIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPauseOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPauseOption",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Pause\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPauseOutage.add(lblIconPause, lblPauseOption);
            var flxResumeOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxResumeOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i1d238dfcce748",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxResumeOption.setDefaultUnit(kony.flex.DP);
            var lblIconResumeOption = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconResumeOption",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResumeIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblResumeOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblResumeOption",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Resume\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResumeOption.add(lblIconResumeOption, lblResumeOption);
            var flxTerminateOutage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxTerminateOutage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxTerminateOutage.setDefaultUnit(kony.flex.DP);
            var lblIconTerminate = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconTerminate",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18pxRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TerminateIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTerminateOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTerminateOption",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Terminate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTerminateOutage.add(lblIconTerminate, lblTerminateOption);
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b2deaad0a9f24f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDelete.setDefaultUnit(kony.flex.DP);
            var lblIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption3",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(lblIconOption3, lblOption3);
            var flxCopy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCopy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b2deaad0a9f24f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxCopy.setDefaultUnit(kony.flex.DP);
            var lblIconOptionCopy = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOptionCopy",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon20px",
                "text": "",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionCopy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionCopy",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Copy",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCopy.add(lblIconOptionCopy, lblOptionCopy);
            flxSelectOptionsInner.add(flxEdit, flxPauseOutage, flxResumeOption, flxTerminateOutage, flxDelete, flxCopy);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSelectOptions.add(flxUpArrowImage, flxSelectOptionsInner, flxDownArrowImage);
            var flxTableView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTableView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTableView.setDefaultUnit(kony.flex.DP);
            var flxHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaders.setDefaultUnit(kony.flex.DP);
            var flxHeaderCheckbox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30px",
                "id": "flxHeaderCheckbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d77d74d95a5b43fc83a09b335071bd85,
                "skin": "slFbox",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxHeaderCheckbox.setDefaultUnit(kony.flex.DP);
            var imgHeaderCheckBox = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12px",
                "id": "imgHeaderCheckBox",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderCheckbox.add(imgHeaderCheckBox);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontIconSortName",
                "isVisible": true,
                "right": "10px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxName.add(lblName, lblFontIconSortName);
            var flxHeaderAppName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "flxHeaderAppName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxHeaderAppName.setDefaultUnit(kony.flex.DP);
            var lblAppName = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblAppName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessage.ApplicationNameCaps\")",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortApp = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortApp",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxHeaderAppName.add(lblAppName, fontIconSortApp);
            var flxStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%"
            }, {}, {});
            flxStartDate.setDefaultUnit(kony.flex.DP);
            var lblStartDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.campaignStartDateTimeHeader\")",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFontIconSortStartDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontIconSortStartDate",
                "isVisible": true,
                "right": "10px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStartDate.add(lblStartDate, lblFontIconSortStartDate);
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%"
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var lblEndDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEndDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.campaignEndDateTimeHeader\")",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFontIconSortEndDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontIconSortEndDate",
                "isVisible": true,
                "right": "10px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxEndDate.add(lblEndDate, lblFontIconSortEndDate);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "9%",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblHeaderServiceStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderServiceStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatus.add(lblHeaderServiceStatus, fontIconSortStatus);
            flxHeaders.add(flxHeaderCheckbox, flxName, flxHeaderAppName, flxStartDate, flxEndDate, flxStatus);
            var lblHeaderSeparator = new kony.ui.Label({
                "bottom": "0px",
                "centerX": "50%",
                "height": "1dp",
                "id": "lblHeaderSeparator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
                "width": "96%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segOutageMessages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }, {
                    "fonticonActive": "",
                    "imgCheckBox": "checkbox.png",
                    "imgOptions": "dots3x.png",
                    "lblAppName": "Mobile App",
                    "lblEndDate": "End Date",
                    "lblIconOptions": "",
                    "lblName": "Jompay",
                    "lblSeparator": "Label",
                    "lblServiceStatus": "Active",
                    "lblStartDate": "Date"
                }],
                "groupCells": false,
                "height": "240dp",
                "id": "segOutageMessages",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxOutageMessages",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkboxnormal.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCheckbox": "flxCheckbox",
                    "flxOptions": "flxOptions",
                    "flxOutageMessages": "flxOutageMessages",
                    "flxOutageRow": "flxOutageRow",
                    "flxRow": "flxRow",
                    "flxStatus": "flxStatus",
                    "fonticonActive": "fonticonActive",
                    "imgCheckBox": "imgCheckBox",
                    "imgOptions": "imgOptions",
                    "lblAppName": "lblAppName",
                    "lblEndDate": "lblEndDate",
                    "lblIconOptions": "lblIconOptions",
                    "lblName": "lblName",
                    "lblSeparator": "lblSeparator",
                    "lblServiceStatus": "lblServiceStatus",
                    "lblStartDate": "lblStartDate"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTableView.add(flxHeaders, lblHeaderSeparator, segOutageMessages);
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            var flxAppFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAppFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxAppFilter.setDefaultUnit(kony.flex.DP);
            var appFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "appFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAppFilter.add(appFilterMenu);
            FlexUserTable.add(flxSelectOptions, flxTableView, flxNoRecordsFound, flxStatusFilter, flxAppFilter);
            FlexUsers.add(FlexUserTable);
            var flxSelectActions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectActions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "60px",
                "width": "150dp",
                "zIndex": 111
            }, {}, {});
            flxSelectActions.setDefaultUnit(kony.flex.DP);
            var flxPause = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPause",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i1d238dfcce748",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxPause.setDefaultUnit(kony.flex.DP);
            var lblIconOptionPause = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconOptionPause",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PauseIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPause = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPause",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Pause\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPause.add(lblIconOptionPause, lblPause);
            var flxResume = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxResume",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i1d238dfcce748",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxResume.setDefaultUnit(kony.flex.DP);
            var lblIconResume = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconResume",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResumeIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblResume = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblResume",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Resume\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResume.add(lblIconResume, lblResume);
            var flxTerminate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxTerminate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxTerminate.setDefaultUnit(kony.flex.DP);
            var lblIconOptionTerminate = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconOptionTerminate",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18pxRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TerminateIcon\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTerminate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTerminate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Terminate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTerminate.add(lblIconOptionTerminate, lblTerminate);
            var flxDeleteAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeleteAction",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b2deaad0a9f24f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDeleteAction.setDefaultUnit(kony.flex.DP);
            var lblIconOptionDelete = new kony.ui.Label({
                "centerY": "50%",
                "height": "18px",
                "id": "lblIconOptionDelete",
                "isVisible": true,
                "left": "14px",
                "skin": "sknIcon18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDelete",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeleteAction.add(lblIconOptionDelete, lblDelete);
            flxSelectActions.add(flxPause, flxResume, flxTerminate, flxDeleteAction);
            flxMainContent.add(FlexSearchBar, FlexUsers, flxSelectActions);
            var flxBreadCrum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrum",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrum.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrum.add(breadcrumbs);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxNoOutageMsgWrapper, flxMainContent, flxBreadCrum, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxOutageMsg.add(flxLeftPannel, flxRightPanel, flxToastMessage);
            var flxDeleteOutageMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteOutageMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteOutageMessage.setDefaultUnit(kony.flex.DP);
            var popUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            popUp.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.DeleteQuestion\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxPopUpDisclaimer = new kony.ui.RichText({
                "bottom": 30,
                "id": "rtxPopUpDisclaimer",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknrtxLato0df8337c414274d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxPopUpDisclaimer\")",
                "top": "30px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, rtxPopUpDisclaimer);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDelete\")",
                "width": "85dp",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": "80dp",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            flxPopUpButtons.add(btnPopUpDelete, btnPopUpCancel);
            flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
            popUp.add(flxPopUp);
            flxDeleteOutageMessage.add(popUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxAddOutageMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddOutageMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAddOutageMessage.setDefaultUnit(kony.flex.DP);
            var flxAddContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "528dp",
                "id": "flxAddContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "830dp"
            }, {}, {});
            flxAddContainer.setDefaultUnit(kony.flex.DP);
            var flxTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBar.setDefaultUnit(kony.flex.DP);
            flxTopBar.add();
            var flxAddOutageBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "438dp",
                "id": "flxAddOutageBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddOutageBody.setDefaultUnit(kony.flex.DP);
            var flxAddOutageClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAddOutageClose",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "15dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxAddOutageClose.setDefaultUnit(kony.flex.DP);
            var lblflxAddMessageCloseIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblflxAddMessageCloseIcon",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOutageClose.add(lblflxAddMessageCloseIcon);
            var lblAddOutageMessageTitle = new kony.ui.Label({
                "id": "lblAddOutageMessageTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl16pxLato192B45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.Add_Outage_Message\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 1, 2, 2],
                "paddingInPixel": false
            }, {});
            var flxAddOutageData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAddOutageData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOutageData.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "85dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxNameApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNameApps",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNameApps.setDefaultUnit(kony.flex.DP);
            var flxNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "388dp"
            }, {}, {});
            flxNameHeader.setDefaultUnit(kony.flex.DP);
            var lblOutageName = new kony.ui.Label({
                "id": "lblOutageName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknllbl2C4665Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameSize = new kony.ui.Label({
                "bottom": 0,
                "id": "lblNameSize",
                "isVisible": false,
                "right": "0px",
                "skin": "sknllbl485c75Lato13px",
                "text": "0/50",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameHeader.add(lblOutageName, lblNameSize);
            var lblApps = new kony.ui.Label({
                "id": "lblApps",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknllbl2C4665Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Applications\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameApps.add(flxNameHeader, lblApps);
            var flxContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxContent1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContent1.setDefaultUnit(kony.flex.DP);
            var tbxData = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "id": "tbxData",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 25,
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "388dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxApps",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "388dp",
                "zIndex": 2,
                "overrides": {
                    "customListbox": {
                        "isVisible": false,
                        "left": "15dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "388dp",
                        "zIndex": 2
                    },
                    "flxListboxError": {
                        "isVisible": true,
                        "width": "100%"
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": false,
                        "isVisible": false,
                        "width": "100%",
                        "zIndex": 2
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "flxSelectedText": {
                        "width": "100%"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.apps\")",
                        "width": "100%"
                    },
                    "lblSelectedValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.All\")"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "",
                            "lblDescription": ""
                        }],
                        "height": "100dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDataCol2Edit = new kony.ui.Label({
                "id": "lblDataCol2Edit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0dp",
                "width": "388dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContent1.add(tbxData, customListboxApps, lblDataCol2Edit);
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "388dp",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.name\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            flxRow1.add(flxNameApps, flxContent1, flxNoNameError);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxMessageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxMessageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxMessageHeader.setDefaultUnit(kony.flex.DP);
            var lblMessage = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblMessage",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.Message\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageSize = new kony.ui.Label({
                "bottom": 0,
                "id": "lblMessageSize",
                "isVisible": true,
                "right": "40px",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageHeader.add(lblMessage, lblMessageSize);
            var flxRichText = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRichText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxRichText.setDefaultUnit(kony.flex.DP);
            var rtxAddStaticContent = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "bottom": "0px",
                "height": "72px",
                "id": "rtxAddStaticContent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "right": "40px",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxRichText.add(rtxAddStaticContent);
            var flxErrorRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxErrorRow2.setDefaultUnit(kony.flex.DP);
            var flxNoMessageError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoMessageError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxNoMessageError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.message\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoMessageError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            flxErrorRow2.add(flxNoMessageError);
            flxRow2.add(flxMessageHeader, flxRichText, flxErrorRow2);
            var addStaticData = new com.adminConsole.staticContent.addStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "height": "120px",
                "id": "addStaticData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "10dp",
                "zIndex": 1,
                "overrides": {
                    "SwitchToggleStatus": {
                        "centerY": "50%",
                        "height": "23px",
                        "left": "0.00%",
                        "width": "36dp"
                    },
                    "addStaticData": {
                        "bottom": "0dp",
                        "height": "120px",
                        "isVisible": false,
                        "left": "0px",
                        "right": "0px",
                        "top": "10dp"
                    },
                    "flxAddStaticContentWrapper": {
                        "height": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxDescription": {
                        "isVisible": true
                    },
                    "flxNoDescriptionError": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxPhraseStatus": {
                        "height": "40px",
                        "isVisible": false,
                        "left": "0px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxRichText": {
                        "height": "72dp",
                        "left": "0dp"
                    },
                    "imgMandatory": {
                        "centerY": "50%",
                        "isVisible": false,
                        "src": "mandatory_2x.png"
                    },
                    "lblDescription": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.Message\")",
                        "left": "0dp"
                    },
                    "lblMessageSize": {
                        "centerY": "65%",
                        "isVisible": true,
                        "right": "40px"
                    },
                    "lblPhraseStatus": {
                        "width": "45px"
                    },
                    "rtxAddStaticContent": {
                        "height": "100%",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxDateTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxDateTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDateTime.setDefaultUnit(kony.flex.DP);
            var flxFrom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "388dp",
                "zIndex": 1
            }, {}, {});
            flxFrom.setDefaultUnit(kony.flex.DP);
            var lblFrom = new kony.ui.Label({
                "id": "lblFrom",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.startDateTime\")",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStartDateTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStartDateTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxStartDateTime.setDefaultUnit(kony.flex.DP);
            var flxStartCal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStartCal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "188dp"
            }, {}, {});
            flxStartCal.setDefaultUnit(kony.flex.DP);
            var flxCalendarStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0dp",
                "width": "188dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarStartDate.setDefaultUnit(kony.flex.DP);
            var customCalStartDate = new kony.ui.CustomWidget({
                "id": "customCalStartDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "maxDateRangeInMonths": "3",
                "minDateRangeInMonths": "0",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarStartDate.add(customCalStartDate);
            flxStartCal.add(flxCalendarStartDate);
            var timePicker = new com.adminConsole.common.timePicker({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "timePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "240dp",
                "overrides": {
                    "flxOuterBorder": {
                        "width": "120px"
                    },
                    "lblColen": {
                        "left": "60px"
                    },
                    "lstbxAMPM": {
                        "left": "130px",
                        "width": "60px"
                    },
                    "lstbxHours": {
                        "left": "5px",
                        "width": "48dp"
                    },
                    "lstbxMinutes": {
                        "right": "5px",
                        "width": "48dp"
                    },
                    "timePicker": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": true,
                        "left": "10dp",
                        "right": "viz.val_cleared",
                        "width": "240dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStartDateTime.add(flxStartCal, timePicker);
            flxFrom.add(lblFrom, flxStartDateTime);
            var flxTo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxTo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "388dp",
                "zIndex": 1
            }, {}, {});
            flxTo.setDefaultUnit(kony.flex.DP);
            var lblTo = new kony.ui.Label({
                "id": "lblTo",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.endDateTime\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEndDateTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxEndDateTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEndDateTime.setDefaultUnit(kony.flex.DP);
            var flxEndCal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxEndCal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "188dp"
            }, {}, {});
            flxEndCal.setDefaultUnit(kony.flex.DP);
            var flxCalendarEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0dp",
                "width": "188dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarEndDate.setDefaultUnit(kony.flex.DP);
            var customCalEndDate = new kony.ui.CustomWidget({
                "id": "customCalEndDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "maxDateRangeInMonths": "3",
                "minDateRangeInMonths": "0",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarEndDate.add(customCalEndDate);
            flxEndCal.add(flxCalendarEndDate);
            var timePicker1 = new com.adminConsole.common.timePicker({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "timePicker1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxOuterBorder": {
                        "width": "120px"
                    },
                    "lblColen": {
                        "left": "60px"
                    },
                    "lstbxAMPM": {
                        "left": "130px"
                    },
                    "lstbxHours": {
                        "left": "5px",
                        "width": "48dp"
                    },
                    "lstbxMinutes": {
                        "right": "5px",
                        "width": "48dp"
                    },
                    "timePicker": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": true,
                        "left": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEndDateTime.add(flxEndCal, timePicker1);
            flxTo.add(lblTo, flxEndDateTime);
            flxDateTime.add(flxFrom, flxTo);
            var flxErrorText = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxErrorText",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorText.setDefaultUnit(kony.flex.DP);
            var lblErrorText = new kony.ui.Label({
                "id": "lblErrorText",
                "isVisible": true,
                "left": "15dp",
                "right": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.startEndDatesRequired\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorTextIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTextIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorText.add(lblErrorText, lblErrorTextIcon);
            var flxErrorText2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxErrorText2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorText2.setDefaultUnit(kony.flex.DP);
            var lblErrorText2 = new kony.ui.Label({
                "id": "lblErrorText2",
                "isVisible": true,
                "left": "15dp",
                "right": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.warning.messageAlreadyScheduled\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorTextIcon2 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTextIcon2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorText2.add(lblErrorText2, lblErrorTextIcon2);
            flxRow3.add(flxDateTime, flxErrorText, flxErrorText2);
            flxAddOutageData.add(flxRow1, flxRow2, addStaticData, flxRow3);
            flxAddOutageBody.add(flxAddOutageClose, lblAddOutageMessageTitle, flxAddOutageData);
            var flxBtns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxBtns",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFooter",
                "zIndex": 1
            }, {}, {});
            flxBtns.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "20px",
                        "width": "100px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "110px",
                        "text": "SAVE AND UPDATE"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateCAPS\")",
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "300px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBtns.add(commonButtons);
            flxAddContainer.add(flxTopBar, flxAddOutageBody, flxBtns);
            var flxLoading2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "528dp",
                "id": "flxLoading2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "width": "830dp",
                "zIndex": 150
            }, {}, {});
            flxLoading2.setDefaultUnit(kony.flex.DP);
            var flxImageCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageCont.setDefaultUnit(kony.flex.DP);
            var imageLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imageLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageCont.add(imageLoading);
            flxLoading2.add(flxImageCont);
            flxAddOutageMessage.add(flxAddContainer, flxLoading2);
            var flxViewOutageMessagePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewOutageMessagePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxViewOutageMessagePopup.setDefaultUnit(kony.flex.DP);
            var flxViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "348dp",
                "id": "flxViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "830dp"
            }, {}, {});
            flxViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxViewTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewTopBar.setDefaultUnit(kony.flex.DP);
            flxViewTopBar.add();
            var flxViewOutageClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxViewOutageClose",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "15dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxViewOutageClose.setDefaultUnit(kony.flex.DP);
            var lblViewOutageCloseIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblViewOutageCloseIcon",
                "isVisible": true,
                "left": "0",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewOutageClose.add(lblViewOutageCloseIcon);
            var flxViewTitleAndButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTitleAndButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxViewTitleAndButton.setDefaultUnit(kony.flex.DP);
            var lblViewOutageMessageTitle = new kony.ui.Label({
                "id": "lblViewOutageMessageTitle",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl16pxLato192B45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Outage.viewHeader\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewEditButton = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "0dp",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxViewTitleAndButton.add(lblViewOutageMessageTitle, flxViewEditButton);
            var flxViewHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxViewHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflx115678",
                "top": "20dp",
                "width": "790dp",
                "zIndex": 1
            }, {}, {});
            flxViewHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxViewHeaderSeperator.add();
            var flxScrollViewData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollViewData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollViewData.setDefaultUnit(kony.flex.DP);
            var flxViewRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow1.setDefaultUnit(kony.flex.DP);
            var details1 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "details1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "btnLink1": {
                        "centerY": "viz.val_cleared"
                    },
                    "btnLink2": {
                        "centerY": "viz.val_cleared"
                    },
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "right": "40dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxColumn1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0px",
                        "width": "50%"
                    },
                    "flxColumn2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "50%",
                        "top": "0dp",
                        "width": "50%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "66%",
                        "right": "20px"
                    },
                    "flxDataAndImage1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "right": "20dp",
                        "top": "20dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxDataAndImage2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "20dp"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "25px"
                    },
                    "imgData1": {
                        "centerY": "viz.val_cleared",
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "centerY": "viz.val_cleared",
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "centerY": "viz.val_cleared",
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                        "right": "20dp",
                        "width": "100%"
                    },
                    "lblData2": {
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                        "right": "20dp",
                        "width": "100%"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "text": "Data for detailsData for detailsData for detailsData for detailsData for detailsData for detailsData for details",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderName\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplicationStatic\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")"
                    },
                    "lblIconData2": {
                        "centerY": "viz.val_cleared"
                    },
                    "lblSignData2": {
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewRow1.add(details1);
            var flxViewRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow2.setDefaultUnit(kony.flex.DP);
            var lblHeadingRow2 = new kony.ui.Label({
                "id": "lblHeadingRow2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.MessageCaps\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRowData2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRowData2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxRowData2.setDefaultUnit(kony.flex.DP);
            var lblDataRow2 = new kony.ui.Label({
                "id": "lblDataRow2",
                "isVisible": true,
                "left": "0px",
                "right": "40dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0dp",
                "width": "790dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRowData2.add(lblDataRow2);
            flxViewRow2.add(lblHeadingRow2, flxRowData2);
            var flxViewRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow3.setDefaultUnit(kony.flex.DP);
            var details2 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "details2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "right": "40dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxColumn1": {
                        "left": "0px",
                        "width": "50%"
                    },
                    "flxColumn2": {
                        "left": "50%",
                        "top": "0dp",
                        "width": "50%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "66%",
                        "right": "20px"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "25px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "centerY": "viz.val_cleared",
                        "src": "active_circle2x.png"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "text": "Data for detailsData for detailsData for detailsData for detailsData for detailsData for detailsData for details",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.campaignStartDateTimeHeader\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.campaignEndDateTimeHeader\")",
                        "right": "viz.val_cleared"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewRow3.add(details2);
            flxScrollViewData.add(flxViewRow1, flxViewRow2, flxViewRow3);
            flxViewContainer.add(flxViewTopBar, flxViewOutageClose, flxViewTitleAndButton, flxViewHeaderSeperator, flxScrollViewData);
            flxViewOutageMessagePopup.add(flxViewContainer);
            var flxConflictPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConflictPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxConflictPopup.setDefaultUnit(kony.flex.DP);
            var conflictPopup = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "conflictPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.close_popup\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")",
                        "isVisible": false
                    },
                    "lblPopUpMainMessage": {
                        "text": "Message Status Conflict"
                    },
                    "popUp1": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Messages with different status cannot be selected  for a bulk action",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConflictPopup.add(conflictPopup);
            this.add(flxOutageMsg, flxDeleteOutageMessage, flxEditCancelConfirmation, flxAddOutageMessage, flxViewOutageMessagePopup, flxConflictPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmOutageMessage,
            "enabledForIdleTimeout": true,
            "id": "frmOutageMessage",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f99c5cffa0864cb8a65d17ad1e600555(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_fcb2e4c3e55a4a96bf1354b6caf7e017,
            "retainScrollPosition": false
        }]
    }
});