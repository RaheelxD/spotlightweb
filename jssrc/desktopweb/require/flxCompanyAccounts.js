define("flxCompanyAccounts", function() {
    return function(controller) {
        var flxCompanyAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxCompanyAccounts.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {}, {});
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxAccountDetail = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountDetail",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15dp",
            "isModalContainer": false,
            "right": "30dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {}, {});
        flxAccountDetail.setDefaultUnit(kony.flex.DP);
        var lblAccountNumber = new kony.ui.Label({
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "skn73767812pxLato",
            "text": "0989865887",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountType = new kony.ui.Label({
            "id": "lblAccountType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato84939E12px",
            "text": "Salary Account",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountDetail.add(lblAccountNumber, lblAccountType);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxArrow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "10dp",
            "width": "30dp"
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label({
            "id": "lblArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcon485C7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(lblArrow);
        flxContent.add(flxAccountDetail, flxArrow);
        var lblEntitlementsSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblEntitlementsSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCompanyAccounts.add(flxContent, lblEntitlementsSeperator);
        return flxCompanyAccounts;
    }
})