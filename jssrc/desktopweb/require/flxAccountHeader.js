define("flxAccountHeader", function() {
    return function(controller) {
        var flxAccountHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAccountHeader.setDefaultUnit(kony.flex.DP);
        var flxCompanyDetailsAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyDetailsAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCompanyDetailsAccounts.setDefaultUnit(kony.flex.DP);
        var flxCompanyAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompanyAccounts.setDefaultUnit(kony.flex.DP);
        var lblAccountNumber = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1234567890123456",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountType",
            "isVisible": true,
            "left": "12dp",
            "right": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Savings",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountName",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Gold saving",
            "top": "15dp",
            "width": "19.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOwnershipType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblOwnershipType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Gold saving",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatusicon = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatusicon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblStatusicon, lblStatus);
        flxCompanyAccounts.add(lblAccountNumber, lblAccountType, lblAccountName, lblOwnershipType, flxStatus);
        var lblSeperator = new kony.ui.Label({
            "bottom": "1dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLblD0c34817e9d9f548",
            "text": ".",
            "width": "95.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPagingContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxPagingContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxPagingContainer.setDefaultUnit(kony.flex.DP);
        var lblShowing = new kony.ui.Label({
            "bottom": "17px",
            "height": "15dp",
            "id": "lblShowing",
            "isVisible": true,
            "left": "20dp",
            "skin": "skn73767812pxLato",
            "text": "Showing 1-10 of 95 ",
            "top": "17dp",
            "width": "60%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPagingNumber = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxPagingNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "40%",
            "zIndex": 1
        }, {}, {});
        flxPagingNumber.setDefaultUnit(kony.flex.DP);
        var flxPreviousfirst = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxPreviousfirst",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "16dp",
            "zIndex": 1
        }, {}, {});
        flxPreviousfirst.setDefaultUnit(kony.flex.DP);
        var lblarrowprevfirst = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowprevfirst",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPreviousfirst.add(lblarrowprevfirst);
        var flxPrevious = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var lblarrowprevious = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowprevious",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPrevious.add(lblarrowprevious);
        var flxNumber = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBg003e753pxrad",
            "width": "33dp",
            "zIndex": 1
        }, {}, {});
        flxNumber.setDefaultUnit(kony.flex.DP);
        var labelOne = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16dp",
            "id": "labelOne",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl0OFontFFFFFF100O",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNumber.add(labelOne);
        var flxnext = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxnext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "16dp",
            "zIndex": 1
        }, {}, {});
        flxnext.setDefaultUnit(kony.flex.DP);
        var lblarrowNext = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowNext",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxnext.add(lblarrowNext);
        var flxnextLast = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxnextLast",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "16dp",
            "zIndex": 1
        }, {}, {});
        flxnextLast.setDefaultUnit(kony.flex.DP);
        var lblarrowNextLast = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowNextLast",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxnextLast.add(lblarrowNextLast);
        var flxpageNocontainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "21dp",
            "id": "flxpageNocontainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxborder979797",
            "width": "38dp",
            "zIndex": 1
        }, {}, {});
        flxpageNocontainer.setDefaultUnit(kony.flex.DP);
        var textpageNo = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "sknTbxlatreg13px000000Noborder",
            "id": "textpageNo",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxlatreg13px000000Noborder",
            "text": "01",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "38dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxpageNocontainer.add(textpageNo);
        var flxpageGoContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "21dp",
            "id": "flxpageGoContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBg003e753pxrad",
            "width": "33dp",
            "zIndex": 1
        }, {}, {});
        flxpageGoContainer.setDefaultUnit(kony.flex.DP);
        var txtPageGo = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "sknTbxlatreg13pxffffffNoborder",
            "id": "txtPageGo",
            "isVisible": false,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxlatreg13pxffffffNoborder",
            "text": "GO",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "33dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var lblPageGo = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16dp",
            "id": "lblPageGo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl0OFontFFFFFF100O",
            "text": "GO",
            "width": "33dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxpageGoContainer.add(txtPageGo, lblPageGo);
        flxPagingNumber.add(flxPreviousfirst, flxPrevious, flxNumber, flxnext, flxnextLast, flxpageNocontainer, flxpageGoContainer);
        var lblPageCount = new kony.ui.Label({
            "id": "lblPageCount",
            "isVisible": false,
            "left": "0dp",
            "skin": "slLabel",
            "text": "0",
            "top": "11dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblsegId = new kony.ui.Label({
            "id": "lblsegId",
            "isVisible": false,
            "left": "0dp",
            "skin": "slLabel",
            "text": "0",
            "top": "11dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPagingContainer.add(lblShowing, flxPagingNumber, lblPageCount, lblsegId);
        flxCompanyDetailsAccounts.add(flxCompanyAccounts, lblSeperator, flxPagingContainer);
        flxAccountHeader.add(flxCompanyDetailsAccounts);
        return flxAccountHeader;
    }
})