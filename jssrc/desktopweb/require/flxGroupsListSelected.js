define("flxGroupsListSelected", function() {
    return function(controller) {
        var flxGroupsListSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxGroupsListSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxGroupsListSelected.setDefaultUnit(kony.flex.DP);
        var flxGroupSeg = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupSeg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupSeg.setDefaultUnit(kony.flex.DP);
        var flxSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxSegMain.setDefaultUnit(kony.flex.DP);
        var flxGroupsegmain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxGroupsegmain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxGroupsegmain.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b61f7d7ea40c4d468df00c912470b289,
            "right": "10px",
            "skin": "slFbox",
            "top": "15dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "height": "12px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescDownArrow12px",
            "text": "",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(ImgArrow, fonticonArrow);
        var lblGroupName = new kony.ui.Label({
            "id": "lblGroupName",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Platinum Customers",
            "top": "15px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblGroupType = new kony.ui.Label({
            "id": "lblGroupType",
            "isVisible": true,
            "left": "19.50%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "20",
            "top": "15px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRowBusinessTypes = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowBusinessTypes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "30%"
        }, {}, {});
        flxRowBusinessTypes.setDefaultUnit(kony.flex.DP);
        var lblGroupBusinessType = new kony.ui.Label({
            "id": "lblGroupBusinessType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Sole Proprietor, Trust, Partnership, Corporation",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxMore = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxMore",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknCursor",
            "top": "16dp",
            "width": "60dp",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "hoverhandSkin2"
        });
        flxMore.setDefaultUnit(kony.flex.DP);
        var lblMore = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMore",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "text": "+4 more",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMore.add(lblMore);
        flxRowBusinessTypes.add(lblGroupBusinessType, flxMore);
        var lblGroupCustomers = new kony.ui.Label({
            "id": "lblGroupCustomers",
            "isVisible": true,
            "left": "70%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "25",
            "top": "15px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "7%",
            "skin": "slFbox",
            "top": "12px",
            "width": "8.20%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblGroupStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconGroupStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblGroupStatus, fontIconGroupStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c91af5ad1bdd4cb19780226e8730bc7b,
            "right": "10px",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10px",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var fontIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "fontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(fontIconOptions);
        flxGroupsegmain.add(flxDropdown, lblGroupName, lblGroupType, flxRowBusinessTypes, lblGroupCustomers, flxStatus, flxOptions);
        var flxGroupDescriptionContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxGroupDescriptionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxGroupDescriptionContent.setDefaultUnit(kony.flex.DP);
        var flxGroupDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "91%",
            "zIndex": 20
        }, {}, {});
        flxGroupDesc.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "height": "15px",
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionValue = new kony.ui.Label({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknLblLato485c7513px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum accumsan odio efficituDonec felis dolor, molestie. Curabitur est eros, volutpat in elit rutrum, tristique placerat nunc.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin  scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum<br> accumsan odio efficitur est molestie viverra.",
            "top": "25px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupDesc.add(lblDescriptionHeader, lblDescriptionValue);
        flxGroupDescriptionContent.add(flxGroupDesc);
        flxSegMain.add(flxGroupsegmain, flxGroupDescriptionContent);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "10dp",
            "right": "10dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 20
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupSeg.add(flxSegMain, lblSeparator);
        flxGroupsListSelected.add(flxGroupSeg);
        return flxGroupsListSelected;
    }
})