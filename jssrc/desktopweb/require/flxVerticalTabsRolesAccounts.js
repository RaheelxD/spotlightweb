define("flxVerticalTabsRolesAccounts", function() {
    return function(controller) {
        var flxVerticalTabsRolesAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVerticalTabsRolesAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxVerticalTabsRolesAccounts.setDefaultUnit(kony.flex.DP);
        var flxNamesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxNamesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "96%"
        }, {}, {});
        flxNamesContainer.setDefaultUnit(kony.flex.DP);
        var btnOption = new kony.ui.Button({
            "id": "btnOption",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknBtnUtilRest73767812pxReg",
            "text": "196365768636",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOptionName = new kony.ui.Label({
            "id": "lblOptionName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLblLato84939E12px",
            "text": "Account Name",
            "top": "5dp",
            "width": "98%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNamesContainer.add(btnOption, lblOptionName);
        var flxImgArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "15dp",
            "width": "15px",
            "zIndex": 1
        }, {}, {});
        flxImgArrow.setDefaultUnit(kony.flex.DP);
        var lblIconSelected = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblIconSelected",
            "isVisible": true,
            "skin": "sknIcon485C7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgArrow.add(lblIconSelected);
        var lblTabsSeperator = new kony.ui.Label({
            "bottom": "1dp",
            "height": "1dp",
            "id": "lblTabsSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "20dp",
            "skin": "sknLblD5D9DD1000",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxVerticalTabsRolesAccounts.add(flxNamesContainer, flxImgArrow, lblTabsSeperator);
        return flxVerticalTabsRolesAccounts;
    }
})