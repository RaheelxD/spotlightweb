define("flxSuspendFeature", function() {
    return function(controller) {
        var flxSuspendFeature = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxSuspendFeature",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSuspendFeature.setDefaultUnit(kony.flex.DP);
        var flxFeaturesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeaturesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "60dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxFeaturesContainer.setDefaultUnit(kony.flex.DP);
        var flxFeatureUpper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxFeatureUpper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": 5,
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxFeatureUpper.setDefaultUnit(kony.flex.DP);
        var lblFeature = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeature",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoRegular192B4516px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureUpper.add(lblFeature);
        var flxFeaturesLower = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxFeaturesLower",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxFeaturesLower.setDefaultUnit(kony.flex.DP);
        var flxViewDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxViewDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80dp"
        }, {}, {});
        flxViewDetails.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureDetails = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewFeatureDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "text": "View Details",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewDetails.add(lblViewFeatureDetails);
        flxFeaturesLower.add(flxViewDetails);
        flxFeaturesContainer.add(flxFeatureUpper, flxFeaturesLower);
        var switchSuspend = new kony.ui.Switch({
            "centerY": "50%",
            "height": "25dp",
            "id": "switchSuspend",
            "isVisible": true,
            "leftSideText": "ON",
            "right": "30px",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0px",
            "width": "36dp",
            "zIndex": 1
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSuspendFeature.add(flxFeaturesContainer, switchSuspend);
        return flxSuspendFeature;
    }
})