define("flxAssignLimits", function() {
    return function(controller) {
        var flxAssignLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAssignLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAssignLimits.setDefaultUnit(kony.flex.DP);
        var flxLimitsContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxLimitsContainer.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxBgFCFCFCBrD7D9E0Rd1pxTB",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl485c75LatoBold13px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeader.add(lblFeatureName);
        var flxLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "35dp"
        }, {}, {});
        flxLimits.setDefaultUnit(kony.flex.DP);
        var flxLimitsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxLimitsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxLimitsHeader.setDefaultUnit(kony.flex.DP);
        var lblTransactionType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransactionType",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTransactionLimits = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransactionLimits",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLato696c7311px",
            "text": "TRANSACTION LIMIT",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPreApprovedLimit = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPreApprovedLimit",
            "isVisible": true,
            "left": "51%",
            "right": "20dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.PreApproved_UC\")",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAutoDenyLimits = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAutoDenyLimits",
            "isVisible": true,
            "left": "76%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AutoDeny_UC\")",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitsHeader.add(lblTransactionType, lblTransactionLimits, lblPreApprovedLimit, lblAutoDenyLimits);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblSeparator696C73",
            "text": ".",
            "top": "40dp",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitsContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxLimitsContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "41dp",
            "width": "100%"
        }, {}, {});
        flxLimitsContent.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxPerTransactionCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxPerTransactionCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "26%"
        }, {}, {});
        flxPerTransactionCont.setDefaultUnit(kony.flex.DP);
        var lblperTransactionLimits = new kony.ui.Label({
            "id": "lblperTransactionLimits",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.PerTransactionLimitLC\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRangeIcon1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxRangeIcon1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "15dp"
        }, {}, {});
        flxRangeIcon1.setDefaultUnit(kony.flex.DP);
        var lblIconRangeInfo1 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconRangeInfo1",
            "isVisible": true,
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "width": "15dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRangeIcon1.add(lblIconRangeInfo1);
        flxPerTransactionCont.add(lblperTransactionLimits, flxRangeIcon1);
        var flxPerTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxPerTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxPerTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblPerTransactionLimitDollar = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPerTransactionLimitDollar",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPerTransactionLimitValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPerTransactionLimitValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblLato485c7513px",
            "text": "20,000",
            "width": "92%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerTransactionLimit.add(lblPerTransactionLimitDollar, lblPerTransactionLimitValue);
        var flxPerCurrencyPreLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPerCurrencyPreLimit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxPerCurrencyPreLimit.setDefaultUnit(kony.flex.DP);
        var lblPerCurrencyPreLimit = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblPerCurrencyPreLimit",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerCurrencyPreLimit.add(lblPerCurrencyPreLimit);
        var txtPerTransPreApprovedLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtPerTransPreApprovedLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "51%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": 0,
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxPerCurrencyADLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPerCurrencyADLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "76%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxPerCurrencyADLimits.setDefaultUnit(kony.flex.DP);
        var lblPerCurrencyADLimits = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblPerCurrencyADLimits",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerCurrencyADLimits.add(lblPerCurrencyADLimits);
        var txtPerTransAutoDenyLimits = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtPerTransAutoDenyLimits",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "76%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxRow1.add(flxPerTransactionCont, flxPerTransactionLimit, flxPerCurrencyPreLimit, txtPerTransPreApprovedLimit, flxPerCurrencyADLimits, txtPerTransAutoDenyLimits);
        var flxErrorRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxErrorRow1.setDefaultUnit(kony.flex.DP);
        var flxColumn11 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn11",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn11.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon11 = new kony.ui.Label({
            "id": "lblErrorIcon11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg11 = new kony.ui.Label({
            "id": "lblErrorMsg11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn11.add(lblErrorIcon11, lblErrorMsg11);
        var flxColumn12 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn12",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "76%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn12.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon12 = new kony.ui.Label({
            "id": "lblErrorIcon12",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg12 = new kony.ui.Label({
            "id": "lblErrorMsg12",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn12.add(lblErrorIcon12, lblErrorMsg12);
        flxErrorRow1.add(flxColumn11, flxColumn12);
        var lblSeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxDailyTransactionCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxDailyTransactionCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "26%"
        }, {}, {});
        flxDailyTransactionCont.setDefaultUnit(kony.flex.DP);
        var lblDailyTransactionLimits = new kony.ui.Label({
            "id": "lblDailyTransactionLimits",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DailyTransactionLimitLC\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRangeIcon2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxRangeIcon2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0",
            "width": "15dp"
        }, {}, {});
        flxRangeIcon2.setDefaultUnit(kony.flex.DP);
        var lblIconRangeInfo2 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconRangeInfo2",
            "isVisible": true,
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "width": "15dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRangeIcon2.add(lblIconRangeInfo2);
        flxDailyTransactionCont.add(lblDailyTransactionLimits, flxRangeIcon2);
        var flxDailyTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDailyTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxDailyTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblDailyTransactionLimitDollar = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDailyTransactionLimitDollar",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDailyTransactionLimitValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDailyTransactionLimitValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblLato485c7513px",
            "text": "20,000",
            "top": "0",
            "width": "92%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyTransactionLimit.add(lblDailyTransactionLimitDollar, lblDailyTransactionLimitValue);
        var txtDailyTransPreApprovedLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtDailyTransPreApprovedLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "51%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var txtDailyTransAutoDenyLimits = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtDailyTransAutoDenyLimits",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "76%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxDailyCurrencyPreLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDailyCurrencyPreLimit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxDailyCurrencyPreLimit.setDefaultUnit(kony.flex.DP);
        var lblDailyCurrencyPreLimit = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblDailyCurrencyPreLimit",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyCurrencyPreLimit.add(lblDailyCurrencyPreLimit);
        var flxDailyCurrencyADLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDailyCurrencyADLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "76%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxDailyCurrencyADLimits.setDefaultUnit(kony.flex.DP);
        var lblDailyCurrencyADLimits = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblDailyCurrencyADLimits",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyCurrencyADLimits.add(lblDailyCurrencyADLimits);
        flxRow2.add(flxDailyTransactionCont, flxDailyTransactionLimit, txtDailyTransPreApprovedLimit, txtDailyTransAutoDenyLimits, flxDailyCurrencyPreLimit, flxDailyCurrencyADLimits);
        var flxErrorRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxErrorRow2.setDefaultUnit(kony.flex.DP);
        var flxColumn21 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn21",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn21.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon21 = new kony.ui.Label({
            "id": "lblErrorIcon21",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg21 = new kony.ui.Label({
            "id": "lblErrorMsg21",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn21.add(lblErrorIcon21, lblErrorMsg21);
        var flxColumn22 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn22",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "77%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn22.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon22 = new kony.ui.Label({
            "id": "lblErrorIcon22",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg22 = new kony.ui.Label({
            "id": "lblErrorMsg22",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn22.add(lblErrorIcon22, lblErrorMsg22);
        flxErrorRow2.add(flxColumn21, flxColumn22);
        var lblSeperator2 = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRow3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, {}, {});
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxWeeklyTransactionCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxWeeklyTransactionCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "27%"
        }, {}, {});
        flxWeeklyTransactionCont.setDefaultUnit(kony.flex.DP);
        var lblWeeklyTransactionLimits = new kony.ui.Label({
            "id": "lblWeeklyTransactionLimits",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.WeeklyTransLimitLC\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRangeIcon3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxRangeIcon3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0",
            "width": "15dp"
        }, {}, {});
        flxRangeIcon3.setDefaultUnit(kony.flex.DP);
        var lblIconRangeInfo3 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconRangeInfo3",
            "isVisible": true,
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "width": "15dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRangeIcon3.add(lblIconRangeInfo3);
        flxWeeklyTransactionCont.add(lblWeeklyTransactionLimits, flxRangeIcon3);
        var flxWeeklyTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxWeeklyTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxWeeklyTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblWeeklyTransactionLimitDollar = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblWeeklyTransactionLimitDollar",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWeeklyTransactionLimitValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblWeeklyTransactionLimitValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblLato485c7513px",
            "text": "20,000",
            "top": "0",
            "width": "92%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyTransactionLimit.add(lblWeeklyTransactionLimitDollar, lblWeeklyTransactionLimitValue);
        var txtWeeklyTransPreApprovedLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtWeeklyTransPreApprovedLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "51%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var txtWeeklyTransAutoDenyLimits = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtWeeklyTransAutoDenyLimits",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "76%",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "23%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxWeeklyCurrencyADLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxWeeklyCurrencyADLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "76%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxWeeklyCurrencyADLimits.setDefaultUnit(kony.flex.DP);
        var lblWeeklyCurrencyADLimits = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblWeeklyCurrencyADLimits",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyCurrencyADLimits.add(lblWeeklyCurrencyADLimits);
        var flxWeeklyCurrencyPreLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxWeeklyCurrencyPreLimit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30dp",
            "zIndex": 2
        }, {}, {});
        flxWeeklyCurrencyPreLimit.setDefaultUnit(kony.flex.DP);
        var lblWeeklyCurrencyPreLimit = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "51%",
            "id": "lblWeeklyCurrencyPreLimit",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyCurrencyPreLimit.add(lblWeeklyCurrencyPreLimit);
        flxRow3.add(flxWeeklyTransactionCont, flxWeeklyTransactionLimit, txtWeeklyTransPreApprovedLimit, txtWeeklyTransAutoDenyLimits, flxWeeklyCurrencyADLimits, flxWeeklyCurrencyPreLimit);
        var flxErrorRow3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxErrorRow3.setDefaultUnit(kony.flex.DP);
        var flxColumn31 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn31",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn31.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon31 = new kony.ui.Label({
            "id": "lblErrorIcon31",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg31 = new kony.ui.Label({
            "id": "lblErrorMsg31",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn31.add(lblErrorIcon31, lblErrorMsg31);
        var flxColumn32 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn32",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "76%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxColumn32.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon32 = new kony.ui.Label({
            "id": "lblErrorIcon32",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorMsg32 = new kony.ui.Label({
            "id": "lblErrorMsg32",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
            "top": "0dp",
            "width": "89%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn32.add(lblErrorIcon32, lblErrorMsg32);
        flxErrorRow3.add(flxColumn31, flxColumn32);
        flxLimitsContent.add(flxRow1, flxErrorRow1, lblSeperator1, flxRow2, flxErrorRow2, lblSeperator2, flxRow3, flxErrorRow3);
        flxLimits.add(flxLimitsHeader, lblSeperator, flxLimitsContent);
        flxLimitsContainer.add(flxHeader, flxLimits);
        flxAssignLimits.add(flxLimitsContainer);
        return flxAssignLimits;
    }
})