define("AuthModule/frmErrorLogin", function() {
    return function(controller) {
        function addWidgetsfrmErrorLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var frmErrorLoginWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "frmErrorLoginWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            frmErrorLoginWrapper.setDefaultUnit(kony.flex.DP);
            var flxErrorLoginInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxErrorLoginInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "410dp"
            }, {}, {});
            flxErrorLoginInner.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 3
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "385dp"
            }, {}, {});
            flxIconContainer.setDefaultUnit(kony.flex.DP);
            var konyImgKA = new kony.ui.Image2({
                "bottom": "5dp",
                "centerX": "50%",
                "height": "60dp",
                "id": "konyImgKA",
                "isVisible": true,
                "skin": "slImage0e4c8240073b34d",
                "src": "infinity_dbxc360_black_2x.png",
                "width": "100dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Konysmalltxt = new kony.ui.Label({
                "id": "Konysmalltxt",
                "isVisible": false,
                "left": "100dp",
                "skin": "slLabel0bffd884822a345",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Konysmalltxt\")",
                "top": "70dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIconContainer.add(konyImgKA, Konysmalltxt);
            flxHeader.add(flxIconContainer);
            var flxMainError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxMainError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "480dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0d1de3266f24048",
                "top": "-15dp",
                "width": "385px",
                "zIndex": 1
            }, {}, {});
            flxMainError.setDefaultUnit(kony.flex.DP);
            var flxForgotPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxForgotPassword",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxForgotPassword.setDefaultUnit(kony.flex.DP);
            var flxFPToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "49.14%",
                "centerY": "4.62%",
                "clipBounds": true,
                "height": "9.09%",
                "id": "flxFPToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFPToastContainer.setDefaultUnit(kony.flex.DP);
            var lblFPtoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFPtoastMessage",
                "isVisible": true,
                "left": "3%",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFPtoastMessage\")",
                "top": "18%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFPToastContainer.add(lblFPtoastMessage);
            var setpwdText = new kony.ui.Label({
                "id": "setpwdText",
                "isVisible": true,
                "left": "11%",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblTimeForNewPassword\")",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var userid = new kony.ui.Label({
                "id": "userid",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                "top": "80px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtUserName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "id": "txtUserName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "secureTextEntry": false,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "102dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDisableTbxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableTbxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknTbxDisabledf3f3f3",
                "top": "102dp",
                "width": "271dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableTbxUserName.setDefaultUnit(kony.flex.DP);
            flxDisableTbxUserName.add();
            var newpwdtext = new kony.ui.Label({
                "id": "newpwdtext",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblNewPassword\")",
                "top": "172px",
                "width": "50%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRulesFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRulesFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "11%",
                "skin": "sknCursor",
                "top": "172dp",
                "width": "40px"
            }, {}, {});
            flxRulesFP.setDefaultUnit(kony.flex.DP);
            var lblRulesFP = new kony.ui.Label({
                "id": "lblRulesFP",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl00AAEELatoMed12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRulesFP.add(lblRulesFP);
            var pwdrules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "155dp",
                "id": "pwdrules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "10px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            pwdrules.setDefaultUnit(kony.flex.DP);
            var pwdheading = new kony.ui.Label({
                "id": "pwdheading",
                "isVisible": false,
                "left": "19dp",
                "skin": "slLabel0ff9cfa99545646",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesA\")",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlb2 = new kony.ui.Label({
                "id": "pwdlb2",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesB\")",
                "top": "36px",
                "width": "265px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl4 = new kony.ui.Label({
                "id": "pwdlbl4",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesC\")",
                "top": "104px",
                "width": "265px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl1 = new kony.ui.Label({
                "id": "pwdlbl1",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.pwdlbl1\")",
                "top": "16px",
                "width": "265px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var pwdlbl3 = new kony.ui.Label({
                "id": "pwdlbl3",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesE\")",
                "top": "56px",
                "width": "265px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot1 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot1",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "22px",
                "width": "5px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot2 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot2",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "42px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot3 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot3",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "62px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dot4 = new kony.ui.Label({
                "height": "5dp",
                "id": "dot4",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "109px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            pwdrules.add(pwdheading, pwdlb2, pwdlbl4, pwdlbl1, pwdlbl3, dot1, dot2, dot3, dot4);
            var imgDownArrowFP = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrowFP",
                "isVisible": false,
                "right": "45dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "163dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxForgetPasswordInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxForgetPasswordInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "185dp",
                "width": "100%"
            }, {}, {});
            flxForgetPasswordInner.setDefaultUnit(kony.flex.DP);
            var flxNewPasswordTxtBoxContainerFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxNewPasswordTxtBoxContainerFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "273px"
            }, {}, {});
            flxNewPasswordTxtBoxContainerFP.setDefaultUnit(kony.flex.DP);
            var txtNewPassword1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40px",
                "id": "txtNewPassword1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.EnterNewPassword\")",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEyeIconFPContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconFPContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4px",
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "2px",
                "width": "36px"
            }, {}, {});
            flxEyeIconFPContainer.setDefaultUnit(kony.flex.DP);
            var flxEyecrossFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossFP.setDefaultUnit(kony.flex.DP);
            var lblEyecrossFP = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossFP",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossFP.add(lblEyecrossFP);
            var flxEyeiconFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconFP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconFP.setDefaultUnit(kony.flex.DP);
            var lblEyeiconFP = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconFP",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconFP.add(lblEyeiconFP);
            flxEyeIconFPContainer.add(flxEyecrossFP, flxEyeiconFP);
            flxNewPasswordTxtBoxContainerFP.add(txtNewPassword1, flxEyeIconFPContainer);
            var flxErrorMsgNewPasswordFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorMsgNewPasswordFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "273px"
            }, {}, {});
            flxErrorMsgNewPasswordFP.setDefaultUnit(kony.flex.DP);
            var lblErrorMsgNewPasswordFP = new kony.ui.Label({
                "id": "lblErrorMsgNewPasswordFP",
                "isVisible": false,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Password must contain at least one uppercase letter, one lowercase letter, and one numer.",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorIconNewPasswordFP = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIconNewPasswordFP",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgNewPasswordFP.add(lblErrorMsgNewPasswordFP, lblErrorIconNewPasswordFP);
            var reenterpwdtext = new kony.ui.Label({
                "id": "reenterpwdtext",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "top": "10px",
                "width": "80%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxReenterPasswordTxtBoxContainerFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxReenterPasswordTxtBoxContainerFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "273px"
            }, {}, {});
            flxReenterPasswordTxtBoxContainerFP.setDefaultUnit(kony.flex.DP);
            var txtReenterPassword1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40px",
                "id": "txtReenterPassword1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEyeIconFPReEnterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconFPReEnterContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4px",
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "2px",
                "width": "36px"
            }, {}, {});
            flxEyeIconFPReEnterContainer.setDefaultUnit(kony.flex.DP);
            var flxEyeiconFPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconFPReEnter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconFPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyeiconFPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconFPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconFPReEnter.add(lblEyeiconFPReEnter);
            var flxEyecrossFPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossFPReEnter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossFPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyecrossFPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossFPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossFPReEnter.add(lblEyecrossFPReEnter);
            flxEyeIconFPReEnterContainer.add(flxEyeiconFPReEnter, flxEyecrossFPReEnter);
            flxReenterPasswordTxtBoxContainerFP.add(txtReenterPassword1, flxEyeIconFPReEnterContainer);
            var flxErrorMsgReenterPasswordFP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorMsgReenterPasswordFP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "273px"
            }, {}, {});
            flxErrorMsgReenterPasswordFP.setDefaultUnit(kony.flex.DP);
            var lblErrorMsgReenterPasswordFP = new kony.ui.Label({
                "id": "lblErrorMsgReenterPasswordFP",
                "isVisible": false,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Password must contain at least one uppercase letter, one lowercase letter, and one numer.",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorIconReenterPasswordFP = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIconReenterPasswordFP",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgReenterPasswordFP.add(lblErrorMsgReenterPasswordFP, lblErrorIconReenterPasswordFP);
            var btnResetPassword1 = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnResetPassword1",
                "isVisible": true,
                "right": "40dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnResetPassword\")",
                "top": "15px",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "Set Password"
            });
            flxForgetPasswordInner.add(flxNewPasswordTxtBoxContainerFP, flxErrorMsgNewPasswordFP, reenterpwdtext, flxReenterPasswordTxtBoxContainerFP, flxErrorMsgReenterPasswordFP, btnResetPassword1);
            flxForgotPassword.add(flxFPToastContainer, setpwdText, userid, txtUserName, flxDisableTbxUserName, newpwdtext, flxRulesFP, pwdrules, imgDownArrowFP, flxForgetPasswordInner);
            var flxDownTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxDownTime",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxDownTime.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblErrorMsg\")",
                "top": "216px",
                "width": "65%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRetry = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnRetry",
                "isVisible": true,
                "left": "36dp",
                "onClick": controller.AS_Button_j56af8bb73c74538a56fed0c025e0fe7,
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnRetry\")",
                "top": "296px",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "Retry"
            });
            var imgErrorIcon = new kony.ui.Image2({
                "centerX": "50%",
                "height": "74px",
                "id": "imgErrorIcon",
                "isVisible": true,
                "left": "566dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "timeerror.png",
                "top": "90dp",
                "width": "74px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCallSupport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupport",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "366dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupport.setDefaultUnit(kony.flex.DP);
            var lblCallsupport = new kony.ui.Label({
                "id": "lblCallsupport",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileno = new kony.ui.Label({
                "id": "lblMobileno",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupport.add(lblCallsupport, lblMobileno);
            flxDownTime.add(lblErrorMsg, btnRetry, imgErrorIcon, flxCallSupport);
            var flxChangePassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440px",
                "id": "flxChangePassword",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxChangePassword.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "36px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "3%",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbltoastMessage\")",
                "top": "18%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(lbltoastMessage);
            var flxPwdRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "160px",
                "id": "flxPwdRules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknLblTabShadow",
                "top": "13px",
                "width": "90%",
                "zIndex": 10
            }, {}, {});
            flxPwdRules.setDefaultUnit(kony.flex.DP);
            var lblRulesA = new kony.ui.Label({
                "id": "lblRulesA",
                "isVisible": false,
                "left": "19dp",
                "skin": "slLabel0ff9cfa99545646",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesA\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesB = new kony.ui.Label({
                "id": "lblRulesB",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesB\")",
                "top": "45px",
                "width": "260px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesC = new kony.ui.Label({
                "id": "lblRulesC",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesC\")",
                "top": "115px",
                "width": "260px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesD = new kony.ui.Label({
                "id": "lblRulesD",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesD\")",
                "top": "12px",
                "width": "260px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesE = new kony.ui.Label({
                "id": "lblRulesE",
                "isVisible": true,
                "left": "35px",
                "skin": "slLabel0fbdfd22dddc04b",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblRulesE\")",
                "top": "65px",
                "width": "260px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesF = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesF",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "19px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesG = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesG",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "50px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesH = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesH",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "70px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRulesI = new kony.ui.Label({
                "height": "5dp",
                "id": "lblRulesI",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0baf3ed3ee2974e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "120px",
                "width": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPwdRules.add(lblRulesA, lblRulesB, lblRulesC, lblRulesD, lblRulesE, lblRulesF, lblRulesG, lblRulesH, lblRulesI);
            var imgDownArrowCP = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrowCP",
                "isVisible": false,
                "right": "45dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "171dp",
                "width": "15dp",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxChangePasswordInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChangePasswordInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxChangePasswordInner.setDefaultUnit(kony.flex.DP);
            var lblTimeForNewPassword = new kony.ui.Label({
                "id": "lblTimeForNewPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblTimeForNewPassword\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPasswordExpired = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16px",
                "id": "flxPasswordExpired",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPasswordExpired.setDefaultUnit(kony.flex.DP);
            var lblPasswordExpired = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblPasswordExpired",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblPasswordExpired\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPasswordExpired.add(lblPasswordExpired);
            var lblCurrentPassword = new kony.ui.Label({
                "id": "lblCurrentPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCurrentPassword\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCurrentPasswordTxtBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCurrentPasswordTxtBoxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "273px"
            }, {}, {});
            flxCurrentPasswordTxtBoxContainer.setDefaultUnit(kony.flex.DP);
            var txtCurrentPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40px",
                "id": "txtCurrentPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.EnterCurrentPassword\")",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEyeIconCPCurrentContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconCPCurrentContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4px",
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "2px",
                "width": "36px"
            }, {}, {});
            flxEyeIconCPCurrentContainer.setDefaultUnit(kony.flex.DP);
            var flxEyecrossCPCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPCurrent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPCurrent.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPCurrent = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPCurrent",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPCurrent.add(lblEyecrossCPCurrent);
            var flxEyeiconCPCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPCurrent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPCurrent.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPCurrent = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPCurrent",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPCurrent.add(lblEyeiconCPCurrent);
            flxEyeIconCPCurrentContainer.add(flxEyecrossCPCurrent, flxEyeiconCPCurrent);
            flxCurrentPasswordTxtBoxContainer.add(txtCurrentPassword, flxEyeIconCPCurrentContainer);
            var flxErrorMsgCurrentPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorMsgCurrentPassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "273px"
            }, {}, {});
            flxErrorMsgCurrentPassword.setDefaultUnit(kony.flex.DP);
            var lblIncorrectCurrentPassword = new kony.ui.Label({
                "id": "lblIncorrectCurrentPassword",
                "isVisible": false,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblIncorrectCurrentPassword\")",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorIconCurrentPassword = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIconCurrentPassword",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgCurrentPassword.add(lblIncorrectCurrentPassword, lblErrorIconCurrentPassword);
            var flxNewPasswordRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewPasswordRules",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxNewPasswordRules.setDefaultUnit(kony.flex.DP);
            var lblNewPassword = new kony.ui.Label({
                "id": "lblNewPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblNewPassword\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRules",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "sknCursor",
                "top": "0px",
                "width": "40px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRules.setDefaultUnit(kony.flex.DP);
            var lblRules = new kony.ui.Label({
                "id": "lblRules",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl00AAEELatoMed12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRules.add(lblRules);
            flxNewPasswordRules.add(lblNewPassword, flxRules);
            var flxNewPasswordTxtBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxNewPasswordTxtBoxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "273px"
            }, {}, {});
            flxNewPasswordTxtBoxContainer.setDefaultUnit(kony.flex.DP);
            var txtNewPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40px",
                "id": "txtNewPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.EnterNewPassword\")",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEyeIconCPNewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconCPNewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4px",
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "2px",
                "width": "36px"
            }, {}, {});
            flxEyeIconCPNewContainer.setDefaultUnit(kony.flex.DP);
            var flxEyecrossCPNew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPNew.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPNew = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPNew",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPNew.add(lblEyecrossCPNew);
            var flxEyeiconCPNew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPNew",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPNew.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPNew = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPNew",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPNew.add(lblEyeiconCPNew);
            flxEyeIconCPNewContainer.add(flxEyecrossCPNew, flxEyeiconCPNew);
            flxNewPasswordTxtBoxContainer.add(txtNewPassword, flxEyeIconCPNewContainer);
            var flxErrorMsgNewPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorMsgNewPassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "273px"
            }, {}, {});
            flxErrorMsgNewPassword.setDefaultUnit(kony.flex.DP);
            var lblPasswordNotValid = new kony.ui.Label({
                "id": "lblPasswordNotValid",
                "isVisible": false,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Password must contain at least one uppercase letter, one lowercase letter, and one numer.",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorIconNewPassword = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIconNewPassword",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgNewPassword.add(lblPasswordNotValid, lblErrorIconNewPassword);
            var lblReenterPassword = new kony.ui.Label({
                "id": "lblReenterPassword",
                "isVisible": true,
                "left": "11%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxReenterPasswordTxtBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxReenterPasswordTxtBoxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "273px"
            }, {}, {});
            flxReenterPasswordTxtBoxContainer.setDefaultUnit(kony.flex.DP);
            var txtReenterPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40px",
                "id": "txtReenterPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblReenterPassword\")",
                "secureTextEntry": true,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "271dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEyeIconCPReEnterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconCPReEnterContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4px",
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "2px",
                "width": "36px"
            }, {}, {});
            flxEyeIconCPReEnterContainer.setDefaultUnit(kony.flex.DP);
            var flxEyeiconCPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeiconCPReEnter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeiconCPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyeiconCPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeiconCPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeiconCPReEnter.add(lblEyeiconCPReEnter);
            var flxEyecrossCPReEnter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecrossCPReEnter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecrossCPReEnter.setDefaultUnit(kony.flex.DP);
            var lblEyecrossCPReEnter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecrossCPReEnter",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecrossCPReEnter.add(lblEyecrossCPReEnter);
            flxEyeIconCPReEnterContainer.add(flxEyeiconCPReEnter, flxEyecrossCPReEnter);
            flxReenterPasswordTxtBoxContainer.add(txtReenterPassword, flxEyeIconCPReEnterContainer);
            var flxErrorMsgReenterNewPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorMsgReenterNewPassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "273px"
            }, {}, {});
            flxErrorMsgReenterNewPassword.setDefaultUnit(kony.flex.DP);
            var lblPasswordDontMatch = new kony.ui.Label({
                "id": "lblPasswordDontMatch",
                "isVisible": false,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblPasswordDontMatch\")",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorIconReenterNewPassword = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIconReenterNewPassword",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMsgReenterNewPassword.add(lblPasswordDontMatch, lblErrorIconReenterNewPassword);
            var flxButtonsCP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxButtonsCP",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "271px"
            }, {}, {});
            flxButtonsCP.setDefaultUnit(kony.flex.DP);
            var flxCancelText = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxCancelText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "hoverhandSkin2",
                "top": "0px",
                "width": "60px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxCancelText.setDefaultUnit(kony.flex.DP);
            var lblCancelText = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCancelText",
                "isVisible": true,
                "skin": "sknlblLatoRegularBlue13px",
                "text": "Cancel",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCancelText.add(lblCancelText);
            var btnCancel = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "slButtonGlossRed0cad9addff35240",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": false,
                "left": "36dp",
                "onClick": controller.AS_Button_fd2d4688e7114c4cb16c25d02a266c33,
                "skin": "btnSkinBlueEnabled",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "400dp",
                "width": "271dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Reset password"
            });
            var btnResetPassword = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnResetPassword",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnResetPassword\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "Change Password"
            });
            flxButtonsCP.add(flxCancelText, btnCancel, btnResetPassword);
            flxChangePasswordInner.add(lblTimeForNewPassword, flxPasswordExpired, lblCurrentPassword, flxCurrentPasswordTxtBoxContainer, flxErrorMsgCurrentPassword, flxNewPasswordRules, flxNewPasswordTxtBoxContainer, flxErrorMsgNewPassword, lblReenterPassword, flxReenterPasswordTxtBoxContainer, flxErrorMsgReenterNewPassword, flxButtonsCP);
            flxChangePassword.add(flxToastContainer, flxPwdRules, imgDownArrowCP, flxChangePasswordInner);
            var flxForgotEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "338dp",
                "id": "flxForgotEmail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxForgotEmail.setDefaultUnit(kony.flex.DP);
            var registertextbox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "registertextbox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Enter_Registered_EmailID\")",
                "right": "40dp",
                "secureTextEntry": false,
                "skin": "skinPasswordTextField",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "180px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var registerLbl = new kony.ui.Label({
                "height": "30px",
                "id": "registerLbl",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.registerLbl\")",
                "top": "142px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var flxForgotEmailErrorMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxForgotEmailErrorMsg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "225px",
                "width": "273px"
            }, {}, {});
            flxForgotEmailErrorMsg.setDefaultUnit(kony.flex.DP);
            var registerErrorMsgTextbox = new kony.ui.Label({
                "id": "registerErrorMsgTextbox",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Registererrormsg\")",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblForgotEmailErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblForgotEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxForgotEmailErrorMsg.add(registerErrorMsgTextbox, lblForgotEmailErrorIcon);
            var ResetLInk = new kony.ui.Label({
                "height": "30px",
                "id": "ResetLInk",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.ResetLInk\")",
                "top": "92px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var underline = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "groupCells": false,
                "height": "13.89%",
                "id": "underline",
                "isVisible": false,
                "left": "10%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg0a4ad1052cc5246",
                "rowSkin": "seg0a46ee49b8f994e",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0c51fb80aad8644",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d6c1d600",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "7%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var heading = new kony.ui.Label({
                "height": "30px",
                "id": "heading",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.heading\")",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var tbxnulltextboxKA = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxnulltextboxKA",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "110dp",
                "secureTextEntry": false,
                "skin": "slTextBox0ccadf8e598664b",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "216dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var tbxwrongTextboxKA = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxwrongTextboxKA",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "131dp",
                "secureTextEntry": false,
                "skin": "slTextBox0ccadf8e598664b",
                "text": "admin@konybank.com",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "215dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxForgetEmailButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxForgetEmailButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "260px"
            }, {}, {});
            flxForgetEmailButtons.setDefaultUnit(kony.flex.DP);
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0px",
                "width": "120dp",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxCancel.setDefaultUnit(kony.flex.DP);
            var Canceltext = new kony.ui.Label({
                "centerY": "50%",
                "id": "Canceltext",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegularBlue13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.BackToSignInCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCancel.add(Canceltext);
            var ContinueButton = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "ContinueButton",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.RequestResetLinkCAPS\")",
                "top": "0dp",
                "width": "191dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "CONTINUE"
            });
            flxForgetEmailButtons.add(flxCancel, ContinueButton);
            flxForgotEmail.add(registertextbox, registerLbl, flxForgotEmailErrorMsg, ResetLInk, underline, heading, tbxnulltextboxKA, tbxwrongTextboxKA, flxForgetEmailButtons);
            var flxExpiredForgetPasswordLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "338dp",
                "id": "flxExpiredForgetPasswordLink",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxExpiredForgetPasswordLink.setDefaultUnit(kony.flex.DP);
            var lblExpiredLinkHeader = new kony.ui.Label({
                "height": "30px",
                "id": "lblExpiredLinkHeader",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Expired_Link_Header\")",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblExpiredLinkMsg = new kony.ui.Label({
                "height": "100dp",
                "id": "lblExpiredLinkMsg",
                "isVisible": true,
                "left": "40dp",
                "right": "40dp",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Expired_Link_Msg\")",
                "top": "92px",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var btnSendNewLink = new kony.ui.Button({
                "bottom": "40dp",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSendNewLink",
                "isVisible": true,
                "right": "40dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnResetPassword\")",
                "width": "191dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "CONTINUE"
            });
            flxExpiredForgetPasswordLink.add(lblExpiredLinkHeader, lblExpiredLinkMsg, btnSendNewLink);
            var flxNewForgetPasswordLinkSend = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "338dp",
                "id": "flxNewForgetPasswordLinkSend",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxNewForgetPasswordLinkSend.setDefaultUnit(kony.flex.DP);
            var lblNewLinkSendHeader = new kony.ui.Label({
                "height": "30px",
                "id": "lblNewLinkSendHeader",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.check_your_email\")",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblNewLinkSendMsg = new kony.ui.Label({
                "id": "lblNewLinkSendMsg",
                "isVisible": true,
                "left": "40dp",
                "right": "40dp",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Send_New_Link_Msg\")",
                "top": "92px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var btnClose = new kony.ui.Button({
                "bottom": "40dp",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnClose",
                "isVisible": true,
                "right": "40dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.closeCAPS\")",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "CONTINUE"
            });
            flxNewForgetPasswordLinkSend.add(lblNewLinkSendHeader, lblNewLinkSendMsg, btnClose);
            var flxMail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxMail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxMail.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "id": "lbl1",
                "isVisible": true,
                "left": "10%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl1\")",
                "top": "170px",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRegistererrormsgKA = new kony.ui.Label({
                "id": "lblRegistererrormsgKA",
                "isVisible": false,
                "left": "10%",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Registererrormsg\")",
                "top": "53%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var verificationlnk = new kony.ui.Label({
                "centerX": "50%",
                "id": "verificationlnk",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.verificationlnk\")",
                "top": "96px",
                "width": "80%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SegunderlineKA = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "groupCells": false,
                "height": "13.89%",
                "id": "SegunderlineKA",
                "isVisible": false,
                "left": "10%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg0a4ad1052cc5246",
                "rowSkin": "seg0a46ee49b8f994e",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0c51fb80aad8644",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d6c1d600",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "7%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var mailTxt = new kony.ui.Label({
                "centerX": "50%",
                "id": "mailTxt",
                "isVisible": true,
                "skin": "sknlblLatoRegular20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.mailTxt\")",
                "top": "64px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmail = new kony.ui.Label({
                "id": "lblEmail",
                "isVisible": true,
                "left": "10%",
                "skin": "sknlblLatoBold0d5b3368f619940",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblEmail\")",
                "top": "194px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl3 = new kony.ui.Label({
                "id": "lbl3",
                "isVisible": false,
                "left": "10%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl3\")",
                "top": "52%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl4 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lbl4",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lbl4\")",
                "top": "320px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl5 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lbl5",
                "isVisible": false,
                "skin": "sknlblLatoRegularBlue13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.contacttext\")",
                "top": "340px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7513pxHoverCursor"
            });
            var Resendlbl = new kony.ui.RichText({
                "id": "Resendlbl",
                "isVisible": true,
                "right": "11%",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Resend\")",
                "top": "170px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "linkFocusSkin": "slRichText0e25a156e33f94bHover"
            });
            var underline3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "underline3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "underlinelight0h40367f3fcfe4d",
                "top": "300px",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            underline3.setDefaultUnit(kony.flex.DP);
            underline3.add();
            var flxCallSupport2KA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupport2KA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "375dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupport2KA.setDefaultUnit(kony.flex.DP);
            var lblOrCallSupport2KA = new kony.ui.Label({
                "id": "lblOrCallSupport2KA",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileNumber2KA = new kony.ui.Label({
                "id": "lblMobileNumber2KA",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupport2KA.add(lblOrCallSupport2KA, lblMobileNumber2KA);
            flxMail.add(lbl1, lblRegistererrormsgKA, verificationlnk, SegunderlineKA, mailTxt, lblEmail, lbl3, lbl4, lbl5, Resendlbl, underline3, flxCallSupport2KA);
            var flxExcededAttempts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxExcededAttempts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxExcededAttempts.setDefaultUnit(kony.flex.DP);
            var successmsg = new kony.ui.Label({
                "centerX": "50%",
                "id": "successmsg",
                "isVisible": true,
                "left": "13%",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.successmsg\")",
                "top": "184px",
                "width": "60%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var underlinelight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "underlinelight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "underlinelight0h40367f3fcfe4d",
                "top": "300px",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            underlinelight.setDefaultUnit(kony.flex.DP);
            underlinelight.add();
            var contacttext = new kony.ui.Label({
                "centerX": "50%",
                "id": "contacttext",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.contacttext\")",
                "top": "330dp",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var error2img = new kony.ui.Image2({
                "centerX": "50%",
                "height": "59px",
                "id": "error2img",
                "isVisible": true,
                "left": "92dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "error2.png",
                "top": "100px",
                "width": "139px",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCallSupportKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCallSupportKA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "360dp",
                "width": "246dp"
            }, {}, {});
            flxCallSupportKA.setDefaultUnit(kony.flex.DP);
            var lblcallsupportKA = new kony.ui.Label({
                "id": "lblcallsupportKA",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCallsupport\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblmobilenoKA = new kony.ui.Label({
                "id": "lblmobilenoKA",
                "isVisible": true,
                "left": "3px",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblMobileno\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCallSupportKA.add(lblcallsupportKA, lblmobilenoKA);
            flxExcededAttempts.add(successmsg, underlinelight, contacttext, error2img, flxCallSupportKA);
            var flxResetPasswordConfirm = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxResetPasswordConfirm",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0f1aa6e27397f40",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxResetPasswordConfirm.setDefaultUnit(kony.flex.DP);
            var imgSuccess = new kony.ui.Image2({
                "centerX": "50%",
                "height": "58dp",
                "id": "imgSuccess",
                "isVisible": true,
                "left": "86dp",
                "skin": "slImage0e4c8240073b34d",
                "src": "success1.png",
                "top": "111dp",
                "width": "142dp",
                "zIndex": 20
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSuccessMessage = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblSuccessMessage",
                "isVisible": true,
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblSuccessMessage\")",
                "top": "197px",
                "width": "100%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRelogin = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnRelogin",
                "isVisible": true,
                "left": "36dp",
                "onClick": controller.AS_Button_fcd23acf92dc4eb48b87832870f60bcc,
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.btnRelogin\")",
                "top": "320dp",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px",
                "toolTip": "Re-Sign in"
            });
            flxResetPasswordConfirm.add(imgSuccess, lblSuccessMessage, btnRelogin);
            flxMainError.add(flxForgotPassword, flxDownTime, flxChangePassword, flxForgotEmail, flxExpiredForgetPasswordLink, flxNewForgetPasswordLinkSend, flxMail, flxExcededAttempts, flxResetPasswordConfirm);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50px",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "400dp",
                "zIndex": 1
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var imgLoginFooterLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "imgLoginFooterLogo",
                "isVisible": true,
                "left": "8%",
                "skin": "slImage",
                "src": "logo_2x.png",
                "top": "23dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCopright = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCopright",
                "isVisible": true,
                "left": "5px",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCopright\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFooterText = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFooterText",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblSansPro546e7a12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFooterText\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFooter.add(imgLoginFooterLogo, lblCopright, lblFooterText);
            flxErrorLoginInner.add(flxHeader, flxMainError, flxFooter);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            frmErrorLoginWrapper.add(flxErrorLoginInner, flxLoading);
            this.add(frmErrorLoginWrapper);
        };
        return [{
            "addWidgets": addWidgetsfrmErrorLogin,
            "enabledForIdleTimeout": false,
            "id": "frmErrorLogin",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a7af3565c840460f98fbc9638c2db7e1(eventobject);
            },
            "skin": "sknfrm11"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_g4cef829c2384a968122661209cb0326,
            "retainScrollPosition": false
        }]
    }
});