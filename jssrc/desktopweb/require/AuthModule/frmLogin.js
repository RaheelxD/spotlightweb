define("AuthModule/frmLogin", function() {
    return function(controller) {
        function addWidgetsfrmLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "700dp",
                "id": "flxMainLogin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0bec2566294e74d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainLogin.setDefaultUnit(kony.flex.DP);
            var lblKony = new kony.ui.Label({
                "id": "lblKony",
                "isVisible": false,
                "left": "360dp",
                "skin": "slLabel0a8c6a6ddbc5f41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.kony\")",
                "top": "110dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTimeExpired = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxTimeExpired",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e8d4a065064f41",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTimeExpired.setDefaultUnit(kony.flex.DP);
            var flxCross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxCross",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "17dp",
                "skin": "slFbox",
                "width": "13dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCross.setDefaultUnit(kony.flex.DP);
            var lblCross = new kony.ui.Label({
                "height": "100%",
                "id": "lblCross",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxWhite",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCross.add(lblCross);
            var flxtimeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxtimeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "390dp",
                "zIndex": 1
            }, {}, {});
            flxtimeContainer.setDefaultUnit(kony.flex.DP);
            var lblTimeExpired = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblTimeExpired",
                "isVisible": true,
                "skin": "slLabel0f65ab23851324d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblTimeExpired\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTimeout = new kony.ui.Label({
                "centerY": "50%",
                "height": "32dp",
                "id": "lblTimeout",
                "isVisible": true,
                "left": "19dp",
                "skin": "sknIcon32pxWhiteNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblTimeout\")",
                "width": "32dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxtimeContainer.add(lblTimeExpired, lblTimeout);
            flxTimeExpired.add(flxCross, flxtimeContainer);
            var flxUnderlinelight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxUnderlinelight",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "515dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "underlinelight0h970648ba15246",
                "top": "400dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            flxUnderlinelight.setDefaultUnit(kony.flex.DP);
            flxUnderlinelight.add();
            var flxLoginInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "510dp",
                "id": "flxLoginInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "770dp"
            }, {}, {});
            flxLoginInner.setDefaultUnit(kony.flex.DP);
            var flxLoginWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "440px",
                "id": "flxLoginWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "770px",
                "zIndex": 1
            }, {}, {});
            flxLoginWrapper.setDefaultUnit(kony.flex.DP);
            var flxKonyLogo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxKonyLogo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknFlxBgGradTop012647Btm003E75RounddLeft",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxKonyLogo.setDefaultUnit(kony.flex.DP);
            var KonyImg = new kony.ui.Image2({
                "height": "60dp",
                "id": "KonyImg",
                "isVisible": false,
                "left": "100dp",
                "skin": "slImage0ie17d400557f45",
                "src": "loader.png",
                "top": "20dp",
                "width": "77dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var KonyBankLbl = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "KonyBankLbl",
                "isVisible": false,
                "skin": "slLabel0hc85d74cc7f74e",
                "text": "Customer 360",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Welcomelbl = new kony.ui.Label({
                "id": "Welcomelbl",
                "isVisible": false,
                "left": "38dp",
                "skin": "slLabel0ea43a2a5545d46",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Welcomelbl\")",
                "top": "230dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var konytextlogo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "70dp",
                "id": "konytextlogo",
                "isVisible": true,
                "skin": "slImage",
                "src": "infinity_login_2x.png",
                "width": "200dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var konybgimg = new kony.ui.Image2({
                "height": "235dp",
                "id": "konybgimg",
                "isVisible": false,
                "left": "149dp",
                "skin": "slImage0ie17d400557f45",
                "src": "konybg.png",
                "top": "219dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRelease = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblRelease",
                "isVisible": false,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "202101",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxKonyLogo.add(KonyImg, KonyBankLbl, Welcomelbl, konytextlogo, konybgimg, lblRelease);
            var flxLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "440dp",
                "id": "flxLogin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "385dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0i27a4037c45c43",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxLogin.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "height": "30px",
                "id": "lblHeading",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlbl484b52SourceSans16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.welcomeHeader\")",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblUserid = new kony.ui.Label({
                "id": "lblUserid",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
                "top": "100dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtUserName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtUserName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Enter_Username\")",
                "right": 40,
                "secureTextEntry": false,
                "skin": "Skinuseridfield0bc733c4148544e",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "125dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_b9c2411768f14bf19f3784bfbd790d85
            });
            var lblPassword = new kony.ui.Label({
                "id": "lblPassword",
                "isVisible": true,
                "left": "40dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPasswordUser\")",
                "top": "200dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "txtPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Enter_Password\")",
                "right": 40,
                "secureTextEntry": true,
                "skin": "skinPasswordLogin",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "225dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "onBeginEditing": controller.AS_TextField_b8175308d8424df495ecc3ca9d37b599
            });
            var chkbxRemember = new kony.ui.CheckBoxGroup({
                "height": "23px",
                "id": "chkbxRemember",
                "isVisible": false,
                "left": "42dp",
                "masterData": [
                    ["true", "Remember me"]
                ],
                "selectedKeys": ["true"],
                "skin": "slCheckBoxGroup0h1860dff06d344",
                "top": "242dp",
                "width": "50%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLoginRememberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxLoginRememberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "42dp",
                "isModalContainer": false,
                "top": "300dp",
                "width": "130px",
                "zIndex": 1
            }, {}, {});
            flxLoginRememberMe.setDefaultUnit(kony.flex.DP);
            var flxRememberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRememberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRememberMe.setDefaultUnit(kony.flex.DP);
            var imgLoginRememberMe = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLoginRememberMe",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRememberMe.add(imgLoginRememberMe);
            var lblLoginRememberMe = new kony.ui.Label({
                "id": "lblLoginRememberMe",
                "isVisible": true,
                "left": "25dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblLoginRememberMe\")",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoginRememberMe.add(flxRememberMe, lblLoginRememberMe);
            var btnImage = new kony.ui.Button({
                "focusSkin": "slButtonGlossRed0ac4572db3b9342",
                "height": "25dp",
                "id": "btnImage",
                "isVisible": false,
                "left": "250dp",
                "skin": "slButtonGlossBlue0a0c10b45f20d47",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "182dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsernameErrorMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUsernameErrorMsg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170px",
                "width": "273px"
            }, {}, {});
            flxUsernameErrorMsg.setDefaultUnit(kony.flex.DP);
            var lblUsernameErrorMsg = new kony.ui.Label({
                "id": "lblUsernameErrorMsg",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblUsernameErrorMsg\")",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUsernameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUsernameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsernameErrorMsg.add(lblUsernameErrorMsg, lblUsernameErrorIcon);
            var flxPasswordErrorMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPasswordErrorMsg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11%",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "270px",
                "width": "273px"
            }, {}, {});
            flxPasswordErrorMsg.setDefaultUnit(kony.flex.DP);
            var lblPasswordErrorMsg = new kony.ui.Label({
                "id": "lblPasswordErrorMsg",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblPasswordErrorMsg\")",
                "top": "0px",
                "width": "256px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPasswordErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPasswordErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPasswordErrorMsg.add(lblPasswordErrorMsg, lblPasswordErrorIcon);
            var flxEyeIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxEyeIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "42px",
                "skin": "sknFlxTrans",
                "top": "227px",
                "width": "36px"
            }, {}, {});
            flxEyeIconContainer.setDefaultUnit(kony.flex.DP);
            var flxEyecross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyecross",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyecross.setDefaultUnit(kony.flex.DP);
            var lblEyecross = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "25dp",
                "id": "lblEyecross",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknEyeIconHover"
            });
            flxEyecross.add(lblEyecross);
            var flxEyeicon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeicon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEyeicon.setDefaultUnit(kony.flex.DP);
            var lblEyeicon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": 25,
                "id": "lblEyeicon",
                "isVisible": true,
                "skin": "sknEyeIcon",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeicon.add(lblEyeicon);
            flxEyeIconContainer.add(flxEyecross, flxEyeicon);
            var flxLoginButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxLoginButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "350px"
            }, {}, {});
            flxLoginButtons.setDefaultUnit(kony.flex.DP);
            var flxForgotCredentials = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxForgotCredentials",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0px",
                "width": "190px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxForgotCredentials.setDefaultUnit(kony.flex.DP);
            var lblForgotCredentials = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblForgotCredentials",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.flxForgotCredentials\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxForgotCredentials.add(lblForgotCredentials);
            var btnLogin = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnLogin",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.btnLogin\")",
                "top": "0dp",
                "width": "107dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxLoginButtons.add(flxForgotCredentials, btnLogin);
            flxLogin.add(lblHeading, lblUserid, txtUserName, lblPassword, txtPassword, chkbxRemember, flxLoginRememberMe, btnImage, flxUsernameErrorMsg, flxPasswordErrorMsg, flxEyeIconContainer, flxLoginButtons);
            var flxErrorAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxErrorAlert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "385dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0e26999f688114d",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxErrorAlert.setDefaultUnit(kony.flex.DP);
            var lblError = new kony.ui.Label({
                "height": "20dp",
                "id": "lblError",
                "isVisible": true,
                "left": "12dp",
                "skin": "sknIcon20pxWhite",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblError\")",
                "top": "15dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtError = new kony.ui.Label({
                "centerY": "50%",
                "id": "txtError",
                "isVisible": false,
                "left": "40dp",
                "right": "40px",
                "skin": "skinLabelError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.txtError\")",
                "top": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtError1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "txtError1",
                "isVisible": true,
                "left": "60dp",
                "skin": "skinLabelError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.txtError1\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlert.add(lblError, txtError, txtError1);
            var flxRedirect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRedirect",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "385dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0i27a4037c45c43",
                "top": "0dp",
                "width": "385dp",
                "zIndex": 1
            }, {}, {});
            flxRedirect.setDefaultUnit(kony.flex.DP);
            var lblRedirectMessage = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "30px",
                "id": "lblRedirectMessage",
                "isVisible": true,
                "skin": "sknlbl484b52SourceSans16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.label.redirect_keycloak\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxRedirect.add(lblRedirectMessage);
            flxLoginWrapper.add(flxKonyLogo, flxLogin, flxErrorAlert, flxRedirect);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50px",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "450dp",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var imgLoginFooterLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "imgLoginFooterLogo",
                "isVisible": true,
                "left": "25.50%",
                "skin": "slImage",
                "src": "logo_2x.png",
                "top": "23dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCopright = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCopright",
                "isVisible": true,
                "left": "5dp",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblCopright\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFooterText = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFooterText",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknLblSansPro546e7a12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.lblFooterText\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFooter.add(imgLoginFooterLogo, lblCopright, lblFooterText);
            flxLoginInner.add(flxLoginWrapper, flxFooter);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50dp",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxMainLogin.add(lblKony, flxTimeExpired, flxUnderlinelight, flxLoginInner, flxLoading, flxToastMessage);
            this.add(flxMainLogin);
        };
        return [{
            "addWidgets": addWidgetsfrmLogin,
            "enabledForIdleTimeout": false,
            "id": "frmLogin",
            "init": controller.AS_Form_af28b8dd4547421b91987dc073f49cfa,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d80f17429434469ba228dc2bf1d92f17(eventobject);
            },
            "skin": "sknfrm11"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_e4cb462ee4f54821a534ae29712c76a2,
            "retainScrollPosition": false
        }]
    }
});