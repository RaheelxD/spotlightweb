define("flxServicesListSelected", function() {
    return function(controller) {
        var flxServicesListSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxServicesListSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknflxffffffop100"
        });
        flxServicesListSelected.setDefaultUnit(kony.flex.DP);
        var flxServicesList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxServicesList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxServicesList.setDefaultUnit(kony.flex.DP);
        var flxSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxSegMain.setDefaultUnit(kony.flex.DP);
        var flxGroupsegmain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxGroupsegmain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxGroupsegmain.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b1057bf841aa44f4aa0f68532b65c2ed,
            "right": "10px",
            "skin": "sknCursor",
            "top": "15dp",
            "width": "15px",
            "zIndex": 2
        }, {}, {});
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label({
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(fonticonArrow);
        var lblGroupName = new kony.ui.Label({
            "id": "lblGroupName",
            "isVisible": true,
            "left": "3.50%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Platinum Customers",
            "top": "15dp",
            "width": "14.70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRowBusinessTypes = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowBusinessTypes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25.80%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "150dp"
        }, {}, {});
        flxRowBusinessTypes.setDefaultUnit(kony.flex.DP);
        var lblGroupBusinessType = new kony.ui.Label({
            "height": "15px",
            "id": "lblGroupBusinessType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Business banking",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRowBusinessTypes.add(lblGroupBusinessType);
        var lblContracts = new kony.ui.Label({
            "id": "lblContracts",
            "isVisible": true,
            "left": "42%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "20",
            "top": "15px",
            "width": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatures = new kony.ui.Label({
            "id": "lblFeatures",
            "isVisible": true,
            "left": "59%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "25",
            "top": "15px",
            "width": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoles = new kony.ui.Label({
            "id": "lblRoles",
            "isVisible": true,
            "left": "74.50%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "25",
            "top": "15px",
            "width": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "88.50%",
            "isModalContainer": false,
            "right": "7%",
            "skin": "slFbox",
            "top": "12px",
            "width": "67dp",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblGroupStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconGroupStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconGroupStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblGroupStatus, fontIconGroupStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "96%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ece0ede19bcf4bbd92100b472ff0641c,
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10px",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var fontIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "fontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(fontIconOptions);
        flxGroupsegmain.add(flxDropdown, lblGroupName, flxRowBusinessTypes, lblContracts, lblFeatures, lblRoles, flxStatus, flxOptions);
        var flxGroupDescriptionContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxGroupDescriptionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxGroupDescriptionContent.setDefaultUnit(kony.flex.DP);
        var flxGroupDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "91%",
            "zIndex": 20
        }, {}, {});
        flxGroupDesc.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "height": "15px",
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionValue = new kony.ui.Label({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknLblLato485c7513px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum accumsan odio efficituDonec felis dolor, molestie. Curabitur est eros, volutpat in elit rutrum, tristique placerat nunc.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin  scelerisque eleifend libero, a rhoncus sapien mattis ac. Donec dictum finibus sagittis. Vestibulum<br> accumsan odio efficitur est molestie viverra.",
            "top": "25px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupDesc.add(lblDescriptionHeader, lblDescriptionValue);
        flxGroupDescriptionContent.add(flxGroupDesc);
        flxSegMain.add(flxGroupsegmain, flxGroupDescriptionContent);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "10dp",
            "right": "10dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 20
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesList.add(flxSegMain, lblSeparator);
        flxServicesListSelected.add(flxServicesList);
        return flxServicesListSelected;
    }
})