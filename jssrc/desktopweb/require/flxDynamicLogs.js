define("flxDynamicLogs", function() {
    return function(controller) {
        var flxDynamicLogs = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "50dp",
            "id": "flxDynamicLogs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDynamicLogs.setDefaultUnit(kony.flex.DP);
        var flxLogs = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "100%",
            "id": "flxLogs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "3120dp"
        }, {}, {});
        flxLogs.setDefaultUnit(kony.flex.DP);
        var lblEvent = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEvent",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Event",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEventSubType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEventSubType",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAdminName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAdminName",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAdminRole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAdminRole",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFromAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFromAccount",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblToAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblToAccount",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmount",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrency = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrency",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRefNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRefNumber",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMaskedCardNo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMaskedCardNo",
            "isVisible": false,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPayeeName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPayeeName",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPayeeId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPayeeId",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPersonId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPersonId",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120dp"
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "top": "1dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblIconStatus, lblStatus);
        var lblMFAType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMFAType",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMFAServiceKey = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMFAServiceKey",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMFAState = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMFAState",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDeviceBrowser = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDeviceBrowser",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOs = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOs",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDeviceId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDeviceId",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIPAddress = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIPAddress",
            "isVisible": true,
            "left": "0",
            "right": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "top": 0,
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOtherInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxOtherInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "120dp"
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxOtherInfo.setDefaultUnit(kony.flex.DP);
        var lblOtherInfo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOtherInfo",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblLato13px117eb0",
            "text": "View",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOtherInfo.add(lblOtherInfo);
        flxLogs.add(lblEvent, lblEventSubType, lblAdminName, lblAdminRole, lblDate, lblFromAccount, lblToAccount, lblAmount, lblCurrency, lblRefNumber, lblMaskedCardNo, lblPayeeName, lblPayeeId, lblPersonId, lblAccount, flxStatus, lblMFAType, lblMFAServiceKey, lblMFAState, lblChannel, lblDeviceBrowser, lblOs, lblDeviceId, lblIPAddress, flxOtherInfo);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": 0,
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDynamicLogs.add(flxLogs, lblSeperator);
        return flxDynamicLogs;
    }
})