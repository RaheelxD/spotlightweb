define("userflxCompanySelectedAccountsRowController", {
    //Type your controller code here 
});
define("flxCompanySelectedAccountsRowControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxClose **/
    AS_FlexContainer_ha3f4372cafe439c95f4619c828e77b2: function AS_FlexContainer_ha3f4372cafe439c95f4619c828e77b2(eventobject, context) {
        var self = this;
        this.executeOnParent("unSelectedOption");
    }
});
define("flxCompanySelectedAccountsRowController", ["userflxCompanySelectedAccountsRowController", "flxCompanySelectedAccountsRowControllerActions"], function() {
    var controller = require("userflxCompanySelectedAccountsRowController");
    var controllerActions = ["flxCompanySelectedAccountsRowControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
