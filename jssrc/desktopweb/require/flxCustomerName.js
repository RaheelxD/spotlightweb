define("flxCustomerName", function() {
    return function(controller) {
        var flxCustomerName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustomerName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCustomerName.setDefaultUnit(kony.flex.DP);
        var flxCustomerNameCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxCustomerNameCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxCustomerNameCont.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnPrimaryCustomers = new kony.ui.Button({
            "centerY": "50%",
            "height": "20dp",
            "id": "btnPrimaryCustomers",
            "isVisible": true,
            "right": "15px",
            "skin": "sknbg1f844dborrad16pxffffff",
            "text": "Primary",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerNameCont.add(lblCustomerName, btnPrimaryCustomers);
        flxCustomerName.add(flxCustomerNameCont);
        return flxCustomerName;
    }
})