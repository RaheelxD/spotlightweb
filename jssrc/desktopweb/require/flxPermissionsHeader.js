define("flxPermissionsHeader", function() {
    return function(controller) {
        var flxPermissionsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxPermissionsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxPermissionsHeader.setDefaultUnit(kony.flex.DP);
        var flxName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ea5a1d7acb014702a3c4b891b9dbed22,
            "skin": "slFbox",
            "top": 0,
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxName.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "top": 0,
            "width": "37px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortName = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxName.add(lblName, fontIconSortName);
        var flxDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "24%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35%",
            "zIndex": 1
        }, {}, {});
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblHeaderDesc = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblHeaderDesc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "width": "90px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDescription.add(lblHeaderDesc);
        var flxRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRoles",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "65%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e5a4ae5d550e4e688d0f295ebc5136b1,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxRoles.setDefaultUnit(kony.flex.DP);
        var lblRoles = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoles",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortRoles = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortRoles",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxRoles.add(lblRoles, fontIconSortRoles);
        var flxUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "75%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxUsers.setDefaultUnit(kony.flex.DP);
        var lblUsers = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsers",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERS\")",
            "top": 0,
            "width": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconSortUser = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconSortUser",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsers.add(lblUsers, fontIconSortUser);
        var flxHeaderStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "86%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_j583552517b14a98bf96d906d9194457,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
            "top": 0,
            "width": "46px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var fontIconFilterStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxHeaderStatus.add(lblStatus, fontIconFilterStatus);
        var lblHeaderSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPermissionsHeader.add(flxName, flxDescription, flxRoles, flxUsers, flxHeaderStatus, lblHeaderSeperator);
        return flxPermissionsHeader;
    }
})