define("flxEnrollSelectedAccountsSec", function() {
    return function(controller) {
        var flxEnrollSelectedAccountsSec = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEnrollSelectedAccountsSec",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEnrollSelectedAccountsSec.setDefaultUnit(kony.flex.DP);
        var flxAccountSectionCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxAccountSectionCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxBgF5F6F8BrD7D9E0rd3pxTopRound",
            "top": "15dp",
            "zIndex": 3
        }, {}, {});
        flxAccountSectionCont.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "90%"
        }, {}, {});
        flxLeft.setDefaultUnit(kony.flex.DP);
        var flxToggleArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxToggleArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "2dp",
            "width": "15dp"
        }, {}, {});
        flxToggleArrow.setDefaultUnit(kony.flex.DP);
        var lblIconToggleArrow = new kony.ui.Label({
            "id": "lblIconToggleArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggleArrow.add(lblIconToggleArrow);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeft.add(flxToggleArrow, lblFeatureName);
        var flxRight = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, {}, {});
        flxRight.setDefaultUnit(kony.flex.DP);
        var lblStatusValue = new kony.ui.Label({
            "id": "lblStatusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "id": "lblIconStatus",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRight.add(lblStatusValue, lblIconStatus);
        flxRow1.add(flxLeft, flxRight);
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "8dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblAvailableActions = new kony.ui.Label({
            "id": "lblAvailableActions",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Available Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTotalActions = new kony.ui.Label({
            "id": "lblTotalActions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "of 2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow2.add(lblAvailableActions, lblCountActions, lblTotalActions);
        var lblSectionLine = new kony.ui.Label({
            "height": "1dp",
            "id": "lblSectionLine",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "John Doe",
            "top": "10dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountSectionCont.add(flxRow1, flxRow2, lblSectionLine);
        var flxHeaderContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxHeaderContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFbrD7D9E0r1pxLeftRight",
            "top": "80dp",
            "width": "100%"
        }, {}, {});
        flxHeaderContainer.setDefaultUnit(kony.flex.DP);
        var flxAccountNumCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxAccountNumCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "27%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxAccountNumCont.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgSectionCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgSectionCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgSectionCheckbox);
        var lblAccountNumber = new kony.ui.Label({
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000LatoReg11px"
        });
        var lblIconSortAccName = new kony.ui.Label({
            "id": "lblIconSortAccName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNumCont.add(flxCheckbox, lblAccountNumber, lblIconSortAccName);
        var flxAccountType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxAccountType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "31%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxAccountType.setDefaultUnit(kony.flex.DP);
        var lblAccountType = new kony.ui.Label({
            "id": "lblAccountType",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconFilterAccType = new kony.ui.Label({
            "id": "lblIconFilterAccType",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountType.add(lblAccountType, lblIconFilterAccType);
        var flxAccountName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxAccountName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "52%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "22%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxAccountName.setDefaultUnit(kony.flex.DP);
        var lblAccountName = new kony.ui.Label({
            "id": "lblAccountName",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000LatoReg11px"
        });
        var lblIconAccNameSort = new kony.ui.Label({
            "id": "lblIconAccNameSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountName.add(lblAccountName, lblIconAccNameSort);
        var flxAccountHolder = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxAccountHolder",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "75%",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxAccountHolder.setDefaultUnit(kony.flex.DP);
        var lblAccountHolder = new kony.ui.Label({
            "id": "lblAccountHolder",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.OwnershipType_UC\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000LatoReg11px"
        });
        var lblIconSortAccHolder = new kony.ui.Label({
            "id": "lblIconSortAccHolder",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountHolder.add(lblAccountHolder, lblIconSortAccHolder);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderContainer.add(flxAccountNumCont, flxAccountType, flxAccountName, flxAccountHolder, lblSeperator);
        flxEnrollSelectedAccountsSec.add(flxAccountSectionCont, flxHeaderContainer);
        return flxEnrollSelectedAccountsSec;
    }
})