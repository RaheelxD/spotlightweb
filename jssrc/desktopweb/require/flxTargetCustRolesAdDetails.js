define("flxTargetCustRolesAdDetails", function() {
    return function(controller) {
        var flxTargetCustRolesAdDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTargetCustRolesAdDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxHoverEAF3FB"
        });
        flxTargetCustRolesAdDetails.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "bottom": "10dp",
            "height": "17px",
            "id": "lblRoleName",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Platinum DBX Customers",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoleDescription = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblRoleDescription",
            "isVisible": true,
            "left": "27%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
            "top": "15dp",
            "width": "30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAttributesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxAttributesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "4%"
        }, {}, {});
        flxAttributesContainer.setDefaultUnit(kony.flex.DP);
        var lblAttributes = new kony.ui.Label({
            "height": "100%",
            "id": "lblAttributes",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLblLato13px117eb0",
            "text": "View",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttributesContainer.add(lblAttributes);
        var lblNoOfUsers = new kony.ui.Label({
            "bottom": "10dp",
            "height": "17px",
            "id": "lblNoOfUsers",
            "isVisible": true,
            "left": "75%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "32455",
            "top": "15dp",
            "width": "6%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxIconDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxIconDelete",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "88%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "15dp"
        }, {}, {});
        flxIconDelete.setDefaultUnit(kony.flex.DP);
        var lblDeleteIcon = new kony.ui.Label({
            "id": "lblDeleteIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblIconMoon16px192b45",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxIconDelete.add(lblDeleteIcon);
        flxTargetCustRolesAdDetails.add(lblRoleName, lblRoleDescription, flxAttributesContainer, lblNoOfUsers, flxIconDelete);
        return flxTargetCustRolesAdDetails;
    }
})