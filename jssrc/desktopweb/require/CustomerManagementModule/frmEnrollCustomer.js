define("CustomerManagementModule/frmEnrollCustomer", function() {
    return function(controller) {
        function addWidgetsfrmEnrollCustomer() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "126dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "20px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "20dp",
                        "width": "100%"
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCHCUSTOMERS\")"
                    },
                    "btnPreviousPage": {
                        "isVisible": true,
                        "text": "JOHN"
                    },
                    "btnPreviousPage1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.Enroll_UC\")",
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            flxMainHeader.add(mainHeader, flxBreadCrumbs);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "133dp",
                "width": "100%"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxEnrollCustomerContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp"
            }, {}, {});
            flxEnrollCustomerContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollCustListContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnrollCustListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxEnrollCustListContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollCusList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxEnrollCusList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollCusList.setDefaultUnit(kony.flex.DP);
            var lblEnrollCustomerTitle = new kony.ui.Label({
                "id": "lblEnrollCustomerTitle",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Enroll\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddCustomerId = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddCustomerId",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add Customer ID",
                "top": "20dp",
                "width": "122dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxEnrollCustListHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEnrollCustListHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxFFFFFF100O",
                "top": "55dp"
            }, {}, {});
            flxEnrollCustListHeader.setDefaultUnit(kony.flex.DP);
            var lblCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerId",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerId\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "25%",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomerName_UC\")",
                "width": "23%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEnrollService = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEnrollService",
                "isVisible": true,
                "left": "50%",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.Service_UC\")",
                "width": "19%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEnrollRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEnrollRole",
                "isVisible": true,
                "left": "71%",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ROLE\")",
                "width": "19%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBg696C73Op100",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            flxEnrollCustListHeader.add(lblCustomerId, lblCustomerName, lblEnrollService, lblEnrollRole, flxSeperator);
            var flxEnrollCustomerSeg = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEnrollCustomerSeg",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "105dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollCustomerSeg.setDefaultUnit(kony.flex.DP);
            var flxEnrollCusSegmentCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEnrollCusSegmentCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollCusSegmentCont.setDefaultUnit(kony.flex.DP);
            var segEnrollCustList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblCustomerId": "",
                    "lblCustomerName": "",
                    "lblIconRoleError": "",
                    "lblIconServiceError": "",
                    "lblOptions": "",
                    "lblPrimary": "",
                    "lblRemoved": "",
                    "lblRoleErrorMsg": "",
                    "lblSeperator": "",
                    "lblServiceErrorMsg": "",
                    "lstBoxRole": {
                        "masterData": []
                    },
                    "lstBoxService": {
                        "masterData": []
                    }
                }],
                "groupCells": false,
                "id": "segEnrollCustList",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxEnrollCustomerList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCustomerIdContainer": "flxCustomerIdContainer",
                    "flxEnrollCustomerList": "flxEnrollCustomerList",
                    "flxOptions": "flxOptions",
                    "flxPrimary": "flxPrimary",
                    "flxRoleError": "flxRoleError",
                    "flxServiceError": "flxServiceError",
                    "lblCustomerId": "lblCustomerId",
                    "lblCustomerName": "lblCustomerName",
                    "lblIconRoleError": "lblIconRoleError",
                    "lblIconServiceError": "lblIconServiceError",
                    "lblOptions": "lblOptions",
                    "lblPrimary": "lblPrimary",
                    "lblRemoved": "lblRemoved",
                    "lblRoleErrorMsg": "lblRoleErrorMsg",
                    "lblSeperator": "lblSeperator",
                    "lblServiceErrorMsg": "lblServiceErrorMsg",
                    "lstBoxRole": "lstBoxRole",
                    "lstBoxService": "lstBoxService"
                }
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollCusSegmentCont.add(segEnrollCustList);
            flxEnrollCustomerSeg.add(flxEnrollCusSegmentCont);
            flxEnrollCusList.add(lblEnrollCustomerTitle, btnAddCustomerId, flxEnrollCustListHeader, flxEnrollCustomerSeg);
            var flxEnrollListButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollListButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollListButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.Enroll_UC\")"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEnrollListSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEnrollListSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollListSeperator.setDefaultUnit(kony.flex.DP);
            flxEnrollListSeperator.add();
            flxEnrollListButtons.add(commonButtons, flxEnrollListSeperator);
            flxEnrollCustListContainer.add(flxEnrollCusList, flxEnrollListButtons);
            var flxContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "124dp",
                "zIndex": 10
            }, {}, {});
            flxContextualMenu.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxContextualMenuOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContextualMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContextualMenuOptions.setDefaultUnit(kony.flex.DP);
            var lblHeader = new kony.ui.Label({
                "id": "lblHeader",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato9ca9ba11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.EditUser_UC\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnLink1 = new kony.ui.Button({
                "id": "btnLink1",
                "isVisible": true,
                "left": "15dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Accounts\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnLink2 = new kony.ui.Button({
                "id": "btnLink2",
                "isVisible": true,
                "left": "15dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnLink3 = new kony.ui.Button({
                "id": "btnLink3",
                "isVisible": true,
                "left": "15dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.Limits\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnLink4 = new kony.ui.Button({
                "id": "btnLink4",
                "isVisible": false,
                "left": "15dp",
                "skin": "contextuallinks",
                "text": "Signatory Groups",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxOptionsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxOptionsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOptionsSeperator.setDefaultUnit(kony.flex.DP);
            flxOptionsSeperator.add();
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxOption1.setDefaultUnit(kony.flex.DP);
            var lblIconOption1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption1",
                "isVisible": false,
                "left": "16px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractDetails\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(lblIconOption1, lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxOption2.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "16dp",
                "skin": "sknIcon20px",
                "text": "",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Remove\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(lblIconOption2, lblOption2);
            flxContextualMenuOptions.add(lblHeader, btnLink1, btnLink2, btnLink3, btnLink4, flxOptionsSeperator, flxOption1, flxOption2);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxContextualMenu.add(flxUpArrowImage, flxContextualMenuOptions, flxDownArrowImage);
            flxEnrollCustomerContainer.add(flxEnrollCustListContainer, flxContextualMenu);
            var flxAddAnotherCustContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddAnotherCustContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxAddAnotherCustContainer.setDefaultUnit(kony.flex.DP);
            var flxAddCustomerBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddCustomerBox",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp"
            }, {}, {});
            flxAddCustomerBox.setDefaultUnit(kony.flex.DP);
            var flxAddAnotherCustScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAnotherCustScreen",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAddAnotherCustScreen.setDefaultUnit(kony.flex.DP);
            var flxAddAnotherCustHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "93dp",
                "id": "flxAddAnotherCustHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAddAnotherCustHeader.setDefaultUnit(kony.flex.DP);
            var lblAddAnotherCustHeader = new kony.ui.Label({
                "id": "lblAddAnotherCustHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AddAnotherCustomerId\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddCustSearchHeading = new kony.ui.Label({
                "id": "lblAddCustSearchHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.searchBy\")",
                "top": "54dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddRelatedCustomers = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddRelatedCustomers",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ViewRelatedCustomers\")",
                "top": "20dp",
                "width": "159dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxCustSearchErrorCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCustSearchErrorCont",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "100dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "54dp",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxCustSearchErrorCont.setDefaultUnit(kony.flex.DP);
            var lblIconCustSearchError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblIconCustSearchError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustSearchErrorText = new kony.ui.Label({
                "height": "15dp",
                "id": "lblCustSearchErrorText",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustSearchErrorCont.add(lblIconCustSearchError, lblCustSearchErrorText);
            flxAddAnotherCustHeader.add(lblAddAnotherCustHeader, lblAddCustSearchHeading, btnAddRelatedCustomers, flxCustSearchErrorCont);
            var flxAddCustBasicSearchRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddCustBasicSearchRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAddCustBasicSearchRow.setDefaultUnit(kony.flex.DP);
            var flxSearchField11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33.30%"
            }, {}, {});
            flxSearchField11.setDefaultUnit(kony.flex.DP);
            var textBoxEntry11 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "1dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField11.add(textBoxEntry11);
            var flxSearchField12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%"
            }, {}, {});
            flxSearchField12.setDefaultUnit(kony.flex.DP);
            var textBoxEntry12 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")"
                    },
                    "textBoxEntry": {
                        "left": "5dp",
                        "right": "1dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField12.add(textBoxEntry12);
            var flxSearchField13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "34.60%"
            }, {}, {});
            flxSearchField13.setDefaultUnit(kony.flex.DP);
            var textBoxEntry13 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Contracts.emailID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Contracts.emailID\")"
                    },
                    "textBoxEntry": {
                        "left": "5dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField13.add(textBoxEntry13);
            flxAddCustBasicSearchRow.add(flxSearchField11, flxSearchField12, flxSearchField13);
            var flxAddCustAdvanceSearchCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxAddCustAdvanceSearchCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAddCustAdvanceSearchCont.setDefaultUnit(kony.flex.DP);
            var flxAddCustAdvSearchRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddCustAdvSearchRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAddCustAdvSearchRow1.setDefaultUnit(kony.flex.DP);
            var flxSearchField21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxSearchField21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33.30%"
            }, {}, {});
            flxSearchField21.setDefaultUnit(kony.flex.DP);
            var lblSearchParamHeader21 = new kony.ui.Label({
                "id": "lblSearchParamHeader21",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var contactNumber21 = new com.adminConsole.common.contactNumber({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60dp",
                "id": "contactNumber21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "25dp",
                "overrides": {
                    "contactNumber": {
                        "height": "60dp",
                        "left": "20dp",
                        "right": "1dp",
                        "top": "25dp",
                        "width": "viz.val_cleared"
                    },
                    "flxContactNumberWrapper": {
                        "height": "60dp",
                        "width": "100%"
                    },
                    "flxError": {
                        "isVisible": false
                    },
                    "lblDash": {
                        "left": "75dp"
                    },
                    "txtContactNumber": {
                        "left": "90dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField21.add(lblSearchParamHeader21, contactNumber21);
            var flxSearchField22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxSearchField22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%"
            }, {}, {});
            flxSearchField22.setDefaultUnit(kony.flex.DP);
            var lblSearchParamHeader22 = new kony.ui.Label({
                "id": "lblSearchParamHeader22",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.DOB\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCalendarDob = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarDob",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "25dp"
            }, {}, {});
            flxCalendarDob.setDefaultUnit(kony.flex.DP);
            var customCalDob = new kony.ui.CustomWidget({
                "id": "customCalDob",
                "isVisible": true,
                "left": "0dp",
                "top": "5dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarDob.add(customCalDob);
            flxSearchField22.add(lblSearchParamHeader22, flxCalendarDob);
            var flxSearchField23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "34.60%",
                "zIndex": 1
            }, {}, {});
            flxSearchField23.setDefaultUnit(kony.flex.DP);
            var lblSearchParamHeader23 = new kony.ui.Label({
                "id": "lblSearchParamHeader23",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Customer Status",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSearchParam23 = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSearchParam23",
                "isVisible": true,
                "left": "15dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20dp",
                "skin": "sknLbxborderd7d9e03pxradius",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxInlineError23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInlineError23",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "68dp",
                "zIndex": 1
            }, {}, {});
            flxInlineError23.setDefaultUnit(kony.flex.DP);
            var lblIconError23 = new kony.ui.Label({
                "id": "lblIconError23",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchParamErrorMsg23 = new kony.ui.Label({
                "id": "lblSearchParamErrorMsg23",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectCode\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineError23.add(lblIconError23, lblSearchParamErrorMsg23);
            flxSearchField23.add(lblSearchParamHeader23, lstBoxSearchParam23, flxInlineError23);
            flxAddCustAdvSearchRow1.add(flxSearchField21, flxSearchField22, flxSearchField23);
            var flxAddCustAdvSearchRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddCustAdvSearchRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%"
            }, {}, {});
            flxAddCustAdvSearchRow2.setDefaultUnit(kony.flex.DP);
            var flxSearchField31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33.30%"
            }, {}, {});
            flxSearchField31.setDefaultUnit(kony.flex.DP);
            var textBoxEntry31 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "1dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField31.add(textBoxEntry31);
            var flxSearchField32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%"
            }, {}, {});
            flxSearchField32.setDefaultUnit(kony.flex.DP);
            var textBoxEntry32 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.TownorCity\")"
                    },
                    "tbxEnterValue": {
                        "placeholder": "Town/City"
                    },
                    "textBoxEntry": {
                        "left": "5dp",
                        "right": "1dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField32.add(textBoxEntry32);
            var flxSearchField33 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchField33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "34.60%"
            }, {}, {});
            flxSearchField33.setDefaultUnit(kony.flex.DP);
            var textBoxEntry33 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")"
                    },
                    "textBoxEntry": {
                        "left": "5dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchField33.add(textBoxEntry33);
            flxAddCustAdvSearchRow2.add(flxSearchField31, flxSearchField32, flxSearchField33);
            flxAddCustAdvanceSearchCont.add(flxAddCustAdvSearchRow1, flxAddCustAdvSearchRow2);
            var flxSearchToggleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSearchToggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxSearchToggleButtons.setDefaultUnit(kony.flex.DP);
            var flxShowHideButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxShowHideButton",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "50%"
            }, {}, {});
            flxShowHideButton.setDefaultUnit(kony.flex.DP);
            var btnShowHideAdvanceSearch = new kony.ui.Button({
                "height": "20dp",
                "id": "btnShowHideAdvanceSearch",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLato117eb013Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.AdvancedSearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSearchArrow = new kony.ui.Label({
                "id": "lblIconSearchArrow",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknfontIconDescRightArrow14px",
                "text": "",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxShowHideButton.add(btnShowHideAdvanceSearch, lblIconSearchArrow);
            var flxAddCustSearchButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddCustSearchButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "reverseLayoutDirection": false,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": 0,
                "width": "50%"
            }, {}, {});
            flxAddCustSearchButtons.setDefaultUnit(kony.flex.DP);
            var commonButonsSearchCust = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "30px",
                "id": "commonButonsSearchCust",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "btnCancel": {
                        "height": "30dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ClearAll_UC\")",
                        "left": "viz.val_cleared",
                        "right": "120dp"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "height": "30dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCH\")"
                    },
                    "commonButtons": {
                        "height": "30px",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxRightButtons": {
                        "centerY": "viz.val_cleared",
                        "height": "30dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddCustSearchButtons.add(commonButonsSearchCust);
            var flxAddCustSearchSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddCustSearchSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxD7D9E0Separator"
            }, {}, {});
            flxAddCustSearchSeperator.setDefaultUnit(kony.flex.DP);
            flxAddCustSearchSeperator.add();
            flxSearchToggleButtons.add(flxShowHideButton, flxAddCustSearchButtons, flxAddCustSearchSeperator);
            var flxNoContractAvailableFlag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxNoContractAvailableFlag",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxNoContractAvailableFlag.setDefaultUnit(kony.flex.DP);
            var contractInfoFlagMessage = new com.adminConsole.common.errorFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractInfoFlagMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "overrides": {
                    "errorFlagMessage": {
                        "left": "20dp",
                        "right": "20dp",
                        "top": "15dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoContractAvailableFlag.add(contractInfoFlagMessage);
            var flxSearchResultsForAddCust = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultsForAddCust",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxSearchResultsForAddCust.setDefaultUnit(kony.flex.DP);
            var flxSearchResultHeaderAddCust = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultHeaderAddCust",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxSearchResultHeaderAddCust.setDefaultUnit(kony.flex.DP);
            var lblSelectedSearchCriteria = new kony.ui.Label({
                "id": "lblSelectedSearchCriteria",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractExistsFor\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedSearchSubCriteria = new kony.ui.Label({
                "id": "lblSelectedSearchSubCriteria",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractExistsFor\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultHeaderAddCust.add(lblSelectedSearchCriteria, lblSelectedSearchSubCriteria);
            var flxSearchResultCustSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultCustSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "46dp"
            }, {}, {});
            flxSearchResultCustSegment.setDefaultUnit(kony.flex.DP);
            var flxSearchResultsSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSearchResultsSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxSearchResultsSegHeader.setDefaultUnit(kony.flex.DP);
            var flxResultsCustIdHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxResultsCustIdHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "17%"
            }, {}, {});
            flxResultsCustIdHeader.setDefaultUnit(kony.flex.DP);
            var lblSearchSegHeaderCustId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchSegHeaderCustId",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl000000LatoReg11px"
            });
            var lblIconCustIdSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCustIdSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResultsCustIdHeader.add(lblSearchSegHeaderCustId, lblIconCustIdSort);
            var flxResultsCustNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxResultsCustNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "28%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "17%"
            }, {}, {});
            flxResultsCustNameHeader.setDefaultUnit(kony.flex.DP);
            var lblSearchSegHeaderCustName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchSegHeaderCustName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomerName_UC\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl000000LatoReg11px"
            });
            var lblIconCustNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCustNameSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResultsCustNameHeader.add(lblSearchSegHeaderCustName, lblIconCustNameSort);
            var flxResultsCustEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxResultsCustEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "9%"
            }, {}, {});
            flxResultsCustEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblSearchSegHeaderEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchSegHeaderEmail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl000000LatoReg11px"
            });
            var lblIconCustEmailSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCustEmailSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResultsCustEmailHeader.add(lblSearchSegHeaderEmail, lblIconCustEmailSort);
            var lblSearchSegHeaderPhoneNum = new kony.ui.Label({
                "id": "lblSearchSegHeaderPhoneNum",
                "isVisible": true,
                "left": "82%",
                "skin": "sknlblLato696c7311px",
                "text": "PHONE NUMBER",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblSeparator696C73",
                "text": ".",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultsSegHeader.add(flxResultsCustIdHeader, flxResultsCustNameHeader, flxResultsCustEmailHeader, lblSearchSegHeaderPhoneNum, lblSeperator);
            var segSearchResultsCust = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblSearchSegHeaderCustId": "Label",
                    "lblSearchSegHeaderCustName": "Label",
                    "lblSearchSegHeaderEmail": "Label",
                    "lblSearchSegHeaderPhoneNum": "Label",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segSearchResultsCust",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxEnrollCustomerSearchResult",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "50dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxEnrollCustomerSearchResult": "flxEnrollCustomerSearchResult",
                    "flxSearchResultCont": "flxSearchResultCont",
                    "lblSearchSegHeaderCustId": "lblSearchSegHeaderCustId",
                    "lblSearchSegHeaderCustName": "lblSearchSegHeaderCustName",
                    "lblSearchSegHeaderEmail": "lblSearchSegHeaderEmail",
                    "lblSearchSegHeaderPhoneNum": "lblSearchSegHeaderPhoneNum",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultCustSegment.add(flxSearchResultsSegHeader, segSearchResultsCust);
            flxSearchResultsForAddCust.add(flxSearchResultHeaderAddCust, flxSearchResultCustSegment);
            var flxSearchCustomerContractList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchCustomerContractList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxSearchCustomerContractList.setDefaultUnit(kony.flex.DP);
            var flxBackOptionAddCust = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBackOptionAddCust",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "65dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxBackOptionAddCust.setDefaultUnit(kony.flex.DP);
            var lblIconBack = new kony.ui.Label({
                "height": "20dp",
                "id": "lblIconBack",
                "isVisible": true,
                "left": "0px",
                "skin": "sknIcon117EB018px",
                "text": "",
                "top": "0px",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBackText = new kony.ui.Label({
                "height": "20dp",
                "id": "lblBackText",
                "isVisible": true,
                "left": "7px",
                "skin": "sknLblLato13px117eb0",
                "text": "Back",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBackOptionAddCust.add(lblIconBack, lblBackText);
            var flxSearchResultHeaderContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultHeaderContracts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSearchResultHeaderContracts.setDefaultUnit(kony.flex.DP);
            var flxContractListHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractListHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%"
            }, {}, {});
            flxContractListHeader.setDefaultUnit(kony.flex.DP);
            var lblContractListHeader = new kony.ui.Label({
                "id": "lblContractListHeader",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractExistsFor\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddContractInfoHeading = new kony.ui.Label({
                "id": "lblAddContractInfoHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractExistsFor\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContractListHeader.add(lblContractListHeader, lblAddContractInfoHeading);
            var btnAddContract = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddContract",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AddNewContract\")",
                "top": "0dp",
                "width": "142dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxSearchResultHeaderContracts.add(flxContractListHeader, btnAddContract);
            var flxCustomerContractsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxCustomerContractsList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxCustomerContractsList.setDefaultUnit(kony.flex.DP);
            flxCustomerContractsList.add();
            var addAdditionalCustContractList = new com.adminConsole.enrollCustomer.contractsList({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "id": "addAdditionalCustContractList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "96%",
                "overrides": {
                    "contractsList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "isVisible": false,
                        "left": "20dp",
                        "right": "viz.val_cleared",
                        "top": "15dp",
                        "width": "96%"
                    },
                    "flxCheckbox": {
                        "isVisible": false
                    },
                    "flxCustomersListHeader": {
                        "isVisible": false
                    },
                    "imgCheckBoxHeader": {
                        "src": "checkboxnormal.png"
                    },
                    "imgCheckbox": {
                        "src": "checkboxnormal.png"
                    },
                    "segRelatedContractsList": {
                        "data": [{
                            "imgCheckbox": "checkboxnormal.png",
                            "imgSectionCheckbox": "checkboxnormal.png",
                            "lblContractAddress": "Label",
                            "lblContractId": "Label",
                            "lblContractName": "Label",
                            "lblSeperator": "Label"
                        }],
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchCustomerContractList.add(flxBackOptionAddCust, flxSearchResultHeaderContracts, flxCustomerContractsList, addAdditionalCustContractList);
            var flxNoResultsContainer = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxNoResultsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxNoResultsContainer.setDefaultUnit(kony.flex.DP);
            var noResultsSearchCustomers = new com.adminConsole.common.noResultsWithButton({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "100dp",
                "id": "noResultsSearchCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "overrides": {
                    "btnAddRecord": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AddNewContract\")",
                        "width": "142dp"
                    },
                    "noResultsWithButton": {
                        "centerY": "50%",
                        "height": "100dp",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoResultsContainer.add(noResultsSearchCustomers);
            var flxAddAnotherCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddAnotherCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxAddAnotherCustButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsAddAnotherCust = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsAddAnotherCust",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonAdd\")",
                        "width": "88px"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAnotherCustSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAnotherCustSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddAnotherCustSeperator.setDefaultUnit(kony.flex.DP);
            flxAddAnotherCustSeperator.add();
            flxAddAnotherCustButtons.add(commonButtonsAddAnotherCust, flxAddAnotherCustSeperator);
            flxAddAnotherCustScreen.add(flxAddAnotherCustHeader, flxAddCustBasicSearchRow, flxAddCustAdvanceSearchCont, flxSearchToggleButtons, flxNoContractAvailableFlag, flxSearchResultsForAddCust, flxSearchCustomerContractList, flxNoResultsContainer, flxAddAnotherCustButtons);
            flxAddCustomerBox.add(flxAddAnotherCustScreen);
            var flxEnrollRelatedCustomersBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxEnrollRelatedCustomersBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0"
            }, {}, {});
            flxEnrollRelatedCustomersBox.setDefaultUnit(kony.flex.DP);
            var flxRelatedCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxRelatedCustomerContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRelatedCustomerContainer.setDefaultUnit(kony.flex.DP);
            var flxRelatedCustomersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxRelatedCustomersHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRelatedCustomersHeader.setDefaultUnit(kony.flex.DP);
            var lblRelatedCustomerHeader = new kony.ui.Label({
                "id": "lblRelatedCustomerHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AddAnotherCustomerId\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddOtherCustomers = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddOtherCustomers",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchOtherCustomers\")",
                "top": "20dp",
                "width": "167dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxRelatedCustomersHeader.add(lblRelatedCustomerHeader, btnAddOtherCustomers);
            var lblRelatedcustSubHeading = new kony.ui.Label({
                "id": "lblRelatedcustSubHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomersRelatedTo\")",
                "top": "65dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRelatedCustomerSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRelatedCustomerSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 97,
                "width": "100%"
            }, {}, {});
            flxRelatedCustomerSegment.setDefaultUnit(kony.flex.DP);
            var segRelatedCustomers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "btnRelation": "",
                    "imgCheckbox": "",
                    "imgRadioButton": "",
                    "lblContractId": "",
                    "lblContractInfo": "",
                    "lblContractName": "",
                    "lblContractRelation": "",
                    "lblData1": "",
                    "lblData2": "",
                    "lblData3": "",
                    "lblHeading1": "",
                    "lblHeading2": "",
                    "lblHeading3": "",
                    "lblIconRemove": "",
                    "lblLine": "",
                    "lblPrimaryText": ""
                }],
                "groupCells": false,
                "id": "segRelatedCustomers",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxRelatedCustomerList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckbox",
                    "selectedStateImage": "radio_selected.png",
                    "unselectedStateImage": "radio_notselected.png"
                },
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnRelation": "btnRelation",
                    "flxCheckbox": "flxCheckbox",
                    "flxColumn1": "flxColumn1",
                    "flxColumn2": "flxColumn2",
                    "flxColumn3": "flxColumn3",
                    "flxContractDetails": "flxContractDetails",
                    "flxContractName": "flxContractName",
                    "flxContractRelation": "flxContractRelation",
                    "flxDetails": "flxDetails",
                    "flxLeftDetailsCont": "flxLeftDetailsCont",
                    "flxPrimaryBtn": "flxPrimaryBtn",
                    "flxRelatedCustomerList": "flxRelatedCustomerList",
                    "flxRelatedCustomerRow": "flxRelatedCustomerRow",
                    "flxRemoveContract": "flxRemoveContract",
                    "flxRightActionCont": "flxRightActionCont",
                    "flxRightDetailsCont": "flxRightDetailsCont",
                    "imgCheckbox": "imgCheckbox",
                    "imgRadioButton": "imgRadioButton",
                    "lblContractId": "lblContractId",
                    "lblContractInfo": "lblContractInfo",
                    "lblContractName": "lblContractName",
                    "lblContractRelation": "lblContractRelation",
                    "lblData1": "lblData1",
                    "lblData2": "lblData2",
                    "lblData3": "lblData3",
                    "lblHeading1": "lblHeading1",
                    "lblHeading2": "lblHeading2",
                    "lblHeading3": "lblHeading3",
                    "lblIconRemove": "lblIconRemove",
                    "lblLine": "lblLine",
                    "lblPrimaryText": "lblPrimaryText"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var noResultsRelatedCustomers = new com.adminConsole.common.noResultsWithButton({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "40%",
                "height": "100dp",
                "id": "noResultsRelatedCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "overrides": {
                    "btnAddRecord": {
                        "isVisible": false,
                        "width": "128dp"
                    },
                    "lblNoRecordsAvailable": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.rtxNoRecordsAvailable\")"
                    },
                    "noResultsWithButton": {
                        "centerY": "40%",
                        "height": "100dp",
                        "isVisible": false,
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRelatedCustomerSegment.add(segRelatedCustomers, noResultsRelatedCustomers);
            flxRelatedCustomerContainer.add(flxRelatedCustomersHeader, lblRelatedcustSubHeading, flxRelatedCustomerSegment);
            var flxRelatedCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxRelatedCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxRelatedCustButtons.setDefaultUnit(kony.flex.DP);
            var flxRelatedCustButtonSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxRelatedCustButtonSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRelatedCustButtonSeperator.setDefaultUnit(kony.flex.DP);
            flxRelatedCustButtonSeperator.add();
            var commonButtonsRelatedCust = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsRelatedCust",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnSelect\")"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRelatedCustButtons.add(flxRelatedCustButtonSeperator, commonButtonsRelatedCust);
            flxEnrollRelatedCustomersBox.add(flxRelatedCustomerContainer, flxRelatedCustButtons);
            var flxSelectedRelatedCustDetailsBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectedRelatedCustDetailsBox",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0"
            }, {}, {});
            flxSelectedRelatedCustDetailsBox.setDefaultUnit(kony.flex.DP);
            var flxRelatedCustDetailsCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRelatedCustDetailsCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxRelatedCustDetailsCont.setDefaultUnit(kony.flex.DP);
            var flxRelatedCustDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRelatedCustDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRelatedCustDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblRelatedCustDetailsTitle = new kony.ui.Label({
                "id": "lblRelatedCustDetailsTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Label",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRelatedDetailsShowSearch = new kony.ui.Button({
                "height": "22px",
                "id": "btnRelatedDetailsShowSearch",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchOtherCustomers\")",
                "top": "20dp",
                "width": "160dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxRelatedCustDetailsHeader.add(lblRelatedCustDetailsTitle, btnRelatedDetailsShowSearch);
            var flxRelatedCustDetailsRows = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRelatedCustDetailsRows",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxRelatedCustDetailsRows.setDefaultUnit(kony.flex.DP);
            var detailsRelatedCust1 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "detailsRelatedCust1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "0dp"
                    },
                    "flxColumn1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0px",
                        "width": "33%"
                    },
                    "flxColumn2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "35%",
                        "width": "33%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "70%",
                        "right": "0dp"
                    },
                    "flxDataAndImage1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "lblData1": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData2": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomerName_UC\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EMAIL\")"
                    },
                    "lblIconData1": {
                        "centerY": "viz.val_cleared"
                    },
                    "lblIconData2": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var detailsRelatedCust2 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "detailsRelatedCust2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "top": "30dp"
                    },
                    "flxColumn1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "width": "33%"
                    },
                    "flxColumn2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "35%",
                        "width": "33%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "70%",
                        "right": "0dp"
                    },
                    "flxDataAndImage1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "lblData1": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData2": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblPhoneNumber\")"
                    },
                    "lblIconData1": {
                        "centerY": "viz.val_cleared"
                    },
                    "lblIconData2": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var detailsRelatedCust3 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "detailsRelatedCust3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "top": "30dp"
                    },
                    "flxColumn1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "width": "33%"
                    },
                    "flxColumn2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "35%",
                        "width": "33%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "70%",
                        "right": "0dp"
                    },
                    "flxDataAndImage1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage2": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "top": "23dp"
                    },
                    "lblData1": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData2": {
                        "centerY": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "98%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")"
                    },
                    "lblIconData1": {
                        "centerY": "viz.val_cleared"
                    },
                    "lblIconData2": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRelatedCustDetailsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxRelatedCustDetailsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknFlxD7D9E0Separator",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRelatedCustDetailsSeperator.setDefaultUnit(kony.flex.DP);
            flxRelatedCustDetailsSeperator.add();
            flxRelatedCustDetailsRows.add(detailsRelatedCust1, detailsRelatedCust2, detailsRelatedCust3, flxRelatedCustDetailsSeperator);
            var flxRelatedCustContractCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRelatedCustContractCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRelatedCustContractCont.setDefaultUnit(kony.flex.DP);
            var lblRelatedCustContractHeader = new kony.ui.Label({
                "id": "lblRelatedCustContractHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ContractExistsFor\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRelatedCustContractsList = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxRelatedCustContractsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "56dp"
            }, {}, {});
            flxRelatedCustContractsList.setDefaultUnit(kony.flex.DP);
            flxRelatedCustContractsList.add();
            var relatedCustContractCard = new com.adminConsole.enrollCustomer.contractsList({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "id": "relatedCustContractCard",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "56dp",
                "overrides": {
                    "contractsList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "left": "0dp",
                        "right": "0dp",
                        "top": "56dp",
                        "width": "viz.val_cleared"
                    },
                    "flxCustomersListHeader": {
                        "isVisible": false
                    },
                    "imgCheckBoxHeader": {
                        "src": "checkboxnormal.png"
                    },
                    "imgCheckbox": {
                        "src": "checkboxnormal.png"
                    },
                    "segRelatedContractsList": {
                        "data": [{
                            "imgCheckbox": "checkboxnormal.png",
                            "imgSectionCheckbox": "checkboxnormal.png",
                            "lblContractAddress": "Label",
                            "lblContractId": "Label",
                            "lblContractName": "Label",
                            "lblSeperator": "Label"
                        }],
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnRelatedCustAddContract = new kony.ui.Button({
                "height": "22px",
                "id": "btnRelatedCustAddContract",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add Category",
                "top": "20dp",
                "width": "128dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxRelatedCustContractCont.add(lblRelatedCustContractHeader, flxRelatedCustContractsList, relatedCustContractCard, btnRelatedCustAddContract);
            var flxRelatedCustDetailsNoContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "150dp",
                "id": "flxRelatedCustDetailsNoContracts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRelatedCustDetailsNoContracts.setDefaultUnit(kony.flex.DP);
            var noResultsRelatedCustDetails = new com.adminConsole.common.noResultsWithButton({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "100dp",
                "id": "noResultsRelatedCustDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "overrides": {
                    "btnAddRecord": {
                        "isVisible": false,
                        "width": "128dp"
                    },
                    "lblNoRecordsAvailable": {
                        "text": "No contracts available"
                    },
                    "noResultsWithButton": {
                        "centerY": "50%",
                        "height": "100dp",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRelatedCustDetailsNoContracts.add(noResultsRelatedCustDetails);
            var flxSelectedRelatedCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxSelectedRelatedCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxSelectedRelatedCustButtons.setDefaultUnit(kony.flex.DP);
            var flxRelatedDetailsBtnSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxRelatedDetailsBtnSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRelatedDetailsBtnSeperator.setDefaultUnit(kony.flex.DP);
            flxRelatedDetailsBtnSeperator.add();
            var commonButtonsRelatedDetails = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsRelatedDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.policies.ADD\")",
                        "width": "88px"
                    },
                    "commonButtons": {
                        "left": "0dp",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectedRelatedCustButtons.add(flxRelatedDetailsBtnSeperator, commonButtonsRelatedDetails);
            flxRelatedCustDetailsCont.add(flxRelatedCustDetailsHeader, flxRelatedCustDetailsRows, flxRelatedCustContractCont, flxRelatedCustDetailsNoContracts, flxSelectedRelatedCustButtons);
            flxSelectedRelatedCustDetailsBox.add(flxRelatedCustDetailsCont);
            flxAddAnotherCustContainer.add(flxAddCustomerBox, flxEnrollRelatedCustomersBox, flxSelectedRelatedCustDetailsBox);
            var flxEditUserContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxEditUserContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp"
            }, {}, {});
            flxEditUserContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEditVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "230dp"
            }, {}, {});
            flxEnrollEditVerticalTabs.setDefaultUnit(kony.flex.DP);
            var enrollVerticalTabs = new com.adminConsole.common.verticalTabs1({
                "height": "100%",
                "id": "enrollVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SelectAccounts_UC\")"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.AssignFeaturesAndActions_UC\")",
                        "right": "45dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileFeaturesActions.OtherFeaturesAndActions_UC\")",
                        "right": "45dp"
                    },
                    "btnOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCreate.AssignLimits_UC\")"
                    },
                    "flxOption4": {
                        "isVisible": true
                    },
                    "lblOptional0": {
                        "isVisible": false
                    },
                    "lblOptional1": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxVericalTabSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxVericalTabSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxBgE4E6EC",
                "top": 0,
                "width": "1dp",
                "zIndex": 2
            }, {}, {});
            flxVericalTabSeperator.setDefaultUnit(kony.flex.DP);
            flxVericalTabSeperator.add();
            flxEnrollEditVerticalTabs.add(enrollVerticalTabs, flxVericalTabSeperator);
            var flxEditCustRightContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEditCustRightContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxEditCustRightContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditAccountsContainer = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEditAccountsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditAccountsContainer.setDefaultUnit(kony.flex.DP);
            var lblAccountsCustHeader = new kony.ui.Label({
                "id": "lblAccountsCustHeader",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Label",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEnrollEditSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxEnrollEditSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnrollEditSearch.setDefaultUnit(kony.flex.DP);
            var customersDropdownAccounts = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersDropdownAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "230dp",
                "zIndex": 1,
                "overrides": {
                    "btnPrimary": {
                        "isVisible": true
                    },
                    "customersDropdown": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "20dp",
                        "top": "20dp",
                        "width": "230dp",
                        "zIndex": 1
                    },
                    "flxSegmentList": {
                        "isVisible": false,
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var searchEditAccounts = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "searchEditAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "overrides": {
                    "flxSearchCancel": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "searchBox": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "70dp",
                        "left": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "top": "0dp",
                        "width": "50%"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByAccountNumberName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditSearch.add(customersDropdownAccounts, searchEditAccounts);
            var flxEnrollEditAccountsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditAccountsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "100dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditAccountsList.setDefaultUnit(kony.flex.DP);
            var enrollEditAccountsCard = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "enrollEditAccountsCard",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxCardBottomContainer": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true
                    },
                    "flxCheckboxCont": {
                        "isVisible": true
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Accounts\")"
                    },
                    "lblTotalCount": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAccountsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "slFbox",
                "top": "200px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxAccountsFilter.setDefaultUnit(kony.flex.DP);
            var accountTypesFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "accountTypesFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountsFilter.add(accountTypesFilterMenu);
            var flxOwnershipFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOwnershipFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "200px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxOwnershipFilter.setDefaultUnit(kony.flex.DP);
            var ownershipFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ownershipFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnershipFilter.add(ownershipFilterMenu);
            flxEnrollEditAccountsList.add(enrollEditAccountsCard, flxAccountsFilter, flxOwnershipFilter);
            var flxEnrollEditAccountsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollEditAccountsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollEditAccountsButtons.setDefaultUnit(kony.flex.DP);
            var flxEditAccButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAccButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditAccButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxEditAccButtonsSeperator.add();
            var commonButtonsEditAccounts = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsEditAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SaveChanges_UC\")",
                        "width": "156dp"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "275dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditAccountsButtons.add(flxEditAccButtonsSeperator, commonButtonsEditAccounts);
            flxEnrollEditAccountsContainer.add(lblAccountsCustHeader, flxEnrollEditSearch, flxEnrollEditAccountsList, flxEnrollEditAccountsButtons);
            var flxEnrollEdiSignatoryContainer = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEdiSignatoryContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEdiSignatoryContainer.setDefaultUnit(kony.flex.DP);
            var lblSignatoryCustHeader = new kony.ui.Label({
                "id": "lblSignatoryCustHeader",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Label",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEnrollEditSearchSignatory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxEnrollEditSearchSignatory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnrollEditSearchSignatory.setDefaultUnit(kony.flex.DP);
            var customersDropdownSignatory = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersDropdownSignatory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "230dp",
                "zIndex": 1,
                "overrides": {
                    "btnPrimary": {
                        "isVisible": true
                    },
                    "customersDropdown": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "20dp",
                        "top": "20dp",
                        "width": "230dp",
                        "zIndex": 1
                    },
                    "flxSegmentList": {
                        "isVisible": false,
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var searchEditSignatory = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "searchEditSignatory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "overrides": {
                    "flxSearchCancel": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "searchBox": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "70dp",
                        "left": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "top": "0dp",
                        "width": "50%"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByAccountNumberName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditSearchSignatory.add(customersDropdownSignatory, searchEditSignatory);
            var flxEnrollEditSignatoryList = new kony.ui.FlexContainer({
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxEnrollEditSignatoryList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditSignatoryList.setDefaultUnit(kony.flex.DP);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var lblNoResults = new kony.ui.Label({
                "centerX": "47%",
                "id": "lblNoResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(lblNoResults);
            var flxResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var flxRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRoleError.setDefaultUnit(kony.flex.DP);
            var flxRoleErrorContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleErrorContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRoleErrorContainer.setDefaultUnit(kony.flex.DP);
            var flxRoleErrorIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleErrorIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxRoleErrorIconContainer.setDefaultUnit(kony.flex.DP);
            var lblRoleErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblRoleErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleErrorIconContainer.add(lblRoleErrorIcon);
            var lblErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateCustomer.select_a_role\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleErrorContainer.add(flxRoleErrorIconContainer, lblErrorValue);
            flxRoleError.add(flxRoleErrorContainer);
            var flxScrollCustomerRoles = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollCustomerRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollCustomerRoles.setDefaultUnit(kony.flex.DP);
            var segCustomerRolesEdit = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "imgRoleRadio": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "id": "segCustomerRolesEdit",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgRoleCheckbox",
                    "selectedStateImage": "radio_selected.png",
                    "unselectedStateImage": "radio_notselected.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "flxRoleRadio": "flxRoleRadio",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "imgRoleRadio": "imgRoleRadio",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flx20px = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flx20px",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flx20px.setDefaultUnit(kony.flex.DP);
            flx20px.add();
            flxScrollCustomerRoles.add(segCustomerRolesEdit, flx20px);
            flxResults.add(flxRoleError, flxScrollCustomerRoles);
            flxEnrollEditSignatoryList.add(flxNoResultsFound, flxResults);
            var flxEnrollEditSignatoryButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollEditSignatoryButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollEditSignatoryButtons.setDefaultUnit(kony.flex.DP);
            var flxEditSignatoryButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditSignatoryButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditSignatoryButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxEditSignatoryButtonsSeperator.add();
            var commonButtonsEditSignatories = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsEditSignatories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SaveChanges_UC\")",
                        "width": "156dp"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "275dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditSignatoryButtons.add(flxEditSignatoryButtonsSeperator, commonButtonsEditSignatories);
            flxEnrollEdiSignatoryContainer.add(lblSignatoryCustHeader, flxEnrollEditSearchSignatory, flxEnrollEditSignatoryList, flxEnrollEditSignatoryButtons);
            var flxEnrollEditFeaturesContainer = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEditFeaturesContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditFeaturesContainer.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditFeatureTopCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxEnrollEditFeatureTopCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditFeatureTopCont.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditFeaturesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxEnrollEditFeaturesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditFeaturesHeader.setDefaultUnit(kony.flex.DP);
            var lblFeaturesCustHeader = new kony.ui.Label({
                "id": "lblFeaturesCustHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.UserColon\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBulkUpdateFeatures = new kony.ui.Button({
                "height": "22px",
                "id": "btnBulkUpdateFeatures",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.UpdatePermissionsInBulk\")",
                "top": "20dp",
                "width": "173dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxEnrollEditFeaturesHeader.add(lblFeaturesCustHeader, btnBulkUpdateFeatures);
            var flxFeaturesToggleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxFeaturesToggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxFeaturesToggleButtons.setDefaultUnit(kony.flex.DP);
            var toggleButtonsFeatures = new com.adminConsole.common.toggleButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "toggleButtonsFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%",
                "overrides": {
                    "btnToggleLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ByCustomer\")"
                    },
                    "btnToggleRight": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ByAccounts\")"
                    },
                    "toggleButtons": {
                        "left": "20dp",
                        "width": "80%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxFeaturesToggleSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxFeaturesToggleSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxD7D9E0Separator"
            }, {}, {});
            flxFeaturesToggleSeperator.setDefaultUnit(kony.flex.DP);
            flxFeaturesToggleSeperator.add();
            flxFeaturesToggleButtons.add(toggleButtonsFeatures, flxFeaturesToggleSeperator);
            var flxEnrollEditFeaturesSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "65dp",
                "id": "flxEnrollEditFeaturesSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnrollEditFeaturesSearch.setDefaultUnit(kony.flex.DP);
            var customersDropdownFeatures = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersDropdownFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "230dp",
                "overrides": {
                    "customersDropdown": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "20dp",
                        "top": "20dp",
                        "width": "230dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEnrollEditFeaturesSearchFilterCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnrollEditFeaturesSearchFilterCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "53%"
            }, {}, {});
            flxEnrollEditFeaturesSearchFilterCont.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditFeatureFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxEnrollEditFeatureFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "20dp"
            }, {}, {});
            flxEnrollEditFeatureFilter.setDefaultUnit(kony.flex.DP);
            var lblIconFilterFeatures = new kony.ui.Label({
                "id": "lblIconFilterFeatures",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon696C73sz16px",
                "text": "",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollEditFeatureFilter.add(lblIconFilterFeatures);
            var searchEditFeatures = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "searchEditFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%",
                "overrides": {
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "searchBox": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "70dp",
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "10dp",
                        "top": "0dp",
                        "width": "85%"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByFeatureActionName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditFeaturesSearchFilterCont.add(flxEnrollEditFeatureFilter, searchEditFeatures);
            flxEnrollEditFeaturesSearch.add(customersDropdownFeatures, flxEnrollEditFeaturesSearchFilterCont);
            var flxEnrollEditFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "300dp",
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditFeaturesList.setDefaultUnit(kony.flex.DP);
            var enrollEditFeaturesCard = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "enrollEditFeaturesCard",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "10dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "left": "20dp",
                        "right": "20dp",
                        "top": "10dp",
                        "width": "viz.val_cleared"
                    },
                    "lblHeading": {
                        "text": "Selected Features:"
                    },
                    "lblTotalCount": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditFeaturesList.add(enrollEditFeaturesCard);
            var flxEnrollEditAccFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "300dp",
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditAccFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditAccFeaturesList.setDefaultUnit(kony.flex.DP);
            flxEnrollEditAccFeaturesList.add();
            var flxEnrollEditNoResultAccFeatures = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "150dp",
                "id": "flxEnrollEditNoResultAccFeatures",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditNoResultAccFeatures.setDefaultUnit(kony.flex.DP);
            var lblEnrollFeaturesNoFilterResults = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblEnrollFeaturesNoFilterResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollEditNoResultAccFeatures.add(lblEnrollFeaturesNoFilterResults);
            flxEnrollEditFeatureTopCont.add(flxEnrollEditFeaturesHeader, flxFeaturesToggleButtons, flxEnrollEditFeaturesSearch, flxEnrollEditFeaturesList, flxEnrollEditAccFeaturesList, flxEnrollEditNoResultAccFeatures);
            var flxEnrollEditFeatureButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollEditFeatureButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollEditFeatureButtons.setDefaultUnit(kony.flex.DP);
            var flxFeaturesBtnSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxFeaturesBtnSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxFeaturesBtnSeperator.setDefaultUnit(kony.flex.DP);
            flxFeaturesBtnSeperator.add();
            var commonButtonsEditFeatures = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsEditFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SaveChanges_UC\")",
                        "width": "156px"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "275px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditFeatureButtons.add(flxFeaturesBtnSeperator, commonButtonsEditFeatures);
            flxEnrollEditFeaturesContainer.add(flxEnrollEditFeatureTopCont, flxEnrollEditFeatureButtons);
            var flxEnrollEditOtherFeaturesCont = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEditOtherFeaturesCont",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditOtherFeaturesCont.setDefaultUnit(kony.flex.DP);
            var lblOtherFeaturesCustHeader = new kony.ui.Label({
                "id": "lblOtherFeaturesCustHeader",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.UserColon\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEnrollEditOFSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxEnrollEditOFSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnrollEditOFSearch.setDefaultUnit(kony.flex.DP);
            var customersDropdownOF = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersDropdownOF",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "230dp",
                "overrides": {
                    "customersDropdown": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "20dp",
                        "top": "20dp",
                        "width": "230dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var searchEditOtherFeatures = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "searchEditOtherFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "overrides": {
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "searchBox": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "70dp",
                        "left": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "top": "0dp",
                        "width": "50%"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByFeatureActionName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditOFSearch.add(customersDropdownOF, searchEditOtherFeatures);
            var flxEnrollEditOtherFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditOtherFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "100dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditOtherFeaturesList.setDefaultUnit(kony.flex.DP);
            var enrollEditOtherFeaturesCard = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "enrollEditOtherFeaturesCard",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxCardBottomContainer": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true
                    },
                    "lblHeading": {
                        "text": "Selected Features:"
                    },
                    "lblTotalCount": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditOtherFeaturesList.add(enrollEditOtherFeaturesCard);
            var flxEnrollEditOtherFeaturesButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollEditOtherFeaturesButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollEditOtherFeaturesButtons.setDefaultUnit(kony.flex.DP);
            var flxEditOFButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditOFButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditOFButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxEditOFButtonsSeperator.add();
            var commonButtonsEditOF = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsEditOF",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SaveChanges_UC\")",
                        "width": "156dp"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "275dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditOtherFeaturesButtons.add(flxEditOFButtonsSeperator, commonButtonsEditOF);
            flxEnrollEditOtherFeaturesCont.add(lblOtherFeaturesCustHeader, flxEnrollEditOFSearch, flxEnrollEditOtherFeaturesList, flxEnrollEditOtherFeaturesButtons);
            var flxEnrollEditLimitsContainer = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEnrollEditLimitsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxLimitErrorEditUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitErrorEditUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxLimitErrorEditUser.setDefaultUnit(kony.flex.DP);
            var limitsEnrollErrorFlag = new com.adminConsole.common.errorFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "limitsEnrollErrorFlag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblErrorValue": {
                        "text": "Please correct the error messages across all the customers to proceed further."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLimitErrorEditUser.add(limitsEnrollErrorFlag);
            var flxEnrollEditLimitsTopCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxEnrollEditLimitsTopCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsTopCont.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditLimitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxEnrollEditLimitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsHeader.setDefaultUnit(kony.flex.DP);
            var lblLimitsCustHeader = new kony.ui.Label({
                "id": "lblLimitsCustHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.UserColon\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBulkUpdateLimits = new kony.ui.Button({
                "height": "22px",
                "id": "btnBulkUpdateLimits",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.UpdateLimitsInBulk\")",
                "top": "20dp",
                "width": "142dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxEnrollEditLimitsHeader.add(lblLimitsCustHeader, btnBulkUpdateLimits);
            var flxLimitsToggleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxLimitsToggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxLimitsToggleButtons.setDefaultUnit(kony.flex.DP);
            var toggleButtonsLimits = new com.adminConsole.common.toggleButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "toggleButtonsLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%",
                "overrides": {
                    "btnToggleLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ByCustomer\")"
                    },
                    "btnToggleRight": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ByAccounts\")"
                    },
                    "toggleButtons": {
                        "left": "20dp",
                        "width": "80%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxLimitsToggleSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLimitsToggleSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxD7D9E0Separator"
            }, {}, {});
            flxLimitsToggleSeperator.setDefaultUnit(kony.flex.DP);
            flxLimitsToggleSeperator.add();
            flxLimitsToggleButtons.add(toggleButtonsLimits, flxLimitsToggleSeperator);
            var flxEnrollEditLimitSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "65dp",
                "id": "flxEnrollEditLimitSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnrollEditLimitSearch.setDefaultUnit(kony.flex.DP);
            var customersDropdownLimits = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersDropdownLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "230dp",
                "overrides": {
                    "customersDropdown": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerY": "viz.val_cleared",
                        "left": "20dp",
                        "top": "20dp",
                        "width": "230dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEnrollEditLimitsSearchFilterCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnrollEditLimitsSearchFilterCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "53%"
            }, {}, {});
            flxEnrollEditLimitsSearchFilterCont.setDefaultUnit(kony.flex.DP);
            var flxEnrollEditLimitsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxEnrollEditLimitsFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "20dp"
            }, {}, {});
            flxEnrollEditLimitsFilter.setDefaultUnit(kony.flex.DP);
            var lblIconFilterLimits = new kony.ui.Label({
                "id": "lblIconFilterLimits",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon696C73sz16px",
                "text": "",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollEditLimitsFilter.add(lblIconFilterLimits);
            var searchEditLimits = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "searchEditLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%",
                "overrides": {
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "searchBox": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "70dp",
                        "left": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "10dp",
                        "top": "0dp",
                        "width": "85%"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByFeatureActionName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditLimitsSearchFilterCont.add(flxEnrollEditLimitsFilter, searchEditLimits);
            flxEnrollEditLimitSearch.add(customersDropdownLimits, flxEnrollEditLimitsSearchFilterCont);
            var flxEnrollEditLimitsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200dp",
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditLimitsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsList.setDefaultUnit(kony.flex.DP);
            var enrollEditLimitsCard = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "enrollEditLimitsCard",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "btnReset": {
                        "isVisible": true
                    },
                    "lblCount": {
                        "isVisible": false
                    },
                    "lblHeading": {
                        "text": "Limits"
                    },
                    "lblTotalCount": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditLimitsList.add(enrollEditLimitsCard);
            var flxEnrollEditAccLimitsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxEnrollEditAccLimitsList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEnrollEditAccLimitsList.setDefaultUnit(kony.flex.DP);
            flxEnrollEditAccLimitsList.add();
            var flxEnrollEditNoResultAccLimits = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "150dp",
                "id": "flxEnrollEditNoResultAccLimits",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditNoResultAccLimits.setDefaultUnit(kony.flex.DP);
            var lblEnrollLimitsNoFilterResults = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblEnrollLimitsNoFilterResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollEditNoResultAccLimits.add(lblEnrollLimitsNoFilterResults);
            flxEnrollEditLimitsTopCont.add(flxEnrollEditLimitsHeader, flxLimitsToggleButtons, flxEnrollEditLimitSearch, flxEnrollEditLimitsList, flxEnrollEditAccLimitsList, flxEnrollEditNoResultAccLimits);
            var flxEnrollEditLimitsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEnrollEditLimitsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsButtons.setDefaultUnit(kony.flex.DP);
            var flxEditLimitButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditLimitButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditLimitButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxEditLimitButtonsSeperator.add();
            var commonButtonsEditLimits = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsEditLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SaveChanges_UC\")",
                        "width": "156px"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "275px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollEditLimitsButtons.add(flxEditLimitButtonsSeperator, commonButtonsEditLimits);
            var flxRangeTooltip = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "320dp",
                "zIndex": 5
            }, {}, {});
            flxRangeTooltip.setDefaultUnit(kony.flex.DP);
            var flxRangeTooltipArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxRangeTooltipArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxRangeTooltipArrow.setDefaultUnit(kony.flex.DP);
            var imgRangeTooltipArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgRangeTooltipArrow",
                "isVisible": true,
                "left": "20dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeTooltipArrow.add(imgRangeTooltipArrow);
            var flxRangeTooltipBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltipBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFbrD7D9E01pxRd3pxShd0D0D11",
                "top": "11dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRangeTooltipBody.setDefaultUnit(kony.flex.DP);
            var flxRangeTooltipHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltipHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRangeTooltipHeader.setDefaultUnit(kony.flex.DP);
            var lblHeading1 = new kony.ui.Label({
                "id": "lblHeading1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7311px",
                "text": "APPROVAL TYPE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeading2 = new kony.ui.Label({
                "id": "lblHeading2",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblLato696c7311px",
                "text": "APPROVAL TYPE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeading3 = new kony.ui.Label({
                "id": "lblHeading3",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblLato696c7311px",
                "text": "APPROVAL TYPE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeTooltipHeader.add(lblHeading1, lblHeading2, lblHeading3);
            var flxRangeRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxRangeRow1.setDefaultUnit(kony.flex.DP);
            var lblRangeTitle1 = new kony.ui.Label({
                "id": "lblRangeTitle1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.PreApproved\")",
                "top": "0dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRangeValue11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValue11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValue11.setDefaultUnit(kony.flex.DP);
            var lblIconRangeCurr11 = new kony.ui.Label({
                "id": "lblIconRangeCurr11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue11 = new kony.ui.Label({
                "id": "lblRangeValue11",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValue11.add(lblIconRangeCurr11, lblRangeValue11);
            var flxRangeValue12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValue12",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValue12.setDefaultUnit(kony.flex.DP);
            var lblIconRangeCurr12 = new kony.ui.Label({
                "id": "lblIconRangeCurr12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue12 = new kony.ui.Label({
                "id": "lblRangeValue12",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValue12.add(lblIconRangeCurr12, lblRangeValue12);
            flxRangeRow1.add(lblRangeTitle1, flxRangeValue11, flxRangeValue12);
            var flxRangeRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxRangeRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxRangeRow2.setDefaultUnit(kony.flex.DP);
            var lblRangeTitle2 = new kony.ui.Label({
                "id": "lblRangeTitle2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.AutoDeny\")",
                "top": "0dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRangeValue21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValue21",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValue21.setDefaultUnit(kony.flex.DP);
            var lblIconRangeCurr21 = new kony.ui.Label({
                "id": "lblIconRangeCurr21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue21 = new kony.ui.Label({
                "id": "lblRangeValue21",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValue21.add(lblIconRangeCurr21, lblRangeValue21);
            var flxRangeValue22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValue22",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValue22.setDefaultUnit(kony.flex.DP);
            var lblIconRangeCurr22 = new kony.ui.Label({
                "id": "lblIconRangeCurr22",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue22 = new kony.ui.Label({
                "id": "lblRangeValue22",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValue22.add(lblIconRangeCurr22, lblRangeValue22);
            flxRangeRow2.add(lblRangeTitle2, flxRangeValue21, flxRangeValue22);
            flxRangeTooltipBody.add(flxRangeTooltipHeader, flxRangeRow1, flxRangeRow2);
            flxRangeTooltip.add(flxRangeTooltipArrow, flxRangeTooltipBody);
            var flxEnrollEditLimitsTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEnrollEditLimitsTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEnrollEditLimitsTemplate.setDefaultUnit(kony.flex.DP);
            var lblLimitsHeading = new kony.ui.Label({
                "id": "lblLimitsHeading",
                "isVisible": true,
                "left": 15,
                "skin": "sknlblLatoBold485c7512px",
                "text": "Label",
                "top": 15,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45dp",
                "width": "100%"
            }, {}, {});
            flxCustLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxCustLimitsRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustLimitsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustLimitsRow1.setDefaultUnit(kony.flex.DP);
            var lblRowHeading1 = new kony.ui.Label({
                "id": "lblRowHeading1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Per Transaction",
                "top": "15dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLimitValue1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitValue1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "right": "15dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxLimitValue1.setDefaultUnit(kony.flex.DP);
            var lblCurrencySymbol1 = new kony.ui.Label({
                "centerY": "52%",
                "id": "lblCurrencySymbol1",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknFontIcon485C7518px",
                "text": "",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxLimitValue1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
                "height": "40dp",
                "id": "tbxLimitValue1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "25dp",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxLimitValue1.add(lblCurrencySymbol1, tbxLimitValue1);
            var flxLimitError1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLimitError1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxLimitError1.setDefaultUnit(kony.flex.DP);
            var lblLimitErrorIcon1 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblLimitErrorIcon1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimitErrorMsg1 = new kony.ui.Label({
                "id": "lblLimitErrorMsg1",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitError1.add(lblLimitErrorIcon1, lblLimitErrorMsg1);
            var flxRangeCont1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeCont1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "23%"
            }, {}, {});
            flxRangeCont1.setDefaultUnit(kony.flex.DP);
            var lblRangeText1 = new kony.ui.Label({
                "id": "lblRangeText1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Range: ",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue1 = new kony.ui.Label({
                "id": "lblRangeValue1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "$2000 - $4000",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeCont1.add(lblRangeText1, lblRangeValue1);
            flxCustLimitsRow1.add(lblRowHeading1, flxLimitValue1, flxLimitError1, flxRangeCont1);
            var flxCustLimitsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustLimitsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustLimitsRow2.setDefaultUnit(kony.flex.DP);
            var lblRowHeading2 = new kony.ui.Label({
                "id": "lblRowHeading2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Daily Transaction",
                "top": "13dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLimitValue2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitValue2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxLimitValue2.setDefaultUnit(kony.flex.DP);
            var lblCurrencySymbol2 = new kony.ui.Label({
                "centerY": "52%",
                "id": "lblCurrencySymbol2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknFontIcon485C7518px",
                "text": "",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxLimitValue2 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
                "height": "40dp",
                "id": "tbxLimitValue2",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "25dp",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxLimitValue2.add(lblCurrencySymbol2, tbxLimitValue2);
            var flxLimitError2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLimitError2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxLimitError2.setDefaultUnit(kony.flex.DP);
            var lblLimitErrorIcon2 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblLimitErrorIcon2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimitErrorMsg2 = new kony.ui.Label({
                "id": "lblLimitErrorMsg2",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitError2.add(lblLimitErrorIcon2, lblLimitErrorMsg2);
            var flxRangeCont2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeCont2",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "23%"
            }, {}, {});
            flxRangeCont2.setDefaultUnit(kony.flex.DP);
            var lblRangeText2 = new kony.ui.Label({
                "id": "lblRangeText2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Range: ",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue2 = new kony.ui.Label({
                "id": "lblRangeValue2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "$2000 - $4000",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeCont2.add(lblRangeText2, lblRangeValue2);
            flxCustLimitsRow2.add(lblRowHeading2, flxLimitValue2, flxLimitError2, flxRangeCont2);
            var flxCustLimitsRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15dp",
                "clipBounds": true,
                "id": "flxCustLimitsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustLimitsRow3.setDefaultUnit(kony.flex.DP);
            var lblRowHeading3 = new kony.ui.Label({
                "id": "lblRowHeading3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Weekly Transaction",
                "top": "13dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLimitValue3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitValue3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxLimitValue3.setDefaultUnit(kony.flex.DP);
            var lblCurrencySymbol3 = new kony.ui.Label({
                "centerY": "52%",
                "id": "lblCurrencySymbol3",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknFontIcon485C7518px",
                "text": "",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxLimitValue3 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
                "height": "40dp",
                "id": "tbxLimitValue3",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "25dp",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxLimitValue3.add(lblCurrencySymbol3, tbxLimitValue3);
            var flxLimitError3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLimitError3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxLimitError3.setDefaultUnit(kony.flex.DP);
            var lblLimitErrorIcon3 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblLimitErrorIcon3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimitErrorMsg3 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblLimitErrorMsg3",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitError3.add(lblLimitErrorIcon3, lblLimitErrorMsg3);
            var flxRangeCont3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeCont3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "160dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "43dp",
                "width": "23%"
            }, {}, {});
            flxRangeCont3.setDefaultUnit(kony.flex.DP);
            var lblRangeText3 = new kony.ui.Label({
                "id": "lblRangeText3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Range: ",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValue3 = new kony.ui.Label({
                "id": "lblRangeValue3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "$2000 - $4000",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeCont3.add(lblRangeText3, lblRangeValue3);
            flxCustLimitsRow3.add(lblRowHeading3, flxLimitValue3, flxLimitError3, flxRangeCont3);
            flxCustLimitsContainer.add(flxCustLimitsRow1, flxCustLimitsRow2, flxCustLimitsRow3);
            var lblEditLimitsSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblEditLimitsSeperator",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Label"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnrollEditLimitsTemplate.add(lblLimitsHeading, flxCustLimitsContainer, lblEditLimitsSeperator);
            flxEnrollEditLimitsContainer.add(flxLimitErrorEditUser, flxEnrollEditLimitsTopCont, flxEnrollEditLimitsButtons, flxRangeTooltip, flxEnrollEditLimitsTemplate);
            var flxAccountTypesFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAccountTypesFilter",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "157dp",
                "width": "250px",
                "zIndex": 50
            }, {}, {});
            flxAccountTypesFilter.setDefaultUnit(kony.flex.DP);
            var flxArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-1dp",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArrowImage.setDefaultUnit(kony.flex.DP);
            var imgFilterArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgFilterArrow",
                "isVisible": true,
                "right": "10dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowImage.add(imgFilterArrow);
            var flxAccountTypesFilterBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "175px",
                "id": "flxAccountTypesFilterBox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBgF8F9FAbrD7D9E01pxRd3pxShd0D0D11",
                "top": "0px",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxAccountTypesFilterBox.setDefaultUnit(kony.flex.DP);
            var flxHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxHeader1.setDefaultUnit(kony.flex.DP);
            var lblFilterByHeading = new kony.ui.Label({
                "id": "lblFilterByHeading",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.FilterDataBy\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0px",
                "width": "20px"
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var lblIconStatus = new kony.ui.Label({
                "height": "15dp",
                "id": "lblIconStatus",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIcon485C7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage.add(lblIconStatus);
            flxHeader1.add(lblFilterByHeading, flxImage);
            var flxFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFilters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFilters.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteria1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "80dp",
                "id": "flxFilterCriteria1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "220px",
                "zIndex": 3
            }, {}, {});
            flxFilterCriteria1.setDefaultUnit(kony.flex.DP);
            var lblAccountTypesHeader = new kony.ui.Label({
                "id": "lblAccountTypesHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "Account Type",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListBoxAccounts = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "customListBoxAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27dp",
                "width": "100%",
                "overrides": {
                    "btnOk": {
                        "isVisible": false
                    },
                    "customListbox": {
                        "height": "40dp",
                        "top": "27dp"
                    },
                    "flxSegmentList": {
                        "isVisible": false
                    },
                    "flxSelectAll": {
                        "isVisible": false
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "top": "7dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterCriteria1.add(lblAccountTypesHeader, customListBoxAccounts);
            var flxFilterButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxFilterButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxFilterButtons.setDefaultUnit(kony.flex.DP);
            var btnApplyFilter = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnApplyFilter",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "0px",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxFilterButtons.add(btnApplyFilter);
            flxFilters.add(flxFilterCriteria1, flxFilterButtons);
            flxAccountTypesFilterBox.add(flxHeader1, flxFilters);
            flxAccountTypesFilter.add(flxArrowImage, flxAccountTypesFilterBox);
            flxEditCustRightContainer.add(flxEnrollEditAccountsContainer, flxEnrollEdiSignatoryContainer, flxEnrollEditFeaturesContainer, flxEnrollEditOtherFeaturesCont, flxEnrollEditLimitsContainer, flxAccountTypesFilter);
            flxEditUserContainer.add(flxEnrollEditVerticalTabs, flxEditCustRightContainer);
            var flxCreateContract = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreateContract",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCreateContract.setDefaultUnit(kony.flex.DP);
            var flxCreateContractContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxCreateContractContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCreateContractContainer.setDefaultUnit(kony.flex.DP);
            var flxCreateContractVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCreateContractVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxCreateContractVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabsContract = new com.adminConsole.common.verticalTabs1({
                "height": "100%",
                "id": "verticalTabsContract",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption0": {
                        "text": "SELECT SERVICE"
                    },
                    "btnOption1": {
                        "text": "SELECT CUSTOMERS"
                    },
                    "btnOption2": {
                        "text": "CONTRACT DETAILS"
                    },
                    "btnOption3": {
                        "text": "SELECT ACCOUNTS"
                    },
                    "btnOption4": {
                        "text": "ASSIGN FEATURES AND ACTIONS",
                        "width": "80%"
                    },
                    "btnOption5": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCreate.AssignLimits_UC\")"
                    },
                    "flxOption0": {
                        "isVisible": true
                    },
                    "flxOption3": {
                        "isVisible": true
                    },
                    "flxOption4": {
                        "isVisible": true
                    },
                    "flxOption5": {
                        "isVisible": true
                    },
                    "flxOption6": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected5": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "isVisible": true
                    },
                    "lblOptional3": {
                        "isVisible": true
                    },
                    "lblOptional4": {
                        "isVisible": true
                    },
                    "lblOptional5": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractVerticalTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractVerticalTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxContractVerticalTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractVerticalTabsSeperator.add();
            flxCreateContractVerticalTabs.add(verticalTabsContract, flxContractVerticalTabsSeperator);
            var flxContractServiceTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractServiceTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractServiceTab.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoriesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxAlertCategoriesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoriesList.setDefaultUnit(kony.flex.DP);
            var flxCategoryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxCategoryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryHeader.setDefaultUnit(kony.flex.DP);
            var lblContractService = new kony.ui.Label({
                "id": "lblContractService",
                "isVisible": true,
                "left": "25dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Select Service",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRecordsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxRecordsSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxRecordsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconRecordsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconRecordsSearch",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxRecordsSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxRecordsSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Service",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearRecordsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearRecordsSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearRecordsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCrossRecordsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossRecordsSearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearRecordsSearch.add(fontIconCrossRecordsSearch);
            flxRecordsSearch.add(fontIconRecordsSearch, tbxRecordsSearch, flxClearRecordsSearch);
            var flxServiceFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxServiceFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "15dp",
                "zIndex": 1
            }, {}, {});
            flxServiceFilter.setDefaultUnit(kony.flex.DP);
            var fontIconServiceFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconServiceFilter",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknSortIconHover15px"
            });
            flxServiceFilter.add(fontIconServiceFilter);
            flxCategoryHeader.add(lblContractService, flxRecordsSearch, flxServiceFilter);
            var flxContractServicesScrollCont = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "625dp",
                "horizontalScrollIndicator": true,
                "id": "flxContractServicesScrollCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "55dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxContractServicesScrollCont.setDefaultUnit(kony.flex.DP);
            var flxContractServiceCards = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractServiceCards",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractServiceCards.setDefaultUnit(kony.flex.DP);
            flxContractServiceCards.add();
            var flxNoServiceSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoServiceSearchResults",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxNoServiceSearchResults.setDefaultUnit(kony.flex.DP);
            var lblNoServiceSearchResults = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoServiceSearchResults",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceSearchResults.add(lblNoServiceSearchResults);
            flxContractServicesScrollCont.add(flxContractServiceCards, flxNoServiceSearchResults);
            flxAlertCategoriesList.add(flxCategoryHeader, flxContractServicesScrollCont);
            var flxContractServiceButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractServiceButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractServiceButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsServiceDetails = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsServiceDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                        "isVisible": true,
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractServiceButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractServiceButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractServiceButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractServiceButtonsSeperator.add();
            flxContractServiceButtons.add(commonButtonsServiceDetails, flxContractServiceButtonsSeperator);
            var flxServiceTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "slFbox",
                "top": "50px",
                "width": "150px",
                "zIndex": 5
            }, {}, {});
            flxServiceTypeFilter.setDefaultUnit(kony.flex.DP);
            var serviceTypeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "serviceTypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxServiceTypeFilter.add(serviceTypeFilterMenu);
            flxContractServiceTab.add(flxAlertCategoriesList, flxContractServiceButtons, flxServiceTypeFilter);
            var flxContractCustomersTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractCustomersTab",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractCustomersTab.setDefaultUnit(kony.flex.DP);
            var lblContractCustomersHeader = new kony.ui.Label({
                "id": "lblContractCustomersHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Select Customers",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContractCustomersButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractCustomersButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractCustomersButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsCustomers = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "220dp"
                    },
                    "btnSave": {
                        "isVisible": true,
                        "right": "20dp",
                        "text": "CREATE CONTRACT",
                        "width": "180px"
                    },
                    "flxRightButtons": {
                        "width": "350px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractCustomersButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractCustomersButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractCustomersButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractCustomersButtonsSeperator.add();
            flxContractCustomersButtons.add(commonButtonsCustomers, flxContractCustomersButtonsSeperator);
            var flxNoCustomersAdded = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoCustomersAdded",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoCustomersAdded.setDefaultUnit(kony.flex.DP);
            var imgContractCustomers = new kony.ui.Image2({
                "bottom": "20dp",
                "centerX": "50%",
                "height": "50dp",
                "id": "imgContractCustomers",
                "isVisible": true,
                "skin": "slImage",
                "src": "enrolluser.png",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersAdded = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersAdded",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Search Customers to add to this contract",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSearchContractCustomers = new kony.ui.Button({
                "centerX": "50%",
                "height": "22dp",
                "id": "btnSearchContractCustomers",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Search Customers",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoCustomersAdded.add(imgContractCustomers, lblNoCustomersAdded, btnSearchContractCustomers);
            var btnSelectCustomers = new kony.ui.Button({
                "height": "22dp",
                "id": "btnSelectCustomers",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Select Customers",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var segAddedCustomers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "85px",
                "data": [{
                    "btnRelation": "",
                    "imgCheckbox": "",
                    "imgRadioButton": "",
                    "lblContractId": "",
                    "lblContractInfo": "",
                    "lblContractName": "",
                    "lblContractRelation": "",
                    "lblData1": "",
                    "lblData2": "",
                    "lblData3": "",
                    "lblHeading1": "",
                    "lblHeading2": "",
                    "lblHeading3": "",
                    "lblIconRemove": "",
                    "lblLine": "",
                    "lblPrimaryText": ""
                }],
                "groupCells": false,
                "id": "segAddedCustomers",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxRelatedCustomerList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": 45,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnRelation": "btnRelation",
                    "flxCheckbox": "flxCheckbox",
                    "flxColumn1": "flxColumn1",
                    "flxColumn2": "flxColumn2",
                    "flxColumn3": "flxColumn3",
                    "flxContractDetails": "flxContractDetails",
                    "flxContractName": "flxContractName",
                    "flxContractRelation": "flxContractRelation",
                    "flxDetails": "flxDetails",
                    "flxLeftDetailsCont": "flxLeftDetailsCont",
                    "flxPrimaryBtn": "flxPrimaryBtn",
                    "flxRelatedCustomerList": "flxRelatedCustomerList",
                    "flxRelatedCustomerRow": "flxRelatedCustomerRow",
                    "flxRemoveContract": "flxRemoveContract",
                    "flxRightActionCont": "flxRightActionCont",
                    "flxRightDetailsCont": "flxRightDetailsCont",
                    "imgCheckbox": "imgCheckbox",
                    "imgRadioButton": "imgRadioButton",
                    "lblContractId": "lblContractId",
                    "lblContractInfo": "lblContractInfo",
                    "lblContractName": "lblContractName",
                    "lblContractRelation": "lblContractRelation",
                    "lblData1": "lblData1",
                    "lblData2": "lblData2",
                    "lblData3": "lblData3",
                    "lblHeading1": "lblHeading1",
                    "lblHeading2": "lblHeading2",
                    "lblHeading3": "lblHeading3",
                    "lblIconRemove": "lblIconRemove",
                    "lblLine": "lblLine",
                    "lblPrimaryText": "lblPrimaryText"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var ToolTipDelete = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ToolTipDelete",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "77dp",
                "width": "120px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "top": "77dp",
                        "width": "120px"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "left": "0dp",
                        "minHeight": "viz.val_cleared",
                        "top": "9dp",
                        "width": "110dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5dp",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "left": "5dp",
                        "text": "Primary Customer cannot be deleted",
                        "top": "5dp",
                        "width": "110dp"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "height": "10dp",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "17dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCustomerOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp"
            }, {}, {});
            flxCustomerOptions.setDefaultUnit(kony.flex.DP);
            var flxUpArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrow.setDefaultUnit(kony.flex.DP);
            var imgUp = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUp",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrow.add(imgUp);
            var contextualMenuInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "contextualMenuInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            contextualMenuInner.setDefaultUnit(kony.flex.DP);
            var flxEditOptionsCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditOptionsCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditOptionsCont.setDefaultUnit(kony.flex.DP);
            var lblOptionsHeader = new kony.ui.Label({
                "id": "lblOptionsHeader",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLatoReg9ca9ba10px",
                "text": "EDIT PREFERENCES",
                "top": 10,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCheckboxCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "30px",
                "id": "flxCheckboxCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxCheckboxCont.setDefaultUnit(kony.flex.DP);
            var flxCheckboxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCheckboxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15dp"
            }, {}, {});
            flxCheckboxOptions.setDefaultUnit(kony.flex.DP);
            var imgCheckboxOptions = new kony.ui.Image2({
                "height": "100%",
                "id": "imgCheckboxOptions",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxOptions.add(imgCheckboxOptions);
            var lblAccountOptions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountOptions",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Implicit Account Access",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxCont.add(flxCheckboxOptions, lblAccountOptions);
            flxEditOptionsCont.add(lblOptionsHeader, flxCheckboxCont);
            var flxDeleteContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDeleteContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDeleteContainer.setDefaultUnit(kony.flex.DP);
            var flxCustOptionsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxCustOptionsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustOptionsSeperator.setDefaultUnit(kony.flex.DP);
            flxCustOptionsSeperator.add();
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDelete.setDefaultUnit(kony.flex.DP);
            var lblIconDelete = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblIconDelete",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknIcon20px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDelete",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.delete\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(lblIconDelete, lblDelete);
            flxDeleteContainer.add(flxCustOptionsSeperator, flxDelete);
            contextualMenuInner.add(flxEditOptionsCont, flxDeleteContainer);
            var flxDownArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrow.setDefaultUnit(kony.flex.DP);
            var imgDown = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDown",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrow.add(imgDown);
            flxCustomerOptions.add(flxUpArrow, contextualMenuInner, flxDownArrow);
            flxContractCustomersTab.add(lblContractCustomersHeader, flxContractCustomersButtons, flxNoCustomersAdded, btnSelectCustomers, segAddedCustomers, ToolTipDelete, flxCustomerOptions);
            var flxContractDetailsTab = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractDetailsTab",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractDetailsTab.setDefaultUnit(kony.flex.DP);
            var flxContractDetailsContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "95dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxContractDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxContractDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxContractNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxContractNotification",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractNotification.setDefaultUnit(kony.flex.DP);
            var NotificationFlagMessage = new com.adminConsole.common.NotificationFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "NotificationFlagMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "NotificationFlagMessage": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "text": "This Contract is associated with multiple customers"
                    },
                    "lblNotificationIcon": {
                        "centerX": "45%",
                        "centerY": "45%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractNotification.add(NotificationFlagMessage);
            var flxContractyDetailsRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxContractyDetailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContractyDetailsRow1.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry1 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "51%",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblErrorText": {
                        "text": "Contract Name cannot be empty"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Contracts.ContractName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Contracts.ContractName\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "51%",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractDetail2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractDetail2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "51%",
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractDetail2.setDefaultUnit(kony.flex.DP);
            var lblContractServiceHeading = new kony.ui.Label({
                "id": "lblContractServiceHeading",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Contracts.serviceDefinition\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxContractService = new kony.ui.ListBox({
                "height": "40dp",
                "id": "lstBoxContractService",
                "isVisible": true,
                "left": "10dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "0dp",
                "skin": "sknLbxborderd7d9e03pxradiusF3F3F3Disabled",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLstBxBre1e5edBgf5f6f813pxDisableHover",
                "multiSelect": false
            });
            flxContractDetail2.add(lblContractServiceHeading, lstBoxContractService);
            flxContractyDetailsRow1.add(contractDetailsEntry1, flxContractDetail2);
            var flxContractDetailsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxContractDetailsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContractDetailsHeading.setDefaultUnit(kony.flex.DP);
            var flxContractDetailsHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractDetailsHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractDetailsHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxContractDetailsHeaderSeperator.add();
            var lblContactDetailsHeading = new kony.ui.Label({
                "id": "lblContactDetailsHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.ContactDetails\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactDetailsHeadingInfo = new kony.ui.Label({
                "id": "lblContactDetailsHeadingInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "(Primary customer details will be applied to the contract by default)",
                "top": "45px",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContractDetailsHeading.add(flxContractDetailsHeaderSeperator, lblContactDetailsHeading, lblContactDetailsHeadingInfo);
            var flxContractDetailsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "100dp",
                "id": "flxContractDetailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxContractDetailsRow2.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry2 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "51%",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompaniesController.Fax\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompaniesController.Fax\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "51%",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractDetailsContactNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxContractDetailsContactNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "52.50%",
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractDetailsContactNo.setDefaultUnit(kony.flex.DP);
            var lblContractDetailsContact = new kony.ui.Label({
                "id": "lblContractDetailsContact",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptional1 = new kony.ui.Label({
                "id": "lblOptional1",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var contractContactNumber = new com.adminConsole.common.contactNumber({
                "height": "100%",
                "id": "contractContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "contactNumber": {
                        "top": "30dp"
                    },
                    "flxContactNumberWrapper": {
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxError": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "top": "40dp",
                        "width": "100%"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "33%"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "width": "61%"
                    },
                    "txtISDCode": {
                        "isVisible": true,
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsContactNo.add(lblContractDetailsContact, lblOptional1, contractContactNumber);
            flxContractDetailsRow2.add(contractDetailsEntry2, flxContractDetailsContactNo);
            var flxContractDetailsRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxContractDetailsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContractDetailsRow3.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry3 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "51%",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompaniesController.EmailAddress\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompaniesController.EmailAddress\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "51%",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsRow3.add(contractDetailsEntry3);
            var flxContractDetailsRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxContractDetailsRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContractDetailsRow4.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry4 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "right": "0dp",
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "15dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsRow4.add(contractDetailsEntry4);
            var flxContractDetailsRow5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100px",
                "id": "flxContractDetailsRow5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContractDetailsRow5.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry5 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "right": "0dp",
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "15dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsRow5.add(contractDetailsEntry5);
            var flxContractDetailsRow6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxContractDetailsRow6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxContractDetailsRow6.setDefaultUnit(kony.flex.DP);
            var contractDetailsEntry6 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "51%",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")"
                    },
                    "textBoxEntry": {
                        "left": "10dp",
                        "right": "51%",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var contractDetailsEntry7 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry7",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "51%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "textBoxEntry": {
                        "isVisible": false,
                        "left": "51%",
                        "right": "15dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var contractDetailsEntry8 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contractDetailsEntry8",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "51%",
                "skin": "slFbox",
                "top": "100dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "top": "30dp"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.TownorCity\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.TownorCity\")"
                    },
                    "textBoxEntry": {
                        "isVisible": true,
                        "left": "10dp",
                        "right": "51%",
                        "top": "100dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "51%",
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractCountry.setDefaultUnit(kony.flex.DP);
            var lblContractCountry = new kony.ui.Label({
                "id": "lblContractCountry",
                "isVisible": true,
                "left": "10dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContractCountryOptional = new kony.ui.Label({
                "id": "lblContractCountryOptional",
                "isVisible": true,
                "left": "59px",
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContractCountryTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractCountryTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "30dp"
            }, {}, {});
            flxContractCountryTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadContractCountry = new com.adminConsole.common.typeHead({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "typeHeadContractCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "typeHead": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractCountryTypeHead.add(typeHeadContractCountry);
            var imgMandatoryContractCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryContractCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoContractCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoContractCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoContractCountry.setDefaultUnit(kony.flex.DP);
            var lblNoContractCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoContractCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoContractCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoContractCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ErrorCountry\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoContractCountry.add(lblNoContractCountryErrorIcon, lblNoContractCountryError);
            flxContractCountry.add(lblContractCountry, lblContractCountryOptional, flxContractCountryTypeHead, imgMandatoryContractCountry, flxNoContractCountry);
            flxContractDetailsRow6.add(contractDetailsEntry6, contractDetailsEntry7, contractDetailsEntry8, flxContractCountry);
            flxContractDetailsContainer.add(flxContractNotification, flxContractyDetailsRow1, flxContractDetailsHeading, flxContractDetailsRow2, flxContractDetailsRow3, flxContractDetailsRow4, flxContractDetailsRow5, flxContractDetailsRow6);
            var flxContractDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractDetailsButtons.setDefaultUnit(kony.flex.DP);
            var flxButtonsSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxButtonsSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxButtonsSeperator1.setDefaultUnit(kony.flex.DP);
            flxButtonsSeperator1.add();
            var contractDetailsCommonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "contractDetailsCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "left": "viz.val_cleared",
                        "right": "220dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Contracts.createContract\")",
                        "isVisible": true,
                        "right": "20dp",
                        "width": "180px"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "320px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsButtons.add(flxButtonsSeperator1, contractDetailsCommonButtons);
            flxContractDetailsTab.add(flxContractDetailsContainer, flxContractDetailsButtons);
            var flxContractAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractAccounts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractAccounts.setDefaultUnit(kony.flex.DP);
            var lblContractAccountsHeader = new kony.ui.Label({
                "id": "lblContractAccountsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "View By Customers",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomerDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "30dp",
                "id": "flxCustomerDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "270px",
                "zIndex": 10
            }, {}, {});
            flxCustomerDropdown.setDefaultUnit(kony.flex.DP);
            var customersDropdown = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "customersDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSelectedText": {
                        "width": "270px"
                    },
                    "lblSelectedValue": {
                        "text": "Temenos India Private Li...",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCustomerDropdown.add(customersDropdown);
            var lblSeperatorAccounts = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperatorAccounts",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "-",
                "top": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContractAccountsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxContractAccountsSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "55dp",
                "width": "270px",
                "zIndex": 1
            }, {}, {});
            flxContractAccountsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconAccountsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAccountsSearch",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAccountsSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxAccountsSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Acc.Number, Acc.Name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearAccountsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearAccountsSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearAccountsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCrossAccountsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossAccountsSearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearAccountsSearch.add(fontIconCrossAccountsSearch);
            flxContractAccountsSearch.add(fontIconAccountsSearch, tbxAccountsSearch, flxClearAccountsSearch);
            var flxNoCustomerSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoCustomerSelected",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoCustomerSelected.setDefaultUnit(kony.flex.DP);
            var imgContractAccounts = new kony.ui.Image2({
                "bottom": "20dp",
                "centerX": "50%",
                "height": "55dp",
                "id": "imgContractAccounts",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectscenario.png",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersSelected = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersSelected",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Select a customer to view its Account Details",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCustomerSelected.add(imgContractAccounts, lblNoCustomersSelected);
            var flxContractAccountsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractAccountsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractAccountsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsContractAccounts = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsContractAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "220dp"
                    },
                    "btnSave": {
                        "isVisible": true,
                        "right": "20dp",
                        "text": "CREATE CONTRACT",
                        "width": "180px"
                    },
                    "flxRightButtons": {
                        "width": "350px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractAccountsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractAccountsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractAccountsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractAccountsButtonsSeperator.add();
            flxContractAccountsButtons.add(commonButtonsContractAccounts, flxContractAccountsButtonsSeperator);
            var flxContractAccountsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxContractAccountsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "115dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxContractAccountsList.setDefaultUnit(kony.flex.DP);
            var ContractAccountsList = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContractAccountsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxArrow": {
                        "isVisible": false
                    },
                    "flxCardBottomContainer": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true
                    },
                    "flxCheckboxCont": {
                        "isVisible": false
                    },
                    "lblCount": {
                        "text": "(02)"
                    },
                    "lblHeading": {
                        "text": "Accounts"
                    },
                    "lblTotalCount": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractAccountsList.add(ContractAccountsList);
            var flxAccountsFilterContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsFilterContracts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "slFbox",
                "top": "200px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxAccountsFilterContracts.setDefaultUnit(kony.flex.DP);
            var accountTypesFilterContracts = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "accountTypesFilterContracts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountsFilterContracts.add(accountTypesFilterContracts);
            var flxOwnershipFilterContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOwnershipFilterContracts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "200px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxOwnershipFilterContracts.setDefaultUnit(kony.flex.DP);
            var ownershipFilterContracts = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ownershipFilterContracts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOwnershipFilterContracts.add(ownershipFilterContracts);
            flxContractAccounts.add(lblContractAccountsHeader, flxCustomerDropdown, lblSeperatorAccounts, flxContractAccountsSearch, flxNoCustomerSelected, flxContractAccountsButtons, flxContractAccountsList, flxAccountsFilterContracts, flxOwnershipFilterContracts);
            var flxContractFA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractFA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractFA.setDefaultUnit(kony.flex.DP);
            var lblContractFAHeader = new kony.ui.Label({
                "id": "lblContractFAHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "View By Customers",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomerDropdownFA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "30dp",
                "id": "flxCustomerDropdownFA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "270px",
                "zIndex": 10
            }, {}, {});
            flxCustomerDropdownFA.setDefaultUnit(kony.flex.DP);
            var customersDropdownFA = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "customersDropdownFA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSelectedText": {
                        "width": "270px"
                    },
                    "lblSelectedValue": {
                        "text": "Temenos India Private Li...",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCustomerDropdownFA.add(customersDropdownFA);
            var btnUpdateInBulkFA = new kony.ui.Button({
                "height": "22dp",
                "id": "btnUpdateInBulkFA",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Update Permissions in Bulk",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxContractFASearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxContractFASearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "55dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxContractFASearch.setDefaultUnit(kony.flex.DP);
            var fontIconContractFASearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconContractFASearch",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxContractFASearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxContractFASearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Feature Name, Action Name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearContractFASearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearContractFASearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearContractFASearch.setDefaultUnit(kony.flex.DP);
            var fontIconCrossFASearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossFASearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearContractFASearch.add(fontIconCrossFASearch);
            flxContractFASearch.add(fontIconContractFASearch, tbxContractFASearch, flxClearContractFASearch);
            var lblSeperatorFA = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperatorFA",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "-",
                "top": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCustomerSelectedFA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoCustomerSelectedFA",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoCustomerSelectedFA.setDefaultUnit(kony.flex.DP);
            var imgContractFA = new kony.ui.Image2({
                "bottom": "20dp",
                "centerX": "50%",
                "height": "55dp",
                "id": "imgContractFA",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectscenario.png",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersSelectedFA = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersSelectedFA",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Select a customer to view its Features and Actions",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCustomerSelectedFA.add(imgContractFA, lblNoCustomersSelectedFA);
            var flxContractFAButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractFAButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractFAButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsContractFA = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsContractFA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "220dp"
                    },
                    "btnSave": {
                        "isVisible": true,
                        "right": "20dp",
                        "text": "CREATE CONTRACT",
                        "width": "180px"
                    },
                    "flxRightButtons": {
                        "width": "350px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractFAButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractFAButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractFAButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractFAButtonsSeperator.add();
            flxContractFAButtons.add(commonButtonsContractFA, flxContractFAButtonsSeperator);
            var flxContractFAList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxContractFAList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "115dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxContractFAList.setDefaultUnit(kony.flex.DP);
            var ContractFAList = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContractFAList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "flxCardBottomContainer": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true
                    },
                    "lblHeading": {
                        "text": "Features"
                    },
                    "lblTotalCount": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractFAList.add(ContractFAList);
            flxContractFA.add(lblContractFAHeader, flxCustomerDropdownFA, btnUpdateInBulkFA, flxContractFASearch, lblSeperatorFA, flxNoCustomerSelectedFA, flxContractFAButtons, flxContractFAList);
            var flxContractLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractLimits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractLimits.setDefaultUnit(kony.flex.DP);
            var flxLimitsErrorFlag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitsErrorFlag",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxLimitsErrorFlag.setDefaultUnit(kony.flex.DP);
            var limitsErrorFlag = new com.adminConsole.common.errorFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "limitsErrorFlag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblErrorValue": {
                        "text": "Please correct the error messages across all the customers to proceed further."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLimitsErrorFlag.add(limitsErrorFlag);
            var flxContractLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxContractLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxContractLimitsContainer.setDefaultUnit(kony.flex.DP);
            var lblContractLimitsHeader = new kony.ui.Label({
                "id": "lblContractLimitsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "View By Customers",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomerDropdownLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "30dp",
                "id": "flxCustomerDropdownLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "270px",
                "zIndex": 10
            }, {}, {});
            flxCustomerDropdownLimits.setDefaultUnit(kony.flex.DP);
            var customersDropdownContractLimits = new com.adminConsole.contracts.customersDropdown({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "customersDropdownContractLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSelectedText": {
                        "width": "270px"
                    },
                    "lblSelectedValue": {
                        "text": "Temenos India Private Li...",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCustomerDropdownLimits.add(customersDropdownContractLimits);
            var btnUpdateInBulkLimits = new kony.ui.Button({
                "height": "22dp",
                "id": "btnUpdateInBulkLimits",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Update Limits in Bulk",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxContractLimitsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxContractLimitsSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "55dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxContractLimitsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconContractLimitsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconContractLimitsSearch",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxContractLimitsSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxContractLimitsSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Feature Name, Action Name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearContractLimitsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearContractLimitsSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearContractLimitsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCrossLimitsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossLimitsSearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearContractLimitsSearch.add(fontIconCrossLimitsSearch);
            flxContractLimitsSearch.add(fontIconContractLimitsSearch, tbxContractLimitsSearch, flxClearContractLimitsSearch);
            var lblSeperatorLimits = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperatorLimits",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "-",
                "top": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCustomerSelectedLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoCustomerSelectedLimits",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoCustomerSelectedLimits.setDefaultUnit(kony.flex.DP);
            var imgContractLimits = new kony.ui.Image2({
                "bottom": "20dp",
                "centerX": "50%",
                "height": "55dp",
                "id": "imgContractLimits",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectscenario.png",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersSelectedLimits = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersSelectedLimits",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Select a customer to view its Limits",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCustomerSelectedLimits.add(imgContractLimits, lblNoCustomersSelectedLimits);
            var flxContractLimitsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxContractLimitsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractLimitsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsContractLimits = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsContractLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20dp"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "isVisible": true,
                        "right": "20dp",
                        "text": "CREATE CONTRACT",
                        "width": "180px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContractLimitsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContractLimitsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxContractLimitsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxContractLimitsButtonsSeperator.add();
            flxContractLimitsButtons.add(commonButtonsContractLimits, flxContractLimitsButtonsSeperator);
            var flxContractLimitsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxContractLimitsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "115dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxContractLimitsList.setDefaultUnit(kony.flex.DP);
            var ContractLimitsList = new com.adminConsole.contracts.accountsFeaturesCard({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContractLimitsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0dp",
                "overrides": {
                    "accountsFeaturesCard": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": "20dp",
                        "width": "viz.val_cleared"
                    },
                    "btnReset": {
                        "isVisible": true
                    },
                    "flxArrow": {
                        "isVisible": false
                    },
                    "flxBottomSeperator": {
                        "isVisible": false
                    },
                    "flxCardBottomContainer": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true
                    },
                    "lblCount": {
                        "isVisible": false
                    },
                    "lblHeading": {
                        "text": "Limits"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractLimitsList.add(ContractLimitsList);
            var flxRangeTooltipContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltipContracts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "320dp",
                "zIndex": 5
            }, {}, {});
            flxRangeTooltipContracts.setDefaultUnit(kony.flex.DP);
            var flxRangeTooltipArrowContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxRangeTooltipArrowContracts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxRangeTooltipArrowContracts.setDefaultUnit(kony.flex.DP);
            var imgRangeTooltipArrowContracts = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgRangeTooltipArrowContracts",
                "isVisible": true,
                "left": "20dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeTooltipArrowContracts.add(imgRangeTooltipArrowContracts);
            var flxRangeTooltipBodyContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltipBodyContracts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFbrD7D9E01pxRd3pxShd0D0D11",
                "top": "11dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRangeTooltipBodyContracts.setDefaultUnit(kony.flex.DP);
            var flxRangeTooltipHeaderContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeTooltipHeaderContracts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRangeTooltipHeaderContracts.setDefaultUnit(kony.flex.DP);
            var lblHeadingContract1 = new kony.ui.Label({
                "id": "lblHeadingContract1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7311px",
                "text": "TRANSACTION TYPE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeadingContract2 = new kony.ui.Label({
                "id": "lblHeadingContract2",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblLato696c7311px",
                "text": "MIN VALUE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeadingContract3 = new kony.ui.Label({
                "id": "lblHeadingContract3",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknlblLato696c7311px",
                "text": "MAX VALUE",
                "top": "10dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeTooltipHeaderContracts.add(lblHeadingContract1, lblHeadingContract2, lblHeadingContract3);
            var flxRangeRowContracts1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeRowContracts1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxRangeRowContracts1.setDefaultUnit(kony.flex.DP);
            var lblRangeTitleContract1 = new kony.ui.Label({
                "id": "lblRangeTitleContract1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Per Transaction",
                "top": "0dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRangeValueContract11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract11.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract11 = new kony.ui.Label({
                "id": "lblIconCurrContract11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract11 = new kony.ui.Label({
                "id": "lblRangeValueContract11",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract11.add(lblIconCurrContract11, lblRangeValueContract11);
            var flxRangeValueContract12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract12",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract12.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract12 = new kony.ui.Label({
                "id": "lblIconCurrContract12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract12 = new kony.ui.Label({
                "id": "lblRangeValueContract12",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract12.add(lblIconCurrContract12, lblRangeValueContract12);
            flxRangeRowContracts1.add(lblRangeTitleContract1, flxRangeValueContract11, flxRangeValueContract12);
            var flxRangeRowContracts2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeRowContracts2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxRangeRowContracts2.setDefaultUnit(kony.flex.DP);
            var lblRangeTitleContract2 = new kony.ui.Label({
                "id": "lblRangeTitleContract2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Daily Transaction",
                "top": "0dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRangeValueContract21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract21",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract21.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract21 = new kony.ui.Label({
                "id": "lblIconCurrContract21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract21 = new kony.ui.Label({
                "id": "lblRangeValueContract21",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract21.add(lblIconCurrContract21, lblRangeValueContract21);
            var flxRangeValueContract22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract22",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract22.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract22 = new kony.ui.Label({
                "id": "lblIconCurrContract22",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract22 = new kony.ui.Label({
                "id": "lblRangeValueContract22",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract22.add(lblIconCurrContract22, lblRangeValueContract22);
            flxRangeRowContracts2.add(lblRangeTitleContract2, flxRangeValueContract21, flxRangeValueContract22);
            var flxRangeRowContracts3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxRangeRowContracts3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxRangeRowContracts3.setDefaultUnit(kony.flex.DP);
            var lblRangeTitleContract3 = new kony.ui.Label({
                "id": "lblRangeTitleContract3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Daily Transaction",
                "top": "0dp",
                "width": "31%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRangeValueContract31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract31",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract31.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract31 = new kony.ui.Label({
                "id": "lblIconCurrContract31",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract31 = new kony.ui.Label({
                "id": "lblRangeValueContract31",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract31.add(lblIconCurrContract31, lblRangeValueContract31);
            var flxRangeValueContract32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRangeValueContract32",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%"
            }, {}, {});
            flxRangeValueContract32.setDefaultUnit(kony.flex.DP);
            var lblIconCurrContract32 = new kony.ui.Label({
                "id": "lblIconCurrContract32",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": "15dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRangeValueContract32 = new kony.ui.Label({
                "id": "lblRangeValueContract32",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato696c7311px",
                "text": "2000",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRangeValueContract32.add(lblIconCurrContract32, lblRangeValueContract32);
            flxRangeRowContracts3.add(lblRangeTitleContract3, flxRangeValueContract31, flxRangeValueContract32);
            flxRangeTooltipBodyContracts.add(flxRangeTooltipHeaderContracts, flxRangeRowContracts1, flxRangeRowContracts2, flxRangeRowContracts3);
            flxRangeTooltipContracts.add(flxRangeTooltipArrowContracts, flxRangeTooltipBodyContracts);
            flxContractLimitsContainer.add(lblContractLimitsHeader, flxCustomerDropdownLimits, btnUpdateInBulkLimits, flxContractLimitsSearch, lblSeperatorLimits, flxNoCustomerSelectedLimits, flxContractLimitsButtons, flxContractLimitsList, flxRangeTooltipContracts);
            flxContractLimits.add(flxLimitsErrorFlag, flxContractLimitsContainer);
            flxCreateContractContainer.add(flxCreateContractVerticalTabs, flxContractServiceTab, flxContractCustomersTab, flxContractDetailsTab, flxContractAccounts, flxContractFA, flxContractLimits);
            var flxContractInlineError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxContractInlineError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContractInlineError.setDefaultUnit(kony.flex.DP);
            var lblContractInlineErrorIcon = new kony.ui.Label({
                "centerX": "41%",
                "centerY": "52%",
                "id": "lblContractInlineErrorIcon",
                "isVisible": true,
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContractInlineError = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblContractInlineError",
                "isVisible": true,
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.AddAtleastOneAccountToContinue\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContractInlineError.add(lblContractInlineErrorIcon, lblContractInlineError);
            flxCreateContract.add(flxCreateContractContainer, flxContractInlineError);
            flxMainContainer.add(flxEnrollCustomerContainer, flxAddAnotherCustContainer, flxEditUserContainer, flxCreateContract);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessageWithLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessageWithLink",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToastMessageWithLink.setDefaultUnit(kony.flex.DP);
            var toastMessageWithLink = new com.adminConsole.common.toastMessageWithLink({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessageWithLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblToastMessageLeft": {
                        "text": "20/100 branches uploaded to the system."
                    },
                    "lblToastMessageRight": {
                        "text": "to download the details."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessageWithLink.add(toastMessageWithLink);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxBulkUpdateFeaturesPopupContract = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBulkUpdateFeaturesPopupContract",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBulkUpdateFeaturesPopupContract.setDefaultUnit(kony.flex.DP);
            var bulkUpdateFeaturesLimitsPopupContract = new com.adminConsole.contracts.bulkUpdateFeaturesLimitsPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "bulkUpdateFeaturesLimitsPopupContract",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnAddNewRow": {
                        "text": "+Add New Permissions"
                    },
                    "bulkUpdateAddNewFeatureRow.lblFieldName11": {
                        "text": "Feature"
                    },
                    "bulkUpdateAddNewFeatureRow.lblFieldName12": {
                        "text": "Action"
                    },
                    "bulkUpdateAddNewFeatureRow.lblFieldName13": {
                        "text": "Permission Type"
                    },
                    "commonButtonsScreen1.btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Next\")"
                    },
                    "commonButtonsScreen2.btnSave": {
                        "text": "APPLY CHANGES",
                        "width": "162px"
                    },
                    "flxAddNewRowContainer": {
                        "isVisible": false
                    },
                    "flxAddNewRowListCont": {
                        "clipBounds": false,
                        "isVisible": false
                    },
                    "flxBulkUpdateScreen1": {
                        "isVisible": true
                    },
                    "flxBulkUpdateScreen2": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "flxSegmentContainer": {
                        "top": "70dp"
                    },
                    "lblDetailsHeading": {
                        "text": "Update Permissions in Bulk"
                    },
                    "lblHeadingScreen1": {
                        "text": "Select Customer"
                    },
                    "lblSelectedHeading": {
                        "text": "Selected Customers"
                    },
                    "lblTitle": {
                        "text": "Add Permission"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBulkUpdateFeaturesPopupContract.add(bulkUpdateFeaturesLimitsPopupContract);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContainer, flxLoading, flxToastMessageWithLink, flxToastMessage, flxBulkUpdateFeaturesPopupContract);
            var flxEnrollCustConfirmationPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnrollCustConfirmationPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxEnrollCustConfirmationPopup.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "flxPopupHeader": {
                        "width": "100%"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate Group"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Group Auto Loan.\nThe Group has been assigned to few customers. Deactivating this may result in losing certain entitlements.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEnrollCustConfirmationPopup.add(popUpDeactivate);
            var flxContractDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContractDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxContractDetailsPopup.setDefaultUnit(kony.flex.DP);
            var contractDetailsPopup = new com.adminConsole.contracts.contractDetailsPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "contractDetailsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContractDetailsPopup.add(contractDetailsPopup);
            var flxBulkUpdateFeaturesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBulkUpdateFeaturesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBulkUpdateFeaturesPopup.setDefaultUnit(kony.flex.DP);
            var bulkUpdateFeaturesLimitsPopup = new com.adminConsole.contracts.bulkUpdateFeaturesLimitsPopup({
                "height": "100%",
                "id": "bulkUpdateFeaturesLimitsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "bulkUpdateAddNewFeatureRow.flxFieldColumn22": {
                        "isVisible": true
                    },
                    "bulkUpdateFeaturesLimitsPopup": {
                        "isVisible": true
                    },
                    "commonButtonsScreen1.btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Next\")"
                    },
                    "commonButtonsScreen2.btnSave": {
                        "text": "APPLY CHANGES",
                        "width": "162px"
                    },
                    "customRadioButtonGroup.imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "customRadioButtonGroup.imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "flxAddNewRowContainer": {
                        "isVisible": false
                    },
                    "flxBulkUpdateScreen1": {
                        "isVisible": true
                    },
                    "flxBulkUpdateScreen2": {
                        "isVisible": false
                    },
                    "lblHeadingScreen1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SelectAccounts\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBulkUpdateFeaturesPopup.add(bulkUpdateFeaturesLimitsPopup);
            var flxCustomerSearchPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerSearchPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxCustomerSearchPopUp.setDefaultUnit(kony.flex.DP);
            var flxCustomerSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "95%",
                "id": "flxCustomerSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknbackGroundffffff100",
                "top": "20px",
                "width": "70%",
                "zIndex": 100
            }, {}, {});
            flxCustomerSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxSearchNoResultPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchNoResultPage",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxSearchNoResultPage.setDefaultUnit(kony.flex.DP);
            var flxSearchHeadingNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10%",
                "id": "flxSearchHeadingNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeadingNoResult.setDefaultUnit(kony.flex.DP);
            var flxPopUpTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTop.setDefaultUnit(kony.flex.DP);
            flxPopUpTop.add();
            var flxPopupHeaderNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxPopupHeaderNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "20%",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeaderNoResult.setDefaultUnit(kony.flex.DP);
            var flxArrowNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "70%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxArrowNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxArrowNoResult.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessageNoResult = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpMainMessageNoResult",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblPopUpMainMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgArrowNoResult = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12dp",
                "id": "imgArrowNoResult",
                "isVisible": false,
                "left": "10%",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnModifySearchNoResult = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnModifySearchNoResult",
                "isVisible": true,
                "left": "10%",
                "onClick": controller.AS_Button_j73c6a5a112d4eefb808714c05a92535,
                "right": "0px",
                "skin": "sknBtnLato117eb013Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxArrowNoResult.add(lblPopUpMainMessageNoResult, imgArrowNoResult, btnModifySearchNoResult);
            var flxRightImageNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxRightImageNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRightImageNoResult.setDefaultUnit(kony.flex.DP);
            var fontIconNoResultCross = new kony.ui.Label({
                "id": "fontIconNoResultCross",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconSearchCross20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightImageNoResult.add(fontIconNoResultCross);
            flxPopupHeaderNoResult.add(flxArrowNoResult, flxRightImageNoResult);
            flxSearchHeadingNoResult.add(flxPopUpTop, flxPopupHeaderNoResult);
            var flxNoResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "85%",
                "id": "flxNoResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "0px",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxNoResult.setDefaultUnit(kony.flex.DP);
            var lblMainMessageNoResult = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "49%",
                "id": "lblMainMessageNoResult",
                "isVisible": true,
                "skin": "sknLblLatoBold485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblMainMessageNoResult\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnModifySearchNoResult1 = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "52%",
                "id": "btnModifySearchNoResult1",
                "isVisible": true,
                "onClick": controller.AS_Button_f37a1f527eec4a838faa9efd7499c1ea,
                "skin": "sknBtnLato117eb013Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxNoResult.add(lblMainMessageNoResult, btnModifySearchNoResult1);
            flxSearchNoResultPage.add(flxSearchHeadingNoResult, flxNoResult);
            var flxToastMessageSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "centerX": "50%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessageSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "60%",
                "zIndex": 110
            }, {}, {});
            flxToastMessageSearch.setDefaultUnit(kony.flex.DP);
            var toastMessageAdvSearch = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessageAdvSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxToastContainer": {
                        "width": "80%"
                    },
                    "lbltoastMessage": {
                        "text": "251 Customer added successfully."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessageSearch.add(toastMessageAdvSearch);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "9%",
                "clipBounds": false,
                "height": "100%",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0%",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBg4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "50px",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "zIndex": 20
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxCustomerSearchHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCustomerSearchHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchHeader.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "SEARCH CUSTOMERS BY",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCustomerSearchError = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgCustomerSearchError",
                "isVisible": false,
                "left": "20dp",
                "skin": "slImage",
                "src": "error_2x.png",
                "top": 0,
                "width": "12px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerSearchError = new kony.ui.Label({
                "id": "lblCustomerSearchError",
                "isVisible": false,
                "left": "10px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerSearchHeader.add(lblPopUpMainMessage, imgCustomerSearchError, lblCustomerSearchError);
            var flxRightImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxRightImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRightImage.setDefaultUnit(kony.flex.DP);
            var fontIconRight = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconRight",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconSearchCross20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightImage.add(fontIconRight);
            var flxCustomersBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "20px",
                "id": "flxCustomersBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "30px",
                "zIndex": 1
            }, {}, {});
            flxCustomersBreadcrumb.setDefaultUnit(kony.flex.DP);
            var flxSearchBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "15dp",
                "id": "flxSearchBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxSearchBreadcrumb.setDefaultUnit(kony.flex.DP);
            var btnBackToMain = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnBackToMain",
                "isVisible": true,
                "left": "0px",
                "skin": "sknbtn117eb011px",
                "text": "SEARCH RESULTS",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconBreadcrumbsRight = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconBreadcrumbsRight",
                "isVisible": true,
                "left": "6dp",
                "right": 6,
                "skin": "sknFontIconBreadcrumb",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrentScreen = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblCurrentScreen",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato00000011px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.ADDQUESTION\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchBreadcrumb.add(btnBackToMain, fontIconBreadcrumbsRight, lblCurrentScreen);
            var flxSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegMore",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxSegMore.setDefaultUnit(kony.flex.DP);
            var flxUpArrowMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowMore",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowMore.setDefaultUnit(kony.flex.DP);
            var imgUpArrowMore = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgUpArrowMore",
                "isVisible": true,
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowMore.add(imgUpArrowMore);
            var flxCustomersOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomersOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustomersOuter.setDefaultUnit(kony.flex.DP);
            var segBreadcrumbs = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segBreadcrumbs",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": 5,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                }
            }, {
                "padding": [0, 0, 0, 2],
                "paddingInPixel": false
            }, {});
            flxCustomersOuter.add(segBreadcrumbs);
            flxSegMore.add(flxUpArrowMore, flxCustomersOuter);
            flxCustomersBreadcrumb.add(flxSearchBreadcrumb, flxSegMore);
            flxPopupHeader.add(flxCustomerSearchHeader, flxRightImage, flxCustomersBreadcrumb);
            var flxCustomerSearchFilters = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCustomerSearchFilters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "60px",
                "verticalScrollIndicator": true,
                "zIndex": 10
            }, {}, {});
            flxCustomerSearchFilters.setDefaultUnit(kony.flex.DP);
            var flxCustomerDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxCustomerDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCustomerDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn11.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust11 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "right": "viz.val_cleared"
                    },
                    "lblHeadingText": {
                        "text": "Customer ID"
                    },
                    "tbxEnterValue": {
                        "placeholder": "Customer ID"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn11.add(textBoxEntryContractCust11);
            var flxColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn12.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust12 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": false,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn12.add(textBoxEntryContractCust12);
            var flxColumn13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn13",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn13.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust13 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Contracts.emailID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Contracts.emailID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn13.add(textBoxEntryContractCust13);
            flxRow1.add(flxColumn11, flxColumn12, flxColumn13);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "80dp",
                "id": "flxRow2",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn21.setDefaultUnit(kony.flex.DP);
            var lblHeading21 = new kony.ui.Label({
                "id": "lblHeading21",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var textBoxEntryContractCust21 = new com.adminConsole.common.contactNumber({
                "height": "70.56%",
                "id": "textBoxEntryContractCust21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "26dp",
                "overrides": {
                    "contactNumber": {
                        "height": "70.56%",
                        "left": "10dp",
                        "right": "1dp",
                        "top": "26dp",
                        "width": "viz.val_cleared"
                    },
                    "flxContactNumberWrapper": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxError": {
                        "height": "33dp",
                        "isVisible": false,
                        "left": "0%",
                        "right": "0dp",
                        "top": "44dp",
                        "width": "viz.val_cleared"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "32.50%",
                        "width": "5dp"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                        "left": "36.50%",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtISDCode": {
                        "width": "32%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn21.add(lblHeading21, textBoxEntryContractCust21);
            var flxColumn22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn22.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust22 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "isVisible": true
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DateOfBirth\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DateOfBirth\")",
                        "isVisible": false
                    },
                    "textBoxEntry": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCalendarDOB22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarDOB22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarDOB22.setDefaultUnit(kony.flex.DP);
            var customCalCustomerDOB = new kony.ui.CustomWidget({
                "id": "customCalCustomerDOB",
                "isVisible": true,
                "left": "0dp",
                "top": "5dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarDOB22.add(customCalCustomerDOB);
            flxColumn22.add(textBoxEntryContractCust22, flxCalendarDOB22);
            var flxColumn23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxColumn23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1dp",
                "width": "31.40%",
                "zIndex": 40
            }, {}, {});
            flxColumn23.setDefaultUnit(kony.flex.DP);
            var lblCol23 = new kony.ui.Label({
                "id": "lblCol23",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDown23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDown23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "24dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxDropDown23.setDefaultUnit(kony.flex.DP);
            var lblSelectedRows = new kony.ui.Label({
                "centerY": "49%",
                "id": "lblSelectedRows",
                "isVisible": true,
                "left": "12px",
                "right": "45dp",
                "skin": "sknlbl0h2ff0d6b13f947AD",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchDownImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxSearchDownImg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxSearchDownImg.setDefaultUnit(kony.flex.DP);
            var fontIconSearchDown01 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchDown01",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon696C73sz13px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchDownImg.add(fontIconSearchDown01);
            flxDropDown23.add(lblSelectedRows, flxSearchDownImg);
            var flxDropDownDetail23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "120px",
                "id": "flxDropDownDetail23",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "skntxtbxNormald7d9e0",
                "top": "67px",
                "zIndex": 40
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDownDetail23.setDefaultUnit(kony.flex.DP);
            var AdvancedSearchDropDown01 = new com.adminConsole.Groups.AdvancedSearchDropDown({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "AdvancedSearchDropDown01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknNormalDefault",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100,
                "overrides": {
                    "AdvancedSearchDropDown": {
                        "height": "100%"
                    },
                    "flxPopupBody": {
                        "top": "0dp"
                    },
                    "flxSearch": {
                        "isVisible": false
                    },
                    "sgmentData": {
                        "height": "120dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDropDownDetail23.add(AdvancedSearchDropDown01);
            flxColumn23.add(lblCol23, flxDropDown23, flxDropDownDetail23);
            flxRow2.add(flxColumn21, flxColumn22, flxColumn23);
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxRow3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn31.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust31 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.WireTransfer.Country\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.WireTransfer.Country\")",
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn31.add(textBoxEntryContractCust31);
            var flxColumn32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn32.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust32 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxHeading": {
                        "width": "220dp"
                    },
                    "lblHeadingText": {
                        "text": "Town/City"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": false,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "placeholder": "Town/City"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn32.add(textBoxEntryContractCust32);
            var flxColumn33 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn33.setDefaultUnit(kony.flex.DP);
            var textBoxEntryContractCust33 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryContractCust33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxHeading": {
                        "width": "220dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.zipcode\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": false,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.common.zipcode\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn33.add(textBoxEntryContractCust33);
            flxRow3.add(flxColumn31, flxColumn32, flxColumn33);
            flxCustomerDetailsContainer.add(flxRow1, flxRow2, flxRow3);
            var flxCustomerAdvSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "flxCustomerAdvSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustomerAdvSearch.setDefaultUnit(kony.flex.DP);
            var flxShowHideAdvSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxShowHideAdvSearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "40%"
            }, {}, {});
            flxShowHideAdvSearch.setDefaultUnit(kony.flex.DP);
            var btnShowHideAdvSearch = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnShowHideAdvSearch",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLato117eb013Px",
                "text": "Show Advanced Search",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImgArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxImgArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxImgArrow.setDefaultUnit(kony.flex.DP);
            var fonticonrightarrowSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonrightarrowSearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgArrow.add(fonticonrightarrowSearch);
            flxShowHideAdvSearch.add(btnShowHideAdvSearch, flxImgArrow);
            var btnSearchCustomers = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30dp",
                "id": "btnSearchCustomers",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCH\")",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnClearAll = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "30dp",
                "id": "btnClearAll",
                "isVisible": true,
                "right": 150,
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "text": "CLEAR ALL",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var lblSearchSeparator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSearchSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperatorE0E3E6",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerAdvSearch.add(flxShowHideAdvSearch, btnSearchCustomers, btnClearAll, lblSearchSeparator);
            var flxAddedCustomerInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddedCustomerInfo",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddedCustomerInfo.setDefaultUnit(kony.flex.DP);
            var flxSelectedCustomersTags = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectedCustomersTags",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelectedCustomersTags.setDefaultUnit(kony.flex.DP);
            var flxSelectedCustomersArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSelectedCustomersArrow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxSelectedCustomersArrow.setDefaultUnit(kony.flex.DP);
            var lblSelectCustomers = new kony.ui.Label({
                "id": "lblSelectCustomers",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Selected Customers",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconArrow = new kony.ui.Label({
                "id": "lblIconArrow",
                "isVisible": true,
                "left": "10px",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 200
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedCustomersArrow.add(lblSelectCustomers, lblIconArrow);
            var flxSearchFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchFilter.setDefaultUnit(kony.flex.DP);
            flxSearchFilter.add();
            var flxFilterTag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxFilterTag",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFbor1px003E75",
                "top": "30dp",
                "width": "170px",
                "zIndex": 1
            }, {}, {});
            flxFilterTag.setDefaultUnit(kony.flex.DP);
            var lblTagName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblTagName",
                "isVisible": true,
                "left": "10px",
                "skin": "skn003E75Lato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblCsrAssist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCross = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCross",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxCross.setDefaultUnit(kony.flex.DP);
            var lblIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCross",
                "isVisible": true,
                "right": "7dp",
                "skin": "sknIcoMoon10pxCursor003E75",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCross.add(lblIconCross);
            flxFilterTag.add(lblTagName, flxCross);
            flxSelectedCustomersTags.add(flxSelectedCustomersArrow, flxSearchFilter, flxFilterTag);
            var flxSelectedCustomerInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180dp",
                "id": "flxSelectedCustomerInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSelectedCustomerInfo.setDefaultUnit(kony.flex.DP);
            var flxCustomerDetailsCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "170px",
                "id": "flxCustomerDetailsCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "10dp"
            }, {}, {});
            flxCustomerDetailsCont.setDefaultUnit(kony.flex.DP);
            var flxCheckboxCustomerInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCheckboxCustomerInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "18dp",
                "width": "15dp"
            }, {}, {});
            flxCheckboxCustomerInfo.setDefaultUnit(kony.flex.DP);
            var imgCheckbox = new kony.ui.Image2({
                "height": "100%",
                "id": "imgCheckbox",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxCustomerInfo.add(imgCheckbox);
            var flxContractDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "95%"
            }, {}, {});
            flxContractDetails.setDefaultUnit(kony.flex.DP);
            var flxContractName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContractName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxContractName.setDefaultUnit(kony.flex.DP);
            var lblContractName = new kony.ui.Label({
                "id": "lblContractName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoRegular192B4516px",
                "text": "Temenos Global Limited",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContractId = new kony.ui.Label({
                "id": "lblContractId",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "Label",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContractName.add(lblContractName, lblContractId);
            var flxCustomerInfoRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerInfoRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxCustomerInfoRow1.setDefaultUnit(kony.flex.DP);
            var flxColumnInfo11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumnInfo11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "27%",
                "zIndex": 1
            }, {}, {});
            flxColumnInfo11.setDefaultUnit(kony.flex.DP);
            var lblHeadingInfo11 = new kony.ui.Label({
                "id": "lblHeadingInfo11",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "CUSTOMER ID",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataInfo11 = new kony.ui.Label({
                "id": "lblDataInfo11",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "8px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumnInfo11.add(lblHeadingInfo11, lblDataInfo11);
            var flxColumnInfo12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumnInfo12",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxColumnInfo12.setDefaultUnit(kony.flex.DP);
            var lblHeadingInfo12 = new kony.ui.Label({
                "id": "lblHeadingInfo12",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "ADDRESS",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataInfo12 = new kony.ui.Label({
                "id": "lblDataInfo12",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "8px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumnInfo12.add(lblHeadingInfo12, lblDataInfo12);
            var flxColumnInfo13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumnInfo13",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "66%",
                "isModalContainer": false,
                "right": "30px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxColumnInfo13.setDefaultUnit(kony.flex.DP);
            var lblHeadingInfo13 = new kony.ui.Label({
                "id": "lblHeadingInfo13",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "INDUSTRY",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataInfo13 = new kony.ui.Label({
                "id": "lblDataInfo13",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "8px",
                "width": "97%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumnInfo13.add(lblHeadingInfo13, lblDataInfo13);
            flxCustomerInfoRow1.add(flxColumnInfo11, flxColumnInfo12, flxColumnInfo13);
            var flxCustomerInfoRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerInfoRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxCustomerInfoRow2.setDefaultUnit(kony.flex.DP);
            var flxColumnInfo21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumnInfo21",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "27%",
                "zIndex": 1
            }, {}, {});
            flxColumnInfo21.setDefaultUnit(kony.flex.DP);
            var lblHeadingInfo21 = new kony.ui.Label({
                "id": "lblHeadingInfo21",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "EMAIL ID",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataInfo21 = new kony.ui.Label({
                "id": "lblDataInfo21",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "8px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumnInfo21.add(lblHeadingInfo21, lblDataInfo21);
            var flxColumnInfo22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumnInfo22",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxColumnInfo22.setDefaultUnit(kony.flex.DP);
            var lblHeadingInfo22 = new kony.ui.Label({
                "id": "lblHeadingInfo22",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "PHONE NUMBER",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataInfo22 = new kony.ui.Label({
                "id": "lblDataInfo22",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "8px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumnInfo22.add(lblHeadingInfo22, lblDataInfo22);
            flxCustomerInfoRow2.add(flxColumnInfo21, flxColumnInfo22);
            flxContractDetails.add(flxContractName, flxCustomerInfoRow1, flxCustomerInfoRow2);
            flxCustomerDetailsCont.add(flxCheckboxCustomerInfo, flxContractDetails);
            flxSelectedCustomerInfo.add(flxCustomerDetailsCont);
            flxAddedCustomerInfo.add(flxSelectedCustomersTags, flxSelectedCustomerInfo);
            var flxCustomerSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerSearchResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchResults.setDefaultUnit(kony.flex.DP);
            var flxNoCustomersSearched = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoCustomersSearched",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxNoCustomersSearched.setDefaultUnit(kony.flex.DP);
            var imgContractCustomersSearch = new kony.ui.Image2({
                "bottom": "20dp",
                "centerX": "50%",
                "height": "70dp",
                "id": "imgContractCustomersSearch",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectcustomer.png",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersSearched = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersSearched",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Search for a Customer to see the results",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCustomersSearched.add(imgContractCustomersSearch, lblNoCustomersSearched);
            var flxRelatedCustomerSegContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRelatedCustomerSegContracts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRelatedCustomerSegContracts.setDefaultUnit(kony.flex.DP);
            var segRelatedCustomersContract = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "data": [{
                    "btnRelation": "",
                    "imgCheckbox": "",
                    "imgRadioButton": "",
                    "lblContractId": "",
                    "lblContractInfo": "",
                    "lblContractName": "",
                    "lblContractRelation": "",
                    "lblData1": "",
                    "lblData2": "",
                    "lblData3": "",
                    "lblHeading1": "",
                    "lblHeading2": "",
                    "lblHeading3": "",
                    "lblIconRemove": "",
                    "lblLine": "",
                    "lblPrimaryText": ""
                }],
                "groupCells": false,
                "id": "segRelatedCustomersContract",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxRelatedCustomerList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnRelation": "btnRelation",
                    "flxCheckbox": "flxCheckbox",
                    "flxColumn1": "flxColumn1",
                    "flxColumn2": "flxColumn2",
                    "flxColumn3": "flxColumn3",
                    "flxContractDetails": "flxContractDetails",
                    "flxContractName": "flxContractName",
                    "flxContractRelation": "flxContractRelation",
                    "flxDetails": "flxDetails",
                    "flxLeftDetailsCont": "flxLeftDetailsCont",
                    "flxPrimaryBtn": "flxPrimaryBtn",
                    "flxRelatedCustomerList": "flxRelatedCustomerList",
                    "flxRelatedCustomerRow": "flxRelatedCustomerRow",
                    "flxRemoveContract": "flxRemoveContract",
                    "flxRightActionCont": "flxRightActionCont",
                    "flxRightDetailsCont": "flxRightDetailsCont",
                    "imgCheckbox": "imgCheckbox",
                    "imgRadioButton": "imgRadioButton",
                    "lblContractId": "lblContractId",
                    "lblContractInfo": "lblContractInfo",
                    "lblContractName": "lblContractName",
                    "lblContractRelation": "lblContractRelation",
                    "lblData1": "lblData1",
                    "lblData2": "lblData2",
                    "lblData3": "lblData3",
                    "lblHeading1": "lblHeading1",
                    "lblHeading2": "lblHeading2",
                    "lblHeading3": "lblHeading3",
                    "lblIconRemove": "lblIconRemove",
                    "lblLine": "lblLine",
                    "lblPrimaryText": "lblPrimaryText"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRelatedCustomerSegContracts.add(segRelatedCustomersContract);
            var lblRelatedCustSubHeadingContract = new kony.ui.Label({
                "id": "lblRelatedCustSubHeadingContract",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Search results for \"KEYWORD\" (count)",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerSearchResults.add(flxNoCustomersSearched, flxRelatedCustomerSegContracts, lblRelatedCustSubHeadingContract);
            flxCustomerSearchFilters.add(flxCustomerDetailsContainer, flxCustomerAdvSearch, flxAddedCustomerInfo, flxCustomerSearchResults);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "lFbox0he6e13dfc3b44eAD",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var flxCommonButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "80px",
                "id": "flxCommonButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "lFbox0he6e13dfc3b44eAD",
                "zIndex": 1
            }, {}, {});
            flxCommonButton.setDefaultUnit(kony.flex.DP);
            var commonButtonsContractSearchCust = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtonsContractSearchCust",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "btnCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "text": "SAVE AND SELECT ANOTHER"
                    },
                    "btnSave": {
                        "text": "SELECT"
                    },
                    "commonButtons": {
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 20
                    },
                    "flxRightButtons": {
                        "right": "10px",
                        "width": "355px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButton.add(commonButtonsContractSearchCust);
            flxPopUpButtons.add(flxCommonButton);
            flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxCustomerSearchFilters, flxPopUpButtons);
            flxCustomerSearchContainer.add(flxSearchNoResultPage, flxToastMessageSearch, flxPopUp);
            var flxLoading2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "95%",
                "id": "flxLoading2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknLoadingBlur",
                "top": "20px",
                "width": "70%",
                "zIndex": 150
            }, {}, {});
            flxLoading2.setDefaultUnit(kony.flex.DP);
            var flxImageCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageCont.setDefaultUnit(kony.flex.DP);
            var imageLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imageLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageCont.add(imageLoading);
            flxLoading2.add(flxImageCont);
            flxCustomerSearchPopUp.add(flxCustomerSearchContainer, flxLoading2);
            var tooltipEnroll = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "tooltipEnroll",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "70dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "77dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "70dp",
                        "top": "77dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "left": "0dp",
                        "minHeight": "70dp",
                        "top": "9dp",
                        "width": "210dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "10dp",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "left": "5dp",
                        "top": "10dp",
                        "width": "195dp"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "height": "10dp",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "17dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMain.add(flxLeftPannel, flxRightPanel, flxEnrollCustConfirmationPopup, flxContractDetailsPopup, flxBulkUpdateFeaturesPopup, flxCustomerSearchPopUp, tooltipEnroll);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmEnrollCustomer,
            "enabledForIdleTimeout": true,
            "id": "frmEnrollCustomer",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_c753e33d1f7f4108a431c91a104fa135(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});