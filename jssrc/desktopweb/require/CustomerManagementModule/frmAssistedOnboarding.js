define("CustomerManagementModule/frmAssistedOnboarding", function() {
    return function(controller) {
        function addWidgetsfrmAssistedOnboarding() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.AssistOnBoarding\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "10px",
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "100px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "left": "0px",
                        "text": "ALERTS"
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumb.add(breadcrumbs);
            var flxAddMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxAddMainContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "30px",
                "zIndex": 1
            }, {}, {});
            flxAddMainContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0%",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxPersonalDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPersonalDetails.setDefaultUnit(kony.flex.DP);
            var flxPersonalDetailContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalDetailContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxPersonalDetailContainer.setDefaultUnit(kony.flex.DP);
            var btnPersonalDetails = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnPersonalDetails",
                "isVisible": true,
                "left": "20px",
                "skin": "Btn000000font14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.CustomerInfo\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow2 = new kony.ui.Label({
                "bottom": "0dp",
                "id": "fontIconRightArrow2",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonalDetailContainer.add(btnPersonalDetails, fontIconRightArrow2);
            var flxPersonalDetailSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxPersonalDetailSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxPersonalDetailSeprator.setDefaultUnit(kony.flex.DP);
            var flxoptionSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator1.add();
            flxPersonalDetailSeprator.add(flxoptionSeperator1);
            flxPersonalDetails.add(flxPersonalDetailContainer, flxPersonalDetailSeprator);
            var flxIdInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxIdInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxIdInfo.setDefaultUnit(kony.flex.DP);
            var flxIdInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxIdInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxIdInfoContainer.setDefaultUnit(kony.flex.DP);
            var btnIdInfo = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnIdInfo",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.idInformation\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow3 = new kony.ui.Label({
                "bottom": "0dp",
                "id": "fontIconRightArrow3",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIdInfoContainer.add(btnIdInfo, fontIconRightArrow3);
            var lblAddOptional3 = new kony.ui.Label({
                "id": "lblAddOptional3",
                "isVisible": false,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "10px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxIdInfoSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxIdInfoSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxIdInfoSeparator.setDefaultUnit(kony.flex.DP);
            var flxAddSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator4.setDefaultUnit(kony.flex.DP);
            flxAddSeperator4.add();
            flxIdInfoSeparator.add(flxAddSeperator4);
            flxIdInfo.add(flxIdInfoContainer, lblAddOptional3, flxIdInfoSeparator);
            var flxTermsAndCond = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTermsAndCond",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTermsAndCond.setDefaultUnit(kony.flex.DP);
            var flxTermAndConditionConatiner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTermAndConditionConatiner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxTermAndConditionConatiner.setDefaultUnit(kony.flex.DP);
            var btnTermsAndCond = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnTermsAndCond",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblDetailsServiceTACHeader\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow4 = new kony.ui.Label({
                "bottom": "0dp",
                "id": "fontIconRightArrow4",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTermAndConditionConatiner.add(btnTermsAndCond, fontIconRightArrow4);
            var lblAddOptional1 = new kony.ui.Label({
                "id": "lblAddOptional1",
                "isVisible": false,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "10px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTermAndCondiationSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTermAndCondiationSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxTermAndCondiationSeprator.setDefaultUnit(kony.flex.DP);
            var flxAddSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator2.setDefaultUnit(kony.flex.DP);
            flxAddSeperator2.add();
            flxTermAndCondiationSeprator.add(flxAddSeperator2);
            flxTermsAndCond.add(flxTermAndConditionConatiner, lblAddOptional1, flxTermAndCondiationSeprator);
            var flxAddUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddUsers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddUsers.setDefaultUnit(kony.flex.DP);
            var flxAddUserContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddUserContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAddUserContainer.setDefaultUnit(kony.flex.DP);
            var btnAddUsers = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnAddUsers",
                "isVisible": true,
                "left": "20dp",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ASSIGNUSERS\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRightArrow5 = new kony.ui.Label({
                "bottom": "0dp",
                "id": "fontIconRightArrow5",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddUserContainer.add(btnAddUsers, fontIconRightArrow5);
            var lblAddOptional2 = new kony.ui.Label({
                "id": "lblAddOptional2",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "10px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddUserSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddUserSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAddUserSeparator.setDefaultUnit(kony.flex.DP);
            var flxAddSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator3.setDefaultUnit(kony.flex.DP);
            flxAddSeperator3.add();
            flxAddUserSeparator.add(flxAddSeperator3);
            flxAddUsers.add(flxAddUserContainer, lblAddOptional2, flxAddUserSeparator);
            flxOptions.add(flxPersonalDetails, flxIdInfo, flxTermsAndCond, flxAddUsers);
            var flxVerticalTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxVerticalTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230px",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 2
            }, {}, {});
            flxVerticalTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxVerticalTabsSeperator.add();
            var flxAddOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSection.setDefaultUnit(kony.flex.DP);
            var flxAddIdInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80px",
                "clipBounds": true,
                "id": "flxAddIdInfo",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAddIdInfo.setDefaultUnit(kony.flex.DP);
            var lblSubTitle = new kony.ui.Label({
                "id": "lblSubTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "text": "IDENTITY (ID)  INFORMATION",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLineSeperator = new kony.ui.Label({
                "bottom": "20px",
                "height": "1px",
                "id": "lblLineSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxIdentityInformation = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "280dp",
                "horizontalScrollIndicator": true,
                "id": "flxIdentityInformation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "24dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxIdentityInformation.setDefaultUnit(kony.flex.DP);
            var flxIDSelect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIDSelect",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0c2f726f5837246",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxIDSelect.setDefaultUnit(kony.flex.DP);
            var flxTextListBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxTextListBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTextListBox.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e922ecfc0a6e44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "FlexContainer0e922ecfc0a6e44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 3
            }, {}, {
                "hoverSkin": "sknGreyHover"
            });
            FlexContainer0e922ecfc0a6e44.setDefaultUnit(kony.flex.DP);
            var ListBoxSelectId = new kony.ui.ListBox({
                "focusSkin": "sknlstbx13abebHover",
                "height": "40px",
                "id": "ListBoxSelectId",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select ID", "Select ID"],
                    ["Drivers Licence", "Drivers Licence"],
                    ["Passport", "Passport"]
                ],
                "right": "20dp",
                "selectedKey": "Select ID",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            FlexContainer0e922ecfc0a6e44.add(ListBoxSelectId);
            flxTextListBox.add(FlexContainer0e922ecfc0a6e44);
            var flxIdSearchInlineError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxIdSearchInlineError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "63dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxIdSearchInlineError.setDefaultUnit(kony.flex.DP);
            var lblIdSearchBarErrorMsg = new kony.ui.Label({
                "id": "lblIdSearchBarErrorMsg",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknLabelRed",
                "text": "Should select an ID",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIdSearchBarErrorIcon = new kony.ui.Label({
                "id": "lblIdSearchBarErrorIcon",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIdSearchInlineError.add(lblIdSearchBarErrorMsg, lblIdSearchBarErrorIcon);
            flxIDSelect.add(flxTextListBox, flxIdSearchInlineError);
            var flxDriversLicence = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "280dp",
                "id": "flxDriversLicence",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknBordereeeeee",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDriversLicence.setDefaultUnit(kony.flex.DP);
            var flxComponentTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxComponentTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknbackGroundeeeeee",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxComponentTopBar.setDefaultUnit(kony.flex.DP);
            var lblDriversLicenceIDTopBar = new kony.ui.Label({
                "id": "lblDriversLicenceIDTopBar",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknTextDarkBlueTitle",
                "text": "Drivers Licence",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeleteDriversLicence = new kony.ui.Label({
                "id": "lblDeleteDriversLicence",
                "isVisible": true,
                "right": "20dp",
                "skin": "lblIconDarkBlueBold",
                "text": "",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxComponentTopBar.add(lblDriversLicenceIDTopBar, lblDeleteDriversLicence);
            var flxComponentRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxComponentRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "77dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxComponentRow1.setDefaultUnit(kony.flex.DP);
            var FlexGroup0de3a3adc676445 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0de3a3adc676445",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            FlexGroup0de3a3adc676445.setDefaultUnit(kony.flex.DP);
            var DriversCardNumber = new com.adminConsole.common.dataEntry({
                "height": "85dp",
                "id": "DriversCardNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "height": "85dp",
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexGroup0de3a3adc676445.add(DriversCardNumber);
            var flxComponentCol2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxComponentCol2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxComponentCol2.setDefaultUnit(kony.flex.DP);
            var CopyLabel0c178d593d99d43 = new kony.ui.Label({
                "id": "CopyLabel0c178d593d99d43",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Issued Date",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDriversIssueDateErrorBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDriversIssueDateErrorBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBordereeeeee",
                "top": "25dp",
                "zIndex": 1
            }, {}, {});
            flxDriversIssueDateErrorBorder.setDefaultUnit(kony.flex.DP);
            var calDriversLicenceIssueDate = new kony.ui.CustomWidget({
                "id": "calDriversLicenceIssueDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxDriversIssueDateErrorBorder.add(calDriversLicenceIssueDate);
            var flxDriversIssueDateError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDriversIssueDateError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDriversIssueDateError.setDefaultUnit(kony.flex.DP);
            var lblDriversIssueDateError = new kony.ui.Label({
                "id": "lblDriversIssueDateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Invalid Input",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblIdSearchBarErrorIcon0ca80ccb8246242 = new kony.ui.Label({
                "id": "CopylblIdSearchBarErrorIcon0ca80ccb8246242",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDriversIssueDateError.add(lblDriversIssueDateError, CopylblIdSearchBarErrorIcon0ca80ccb8246242);
            flxComponentCol2.add(CopyLabel0c178d593d99d43, flxDriversIssueDateErrorBorder, flxDriversIssueDateError);
            var flxComponentCol3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxComponentCol3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxComponentCol3.setDefaultUnit(kony.flex.DP);
            var CopyLabel0fa5854bcb96d4a = new kony.ui.Label({
                "id": "CopyLabel0fa5854bcb96d4a",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Expiry Date",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDriversExpiryDateErrorBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDriversExpiryDateErrorBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBordereeeeee",
                "top": "25dp",
                "zIndex": 1
            }, {}, {});
            flxDriversExpiryDateErrorBorder.setDefaultUnit(kony.flex.DP);
            var calDriversLicenceExpiryDate = new kony.ui.CustomWidget({
                "id": "calDriversLicenceExpiryDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxDriversExpiryDateErrorBorder.add(calDriversLicenceExpiryDate);
            var flxDriversExpiryDateError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDriversExpiryDateError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDriversExpiryDateError.setDefaultUnit(kony.flex.DP);
            var lblDriversExpiryDateError = new kony.ui.Label({
                "id": "lblDriversExpiryDateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Invalid Input",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblIdSearchBarErrorIcon0g62b4a7cae9f4f = new kony.ui.Label({
                "id": "CopylblIdSearchBarErrorIcon0g62b4a7cae9f4f",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDriversExpiryDateError.add(lblDriversExpiryDateError, CopylblIdSearchBarErrorIcon0g62b4a7cae9f4f);
            flxComponentCol3.add(CopyLabel0fa5854bcb96d4a, flxDriversExpiryDateErrorBorder, flxDriversExpiryDateError);
            flxComponentRow1.add(FlexGroup0de3a3adc676445, flxComponentCol2, flxComponentCol3);
            var flxComponentRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxComponentRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "167dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxComponentRow2.setDefaultUnit(kony.flex.DP);
            var FlexGroup0i3aa37a56ae04f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0i3aa37a56ae04f",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            FlexGroup0i3aa37a56ae04f.setDefaultUnit(kony.flex.DP);
            var DriversIssuedByState = new com.adminConsole.common.dataEntry({
                "height": "90dp",
                "id": "DriversIssuedByState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "height": "90dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexGroup0i3aa37a56ae04f.add(DriversIssuedByState);
            flxComponentRow2.add(FlexGroup0i3aa37a56ae04f);
            flxDriversLicence.add(flxComponentTopBar, flxComponentRow1, flxComponentRow2);
            var flxPassportID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "260dp",
                "id": "flxPassportID",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknBordereeeeee",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPassportID.setDefaultUnit(kony.flex.DP);
            var CopyflxComponentTopBar0d07e11ae475c48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyflxComponentTopBar0d07e11ae475c48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknbackGroundeeeeee",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxComponentTopBar0d07e11ae475c48.setDefaultUnit(kony.flex.DP);
            var lblPassportTopBar = new kony.ui.Label({
                "id": "lblPassportTopBar",
                "isVisible": true,
                "left": "5%",
                "skin": "sknTextDarkBlueTitle",
                "text": "Passport",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeletePassport = new kony.ui.Label({
                "id": "lblDeletePassport",
                "isVisible": true,
                "right": "20dp",
                "skin": "lblIconDarkBlueBold",
                "text": "",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxComponentTopBar0d07e11ae475c48.add(lblPassportTopBar, lblDeletePassport);
            var CopyflxComponentRow0c6e176dd27d44d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyflxComponentRow0c6e176dd27d44d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "77dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            CopyflxComponentRow0c6e176dd27d44d.setDefaultUnit(kony.flex.DP);
            var FlexGroup0b25525290e6849 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0b25525290e6849",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            FlexGroup0b25525290e6849.setDefaultUnit(kony.flex.DP);
            var PassportCardNumber = new com.adminConsole.common.dataEntry({
                "height": "90dp",
                "id": "PassportCardNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "height": "90dp",
                        "right": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexGroup0b25525290e6849.add(PassportCardNumber);
            var CopyflxComponentCol0c7f6e9822c804a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyflxComponentCol0c7f6e9822c804a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            CopyflxComponentCol0c7f6e9822c804a.setDefaultUnit(kony.flex.DP);
            var CopyLabel0bc27fcd7b61840 = new kony.ui.Label({
                "id": "CopyLabel0bc27fcd7b61840",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Issued Date",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPassportIssueDateErrorBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPassportIssueDateErrorBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBordereeeeee",
                "top": "25dp",
                "zIndex": 1
            }, {}, {});
            flxPassportIssueDateErrorBorder.setDefaultUnit(kony.flex.DP);
            var calPassportIssueDate = new kony.ui.CustomWidget({
                "id": "calPassportIssueDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxPassportIssueDateErrorBorder.add(calPassportIssueDate);
            var flxPassportIssueDateError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPassportIssueDateError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPassportIssueDateError.setDefaultUnit(kony.flex.DP);
            var lblPassportIssueDateError = new kony.ui.Label({
                "id": "lblPassportIssueDateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Invalid Input",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblIdSearchBarErrorIcon0e5de96a7cf414c = new kony.ui.Label({
                "id": "CopylblIdSearchBarErrorIcon0e5de96a7cf414c",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPassportIssueDateError.add(lblPassportIssueDateError, CopylblIdSearchBarErrorIcon0e5de96a7cf414c);
            CopyflxComponentCol0c7f6e9822c804a.add(CopyLabel0bc27fcd7b61840, flxPassportIssueDateErrorBorder, flxPassportIssueDateError);
            var CopyflxComponentCol0ed644d7d8da941 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyflxComponentCol0ed644d7d8da941",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            CopyflxComponentCol0ed644d7d8da941.setDefaultUnit(kony.flex.DP);
            var CopyLabel0c33affcf6a184d = new kony.ui.Label({
                "id": "CopyLabel0c33affcf6a184d",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Expiry Date",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPassportExpiryDateErrorBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPassportExpiryDateErrorBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBordereeeeee",
                "top": "25dp",
                "zIndex": 1
            }, {}, {});
            flxPassportExpiryDateErrorBorder.setDefaultUnit(kony.flex.DP);
            var calPassportExpiryDate = new kony.ui.CustomWidget({
                "id": "calPassportExpiryDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "right",
                "rangeType": null,
                "resetData": "End Date",
                "type": "single",
                "uniqueId": "calEndDate",
                "value": ""
            });
            flxPassportExpiryDateErrorBorder.add(calPassportExpiryDate);
            var flxPassportExpiryDateError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPassportExpiryDateError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPassportExpiryDateError.setDefaultUnit(kony.flex.DP);
            var lblPassportExpiryDateError = new kony.ui.Label({
                "id": "lblPassportExpiryDateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Invalid Input",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblIdSearchBarErrorIcon0b55c84fccaa64b = new kony.ui.Label({
                "id": "CopylblIdSearchBarErrorIcon0b55c84fccaa64b",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPassportExpiryDateError.add(lblPassportExpiryDateError, CopylblIdSearchBarErrorIcon0b55c84fccaa64b);
            CopyflxComponentCol0ed644d7d8da941.add(CopyLabel0c33affcf6a184d, flxPassportExpiryDateErrorBorder, flxPassportExpiryDateError);
            CopyflxComponentRow0c6e176dd27d44d.add(FlexGroup0b25525290e6849, CopyflxComponentCol0c7f6e9822c804a, CopyflxComponentCol0ed644d7d8da941);
            var CopyflxComponentRow0aada287c91e748 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyflxComponentRow0aada287c91e748",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "167dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            CopyflxComponentRow0aada287c91e748.setDefaultUnit(kony.flex.DP);
            var FlexGroup0feaf5ce1ed5c4e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0feaf5ce1ed5c4e",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            FlexGroup0feaf5ce1ed5c4e.setDefaultUnit(kony.flex.DP);
            var PassportIssuedByState = new com.adminConsole.common.dataEntry({
                "height": "90dp",
                "id": "PassportIssuedByState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "height": "90dp",
                        "left": "0dp",
                        "right": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexGroup0feaf5ce1ed5c4e.add(PassportIssuedByState);
            var FlexGroup0a04a6ddf639745 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0a04a6ddf639745",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            FlexGroup0a04a6ddf639745.setDefaultUnit(kony.flex.DP);
            var PassportIssuedByCountry = new com.adminConsole.common.dataEntry({
                "height": "90dp",
                "id": "PassportIssuedByCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "height": "90dp",
                        "right": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexGroup0a04a6ddf639745.add(PassportIssuedByCountry);
            CopyflxComponentRow0aada287c91e748.add(FlexGroup0feaf5ce1ed5c4e, FlexGroup0a04a6ddf639745);
            flxPassportID.add(CopyflxComponentTopBar0d07e11ae475c48, CopyflxComponentRow0c6e176dd27d44d, CopyflxComponentRow0aada287c91e748);
            flxIdentityInformation.add(flxIDSelect, flxDriversLicence, flxPassportID);
            flxAddIdInfo.add(lblSubTitle, lblLineSeperator, flxIdentityInformation);
            var flxTermsAndCoditions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxTermsAndCoditions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxTermsAndCoditions.setDefaultUnit(kony.flex.DP);
            var segTermsAndCond = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblSeperator": ".",
                    "lblSubHeading": "SUB - HEADING 1",
                    "rtxTermsAndCond": "You authorize, XYZ Bank, to:\n\n\n1. Pull a credit report on you and co-applicant (if any) in order to make a credit decision.\n\n\n2. Use the information on this application and any other information we or our affiliates have about you to determine your ability to pay, as required by federal law.\n\n\n3. Verify, at our option and for your benefit, your application information.\n\n\n4. Contact you or receive a detailed message or messages at the telephone, email or direct mail contact data you provided above for purposes of fulfilling this inquiry about automobile loan financing even if you have previously registered on a Do No Call registry or requested XYZ Bank not to send marketing information to you by email and/or direct mail.\n"
                }, {
                    "lblSeperator": ".",
                    "lblSubHeading": "SUB - HEADING 2",
                    "rtxTermsAndCond": "By giving us your phone number and submitting this application, you acknowledge that,  1. You've reviewed any special disclosure that applies in your state: ME, CA, NY, RI, OH, VT and WI residents  2. All information you've submitted is true and correct and is given to obtain credit from us  3. Co-applicant (if any) has authorized you to submit this application on his or her behalf.  4. You and co-applicant (if any) are of legal contracting age.  5. Credit approval, Annual Percentage Rate (APR), and credit terms are based on our review of each applicant’s application information and credit report.  6. You understand we collect your individual income information to determine your individual ability to pay. Examples may include income earned from your employment (salary, overtime, bonus), retirement/pension benefits, and rental properties. Student loans are not acceptable sources of income. Alimony, child support, or separate maintenance income need not be revealed if you don't wish it to be considered as a basis for repayment.And you agree that XYZ Bank, N.A. may,  Use automatic telephone dialling systems and prerecorded voice messages in connection with calls or texts made to any telephone number you provide even if the telephone number is assigned to a cellular/mobile telephone service or other service for which the called party is charged according to their normal text messaging rates.    Privacy  All financial institutions are required by federal law to obtain, verify and record information that identifies each customer before opening an account with us. When you open an account with us, we will ask you for your name, address and other information that will allow us to identify you, such as Social Security number and date of birth. In addition, if the account application is approved, do we have your permission to open your account prior to sending you a copy of our XYZ Bank US Consumer Privacy Notice, which will be sent with your customary account disclosures?    Electronic Consumer Information consent  To receive the Consumer Information electronically, scroll to read the complete disclosure and then select I consent on this page.    As part of your online loan application, XYZ bank., is required by law to provide you with certain Consumer Information. You have the right to receive the Consumer Information on paper. With your consent, we may provide the Consumer Information to you electronically instead. If you prefer to receive the Consumer Information on paper, you’ll need to visit any XYZ banking center to apply in person.    Your consent only applies to this loan application and the documents listed here. You aren't required to consent to receive information electronically to complete this application. If you’d like to withdraw your consent after consenting to receive information electronically, or if after receiving electronic documents you would like paper copies, you may do so in the Application Status Center or contact us at 1.888.266.1034 and we will provide you with paper copies at no charge. You can also update your contact information with us by calling that same telephone number. Consent withdrawal won’t apply to any actions already taken or initiated in reliance on your consent.    Type of electronic Consumer Information you may receive:  ●     Credit Score Notice, if your application is approved  ●     Adverse Action Letter, if your application is declined  ●     Any other information related to your application"
                }],
                "groupCells": false,
                "id": "segTermsAndCond",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTAndC",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxTAndC": "flxTAndC",
                    "flxTAndCHeader": "flxTAndCHeader",
                    "lblSeperator": "lblSeperator",
                    "lblSubHeading": "lblSubHeading",
                    "rtxTermsAndCond": "rtxTermsAndCond"
                },
                "width": "99%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxTermsAndCond = new kony.ui.RichText({
                "bottom": "10dp",
                "id": "rtxTermsAndCond",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "right": "10px",
                "skin": "sknlRichTextLatoRegular",
                "text": "You authorize, XYZ Bank, to:\\n\n\n1. Pull a credit report on you and co-applicant (if any) in order to make a credit decision.\\n\n\n2. Use the information on this application and any other information we or our affiliates have about you to determine your ability to pay, as required by federal law.\\n\n\n3. Verify, at our option and for your benefit, your application information.\\n\n\n4. Contact you or receive a detailed message or messages at the telephone, email or direct mail contact data you provided above for purposes of fulfilling this inquiry about automobile loan financing even if you have previously registered on a Do No Call registry or requested XYZ Bank not to send marketing information to you by email and/or direct mail.\\n",
                "top": "35dp",
                "width": "99%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var chbxConsent = new kony.ui.CheckBoxGroup({
                "height": "40dp",
                "id": "chbxConsent",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["cbg1", "I agree"]
                ],
                "skin": "slCheckBoxGroup0h1860dff06d344",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoChbx = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoChbx",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoChbx.setDefaultUnit(kony.flex.DP);
            var lblNoChbxIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoChbxIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "2dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 1],
                "paddingInPixel": false
            }, {});
            var lblNoChbxError = new kony.ui.Label({
                "height": "100%",
                "id": "lblNoChbxError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.TandCCheck\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 2],
                "paddingInPixel": false
            }, {});
            flxNoChbx.add(lblNoChbxIcon, lblNoChbxError);
            flxTermsAndCoditions.add(segTermsAndCond, rtxTermsAndCond, chbxConsent, flxNoChbx);
            var flxAddUsersButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddUsersButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddUsersButtons.setDefaultUnit(kony.flex.DP);
            var flxRightButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxRightButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxRightButtonsSeperator.add();
            var btnAddUsersCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddUsersCancel",
                "isVisible": true,
                "left": "20dp",
                "right": "29.50%",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxAddPermissionRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddPermissionRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddUsersSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddUsersSave",
                "isVisible": true,
                "right": "5px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddPermissionRightButtons.add(btnAddUsersSave);
            flxAddUsersButtons.add(flxRightButtonsSeperator, btnAddUsersCancel, flxAddPermissionRightButtons);
            var flxDetailContentContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxDetailContentContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxDetailContentContainer.setDefaultUnit(kony.flex.DP);
            var lblPersonalInfo = new kony.ui.Label({
                "id": "lblPersonalInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblSubTitle\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPersonalInfoLine = new kony.ui.Label({
                "bottom": "20px",
                "height": "1px",
                "id": "lblPersonalInfoLine",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknSeparator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "55px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var flxFirstName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxFirstName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 1
            }, {}, {});
            flxFirstName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblNameCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.FirstNameMissing\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            flxFirstName.add(lblName, lblNameCount, tbxNameValue, flxNoNameError);
            var flxMiddleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxMiddleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 1
            }, {}, {});
            flxMiddleName.setDefaultUnit(kony.flex.DP);
            var lblMiddleName = new kony.ui.Label({
                "id": "lblMiddleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxMiddleName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxMiddleName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 35,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblMiddleNameCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMiddleNameCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMiddleName.add(lblMiddleName, tbxMiddleName, lblMiddleNameCount);
            var flxLastName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxLastName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 1
            }, {}, {});
            flxLastName.setDefaultUnit(kony.flex.DP);
            var lblLastName = new kony.ui.Label({
                "id": "lblLastName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxLastName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxLastName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoLastNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoLastNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoLastNameError.setDefaultUnit(kony.flex.DP);
            var lblNoLastNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoLastNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoLastNameError = new kony.ui.Label({
                "id": "lblNoLastNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.LastNameMissing\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLastNameError.add(lblNoLastNameErrorIcon, lblNoLastNameError);
            var lblLastNameCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLastNameCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastName.add(lblLastName, tbxLastName, flxNoLastNameError, lblLastNameCount);
            flxFirstRow.add(flxFirstName, flxMiddleName, flxLastName);
            var flxSecondRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxSecondRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "169dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxSecondRow.setDefaultUnit(kony.flex.DP);
            var flxEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 2
            }, {}, {});
            flxEmailID.setDefaultUnit(kony.flex.DP);
            var lblEmailID = new kony.ui.Label({
                "id": "lblEmailID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxEmailValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 70,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoEmailError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailError.setDefaultUnit(kony.flex.DP);
            var lblNoEmailErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmailError = new kony.ui.Label({
                "id": "lblNoEmailError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.frmGuestDashboard.EmailMissing\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailError.add(lblNoEmailErrorIcon, lblNoEmailError);
            var lblEmailCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblEmailCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.lblEmailCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailID.add(lblEmailID, tbxEmailValue, flxNoEmailError, lblEmailCount);
            var flxDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 2
            }, {}, {});
            flxDOB.setDefaultUnit(kony.flex.DP);
            var lblDOB = new kony.ui.Label({
                "id": "lblDOB",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DateOfBirth\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoDOBError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoDOBError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoDOBError.setDefaultUnit(kony.flex.DP);
            var lblNoDOBErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDOBErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDOBError = new kony.ui.Label({
                "id": "lblNoDOBError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDOBError.add(lblNoDOBErrorIcon, lblNoDOBError);
            var flxDOBValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxDOBValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxDOBValue.setDefaultUnit(kony.flex.DP);
            var datePicker = new kony.ui.CustomWidget({
                "id": "datePicker",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxDOBValue.add(datePicker);
            flxDOB.add(lblDOB, flxNoDOBError, flxDOBValue);
            var flxSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxSSN",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 2
            }, {}, {});
            flxSSN.setDefaultUnit(kony.flex.DP);
            var lblSSN = new kony.ui.Label({
                "id": "lblSSN",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSSN = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxSSN",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 9,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoSSN",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoSSN.setDefaultUnit(kony.flex.DP);
            var lblNoSSNError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSSNError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSSN = new kony.ui.Label({
                "id": "lblNoSSN",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.SSN_cannot_be_empty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSSN.add(lblNoSSNError, lblNoSSN);
            var lblSSNCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblSSNCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.lblSSNCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSSN.add(lblSSN, tbxSSN, flxNoSSN, lblSSNCount);
            flxSecondRow.add(flxEmailID, flxDOB, flxSSN);
            var flxThirdRow = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxThirdRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "268dp",
                "width": "100%"
            }, {}, {});
            flxThirdRow.setDefaultUnit(kony.flex.DP);
            var flxMotherMaidenName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxMotherMaidenName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 2
            }, {}, {});
            flxMotherMaidenName.setDefaultUnit(kony.flex.DP);
            var lblMotherMaidenName = new kony.ui.Label({
                "id": "lblMotherMaidenName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.lblMotherMaidenName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxMotherMaidenName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxMotherMaidenName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.lblMotherMaidenName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoMotherMaidenNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoMotherMaidenNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoMotherMaidenNameError.setDefaultUnit(kony.flex.DP);
            var lblNoMotherMaidenNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoMotherMaidenNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "5dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoMotherMaidenNameError = new kony.ui.Label({
                "height": "100%",
                "id": "lblNoMotherMaidenNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoMotherMaidenNameError.add(lblNoMotherMaidenNameErrorIcon, lblNoMotherMaidenNameError);
            var lblMotherMaidenNameCount = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMotherMaidenNameCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": 0,
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMotherMaidenName.add(lblMotherMaidenName, tbxMotherMaidenName, flxNoMotherMaidenNameError, lblMotherMaidenNameCount);
            var flxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.50%",
                "zIndex": 2
            }, {}, {});
            flxUserName.setDefaultUnit(kony.flex.DP);
            var flxUserLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "28px",
                "id": "flxUserLoading",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3dp",
                "isModalContainer": false,
                "skin": "CopysknFlxffffffbordere0a948b2963e9a4d",
                "top": "37dp",
                "width": "53%",
                "zIndex": 2
            }, {}, {});
            flxUserLoading.setDefaultUnit(kony.flex.DP);
            var flxUserLoadingContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxUserLoadingContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxUserLoadingContainer.setDefaultUnit(kony.flex.DP);
            var imgUserLoading = new kony.ui.Image2({
                "height": "20px",
                "id": "imgUserLoading",
                "isVisible": true,
                "left": "3dp",
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserLoadingContainer.add(imgUserLoading);
            var lblVerifying = new kony.ui.Label({
                "id": "lblVerifying",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "text": "Verifying . . .",
                "top": "6dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserLoading.add(flxUserLoadingContainer, lblVerifying);
            var flxUserNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxUserNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUserNameError.setDefaultUnit(kony.flex.DP);
            var lblUsernameCheckIcon = new kony.ui.Label({
                "height": "100%",
                "id": "lblUsernameCheckIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.Username_cannot_be_empty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUsernameCheck = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUsernameCheck",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserNameError.add(lblUsernameCheckIcon, lblUsernameCheck);
            var textBoxEntryAsst = new com.adminConsole.common.textBoxEntryAsst({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryAsst",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnCheck": {
                        "left": "0dp",
                        "width": "75dp"
                    },
                    "flxBtnCheck": {
                        "isVisible": true,
                        "right": "7dp",
                        "top": "30dp",
                        "width": "75dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                        "top": "30dp"
                    },
                    "textBoxEntryAsst": {
                        "top": "1dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRulesLabel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRulesLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "top": "0px",
                "width": "30px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRulesLabel.setDefaultUnit(kony.flex.DP);
            var lblRules = new kony.ui.RichText({
                "id": "lblRules",
                "isVisible": true,
                "onClick": controller.AS_RichText_bd35a9814af54de48c78a578bdfcf027,
                "right": "0px",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRulesLabel.add(lblRules);
            flxUserName.add(flxUserLoading, flxUserNameError, textBoxEntryAsst, flxRulesLabel);
            flxThirdRow.add(flxMotherMaidenName, flxUserName);
            var lblContactDetails = new kony.ui.Label({
                "id": "lblContactDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblContactDetails\")",
                "top": "351dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactDetailsLine = new kony.ui.Label({
                "bottom": "20px",
                "height": "1px",
                "id": "lblContactDetailsLine",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknSeparator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "371px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPhoneNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPhoneNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "389dp",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxPhoneNumber.setDefaultUnit(kony.flex.DP);
            var flxPhoneNumberContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPhoneNumberContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "230dp"
            }, {}, {});
            flxPhoneNumberContainer.setDefaultUnit(kony.flex.DP);
            var contactNumber = new com.adminConsole.common.contactNumber({
                "height": "100%",
                "id": "contactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxError": {
                        "isVisible": true
                    },
                    "txtContactNumber": {
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                        "width": "130dp"
                    },
                    "txtISDCode": {
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPhoneNumberContainer.add(contactNumber);
            var tbxExtension = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxExtension",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "250dp",
                "placeholder": "Extension",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "20%",
                "zIndex": 3
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPhoneNumber.add(flxPhoneNumberContainer, tbxExtension);
            var flxNoPhoneNumberError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoPhoneNumberError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "433dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPhoneNumberError.setDefaultUnit(kony.flex.DP);
            var lblNoPhoneNumberErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPhoneNumberErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPhoneNumberError = new kony.ui.Label({
                "id": "lblNoPhoneNumberError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoPhnNumberError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPhoneNumberError.add(lblNoPhoneNumberErrorIcon, lblNoPhoneNumberError);
            var lblAddress = new kony.ui.Label({
                "id": "lblAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
                "top": "457dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddressLine = new kony.ui.Label({
                "bottom": "20px",
                "height": "1px",
                "id": "lblAddressLine",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "477px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoAddressError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoAddressError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "455dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoAddressError.setDefaultUnit(kony.flex.DP);
            var lblNoAddressErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoAddressErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "1dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoAddressError = new kony.ui.Label({
                "id": "lblNoAddressError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.lblAddressCannotBeEmpty\")",
                "top": "1dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAddressError.add(lblNoAddressErrorIcon, lblNoAddressError);
            var flxAddressContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "flxAddressContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "515dp",
                "width": "46.30%"
            }, {}, {});
            flxAddressContainer.setDefaultUnit(kony.flex.DP);
            var flxStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxStreetName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStreetName.setDefaultUnit(kony.flex.DP);
            var lblStreetName = new kony.ui.Label({
                "id": "lblStreetName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxStreetName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxStreetName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoStreetName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoStreetName.setDefaultUnit(kony.flex.DP);
            var lblNoStreetNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStreetNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStreetNameError = new kony.ui.Label({
                "id": "lblNoStreetNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoStreetName.add(lblNoStreetNameErrorIcon, lblNoStreetNameError);
            flxStreetName.add(lblStreetName, tbxStreetName, flxNoStreetName);
            var flxBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxBuildingName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBuildingName.setDefaultUnit(kony.flex.DP);
            var lblBuildingName = new kony.ui.Label({
                "id": "lblBuildingName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblBuildingNumber\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxBuildingName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxBuildingName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.BuildingName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoBuildingName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBuildingName.setDefaultUnit(kony.flex.DP);
            var lblNoBuildingNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBuildingNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBuildingNameError = new kony.ui.Label({
                "id": "lblNoBuildingNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBuildingName.add(lblNoBuildingNameErrorIcon, lblNoBuildingNameError);
            flxBuildingName.add(lblBuildingName, tbxBuildingName, flxNoBuildingName);
            var flxZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxZipCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxZipCode.setDefaultUnit(kony.flex.DP);
            var lblZipCode = new kony.ui.Label({
                "id": "lblZipCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxZipCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxZipCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryZip = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZip",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoZipCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoZipCode.setDefaultUnit(kony.flex.DP);
            var lblNoZipErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoZipError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoZipCode.add(lblNoZipErrorIcon, lblNoZipError);
            flxZipCode.add(lblZipCode, tbxZipCode, imgMandatoryZip, flxNoZipCode);
            var flxCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCountry.setDefaultUnit(kony.flex.DP);
            var lblCountry = new kony.ui.Label({
                "id": "lblCountry",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCountryTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCountryTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCountry = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "typeHead": {
                        "height": "170dp",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCountryTypeHead.add(typeHeadCountry);
            var imgMandatoryCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountry.setDefaultUnit(kony.flex.DP);
            var lblNoCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountry.add(lblNoCountryErrorIcon, lblNoCountryError);
            flxCountry.add(lblCountry, flxCountryTypeHead, imgMandatoryCountry, flxNoCountry);
            var flxState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxState.setDefaultUnit(kony.flex.DP);
            var lblState = new kony.ui.Label({
                "id": "lblState",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStateTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStateTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxStateTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadState = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStateTypeHead.add(typeHeadState);
            var imgMandatoryState = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryState",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoState.setDefaultUnit(kony.flex.DP);
            var lblNoStateErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStateError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoState.add(lblNoStateErrorIcon, lblNoStateError);
            flxState.add(lblState, flxStateTypeHead, imgMandatoryState, flxNoState);
            var flxCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCity.setDefaultUnit(kony.flex.DP);
            var lblCity = new kony.ui.Label({
                "id": "lblCity",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCityTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCityTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCityTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCity = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCityTypeHead.add(typeHeadCity);
            var imgMandatoryCity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCity",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCity.setDefaultUnit(kony.flex.DP);
            var lblNoCityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCity.add(lblNoCityErrorIcon, lblNoCityError);
            flxCity.add(lblCity, flxCityTypeHead, imgMandatoryCity, flxNoCity);
            flxAddressContainer.add(flxStreetName, flxBuildingName, flxZipCode, flxCountry, flxState, flxCity);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "515dp",
                "width": "46.30%",
                "zIndex": 2
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var segSearch = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }, {
                    "lblAddress": "Granville Street, Vancover, BC, Canada",
                    "lblPinIcon": ""
                }],
                "groupCells": false,
                "height": "75%",
                "id": "segSearch",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchCompanyMap",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSearchCompanyMap": "flxSearchCompanyMap",
                    "lblAddress": "lblAddress",
                    "lblPinIcon": "lblPinIcon"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var map1 = new com.konymp.map1({
                "height": "90%",
                "id": "map1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "overrides": {
                    "map1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90%",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "40dp"
                    },
                    "mapLocations": {
                        "height": "100%",
                        "top": "0%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchContainer.add(tbxSearch, segSearch, map1);
            var flxUSPS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxUSPS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "30dp",
                "skin": "sknflxConfigBundlesFillVLightGrey",
                "top": "880dp",
                "zIndex": 2
            }, {}, {});
            flxUSPS.setDefaultUnit(kony.flex.DP);
            var flxUSPSContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxUSPSContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxUSPSContainer.setDefaultUnit(kony.flex.DP);
            var lblUSPSIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUSPSIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcomoonGreenTick",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUSPStext = new kony.ui.Label({
                "id": "lblUSPStext",
                "isVisible": true,
                "left": "1dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.fillOutTheAddressToCheckUSPSValidation\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUSPSButton = new kony.ui.Label({
                "id": "lblUSPSButton",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknLbl00AAEELatoMed13px",
                "text": "Validate",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUSPSContainer.add(lblUSPSIcon, lblUSPStext, lblUSPSButton);
            flxUSPS.add(flxUSPSContainer);
            flxDetailContentContainer.add(lblPersonalInfo, lblPersonalInfoLine, flxFirstRow, flxSecondRow, flxThirdRow, lblContactDetails, lblContactDetailsLine, flxPhoneNumber, flxNoPhoneNumberError, lblAddress, lblAddressLine, flxNoAddressError, flxAddressContainer, flxSearchContainer, flxUSPS);
            var flxUsernameRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUsernameRules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150px",
                "isModalContainer": false,
                "skin": "slFbox0a30851fc80b24b",
                "top": "65%",
                "width": "370px",
                "zIndex": 10
            }, {}, {});
            flxUsernameRules.setDefaultUnit(kony.flex.DP);
            var lblUsernameRules = new kony.ui.Label({
                "height": "15px",
                "id": "lblUsernameRules",
                "isVisible": true,
                "left": "10px",
                "skin": "slLabel0ff9cfa99545646",
                "text": "Username rules:",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsernameRulesInner = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "10px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "120px",
                "horizontalScrollIndicator": true,
                "id": "flxUsernameRulesInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "40px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxUsernameRulesInner.setDefaultUnit(kony.flex.DP);
            var rtxUsernameRules = new kony.ui.RichText({
                "bottom": "10px",
                "height": "100%",
                "id": "rtxUsernameRules",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLato485c7514px",
                "text": "<ul><li>Minimum Length- 8, Maximum Length- 24</li> <li>Can be alpha-numeric (a-z, A-Z, 0-9)</li> <li>No spaces or special characters allowed except period '.'</li> <li>Should Start with an alphabet or digit.</li> <li>Username is not case sensitive.</li> <li>Period '.' cannot be used more than once.</li></ul>",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsernameRulesInner.add(rtxUsernameRules);
            flxUsernameRules.add(lblUsernameRules, flxUsernameRulesInner);
            flxAddOptionsSection.add(flxAddIdInfo, flxTermsAndCoditions, flxAddUsersButtons, flxDetailContentContainer, flxUsernameRules);
            flxAddMainContainer.add(flxOptions, flxVerticalTabsSeperator, flxAddOptionsSection);
            flxMainContent.add(flxBreadCrumb, flxAddMainContainer);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContent, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxUSPSValidation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUSPSValidation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxUSPSValidation.setDefaultUnit(kony.flex.DP);
            var flxEligibilityEditPolicy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxEligibilityEditPolicy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxEligibilityEditPolicy.setDefaultUnit(kony.flex.DP);
            var flxEligibilityTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEligibilityTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEligibilityTopColor.setDefaultUnit(kony.flex.DP);
            flxEligibilityTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxEligibilityClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEligibilityClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxEligibilityClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEligibilityClose.add(fontIconImgCLose);
            var flxheaderEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxheaderEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheaderEdit.setDefaultUnit(kony.flex.DP);
            var lblAddCriteria = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddCriteria",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.USPSValidation\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCriteriaError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoCriteriaError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "220px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoCriteriaError.setDefaultUnit(kony.flex.DP);
            var lblNoCriteriaErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoCriteriaErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCriteriaError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoCriteriaError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCriteriaError.add(lblNoCriteriaErrorIcon, lblNoCriteriaError);
            flxheaderEdit.add(lblAddCriteria, flxNoCriteriaError);
            var flxOriginalAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "focusSkin": "sknTextAreaHover",
                "height": "55dp",
                "id": "flxOriginalAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "20dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknTextAreaHover"
            });
            flxOriginalAddress.setDefaultUnit(kony.flex.DP);
            var lblOriginalAddress = new kony.ui.Label({
                "id": "lblOriginalAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.OriginalAddressProvided\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOriginalAddressValue = new kony.ui.Label({
                "id": "lblOriginalAddressValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatobold000000",
                "text": "Original Address Provided",
                "top": 25,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOriginalAddress.add(lblOriginalAddress, lblOriginalAddressValue);
            var flxUSPSAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "focusSkin": "sknTextAreaHover",
                "height": "55dp",
                "id": "flxUSPSAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknTextAreaHover"
            });
            flxUSPSAddress.setDefaultUnit(kony.flex.DP);
            var lblUSPSAddress = new kony.ui.Label({
                "id": "lblUSPSAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.USPSRecommendation\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUSPSAddressValue = new kony.ui.Label({
                "id": "lblUSPSAddressValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatobold000000",
                "text": "Original Address Provided",
                "top": 25,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUSPSAddress.add(lblUSPSAddress, lblUSPSAddressValue);
            var CopylblUSPSAddress0b2cd2a15825d41 = new kony.ui.Label({
                "id": "CopylblUSPSAddress0b2cd2a15825d41",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.UseUSPSRecommendationAddress\")",
                "top": 4,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPhraseStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxPhraseStatus",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox0h43cfab67a134d",
                "top": 10,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPhraseStatus.setDefaultUnit(kony.flex.DP);
            var lblPhraseStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhraseStatus",
                "isVisible": true,
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchToggleStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25px",
                "id": "SwitchToggleStatus",
                "isVisible": true,
                "left": "0%",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhraseStatus.add(lblPhraseStatus, SwitchToggleStatus);
            flxPopupHeader.add(flxEligibilityClose, flxheaderEdit, flxOriginalAddress, flxUSPSAddress, CopylblUSPSAddress0b2cd2a15825d41, flxPhraseStatus);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgError = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(lblErrorMsg, imgError);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "160px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": "155px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.YES__UPDATE\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEditButtons.add(flxError, btnCancel, btnsave);
            flxEligibilityEditPolicy.add(flxEligibilityTopColor, flxPopupHeader, flxEditButtons);
            flxUSPSValidation.add(flxEligibilityEditPolicy);
            var flxSubmitPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubmitPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxSubmitPopup.setDefaultUnit(kony.flex.DP);
            var submitPopUp = new com.adminConsole.onBoarding.popUp({
                "height": "100%",
                "id": "submitPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "163px"
                    },
                    "btnPopUpDelete": {
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopupMsg": {
                        "centerX": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.approveMsg\")",
                        "top": "10dp"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": 30,
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.approveRtx\")",
                        "left": 20,
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSubmitPopup.add(submitPopUp);
            var flxCancelPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancelPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCancelPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "top": "viz.val_cleared"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.YesCancel\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Cancel Application"
                    },
                    "rtxPopUpDisclaimer": {
                        "left": "20px",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to cancel this on-boarding application?<br><br>\nOn cancellation, you will loose all the data regarding the applicant.\n",
                        "top": "30px",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCancelPopup.add(popUp);
            var flxDuplicateSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDuplicateSSN",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDuplicateSSN.setDefaultUnit(kony.flex.DP);
            var flxDuplicateSSNContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "290px",
                "id": "flxDuplicateSSNContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxDuplicateSSNContainer.setDefaultUnit(kony.flex.DP);
            var flxTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor.setDefaultUnit(kony.flex.DP);
            flxTopColor.add();
            var flxSSNPopupHeaderClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200px",
                "id": "flxSSNPopupHeaderClose",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSSNPopupHeaderClose.setDefaultUnit(kony.flex.DP);
            var flxSSNDuplicateClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxSSNDuplicateClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSSNDuplicateClose.setDefaultUnit(kony.flex.DP);
            var lblIconImg = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconImg",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSSNDuplicateClose.add(lblIconImg);
            var flxSSNHeaderEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSSNHeaderEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxSSNHeaderEdit.setDefaultUnit(kony.flex.DP);
            var lblSSNHeaderEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSSNHeaderEdit",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl16pxLatoRegular",
                "text": "Customer Already Exists in the System",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSSNHeaderEdit.add(lblSSNHeaderEdit);
            var lblSSNDuplicate = new kony.ui.Label({
                "id": "lblSSNDuplicate",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "The user with the same SSN already in the system",
                "top": 14,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSSNValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSSNValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "6dp",
                "width": "100%"
            }, {}, {});
            flxSSNValue.setDefaultUnit(kony.flex.DP);
            var lblSSNName = new kony.ui.Label({
                "id": "lblSSNName",
                "isVisible": true,
                "left": "20px",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 14,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNNameValue = new kony.ui.Label({
                "id": "lblSSNNameValue",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Martin luther",
                "top": 46,
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNDOB = new kony.ui.Label({
                "id": "lblSSNDOB",
                "isVisible": true,
                "left": "124px",
                "skin": "lblLato14px",
                "text": "DATE OF BIRTH",
                "top": 14,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNEmail = new kony.ui.Label({
                "id": "lblSSNEmail",
                "isVisible": true,
                "left": "250px",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.EmailAddressCAPS\")",
                "top": 14,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNSSN = new kony.ui.Label({
                "id": "lblSSNSSN",
                "isVisible": true,
                "left": "447px",
                "skin": "lblLato14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN_HEADER\")",
                "top": 14,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNDOBValue = new kony.ui.Label({
                "id": "lblSSNDOBValue",
                "isVisible": true,
                "left": "124px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Martin luther",
                "top": 45,
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNEmailValue = new kony.ui.Label({
                "id": "lblSSNEmailValue",
                "isVisible": true,
                "left": "250px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Martin luther",
                "top": 45,
                "width": "28%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSSNValue = new kony.ui.Label({
                "id": "lblSSNValue",
                "isVisible": true,
                "left": "447px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Martin luther",
                "top": 46,
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSSNValue.add(lblSSNName, lblSSNNameValue, lblSSNDOB, lblSSNEmail, lblSSNSSN, lblSSNDOBValue, lblSSNEmailValue, lblSSNValue);
            flxSSNPopupHeaderClose.add(flxSSNDuplicateClose, flxSSNHeaderEdit, lblSSNDuplicate, flxSSNValue);
            var flxSSNEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSSNEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSSNEditButtons.setDefaultUnit(kony.flex.DP);
            var btnSSNCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSSNCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnChangeInfo = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnChangeInfo",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "CHANGE INFO",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxSSNEditButtons.add(btnSSNCancel, btnChangeInfo);
            flxDuplicateSSNContainer.add(flxTopColor, flxSSNPopupHeaderClose, flxSSNEditButtons);
            flxDuplicateSSN.add(flxDuplicateSSNContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxUSPSValidation, flxSubmitPopup, flxCancelPopup, flxDuplicateSSN);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmAssistedOnboarding,
            "enabledForIdleTimeout": true,
            "id": "frmAssistedOnboarding",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a6284269075144f8bbb5541ae8c788b7(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b06b861fcf8b4818aace9b896fd3b82d,
            "retainScrollPosition": false
        }]
    }
});