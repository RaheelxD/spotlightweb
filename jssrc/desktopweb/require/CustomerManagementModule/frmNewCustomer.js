define("CustomerManagementModule/frmNewCustomer", function() {
    return function(controller) {
        function addWidgetsfrmNewCustomer() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "100px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "2px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCHCUSTOMERS\")",
                        "left": "0px"
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "btnPreviousPage1": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsDown": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.Breadcrumbs.NewCustomer\")",
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumb.add(breadcrumbs);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknBackgroundFFFFFF",
                "top": "30px",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "18dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContentHeader.setDefaultUnit(kony.flex.DP);
            var flxTag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxTag",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxLead",
                "top": "0dp",
                "width": "55dp",
                "zIndex": 1
            }, {}, {});
            flxTag.setDefaultUnit(kony.flex.DP);
            var fontIconCircle4 = new kony.ui.Label({
                "centerY": "48%",
                "id": "fontIconCircle4",
                "isVisible": true,
                "left": "6px",
                "skin": "sknCircleCustomerTag",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContent4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContent4",
                "isVisible": true,
                "left": "3px",
                "skin": "sknCustomerTypeTag",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.leadTag\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTag.add(fontIconCircle4, lblContent4);
            var lblNewCustomerHeader = new kony.ui.Label({
                "id": "lblNewCustomerHeader",
                "isVisible": false,
                "left": "35dp",
                "skin": "sknLblLatoRegulat18Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.newCustomerName\")",
                "top": "32dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxe1e5edop100",
                "top": "40dp",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var generalInfoHeader = new com.adminConsole.customerMang.generalInfoHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "generalInfoHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "95%",
                "overrides": {
                    "flxActionButtons": {
                        "reverseLayoutDirection": false
                    },
                    "flxCSRAssist": {
                        "isVisible": true
                    },
                    "flxRiskStatus": {
                        "isVisible": true,
                        "zIndex": 10
                    },
                    "flxUnlock": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": 225
                    },
                    "generalInfoHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "isVisible": false,
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "95%"
                    },
                    "lblSeparator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContentHeader.add(flxTag, lblNewCustomerHeader, flxSeperator, generalInfoHeader);
            var flxContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "166px",
                "id": "flxLoans",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCustomRadius5px",
                "top": "0px",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxLoans.setDefaultUnit(kony.flex.DP);
            var lblLoansHeader = new kony.ui.Label({
                "id": "lblLoansHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblA1ACBALatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.simulateOrApply\")",
                "top": "11dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparatorLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLoans",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "top": "11dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLoans.setDefaultUnit(kony.flex.DP);
            flxSeparatorLoans.add();
            var listBoxLoanTypes = new kony.ui.ListBox({
                "centerX": "50%",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "listBoxLoanTypes",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["PERSONAL_LOAN", "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Personal\")"],
                    ["VEHICLE_LOAN", "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.vehicle\")"],
                    ["CREDITCARD_LOAN", "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.creditCard\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "16dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxLoansButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLoansButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "11px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLoansButtons.setDefaultUnit(kony.flex.DP);
            var flxLearnMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLearnMore",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "width": "20%"
            }, {}, {});
            flxLearnMore.setDefaultUnit(kony.flex.DP);
            var lblLearnMore = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLearnMore",
                "isVisible": true,
                "left": 0,
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LearnMore\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLearnMore.add(lblLearnMore);
            var btnAppply = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnAppply",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
                "top": "0px",
                "width": "90dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxLoansButtons.add(flxLearnMore, btnAppply);
            flxLoans.add(lblLoansHeader, flxSeparatorLoans, listBoxLoanTypes, flxLoansButtons);
            var flxOnBoardingInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "166px",
                "id": "flxOnBoardingInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknFlxffffffCustomRadius5px",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingInner.setDefaultUnit(kony.flex.DP);
            var lblOnBoardingHeader = new kony.ui.Label({
                "id": "lblOnBoardingHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblA1ACBALatoRegular12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailBanking\")",
                "top": "11dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxOnBoardingSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxOnBoardingSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "top": "11px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingSeparator.setDefaultUnit(kony.flex.DP);
            flxOnBoardingSeparator.add();
            var lblOnBoardingDescription = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblOnBoardingDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.retailDescription\")",
                "top": "13dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnonBoarding = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30dp",
                "id": "btnonBoarding",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmNewCustomer.btnOnnBoarding\")",
                "top": "26dp",
                "width": "170dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxOnBoardingInner.add(lblOnBoardingHeader, flxOnBoardingSeparator, lblOnBoardingDescription, btnonBoarding);
            flxContent.add(flxLoans, flxOnBoardingInner);
            flxDetails.add(flxContentHeader, flxContent);
            flxMainContent.add(flxBreadCrumb, flxDetails);
            var flxOnboarding = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxOnboarding",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxOnboarding.setDefaultUnit(kony.flex.DP);
            var flxOnboardingPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxOnboardingPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "110dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "140dp",
                "width": "800dp",
                "zIndex": 10
            }, {}, {});
            flxOnboardingPopup.setDefaultUnit(kony.flex.DP);
            var flxOnBoardingTopBlueBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxOnBoardingTopBlueBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknOnBoardingTopBarBlue",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingTopBlueBar.setDefaultUnit(kony.flex.DP);
            flxOnBoardingTopBlueBar.add();
            var flxOnBoardingPopupContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "320dp",
                "id": "flxOnBoardingPopupContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknOnBoardingPopupContentWhite",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingPopupContent.setDefaultUnit(kony.flex.DP);
            var flxOnBoardingPopupTitleBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxOnBoardingPopupTitleBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingPopupTitleBar.setDefaultUnit(kony.flex.DP);
            var lblOnBoardingPopupTitle = new kony.ui.Label({
                "id": "lblOnBoardingPopupTitle",
                "isVisible": true,
                "left": "25dp",
                "skin": "lblTextLatoLightBold",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.check_eligibility_popup_heading\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxOnBoardingPopupClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOnBoardingPopupClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "top": "15dp",
                "width": "18dp"
            }, {}, {
                "hoverSkin": "hoverhandSkin"
            });
            flxOnBoardingPopupClose.setDefaultUnit(kony.flex.DP);
            var lblOnBoardingPopupClose = new kony.ui.Label({
                "id": "lblOnBoardingPopupClose",
                "isVisible": true,
                "right": "0dp",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOnBoardingPopupClose.add(lblOnBoardingPopupClose);
            flxOnBoardingPopupTitleBar.add(lblOnBoardingPopupTitle, flxOnBoardingPopupClose);
            var flxOnBoardingEligibility = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "189dp",
                "horizontalScrollIndicator": true,
                "id": "flxOnBoardingEligibility",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOnBoardingEligibility.setDefaultUnit(kony.flex.DP);
            var lblOnBoardingContent = new kony.ui.Label({
                "id": "lblOnBoardingContent",
                "isVisible": true,
                "left": "25dp",
                "skin": "sknTextDarkGreyLightBold",
                "text": " Customer should meet at least one of the criteria  listed below.",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            kony.mvc.registry.add('CopyFBox0d56e3756273342', 'CopyFBox0d56e3756273342', 'CopyFBox0d56e3756273342Controller');
            var segOnBoardingPopup = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblEligibilityCriteria": "Applicant is related to a current Kony bank member",
                    "lblIconCheckBox": "",
                    "radioBtn": {
                        "masterData": [
                            ["rbg1", "Radiobutton One"],
                            ["rbg2", "Radiobutton Two"],
                            ["rbg3", "Radiobutton Three"]
                        ],
                        "selectedKey": "rbg1",
                        "selectedkey": "rbg1",
                        "selectedkeyvalue": ["rbg1", "Radiobutton One"],
                        "selectedKeyValue": ["rbg1", "Radiobutton One"]
                    }
                }, {
                    "lblEligibilityCriteria": "A member of membership organisation that Kony bank serves",
                    "lblIconCheckBox": "",
                    "radioBtn": {
                        "masterData": [
                            ["rbg1", "Radiobutton One"],
                            ["rbg2", "Radiobutton Two"],
                            ["rbg3", "Radiobutton Three"]
                        ],
                        "selectedKey": "rbg1",
                        "selectedkey": "rbg1",
                        "selectedkeyvalue": ["rbg1", "Radiobutton One"],
                        "selectedKeyValue": ["rbg1", "Radiobutton One"]
                    }
                }, {
                    "lblEligibilityCriteria": "An immediate family member or roommate of one of the above.",
                    "lblIconCheckBox": "",
                    "radioBtn": {
                        "masterData": [
                            ["rbg1", "Radiobutton One"],
                            ["rbg2", "Radiobutton Two"],
                            ["rbg3", "Radiobutton Three"]
                        ]
                    }
                }, {
                    "lblEligibilityCriteria": "Lives, works, worship or attend school that Kony bank serves",
                    "lblIconCheckBox": "",
                    "radioBtn": {
                        "masterData": [
                            ["rbg1", "Radiobutton One"],
                            ["rbg2", "Radiobutton Two"],
                            ["rbg3", "Radiobutton Three"]
                        ]
                    }
                }],
                "groupCells": false,
                "id": "segOnBoardingPopup",
                "isVisible": true,
                "left": "25dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "CopyFBox0d56e3756273342",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "30dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "lblEligibilityCriteria": "lblEligibilityCriteria",
                    "lblIconCheckBox": "lblIconCheckBox",
                    "radioBtn": "radioBtn"
                },
                "width": "750dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOnBoardingEligibility.add(lblOnBoardingContent, segOnBoardingPopup);
            var flxOnBoardingPopupSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxOnBoardingPopupSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknOnBoardingGreyBar",
                "top": "249dp",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingPopupSeperator.setDefaultUnit(kony.flex.DP);
            flxOnBoardingPopupSeperator.add();
            var flxOnBoardingNotMeetAny = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxOnBoardingNotMeetAny",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOnBoardingNotMeetAny.setDefaultUnit(kony.flex.DP);
            var flxNoEligibilityCriteriaMatches = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoEligibilityCriteriaMatches",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "top": "10dp"
            }, {}, {});
            flxNoEligibilityCriteriaMatches.setDefaultUnit(kony.flex.DP);
            var radioNoEligibilityMatches = new kony.ui.RadioButtonGroup({
                "height": "25dp",
                "id": "radioNoEligibilityMatches",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["1", "1"]
                ],
                "skin": "slRadioButtonGroup",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOnBoardingNotAnyCriteria = new kony.ui.Label({
                "id": "lblOnBoardingNotAnyCriteria",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknTextDarkGreyLightBold",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.applicant_not_meet_above_criteria\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEligibilityCriteriaMatches.add(radioNoEligibilityMatches, lblOnBoardingNotAnyCriteria);
            var lblOnBoardingErrorMsg = new kony.ui.Label({
                "id": "lblOnBoardingErrorMsg",
                "isVisible": false,
                "left": "50dp",
                "skin": "sknTextRedSmall",
                "text": "Please select at least one criteria before submitting.",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOnBoardingErrorIcon = new kony.ui.Label({
                "id": "lblOnBoardingErrorIcon",
                "isVisible": false,
                "left": "30dp",
                "skin": "sknErrorIconBig",
                "text": "",
                "top": "48dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOnBoardingNotMeetAny.add(flxNoEligibilityCriteriaMatches, lblOnBoardingErrorMsg, lblOnBoardingErrorIcon);
            flxOnBoardingPopupContent.add(flxOnBoardingPopupTitleBar, flxOnBoardingEligibility, flxOnBoardingPopupSeperator, flxOnBoardingNotMeetAny);
            var flxOnboardingBottomBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxOnboardingBottomBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknOnBoardingGreyBar",
                "top": "330dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOnboardingBottomBar.setDefaultUnit(kony.flex.DP);
            var btnOnBoardingCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnOnBoardingCancel",
                "isVisible": true,
                "right": "230dp",
                "skin": "CopysknBtnLatoRegulara0c8769005ae0f41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnContinueOnBoarding = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnContinueOnBoarding",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "CONTINUE ONBOARDING",
                "top": "0%",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxOnboardingBottomBar.add(btnOnBoardingCancel, btnContinueOnBoarding);
            flxOnboardingPopup.add(flxOnBoardingTopBlueBar, flxOnBoardingPopupContent, flxOnboardingBottomBar);
            flxOnboarding.add(flxOnboardingPopup);
            flxRightPanel.add(flxMainHeader, flxMainContent, flxOnboarding);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0px",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCSRAssistLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCSRAssistLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0px",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxCSRAssistLoading.setDefaultUnit(kony.flex.DP);
            var flxCSRLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "300px",
                "id": "flxCSRLoading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "width": "600px",
                "zIndex": 5
            }, {}, {});
            flxCSRLoading.setDefaultUnit(kony.flex.DP);
            var flxCSRLoadingIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxCSRLoadingIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxCSRLoadingIndicator.setDefaultUnit(kony.flex.DP);
            var imgCSRLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "75px",
                "id": "imgCSRLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "75px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCSRLoadingIndicator.add(imgCSRLoading);
            var lblInitializeAssist = new kony.ui.Label({
                "centerX": "51%",
                "height": "15px",
                "id": "lblInitializeAssist",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Initiating_assist\")",
                "top": "15px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCSRLoading.add(flxCSRLoadingIndicator, lblInitializeAssist);
            flxCSRAssistLoading.add(flxCSRLoading);
            flxMain.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading, CSRAssist, flxCSRAssistLoading);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmNewCustomer,
            "enabledForIdleTimeout": true,
            "id": "frmNewCustomer",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d2c44a7dae134d939f82eb9cd8014005(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});