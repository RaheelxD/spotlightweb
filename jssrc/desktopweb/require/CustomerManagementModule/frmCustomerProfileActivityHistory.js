define("CustomerManagementModule/frmCustomerProfileActivityHistory", function() {
    return function(controller) {
        function addWidgetsfrmCustomerProfileActivityHistory() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "SEARCH"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "784px",
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "126px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationWrapper.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoWrapper = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxGeneralInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "700px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "ProfileGeneralInfo": {
                        "maxHeight": "700px"
                    },
                    "generalInfoHeader.flxOptions": {
                        "right": "2px"
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOtherInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxOtherInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOtherInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0hfd18814fd664dCM",
                "overrides": {
                    "flxLeftArrow": {
                        "isVisible": false
                    },
                    "flxTabs": {
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "tabs": {
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOtherInfoTabs.add(tabs);
            var flxTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTabsSeperator.add();
            var flxOtherInfoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherInfoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoDetails.setDefaultUnit(kony.flex.DP);
            var flxActivityHistoryWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActivityHistoryWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActivityHistoryWrapper.setDefaultUnit(kony.flex.DP);
            var flxActivityHistoryListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActivityHistoryListing",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxActivityHistoryListing.setDefaultUnit(kony.flex.DP);
            var flxActivityHistoryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxActivityHistoryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActivityHistoryHeader.setDefaultUnit(kony.flex.DP);
            var flxRecentActivity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "58px",
                "id": "flxRecentActivity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRecentActivity.setDefaultUnit(kony.flex.DP);
            var lblRecentActivities = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRecentActivities",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoRegular484b5216px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Recent_Activities\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnViewAll = new kony.ui.Button({
                "centerY": "50%",
                "id": "btnViewAll",
                "isVisible": true,
                "left": "0px",
                "skin": "sknBtnLato11abeb12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.View_all\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRecentActivity.add(lblRecentActivities, btnViewAll);
            var lblActivityHistorySeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblActivityHistorySeperator",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": 59,
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActivityHistoryHeader.add(flxRecentActivity, lblActivityHistorySeperator);
            var flxActivityHistorySegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": false,
                "horizontalScrollIndicator": true,
                "id": "flxActivityHistorySegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_NONE,
                "skin": "slFSbox",
                "top": "60px",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActivityHistorySegment.setDefaultUnit(kony.flex.DP);
            var flxActivityHistorySegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxActivityHistorySegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100",
                "top": "0px"
            }, {}, {});
            flxActivityHistorySegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxSessionID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSessionID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxSessionID.setDefaultUnit(kony.flex.DP);
            var lblSessionID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSessionID",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SESSION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortSessionID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortSessionID",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortSessionID.setDefaultUnit(kony.flex.DP);
            var fonticonSortSessionID = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortSessionID",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortSessionID.add(fonticonSortSessionID);
            flxSessionID.add(lblSessionID, flxSortSessionID);
            var flxDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDevice",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "8%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxDevice.setDefaultUnit(kony.flex.DP);
            var lblDevice = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDevice",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEORBROWSER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDevice",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDevice.setDefaultUnit(kony.flex.DP);
            var fonticonSortDevice = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortDevice",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortDevice.add(fonticonSortDevice);
            flxDevice.add(lblDevice, flxSortDevice);
            var flxChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxChannel.setDefaultUnit(kony.flex.DP);
            var lblChannel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxFilterChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterChannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterChannel.setDefaultUnit(kony.flex.DP);
            var fonticonFilterChannel = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonFilterChannel",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxFilterChannel.add(fonticonFilterChannel);
            flxChannel.add(lblChannel, flxFilterChannel);
            var flxos = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxos",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxos.setDefaultUnit(kony.flex.DP);
            var lblos = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblos",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.OS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxFilteros = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilteros",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilteros.setDefaultUnit(kony.flex.DP);
            var fonticonFilteros = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonFilteros",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxFilteros.add(fonticonFilteros);
            flxos.add(lblos, flxFilteros);
            var flxIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxIPAddress.setDefaultUnit(kony.flex.DP);
            var lblIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIPAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.IP_ADDRESS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortIPAddress.setDefaultUnit(kony.flex.DP);
            var fonticonSortIPAddress = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortIPAddress",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortIPAddress.add(fonticonSortIPAddress);
            flxIPAddress.add(lblIPAddress, flxSortIPAddress);
            flxActivityHistorySegmentHeader.add(flxSessionID, flxDevice, flxChannel, flxos, flxIPAddress);
            var lblActivityHistoryHeaderSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblActivityHistoryHeaderSeperator",
                "isVisible": true,
                "left": "0px",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "59px",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segActivityHistory = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "data": [{
                    "lblChannel": "Linked a new account number",
                    "lblDevice": "Linked a new account number",
                    "lblDuration": "Mr. Bryan Victor Nash JR",
                    "lblHorizontalSeperator": "Linked a new account number",
                    "lblIpaddress": "Linked a new account number",
                    "lblNumberOfActivities": "Online Banking",
                    "lblOS": "Linked a new account number",
                    "lblSeperator": ".",
                    "lblSessionStart": "10/21/2016 - 6:46 am"
                }, {
                    "lblChannel": "Linked a new account number",
                    "lblDevice": "Linked a new account number",
                    "lblDuration": "Mr. Bryan Victor Nash JR",
                    "lblHorizontalSeperator": "Linked a new account number",
                    "lblIpaddress": "Linked a new account number",
                    "lblNumberOfActivities": "Online Banking",
                    "lblOS": "Linked a new account number",
                    "lblSeperator": ".",
                    "lblSessionStart": "10/21/2016 - 6:46 am"
                }, {
                    "lblChannel": "Linked a new account number",
                    "lblDevice": "Linked a new account number",
                    "lblDuration": "Mr. Bryan Victor Nash JR",
                    "lblHorizontalSeperator": "Linked a new account number",
                    "lblIpaddress": "Linked a new account number",
                    "lblNumberOfActivities": "Online Banking",
                    "lblOS": "Linked a new account number",
                    "lblSeperator": ".",
                    "lblSessionStart": "10/21/2016 - 6:46 am"
                }],
                "groupCells": false,
                "id": "segActivityHistory",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangActivityHistory",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCloumns": "flxCloumns",
                    "flxCustMangActivityHistory": "flxCustMangActivityHistory",
                    "flxFirstRow": "flxFirstRow",
                    "flxSecondRow": "flxSecondRow",
                    "flxSession": "flxSession",
                    "lblChannel": "lblChannel",
                    "lblDevice": "lblDevice",
                    "lblDuration": "lblDuration",
                    "lblHorizontalSeperator": "lblHorizontalSeperator",
                    "lblIpaddress": "lblIpaddress",
                    "lblNumberOfActivities": "lblNumberOfActivities",
                    "lblOS": "lblOS",
                    "lblSeperator": "lblSeperator",
                    "lblSessionStart": "lblSessionStart"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgActivityHistory = new kony.ui.RichText({
                "bottom": "200px",
                "centerX": "50%",
                "id": "rtxMsgActivityHistory",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "230px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "120px",
                "zIndex": 11
            }, {}, {});
            flxFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilter.add(statusFilterMenu);
            flxActivityHistorySegment.add(flxActivityHistorySegmentHeader, lblActivityHistoryHeaderSeperator, segActivityHistory, rtxMsgActivityHistory, flxFilter);
            flxActivityHistoryListing.add(flxActivityHistoryHeader, flxActivityHistorySegment);
            var flxActivityHistorySessionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActivityHistorySessionDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxActivityHistorySessionDetails.setDefaultUnit(kony.flex.DP);
            var flxBackToActivitySessions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxBackToActivitySessions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBackToActivitySessions.setDefaultUnit(kony.flex.DP);
            var backToActivitySessions = new com.adminConsole.customerMang.backToPageHeader({
                "height": "20px",
                "id": "backToActivitySessions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBack": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_BTN_BACK\")"
                    },
                    "flxBack": {
                        "left": "20px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBackToActivitySessions.add(backToActivitySessions);
            var flxSessionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxSessionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxBgF9F9F9",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxSessionDetails.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var lblActivitySessionIDHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivitySessionIDHeader",
                "isVisible": false,
                "left": "20px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Session_ID\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSessionHashSymbol = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSessionHashSymbol",
                "isVisible": false,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "#",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSessionData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSessionData",
                "isVisible": true,
                "left": 20,
                "skin": "sknlblLatoBold485c7516px",
                "text": "835282093",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRow1.add(lblActivitySessionIDHeader, lblSessionHashSymbol, lblSessionData);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var lblIpAddressHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIpAddressHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIpAddressData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIpAddressData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "210.239.2.10",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannelHeader",
                "isVisible": true,
                "left": "50px",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannelData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Mobile",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOSHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOSHeader",
                "isVisible": true,
                "left": "50px",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.OS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOSData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOSData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Android",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeviceHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceHeader",
                "isVisible": true,
                "left": "50px",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICE\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeviceData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Nexus 2",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeviceIDHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceIDHeader",
                "isVisible": true,
                "left": "50px",
                "skin": "sknlbl9CA9BALato12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEID\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeviceIDData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceIDData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "2x232ddw223f2",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRow2.add(lblIpAddressHeader, lblIpAddressData, lblChannelHeader, lblChannelData, lblOSHeader, lblOSData, lblDeviceHeader, lblDeviceData, lblDeviceIDHeader, lblDeviceIDData);
            flxSessionDetails.add(flxRow1, flxRow2);
            var flxActivityHistorySelection = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200px",
                "horizontalScrollIndicator": true,
                "id": "flxActivityHistorySelection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "20px",
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "180px",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActivityHistorySelection.setDefaultUnit(kony.flex.DP);
            var flxActivityHistoryDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxActivityHistoryDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "width": 1400
            }, {}, {});
            flxActivityHistoryDetailsHeader.setDefaultUnit(kony.flex.DP);
            var flxDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "14.50%",
                "zIndex": 1
            }, {}, {});
            flxDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DATEANDTIME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortDateAndTime.setDefaultUnit(kony.flex.DP);
            var fonticonSortDateAndTime = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortDateAndTime",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortDateAndTime.add(fonticonSortDateAndTime);
            flxDateAndTime.add(lblDateAndTime, flxSortDateAndTime);
            var flxModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxModuleName.setDefaultUnit(kony.flex.DP);
            var lblModuleName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModuleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.MODULE_NAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModuleName.setDefaultUnit(kony.flex.DP);
            var fonticonModuleName = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonModuleName",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortModuleName.add(fonticonModuleName);
            flxModuleName.add(lblModuleName, flxSortModuleName);
            var flxActivityType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxActivityType.setDefaultUnit(kony.flex.DP);
            var lblActivityType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_TYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxFilterActivityType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterActivityType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterActivityType.setDefaultUnit(kony.flex.DP);
            var fonticonFilterActivityType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonFilterActivityType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxFilterActivityType.add(fonticonFilterActivityType);
            flxActivityType.add(lblActivityType, flxFilterActivityType);
            var flxActivityDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivityDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "26.50%",
                "zIndex": 1
            }, {}, {});
            flxActivityDescription.setDefaultUnit(kony.flex.DP);
            var lblActivityDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActivityDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxActivityDescription.add(lblActivityDescription);
            var flxActvityStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActvityStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {}, {});
            flxActvityStatus.setDefaultUnit(kony.flex.DP);
            var lblActvityStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActvityStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxFilterActvityStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterActvityStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterActvityStatus.setDefaultUnit(kony.flex.DP);
            var fonticonFilterActvityStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonFilterActvityStatus",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxFilterActvityStatus.add(fonticonFilterActvityStatus);
            flxActvityStatus.add(lblActvityStatus, flxFilterActvityStatus);
            var flxReferenceID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {}, {});
            flxReferenceID.setDefaultUnit(kony.flex.DP);
            var lblReferenceID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceID",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.REFERENCE_ID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortReferenceID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceID",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceID.setDefaultUnit(kony.flex.DP);
            var fonticonSortReferenceID = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortReferenceID",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortReferenceID.add(fonticonSortReferenceID);
            flxReferenceID.add(lblReferenceID, flxSortReferenceID);
            var flxErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxErrorCode.setDefaultUnit(kony.flex.DP);
            var lblErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ERROR_CODE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortErrorCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "8dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortErrorCode.setDefaultUnit(kony.flex.DP);
            var fonticonSortErrorCode = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fonticonSortErrorCode",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxSortErrorCode.add(fonticonSortErrorCode);
            flxErrorCode.add(lblErrorCode, flxSortErrorCode);
            flxActivityHistoryDetailsHeader.add(flxDateAndTime, flxModuleName, flxActivityType, flxActivityDescription, flxActvityStatus, flxReferenceID, flxErrorCode);
            var lblActivityHistoryDetailsHeaderSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblActivityHistoryDetailsHeaderSeperator",
                "isVisible": true,
                "left": "0px",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "top": "59px",
                "width": "1420px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segActivityHistoryDetails = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "data": [{
                    "fontIconStatus": "Successful",
                    "lblActivityDescription": "Profile",
                    "lblActivityType": "Profile",
                    "lblDateAndTime": "02/11/17 - 8:00 AM",
                    "lblErrorCode": "N.A",
                    "lblModuleName": "Profile Management",
                    "lblReferenceID": "23432344",
                    "lblSeperator": ".",
                    "lblStatus": "Successful"
                }, {
                    "fontIconStatus": "Successful",
                    "lblActivityDescription": "Profile",
                    "lblActivityType": "Profile",
                    "lblDateAndTime": "02/11/17 - 8:00 AM",
                    "lblErrorCode": "N.A",
                    "lblModuleName": "Profile Management",
                    "lblReferenceID": "23432344",
                    "lblSeperator": ".",
                    "lblStatus": "Successful"
                }, {
                    "fontIconStatus": "Successful",
                    "lblActivityDescription": "Profile",
                    "lblActivityType": "Profile",
                    "lblDateAndTime": "02/11/17 - 8:00 AM",
                    "lblErrorCode": "N.A",
                    "lblModuleName": "Profile Management",
                    "lblReferenceID": "23432344",
                    "lblSeperator": ".",
                    "lblStatus": "Successful"
                }],
                "groupCells": false,
                "id": "segActivityHistoryDetails",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustMangActivityHistoryDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCloumns": "flxCloumns",
                    "flxCustMangActivityHistoryDetails": "flxCustMangActivityHistoryDetails",
                    "flxStatus": "flxStatus",
                    "fontIconStatus": "fontIconStatus",
                    "lblActivityDescription": "lblActivityDescription",
                    "lblActivityType": "lblActivityType",
                    "lblDateAndTime": "lblDateAndTime",
                    "lblErrorCode": "lblErrorCode",
                    "lblModuleName": "lblModuleName",
                    "lblReferenceID": "lblReferenceID",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus"
                },
                "width": "1420px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgActivityHistoryDetails = new kony.ui.RichText({
                "bottom": "200px",
                "centerX": "50%",
                "id": "rtxMsgActivityHistoryDetails",
                "isVisible": false,
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "250px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilterActivityDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilterActivityDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "120px",
                "zIndex": 11
            }, {}, {});
            flxFilterActivityDetails.setDefaultUnit(kony.flex.DP);
            var statusFilterMenuDetails = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenuDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterActivityDetails.add(statusFilterMenuDetails);
            flxActivityHistorySelection.add(flxActivityHistoryDetailsHeader, lblActivityHistoryDetailsHeaderSeperator, segActivityHistoryDetails, rtxMsgActivityHistoryDetails, flxFilterActivityDetails);
            flxActivityHistorySessionDetails.add(flxBackToActivitySessions, flxSessionDetails, flxActivityHistorySelection);
            flxActivityHistoryWrapper.add(flxActivityHistoryListing, flxActivityHistorySessionDetails);
            flxOtherInfoDetails.add(flxActivityHistoryWrapper);
            flxOtherInfoWrapper.add(flxOtherInfoTabs, flxTabsSeperator, flxOtherInfoDetails);
            flxGeneralInformationWrapper.add(flxGeneralInfoWrapper, flxOtherInfoWrapper);
            var flxEntitlementsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "35px",
                "id": "flxEntitlementsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "36px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "93px",
                "width": "200px",
                "zIndex": 2
            }, {}, {});
            flxEntitlementsContextualMenu.setDefaultUnit(kony.flex.DP);
            var entitlementsContextualMenu = new com.adminConsole.common.contextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "35px",
                "id": "entitlementsContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu": {
                        "bottom": "viz.val_cleared",
                        "height": "35px",
                        "maxHeight": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "imgOption1": {
                        "src": "configure2x.png"
                    },
                    "imgOption2": {
                        "src": "delete_2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblOption1": {
                        "text": "Configure"
                    },
                    "lblOption2": {
                        "text": "Remove"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEntitlementsContextualMenu.add(entitlementsContextualMenu);
            var ResetPasswordToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ResetPasswordToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "425dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "67%",
                        "right": "150px",
                        "top": "425dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px",
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledResetPasswordLink\")",
                        "width": "93%"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CSRAssistToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "58dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "80px",
                        "top": "58dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var EnrolToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EnrolToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "130px",
                "skin": "slFbox",
                "top": "242dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "40%",
                        "right": "130px",
                        "top": "242dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledEnrollLink\")"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "38px",
                "skin": "sknFlxTrans",
                "top": "60dp",
                "width": "200px",
                "overrides": {
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "38px",
                        "top": "60dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContent.add(flxGeneralInformationWrapper, flxEntitlementsContextualMenu, ResetPasswordToolTip, CSRAssistToolTip, EnrolToolTip, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 14
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "segNotes": {
                        "data": [
                            [{
                                    "fonticonArrow": "",
                                    "imgArrow": "img_down_arrow.png",
                                    "lblDate": "October 21, 2017"
                                },
                                [{
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }, {
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxBreadCrumbs, flxModifySearch, flxHeaderDropdown, flxMainContent, flxNote, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "80dp",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "isVisible": false,
                        "width": "83px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxPopupSelectEnrolEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupSelectEnrolEmail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopupSelectEnrolEmail.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSelectEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblPopupHeader = new kony.ui.Label({
                "id": "lblPopupHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.SendEnrollHeader\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            flxSelectEmailHeader.add(lblPopupHeader, flxPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Selectenroll\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstboxEmails = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "35px",
                "id": "lstboxEmails",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select", "kony.i18n.getLocalizedString(\"i18n.frmCustomers.selectEmail\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxPopupBody.add(flxSelectEmailHeader, lblPopUpMainMessage, lstboxEmails);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxPopUp.add(flxPopUpTopColor, flxPopupBody, flxPopUpButtons);
            flxPopupSelectEnrolEmail.add(flxPopUp);
            var flxEnableEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnableEstatement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnableEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatementTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEstatementTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementTopColor.setDefaultUnit(kony.flex.DP);
            flxEstatementTopColor.add();
            var flxEstatementInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEstatementInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementInner.setDefaultUnit(kony.flex.DP);
            var flxEstatementClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEstatementClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxEstatementClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementClose.add(lblPopUpClose, fontIconImgCLose);
            var lblEnableEstatement = new kony.ui.Label({
                "height": "20px",
                "id": "lblEnableEstatement",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnableEstatement\")",
                "top": "40px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEstatementHeader = new kony.ui.Label({
                "height": "20px",
                "id": "lblEstatementHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Following email ID will be used to send e-Statements",
                "top": "80px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxEstatementEmail = new kony.ui.ListBox({
                "bottom": "60px",
                "height": "30px",
                "id": "lbxEstatementEmail",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "120px",
                "width": "400px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblEstatementMessage = new kony.ui.Label({
                "bottom": "10px",
                "height": "40px",
                "id": "lblEstatementMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Once eStatements service is activated, the customer will stop receiving his/her account statements on paper for the selected accounts",
                "top": "170px",
                "width": "560px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementInner.add(flxEstatementClose, lblEnableEstatement, lblEstatementHeader, lbxEstatementEmail, lblEstatementMessage);
            var flxEstatementButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEstatementButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementButtons.setDefaultUnit(kony.flex.DP);
            var btnEstatementLeave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEstatementLeave",
                "isVisible": true,
                "right": "150px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnEstatementEnable = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEstatementEnable",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesEnable\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEstatementButtons.add(btnEstatementLeave, btnEstatementEnable);
            flxEstatement.add(flxEstatementTopColor, flxEstatementInner, flxEstatementButtons);
            flxEnableEstatement.add(flxEstatement);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxAlertPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAlertPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpClose.add(lblAlertPopupClose);
            var lblAlertPopUpHeader = new kony.ui.Label({
                "id": "lblAlertPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Preview Mode",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxAlertPopUpClose, lblAlertPopUpHeader);
            var flxAlertProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxAlertProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "SMS",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "PUSH",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "EMAIL",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "NOTIFICATION CENTER",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxAlertProductsTabs.add(flxAlertProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "Summer discount",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": true,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Title:Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n\nGet Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxAlertPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAlertPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnAlertPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpButtons.add(btnAlertPopUpCancel);
            flxAlertPopUp.add(flxAlertPopUpTopColor, flxPreviewPopupHeader, flxAlertProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxAlertPopUpButtons);
            flxPreviewPopup.add(flxAlertPopUp);
            var flxLinkProfilesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLinkProfilesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLinkProfilesPopup.setDefaultUnit(kony.flex.DP);
            var linkProfilesPopup = new com.adminConsole.businessBanking.linkProfilesPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "linkProfilesPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLinkProfilesPopup.add(linkProfilesPopup);
            var flxDelinkProfilePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDelinkProfilePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxDelinkProfilePopup.setDefaultUnit(kony.flex.DP);
            var delinkProfilePopup = new com.adminConsole.customerMang.delinkProfilePopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "delinkProfilePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDelinkProfilePopup.add(delinkProfilePopup);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxPopUpConfirmation, flxPopUpError, flxPopupSelectEnrolEmail, flxEnableEstatement, flxPreviewPopup, flxLinkProfilesPopup, flxDelinkProfilePopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerProfileActivityHistory,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerProfileActivityHistory",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f2fb96ba627e482b854c5d5828be4c3a(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_e7af5944164342d0938f9545b176d0de,
            "retainScrollPosition": false
        }]
    }
});