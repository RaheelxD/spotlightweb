define("CustomerManagementModule/frmCustomerProfileDueDiligence", function() {
    return function(controller) {
        function addWidgetsfrmCustomerProfileDueDiligence() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "SEARCH"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "784px",
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "126px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationWrapper.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoWrapper = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxGeneralInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "700dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "ProfileGeneralInfo": {
                        "maxHeight": "700dp"
                    },
                    "generalInfoHeader.flxOptions": {
                        "right": "2px"
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOtherInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxOtherInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOtherInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0hfd18814fd664dCM",
                "overrides": {
                    "btnTabName4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")"
                    },
                    "flxLeftArrow": {
                        "isVisible": false
                    },
                    "flxTabs": {
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "tabs": {
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOtherInfoTabs.add(tabs);
            var flxTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTabsSeperator.add();
            var flxOtherInfoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherInfoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoDetails.setDefaultUnit(kony.flex.DP);
            var flxToggleButtonsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxToggleButtonsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToggleButtonsContainer.setDefaultUnit(kony.flex.DP);
            var toggleButtons = new com.adminConsole.common.toggleButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "30dp",
                "id": "toggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnToggleLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailBanking\")"
                    },
                    "btnToggleRight": {
                        "text": "Business Banking"
                    },
                    "toggleButtons": {
                        "height": "30dp",
                        "left": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblToggleRolesHorzLine = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblToggleRolesHorzLine",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggleButtonsContainer.add(toggleButtons, lblToggleRolesHorzLine);
            var flxGroupsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxGroupsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupsWrapper.setDefaultUnit(kony.flex.DP);
            var flxDueDiligenceLeftTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-2dp",
                "clipBounds": false,
                "id": "flxDueDiligenceLeftTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1px",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9BorderE4E6EC3pxRad",
                "top": "-2dp",
                "width": "230dp",
                "zIndex": 2
            }, {}, {});
            flxDueDiligenceLeftTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.dueDiligence.verticalTabs.verticalTabs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.Citizenship&Tax\")"
                    },
                    "btnSubOption31": {
                        "text": "SEND PAYMENTS"
                    },
                    "btnSubOption32": {
                        "text": "RECEIVE PAYMENTS"
                    },
                    "verticalTabs": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDueDiligenceLeftTabs.add(verticalTabs);
            var flxDueDiligenceRightDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxDueDiligenceRightDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxFFFFFFKA",
                "top": "0dp",
                "zIndex": 3
            }, {}, {});
            flxDueDiligenceRightDetails.setDefaultUnit(kony.flex.DP);
            var flxDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails1.setDefaultUnit(kony.flex.DP);
            var lblHeader = new kony.ui.Label({
                "id": "lblHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Citizenship Details",
                "textStyle": {},
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "258px",
                "id": "flxAddDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "60dp",
                "width": "715px",
                "zIndex": 1
            }, {}, {});
            flxAddDetails.setDefaultUnit(kony.flex.DP);
            var imgAddDetails = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "35%",
                "height": "72px",
                "id": "imgAddDetails",
                "isVisible": true,
                "left": "22dp",
                "skin": "slImage",
                "src": "customerrole.png",
                "width": "144px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddDetails = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAddDetails",
                "isVisible": true,
                "left": "71dp",
                "skin": "sknLbl192B45LatoSemiBold13px",
                "text": "To add citizenship details click the ADD button",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddCitizenshipDetails = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "30dp",
                "id": "btnAddCitizenshipDetails",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "ADD",
                "top": "20dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddDetails.add(imgAddDetails, lblAddDetails, btnAddCitizenshipDetails);
            flxDetails1.add(lblHeader, flxAddDetails);
            var flxDetails2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "318dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails2.setDefaultUnit(kony.flex.DP);
            var lblTaxHeader = new kony.ui.Label({
                "id": "lblTaxHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Tax Details",
                "textStyle": {},
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddTaxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "258px",
                "id": "flxAddTaxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "sknFlxBordere1e5eedCustomRadius5px",
                "top": "60dp",
                "width": "715px",
                "zIndex": 1
            }, {}, {});
            flxAddTaxDetails.setDefaultUnit(kony.flex.DP);
            var imgAddDetails2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "35%",
                "height": "72px",
                "id": "imgAddDetails2",
                "isVisible": true,
                "left": "22dp",
                "skin": "slImage",
                "src": "customerrole.png",
                "width": "144px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddTaxDetails = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAddTaxDetails",
                "isVisible": true,
                "left": "71dp",
                "skin": "sknLbl192B45LatoSemiBold13px",
                "text": "To add tax details click the ADD button",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddTaxDetails = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "30dp",
                "id": "btnAddTaxDetails",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "ADD",
                "top": "20dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddTaxDetails.add(imgAddDetails2, lblAddTaxDetails, btnAddTaxDetails);
            flxDetails2.add(lblTaxHeader, flxAddTaxDetails);
            var flxCitizenshipAndTax = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxCitizenshipAndTax",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "18px",
                "zIndex": 3
            }, {}, {});
            flxCitizenshipAndTax.setDefaultUnit(kony.flex.DP);
            var addCitizenshipTemplate = new com.adminConsole.dueDiligence.addTemplate.addTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "addCitizenshipTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addTemplate": {
                        "isVisible": true
                    },
                    "btnAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonAdd\")"
                    },
                    "imgAddDetails": {
                        "src": "customerrole.png"
                    },
                    "lblAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.ToAddCitizenshipDetails\")"
                    },
                    "lblHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.citizenshipDetails\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAlertsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAlertsSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsSegment.setDefaultUnit(kony.flex.DP);
            var lblSegHeaderName = new kony.ui.Label({
                "id": "lblSegHeaderName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.citizenshipDetails\")",
                "textStyle": {},
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddCitizenship = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "20dp",
                "id": "btnAddCitizenship",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "ADD",
                "top": "18dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddedCitizenshipDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "-6dp",
                "clipBounds": true,
                "id": "flxAddedCitizenshipDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "top": "50dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAddedCitizenshipDetails.setDefaultUnit(kony.flex.DP);
            var flxAlertListSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32px",
                "id": "flxAlertListSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9Border1pxRad4px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertNameContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertNameContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxAlertNameContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.country\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertNameContainer.add(lblAlertHeaderName);
            var flxAlertCodeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertCodeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "24%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "16.20%",
                "zIndex": 1
            }, {}, {});
            flxAlertCodeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderCode",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.citizenship\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCitizenshipInfoIcon = new kony.ui.Label({
                "id": "lblCitizenshipInfoIcon",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertCodeContainer.add(lblAlertHeaderCode, lblCitizenshipInfoIcon);
            var flxAlertTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "43%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15.20%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.residency\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblResidencynfoIcon = new kony.ui.Label({
                "id": "lblResidencynfoIcon",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeContainer.add(lblAlertHeaderType, lblResidencynfoIcon);
            var flxAlertStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.statutoryPeriod\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblStatutorynfoIcon = new kony.ui.Label({
                "id": "lblStatutorynfoIcon",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertStatusContainer.add(lblAlertHeaderStatus, lblStatutorynfoIcon);
            var lblAlertSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblAlertSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertListSegHeader.add(flxAlertNameContainer, flxAlertCodeContainer, flxAlertTypeContainer, flxAlertStatusContainer, lblAlertSeperator);
            var CopylblSeperator0d3f61fd02b2345 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "CopylblSeperator0d3f61fd02b2345",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "top": "36dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertListSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegment.setDefaultUnit(kony.flex.DP);
            var segListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segListing",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_d898d5eaf17548d398fc6a18e2acee55,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertListSegment.add(segListing);
            flxAddedCitizenshipDetails.add(flxAlertListSegHeader, CopylblSeperator0d3f61fd02b2345, flxAlertListSegment);
            var flxListingSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxListingSegmentWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxListingSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var rtxNoResultFound = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxNoResultFound",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListingSegmentWrapper.add(rtxNoResultFound);
            flxAlertsSegment.add(lblSegHeaderName, btnAddCitizenship, flxAddedCitizenshipDetails, flxListingSegmentWrapper);
            var addTaxTemplate = new com.adminConsole.dueDiligence.addTemplate.addTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "addTaxTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "width": "100%",
                "overrides": {
                    "addTemplate": {
                        "isVisible": true,
                        "top": "viz.val_cleared"
                    },
                    "btnAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonAdd\")"
                    },
                    "imgAddDetails": {
                        "src": "customerrole.png"
                    },
                    "lblAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.ToAddTaxDetails\")",
                        "right": "viz.val_cleared"
                    },
                    "lblHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.TaxDetails\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxTaxSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": false,
                "id": "flxTaxSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxSegment.setDefaultUnit(kony.flex.DP);
            var lblTaxSegHeaderName = new kony.ui.Label({
                "id": "lblTaxSegHeaderName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.TaxDetails\")",
                "textStyle": {},
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddTax = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "20dp",
                "id": "btnAddTax",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "ADD",
                "top": "18dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddedTaxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "-6dp",
                "clipBounds": true,
                "id": "flxAddedTaxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "top": "50dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAddedTaxDetails.setDefaultUnit(kony.flex.DP);
            var flxTaxListSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32px",
                "id": "flxTaxListSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9Border1pxRad4px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxListSegHeader.setDefaultUnit(kony.flex.DP);
            var flxTaxCountryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxTaxCountryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxTaxCountryHeader.setDefaultUnit(kony.flex.DP);
            var lblTaxCountryHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTaxCountryHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.country\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxTaxCountryHeader.add(lblTaxCountryHeader);
            var flxTaxIdentityHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxTaxIdentityHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "43%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxTaxIdentityHeader.setDefaultUnit(kony.flex.DP);
            var lblTaxIdentity = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTaxIdentity",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.TAXIDENTIFICATIONNUMBER\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxTaxIdentityHeader.add(lblTaxIdentity);
            var lblTaxSegSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblTaxSegSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTaxListSegHeader.add(flxTaxCountryHeader, flxTaxIdentityHeader, lblTaxSegSeperator);
            var lblSeperatorTax = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblSeperatorTax",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "top": "36dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTaxListSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTaxListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxListSegment.setDefaultUnit(kony.flex.DP);
            var segTaxListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segTaxListing",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_d898d5eaf17548d398fc6a18e2acee55,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTaxListSegment.add(segTaxListing);
            flxAddedTaxDetails.add(flxTaxListSegHeader, lblSeperatorTax, flxTaxListSegment);
            var flxTaxSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTaxSegmentWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var rtxNoTaxResultFound = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxNoTaxResultFound",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTaxSegmentWrapper.add(rtxNoTaxResultFound);
            flxTaxSegment.add(lblTaxSegHeaderName, btnAddTax, flxAddedTaxDetails, flxTaxSegmentWrapper);
            flxCitizenshipAndTax.add(addCitizenshipTemplate, flxAlertsSegment, addTaxTemplate, flxTaxSegment);
            var citizenshipInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "citizenshipInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "17%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": "87dp",
                "width": "140px",
                "zIndex": 10,
                "overrides": {
                    "flxArrowImage": {
                        "width": "100%"
                    },
                    "flxContainer": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "17%",
                        "top": "87dp",
                        "width": "140px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "centerX": "50%",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.IsCitizen\")",
                        "top": "8dp",
                        "width": "95%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var residencyInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "residencyInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "44.5%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": "87dp",
                "width": "145px",
                "zIndex": 10,
                "overrides": {
                    "flxArrowImage": {
                        "width": "100%"
                    },
                    "flxContainer": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "imgUpArrow": {
                        "height": "12dp",
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "src": "uparrow_2x.png",
                        "width": "15dp"
                    },
                    "infoIconComponent": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "44.5%",
                        "top": "87dp",
                        "width": "145px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "centerX": "49%",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.IsResident\")",
                        "top": "8dp",
                        "width": "95%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var statutoryInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statutoryInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "69%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": "87dp",
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "flxArrowImage": {
                        "width": "100%"
                    },
                    "flxContainer": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "69%",
                        "top": "87dp",
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "centerX": "50%",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.minStatutoryPeriod\")",
                        "top": "8dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDueDiligenceRightDetails.add(flxDetails1, flxDetails2, flxCitizenshipAndTax, citizenshipInfoIconComponent, residencyInfoIconComponent, statutoryInfoIconComponent);
            var flxEmploymentRightDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxEmploymentRightDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "18px",
                "zIndex": 3
            }, {}, {});
            flxEmploymentRightDetails.setDefaultUnit(kony.flex.DP);
            var addTemplate = new com.adminConsole.dueDiligence.addTemplate.addTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "addTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFFKA",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addTemplate": {
                        "isVisible": true
                    },
                    "btnAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")"
                    },
                    "imgAddDetails": {
                        "src": "customerrole.png"
                    },
                    "lblAddDetails": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentDetailsClick\")"
                    },
                    "lblHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentDetails\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEmploymentDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxEmploymentDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxEmploymentDetails.setDefaultUnit(kony.flex.DP);
            var lblEmployHeader = new kony.ui.Label({
                "id": "lblEmployHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentDetails\")",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperatorEmploy = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblSeperatorEmploy",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblSeperator",
                "top": "36dp",
                "width": "94%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmployDetailsBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployDetailsBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployDetailsBody.setDefaultUnit(kony.flex.DP);
            var flxEmployRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxEmploymentType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmploymentType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentType.setDefaultUnit(kony.flex.DP);
            var lblEmployStatus = new kony.ui.Label({
                "id": "lblEmployStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentType\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployStatusDetail = new kony.ui.Label({
                "id": "lblEmployStatusDetail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentType.add(lblEmployStatus, lblEmployStatusDetail);
            var flxEmploymentRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmploymentRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentRole.setDefaultUnit(kony.flex.DP);
            var lblEmployRole = new kony.ui.Label({
                "id": "lblEmployRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentRole\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployRoleDetails = new kony.ui.Label({
                "id": "lblEmployRoleDetails",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Full Time",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentRole.add(lblEmployRole, lblEmployRoleDetails);
            var flxOccupation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxOccupation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxOccupation.setDefaultUnit(kony.flex.DP);
            var lblEmployOccupation = new kony.ui.Label({
                "id": "lblEmployOccupation",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.occupation\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOccupationDetails = new kony.ui.Label({
                "id": "lblOccupationDetails",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Manual",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOccupation.add(lblEmployOccupation, lblOccupationDetails);
            flxEmployRoleDetails.add(flxEmploymentType, flxEmploymentRole, flxOccupation);
            var flxEmployStartEndDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployStartEndDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployStartEndDetails.setDefaultUnit(kony.flex.DP);
            var flxEmployStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmployStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmployStartDate.setDefaultUnit(kony.flex.DP);
            var lblStartDate = new kony.ui.Label({
                "id": "lblStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.startDateCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployStartDate = new kony.ui.Label({
                "id": "lblEmployStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmployStartDate.add(lblStartDate, lblEmployStartDate);
            var flxEmployEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmployEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "4%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmployEndDate.setDefaultUnit(kony.flex.DP);
            var lblEndDate = new kony.ui.Label({
                "id": "lblEndDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.endDateCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployEndDate = new kony.ui.Label({
                "id": "lblEmployEndDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmployEndDate.add(lblEndDate, lblEmployEndDate);
            flxEmployStartEndDetails.add(flxEmployStartDate, flxEmployEndDate);
            var flxEmployName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployName.setDefaultUnit(kony.flex.DP);
            var flxEmployerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmployerName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxEmployerName.setDefaultUnit(kony.flex.DP);
            var lblNameHeader = new kony.ui.Label({
                "id": "lblNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employerName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployName = new kony.ui.Label({
                "id": "lblEmployName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Full Time",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmployerName.add(lblNameHeader, lblEmployName);
            flxEmployName.add(flxEmployerName);
            var flxEmployBusinessType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployBusinessType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployBusinessType.setDefaultUnit(kony.flex.DP);
            var lblBusinessTypeHeader = new kony.ui.Label({
                "id": "lblBusinessTypeHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessType\")",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmployBusiness = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployBusiness",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployBusiness.setDefaultUnit(kony.flex.DP);
            var flxEmployBusinessCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmployBusinessCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmployBusinessCategory.setDefaultUnit(kony.flex.DP);
            var lblEmployCategoryHeader = new kony.ui.Label({
                "id": "lblEmployCategoryHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessCategory\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployCategory = new kony.ui.Label({
                "id": "lblEmployCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Forestry ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmployBusinessCategory.add(lblEmployCategoryHeader, lblEmployCategory);
            var flxEmployDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmployDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmployDescription.setDefaultUnit(kony.flex.DP);
            var lblEmployDescriptionHeader = new kony.ui.Label({
                "id": "lblEmployDescriptionHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessDesc\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployDescription = new kony.ui.Label({
                "id": "lblEmployDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Hunting and other services",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmployDescription.add(lblEmployDescriptionHeader, lblEmployDescription);
            flxEmployBusiness.add(flxEmployBusinessCategory, flxEmployDescription);
            var flxDetailedDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailedDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxDetailedDescription.setDefaultUnit(kony.flex.DP);
            var flxDetailDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxDetailDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxDetailDescHeader.setDefaultUnit(kony.flex.DP);
            var lblDetailDescHeader = new kony.ui.Label({
                "id": "lblDetailDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessDetailDesc\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmployDetailedDescription = new kony.ui.Label({
                "id": "lblEmployDetailedDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Growing Of cereals",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailDescHeader.add(lblDetailDescHeader, lblEmployDetailedDescription);
            flxDetailedDescription.add(flxDetailDescHeader);
            flxEmployBusinessType.add(lblBusinessTypeHeader, flxEmployBusiness, flxDetailedDescription);
            var flxEmployBusinessAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmployBusinessAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployBusinessAddress.setDefaultUnit(kony.flex.DP);
            var lblBusinessAddressHeader = new kony.ui.Label({
                "id": "lblBusinessAddressHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessAddDetail\")",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAdressContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdressContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAdressContainer.setDefaultUnit(kony.flex.DP);
            var flxBusinessAddressEmploy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "flxBusinessAddressEmploy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxBusinessAddressEmploy.setDefaultUnit(kony.flex.DP);
            var lblBusinessAddressHead = new kony.ui.Label({
                "id": "lblBusinessAddressHead",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessAdd\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBusinessAddressLine1 = new kony.ui.Label({
                "id": "lblBusinessAddressLine1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "8357, Granville Street, Vancover, BC, Canada",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBusinessAddressLine2 = new kony.ui.Label({
                "id": "lblBusinessAddressLine2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "apt,33b",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessAddressEmploy.add(lblBusinessAddressHead, lblBusinessAddressLine1, lblBusinessAddressLine2);
            flxAdressContainer.add(flxBusinessAddressEmploy);
            flxEmployBusinessAddress.add(lblBusinessAddressHeader, flxAdressContainer);
            flxEmployDetailsBody.add(flxEmployRoleDetails, flxEmployStartEndDetails, flxEmployName, flxEmployBusinessType, flxEmployBusinessAddress);
            var flxStudentUnEmployeeDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStudentUnEmployeeDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStudentUnEmployeeDetails.setDefaultUnit(kony.flex.DP);
            var flxRoleAndDateDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleAndDateDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxRoleAndDateDetails.setDefaultUnit(kony.flex.DP);
            var flxRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxRole.setDefaultUnit(kony.flex.DP);
            var lblRoleHeader = new kony.ui.Label({
                "id": "lblRoleHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "text": "EMPLOYMENT TYPE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmploymentType = new kony.ui.Label({
                "id": "lblEmploymentType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRole.add(lblRoleHeader, lblEmploymentType);
            var flxWorkStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxWorkStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxWorkStartDate.setDefaultUnit(kony.flex.DP);
            var lblStartDateHeader = new kony.ui.Label({
                "id": "lblStartDateHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "START DATE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStartDateValue = new kony.ui.Label({
                "id": "lblStartDateValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "11/112000",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWorkStartDate.add(lblStartDateHeader, lblStartDateValue);
            var flxWorkEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxWorkEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxWorkEndDate.setDefaultUnit(kony.flex.DP);
            var lblEndDateHeader = new kony.ui.Label({
                "id": "lblEndDateHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "text": "END DATE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEndDateValue = new kony.ui.Label({
                "id": "lblEndDateValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "12/12/2020",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWorkEndDate.add(lblEndDateHeader, lblEndDateValue);
            flxRoleAndDateDetails.add(flxRole, flxWorkStartDate, flxWorkEndDate);
            var flxSecurityBenefits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSecurityBenefits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSecurityBenefits.setDefaultUnit(kony.flex.DP);
            var flxBenefitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxBenefitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxBenefitsHeader.setDefaultUnit(kony.flex.DP);
            var lblSecurityHeader = new kony.ui.Label({
                "id": "lblSecurityHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "text": "DOES THE CUSTOMER RECEIVE SOCIAL/SECURITY BENEFITS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSecurityBenefits = new kony.ui.Label({
                "id": "lblSecurityBenefits",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Yes ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBenefitsHeader.add(lblSecurityHeader, lblSecurityBenefits);
            flxSecurityBenefits.add(flxBenefitsHeader);
            flxStudentUnEmployeeDetails.add(flxRoleAndDateDetails, flxSecurityBenefits);
            var btnEditEmploymentDetails = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "20dp",
                "id": "btnEditEmploymentDetails",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.Edit\")",
                "top": "0dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentDetails.add(lblEmployHeader, lblSeperatorEmploy, flxEmployDetailsBody, flxStudentUnEmployeeDetails, btnEditEmploymentDetails);
            flxEmploymentRightDetails.add(addTemplate, flxEmploymentDetails);
            var flxAccountUsageDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxAccountUsageDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "18px",
                "zIndex": 3
            }, {}, {});
            flxAccountUsageDetails.setDefaultUnit(kony.flex.DP);
            var lblAccountHeader = new kony.ui.Label({
                "id": "lblAccountHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Account Usage Details (Send Payments)",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperatorAccount = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblSeperatorAccount",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblSeperator",
                "top": "36dp",
                "width": "94%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAccountDetailsBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountDetailsBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountDetailsBody.setDefaultUnit(kony.flex.DP);
            var flxAccountCountriesDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountCountriesDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountCountriesDetails.setDefaultUnit(kony.flex.DP);
            var flxCountries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxCountries",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCountries.setDefaultUnit(kony.flex.DP);
            var lblCountryListHeader = new kony.ui.Label({
                "id": "lblCountryListHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "text": "LIST OF COUNTRIES THE CUSTOMER IS GOING TO MAKE PAYMENTS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCountryList = new kony.ui.Label({
                "id": "lblCountryList",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCountries.add(lblCountryListHeader, lblCountryList);
            flxAccountCountriesDetails.add(flxCountries);
            var flxPaymentsPerMonth = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPaymentsPerMonth",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPaymentsPerMonth.setDefaultUnit(kony.flex.DP);
            var flxNoOfPayments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxNoOfPayments",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxNoOfPayments.setDefaultUnit(kony.flex.DP);
            var lblNoOfPaymentsPerMonth = new kony.ui.Label({
                "id": "lblNoOfPaymentsPerMonth",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "text": "ESTIMATE EXPECTED NUMBER OF PAYMNETS PER MONTH",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPaymentPerMonth = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblPaymentPerMonth",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedNumberPayIcon = new kony.ui.Label({
                "id": "lblExpectedNumberPayIcon",
                "isVisible": true,
                "left": "47dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoOfPayments.add(lblNoOfPaymentsPerMonth, lblPaymentPerMonth, lblExpectedNumberPayIcon);
            var flxPaymentAmounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxPaymentAmounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "4%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxPaymentAmounts.setDefaultUnit(kony.flex.DP);
            var lblPaymentAmountsPerMonth = new kony.ui.Label({
                "id": "lblPaymentAmountsPerMonth",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "text": "ESTIMATE EXPECTED AMOUNT OF PAYMNETS PER MONTH",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmountPerMonth = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblAmountPerMonth",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Employee ",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmountPerMonthInfo = new kony.ui.Label({
                "id": "lblAmountPerMonthInfo",
                "isVisible": true,
                "left": "47dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPaymentAmounts.add(lblPaymentAmountsPerMonth, lblAmountPerMonth, lblAmountPerMonthInfo);
            flxPaymentsPerMonth.add(flxNoOfPayments, flxPaymentAmounts);
            var flxReasonOfPayment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReasonOfPayment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReasonOfPayment.setDefaultUnit(kony.flex.DP);
            var flxReason = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxReason",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxReason.setDefaultUnit(kony.flex.DP);
            var lblPaymentReasonHeader = new kony.ui.Label({
                "id": "lblPaymentReasonHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "PROVIDE A REASON FOR OUTGOING PAYMENTS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPaymentReason = new kony.ui.Label({
                "id": "lblPaymentReason",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Full Time",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReasonInfo = new kony.ui.Label({
                "id": "lblReasonInfo",
                "isVisible": true,
                "left": "257dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "-1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReason.add(lblPaymentReasonHeader, lblPaymentReason, lblReasonInfo);
            flxReasonOfPayment.add(flxReason);
            flxAccountDetailsBody.add(flxAccountCountriesDetails, flxPaymentsPerMonth, flxReasonOfPayment);
            var btnEditAccountUsageDetails = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "20dp",
                "id": "btnEditAccountUsageDetails",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "Edit",
                "top": "0dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SendAmountInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "SendAmountInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "59%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 157,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "left": "10dp",
                        "right": "0dp",
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "59%",
                        "top": 157,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "left": "10dp",
                        "text": "Estimated  approximate amount of payments per month customer is going to send to the selected countries.",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SendpaymentsInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "SendpaymentsInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "9.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 157,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "9.50%",
                        "top": 157,
                        "width": "160px",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SendReasonInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "SendReasonInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "45.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 229,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "45.50%",
                        "top": 229,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "text": "Reason for sending money to the selected countries."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountUsageDetails.add(lblAccountHeader, lblSeperatorAccount, flxAccountDetailsBody, btnEditAccountUsageDetails, SendAmountInfoIconComponent, SendpaymentsInfoIconComponent, SendReasonInfoIconComponent);
            flxGroupsWrapper.add(flxDueDiligenceLeftTabs, flxDueDiligenceRightDetails, flxEmploymentRightDetails, flxAccountUsageDetails);
            flxOtherInfoDetails.add(flxToggleButtonsContainer, flxGroupsWrapper);
            flxOtherInfoWrapper.add(flxOtherInfoTabs, flxTabsSeperator, flxOtherInfoDetails);
            flxGeneralInformationWrapper.add(flxGeneralInfoWrapper, flxOtherInfoWrapper);
            var flxEntitlementsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "35px",
                "id": "flxEntitlementsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "36px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "93px",
                "width": "200px",
                "zIndex": 2
            }, {}, {});
            flxEntitlementsContextualMenu.setDefaultUnit(kony.flex.DP);
            var entitlementsContextualMenu = new com.adminConsole.common.contextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "35px",
                "id": "entitlementsContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu": {
                        "bottom": "viz.val_cleared",
                        "height": "35px",
                        "maxHeight": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "imgOption1": {
                        "src": "configure2x.png"
                    },
                    "imgOption2": {
                        "src": "delete_2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblOption1": {
                        "text": "Configure"
                    },
                    "lblOption2": {
                        "text": "Remove"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEntitlementsContextualMenu.add(entitlementsContextualMenu);
            var ResetPasswordToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ResetPasswordToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "425dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "67%",
                        "right": "150px",
                        "top": "425dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px",
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledResetPasswordLink\")",
                        "width": "93%"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CSRAssistToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "58dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "80px",
                        "top": "58dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var EnrolToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EnrolToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "130px",
                "skin": "slFbox",
                "top": "242dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "40%",
                        "right": "130px",
                        "top": "242dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledEnrollLink\")"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "38px",
                "skin": "sknFlxTrans",
                "top": "60dp",
                "width": "200px",
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "38px",
                        "top": "60dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContent.add(flxGeneralInformationWrapper, flxEntitlementsContextualMenu, ResetPasswordToolTip, CSRAssistToolTip, EnrolToolTip, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 14
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "segNotes": {
                        "data": [
                            [{
                                    "fonticonArrow": "",
                                    "imgArrow": "img_down_arrow.png",
                                    "lblDate": "October 21, 2017"
                                },
                                [{
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }, {
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPanel.add(flxMainHeader, flxBreadCrumbs, flxModifySearch, flxMainContent, flxNote, flxLoading, flxHeaderDropdown);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "minWidth": "80px",
                        "right": "20px",
                        "text": "NO, LEAVE IT AS IT IS",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "text": "YES, OFFLINE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "isVisible": false,
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxPopupSelectEnrolEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupSelectEnrolEmail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopupSelectEnrolEmail.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSelectEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblPopupHeader = new kony.ui.Label({
                "id": "lblPopupHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.SendEnrollHeader\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            flxSelectEmailHeader.add(lblPopupHeader, flxPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Selectenroll\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstboxEmails = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "35px",
                "id": "lstboxEmails",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select", "kony.i18n.getLocalizedString(\"i18n.frmCustomers.selectEmail\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxPopupBody.add(flxSelectEmailHeader, lblPopUpMainMessage, lstboxEmails);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxPopUp.add(flxPopUpTopColor, flxPopupBody, flxPopUpButtons);
            flxPopupSelectEnrolEmail.add(flxPopUp);
            var flxEnableEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnableEstatement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnableEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatementTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEstatementTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementTopColor.setDefaultUnit(kony.flex.DP);
            flxEstatementTopColor.add();
            var flxEstatementInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEstatementInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementInner.setDefaultUnit(kony.flex.DP);
            var flxEstatementClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEstatementClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxEstatementClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementClose.add(lblPopUpClose, fontIconImgCLose);
            var lblEnableEstatement = new kony.ui.Label({
                "height": "20px",
                "id": "lblEnableEstatement",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnableEstatement\")",
                "top": "40px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEstatementHeader = new kony.ui.Label({
                "height": "20px",
                "id": "lblEstatementHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Following email ID will be used to send e-Statements",
                "top": "80px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxEstatementEmail = new kony.ui.ListBox({
                "bottom": "60px",
                "height": "30px",
                "id": "lbxEstatementEmail",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "120px",
                "width": "400px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblEstatementMessage = new kony.ui.Label({
                "bottom": "10px",
                "height": "40px",
                "id": "lblEstatementMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Once eStatements service is activated, the customer will stop receiving his/her account statements on paper for the selected accounts",
                "top": "170px",
                "width": "560px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementInner.add(flxEstatementClose, lblEnableEstatement, lblEstatementHeader, lbxEstatementEmail, lblEstatementMessage);
            var flxEstatementButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEstatementButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementButtons.setDefaultUnit(kony.flex.DP);
            var btnEstatementLeave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEstatementLeave",
                "isVisible": true,
                "right": "150px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnEstatementEnable = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEstatementEnable",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesEnable\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEstatementButtons.add(btnEstatementLeave, btnEstatementEnable);
            flxEstatement.add(flxEstatementTopColor, flxEstatementInner, flxEstatementButtons);
            flxEnableEstatement.add(flxEstatement);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxAlertPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAlertPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpClose.add(lblAlertPopupClose);
            var lblAlertPopUpHeader = new kony.ui.Label({
                "id": "lblAlertPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Preview Mode",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxAlertPopUpClose, lblAlertPopUpHeader);
            var flxAlertProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxAlertProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "SMS",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "PUSH",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "EMAIL",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "NOTIFICATION CENTER",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxAlertProductsTabs.add(flxAlertProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "Summer discount",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": true,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Title:Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n\nGet Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxAlertPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAlertPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnAlertPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpButtons.add(btnAlertPopUpCancel);
            flxAlertPopUp.add(flxAlertPopUpTopColor, flxPreviewPopupHeader, flxAlertProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxAlertPopUpButtons);
            flxPreviewPopup.add(flxAlertPopUp);
            var flxAddCitizenshipDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddCitizenshipDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxAddCitizenshipDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxAddDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "516px",
                "id": "flxAddDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxAddDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxAddCitizenshipPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAddCitizenshipPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddCitizenshipPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxAddCitizenshipPopupTopColor.add();
            var flxCitizenshipPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "82%",
                "id": "flxCitizenshipPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxCitizenshipPopupBody.setDefaultUnit(kony.flex.DP);
            var flxClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClosePopup.add(lblIconClosePopup);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.citizenshipDetails\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblName, lblSeperator);
            var flxCitizenshipWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "80%",
                "horizontalScrollIndicator": true,
                "id": "flxCitizenshipWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCitizenshipWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblViewRoleDescriptionValue = new kony.ui.Label({
                "id": "lblViewRoleDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Select_a_Country\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblViewRoleDescriptionValue);
            var flxCountryUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxCountryUser.setDefaultUnit(kony.flex.DP);
            var lstbxCountryUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCountryUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "305px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxNoCountryError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountryError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountryError.setDefaultUnit(kony.flex.DP);
            var lblNoCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountryError.add(lblNoCountryErrorIcon, lblNoCountryError);
            flxCountryUser.add(lstbxCountryUser, flxNoCountryError);
            var flxCitizenshipQuestions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCitizenshipQuestions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "525px",
                "zIndex": 1
            }, {}, {});
            flxCitizenshipQuestions.setDefaultUnit(kony.flex.DP);
            var flxIsCitizem = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxIsCitizem",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxIsCitizem.setDefaultUnit(kony.flex.DP);
            var lblType1 = new kony.ui.Label({
                "id": "lblType1",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.IsCitizen\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryType = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryType",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIsCitizem.add(lblType1, imgMandatoryType);
            var flxIsCitizenRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxIsCitizenRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxIsCitizenRadio.setDefaultUnit(kony.flex.DP);
            var flxRadio1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadio1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadio1.setDefaultUnit(kony.flex.DP);
            var imgRadio1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadio1",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadio1.add(imgRadio1);
            var lblRadioYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadio2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadio2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadio2.setDefaultUnit(kony.flex.DP);
            var imgRadio2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadio2",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadio2.add(imgRadio2);
            var lblRadioNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIsCitizenRadio.add(flxRadio1, lblRadioYes, flxRadio2, lblRadioNo);
            var flxEndDateCitizenship = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEndDateCitizenship",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxEndDateCitizenship.setDefaultUnit(kony.flex.DP);
            var lblDateEndedCitizenshipHeader = new kony.ui.Label({
                "id": "lblDateEndedCitizenshipHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.citizenshipEndDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateEndCitizenship = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxDateEndCitizenship",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306px"
            }, {}, {});
            flxDateEndCitizenship.setDefaultUnit(kony.flex.DP);
            var customEndDateCitizenship = new kony.ui.CustomWidget({
                "id": "customEndDateCitizenship",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "100%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxDateEndCitizenship.add(customEndDateCitizenship);
            var CitizenshipEndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CitizenshipEndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "top": "80dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEndDateCitizenship.add(lblDateEndedCitizenshipHeader, flxDateEndCitizenship, CitizenshipEndDateError);
            var flxIsResident = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxIsResident",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxIsResident.setDefaultUnit(kony.flex.DP);
            var lblType2 = new kony.ui.Label({
                "id": "lblType2",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.IsResident\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryType2 = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryType2",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIsResident.add(lblType2, imgMandatoryType2);
            var flxIsResidentRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxIsResidentRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxIsResidentRadio.setDefaultUnit(kony.flex.DP);
            var flxRadioYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioYes.setDefaultUnit(kony.flex.DP);
            var imgResidentRadio1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgResidentRadio1",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioYes.add(imgResidentRadio1);
            var lblYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioNo.setDefaultUnit(kony.flex.DP);
            var imgResidentRadio2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgResidentRadio2",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioNo.add(imgResidentRadio2);
            var lblNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIsResidentRadio.add(flxRadioYes, lblYes, flxRadioNo, lblNo);
            var flxEndDateResidency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEndDateResidency",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxEndDateResidency.setDefaultUnit(kony.flex.DP);
            var lblDateEndedResidencyHeader = new kony.ui.Label({
                "id": "lblDateEndedResidencyHeader",
                "isVisible": true,
                "left": "0%",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.residencyEndDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateEndedResidency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDateEndedResidency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306dp"
            }, {}, {});
            flxDateEndedResidency.setDefaultUnit(kony.flex.DP);
            var customEndDateResidency = new kony.ui.CustomWidget({
                "id": "customEndDateResidency",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxDateEndedResidency.add(customEndDateResidency);
            var ResidencyEndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ResidencyEndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEndDateResidency.add(lblDateEndedResidencyHeader, flxDateEndedResidency, ResidencyEndDateError);
            var flxCustomerStayDuration = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCustomerStayDuration",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCustomerStayDuration.setDefaultUnit(kony.flex.DP);
            var lblCustomerStayDuration = new kony.ui.Label({
                "id": "lblCustomerStayDuration",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.minStatutoryPeriod\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatory = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatory",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerStayDuration.add(lblCustomerStayDuration, imgMandatory);
            var flxCustomerStayRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCustomerStayRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerStayRadio.setDefaultUnit(kony.flex.DP);
            var flxYesRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxYesRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxYesRadio.setDefaultUnit(kony.flex.DP);
            var imgYesStayRadio = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgYesStayRadio",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxYesRadio.add(imgYesStayRadio);
            var lblYesOpt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYesOpt",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNoRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNoRadio.setDefaultUnit(kony.flex.DP);
            var imgNoStayRadio = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNoStayRadio",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRadio.add(imgNoStayRadio);
            var lblNoOpt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoOpt",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerStayRadio.add(flxYesRadio, lblYesOpt, flxNoRadio, lblNoOpt);
            flxCitizenshipQuestions.add(flxIsCitizem, flxIsCitizenRadio, flxEndDateCitizenship, flxIsResident, flxIsResidentRadio, flxEndDateResidency, flxCustomerStayDuration, flxCustomerStayRadio);
            flxCitizenshipWrapper.add(flxDescription, flxCountryUser, flxCitizenshipQuestions);
            flxCitizenshipPopupBody.add(flxClosePopup, flxName, flxCitizenshipWrapper);
            var flxDueButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 4,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDueButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "436dp",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxDueButtons.setDefaultUnit(kony.flex.DP);
            var flxDuePopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDuePopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDuePopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnDuePopUpSaveAndAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnDuePopUpSaveAndAdd",
                "isVisible": true,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.save&addAnother\")",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnDuePopUpSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnDuePopUpSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.save&close\")",
                "width": "147px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnDuePopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnDuePopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxDuePopUpButtons.add(btnDuePopUpSaveAndAdd, btnDuePopUpSave, btnDuePopUpCancel);
            flxDueButtons.add(flxDuePopUpButtons);
            flxAddDetailsContainer.add(flxAddCitizenshipPopupTopColor, flxCitizenshipPopupBody, flxDueButtons);
            flxAddCitizenshipDetailsPopup.add(flxAddDetailsContainer);
            var flxViewFeatureDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewFeatureDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewFeatureDetails.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxFeatureDetailsPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTop.setDefaultUnit(kony.flex.DP);
            flxBlueTop.add();
            var flxFeatureDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98.50%",
                "id": "flxFeatureDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFeatureDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFeatureDetailsClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblFontIconClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsClose.add(lblFontIconClose);
            var flxFeatureDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Feature Details -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDetailsHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Bill Payments",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsHeader.add(lblFeatureDetailsHeader1, lblFeatureDetailsHeader2);
            var flxFeatureStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatus.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatus = new kony.ui.Label({
                "id": "lblStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureStatus.add(fontIconActive, lblStatus);
            var lblSeparator = new kony.ui.Label({
                "bottom": "15px",
                "height": "1px",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "15px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFeatureDescription.setDefaultUnit(kony.flex.DP);
            var lblFeatureDescriptionHeading = new kony.ui.Label({
                "id": "lblFeatureDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDescriptionValue = new kony.ui.Label({
                "id": "lblFeatureDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDescription.add(lblFeatureDescriptionHeading, lblFeatureDescriptionValue);
            var flxActionsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "68%",
                "horizontalScrollIndicator": true,
                "id": "flxActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActionsList.setDefaultUnit(kony.flex.DP);
            var flxActionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxActionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 10
            }, {}, {});
            flxActionsHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderSeperator1 = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator1",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionName",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionHeader",
                "isVisible": true,
                "left": "40%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblHeaderSeperator2 = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator2",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionsHeader.add(lblHeaderSeperator1, lblActionName, lblDescriptionHeader, lblHeaderSeperator2);
            var segActions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }],
                "groupCells": false,
                "id": "segActions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxFeatureDetailsActions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatureDetailsActions": "flxFeatureDetailsActions",
                    "lblActionDescription": "lblActionDescription",
                    "lblActionName": "lblActionName",
                    "lblSeparator": "lblSeparator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxActionsList.add(flxActionsHeader, segActions, flxNoRecordsFound);
            flxFeatureDetailsContainer.add(flxFeatureDetailsClose, flxFeatureDetailsHeader, flxFeatureStatus, lblSeparator, flxFeatureDescription, flxActionsList);
            flxFeatureDetailsPopUp.add(flxBlueTop, flxFeatureDetailsContainer);
            flxViewFeatureDetails.add(flxFeatureDetailsPopUp);
            var flxLinkProfilesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLinkProfilesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLinkProfilesPopup.setDefaultUnit(kony.flex.DP);
            var linkProfilesPopup = new com.adminConsole.businessBanking.linkProfilesPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "linkProfilesPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLinkProfilesPopup.add(linkProfilesPopup);
            var flxDelinkProfilePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDelinkProfilePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxDelinkProfilePopup.setDefaultUnit(kony.flex.DP);
            var delinkProfilePopup = new com.adminConsole.customerMang.delinkProfilePopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "delinkProfilePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDelinkProfilePopup.add(delinkProfilePopup);
            var flxAddTaxDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddTaxDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxAddTaxDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxAddTaxDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "377px",
                "id": "flxAddTaxDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "351px",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "199px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxAddTaxDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxAddTaxPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAddTaxPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddTaxPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxAddTaxPopupTopColor.add();
            var flxTaxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "367px",
                "id": "flxTaxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxTaxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxCloseTaxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseTaxPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseTaxPopup.setDefaultUnit(kony.flex.DP);
            var CopylblIconClosePopup0cc3b0136aa404a = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "CopylblIconClosePopup0cc3b0136aa404a",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseTaxPopup.add(CopylblIconClosePopup0cc3b0136aa404a);
            var flxHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeaderName.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.TaxDetails\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTaxSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblTaxSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderName.add(lblHeaderName, lblTaxSeperator);
            var flxTaxWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "194px",
                "id": "flxTaxWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxWrapper.setDefaultUnit(kony.flex.DP);
            var flxSelectCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxSelectCountry.setDefaultUnit(kony.flex.DP);
            var lblSelectCountry = new kony.ui.Label({
                "id": "lblSelectCountry",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Select_a_Country\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectCountry.add(lblSelectCountry);
            var flxTaxCountryUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTaxCountryUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxTaxCountryUser.setDefaultUnit(kony.flex.DP);
            var lstbxTaxCountryUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxTaxCountryUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "305px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxTaxCountryError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxTaxCountryError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxTaxCountryError.setDefaultUnit(kony.flex.DP);
            var CopylblNoCountryErrorIcon0e2bf1776527b40 = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCountryErrorIcon0e2bf1776527b40",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblNoCountryError0cb492b77945f47 = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCountryError0cb492b77945f47",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTaxCountryError.add(CopylblNoCountryErrorIcon0e2bf1776527b40, CopylblNoCountryError0cb492b77945f47);
            flxTaxCountryUser.add(lstbxTaxCountryUser, flxTaxCountryError);
            var flxTaxIdNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTaxIdNum",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxTaxIdNum.setDefaultUnit(kony.flex.DP);
            var lblCityUser = new kony.ui.Label({
                "id": "lblCityUser",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.TaxIdentification\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxTaxIdNum = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxTaxIdNum",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.EnterTaxIdentificationNumber\")",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoCityError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCityError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCityError.setDefaultUnit(kony.flex.DP);
            var lblNoCityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.taxError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCityError.add(lblNoCityErrorIcon, lblNoCityError);
            flxTaxIdNum.add(lblCityUser, txtbxTaxIdNum, flxNoCityError);
            flxTaxWrapper.add(flxSelectCountry, flxTaxCountryUser, flxTaxIdNum);
            var flxDueButtonsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxDueButtonsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDueButtonsPopup.setDefaultUnit(kony.flex.DP);
            var flxTaxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxTaxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTaxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnTaxPopUpSaveAndAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnTaxPopUpSaveAndAdd",
                "isVisible": true,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnTaxPopUpSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnTaxPopUpSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & CLOSE",
                "width": "147px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnTaxPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnTaxPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxTaxPopUpButtons.add(btnTaxPopUpSaveAndAdd, btnTaxPopUpSave, btnTaxPopUpCancel);
            flxDueButtonsPopup.add(flxTaxPopUpButtons);
            flxTaxPopupBody.add(flxCloseTaxPopup, flxHeaderName, flxTaxWrapper, flxDueButtonsPopup);
            flxAddTaxDetailsContainer.add(flxAddTaxPopupTopColor, flxTaxPopupBody);
            flxAddTaxDetailsPopup.add(flxAddTaxDetailsContainer);
            var flxStudentDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStudentDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxStudentDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxStudentDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "396px",
                "id": "flxStudentDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "351px",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "199px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxStudentDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxStudentPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxStudentPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStudentPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxStudentPopupTopColor.add();
            var flxStudentPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "386px",
                "id": "flxStudentPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxStudentPopupBody.setDefaultUnit(kony.flex.DP);
            var flxCloseStudntPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseStudntPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseStudntPopup.setDefaultUnit(kony.flex.DP);
            var lblCloseIconPopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblCloseIconPopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseStudntPopup.add(lblCloseIconPopup);
            var flxEmpStudentHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmpStudentHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEmpStudentHeaderName.setDefaultUnit(kony.flex.DP);
            var lblStudentHeaderName = new kony.ui.Label({
                "id": "lblStudentHeaderName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Employment Details",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStudentSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblStudentSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmpStudentHeaderName.add(lblStudentHeaderName, lblStudentSeperator);
            var flxEmpStudentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "214px",
                "id": "flxEmpStudentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmpStudentWrapper.setDefaultUnit(kony.flex.DP);
            var flxEmpStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmpStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxEmpStatus.setDefaultUnit(kony.flex.DP);
            var lblSelectEmpStatus = new kony.ui.Label({
                "id": "lblSelectEmpStatus",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Employment Status",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmpStatus.add(lblSelectEmpStatus);
            var flxEmpStatusDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmpStatusDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxEmpStatusDetails.setDefaultUnit(kony.flex.DP);
            var lstbxEmpStatus = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxEmpStatus",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "305px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxEmpStatusError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxEmpStatusError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxEmpStatusError.setDefaultUnit(kony.flex.DP);
            var lblStudentErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblStudentErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStudentError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblStudentError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmpStatusError.add(lblStudentErrorIcon, lblStudentError);
            flxEmpStatusDetails.add(lstbxEmpStatus, flxEmpStatusError);
            var flxStudyCompletionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStudyCompletionDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxStudyCompletionDetails.setDefaultUnit(kony.flex.DP);
            var lblDate = new kony.ui.Label({
                "id": "lblDate",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Study Completion Date",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEndCal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxEndCal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "188dp"
            }, {}, {});
            flxEndCal.setDefaultUnit(kony.flex.DP);
            var flxCalendarEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0dp",
                "width": "188dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarEndDate.setDefaultUnit(kony.flex.DP);
            var customCalEndDate = new kony.ui.CustomWidget({
                "id": "customCalEndDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "maxDateRangeInMonths": "3",
                "minDateRangeInMonths": "0",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarEndDate.add(customCalEndDate);
            flxEndCal.add(flxCalendarEndDate);
            flxStudyCompletionDetails.add(lblDate, flxEndCal);
            var flxDateDetailsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDateDetailsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDateDetailsWrapper.setDefaultUnit(kony.flex.DP);
            var lblStartedDate = new kony.ui.Label({
                "id": "lblStartedDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.StartDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStudentDateStarted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxStudentDateStarted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306px"
            }, {}, {});
            flxStudentDateStarted.setDefaultUnit(kony.flex.DP);
            var customStudentStartDate = new kony.ui.CustomWidget({
                "id": "customStudentStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "100%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxStudentDateStarted.add(customStudentStartDate);
            var StudentStartDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "StudentStartDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblEndedDate = new kony.ui.Label({
                "id": "lblEndedDate",
                "isVisible": true,
                "left": "48%",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStudentDateEnded = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStudentDateEnded",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48%",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306dp"
            }, {}, {});
            flxStudentDateEnded.setDefaultUnit(kony.flex.DP);
            var customStudentEndDate = new kony.ui.CustomWidget({
                "id": "customStudentEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxStudentDateEnded.add(customStudentEndDate);
            var StudentEndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "StudentEndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDateDetailsWrapper.add(lblStartedDate, flxStudentDateStarted, StudentStartDateError, lblEndedDate, flxStudentDateEnded, StudentEndDateError);
            flxEmpStudentWrapper.add(flxEmpStatus, flxEmpStatusDetails, flxStudyCompletionDetails, flxDateDetailsWrapper);
            var flxEmpStudentPopupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEmpStudentPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmpStudentPopupButtons.setDefaultUnit(kony.flex.DP);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnStudentSaveAndAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnStudentSaveAndAdd",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnStudentSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnStudentSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "width": "91px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnStudentCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnStudentCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxButtons.add(btnStudentSaveAndAdd, btnStudentSave, btnStudentCancel);
            flxEmpStudentPopupButtons.add(flxButtons);
            flxStudentPopupBody.add(flxCloseStudntPopup, flxEmpStudentHeaderName, flxEmpStudentWrapper, flxEmpStudentPopupButtons);
            flxStudentDetailsContainer.add(flxStudentPopupTopColor, flxStudentPopupBody);
            flxStudentDetailsPopup.add(flxStudentDetailsContainer);
            var flxEmploymentStatusDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmploymentStatusDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "10dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxEmploymentStatusDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxEmploymentStatusDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "396px",
                "id": "flxEmploymentStatusDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "351px",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "199px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxEmploymentStatusPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEmploymentStatusPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxEmploymentStatusPopupTopColor.add();
            var flxEmploymentStatusPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "386px",
                "id": "flxEmploymentStatusPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusPopupBody.setDefaultUnit(kony.flex.DP);
            var flxCloseEmploymentStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseEmploymentStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseEmploymentStatus.setDefaultUnit(kony.flex.DP);
            var lblCloseEmploymentStatus = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblCloseEmploymentStatus",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseEmploymentStatus.add(lblCloseEmploymentStatus);
            var flxEmploymentDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmploymentDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEmploymentDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblEmploymentDetailsHeader = new kony.ui.Label({
                "id": "lblEmploymentDetailsHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentDetails\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmploymentStatusSeparator = new kony.ui.Label({
                "height": "1px",
                "id": "lblEmploymentStatusSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentDetailsHeader.add(lblEmploymentDetailsHeader, lblEmploymentStatusSeparator);
            var flxEmploymentStatusWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "214px",
                "id": "flxEmploymentStatusWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusWrapper.setDefaultUnit(kony.flex.DP);
            var flxEmploymentStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmploymentStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
            var lblSelectEmploymentStatus = new kony.ui.Label({
                "id": "lblSelectEmploymentStatus",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentType\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentStatus.add(lblSelectEmploymentStatus);
            var flxEmploymentStatusDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmploymentStatusDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusDetails.setDefaultUnit(kony.flex.DP);
            var lstBoxEmploymentStatus = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxEmploymentStatus",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Employment Status"],
                    ["lb2", "Employee"],
                    ["lb3", "Self- Employed"],
                    ["lb4", "Student"],
                    ["lb5", "Un- Employed"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "305px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxEmploymentStatusError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxEmploymentStatusError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusError.setDefaultUnit(kony.flex.DP);
            var lblEmploymentStatusErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblEmploymentStatusErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmploymentStatusError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblEmploymentStatusError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Select Employment Status",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentStatusError.add(lblEmploymentStatusErrorIcon, lblEmploymentStatusError);
            flxEmploymentStatusDetails.add(lstBoxEmploymentStatus, flxEmploymentStatusError);
            flxEmploymentStatusWrapper.add(flxEmploymentStatus, flxEmploymentStatusDetails);
            var flxEmploymentDetailsPopupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEmploymentDetailsPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentDetailsPopupButtons.setDefaultUnit(kony.flex.DP);
            var flxEmploymentStatusButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEmploymentStatusButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusButtons.setDefaultUnit(kony.flex.DP);
            var btnEmploymentStatusSaveAndAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEmploymentStatusSaveAndAdd",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnEmploymentStatusSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEmploymentStatusSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SAVE\")",
                "width": "91px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnEmploymentStatusCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEmploymentStatusCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxEmploymentStatusButtons.add(btnEmploymentStatusSaveAndAdd, btnEmploymentStatusSave, btnEmploymentStatusCancel);
            flxEmploymentDetailsPopupButtons.add(flxEmploymentStatusButtons);
            flxEmploymentStatusPopupBody.add(flxCloseEmploymentStatus, flxEmploymentDetailsHeader, flxEmploymentStatusWrapper, flxEmploymentDetailsPopupButtons);
            flxEmploymentStatusDetailsContainer.add(flxEmploymentStatusPopupTopColor, flxEmploymentStatusPopupBody);
            flxEmploymentStatusDetailsPopup.add(flxEmploymentStatusDetailsContainer);
            var flxUnemployDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUnemployDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxUnemployDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxUmemploytDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "426px",
                "id": "flxUmemploytDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "351px",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "199px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxUmemploytDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxUmemployPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxUmemployPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUmemployPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxUmemployPopupTopColor.add();
            var flxUmemployPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "416px",
                "id": "flxUmemployPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "665px",
                "zIndex": 1
            }, {}, {});
            flxUmemployPopupBody.setDefaultUnit(kony.flex.DP);
            var flxCloseUmemploy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseUmemploy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseUmemploy.setDefaultUnit(kony.flex.DP);
            var lblCloseIcon = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblCloseIcon",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseUmemploy.add(lblCloseIcon);
            var flxUmemployHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxUmemployHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxUmemployHeaderName.setDefaultUnit(kony.flex.DP);
            var lblUnemployHeaderName = new kony.ui.Label({
                "id": "lblUnemployHeaderName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.employmentDetails\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUnemploySeparator = new kony.ui.Label({
                "height": "1px",
                "id": "lblUnemploySeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUmemployHeaderName.add(lblUnemployHeaderName, lblUnemploySeparator);
            var flxUmemployWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUmemployWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "73px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUmemployWrapper.setDefaultUnit(kony.flex.DP);
            var flxEmpStatus1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmpStatus1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxEmpStatus1.setDefaultUnit(kony.flex.DP);
            var lblSelectEmpStatus1 = new kony.ui.Label({
                "id": "lblSelectEmpStatus1",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentType\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmpStatus1.add(lblSelectEmpStatus1);
            var flxUnemployDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUnemployDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxUnemployDetails.setDefaultUnit(kony.flex.DP);
            var lstbxUnemployStatus = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxUnemployStatus",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["E", "Employed - full time"],
                    ["S", "Self employed"],
                    ["U", "Unemployed"],
                    ["G", "Student"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "228px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxUnemployStatusError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxUnemployStatusError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxUnemployStatusError.setDefaultUnit(kony.flex.DP);
            var lblUnemployErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUnemployErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUnemployError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUnemployError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please select Employment Type",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUnemployStatusError.add(lblUnemployErrorIcon, lblUnemployError);
            flxUnemployDetails.add(lstbxUnemployStatus, flxUnemployStatusError);
            var flxDateWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDateWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDateWrapper.setDefaultUnit(kony.flex.DP);
            var lblDateStarted = new kony.ui.Label({
                "id": "lblDateStarted",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.StartDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateStarted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxDateStarted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306px"
            }, {}, {});
            flxDateStarted.setDefaultUnit(kony.flex.DP);
            var customEmployStartDate = new kony.ui.CustomWidget({
                "id": "customEmployStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "100%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxDateStarted.add(customEmployStartDate);
            var EmployStartDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EmployStartDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "top": "80dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Calendar.startDateEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDateEnded = new kony.ui.Label({
                "id": "lblDateEnded",
                "isVisible": true,
                "left": "48%",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDate\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateEnded = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDateEnded",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48%",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "306dp"
            }, {}, {});
            flxDateEnded.setDefaultUnit(kony.flex.DP);
            var customEmployEndDate = new kony.ui.CustomWidget({
                "id": "customEmployEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxDateEnded.add(customEmployEndDate);
            var EmployEndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EmployEndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "48%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "left": "48%",
                        "top": "80dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDateWrapper.add(lblDateStarted, flxDateStarted, EmployStartDateError, lblDateEnded, flxDateEnded, EmployEndDateError);
            var flxUnemployQuestions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUnemployQuestions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "525px",
                "zIndex": 1
            }, {}, {});
            flxUnemployQuestions.setDefaultUnit(kony.flex.DP);
            var flxCustomerSocialBenefits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCustomerSocialBenefits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCustomerSocialBenefits.setDefaultUnit(kony.flex.DP);
            var lblSocialBenefits = new kony.ui.Label({
                "id": "lblSocialBenefits",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.socialBenefits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSocialBenefitsMandatory = new kony.ui.Image2({
                "height": "12px",
                "id": "imgSocialBenefitsMandatory",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerSocialBenefits.add(lblSocialBenefits, imgSocialBenefitsMandatory);
            var flxSocialBenefitsRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSocialBenefitsRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSocialBenefitsRadio.setDefaultUnit(kony.flex.DP);
            var flxYesBenefitsRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxYesBenefitsRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxYesBenefitsRadio.setDefaultUnit(kony.flex.DP);
            var imgYesBenefitsRadio = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgYesBenefitsRadio",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxYesBenefitsRadio.add(imgYesBenefitsRadio);
            var lblYesBenefitsOpt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYesBenefitsOpt",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoBenefitsRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNoBenefitsRadio",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNoBenefitsRadio.setDefaultUnit(kony.flex.DP);
            var imgNoBenefitsRadio = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNoBenefitsRadio",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBenefitsRadio.add(imgNoBenefitsRadio);
            var lblNoBenefitsOpt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoBenefitsOpt",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSocialBenefitsRadio.add(flxYesBenefitsRadio, lblYesBenefitsOpt, flxNoBenefitsRadio, lblNoBenefitsOpt);
            var flxSocialBenifitsError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSocialBenifitsError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxSocialBenifitsError.setDefaultUnit(kony.flex.DP);
            var lblSocialBenefitsErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSocialBenefitsErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSocialBeneifts = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSocialBeneifts",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please select Social/Security benefits",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSocialBenifitsError.add(lblSocialBenefitsErrorIcon, lblSocialBeneifts);
            flxUnemployQuestions.add(flxCustomerSocialBenefits, flxSocialBenefitsRadio, flxSocialBenifitsError);
            flxUmemployWrapper.add(flxEmpStatus1, flxUnemployDetails, flxDateWrapper, flxUnemployQuestions);
            var flxUnemployPopupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxUnemployPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUnemployPopupButtons.setDefaultUnit(kony.flex.DP);
            var flxUnemployButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxUnemployButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUnemployButtons.setDefaultUnit(kony.flex.DP);
            var btnUnemploySaveAndAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnUnemploySaveAndAdd",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnUnemploySave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnUnemploySave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "width": "91px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnUnemployCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnUnemployCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxUnemployButtons.add(btnUnemploySaveAndAdd, btnUnemploySave, btnUnemployCancel);
            flxUnemployPopupButtons.add(flxUnemployButtons);
            flxUmemployPopupBody.add(flxCloseUmemploy, flxUmemployHeaderName, flxUmemployWrapper, flxUnemployPopupButtons);
            flxUmemploytDetailsContainer.add(flxUmemployPopupTopColor, flxUmemployPopupBody);
            flxUnemployDetailsPopup.add(flxUmemploytDetailsContainer);
            var flxEmploymentDetailsPopupParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmploymentDetailsPopupParent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxEmploymentDetailsPopupParent.setDefaultUnit(kony.flex.DP);
            var flxEmploymentDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75%",
                "id": "flxEmploymentDetailsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknbackGroundffffff100",
                "width": "60%",
                "zIndex": 6
            }, {}, {});
            flxEmploymentDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxLocationDetailsData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "81px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxLocationDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsData.setDefaultUnit(kony.flex.DP);
            var flxDetailsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDetailsHeading.setDefaultUnit(kony.flex.DP);
            var lblSubTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Employment Details",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxImg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxImg.setDefaultUnit(kony.flex.DP);
            var lblIconToggle = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconToggle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImg.add(lblIconToggle);
            var flxCloseEmploymentPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseEmploymentPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknCursor",
                "top": "-3dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseEmploymentPopup.setDefaultUnit(kony.flex.DP);
            var lblCloseIconEmploymentpopoup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblCloseIconEmploymentpopoup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseEmploymentPopup.add(lblCloseIconEmploymentpopoup);
            flxDetailsHeading.add(lblSubTitle, flxImg, flxCloseEmploymentPopup);
            var lblLineUser = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblLineUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDetailsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsData.setDefaultUnit(kony.flex.DP);
            var flxFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxFullName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFullName.setDefaultUnit(kony.flex.DP);
            var lblEmploymentStatus = new kony.ui.Label({
                "id": "lblEmploymentStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentType\")",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmploymentStatusDetail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmploymentStatusDetail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentStatusDetail.setDefaultUnit(kony.flex.DP);
            var lstBoxEmpStatus = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxEmpStatus",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Employment Type"],
                    ["E", "Employee"],
                    ["G", "Student"],
                    ["S", "Self-Employed"],
                    ["U", "Un-Employed"]
                ],
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxEmploymentStatusDetail.add(lstBoxEmpStatus);
            var flxNoEmploymentStatusError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoEmploymentStatusError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoEmploymentStatusError.setDefaultUnit(kony.flex.DP);
            var lblNoEmploymentStatusErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmploymentStatusErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmploymentStatusError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmploymentStatusError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Employment Status",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmploymentStatusError.add(lblNoEmploymentStatusErrorIcon, lblNoEmploymentStatusError);
            var flxEmploymentRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmploymentRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentRoleDetails.setDefaultUnit(kony.flex.DP);
            var lstBoxEmploymentRole = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxEmploymentRole",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Employment Role"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxEmploymentRoleDetails.add(lstBoxEmploymentRole);
            var flxNoEmploymentRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoEmploymentRoleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoEmploymentRoleError.setDefaultUnit(kony.flex.DP);
            var lblNoEmploymentRoleErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmploymentRoleErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmploymentRoleError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmploymentRoleError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Employment Role",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmploymentRoleError.add(lblNoEmploymentRoleErrorIcon, lblNoEmploymentRoleError);
            var flxOccupationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxOccupationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxOccupationDetails.setDefaultUnit(kony.flex.DP);
            var lstBoxOccupation = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxOccupation",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Occupation"],
                    ["A", "Managerial"],
                    ["B", "Professional"],
                    ["C", "Professional Ancillary"],
                    ["D", "Clerical - Supervisory"],
                    ["E", "Clerical"],
                    ["F", "Sales - Supervisory"],
                    ["G", "Sales"],
                    ["Q", "Student"],
                    ["S", "In Education"],
                    ["T", "YTS Scheme"],
                    ["U", "Unemployed"],
                    ["N", "Armed Forces"],
                    ["I", "Manual - Supervisory"],
                    ["M", "Sole Trader"],
                    ["P", "Retired"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxOccupationDetails.add(lstBoxOccupation);
            var flxNoOccupationError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoOccupationError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoOccupationError.setDefaultUnit(kony.flex.DP);
            var lblNoOccupationNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoOccupationNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoOccupationNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoOccupationNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Occupation",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoOccupationError.add(lblNoOccupationNameErrorIcon, lblNoOccupationNameError);
            var flxEmploymentRoleIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxEmploymentRoleIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentRoleIndicator.setDefaultUnit(kony.flex.DP);
            var lblEmploymentRole = new kony.ui.Label({
                "id": "lblEmploymentRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupEmploymentRole\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentRoleIndicator.add(lblEmploymentRole, imgMandatoryName);
            var flxOccupationIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxOccupationIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxOccupationIndicator.setDefaultUnit(kony.flex.DP);
            var lblOccupation = new kony.ui.Label({
                "id": "lblOccupation",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupOccupation\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryDisplayName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryDisplayName",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOccupationIndicator.add(lblOccupation, imgMandatoryDisplayName);
            flxFullName.add(lblEmploymentStatus, flxEmploymentStatusDetail, flxNoEmploymentStatusError, flxEmploymentRoleDetails, flxNoEmploymentRoleError, flxOccupationDetails, flxNoOccupationError, flxEmploymentRoleIndicator, flxOccupationIndicator);
            var flxDateContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDateContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDateContainer.setDefaultUnit(kony.flex.DP);
            var lblEmployeeStartDate = new kony.ui.Label({
                "id": "lblEmployeeStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.StartDate\")",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "225dp"
            }, {}, {});
            flxStartDate.setDefaultUnit(kony.flex.DP);
            var customStartDate = new kony.ui.CustomWidget({
                "id": "customStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxStartDate.add(customStartDate);
            var StartDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "StartDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "top": "80dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Calendar.startDateEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblEmployeeEndDate = new kony.ui.Label({
                "id": "lblEmployeeEndDate",
                "isVisible": true,
                "left": "33.40%",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDate\")",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.40%",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "30dp",
                "width": "225dp"
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var customEndDate = new kony.ui.CustomWidget({
                "id": "customEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxEndDate.add(customEndDate);
            var EndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33.40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "left": "33.40%",
                        "top": "80dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDateContainer.add(lblEmployeeStartDate, flxStartDate, StartDateError, lblEmployeeEndDate, flxEndDate, EndDateError);
            flxDetailsData.add(flxFullName, flxDateContainer);
            var flxEmployerNameDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxEmployerNameDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmployerNameDetails.setDefaultUnit(kony.flex.DP);
            var lblEmployerName = new kony.ui.Label({
                "id": "lblEmployerName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupEmployerName\")",
                "top": "3dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxEmployerName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxEmployerName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
                "left": "0dp",
                "maxTextLength": 70,
                "placeholder": "Employer Name",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "96.10%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEmployerNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoEmployerNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "62.30%",
                "zIndex": 1
            }, {}, {});
            flxNoEmployerNameError.setDefaultUnit(kony.flex.DP);
            var lblNoEmployerNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmployerNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmployerNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmployerNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter Employer Name",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmployerNameError.add(lblNoEmployerNameErrorIcon, lblNoEmployerNameError);
            flxEmployerNameDetails.add(lblEmployerName, txtbxEmployerName, flxNoEmployerNameError);
            var flxBusinessTypeDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypeDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxBusinessTypeDetails.setDefaultUnit(kony.flex.DP);
            var flxBusinessType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26dp",
                "id": "flxBusinessType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxBusinessType.setDefaultUnit(kony.flex.DP);
            var lblBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessType\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessType.add(lblBusinessType);
            var lblLine2 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1dp",
                "id": "lblLine2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddressData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddressData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddressData.setDefaultUnit(kony.flex.DP);
            var flxCityUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxCityUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCityUser.setDefaultUnit(kony.flex.DP);
            var flxBusinessDescriptionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBusinessDescriptionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxBusinessDescriptionDetails.setDefaultUnit(kony.flex.DP);
            var lblBusinessDescription = new kony.ui.Label({
                "id": "lblBusinessDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupBusinessDesc\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxBusinessDescription = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxBusinessDescription",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Business Description"],
                    ["lb2", "Crop and animal production"],
                    ["lb3", "hunting"],
                    ["lb4", "Related service activities"]
                ],
                "right": 0,
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var imgMandatoryState = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryState",
                "isVisible": false,
                "left": "36px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoBusinessDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoBusinessDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBusinessDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoBusinessDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBusinessDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Business Description",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBusinessDescriptionError.add(lblNoBusinessDescriptionErrorIcon, lblNoBusinessDescriptionError);
            flxBusinessDescriptionDetails.add(lblBusinessDescription, lstbxBusinessDescription, imgMandatoryState, flxNoBusinessDescriptionError);
            var flxBusinessCategoryDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBusinessCategoryDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxBusinessCategoryDetails.setDefaultUnit(kony.flex.DP);
            var lblBusinessCategory = new kony.ui.Label({
                "id": "lblBusinessCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupBusinessCategory\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxBusinessCategory = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxBusinessCategory",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Business Category"],
                    ["lb2", "Agriculture"],
                    ["lb3", "Forestry"],
                    ["lb4", "Fishing"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var imgMandatoryCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoBusinessCategoryError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoBusinessCategoryError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBusinessCategoryError.setDefaultUnit(kony.flex.DP);
            var lblNoBusinessCategoryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessCategoryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBusinessCategoryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessCategoryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Business Category",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBusinessCategoryError.add(lblNoBusinessCategoryErrorIcon, lblNoBusinessCategoryError);
            flxBusinessCategoryDetails.add(lblBusinessCategory, lstBoxBusinessCategory, imgMandatoryCountry, flxNoBusinessCategoryError);
            flxCityUser.add(flxBusinessDescriptionDetails, flxBusinessCategoryDetails);
            var flxBusinessDetailedDescriptionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxBusinessDetailedDescriptionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBusinessDetailedDescriptionDetails.setDefaultUnit(kony.flex.DP);
            var flxBusinessDetailedDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBusinessDetailedDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "93.40%",
                "zIndex": 1
            }, {}, {});
            flxBusinessDetailedDescription.setDefaultUnit(kony.flex.DP);
            var lblBusinessDetailDescription = new kony.ui.Label({
                "id": "lblBusinessDetailDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupBusinessDetailDesc\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDetailedDesc = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxDetailedDesc",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Business Detailed Description",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lstBoxBusinessDetailedDescription = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxBusinessDetailedDescription",
                "isVisible": false,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Business Detailed Description"],
                    ["lb2", "Growing Of Cereals (Except Rice), Leguminous Crops And Oil Seeds"],
                    ["lb3", "Growing Of Rice"],
                    ["lb4", "Growing Of Veg, Melons, Roots And Tubers"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var imgMandatoryZipCode = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZipCode",
                "isVisible": false,
                "left": "55px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoBusinessDetailedDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoBusinessDetailedDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBusinessDetailedDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoBusinessDetailedDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessDetailedDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBusinessDetailedDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBusinessDetailedDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please Select Business Detailed Description",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBusinessDetailedDescriptionError.add(lblNoBusinessDetailedDescriptionErrorIcon, lblNoBusinessDetailedDescriptionError);
            flxBusinessDetailedDescription.add(lblBusinessDetailDescription, tbxDetailedDesc, lstBoxBusinessDetailedDescription, imgMandatoryZipCode, flxNoBusinessDetailedDescriptionError);
            flxBusinessDetailedDescriptionDetails.add(flxBusinessDetailedDescription);
            flxAddressData.add(flxCityUser, flxBusinessDetailedDescriptionDetails);
            flxBusinessTypeDetails.add(flxBusinessType, lblLine2, flxAddressData);
            var flxBusinessAddressDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26dp",
                "id": "flxBusinessAddressDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxBusinessAddressDetails.setDefaultUnit(kony.flex.DP);
            var lblBusinessAddressDetails = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessAddressDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.businessAddDetail\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "-100dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckBox.setDefaultUnit(kony.flex.DP);
            var flxEmploymentBusinessAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxEmploymentBusinessAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxEmploymentBusinessAddress.setDefaultUnit(kony.flex.DP);
            var imgcbEmploymenyBussinessAddress = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgcbEmploymenyBussinessAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmploymentBusinessAddress.add(imgcbEmploymenyBussinessAddress);
            var lblEmploymentBusinessAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmploymentBusinessAddress",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.checkBoxLabel\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox.add(flxEmploymentBusinessAddress, lblEmploymentBusinessAddress);
            flxBusinessAddressDetails.add(lblBusinessAddressDetails, flxCheckBox);
            var lblLineUserWork = new kony.ui.Label({
                "bottom": "0px",
                "height": "1dp",
                "id": "lblLineUserWork",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCompanyDetailsRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxCompanyDetailsRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow4.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "96%",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 20,
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 2
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var segSearch = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAddress": "",
                    "lblPinIcon": ""
                }],
                "groupCells": false,
                "height": "75%",
                "id": "segSearch",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchCompanyMap",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSearchCompanyMap": "flxSearchCompanyMap",
                    "lblAddress": "lblAddress",
                    "lblPinIcon": "lblPinIcon"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var map1 = new com.konymp.map1({
                "height": "90%",
                "id": "map1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "overrides": {
                    "map1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90%",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "40dp"
                    },
                    "mapLocations": {
                        "height": "100%",
                        "top": "0%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchContainer.add(tbxSearch, segSearch, map1);
            var flxAddressContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddressContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.50%"
            }, {}, {});
            flxAddressContainer.setDefaultUnit(kony.flex.DP);
            var flxStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxStreetName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStreetName.setDefaultUnit(kony.flex.DP);
            var lblStreetName = new kony.ui.Label({
                "id": "lblStreetName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.WireTransfer.AddressLine1\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxStreetName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxStreetName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoStreetName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoStreetName.setDefaultUnit(kony.flex.DP);
            var lblNoStreetNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStreetNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStreetNameError = new kony.ui.Label({
                "id": "lblNoStreetNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoStreetName.add(lblNoStreetNameErrorIcon, lblNoStreetNameError);
            flxStreetName.add(lblStreetName, tbxStreetName, flxNoStreetName);
            var flxBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxBuildingName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBuildingName.setDefaultUnit(kony.flex.DP);
            var lblBuildingName = new kony.ui.Label({
                "id": "lblBuildingName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.popupAddressLine2\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxBuildingName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxBuildingName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoBuildingName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBuildingName.setDefaultUnit(kony.flex.DP);
            var lblNoBuildingNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBuildingNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBuildingNameError = new kony.ui.Label({
                "id": "lblNoBuildingNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBuildingName.add(lblNoBuildingNameErrorIcon, lblNoBuildingNameError);
            flxBuildingName.add(lblBuildingName, tbxBuildingName, flxNoBuildingName);
            var flxCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47%",
                "zIndex": 1
            }, {}, {});
            flxCountry.setDefaultUnit(kony.flex.DP);
            var lblCountry = new kony.ui.Label({
                "id": "lblCountry",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCountryTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCountryTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCountry = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "typeHead": {
                        "height": "170dp",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCountryTypeHead.add(typeHeadCountry);
            var CopyimgMandatoryCountry0cb638db6f7dd46 = new kony.ui.Image2({
                "height": "12px",
                "id": "CopyimgMandatoryCountry0cb638db6f7dd46",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountry.setDefaultUnit(kony.flex.DP);
            var CopylblNoCountryErrorIcon0e15a398deca942 = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCountryErrorIcon0e15a398deca942",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblNoCountryError0dcfa80ae11d44e = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCountryError0dcfa80ae11d44e",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountry.add(CopylblNoCountryErrorIcon0e15a398deca942, CopylblNoCountryError0dcfa80ae11d44e);
            flxCountry.add(lblCountry, flxCountryTypeHead, CopyimgMandatoryCountry0cb638db6f7dd46, flxNoCountry);
            var flxState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxState.setDefaultUnit(kony.flex.DP);
            var lblState = new kony.ui.Label({
                "id": "lblState",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStateTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStateTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxStateTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadState = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStateTypeHead.add(typeHeadState);
            var CopyimgMandatoryState0c4f7dba9422b49 = new kony.ui.Image2({
                "height": "12px",
                "id": "CopyimgMandatoryState0c4f7dba9422b49",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoState.setDefaultUnit(kony.flex.DP);
            var lblNoStateErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStateError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoState.add(lblNoStateErrorIcon, lblNoStateError);
            flxState.add(lblState, flxStateTypeHead, CopyimgMandatoryState0c4f7dba9422b49, flxNoState);
            var flxCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCity.setDefaultUnit(kony.flex.DP);
            var lblCity = new kony.ui.Label({
                "id": "lblCity",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCityTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCityTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCityTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCity = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCityTypeHead.add(typeHeadCity);
            var imgMandatoryCity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCity",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCity.setDefaultUnit(kony.flex.DP);
            var CopylblNoCityErrorIcon0cb0611c3cbed44 = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCityErrorIcon0cb0611c3cbed44",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblNoCityError0b530c73a3ffd4e = new kony.ui.Label({
                "height": "15dp",
                "id": "CopylblNoCityError0b530c73a3ffd4e",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCity.add(CopylblNoCityErrorIcon0cb0611c3cbed44, CopylblNoCityError0b530c73a3ffd4e);
            flxCity.add(lblCity, flxCityTypeHead, imgMandatoryCity, flxNoCity);
            var flxZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxZipCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxZipCode.setDefaultUnit(kony.flex.DP);
            var lblZipCode = new kony.ui.Label({
                "id": "lblZipCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxZipCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxZipCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryZip = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZip",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoZipCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoZipCode.setDefaultUnit(kony.flex.DP);
            var lblNoZipErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoZipError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoZipCode.add(lblNoZipErrorIcon, lblNoZipError);
            flxZipCode.add(lblZipCode, tbxZipCode, imgMandatoryZip, flxNoZipCode);
            flxAddressContainer.add(flxStreetName, flxBuildingName, flxCountry, flxState, flxCity, flxZipCode);
            flxCompanyDetailsRow4.add(flxSearchContainer, flxAddressContainer);
            flxLocationDetailsData.add(flxDetailsHeading, lblLineUser, flxDetailsData, flxEmployerNameDetails, flxBusinessTypeDetails, flxBusinessAddressDetails, lblLineUserWork, flxCompanyDetailsRow4);
            var flxBusinessDetailsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxBusinessDetailsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 2
            }, {}, {});
            flxBusinessDetailsSeparator.setDefaultUnit(kony.flex.DP);
            flxBusinessDetailsSeparator.add();
            var flxLocationDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxLocationDetailsButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsButtons.setDefaultUnit(kony.flex.DP);
            var locationDetailsButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "locationDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "10px"
                    },
                    "flxRightButtons": {
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationDetailsButtons.add(locationDetailsButtons);
            var flxEmploymentDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEmploymentDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentDetailsButtons.setDefaultUnit(kony.flex.DP);
            var flxButtonsEmployment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtonsEmployment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtonsEmployment.setDefaultUnit(kony.flex.DP);
            var btnEmploymentSave1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEmploymentSave1",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnEmploymentSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEmploymentSave",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnEmploymentCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEmploymentCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "125px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxButtonsEmployment.add(btnEmploymentSave1, btnEmploymentSave, btnEmploymentCancel);
            flxEmploymentDetailsButtons.add(flxButtonsEmployment);
            var flxEmploymentDetailsPopupTopColour = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEmploymentDetailsPopupTopColour",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmploymentDetailsPopupTopColour.setDefaultUnit(kony.flex.DP);
            flxEmploymentDetailsPopupTopColour.add();
            flxEmploymentDetailsPopup.add(flxLocationDetailsData, flxBusinessDetailsSeparator, flxLocationDetailsButtons, flxEmploymentDetailsButtons, flxEmploymentDetailsPopupTopColour);
            flxEmploymentDetailsPopupParent.add(flxEmploymentDetailsPopup);
            var flxSelfEmploymentDetailsPopupParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSelfEmploymentDetailsPopupParent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxSelfEmploymentDetailsPopupParent.setDefaultUnit(kony.flex.DP);
            var flxSelfEmploymentDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75%",
                "id": "flxSelfEmploymentDetailsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknbackGroundffffff100",
                "width": "75%",
                "zIndex": 6
            }, {}, {});
            flxSelfEmploymentDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxSelfEmploymentDetailsData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxSelfEmploymentDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentDetailsData.setDefaultUnit(kony.flex.DP);
            var flxSelfEmploymentDetailsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSelfEmploymentDetailsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSelfEmploymentDetailsHeading.setDefaultUnit(kony.flex.DP);
            var lblSelfEmploymentSubTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelfEmploymentSubTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Employment Details",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentDetailsHeading.add(lblSelfEmploymentSubTitle);
            var lblLineSelfEmployment = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblLineSelfEmployment",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelfEmploymentDetailsData1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelfEmploymentDetailsData1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentDetailsData1.setDefaultUnit(kony.flex.DP);
            var flxSelfEmploymentStatusRoleOccupation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxSelfEmploymentStatusRoleOccupation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentStatusRoleOccupation.setDefaultUnit(kony.flex.DP);
            var lblSelfEmploymentStatus = new kony.ui.Label({
                "id": "lblSelfEmploymentStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Employment Type",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelfEmploymentStatusDetail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSelfEmploymentStatusDetail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentStatusDetail.setDefaultUnit(kony.flex.DP);
            var lstBoxSelfEmpStatus = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxSelfEmpStatus",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Employment Type"]
                ],
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxSelfEmploymentStatusDetail.add(lstBoxSelfEmpStatus);
            var flxNoSelfEmploymentStatusError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoSelfEmploymentStatusError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoSelfEmploymentStatusError.setDefaultUnit(kony.flex.DP);
            var lblNoSelfEmploymentStatusErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentStatusErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSelfEmploymentStatusError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentStatusError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter Employment Status",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSelfEmploymentStatusError.add(lblNoSelfEmploymentStatusErrorIcon, lblNoSelfEmploymentStatusError);
            var flxSelfEmploymentRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSelfEmploymentRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentRoleDetails.setDefaultUnit(kony.flex.DP);
            var lstBoxSelfEmploymentRole = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxSelfEmploymentRole",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Employment Role"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxSelfEmploymentRoleDetails.add(lstBoxSelfEmploymentRole);
            var flxNoSelfEmploymentRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoSelfEmploymentRoleError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoSelfEmploymentRoleError.setDefaultUnit(kony.flex.DP);
            var lblNoSelfEmploymentRoleErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentRoleErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSelfEmploymentRoleError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentRoleError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter Employment Role",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSelfEmploymentRoleError.add(lblNoSelfEmploymentRoleErrorIcon, lblNoSelfEmploymentRoleError);
            var flxSelfEmploymentOccupationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSelfEmploymentOccupationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentOccupationDetails.setDefaultUnit(kony.flex.DP);
            var lstBoxSelfEmploymentOccupation = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstBoxSelfEmploymentOccupation",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select Occupation"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxSelfEmploymentOccupationDetails.add(lstBoxSelfEmploymentOccupation);
            var flxSelfEmploymentNoOccupationError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSelfEmploymentNoOccupationError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentNoOccupationError.setDefaultUnit(kony.flex.DP);
            var lblNoSelfEmploymentOccupationNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentOccupationNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSelfEmploymentOccupationName = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSelfEmploymentOccupationName",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter Occupation",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentNoOccupationError.add(lblNoSelfEmploymentOccupationNameErrorIcon, lblNoSelfEmploymentOccupationName);
            var flxSelfEmploymentRoleIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSelfEmploymentRoleIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentRoleIndicator.setDefaultUnit(kony.flex.DP);
            var lblSelfEmploymentRole = new kony.ui.Label({
                "id": "lblSelfEmploymentRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Employment Role",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSelfEmploymentMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgSelfEmploymentMandatoryName",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentRoleIndicator.add(lblSelfEmploymentRole, imgSelfEmploymentMandatoryName);
            var flxSelfEmploymentOccupationIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSelfEmploymentOccupationIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentOccupationIndicator.setDefaultUnit(kony.flex.DP);
            var lblSelfEmploymentOccupation = new kony.ui.Label({
                "id": "lblSelfEmploymentOccupation",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Occupation",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatorySelfEmploymentOccupation = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatorySelfEmploymentOccupation",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentOccupationIndicator.add(lblSelfEmploymentOccupation, imgMandatorySelfEmploymentOccupation);
            flxSelfEmploymentStatusRoleOccupation.add(lblSelfEmploymentStatus, flxSelfEmploymentStatusDetail, flxNoSelfEmploymentStatusError, flxSelfEmploymentRoleDetails, flxNoSelfEmploymentRoleError, flxSelfEmploymentOccupationDetails, flxSelfEmploymentNoOccupationError, flxSelfEmploymentRoleIndicator, flxSelfEmploymentOccupationIndicator);
            flxSelfEmploymentDetailsData1.add(flxSelfEmploymentStatusRoleOccupation);
            var flxSelfEmploymentBusinessAddressDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26dp",
                "id": "flxSelfEmploymentBusinessAddressDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSelfEmploymentBusinessAddressDetails.setDefaultUnit(kony.flex.DP);
            var lblSelfEmploymentBusinessAddressDetails = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelfEmploymentBusinessAddressDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Business Address Details",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentBusinessAddressDetails.add(lblSelfEmploymentBusinessAddressDetails);
            var lblLineUserWorkSelfEmployment = new kony.ui.Label({
                "bottom": "0px",
                "height": "1dp",
                "id": "lblLineUserWorkSelfEmployment",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelfEmploymentDetailsData.add(flxSelfEmploymentDetailsHeading, lblLineSelfEmployment, flxSelfEmploymentDetailsData1, flxSelfEmploymentBusinessAddressDetails, lblLineUserWorkSelfEmployment);
            var flxSelfEmploymentBusinessDetailsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSelfEmploymentBusinessDetailsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 2
            }, {}, {});
            flxSelfEmploymentBusinessDetailsSeparator.setDefaultUnit(kony.flex.DP);
            flxSelfEmploymentBusinessDetailsSeparator.add();
            var flxSelfEmploymentDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSelfEmploymentDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentDetailsButtons.setDefaultUnit(kony.flex.DP);
            var flxButtonsSelfEmployment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtonsSelfEmployment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtonsSelfEmployment.setDefaultUnit(kony.flex.DP);
            var btnSelfEmploymentSave1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSelfEmploymentSave1",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnSelfEmploymentSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnSelfEmploymentSave",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnSelfEmploymentCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSelfEmploymentCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "125px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxButtonsSelfEmployment.add(btnSelfEmploymentSave1, btnSelfEmploymentSave, btnSelfEmploymentCancel);
            flxSelfEmploymentDetailsButtons.add(flxButtonsSelfEmployment);
            var flxSelfEmploymentDetailsPopupTopColour = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxSelfEmploymentDetailsPopupTopColour",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelfEmploymentDetailsPopupTopColour.setDefaultUnit(kony.flex.DP);
            flxSelfEmploymentDetailsPopupTopColour.add();
            flxSelfEmploymentDetailsPopup.add(flxSelfEmploymentDetailsData, flxSelfEmploymentBusinessDetailsSeparator, flxSelfEmploymentDetailsButtons, flxSelfEmploymentDetailsPopupTopColour);
            flxSelfEmploymentDetailsPopupParent.add(flxSelfEmploymentDetailsPopup);
            var flxSendPaymentsDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSendPaymentsDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxSendPaymentsDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxSendPaymentsDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "559px",
                "id": "flxSendPaymentsDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "width": "756px",
                "zIndex": 1
            }, {}, {});
            flxSendPaymentsDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxSendPaymentsPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxSendPaymentsPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPaymentsPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxSendPaymentsPopupTopColor.add();
            var flxSendPayPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "549px",
                "id": "flxSendPayPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSendPayClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxSendPayClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSendPayClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconSendPayClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconSendPayClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPayClosePopup.add(lblIconSendPayClosePopup);
            var flxSendPaymentsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSendPaymentsName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSendPaymentsName.setDefaultUnit(kony.flex.DP);
            var lblSendPayments = new kony.ui.Label({
                "id": "lblSendPayments",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.sendPayPopupTitle\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendPaySeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSendPaySeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPaymentsName.add(lblSendPayments, lblSendPaySeperator);
            var flxSendPayWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "69%",
                "id": "flxSendPayWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionCountrySendPay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionCountrySendPay",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDescriptionCountrySendPay.setDefaultUnit(kony.flex.DP);
            var lblViewCountrySendPay = new kony.ui.Label({
                "id": "lblViewCountrySendPay",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.selectSendPaymentsCountries\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInfoIcon = new kony.ui.Label({
                "id": "lblInfoIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionCountrySendPay.add(lblViewCountrySendPay, lblInfoIcon);
            var flxCountryUserSendPay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryUserSendPay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxCountryUserSendPay.setDefaultUnit(kony.flex.DP);
            var lstbxCountryUserSendPay = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCountryUserSendPay",
                "isVisible": false,
                "left": "1dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var customListbox = new com.adminConsole.dueDiligence.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "350px",
                "overrides": {
                    "customListbox": {
                        "width": "350px"
                    },
                    "flxSegmentList": {
                        "isVisible": false
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "lblDescription": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoCountryErrorSendPay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountryErrorSendPay",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountryErrorSendPay.setDefaultUnit(kony.flex.DP);
            var lblNoCountrySendPayErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountrySendPayErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountrySendPayError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountrySendPayError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountryErrorSendPay.add(lblNoCountrySendPayErrorIcon, lblNoCountrySendPayError);
            flxCountryUserSendPay.add(lstbxCountryUserSendPay, customListbox, flxNoCountryErrorSendPay);
            var flxSendPaymnetsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSendPaymnetsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPaymnetsWrapper.setDefaultUnit(kony.flex.DP);
            var flxSendPayNumberOfPayments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSendPayNumberOfPayments",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxSendPayNumberOfPayments.setDefaultUnit(kony.flex.DP);
            var lblExpectedSendPayments = new kony.ui.Label({
                "id": "lblExpectedSendPayments",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.estimateSendNumber\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInfoIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxInfoIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            flxInfoIcon.setDefaultUnit(kony.flex.DP);
            var lblExpectedPayIcon = new kony.ui.Label({
                "id": "lblExpectedPayIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInfoIcon.add(lblExpectedPayIcon);
            var lblExpectedSendPaymentCount = new kony.ui.Label({
                "id": "lblExpectedSendPaymentCount",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/10",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPayNumberOfPayments.add(lblExpectedSendPayments, flxInfoIcon, lblExpectedSendPaymentCount);
            var txtbxExpectedPayments = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxExpectedPayments",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 10,
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxSendNumberOfPaymentsError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSendNumberOfPaymentsError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxSendNumberOfPaymentsError.setDefaultUnit(kony.flex.DP);
            var lblSendNumberOfpaymentsErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendNumberOfpaymentsErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendPayNumberOfPaymentsError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendPayNumberOfPaymentsError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter number of payments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendNumberOfPaymentsError.add(lblSendNumberOfpaymentsErrorIcon, lblSendPayNumberOfPaymentsError);
            var flxSendPayExpectedAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSendPayExpectedAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSendPayExpectedAmount.setDefaultUnit(kony.flex.DP);
            var lblExpectedAmount = new kony.ui.Label({
                "id": "lblExpectedAmount",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.estimateSendAmount\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedAmountInfocon = new kony.ui.Label({
                "id": "lblExpectedAmountInfocon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedAmountCount = new kony.ui.Label({
                "id": "lblExpectedAmountCount",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/10",
                "top": "0dp",
                "width": "6%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxExpectedAmountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxExpectedAmountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxExpectedAmountError.setDefaultUnit(kony.flex.DP);
            var lblNoExpectedAmountErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoExpectedAmountErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedAmountError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblExpectedAmountError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter City Name",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExpectedAmountError.add(lblNoExpectedAmountErrorIcon, lblExpectedAmountError);
            flxSendPayExpectedAmount.add(lblExpectedAmount, lblExpectedAmountInfocon, lblExpectedAmountCount, flxExpectedAmountError);
            var txtbxExpectedAmount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxExpectedAmount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "50%",
                "maxTextLength": 10,
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lbldefaultCurrencySymbol = new kony.ui.Label({
                "id": "lbldefaultCurrencySymbol",
                "isVisible": true,
                "left": "51%",
                "skin": "sknLatoRegular485c7514px",
                "top": "37dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSendPayAmountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSendPayAmountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayAmountError.setDefaultUnit(kony.flex.DP);
            var lblSendPayAmountErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendPayAmountErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendPayAmountError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendPayAmountError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter amount of payments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPayAmountError.add(lblSendPayAmountErrorIcon, lblSendPayAmountError);
            var flxSendPayReason = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSendPayReason",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxSendPayReason.setDefaultUnit(kony.flex.DP);
            var lblSendPaymentsReason = new kony.ui.Label({
                "id": "lblSendPaymentsReason",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.sendReason\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendPayReason = new kony.ui.Label({
                "id": "lblSendPayReason",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPayReason.add(lblSendPaymentsReason, lblSendPayReason);
            var lblExpectedReasonCount = new kony.ui.Label({
                "id": "lblExpectedReasonCount",
                "isVisible": true,
                "right": "30dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/100",
                "top": "96dp",
                "width": "7%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReason = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "72px",
                "id": "txtReason",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 100,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "120dp",
                "width": "715px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxSendPayReasonError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSendPayReasonError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "196dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxSendPayReasonError.setDefaultUnit(kony.flex.DP);
            var lblSendPayReasonErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendPayReasonErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendPayReasonError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSendPayReasonError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter the reason",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSendPayReasonError.add(lblSendPayReasonErrorIcon, lblSendPayReasonError);
            flxSendPaymnetsWrapper.add(flxSendPayNumberOfPayments, txtbxExpectedPayments, flxSendNumberOfPaymentsError, flxSendPayExpectedAmount, txtbxExpectedAmount, lbldefaultCurrencySymbol, flxSendPayAmountError, flxSendPayReason, lblExpectedReasonCount, txtReason, flxSendPayReasonError);
            flxSendPayWrapper.add(flxDescriptionCountrySendPay, flxCountryUserSendPay, flxSendPaymnetsWrapper);
            var flxSendPayButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSendPayButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayButtons.setDefaultUnit(kony.flex.DP);
            var flxSendPayPopupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxSendPayPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayPopupButtons.setDefaultUnit(kony.flex.DP);
            var btnSendPayAddNew = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSendPayAddNew",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnSendPaySave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnSendPaySave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & CLOSE",
                "width": "147px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnSendPayClose = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSendPayClose",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxSendPayPopupButtons.add(btnSendPayAddNew, btnSendPaySave, btnSendPayClose);
            flxSendPayButtons.add(flxSendPayPopupButtons);
            flxSendPayPopupBody.add(flxSendPayClosePopup, flxSendPaymentsName, flxSendPayWrapper, flxSendPayButtons);
            var AmountInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "AmountInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "70.70%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 205,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "70.70%",
                        "top": 205,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.EstmateSendPayAmount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var paymentsInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "paymentsInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 205,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "40%",
                        "top": 205,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.EstmateSendPayNumber\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var reasonInfoIconComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "reasonInfoIconComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "32%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 297,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "32%",
                        "top": 297,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.sendPayReason\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSendPaymentsDetailsContainer.add(flxSendPaymentsPopupTopColor, flxSendPayPopupBody, AmountInfoIconComponent, paymentsInfoIconComponent, reasonInfoIconComponent);
            flxSendPaymentsDetailsPopup.add(flxSendPaymentsDetailsContainer);
            var flxReceivePaymentsDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReceivePaymentsDetailsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxReceivePaymentsDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxReceivePaymentsDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "559px",
                "id": "flxReceivePaymentsDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "width": "756px",
                "zIndex": 1
            }, {}, {});
            flxReceivePaymentsDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxReceivePaymentsPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxReceivePaymentsPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePaymentsPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxReceivePaymentsPopupTopColor.add();
            var flxReceivePayPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "549px",
                "id": "flxReceivePayPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayPopupBody.setDefaultUnit(kony.flex.DP);
            var flxReceivePayClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxReceivePayClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxReceivePayClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconReceivePayClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconReceivePayClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePayClosePopup.add(lblIconReceivePayClosePopup);
            var flxReceivePaymentsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReceivePaymentsName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxReceivePaymentsName.setDefaultUnit(kony.flex.DP);
            var lblReceivePayments = new kony.ui.Label({
                "id": "lblReceivePayments",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.receivePayPopuptitle\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSendReceiveSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSendReceiveSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePaymentsName.add(lblReceivePayments, lblSendReceiveSeperator);
            var flxReceivePayWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "69%",
                "id": "flxReceivePayWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionCountryReceivePay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionCountryReceivePay",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDescriptionCountryReceivePay.setDefaultUnit(kony.flex.DP);
            var lblViewCountryReceivePay = new kony.ui.Label({
                "id": "lblViewCountryReceivePay",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.selectReceivePaymentsCountries\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInfoIconReceivePay = new kony.ui.Label({
                "id": "lblInfoIconReceivePay",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionCountryReceivePay.add(lblViewCountryReceivePay, lblInfoIconReceivePay);
            var flxReceivePay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReceivePay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxReceivePay.setDefaultUnit(kony.flex.DP);
            var lstbxCountryUserReceivePay = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCountryUserReceivePay",
                "isVisible": false,
                "left": "1dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "0dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxNoCountryErrorReceivePay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountryErrorReceivePay",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountryErrorReceivePay.setDefaultUnit(kony.flex.DP);
            var lblNoCountryReceivePayErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryReceivePayErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryReceivePayError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryReceivePayError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountryErrorReceivePay.add(lblNoCountryReceivePayErrorIcon, lblNoCountryReceivePayError);
            var customListbox1 = new com.adminConsole.dueDiligence.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListbox1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "350px",
                "overrides": {
                    "customListbox": {
                        "width": "350px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReceivePay.add(lstbxCountryUserReceivePay, flxNoCountryErrorReceivePay, customListbox1);
            var flxReceivePaymnetsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxReceivePaymnetsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "34px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePaymnetsWrapper.setDefaultUnit(kony.flex.DP);
            var flxReceivePayNumberOfPayments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReceivePayNumberOfPayments",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxReceivePayNumberOfPayments.setDefaultUnit(kony.flex.DP);
            var lblExpectedReceivePayments = new kony.ui.Label({
                "id": "lblExpectedReceivePayments",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.estimateReceivedNumber\")",
                "top": "0dp",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedReceiveIcon = new kony.ui.Label({
                "id": "lblExpectedReceiveIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedReceivePaymentCount = new kony.ui.Label({
                "id": "lblExpectedReceivePaymentCount",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/10",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePayNumberOfPayments.add(lblExpectedReceivePayments, lblExpectedReceiveIcon, lblExpectedReceivePaymentCount);
            var txtbxExpectedReceivePayments = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxExpectedReceivePayments",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 10,
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "33dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxReceiveNumberOfPaymentsError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxReceiveNumberOfPaymentsError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxReceiveNumberOfPaymentsError.setDefaultUnit(kony.flex.DP);
            var lblReceiveNumberOfpaymentsErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceiveNumberOfpaymentsErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReceivePayNumberOfPaymentsError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceivePayNumberOfPaymentsError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter number of payments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceiveNumberOfPaymentsError.add(lblReceiveNumberOfpaymentsErrorIcon, lblReceivePayNumberOfPaymentsError);
            var flxReceivePayExpectedAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReceivePayExpectedAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayExpectedAmount.setDefaultUnit(kony.flex.DP);
            var lblExpectedReceivedAmount = new kony.ui.Label({
                "id": "lblExpectedReceivedAmount",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.estimateReceivedAmount\")",
                "top": "0dp",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedReceivedAmountInfoIcon = new kony.ui.Label({
                "id": "lblExpectedReceivedAmountInfoIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedReceivedAmountCount = new kony.ui.Label({
                "id": "lblExpectedReceivedAmountCount",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/10",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxExpectedReceivedAmountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxExpectedReceivedAmountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxExpectedReceivedAmountError.setDefaultUnit(kony.flex.DP);
            var lblNoExpectedReceivedAmountErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoExpectedReceivedAmountErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpectedReceivedAmountError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblExpectedReceivedAmountError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter City Name",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExpectedReceivedAmountError.add(lblNoExpectedReceivedAmountErrorIcon, lblExpectedReceivedAmountError);
            flxReceivePayExpectedAmount.add(lblExpectedReceivedAmount, lblExpectedReceivedAmountInfoIcon, lblExpectedReceivedAmountCount, flxExpectedReceivedAmountError);
            var txtbxExpectedReceivedAmount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxExpectedReceivedAmount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "50%",
                "maxTextLength": 10,
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "33dp",
                "width": "350px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblCurrencySymbol = new kony.ui.Label({
                "id": "lblCurrencySymbol",
                "isVisible": true,
                "left": "51%",
                "skin": "sknLatoRegular485c7514px",
                "top": "46dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxReceivePayAmountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxReceivePayAmountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayAmountError.setDefaultUnit(kony.flex.DP);
            var lblReceivePayAmountErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceivePayAmountErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReceivePayAmountError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceivePayAmountError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter amount of payments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePayAmountError.add(lblReceivePayAmountErrorIcon, lblReceivePayAmountError);
            var flxReceivePayReason = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReceivePayReason",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "104dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxReceivePayReason.setDefaultUnit(kony.flex.DP);
            var lblReceivePaymentsReason = new kony.ui.Label({
                "id": "lblReceivePaymentsReason",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.receiveReason\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReceivePayReason = new kony.ui.Label({
                "id": "lblReceivePayReason",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcomoon20px",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePayReason.add(lblReceivePaymentsReason, lblReceivePayReason);
            var lblExpectedReceivedReasonCount = new kony.ui.Label({
                "id": "lblExpectedReceivedReasonCount",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "0/100",
                "top": "96dp",
                "width": "7%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReasonReceivePayment = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "72px",
                "id": "txtReasonReceivePayment",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 100,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "133dp",
                "width": "715px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxReceivePayReasonError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxReceivePayReasonError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayReasonError.setDefaultUnit(kony.flex.DP);
            var lblReceivePayReasonErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceivePayReasonErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReceivePayReasonError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblReceivePayReasonError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter the reason",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReceivePayReasonError.add(lblReceivePayReasonErrorIcon, lblReceivePayReasonError);
            flxReceivePaymnetsWrapper.add(flxReceivePayNumberOfPayments, txtbxExpectedReceivePayments, flxReceiveNumberOfPaymentsError, flxReceivePayExpectedAmount, txtbxExpectedReceivedAmount, lblCurrencySymbol, flxReceivePayAmountError, flxReceivePayReason, lblExpectedReceivedReasonCount, txtReasonReceivePayment, flxReceivePayReasonError);
            flxReceivePayWrapper.add(flxDescriptionCountryReceivePay, flxReceivePay, flxReceivePaymnetsWrapper);
            var flxSendReceiveButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSendReceiveButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendReceiveButtons.setDefaultUnit(kony.flex.DP);
            var flxReceivePayPopupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxReceivePayPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReceivePayPopupButtons.setDefaultUnit(kony.flex.DP);
            var btnSendReceiveAddNew = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnSendReceiveAddNew",
                "isVisible": false,
                "right": "185px",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & ADD ANOTHER",
                "top": "0%",
                "width": "200px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnReceivePaySave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnReceivePaySave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & CLOSE",
                "width": "147px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnReceivePayClose = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnReceivePayClose",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxReceivePayPopupButtons.add(btnSendReceiveAddNew, btnReceivePaySave, btnReceivePayClose);
            flxSendReceiveButtons.add(flxReceivePayPopupButtons);
            flxReceivePayPopupBody.add(flxReceivePayClosePopup, flxReceivePaymentsName, flxReceivePayWrapper, flxSendReceiveButtons);
            var ReceiveAmountInfoComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ReceiveAmountInfoComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "70.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 211,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "10dp",
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "70.50%",
                        "top": 211,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.EstmateReceivePayAmount\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ReceivepaymentsInfoComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ReceivepaymentsInfoComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "39.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 211,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "39.50%",
                        "top": 211,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.EstmateReceivePayNumber\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var reasonReceiveInfoComponent = new com.adminConsole.cdd.infoIconComponent({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "reasonReceiveInfoComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "32%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxTrans",
                "top": 315,
                "width": "160px",
                "zIndex": 10,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "infoIconComponent": {
                        "isVisible": false,
                        "left": "32%",
                        "top": 315,
                        "width": "160px",
                        "zIndex": 10
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"kony.customers.dueDiligence.receivePayReason\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReceivePaymentsDetailsContainer.add(flxReceivePaymentsPopupTopColor, flxReceivePayPopupBody, ReceiveAmountInfoComponent, ReceivepaymentsInfoComponent, reasonReceiveInfoComponent);
            flxReceivePaymentsDetailsPopup.add(flxReceivePaymentsDetailsContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxPopUpConfirmation, flxPopUpError, flxPopupSelectEnrolEmail, flxEnableEstatement, flxPreviewPopup, flxAddCitizenshipDetailsPopup, flxViewFeatureDetails, flxLinkProfilesPopup, flxDelinkProfilePopup, flxAddTaxDetailsPopup, flxStudentDetailsPopup, flxEmploymentStatusDetailsPopup, flxUnemployDetailsPopup, flxEmploymentDetailsPopupParent, flxSelfEmploymentDetailsPopupParent, flxSendPaymentsDetailsPopup, flxReceivePaymentsDetailsPopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerProfileDueDiligence,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerProfileDueDiligence",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_b947246ddce64e25a351cabb0606c3b3,
            "preShow": function(eventobject) {
                controller.AS_Form_b1b5dc60899c4baba4c1adcf34047a9a(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_gb8c2d92ea4a4b9b9006d4695e246a55,
            "retainScrollPosition": false
        }]
    }
});