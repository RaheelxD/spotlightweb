define("CustomerManagementModule/frmCustomerProfileRoles", function() {
    return function(controller) {
        function addWidgetsfrmCustomerProfileRoles() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "SEARCH"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "784px",
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "126px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationWrapper.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoWrapper = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxGeneralInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "700dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "ProfileGeneralInfo": {
                        "maxHeight": "700dp"
                    },
                    "generalInfoHeader.flxOptions": {
                        "right": "2px"
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOtherInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxOtherInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOtherInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0hfd18814fd664dCM",
                "overrides": {
                    "btnTabName4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")"
                    },
                    "flxTabs": {
                        "isVisible": true
                    },
                    "tabs": {
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOtherInfoTabs.add(tabs);
            var flxTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTabsSeperator.add();
            var flxOtherInfoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherInfoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoDetails.setDefaultUnit(kony.flex.DP);
            var flxToggleButtonsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxToggleButtonsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToggleButtonsContainer.setDefaultUnit(kony.flex.DP);
            var toggleButtons = new com.adminConsole.common.toggleButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "30dp",
                "id": "toggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnToggleLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailBanking\")"
                    },
                    "btnToggleRight": {
                        "text": "Business Banking"
                    },
                    "toggleButtons": {
                        "height": "30dp",
                        "left": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblToggleRolesHorzLine = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblToggleRolesHorzLine",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggleButtonsContainer.add(toggleButtons, lblToggleRolesHorzLine);
            var flxGroupsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxGroupsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupsWrapper.setDefaultUnit(kony.flex.DP);
            var flxRolesLeftTabs = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxRolesLeftTabs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9",
                "top": "0dp",
                "width": "230dp",
                "zIndex": 2
            }, {}, {});
            flxRolesLeftTabs.setDefaultUnit(kony.flex.DP);
            var segRolesVerticalTabs = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "btnOption1": "CREATE BILL PAY",
                    "lblSelected1": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
                    "lblSeperator": "."
                }, {
                    "btnOption1": "VIEW BILL PAY",
                    "lblSelected1": "",
                    "lblSeperator": "."
                }, {
                    "btnOption1": "DELETE RECIPIENTS",
                    "lblSelected1": "",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segRolesVerticalTabs",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_f0fc7c14f03343b3a69ce4f6f0b5a1f3,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnOption1": "btnOption1",
                    "flxContentContainer": "flxContentContainer",
                    "flxImgArrow": "flxImgArrow",
                    "flxRow": "flxRow",
                    "lblSelected1": "lblSelected1",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRolesTabsVerticaleperator = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblRolesTabsVerticaleperator",
                "isVisible": true,
                "left": "230dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRolesLeftTabs.add(segRolesVerticalTabs, lblRolesTabsVerticaleperator);
            var flxRolesListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRolesListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRolesListing.setDefaultUnit(kony.flex.DP);
            var flxRolesEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxRolesEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolesEdit.setDefaultUnit(kony.flex.DP);
            var btnRolesEdit = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnRolesEdit",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var btnRolesBusinessEdit = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnRolesBusinessEdit",
                "isVisible": false,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var lblAssignedRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignedRoles",
                "isVisible": false,
                "left": "20px",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Assigned Roles 05",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRolesEdit.add(btnRolesEdit, btnRolesBusinessEdit, lblAssignedRoles);
            var segCustomerRoles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "id": "segCustomerRoles",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "34dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoAssignedRolesAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "160dp",
                "id": "flxNoAssignedRolesAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoAssignedRolesAvailable.setDefaultUnit(kony.flex.DP);
            var lblNoRolesAvailable = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "45%",
                "id": "lblNoRolesAvailable",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileRoles.NoRolesAssigned\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAssignRoles = new kony.ui.Button({
                "centerX": "50%",
                "height": "22dp",
                "id": "btnAssignRoles",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Assign Roles",
                "top": "10dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoAssignedRolesAvailable.add(lblNoRolesAvailable, btnAssignRoles);
            flxRolesListing.add(flxRolesEdit, segCustomerRoles, flxNoAssignedRolesAvailable);
            var flxNoFeaturesActionsCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxNoFeaturesActionsCont",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxNoFeaturesActionsCont.setDefaultUnit(kony.flex.DP);
            var lblNoFeaturesActionsHeader = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblNoFeaturesActionsHeader",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "You have not added additional features and actions yet.",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddFeaturesActions = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAddFeaturesActions",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add Features and Actions",
                "width": "165px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoFeaturesActionsCont.add(lblNoFeaturesActionsHeader, btnAddFeaturesActions);
            var flxAddFeaturesActionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "550dp",
                "id": "flxAddFeaturesActionsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "zIndex": 3
            }, {}, {});
            flxAddFeaturesActionsContainer.setDefaultUnit(kony.flex.DP);
            var flxNoFeatureError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNoFeatureError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxNoFeatureError.setDefaultUnit(kony.flex.DP);
            var flxNoFeatureErrorIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNoFeatureErrorIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxNoFeatureErrorIconContainer.setDefaultUnit(kony.flex.DP);
            var lblNoFeatureErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblNoFeatureErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoFeatureErrorIconContainer.add(lblNoFeatureErrorIcon);
            var lblNoFeatureErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoFeatureErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.CustomerProfileEntitle.select_atlease_one_feature\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoFeatureError.add(flxNoFeatureErrorIconContainer, lblNoFeatureErrorValue);
            var flxAddFeaturesActions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "470dp",
                "id": "flxAddFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddFeaturesActions.setDefaultUnit(kony.flex.DP);
            var addFeaturesAndActions = new com.adminConsole.customerRoles.addFeaturesAndActions({
                "height": "100%",
                "id": "addFeaturesAndActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddFeaturesActions.add(addFeaturesAndActions);
            var flxAddFeaturesActionsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddFeaturesActionsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxAddFeaturesActionsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblAddFeaturesButtonsSeprator = new kony.ui.Label({
                "height": "1dp",
                "id": "lblAddFeaturesButtonsSeprator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddFeaturesActionsButtons.add(commonButtons, lblAddFeaturesButtonsSeprator);
            flxAddFeaturesActionsContainer.add(flxNoFeatureError, flxAddFeaturesActions, flxAddFeaturesActionsButtons);
            var flxOtherAddtionalFeaturesActionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherAddtionalFeaturesActionsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "250dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxOtherAddtionalFeaturesActionsContainer.setDefaultUnit(kony.flex.DP);
            var flxOtherFeatureActionsEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherFeatureActionsEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxOtherFeatureActionsEdit.setDefaultUnit(kony.flex.DP);
            var btnEditAdditionalFeatures = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnEditAdditionalFeatures",
                "isVisible": true,
                "right": "0px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "0px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxOtherFeatureActionsEdit.add(btnEditAdditionalFeatures);
            var flxOtherAddtionalFeaturesActions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxOtherAddtionalFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherAddtionalFeaturesActions.setDefaultUnit(kony.flex.DP);
            flxOtherAddtionalFeaturesActions.add();
            flxOtherAddtionalFeaturesActionsContainer.add(flxOtherFeatureActionsEdit, flxOtherAddtionalFeaturesActions);
            var flxRolesEditContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "680px",
                "id": "flxRolesEditContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxRolesEditContainer.setDefaultUnit(kony.flex.DP);
            var flxRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxRoleError.setDefaultUnit(kony.flex.DP);
            var flxRoleErrorIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleErrorIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxRoleErrorIconContainer.setDefaultUnit(kony.flex.DP);
            var lblRoleErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblRoleErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleErrorIconContainer.add(lblRoleErrorIcon);
            var lblRoleErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.CustomerProfileRoles.select_atlease_one_role\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleError.add(flxRoleErrorIconContainer, lblRoleErrorValue);
            var flxRolesEditHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95dp",
                "id": "flxRolesEditHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRolesEditHeader.setDefaultUnit(kony.flex.DP);
            var flxSelectedRolesCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSelectedRolesCount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxSelectedRolesCount.setDefaultUnit(kony.flex.DP);
            var lblSelectedRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedRoles",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Selected Roles ",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedRolesCount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedRolesCount",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLbl192b45LatoBold13px",
                "text": "04",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedRolesTotalCount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedRolesTotalCount",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "of 10",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedRolesCount.add(lblSelectedRoles, lblSelectedRolesCount, lblSelectedRolesTotalCount);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "CopyslFbox2",
                "top": "0px",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "top": "8dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
            flxSearch.add(flxSearchContainer);
            var btnSelectAll = new kony.ui.Button({
                "id": "btnSelectAll",
                "isVisible": true,
                "left": "20dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
                "top": 80,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBackToRolesListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBackToRolesListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBackToRolesListing.setDefaultUnit(kony.flex.DP);
            var backToRolesListing = new com.adminConsole.customerMang.backToPageHeader({
                "height": "20px",
                "id": "backToRolesListing",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxBack": {
                        "left": "20px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBackToRolesListing.add(backToRolesListing);
            flxRolesEditHeader.add(flxSelectedRolesCount, flxSearch, btnSelectAll, flxBackToRolesListing);
            var segCustomerRolesEdit = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "height": "500dp",
                "id": "segCustomerRolesEdit",
                "isVisible": true,
                "left": "-2dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "95dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchNoRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxSearchNoRoles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "95dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchNoRoles.setDefaultUnit(kony.flex.DP);
            var lblNoRolesAvailableForSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "45%",
                "id": "lblNoRolesAvailableForSearch",
                "isVisible": true,
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.NoResultsFound\")",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchNoRoles.add(lblNoRolesAvailableForSearch);
            var flxButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxButtonsSeperator.add();
            var flxRoleEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxRoleEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxRoleEditButtons.setDefaultUnit(kony.flex.DP);
            var RoleEditButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "RoleEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "120px"
                    },
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRoleEditButtons.add(RoleEditButtons);
            flxRolesEditContainer.add(flxRoleError, flxRolesEditHeader, segCustomerRolesEdit, flxSearchNoRoles, flxButtonsSeperator, flxRoleEditButtons);
            flxGroupsWrapper.add(flxRolesLeftTabs, flxRolesListing, flxNoFeaturesActionsCont, flxAddFeaturesActionsContainer, flxOtherAddtionalFeaturesActionsContainer, flxRolesEditContainer);
            flxOtherInfoDetails.add(flxToggleButtonsContainer, flxGroupsWrapper);
            flxOtherInfoWrapper.add(flxOtherInfoTabs, flxTabsSeperator, flxOtherInfoDetails);
            flxGeneralInformationWrapper.add(flxGeneralInfoWrapper, flxOtherInfoWrapper);
            var flxEntitlementsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "35px",
                "id": "flxEntitlementsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "36px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "93px",
                "width": "200px",
                "zIndex": 2
            }, {}, {});
            flxEntitlementsContextualMenu.setDefaultUnit(kony.flex.DP);
            var entitlementsContextualMenu = new com.adminConsole.common.contextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "35px",
                "id": "entitlementsContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu": {
                        "bottom": "viz.val_cleared",
                        "height": "35px",
                        "maxHeight": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "imgOption1": {
                        "src": "configure2x.png"
                    },
                    "imgOption2": {
                        "src": "delete_2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "lblOption1": {
                        "text": "Configure"
                    },
                    "lblOption2": {
                        "text": "Remove"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEntitlementsContextualMenu.add(entitlementsContextualMenu);
            var ResetPasswordToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ResetPasswordToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "425dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "67%",
                        "right": "150px",
                        "top": "425dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px",
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledResetPasswordLink\")",
                        "width": "93%"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CSRAssistToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "58dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "80px",
                        "top": "58dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var EnrolToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EnrolToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "130px",
                "skin": "slFbox",
                "top": "242dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "40%",
                        "right": "130px",
                        "top": "242dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledEnrollLink\")"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "38px",
                "skin": "sknFlxTrans",
                "top": "60dp",
                "width": "200px",
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "38px",
                        "top": "60dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContent.add(flxGeneralInformationWrapper, flxEntitlementsContextualMenu, ResetPasswordToolTip, CSRAssistToolTip, EnrolToolTip, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 14
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "segNotes": {
                        "data": [
                            [{
                                    "fonticonArrow": "",
                                    "imgArrow": "img_down_arrow.png",
                                    "lblDate": "October 21, 2017"
                                },
                                [{
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }, {
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPanel.add(flxMainHeader, flxBreadCrumbs, flxModifySearch, flxMainContent, flxNote, flxLoading, flxHeaderDropdown);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "minWidth": "80px",
                        "right": "20px",
                        "text": "NO, LEAVE IT AS IT IS",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "text": "YES, OFFLINE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "isVisible": false,
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxPopupSelectEnrolEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupSelectEnrolEmail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopupSelectEnrolEmail.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSelectEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblPopupHeader = new kony.ui.Label({
                "id": "lblPopupHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.SendEnrollHeader\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            flxSelectEmailHeader.add(lblPopupHeader, flxPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Selectenroll\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstboxEmails = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "35px",
                "id": "lstboxEmails",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select", "kony.i18n.getLocalizedString(\"i18n.frmCustomers.selectEmail\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxPopupBody.add(flxSelectEmailHeader, lblPopUpMainMessage, lstboxEmails);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxPopUp.add(flxPopUpTopColor, flxPopupBody, flxPopUpButtons);
            flxPopupSelectEnrolEmail.add(flxPopUp);
            var flxEnableEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnableEstatement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnableEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatementTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEstatementTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementTopColor.setDefaultUnit(kony.flex.DP);
            flxEstatementTopColor.add();
            var flxEstatementInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEstatementInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementInner.setDefaultUnit(kony.flex.DP);
            var flxEstatementClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEstatementClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxEstatementClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementClose.add(lblPopUpClose, fontIconImgCLose);
            var lblEnableEstatement = new kony.ui.Label({
                "height": "20px",
                "id": "lblEnableEstatement",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnableEstatement\")",
                "top": "40px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEstatementHeader = new kony.ui.Label({
                "height": "20px",
                "id": "lblEstatementHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Following email ID will be used to send e-Statements",
                "top": "80px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxEstatementEmail = new kony.ui.ListBox({
                "bottom": "60px",
                "height": "30px",
                "id": "lbxEstatementEmail",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "120px",
                "width": "400px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblEstatementMessage = new kony.ui.Label({
                "bottom": "10px",
                "height": "40px",
                "id": "lblEstatementMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Once eStatements service is activated, the customer will stop receiving his/her account statements on paper for the selected accounts",
                "top": "170px",
                "width": "560px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementInner.add(flxEstatementClose, lblEnableEstatement, lblEstatementHeader, lbxEstatementEmail, lblEstatementMessage);
            var flxEstatementButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEstatementButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementButtons.setDefaultUnit(kony.flex.DP);
            var btnEstatementLeave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEstatementLeave",
                "isVisible": true,
                "right": "150px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnEstatementEnable = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEstatementEnable",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesEnable\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEstatementButtons.add(btnEstatementLeave, btnEstatementEnable);
            flxEstatement.add(flxEstatementTopColor, flxEstatementInner, flxEstatementButtons);
            flxEnableEstatement.add(flxEstatement);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxAlertPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAlertPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpClose.add(lblAlertPopupClose);
            var lblAlertPopUpHeader = new kony.ui.Label({
                "id": "lblAlertPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Preview Mode",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxAlertPopUpClose, lblAlertPopUpHeader);
            var flxAlertProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxAlertProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "SMS",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "PUSH",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "EMAIL",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "NOTIFICATION CENTER",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxAlertProductsTabs.add(flxAlertProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "Summer discount",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": true,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Title:Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n\nGet Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxAlertPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAlertPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnAlertPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpButtons.add(btnAlertPopUpCancel);
            flxAlertPopUp.add(flxAlertPopUpTopColor, flxPreviewPopupHeader, flxAlertProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxAlertPopUpButtons);
            flxPreviewPopup.add(flxAlertPopUp);
            var flxViewRolePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxViewRolePopup.setDefaultUnit(kony.flex.DP);
            var flxRoleDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "575dp",
                "id": "flxRoleDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "zIndex": 1
            }, {}, {});
            flxRoleDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxRolePopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxRolePopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupTopColor.setDefaultUnit(kony.flex.DP);
            flxRolePopupTopColor.add();
            var flxRolePopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRolePopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupBody.setDefaultUnit(kony.flex.DP);
            var flxRoleClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxRoleClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRoleClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleClosePopup.add(lblIconClosePopup);
            var flxRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRoleName.setDefaultUnit(kony.flex.DP);
            var lbViewRoleName = new kony.ui.Label({
                "id": "lbViewRoleName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Bill Payment",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleName.add(lbViewRoleName, lblSeperator);
            var flxViewRoleWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewRoleWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRoleWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionViewRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionViewRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxDescriptionViewRole.setDefaultUnit(kony.flex.DP);
            var lblViewRoleDescriptionHeading = new kony.ui.Label({
                "id": "lblViewRoleDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRoleDescriptionValue = new kony.ui.Label({
                "id": "lblViewRoleDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionViewRole.add(lblViewRoleDescriptionHeading, lblViewRoleDescriptionValue);
            var flxFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "30px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "400px",
                "horizontalScrollIndicator": true,
                "id": "flxFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxFeaturesList.setDefaultUnit(kony.flex.DP);
            var ViewRoleFeaturesActions = new com.adminConsole.customerRoles.ViewRoleFeaturesActions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewRoleFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "ViewRoleFeaturesActions": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeaturesList.add(ViewRoleFeaturesActions);
            flxViewRoleWrapper.add(flxDescriptionViewRole, flxFeaturesList);
            flxRolePopupBody.add(flxRoleClosePopup, flxRoleName, flxViewRoleWrapper);
            flxRoleDetailsContainer.add(flxRolePopupTopColor, flxRolePopupBody);
            flxViewRolePopup.add(flxRoleDetailsContainer);
            var flxViewFeatureDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewFeatureDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewFeatureDetails.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxFeatureDetailsPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTop.setDefaultUnit(kony.flex.DP);
            flxBlueTop.add();
            var flxFeatureDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98.50%",
                "id": "flxFeatureDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFeatureDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFeatureDetailsClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblFontIconClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsClose.add(lblFontIconClose);
            var flxFeatureDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Feature Details -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDetailsHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Bill Payments",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsHeader.add(lblFeatureDetailsHeader1, lblFeatureDetailsHeader2);
            var flxFeatureStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatus.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatus = new kony.ui.Label({
                "id": "lblStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureStatus.add(fontIconActive, lblStatus);
            var lblSeparator = new kony.ui.Label({
                "bottom": "15px",
                "height": "1px",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "15px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFeatureDescription.setDefaultUnit(kony.flex.DP);
            var lblFeatureDescriptionHeading = new kony.ui.Label({
                "id": "lblFeatureDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDescriptionValue = new kony.ui.Label({
                "id": "lblFeatureDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDescription.add(lblFeatureDescriptionHeading, lblFeatureDescriptionValue);
            var flxActionsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "68%",
                "horizontalScrollIndicator": true,
                "id": "flxActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActionsList.setDefaultUnit(kony.flex.DP);
            var flxActionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxActionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 10
            }, {}, {});
            flxActionsHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderSeperator1 = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator1",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionName",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionHeader",
                "isVisible": true,
                "left": "40%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblHeaderSeperator2 = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator2",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionsHeader.add(lblHeaderSeperator1, lblActionName, lblDescriptionHeader, lblHeaderSeperator2);
            var segActions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }],
                "groupCells": false,
                "id": "segActions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxFeatureDetailsActions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatureDetailsActions": "flxFeatureDetailsActions",
                    "lblActionDescription": "lblActionDescription",
                    "lblActionName": "lblActionName",
                    "lblSeparator": "lblSeparator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxActionsList.add(flxActionsHeader, segActions, flxNoRecordsFound);
            flxFeatureDetailsContainer.add(flxFeatureDetailsClose, flxFeatureDetailsHeader, flxFeatureStatus, lblSeparator, flxFeatureDescription, flxActionsList);
            flxFeatureDetailsPopUp.add(flxBlueTop, flxFeatureDetailsContainer);
            flxViewFeatureDetails.add(flxFeatureDetailsPopUp);
            var flxLinkProfilesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLinkProfilesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLinkProfilesPopup.setDefaultUnit(kony.flex.DP);
            var linkProfilesPopup = new com.adminConsole.businessBanking.linkProfilesPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "linkProfilesPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLinkProfilesPopup.add(linkProfilesPopup);
            var flxDelinkProfilePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDelinkProfilePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxDelinkProfilePopup.setDefaultUnit(kony.flex.DP);
            var delinkProfilePopup = new com.adminConsole.customerMang.delinkProfilePopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "delinkProfilePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDelinkProfilePopup.add(delinkProfilePopup);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxPopUpConfirmation, flxPopUpError, flxPopupSelectEnrolEmail, flxEnableEstatement, flxPreviewPopup, flxViewRolePopup, flxViewFeatureDetails, flxLinkProfilesPopup, flxDelinkProfilePopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerProfileRoles,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerProfileRoles",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f3cfe4b22a874dea94ed7e1929bd2a02(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_ia44410f215f4d348e729dc6e0788ed0,
            "retainScrollPosition": false
        }]
    }
});