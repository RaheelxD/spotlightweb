define("CustomerManagementModule/frmUpgradeUser", function() {
    return function(controller) {
        function addWidgetsfrmUpgradeUser() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "top": "58px"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "100px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "2px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "left": "0px",
                        "text": "ALERTS"
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "btnPreviousPage1": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsDown": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUpgradeUser.companyInfo\")",
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumb.add(breadcrumbs);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "30px",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxUpgradeUserDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "90%",
                "horizontalScrollIndicator": true,
                "id": "flxUpgradeUserDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxFFFFFF",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUpgradeUserDetails.setDefaultUnit(kony.flex.DP);
            var flxCompanyDetailsRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCompanyDetailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow2.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            flxName.add(lblName, tbxNameValue, flxNoNameError);
            var flxContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "105px",
                "id": "flxContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxContactNumber.setDefaultUnit(kony.flex.DP);
            var lblContactNumber = new kony.ui.Label({
                "id": "lblContactNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompnayContact\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var contactNumber = new com.adminConsole.common.contactNumber({
                "height": "100%",
                "id": "contactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "contactNumber": {
                        "top": "30dp"
                    },
                    "flxContactNumberWrapper": {
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxError": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "top": "40dp",
                        "width": "100%"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "33%"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "width": "61%"
                    },
                    "txtISDCode": {
                        "isVisible": true,
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContactNumber.add(lblContactNumber, contactNumber);
            var flxEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "240px",
                "zIndex": 2
            }, {}, {});
            flxEmailID.setDefaultUnit(kony.flex.DP);
            var lblEmailID = new kony.ui.Label({
                "id": "lblEmailID",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompanyEmailID\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxEmailValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CompanyEmailID\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoEmailError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailError.setDefaultUnit(kony.flex.DP);
            var lblNoEmailErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmailError = new kony.ui.Label({
                "id": "lblNoEmailError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailError.add(lblNoEmailErrorIcon, lblNoEmailError);
            flxEmailID.add(lblEmailID, tbxEmailValue, flxNoEmailError);
            flxCompanyDetailsRow2.add(flxName, flxContactNumber, flxEmailID);
            var flxCompanyDetailsRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90px",
                "id": "flxCompanyDetailsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow3.setDefaultUnit(kony.flex.DP);
            var flxMBTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxMBTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxMBTin.setDefaultUnit(kony.flex.DP);
            var textBoxEntryTin = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnCheck": {
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    },
                    "flxBtnCheck": {
                        "isVisible": false,
                        "right": "0dp",
                        "top": "30dp",
                        "width": "26%"
                    },
                    "flxInlineError": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.no_tin_error\")"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TaxNumberOptional\")",
                        "left": "0dp"
                    },
                    "lblOptional": {
                        "isVisible": false
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.TaxNumber\")",
                        "left": "0dp",
                        "right": "90dp",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "textBoxEntry": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblTINErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblTINErrorIcon",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "75dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTinCheck = new kony.ui.Label({
                "id": "lblTinCheck",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.tin_valid_success\")",
                "top": "75dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMBTin.add(textBoxEntryTin, lblTINErrorIcon, lblTinCheck);
            flxCompanyDetailsRow3.add(flxMBTin);
            var flxCompanyDetailsRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCompanyDetailsRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxCompanyDetailsRow4.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 20,
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "0dp",
                "width": "46.10%",
                "zIndex": 2
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var segSearch = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAddress": "",
                    "lblPinIcon": ""
                }],
                "groupCells": false,
                "height": "75%",
                "id": "segSearch",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchCompanyMap",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSearchCompanyMap": "flxSearchCompanyMap",
                    "lblAddress": "lblAddress",
                    "lblPinIcon": "lblPinIcon"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var map1 = new com.konymp.map1({
                "height": "90%",
                "id": "map1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "overrides": {
                    "map1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90%",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "40dp"
                    },
                    "mapLocations": {
                        "height": "100%",
                        "top": "0%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchContainer.add(tbxSearch, segSearch, map1);
            var flxAddressContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "flxAddressContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 10,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46.40%"
            }, {}, {});
            flxAddressContainer.setDefaultUnit(kony.flex.DP);
            var flxStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxStreetName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStreetName.setDefaultUnit(kony.flex.DP);
            var lblStreetName = new kony.ui.Label({
                "id": "lblStreetName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxStreetName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxStreetName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoStreetName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoStreetName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoStreetName.setDefaultUnit(kony.flex.DP);
            var lblNoStreetNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStreetNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStreetNameError = new kony.ui.Label({
                "id": "lblNoStreetNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoStreetName.add(lblNoStreetNameErrorIcon, lblNoStreetNameError);
            flxStreetName.add(lblStreetName, tbxStreetName, flxNoStreetName);
            var flxBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxBuildingName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBuildingName.setDefaultUnit(kony.flex.DP);
            var lblBuildingName = new kony.ui.Label({
                "id": "lblBuildingName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblBuildingNumber\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxBuildingName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxBuildingName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.BuildingName\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoBuildingName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoBuildingName",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoBuildingName.setDefaultUnit(kony.flex.DP);
            var lblNoBuildingNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoBuildingNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBuildingNameError = new kony.ui.Label({
                "id": "lblNoBuildingNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBuildingName.add(lblNoBuildingNameErrorIcon, lblNoBuildingNameError);
            flxBuildingName.add(lblBuildingName, tbxBuildingName, flxNoBuildingName);
            var flxZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxZipCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxZipCode.setDefaultUnit(kony.flex.DP);
            var lblZipCode = new kony.ui.Label({
                "id": "lblZipCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxZipCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxZipCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryZip = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZip",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoZipCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoZipCode.setDefaultUnit(kony.flex.DP);
            var lblNoZipErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoZipError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoZipCode.add(lblNoZipErrorIcon, lblNoZipError);
            flxZipCode.add(lblZipCode, tbxZipCode, imgMandatoryZip, flxNoZipCode);
            var flxCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "180dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCountry.setDefaultUnit(kony.flex.DP);
            var lblCountry = new kony.ui.Label({
                "id": "lblCountry",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCountryTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCountryTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCountryTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCountry = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCountry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")"
                    },
                    "typeHead": {
                        "height": "170dp",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCountryTypeHead.add(typeHeadCountry);
            var imgMandatoryCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountry.setDefaultUnit(kony.flex.DP);
            var lblNoCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountry.add(lblNoCountryErrorIcon, lblNoCountryError);
            flxCountry.add(lblCountry, flxCountryTypeHead, imgMandatoryCountry, flxNoCountry);
            var flxState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxState.setDefaultUnit(kony.flex.DP);
            var lblState = new kony.ui.Label({
                "id": "lblState",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStateTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStateTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxStateTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadState = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStateTypeHead.add(typeHeadState);
            var imgMandatoryState = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryState",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoState.setDefaultUnit(kony.flex.DP);
            var lblNoStateErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStateError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoState.add(lblNoStateErrorIcon, lblNoStateError);
            flxState.add(lblState, flxStateTypeHead, imgMandatoryState, flxNoState);
            var flxCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "270dp",
                "width": "47.30%",
                "zIndex": 1
            }, {}, {});
            flxCity.setDefaultUnit(kony.flex.DP);
            var lblCity = new kony.ui.Label({
                "id": "lblCity",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCityTypeHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCityTypeHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCityTypeHead.setDefaultUnit(kony.flex.DP);
            var typeHeadCity = new com.adminConsole.common.typeHead({
                "height": "170dp",
                "id": "typeHeadCity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segSearchResult": {
                        "isVisible": false
                    },
                    "tbxSearchKey": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")"
                    },
                    "typeHead": {
                        "height": "170dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCityTypeHead.add(typeHeadCity);
            var imgMandatoryCity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCity",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCity.setDefaultUnit(kony.flex.DP);
            var lblNoCityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCity.add(lblNoCityErrorIcon, lblNoCityError);
            flxCity.add(lblCity, flxCityTypeHead, imgMandatoryCity, flxNoCity);
            flxAddressContainer.add(flxStreetName, flxBuildingName, flxZipCode, flxCountry, flxState, flxCity);
            flxCompanyDetailsRow4.add(flxSearchContainer, flxAddressContainer);
            flxUpgradeUserDetails.add(flxCompanyDetailsRow2, flxCompanyDetailsRow3, flxCompanyDetailsRow4);
            var flxDetailsBottomButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxDetailsBottomButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsBottomButtons.setDefaultUnit(kony.flex.DP);
            var flxRightButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxRightButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxRightButtonsSeperator.add();
            var commonButtonsDetails = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "commonButtonsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "18%"
                    },
                    "btnNext": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgrade\")",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.CommonButtons.Save_AddNew\")",
                        "isVisible": false,
                        "right": "35dp"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsBottomButtons.add(flxRightButtonsSeperator, commonButtonsDetails);
            flxDetails.add(flxUpgradeUserDetails, flxDetailsBottomButtons);
            flxMainContent.add(flxBreadCrumb, flxDetails);
            flxRightPanel.add(flxMainHeader, flxMainContent);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0px",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmUpgradeUser,
            "enabledForIdleTimeout": true,
            "id": "frmUpgradeUser",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_abe26a0756ef40ee96f3f9bcc93acba1(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});