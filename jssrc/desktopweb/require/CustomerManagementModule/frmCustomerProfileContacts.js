define("CustomerManagementModule/frmCustomerProfileContacts", function() {
    return function(controller) {
        function addWidgetsfrmCustomerProfileContacts() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": "0dp",
                        "text": "IMPORT CUSTOMER"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "top": "58px"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customer_Management\")"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnNotes\")",
                "top": "58dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxMainHeader.add(mainHeader, btnNotes);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "SEARCH"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "784px",
                "horizontalScrollIndicator": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "126px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxGeneralInformationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGeneralInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxGeneralInformationWrapper.setDefaultUnit(kony.flex.DP);
            var flxGeneralInfoWrapper = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxGeneralInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "700dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "ProfileGeneralInfo": {
                        "isVisible": true
                    },
                    "flxGeneralInfoCombinedDetail": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxOptions": {
                        "right": "2px"
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxGeneralInfoWrapperUnEnrolled = new com.adminConsole.CustomerManagement.ProfileGeneralInfo({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxGeneralInfoWrapperUnEnrolled",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "700dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "EditGeneralInfo.imgFlag1": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag2": {
                        "src": "checkbox.png"
                    },
                    "EditGeneralInfo.imgFlag3": {
                        "src": "checkbox.png"
                    },
                    "ProfileGeneralInfo": {
                        "isVisible": false
                    },
                    "alertMessage": {
                        "isVisible": false
                    },
                    "dashboardCommonTab.btnDeposits": {
                        "isVisible": false
                    },
                    "flxEditGeneralInformation": {
                        "isVisible": false
                    },
                    "flxGeneralInfoCombinedDetail": {
                        "isVisible": false
                    },
                    "flxGeneralInfoEditButton": {
                        "isVisible": false
                    },
                    "flxViewGeneralInformation": {
                        "isVisible": true
                    },
                    "generalInfoHeader.btnEnrollNow": {
                        "isVisible": true
                    },
                    "generalInfoHeader.flxApplicantTag": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxCSRAssist": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxLinkProfileButton": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxMicroBusinessTag": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxNotification": {
                        "isVisible": false,
                        "top": "10dp",
                        "width": "100%"
                    },
                    "generalInfoHeader.flxOptions": {
                        "isVisible": false,
                        "right": "2px"
                    },
                    "generalInfoHeader.flxResendActivationCode": {
                        "isVisible": true,
                        "width": "65%"
                    },
                    "generalInfoHeader.flxRiskStatus": {
                        "isVisible": false
                    },
                    "generalInfoHeader.flxUnlock": {
                        "isVisible": false
                    },
                    "imgArrow": {
                        "src": "img_down_arrow.png"
                    },
                    "row1.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row1.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row1.lblOption1": {
                        "isVisible": false
                    },
                    "row2.flxColumn2": {
                        "isVisible": false
                    },
                    "row2.flxColumn3": {
                        "isVisible": false
                    },
                    "row2.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row2.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row3": {
                        "isVisible": false
                    },
                    "row3.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row3.imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "row4": {
                        "isVisible": false
                    },
                    "row4.imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "row4.imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOtherInfoWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxOtherInfoWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoWrapper.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOtherInfoTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.customerMang.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60px",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0hfd18814fd664dCM",
                "overrides": {
                    "flxLeftArrow": {
                        "isVisible": false
                    },
                    "flxTabs": {
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "tabs": {
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOtherInfoTabs.add(tabs);
            var flxTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxTabsSeperator.add();
            var flxOtherInfoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOtherInfoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxOtherInfoDetails.setDefaultUnit(kony.flex.DP);
            var flxContactWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactWrapper.setDefaultUnit(kony.flex.DP);
            var flxToggleButtonsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxToggleButtonsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToggleButtonsContainer.setDefaultUnit(kony.flex.DP);
            var toggleButtons = new com.adminConsole.common.toggleButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "30dp",
                "id": "toggleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnToggleLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailBanking\")"
                    },
                    "btnToggleRight": {
                        "text": "Business Banking"
                    },
                    "toggleButtons": {
                        "height": "30dp",
                        "left": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToggleButtonsContainer.add(toggleButtons);
            var flxContactDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxContactDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxContactDetails.setDefaultUnit(kony.flex.DP);
            var ContactPrefTimeMethod = new com.adminConsole.customerMang.ContactNum({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContactPrefTimeMethod",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "overrides": {
                    "ContactNum": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "-5dp"
                    },
                    "btnAdd": {
                        "isVisible": false,
                        "left": 0
                    },
                    "btnDeleteAdditionalDetails1": {
                        "isVisible": false
                    },
                    "btnDeleteAdditionalDetails2": {
                        "isVisible": false
                    },
                    "btnEdit": {
                        "left": "viz.val_cleared"
                    },
                    "flxAdditionalDetails2": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxDetailsRow2": {
                        "isVisible": true
                    },
                    "flxPrimaryCommunication": {
                        "isVisible": false
                    },
                    "flxPrimaryLabelAndTag": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "lblAdditionalDetailsKey1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PREFERRED_TIME_FOR_CALL\")"
                    },
                    "lblAdditionalDetailsValue1": {
                        "left": "0dp",
                        "text": "Morning"
                    },
                    "lblPrimaryKey": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PREFERRED_METHOD\")"
                    },
                    "lblPrimaryValue": {
                        "text": "Call, email"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeprator.setDefaultUnit(kony.flex.DP);
            flxSeprator.add();
            var Address = new com.adminConsole.customerMang.Address({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "Address",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "Address": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "flxButtons": {
                        "width": "115px"
                    },
                    "imgDelete": {
                        "src": "delete_2x.png"
                    },
                    "imgDelete2": {
                        "src": "delete_2x.png"
                    },
                    "lblAddressPrimary": {
                        "text": "WORK"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeprator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeprator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeprator1.setDefaultUnit(kony.flex.DP);
            flxSeprator1.add();
            var ContactNum = new com.adminConsole.customerMang.ContactNum({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContactNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "ContactNum": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "btnAdd": {
                        "left": 0
                    },
                    "btnEdit": {
                        "left": "viz.val_cleared"
                    },
                    "flxButtons": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblAdditionalDetailsValue1": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "30px",
                        "width": "viz.val_cleared"
                    },
                    "lblAdditionalDetailsValue2": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "lblPrimaryValue": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCodeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxNoCodeError.setDefaultUnit(kony.flex.DP);
            var lblNoCodeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCodeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCodeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCodeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Minimum one contact number is required to enroll",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCodeError.add(lblNoCodeErrorIcon, lblNoCodeError);
            var flxSeprator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeprator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeprator2.setDefaultUnit(kony.flex.DP);
            flxSeprator2.add();
            var ContactEmail = new com.adminConsole.customerMang.ContactNum({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContactEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "ContactNum": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "btnAdd": {
                        "left": 0,
                        "right": "viz.val_cleared"
                    },
                    "btnEdit": {
                        "left": "viz.val_cleared"
                    },
                    "flxAdditionalDetails2": {
                        "left": "20px",
                        "top": "10px"
                    },
                    "flxButtons": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblAdditionalDetailsValue1": {
                        "left": "0dp",
                        "right": "0dp",
                        "text": "bryanvnash123@gmail.com",
                        "width": "viz.val_cleared"
                    },
                    "lblAdditionalDetailsValue2": {
                        "right": "0dp",
                        "text": "bryanvnash123@gmail.com",
                        "width": "viz.val_cleared"
                    },
                    "lblPrimaryCommunicationContact": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Primary_Communication_Email\")"
                    },
                    "lblPrimaryValue": {
                        "right": "0dp",
                        "text": "bryanvnash@gmail.com",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoCodeErrorContact = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCodeErrorContact",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxNoCodeErrorContact.setDefaultUnit(kony.flex.DP);
            var lblNoErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoErrorCodeEmail = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoErrorCodeEmail",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Minimum one email address is required to enroll",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCodeErrorContact.add(lblNoErrorIcon, lblNoErrorCodeEmail);
            flxContactDetails.add(ContactPrefTimeMethod, flxSeprator, Address, flxSeprator1, ContactNum, flxNoCodeError, flxSeprator2, ContactEmail, flxNoCodeErrorContact);
            var flxContactDetailsBB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxContactDetailsBB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknNormalDefault",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactDetailsBB.setDefaultUnit(kony.flex.DP);
            var flxSeperatorContactsBB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorContactsBB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorContactsBB.setDefaultUnit(kony.flex.DP);
            flxSeperatorContactsBB.add();
            var flxContactEditBB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxContactEditBB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 2
            }, {}, {});
            flxContactEditBB.setDefaultUnit(kony.flex.DP);
            var btnContactsBusinessEdit = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnContactsBusinessEdit",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxContactEditBB.add(btnContactsBusinessEdit);
            var flxContactsContainerBB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactsContainerBB",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxContactsContainerBB.setDefaultUnit(kony.flex.DP);
            var contactNumBB = new com.adminConsole.customerMang.ContactNum({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contactNumBB",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "overrides": {
                    "ContactNum": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "-5dp"
                    },
                    "btnAdd": {
                        "isVisible": false,
                        "left": 0
                    },
                    "btnEdit": {
                        "left": "viz.val_cleared"
                    },
                    "flxAdditionalDetails1": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxDetails1": {
                        "right": "viz.val_cleared",
                        "top": "15px",
                        "width": "100%"
                    },
                    "flxDetailsRow2": {
                        "isVisible": false
                    },
                    "flxPrimaryCommunication": {
                        "left": "20px"
                    },
                    "flxPrimaryLabelAndTag": {
                        "right": "40dp",
                        "width": kony.flex.USE_PREFFERED_SIZE,
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "lblAdditionalDetailsValue1": {
                        "left": "0dp",
                        "top": "30px"
                    },
                    "lblDetailsHeader": {
                        "bottom": "-2px",
                        "left": "20dp"
                    },
                    "lblPrimaryValue": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var contactEmailBB = new com.adminConsole.customerMang.ContactNum({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contactEmailBB",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "ContactNum": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "10dp"
                    },
                    "btnAdd": {
                        "left": 0,
                        "right": "viz.val_cleared"
                    },
                    "btnEdit": {
                        "left": "viz.val_cleared"
                    },
                    "flxAdditionalDetails1": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxDetails1": {
                        "top": "15px",
                        "width": "100%"
                    },
                    "flxDetailsRow2": {
                        "isVisible": false
                    },
                    "flxPrimaryCommunication": {
                        "left": "20px"
                    },
                    "flxPrimaryLabelAndTag": {
                        "right": "40dp",
                        "width": "viz.val_cleared",
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "lblAdditionalDetailsValue1": {
                        "text": "bryanvnash123@gmail.com"
                    },
                    "lblAdditionalDetailsValue2": {
                        "text": "bryanvnash123@gmail.com"
                    },
                    "lblDetailsHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.EmailAddress\")"
                    },
                    "lblPrimaryCommunicationContact": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Primary_Communication_Email\")"
                    },
                    "lblPrimaryValue": {
                        "isVisible": false,
                        "text": "bryanvnash@gmail.com"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContactsContainerBB.add(contactNumBB, contactEmailBB);
            flxContactDetailsBB.add(flxSeperatorContactsBB, flxContactEditBB, flxContactsContainerBB);
            var flxEditContactDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditContactDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0g34dd780040e45CM",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditContactDetails.setDefaultUnit(kony.flex.DP);
            var backToPageHeader = new com.adminConsole.customerMang.backToPageHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "backToPageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "backToPageHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "btnBack": {
                        "centerY": "viz.val_cleared",
                        "top": "20dp"
                    },
                    "flxBack": {
                        "centerY": "viz.val_cleared",
                        "left": "20px",
                        "top": "19dp"
                    },
                    "fontIconBack": {
                        "width": "16dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditContactAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditContactAddress",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknNormalDefault",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditContactAddress.setDefaultUnit(kony.flex.DP);
            var EditAddress = new com.adminConsole.customerMang.EditAddress({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "490px",
                "id": "EditAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "EditAddress": {
                        "height": "490px"
                    },
                    "imgRadio1": {
                        "src": "radio_notselected.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditAddressSeprator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAddressSeprator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2dp",
                "isModalContainer": false,
                "skin": "sknflx115678",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxEditAddressSeprator1.setDefaultUnit(kony.flex.DP);
            flxEditAddressSeprator1.add();
            var EditAddress1 = new com.adminConsole.customerMang.EditAddress({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "490px",
                "id": "EditAddress1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "EditAddress": {
                        "height": "490px"
                    },
                    "imgRadio1": {
                        "src": "radio_notselected.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditAddressSeprator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAddressSeprator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2dp",
                "isModalContainer": false,
                "skin": "sknflx115678",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxEditAddressSeprator2.setDefaultUnit(kony.flex.DP);
            flxEditAddressSeprator2.add();
            var EditAddress2 = new com.adminConsole.customerMang.EditAddress({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "490px",
                "id": "EditAddress2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "EditAddress": {
                        "height": "490px"
                    },
                    "imgRadio1": {
                        "src": "radio_notselected.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddressEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "81px",
                "id": "flxAddressEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddressEditButtons.setDefaultUnit(kony.flex.DP);
            var flxAddressSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddressSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddressSeparator.setDefaultUnit(kony.flex.DP);
            flxAddressSeparator.add();
            var flxAddressButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddressButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "1px",
                "zIndex": 1
            }, {}, {});
            flxAddressButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var addressEditButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "addressEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddressButtonsWrapper.add(addressEditButtons);
            flxAddressEditButtons.add(flxAddressSeparator, flxAddressButtonsWrapper);
            flxEditContactAddress.add(EditAddress, flxEditAddressSeprator1, EditAddress1, flxEditAddressSeprator2, EditAddress2, flxAddressEditButtons);
            var flxEditContactNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditContactNo",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditContactNo.setDefaultUnit(kony.flex.DP);
            var EditContact = new com.adminConsole.customerMang.EditContact({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EditContact",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "width": "100%",
                "overrides": {
                    "imgRadio1": {
                        "src": "radio_selected.png"
                    },
                    "imgRadio2": {
                        "src": "radio_notselected.png"
                    },
                    "imgRadio3": {
                        "src": "radio_notselected.png"
                    },
                    "txtfldContactNum1": {
                        "maxTextLength": 10
                    },
                    "txtfldContactNum2": {
                        "maxTextLength": 10
                    },
                    "txtfldContactNum3": {
                        "maxTextLength": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditContactButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "81px",
                "id": "flxEditContactButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditContactButtons.setDefaultUnit(kony.flex.DP);
            var flxContactsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxContactsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactsSeparator.setDefaultUnit(kony.flex.DP);
            flxContactsSeparator.add();
            var flxEditContactButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditContactButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "1dp",
                "zIndex": 1
            }, {}, {});
            flxEditContactButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var editContactButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "editContactButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditContactButtonsWrapper.add(editContactButtons);
            flxEditContactButtons.add(flxContactsSeparator, flxEditContactButtonsWrapper);
            flxEditContactNo.add(EditContact, flxEditContactButtons);
            var flxEditEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditEmailId.setDefaultUnit(kony.flex.DP);
            var EditEmailId = new com.adminConsole.customerMang.EditContact({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EditEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "width": "100%",
                "overrides": {
                    "contactNumber1": {
                        "isVisible": false
                    },
                    "contactNumber2": {
                        "isVisible": false
                    },
                    "contactNumber3": {
                        "isVisible": false
                    },
                    "flxContactHeader": {
                        "left": "0px",
                        "top": "0dp"
                    },
                    "imgRadio1": {
                        "src": "radio_selected.png"
                    },
                    "imgRadio2": {
                        "src": "radio_notselected.png"
                    },
                    "imgRadio3": {
                        "src": "radio_notselected.png"
                    },
                    "lblErrorContact1": {
                        "isVisible": false,
                        "text": "Invalid email address"
                    },
                    "lblErrorContact2": {
                        "text": "Invalid email address"
                    },
                    "lblErrorContact3": {
                        "text": "Invalid email address"
                    },
                    "txtfldContactNum1": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")"
                    },
                    "txtfldContactNum2": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")"
                    },
                    "txtfldContactNum3": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditEmailIdButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditEmailIdButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditEmailIdButtons.setDefaultUnit(kony.flex.DP);
            var flxEmailSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxEmailSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailSeparator.setDefaultUnit(kony.flex.DP);
            flxEmailSeparator.add();
            var flxEditEmailIdButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditEmailIdButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditEmailIdButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var editEmailIdButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "editEmailIdButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditEmailIdButtonsWrapper.add(editEmailIdButtons);
            flxEditEmailIdButtons.add(flxEmailSeparator, flxEditEmailIdButtonsWrapper);
            flxEditEmailId.add(EditEmailId, flxEditEmailIdButtons);
            var flxEditTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditTime",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditTime.setDefaultUnit(kony.flex.DP);
            var flxContactHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "49px",
                "id": "flxContactHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0b7c2de721b0a4fCM",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactHeader.setDefaultUnit(kony.flex.DP);
            var lblContactHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Communication\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContactHeader.add(lblContactHeader);
            var flxContactRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "40px",
                "skin": "sknNormalDefault",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactRow1.setDefaultUnit(kony.flex.DP);
            var flxContactDetails2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxContactDetails2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox0f14e6ffcf98744CM",
                "top": "10px",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxContactDetails2.setDefaultUnit(kony.flex.DP);
            var lblContactParam2 = new kony.ui.Label({
                "id": "lblContactParam2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PREFERRED_METHOD\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCheckBox1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxCheckBox1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "38px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxCheckBox1.setDefaultUnit(kony.flex.DP);
            var imgCheckBox1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgCheckBox1",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox1.add(imgCheckBox1);
            var lblCheckBox1 = new kony.ui.Label({
                "id": "lblCheckBox1",
                "isVisible": true,
                "left": "25dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblCheckBox1\")",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCheckBox2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxCheckBox2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "38px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxCheckBox2.setDefaultUnit(kony.flex.DP);
            var imgCheckBox2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgCheckBox2",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox2.add(imgCheckBox2);
            var lblCheckBox2 = new kony.ui.Label({
                "id": "lblCheckBox2",
                "isVisible": true,
                "left": "100px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEmailCheckBox\")",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContactDetails2.add(lblContactParam2, flxCheckBox1, lblCheckBox1, flxCheckBox2, lblCheckBox2);
            flxContactRow1.add(flxContactDetails2);
            var flxContactRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxContactRow2.setDefaultUnit(kony.flex.DP);
            var lblContactParam1 = new kony.ui.Label({
                "id": "lblContactParam1",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PREFERRED_TIME_FOR_CALL\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPreferredTimeCheckboxes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPreferredTimeCheckboxes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "30px",
                "zIndex": 3
            }, {}, {});
            flxPreferredTimeCheckboxes.setDefaultUnit(kony.flex.DP);
            var flxTimeCheckBox1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxTimeCheckBox1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxTimeCheckBox1.setDefaultUnit(kony.flex.DP);
            var imgTimeCheckBox1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgTimeCheckBox1",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTimeCheckBox1.add(imgTimeCheckBox1);
            var lblPreferredTime1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreferredTime1",
                "isVisible": true,
                "left": "7px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Morning\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTimeCheckBox2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxTimeCheckBox2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxTimeCheckBox2.setDefaultUnit(kony.flex.DP);
            var imgTimeCheckBox2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgTimeCheckBox2",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTimeCheckBox2.add(imgTimeCheckBox2);
            var lblPreferredTime2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreferredTime2",
                "isVisible": true,
                "left": "7px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Afternoon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTimeCheckBox3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxTimeCheckBox3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxTimeCheckBox3.setDefaultUnit(kony.flex.DP);
            var imgTimeCheckBox3 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgTimeCheckBox3",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTimeCheckBox3.add(imgTimeCheckBox3);
            var lblPreferredTime3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreferredTime3",
                "isVisible": true,
                "left": "7px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Evening\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTimeCheckBox4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxTimeCheckBox4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxTimeCheckBox4.setDefaultUnit(kony.flex.DP);
            var imgTimeCheckBox4 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgTimeCheckBox4",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTimeCheckBox4.add(imgTimeCheckBox4);
            var lblPreferredTime4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreferredTime4",
                "isVisible": true,
                "left": "7px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Anytime\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreferredTimeCheckboxes.add(flxTimeCheckBox1, lblPreferredTime1, flxTimeCheckBox2, lblPreferredTime2, flxTimeCheckBox3, lblPreferredTime3, flxTimeCheckBox4, lblPreferredTime4);
            flxContactRow2.add(lblContactParam1, flxPreferredTimeCheckboxes);
            var flxEditTimeButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditTimeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditTimeButtons.setDefaultUnit(kony.flex.DP);
            var flxEditTimeButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditTimeButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditTimeButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var editTimeButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "editTimeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditTimeButtonsWrapper.add(editTimeButtons);
            flxEditTimeButtons.add(flxEditTimeButtonsWrapper);
            flxEditTime.add(flxContactHeader, flxContactRow1, flxContactRow2, flxEditTimeButtons);
            flxEditContactDetails.add(backToPageHeader, flxEditContactAddress, flxEditContactNo, flxEditEmailId, flxEditTime);
            flxContactWrapper.add(flxToggleButtonsContainer, flxContactDetails, flxContactDetailsBB, flxEditContactDetails);
            flxOtherInfoDetails.add(flxContactWrapper);
            flxOtherInfoWrapper.add(flxOtherInfoTabs, flxTabsSeperator, flxOtherInfoDetails);
            flxGeneralInformationWrapper.add(flxGeneralInfoWrapper, flxGeneralInfoWrapperUnEnrolled, flxOtherInfoWrapper);
            var ResetPasswordToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ResetPasswordToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "425dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "67%",
                        "right": "150px",
                        "top": "425dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px",
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledResetPasswordLink\")",
                        "width": "93%"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CSRAssistToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CSRAssistToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "58dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "80px",
                        "top": "58dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var EnrolToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EnrolToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "130px",
                "skin": "slFbox",
                "top": "242dp",
                "width": "230px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "40%",
                        "right": "130px",
                        "top": "242dp",
                        "width": "230px"
                    },
                    "flxToolTipMessage": {
                        "height": "50px"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DisabledEnrollLink\")"
                    },
                    "lblarrow": {
                        "centerX": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSelectOptions = new com.adminConsole.customerMang.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "38px",
                "skin": "sknFlxTrans",
                "top": "60dp",
                "width": "200px",
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")"
                    },
                    "selectOptions": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "38px",
                        "top": "60dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContent.add(flxGeneralInformationWrapper, ResetPasswordToolTip, CSRAssistToolTip, EnrolToolTip, flxSelectOptions);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 14
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "imgCloseNotes": {
                        "src": "close_blue.png"
                    },
                    "segNotes": {
                        "data": [
                            [{
                                    "fonticonArrow": "",
                                    "imgArrow": "img_down_arrow.png",
                                    "lblDate": "October 21, 2017"
                                },
                                [{
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }, {
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0px",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxMainContent, flxNote, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "toastMessage": {
                        "zIndex": 20
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "minWidth": "80dp",
                        "right": "20px",
                        "text": "NO, LEAVE IT AS IT IS",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "text": "YES",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Are_you_sure_to\")",
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            var flxPopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpError.setDefaultUnit(kony.flex.DP);
            var popUpError = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "isVisible": true,
                        "right": "20px",
                        "text": "CLOSE",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "isVisible": false,
                        "text": "YES, OFFLINE",
                        "width": "150px"
                    },
                    "flxPopUp": {
                        "centerY": "viz.val_cleared",
                        "top": "220px"
                    },
                    "flxPopUpButtons": {
                        "height": "80px",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Set the Branch Offline?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to set the Branch to Offline mode?<br><br>\n\nIf you set it offline, the Branch does not show operational to the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpError.add(popUpError);
            var flxPopupSelectEnrolEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupSelectEnrolEmail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopupSelectEnrolEmail.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupBody.setDefaultUnit(kony.flex.DP);
            var flxSelectEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblPopupHeader = new kony.ui.Label({
                "id": "lblPopupHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.SendEnrollHeader\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            flxSelectEmailHeader.add(lblPopupHeader, flxPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Selectenroll\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstboxEmails = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "35px",
                "id": "lstboxEmails",
                "isVisible": true,
                "left": "20dp",
                "masterData": [
                    ["Select", "kony.i18n.getLocalizedString(\"i18n.frmCustomers.selectEmail\")"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxPopupBody.add(flxSelectEmailHeader, lblPopUpMainMessage, lstboxEmails);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Send\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxPopUp.add(flxPopUpTopColor, flxPopupBody, flxPopUpButtons);
            flxPopupSelectEnrolEmail.add(flxPopUp);
            var flxEnableEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEnableEstatement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxEnableEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxEstatement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxEstatement.setDefaultUnit(kony.flex.DP);
            var flxEstatementTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEstatementTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementTopColor.setDefaultUnit(kony.flex.DP);
            flxEstatementTopColor.add();
            var flxEstatementInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEstatementInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementInner.setDefaultUnit(kony.flex.DP);
            var flxEstatementClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEstatementClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxEstatementClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementClose.add(lblPopUpClose, fontIconImgCLose);
            var lblEnableEstatement = new kony.ui.Label({
                "height": "20px",
                "id": "lblEnableEstatement",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnableEstatement\")",
                "top": "40px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEstatementHeader = new kony.ui.Label({
                "height": "20px",
                "id": "lblEstatementHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Following email ID will be used to send e-Statements",
                "top": "80px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxEstatementEmail = new kony.ui.ListBox({
                "bottom": "60px",
                "height": "30px",
                "id": "lbxEstatementEmail",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "120px",
                "width": "400px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var lblEstatementMessage = new kony.ui.Label({
                "bottom": "10px",
                "height": "40px",
                "id": "lblEstatementMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknrtxLato0df8337c414274d",
                "text": "Once eStatements service is activated, the customer will stop receiving his/her account statements on paper for the selected accounts",
                "top": "170px",
                "width": "560px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEstatementInner.add(flxEstatementClose, lblEnableEstatement, lblEstatementHeader, lbxEstatementEmail, lblEstatementMessage);
            var flxEstatementButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEstatementButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEstatementButtons.setDefaultUnit(kony.flex.DP);
            var btnEstatementLeave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnEstatementLeave",
                "isVisible": true,
                "right": "150px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnEstatementEnable = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEstatementEnable",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesEnable\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEstatementButtons.add(btnEstatementLeave, btnEstatementEnable);
            flxEstatement.add(flxEstatementTopColor, flxEstatementInner, flxEstatementButtons);
            flxEnableEstatement.add(flxEstatement);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxAlertPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAlertPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpClose.add(lblAlertPopupClose);
            var lblAlertPopUpHeader = new kony.ui.Label({
                "id": "lblAlertPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Preview Mode",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxAlertPopUpClose, lblAlertPopUpHeader);
            var flxAlertProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxAlertProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAlertProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxedefefOpNoBorder",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "SMS",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "PUSH",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "EMAIL",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "NOTIFICATION CENTER",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            flxAlertProductsTabs.add(flxAlertProductsTabsWrapper);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "Summer discount",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": true,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLatoBold35475f14px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "91%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Title:Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n\nGet Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.\n Get Credited with 2000 $  Summer Discount  on shop on Walmart above 10000$ by end of 25th March 2019.",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxAlertPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAlertPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnAlertPopUpCancel",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPopUpButtons.add(btnAlertPopUpCancel);
            flxAlertPopUp.add(flxAlertPopUpTopColor, flxPreviewPopupHeader, flxAlertProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxAlertPopUpButtons);
            flxPreviewPopup.add(flxAlertPopUp);
            var flxLinkProfilesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLinkProfilesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLinkProfilesPopup.setDefaultUnit(kony.flex.DP);
            var linkProfilesPopup = new com.adminConsole.businessBanking.linkProfilesPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "linkProfilesPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLinkProfilesPopup.add(linkProfilesPopup);
            var flxDelinkProfilePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDelinkProfilePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxDelinkProfilePopup.setDefaultUnit(kony.flex.DP);
            var delinkProfilePopup = new com.adminConsole.customerMang.delinkProfilePopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "delinkProfilePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDelinkProfilePopup.add(delinkProfilePopup);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxPopUpConfirmation, flxPopUpError, flxPopupSelectEnrolEmail, flxEnableEstatement, flxPreviewPopup, flxLinkProfilesPopup, flxDelinkProfilePopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var CSRAssist = new com.adminConsole.customerMang.CSRAssist({
                "height": "100%",
                "id": "CSRAssist",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50,
                "overrides": {
                    "CSRAssist": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.add(flxMain, flxEditCancelConfirmation, CSRAssist);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerProfileContacts,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerProfileContacts",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e516afbd3fb0494f8ef97c0a93d9d22a(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_f314e833ae294c55a2b8348370ac9796,
            "retainScrollPosition": false
        }]
    }
});