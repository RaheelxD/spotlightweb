define("flxAccountCentricDetails", function() {
    return function(controller) {
        var flxAccountCentricDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxAccountCentricDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "sknCursor"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAccountCentricDetails.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxRadio = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxRadio.setDefaultUnit(kony.flex.DP);
        var imgRadio = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRadio.add(imgRadio);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "20px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxDetails.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "John",
            "width": "40%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSSN = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSSN",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "123-223-990",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMemberType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMemberType",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Primary Account Holder",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDetails.add(lblName, lblSSN, lblMemberType);
        var flxUnlink = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUnlink",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxUnlink.setDefaultUnit(kony.flex.DP);
        var lblUnlink = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblUnlink",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcomoon20px",
            "text": "",
            "top": "20dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUnlink.add(lblUnlink);
        flxContent.add(flxRadio, flxDetails, flxUnlink);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "20dp",
            "skin": "sknLblD5D9DD1000",
            "text": ".",
            "top": "49px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountCentricDetails.add(flxContent, lblSeperator);
        return flxAccountCentricDetails;
    }
})