define("userflxAddUsersController", {
    addUsers: function() {
        var index = kony.application.getCurrentForm().segAddOptions.selectedIndex;
        var rowIndex = index[0];
        var data = kony.application.getCurrentForm().segAddOptions.data;
        var lblOptionText = data[rowIndex].lblFullName;
        var toAdd = {
            "imgClose": "close_blue.png",
            "lblOption": "" + lblOptionText
        };
        var data2 = kony.application.getCurrentForm().segSelectedOptions.data;
        data2.push(toAdd);
        kony.application.getCurrentForm().segSelectedOptions.setData(data2);
        kony.application.getCurrentForm().forceLayout();
    },
});
define("flxAddUsersControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_i5f0e1b1d621469bb17b0e21177ee705: function AS_Button_i5f0e1b1d621469bb17b0e21177ee705(eventobject, context) {
        var self = this;
        this.executeOnParent("addUserstoRole");
    }
});
define("flxAddUsersController", ["userflxAddUsersController", "flxAddUsersControllerActions"], function() {
    var controller = require("userflxAddUsersController");
    var controllerActions = ["flxAddUsersControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
