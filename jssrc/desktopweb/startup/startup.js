kony.appinit.setApplicationMetaConfiguration("appid", "Spotlight");
kony.appinit.setApplicationMetaConfiguration("build", "debug");
kony.appinit.setApplicationMetaConfiguration("defaultLocale", "en_US");
kony.appinit.setApplicationMetaConfiguration("locales", ["de_DE", "el_GR", "en", "en_GB", "en_US", "es_ES", "fr_FR"]);
kony.appinit.setApplicationMetaConfiguration("i18nArray", []);
kony.appinit.setApplicationMetaConfiguration("localization", "true");
kony.appinit.setApplicationMetaConfiguration("i18nVersion", "2086740172");
//startup.js
var appConfig = {
    appId: "Spotlight",
    appName: "Customer360",
    appVersion: "2.1.0",
    isturlbase: "http://xd-test.xd-test.xpertdigital.co:8080/services",
    isDebug: true,
    isMFApp: true,
    appKey: "a4d4683392ab9f75290c234817828aa0",
    appSecret: "dd93c600498c6a019987b6aa202f2bbb",
    serviceUrl: "http://xd-test.xd-test.xpertdigital.co:8080/authService/100000002/appconfig",
    svcDoc: {
        "identity_meta": {
            "DbxKeyCloakLogin": {
                "success_url": "allow_any"
            }
        },
        "app_version": "1.0",
        "baseId": "7323db5b-a6f0-45f8-9379-149560ebb8c2",
        "app_default_version": "1.0",
        "login": [{
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "KonyBankingAdminConsoleAPIIdentityService",
            "type": "basic",
            "prov": "KonyBankingAdminConsoleAPIIdentityService",
            "url": "http://xd-test.xd-test.xpertdigital.co:8080/authService/100000002"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "KonyBankingAdminConsoleIdentityService",
            "type": "basic",
            "prov": "KonyBankingAdminConsoleIdentityService",
            "url": "http://xd-test.xd-test.xpertdigital.co:8080/authService/100000002"
        }, {
            "provider_type": "oauth2",
            "enable_identity_pkce": false,
            "alias": "DbxKeyCloakLogin",
            "type": "oauth2",
            "prov": "DbxKeyCloakLogin",
            "url": "http://xd-test.xd-test.xpertdigital.co:8080/authService/100000002"
        }],
        "services_meta": {
            "DecisionManagement": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/DecisionManagement",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/DecisionManagement"
            },
            "MFAObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/MFAObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/MFAObjService"
            },
            "CardManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CardManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CardManagementObjService"
            },
            "Feature": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/Feature"
            },
            "SystemManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/SystemManagement"
            },
            "CustomerServiceManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerServiceManagement"
            },
            "ApplicantManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ApplicantManagement"
            },
            "CustomerAndCustomerGroup": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerAndCustomerGroup"
            },
            "ReportsServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ReportsServices"
            },
            "AnalyticsServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AnalyticsServices"
            },
            "BankProductManagment": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/BankProductManagment",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/BankProductManagment"
            },
            "ViewFabricReports": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ViewFabricReports"
            },
            "ProductManagementOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagementOrchService"
            },
            "T24AlertSubscription": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/T24AlertSubscription"
            },
            "CampaignManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CampaignManagement"
            },
            "ContractManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ContractManagement"
            },
            "LocationDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationDetails"
            },
            "BrowserManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/BrowserManagement"
            },
            "InternalusersObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/InternalusersObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/InternalusersObjService"
            },
            "ProductManagementFromMS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagementFromMS"
            },
            "CommonGetDataService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CommonGetDataService"
            },
            "RolesAndPermissionsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/RolesAndPermissionsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/RolesAndPermissionsObjService"
            },
            "BusinessBankingObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/BusinessBankingObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/BusinessBankingObjService"
            },
            "DBPServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DBPServices"
            },
            "ReportService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ReportService"
            },
            "DBPAuthService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DBPAuthService"
            },
            "CustomerGroupsAndEntitlObjSvc": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CustomerGroupsAndEntitlObjSvc",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CustomerGroupsAndEntitlObjSvc"
            },
            "DueDiligencePartyMS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DueDiligencePartyMS"
            },
            "AuditLogsObjSvc": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/AuditLogsObjSvc",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/AuditLogsObjSvc"
            },
            "CardManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CardManagement"
            },
            "KeyCloakOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakOrchService"
            },
            "BrowserManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/BrowserManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/BrowserManagementObjService"
            },
            "AuthKMSService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AuthKMSService"
            },
            "StaticContentManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/StaticContentManagement"
            },
            "FabricReportsOrch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/FabricReportsOrch"
            },
            "ServerManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/ServerManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/ServerManagementObjService"
            },
            "ApplicationConfigurations": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ApplicationConfigurations"
            },
            "DueDiligenceMS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DueDiligenceMS"
            },
            "CRUDLayer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CRUDLayer"
            },
            "CustServiceObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CustServiceObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CustServiceObjService"
            },
            "SpotlightDataStorageAPIs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/SpotlightDataStorageAPIs"
            },
            "CustomerManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CustomerManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CustomerManagementObjService"
            },
            "FeatureObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/FeatureObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/FeatureObjService"
            },
            "AccountRequests": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AccountRequests"
            },
            "C360_DependencyLibraries": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360_DependencyLibraries"
            },
            "MasterDataObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/MasterDataObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/MasterDataObjService"
            },
            "BusinessConfigObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/BusinessConfigObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/BusinessConfigObjService"
            },
            "AddressValidation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AddressValidation"
            },
            "LeadManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LeadManagement"
            },
            "DMSUserCreationObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/DMSUserCreationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/DMSUserCreationObjService"
            },
            "LocationManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationManagement"
            },
            "ContractManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/ContractManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/ContractManagementObjService"
            },
            "CustomReports": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CustomReports",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CustomReports"
            },
            "SecurityImagesManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/SecurityImagesManagement"
            },
            "FetchKeycloakUsersOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/FetchKeycloakUsersOrchService"
            },
            "CampaignManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/CampaignManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/CampaignManagementObjService"
            },
            "EMailService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/EMailService"
            },
            "CustomIdentity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomIdentity"
            },
            "CustomerEntitlements": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerEntitlements"
            },
            "UserManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/UserManagement"
            },
            "CSRManagementOrig": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CSRManagementOrig"
            },
            "FeaturesAndActions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/FeaturesAndActions"
            },
            "EmailObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/EmailObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/EmailObjService"
            },
            "TestReportSvc": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/TestReportSvc"
            },
            "AppConfigurationsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/AppConfigurationsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/AppConfigurationsObjService"
            },
            "LeadAndApplicant": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/LeadAndApplicant",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/LeadAndApplicant"
            },
            "RolesAndPermissionManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/RolesAndPermissionManagement"
            },
            "APICustomIdentity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/APICustomIdentity"
            },
            "CampaignManagementMS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CampaignManagementMS"
            },
            "KeyCloakObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/KeyCloakObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/KeyCloakObjService"
            },
            "Campaign_Segment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/Campaign_Segment"
            },
            "OLBServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/OLBServices"
            },
            "MFAConfigAndScenarios": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/MFAConfigAndScenarios"
            },
            "ConfigurationService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ConfigurationService"
            },
            "AlertManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AlertManagement"
            },
            "BusinessConfiguration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/BusinessConfiguration"
            },
            "SecurityOpsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/SecurityOpsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/SecurityOpsObjService"
            },
            "dbxdbObjects": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/dbxdbObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/dbxdbObjects"
            },
            "KeyCloakIntegration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakIntegration"
            },
            "IdentityManagementOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/IdentityManagementOrchService"
            },
            "UserAuthentication": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/UserAuthentication"
            },
            "ConfigurationObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/ConfigurationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/ConfigurationObjService"
            },
            "EmailKMSJavaService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/EmailKMSJavaService"
            },
            "ManageLimitsAndFees": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/ManageLimitsAndFees",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/ManageLimitsAndFees"
            },
            "TransactionAndAuditLogs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/TransactionAndAuditLogs"
            },
            "AlertOrchestration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AlertOrchestration"
            },
            "IdentityManagementObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/IdentityManagementObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/IdentityManagementObjService"
            },
            "IdentityManagementService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/IdentityManagementService"
            },
            "DashboardObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/DashboardObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/DashboardObjService"
            },
            "KeyCloakAdminServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakAdminServices"
            },
            "DecisionRuleManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DecisionRuleManagement"
            },
            "AlertAndAlertTypes": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/AlertAndAlertTypes",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/AlertAndAlertTypes"
            },
            "BusinessBanking": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/BusinessBanking"
            },
            "AccountRequestsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/AccountRequestsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/AccountRequestsObjService"
            },
            "C360Identity": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360Identity"
            },
            "CustomerManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerManagement"
            },
            "CustomerRequest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerRequest"
            },
            "LocationObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/LocationObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/LocationObjService"
            },
            "DMSUserCreation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/DMSUserCreation"
            },
            "StaticContentObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/StaticContentObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/StaticContentObjService"
            },
            "CustomerGroupsAndEntitlements": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerGroupsAndEntitlements"
            },
            "LimitsAndFeesIntegration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LimitsAndFeesIntegration"
            },
            "KeyCloakCustomerAttributes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakCustomerAttributes"
            },
            "EmailOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/EmailOrchService"
            },
            "ValidationServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ValidationServices"
            },
            "CustomerRole": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerRole"
            },
            "TermsAndConditions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/TermsAndConditions"
            },
            "SecurityQuestionsManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/SecurityQuestionsManagement"
            },
            "EligibilityCriteria": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/EligibilityCriteria"
            },
            "AuditLogs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/AuditLogs"
            },
            "LocationsUsingCSVService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationsUsingCSVService"
            },
            "C360IdentityOrchService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360IdentityOrchService"
            },
            "ReportsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/ReportsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/ReportsObjService"
            },
            "LogServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/LogServices"
            },
            "TermsAndConditionsObjService": {
                "offline": false,
                "metadata_url": "http://xd-test.xd-test.xpertdigital.co:8080/services/metadata/v1/TermsAndConditionsObjService",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/data/v1/TermsAndConditionsObjService"
            },
            "ProductManagamentMS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagamentMS"
            },
            "ProductManagement": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagement"
            }
        },
        "selflink": "http://xd-test.xd-test.xpertdigital.co:8080/authService/100000002/appconfig",
        "integsvc": {
            "TestReportSvc": "http://xd-test.xd-test.xpertdigital.co:8080/services/TestReportSvc",
            "RolesAndPermissionManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/RolesAndPermissionManagement",
            "APICustomIdentity": "http://xd-test.xd-test.xpertdigital.co:8080/services/APICustomIdentity",
            "Feature": "http://xd-test.xd-test.xpertdigital.co:8080/services/Feature",
            "SystemManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/SystemManagement",
            "CampaignManagementMS": "http://xd-test.xd-test.xpertdigital.co:8080/services/CampaignManagementMS",
            "CustomerServiceManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerServiceManagement",
            "ApplicantManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/ApplicantManagement",
            "CustomerAndCustomerGroup": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerAndCustomerGroup",
            "ReportsServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/ReportsServices",
            "Campaign_Segment": "http://xd-test.xd-test.xpertdigital.co:8080/services/Campaign_Segment",
            "AnalyticsServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/AnalyticsServices",
            "OLBServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/OLBServices",
            "MFAConfigAndScenarios": "http://xd-test.xd-test.xpertdigital.co:8080/services/MFAConfigAndScenarios",
            "ViewFabricReports": "http://xd-test.xd-test.xpertdigital.co:8080/services/ViewFabricReports",
            "ConfigurationService": "http://xd-test.xd-test.xpertdigital.co:8080/services/ConfigurationService",
            "ProductManagementOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagementOrchService",
            "T24AlertSubscription": "http://xd-test.xd-test.xpertdigital.co:8080/services/T24AlertSubscription",
            "CampaignManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/CampaignManagement",
            "ContractManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/ContractManagement",
            "AlertManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/AlertManagement",
            "BusinessConfiguration": "http://xd-test.xd-test.xpertdigital.co:8080/services/BusinessConfiguration",
            "LocationDetails": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationDetails",
            "KeyCloakIntegration": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakIntegration",
            "BrowserManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/BrowserManagement",
            "IdentityManagementOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/IdentityManagementOrchService",
            "ProductManagementFromMS": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagementFromMS",
            "CommonGetDataService": "http://xd-test.xd-test.xpertdigital.co:8080/services/CommonGetDataService",
            "UserAuthentication": "http://xd-test.xd-test.xpertdigital.co:8080/services/UserAuthentication",
            "EmailKMSJavaService": "http://xd-test.xd-test.xpertdigital.co:8080/services/EmailKMSJavaService",
            "TransactionAndAuditLogs": "http://xd-test.xd-test.xpertdigital.co:8080/services/TransactionAndAuditLogs",
            "DBPServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/DBPServices",
            "_internal_logout": "http://xd-test.xd-test.xpertdigital.co:8080/services/IST",
            "ReportService": "http://xd-test.xd-test.xpertdigital.co:8080/services/ReportService",
            "AlertOrchestration": "http://xd-test.xd-test.xpertdigital.co:8080/services/AlertOrchestration",
            "DBPAuthService": "http://xd-test.xd-test.xpertdigital.co:8080/services/DBPAuthService",
            "DueDiligencePartyMS": "http://xd-test.xd-test.xpertdigital.co:8080/services/DueDiligencePartyMS",
            "IdentityManagementService": "http://xd-test.xd-test.xpertdigital.co:8080/services/IdentityManagementService",
            "CardManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/CardManagement",
            "KeyCloakOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakOrchService",
            "AuthKMSService": "http://xd-test.xd-test.xpertdigital.co:8080/services/AuthKMSService",
            "StaticContentManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/StaticContentManagement",
            "KeyCloakAdminServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakAdminServices",
            "DecisionRuleManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/DecisionRuleManagement",
            "FabricReportsOrch": "http://xd-test.xd-test.xpertdigital.co:8080/services/FabricReportsOrch",
            "BusinessBanking": "http://xd-test.xd-test.xpertdigital.co:8080/services/BusinessBanking",
            "C360Identity": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360Identity",
            "CustomerManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerManagement",
            "ApplicationConfigurations": "http://xd-test.xd-test.xpertdigital.co:8080/services/ApplicationConfigurations",
            "DueDiligenceMS": "http://xd-test.xd-test.xpertdigital.co:8080/services/DueDiligenceMS",
            "CustomerRequest": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerRequest",
            "CRUDLayer": "http://xd-test.xd-test.xpertdigital.co:8080/services/CRUDLayer",
            "DMSUserCreation": "http://xd-test.xd-test.xpertdigital.co:8080/services/DMSUserCreation",
            "SpotlightDataStorageAPIs": "http://xd-test.xd-test.xpertdigital.co:8080/services/SpotlightDataStorageAPIs",
            "CustomerGroupsAndEntitlements": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerGroupsAndEntitlements",
            "LimitsAndFeesIntegration": "http://xd-test.xd-test.xpertdigital.co:8080/services/LimitsAndFeesIntegration",
            "KeyCloakCustomerAttributes": "http://xd-test.xd-test.xpertdigital.co:8080/services/KeyCloakCustomerAttributes",
            "EmailOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/EmailOrchService",
            "AccountRequests": "http://xd-test.xd-test.xpertdigital.co:8080/services/AccountRequests",
            "ValidationServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/ValidationServices",
            "C360_DependencyLibraries": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360_DependencyLibraries",
            "CustomerRole": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerRole",
            "TermsAndConditions": "http://xd-test.xd-test.xpertdigital.co:8080/services/TermsAndConditions",
            "AddressValidation": "http://xd-test.xd-test.xpertdigital.co:8080/services/AddressValidation",
            "SecurityQuestionsManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/SecurityQuestionsManagement",
            "EligibilityCriteria": "http://xd-test.xd-test.xpertdigital.co:8080/services/EligibilityCriteria",
            "AuditLogs": "http://xd-test.xd-test.xpertdigital.co:8080/services/AuditLogs",
            "LeadManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/LeadManagement",
            "LocationsUsingCSVService": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationsUsingCSVService",
            "LocationManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/LocationManagement",
            "C360IdentityOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/C360IdentityOrchService",
            "SecurityImagesManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/SecurityImagesManagement",
            "FetchKeycloakUsersOrchService": "http://xd-test.xd-test.xpertdigital.co:8080/services/FetchKeycloakUsersOrchService",
            "EMailService": "http://xd-test.xd-test.xpertdigital.co:8080/services/EMailService",
            "CustomIdentity": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomIdentity",
            "CustomerEntitlements": "http://xd-test.xd-test.xpertdigital.co:8080/services/CustomerEntitlements",
            "LogServices": "http://xd-test.xd-test.xpertdigital.co:8080/services/LogServices",
            "UserManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/UserManagement",
            "CSRManagementOrig": "http://xd-test.xd-test.xpertdigital.co:8080/services/CSRManagementOrig",
            "ProductManagamentMS": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagamentMS",
            "ProductManagement": "http://xd-test.xd-test.xpertdigital.co:8080/services/ProductManagement",
            "FeaturesAndActions": "http://xd-test.xd-test.xpertdigital.co:8080/services/FeaturesAndActions"
        },
        "service_doc_etag": "0000017E737576A8",
        "appId": "8cca6ee0-147d-460d-95f6-d6250a917329",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "KonyBankingAdminConsole",
        "reportingsvc": {
            "session": "http://xd-test.xd-test.xpertdigital.co:8080/services/IST",
            "custom": "http://xd-test.xd-test.xpertdigital.co:8080/services/CMS"
        },
        "Webapp": {
            "url": "http://xd-test.xd-test.xpertdigital.co:8080/apps/Spotlight"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        isMVC: true,
        buttonAsLabel: true,
        APILevel: 7300,
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    requirejs.config({
        baseUrl: kony.appinit.getStaticContentPath() + 'desktopweb/appjs'
    });
    require(['kvmodules'], function() {
        applicationController = require("applicationController");
        kony.application.setApplicationInitializationEvents({
            init: applicationController.appInit,
            preappinit: applicationController.AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e,
            deeplink: applicationController.AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598,
            postappinit: applicationController.postAppInitCallBack,
            showstartupform: function() {
                var startupModule = kony.mvc.MDAApplication.getSharedInstance().moduleManager.getModule("AuthModule");
                startupModule.presentationController.presentUserInterface("frmLogin");
            }
        });
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    kony.theme.packagedthemes(["default"]);
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    spaAPM && spaAPM.startTracking();
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //This is the entry point for the application.When Locale comes,Local API call will be the entry point.
    kony.i18n.setDefaultLocaleAsync("en_US", onSuccess, onFailure, null);
};
									function getSPARequireModulesList(){ return ['kvmodules']; }
								