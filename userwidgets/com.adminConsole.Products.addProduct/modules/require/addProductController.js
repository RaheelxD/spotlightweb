define(['./AddProductDAO'],function(AddProductDAO) {
  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this.AddProductDAO = new AddProductDAO();
      this.imageValidationRegex = /\b(https|ftp):(\/\/|\\\\)[^\s]+\.(png|jpg|gif|jpeg|svg|bmp)\b$/i;
      this.currentScreen = 1;
      this.allFeatureType_NameGroupMap = new Map();
      this.maxScreenNumNavAllowed = 1;
      this.productLinesMasterData = [];
      this.productGroupsMasterData = [];
      this.featureTypeMasterData = [];
      this.sortColName = "";
      this.selRowFeatureData = {};
      this.prevIndex=-1;
      this.isFeatureEdit = false;
      this.isEditFlow = false;
      this.areProductLinesFetched = false;
      this.editDataGlobal = {};
      this.isScreenValidatedArr = [false,false,false,false,false];
      this.isConfig2 = true;
      // Value Assigned from Component Properties
      this.productPurposes = [];
      this.optionDisplayTypeMasterdata = [];
      this.imageTypesMasterdata = [];
      // Property Values for Backend Service Details
      this._objectServiceName1="";
      this._objectName1="";
      this._operationName1="";
      this._objectServiceName2="";
      this._objectName2="";
      this._operationName2="";
      this._objectServiceName3="";
      this._objectName3="";
      this._operationName3="";
      this._objectServiceName4="";
      this._objectName4="";
      this._operationName4="";
      this._objectServiceName5="";
      this._objectName5="";
      this._operationName5="";
      // Property Values for Design Time Configs
      this._productPurposes="";
      this._optionDisplayType="";
      this._imageTypes="";
      this._maxAddAttrAllowed="";
      // Property Values for Product Details Screen Text Fields
      this._productLineLblTxt="";
      this._productLineDropdownPlaceholderTxt="";
      this._productLineErrorMsgTxt="";
      this._productGroupLblTxt="";
      this._productGroupDropdownPlaceholderTxt="";
      this._productGroupErrorMsgTxt="";
      this._productNameLblTxt="";
      this._productNamePlaceholderTxt="";
      this._productNameErrorMsgTxt="";
      this._productRefLblTxt="";
      this._productRefPlaceholderTxt="";
      this._productRefErrorMsgTxt="";
      this._startDateLblTxt="";
      this._startDateErrorMsgTxt="";
      this._endDateLblTxt="";
      this._endDateErrorMsgTxt="";
      this._purposeLblTxt="";
      this._purposeDropdownPlaceholderTxt="";
      this._purposeErrorMsgTxt="";
      // Property Values for Additional Fields - Product Details Screen
      this._label1Txt="";
      this._placeholder1Txt="";      
      this._label2Txt="";
      this._placeholder2Txt="";
      this._label3Txt="";
      this._placeholder3Txt="";
      this._label4Txt="";
      this._placeholder4Txt="";
      this._label5Txt="";
      this._placeholder5Txt="";
      this._label6Txt="";
      this._placeholder6Txt="";      
      this._label7Txt="";
      this._placeholder7Txt="";
      this._label8Txt="";
      this._placeholder8Txt="";
      this._label9Txt="";
      this._placeholder9Txt="";      
      this._label10Txt="";
      this._placeholder10Txt="";      
      this._label11Txt="";
      this._placeholder11Txt="";
      this._label12Txt="";
      this._placeholder12Txt="";
      this._label13Txt="";
      this._placeholder13Txt="";
      this._label14Txt="";
      this._placeholder14Txt="";
      this._label15Txt="";
      this._placeholder15Txt="";
      // Property Values for Product Description Screen Text Fields
      this._descLblTxt="";
      this._descPlaceholderTxt="";
      this._detailedDescLblTxt="";
      this._detailedDescPlaceholderTxt="";
      this._notesLblTxt="";
      this._notesPlaceholderTxt="";
      this._notesToolTipTxt="";
      this._disclosureLblTxt="";
      this._disclosurePlaceholderTxt="";
      this._tandCLblTxt="";
      this._tandCPlaceholderTxt="";
      // Property Values for Product Features Screen Text Fields
      this._featureHeadingLblTxt="";
      this._addFeatureBtnTxt="";
      this._noFeatureDescTxt="";
      this._featureNameLblSegTxt="";
      this._featureGroupLblSegTxt="";
      this._featureTypeLblSegTxt="";
      // Property Values for Popup Add Product Features Screen Text Fields
      this._featureAddPopupHeadingLblTxt="";
      this._featureTypeLblTxt="";
      this._featureTypeDropdownPlaceholderTxt="";
      this._featureTypeErrorMsgTxt="";
      this._featureNameLblTxt="";
      this._featureNamePlaceholderTxt="";
      this._featureNameErrorMsgTxt="";
      this._featureGroupLblTxt="";
      this._featureGroupPlaceholderTxt="";
      this._featureGroupErrorMsgTxt="";
      this._featureDescLblTxt="";
      this._featureDescPlaceholderTxt="";
      this._sequenceNumLblTxt="";
      this._sequenceNumPlaceholderTxt="";
      this._sequenceNumErrorMsgTxt="";
      this._mandatoryLblTxt="";
      this._optionDetailsSubHeadingLblTxt="";
      this._optionDisplayTypeLblTxt="";
      this._optionDisplayPlaceholderTxt="";
      this._optionDisplayTypeErrorMsgTxt="";
      this._valueLblTxt="";
      this._optionDescLblTxt="";
      this._optionDefaultValueLblTxt="";
      this._optionValuePlaceholderTxt="";
      this._optionDefaultValueErrorMsgTxt="";
      this._optionDescPlaceholderTxt="";
      this._addValueBtnTxt="";
      this._featureCancelBtnTxt="";
      this._featureSaveBtnTxt="";
      this._featureUpdateBtnTxt="";
      // Property Values for Popup View Product Features Screen Text Fields
      this._featureViewEditBtnTxt="";
      this._featureViewProductGroupLblTxt="";
      this._featureViewSequenceNumberLblTxt="";
      this._featureViewMandatoryLblTxt="";
      this._featureViewDefaultValueLblTxt="";
      this._featureViewDescValueLblTxt="";
      this._featureViewOptionDetailsLblTxt="";
      this._featureViewOptionDisplayTypeLblTxt="";
      this._featureViewValueLblTxt="";
      this._featureViewDescriptionLblTxt="";
      // Property Values for Image Details Screen Text Fields
      this._imageTypeLblTxt="";
      this._imageTypeDropdownPlaceholderTxt="";
      this._imageURLLblTxt="";
      this._imageURLPlaceholderTxt="";
      this._addImageBtnTxt="";
      // Property Values for Additioanl Attributes Screen Text Fields
      this._attributeKeyLblTxt="";
      this._attributeKeyPlaceholderTxt="";
      this._attributeValueLblTxt="";
      this._attributeValuePlaceholderTxt="";
      this._addAttributeBtnTxt="";
      // Property Values for Navigation Bar Text Fields
      this._option1LblTxt="";
      this._subOption1LblTxt="";
      this._subOption2LblTxt="";
      this._option2LblTxt="";
      this._option3LblTxt="";
      this._option4LblTxt="";
      this._optionalFieldTxt="";
      this._cancelBtnTxt="";
      this._nextBtnTxt="";
      this._addProductBtnTxt="";
      this._updateProductBtnTxt="";
      // Property Values for Icon Texts
      this._deleteIcon="";
      this._editIcon="";
      this._selCheckboxIcon="";
      this._unselCheckboxIcon="";
      this._tooltipInfoIcon="";
      this._errorIcon="";
      this._sortUpDownIcon="";
      this._downArrowIcon="";
      this._upArrowIcon="";
      this._segMoreDetailsIcon="";
      this._menuRightArrowIcon="";
      // Property Values for Navigation Bar Skins
      this._leftVBarBgSkin="";
      this._leftVBarBtnUnselectedSkin="";
      this._leftVBarBtnSelectedSkin="";
      this._leftVBarBtnHoverSkin="";
      this._leftVBarOptionalLblSkin="";
      this._leftVBarSeparatorSkin="";
      this._leftVBarCollapseIconSkin="";
      this._leftVBarRightArrowIconSkin="";
      this._bottomBarBgSkin="";
      this._bottomBarCancelBtnSkin="";
      this._bottomBarCancelBtnHoverSkin="";
      this._bottomBarBtnLightSkin="";
      this._bottomBarBtnDarkSkin="";
      // Property Values for Common Skins
      this._commonLabelSkin="";
      this._tbxNormalSkin="";
      this._tbxErrorSkin="";
      this._tbxDisabledSkin="";
      this._tbxAreaNormalSkin="";
      this._tbxAreaErrorSkin="";
      this._lstboxNormalSkin="";
      this._lstboxErrorSkin="";
      this._lstboxDisabledSkin="";
      this._calendarNormalSkin="";
      this._calendarErrorSkin="";
      this._commonRightBackgroundSkin="";
      this._errorMsgIconSkin="";
      this._errorMsgTextSkin="";
      this._txtCounterLabelSkin="";
      this._segmentOptionsIconSkin="";
      this._popupCloseCrossBtnSkin="";
      this._AddSegmentRowBtnSkin="";
      this._sortconsSkin="";
      this._deleteIconSkin="";
      this._checkboxSelIconSkin="";
      this._checkboxUnselIconSkin="";
      // Property Values for Product Details Screen Skins
      this._prodPurposeSkin="";
      this._prodPurposeDropdownIConSkin="";
      this._dateDisabledFlexSkin="";
      this._dateDisabledIconLblSkin="";
      this._dateDisabledLblTxtSkin="";
      // Property Values for Product Features Screen Skins
      this._screenHeaderLblSkin="";
      this._addFeatureBtnSkin="";
      this._noFeatureScreenDescSkin="";
      this._segHeaderLblTxtSkin="";
      // Property Values for Popup Common Skins
      this._popupCmnBgSkin="";
      this._popupCmnTopBarSkin="";
      this._popupCmnHeadingLblSkin="";
      // Property Values for Product Add Features Popup Skins
      this._optionDetailsSegBgSkin="";      
      this._bottomBarBgSkin="";
      // Property Values for Product View Features Popup Skins
      this._lblKeySkin="";
      // Property Values for Product Additional Attributes Skins
      this._addAttrTextBoxSkin="";
      this._externalIndicator="";
    },

    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
      defineSetter(this, "objectServiceName1", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectServiceName1=val;
        }
      });
      defineGetter(this, "objectServiceName1", function(){
        return this._objectServiceName1;
      });
      defineSetter(this, "objectName1", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectName1=val;
        }
      });
      defineGetter(this, "objectName1", function(){
        return this._objectName1;
      });
      defineSetter(this, "operationName1", function(val){
        if((typeof val=="string") && (val != "")){
          this._operationName1=val;
        }
      });
      defineGetter(this, "operationName1", function(){
        return this._operationName1;
      });
      defineSetter(this, "objectServiceName2", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectServiceName2=val;
        }
      });
      defineGetter(this, "objectServiceName2", function(){
        return this._objectServiceName2;
      });
      defineSetter(this, "objectName2", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectName2=val;
        }
      });
      defineGetter(this, "objectName2", function(){
        return this._objectName2;
      });
      defineSetter(this, "operationName2", function(val){
        if((typeof val=="string") && (val != "")){
          this._operationName2=val;
        }
      });
      defineGetter(this, "operationName2", function(){
        return this._operationName2;
      });
      defineSetter(this, "objectServiceName3", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectServiceName3=val;
        }
      });
      defineGetter(this, "objectServiceName3", function(){
        return this._objectServiceName3;
      });
      defineSetter(this, "objectName3", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectName3=val;
        }
      });
      defineGetter(this, "objectName3", function(){
        return this._objectName3;
      });
      defineSetter(this, "operationName3", function(val){
        if((typeof val=="string") && (val != "")){
          this._operationName3=val;
        }
      });
      defineGetter(this, "operationName3", function(){
        return this._operationName3;
      });
      defineSetter(this, "objectServiceName4", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectServiceName4=val;
        }
      });
      defineGetter(this, "objectServiceName4", function(){
        return this._objectServiceName4;
      });
      defineSetter(this, "objectName4", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectName4=val;
        }
      });
      defineGetter(this, "objectName4", function(){
        return this._objectName4;
      });
      defineSetter(this, "operationName4", function(val){
        if((typeof val=="string") && (val != "")){
          this._operationName4=val;
        }
      });
      defineGetter(this, "operationName4", function(){
        return this._operationName4;
      });
      defineSetter(this, "objectServiceName5", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectServiceName5=val;
        }
      });
      defineGetter(this, "objectServiceName5", function(){
        return this._objectServiceName5;
      });
      defineSetter(this, "objectName5", function(val){
        if((typeof val=="string") && (val != "")){
          this._objectName5=val;
        }
      });
      defineGetter(this, "objectName5", function(){
        return this._objectName5;
      });
      defineSetter(this, "operationName5", function(val){
        if((typeof val=="string") && (val != "")){
          this._operationName5=val;
        }
      });
      defineGetter(this, "operationName5", function(){
        return this._operationName5;
      });
      defineSetter(this, "productPurposes", function(val){
        if((typeof val=="string") && (val != "")){
          this._productPurposes=val;
        }
      });
      defineGetter(this, "productPurposes", function(){
        return this._productPurposes;
      });
      defineSetter(this, "optionDisplayType", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDisplayType=val;
        }
      });
      defineGetter(this, "optionDisplayType", function(){
        return this._optionDisplayType;
      });
      defineSetter(this, "imageTypes", function(val){
        if((typeof val=="string") && (val != "")){
          this._imageTypes=val;
        }
      });
      defineGetter(this, "imageTypes", function(){
        return this._imageTypes;
      });
      defineSetter(this, "maxAddAttrAllowed", function(val){
        if((typeof val=="string") && (val != "")){
          this._maxAddAttrAllowed=val;
        }
      });
      defineGetter(this, "maxAddAttrAllowed", function(){
        return this._maxAddAttrAllowed;
      });
      defineSetter(this, "productLineLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productLineLblTxt=val;
        }
      });
      defineGetter(this, "productLineLblTxt", function(){
        return this._productLineLblTxt;
      });
      defineSetter(this, "productLineDropdownPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productLineDropdownPlaceholderTxt=val;
        }
      });
      defineGetter(this, "productLineDropdownPlaceholderTxt", function(){
        return this._productLineDropdownPlaceholderTxt;
      });
      defineSetter(this, "productLineErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productLineErrorMsgTxt=val;
        }
      });
      defineGetter(this, "productLineErrorMsgTxt", function(){
        return this._productLineErrorMsgTxt;
      });
      defineSetter(this, "productGroupLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productGroupLblTxt=val;
        }
      });
      defineGetter(this, "productGroupLblTxt", function(){
        return this._productGroupLblTxt;
      });
      defineSetter(this, "productGroupDropdownPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productGroupDropdownPlaceholderTxt=val;
        }
      });
      defineGetter(this, "productGroupDropdownPlaceholderTxt", function(){
        return this._productGroupDropdownPlaceholderTxt;
      });
      defineSetter(this, "productGroupErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productGroupErrorMsgTxt=val;
        }
      });
      defineGetter(this, "productGroupErrorMsgTxt", function(){
        return this._productGroupErrorMsgTxt;
      });
      defineSetter(this, "productNameLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productNameLblTxt=val;
        }
      });
      defineGetter(this, "productNameLblTxt", function(){
        return this._productNameLblTxt;
      });
      defineSetter(this, "productNamePlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productNamePlaceholderTxt=val;
        }
      });
      defineGetter(this, "productNamePlaceholderTxt", function(){
        return this._productNamePlaceholderTxt;
      });
      defineSetter(this, "productNameErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productNameErrorMsgTxt=val;
        }
      });
      defineGetter(this, "productNameErrorMsgTxt", function(){
        return this._productNameErrorMsgTxt;
      });
      defineSetter(this, "productRefLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productRefLblTxt=val;
        }
      });
      defineGetter(this, "productRefLblTxt", function(){
        return this._productRefLblTxt;
      });
      defineSetter(this, "productRefPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productRefPlaceholderTxt=val;
        }
      });
      defineGetter(this, "productRefPlaceholderTxt", function(){
        return this._productRefPlaceholderTxt;
      });
      defineSetter(this, "productRefErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._productRefErrorMsgTxt=val;
        }
      });
      defineGetter(this, "productRefErrorMsgTxt", function(){
        return this._productRefErrorMsgTxt;
      });
      defineSetter(this, "startDateLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._startDateLblTxt=val;
        }
      });
      defineGetter(this, "startDateLblTxt", function(){
        return this._startDateLblTxt;
      });
      defineSetter(this, "startDateErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._startDateErrorMsgTxt=val;
        }
      });
      defineGetter(this, "startDateErrorMsgTxt", function(){
        return this._startDateErrorMsgTxt;
      });
      defineSetter(this, "endDateLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._endDateLblTxt=val;
        }
      });
      defineGetter(this, "endDateLblTxt", function(){
        return this._endDateLblTxt;
      });
      defineSetter(this, "endDateErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._endDateErrorMsgTxt=val;
        }
      });
      defineGetter(this, "endDateErrorMsgTxt", function(){
        return this._endDateErrorMsgTxt;
      });
      defineSetter(this, "purposeLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._purposeLblTxt=val;
        }
      });
      defineGetter(this, "purposeLblTxt", function(){
        return this._purposeLblTxt;
      });
      defineSetter(this, "purposeDropdownPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._purposeDropdownPlaceholderTxt=val;
        }
      });
      defineGetter(this, "purposeDropdownPlaceholderTxt", function(){
        return this._purposeDropdownPlaceholderTxt;
      });
      defineSetter(this, "purposeErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._purposeErrorMsgTxt=val;
        }
      });
      defineGetter(this, "purposeErrorMsgTxt", function(){
        return this._purposeErrorMsgTxt;
      });
      defineSetter(this, "label1Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label1Txt=val;
        }
      });
      defineGetter(this, "label1Txt", function(){
        return this._label1Txt;
      });
      defineSetter(this, "placeholder1Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder1Txt=val;
        }
      });
      defineGetter(this, "placeholder1Txt", function(){
        return this._placeholder1Txt;
      });
      defineSetter(this, "label2Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label2Txt=val;
        }
      });
      defineGetter(this, "label2Txt", function(){
        return this._label2Txt;
      });
      defineSetter(this, "placeholder2Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder2Txt=val;
        }
      });
      defineGetter(this, "placeholder2Txt", function(){
        return this._placeholder2Txt;
      });
      defineSetter(this, "label3Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label3Txt=val;
        }
      });
      defineGetter(this, "label3Txt", function(){
        return this._label3Txt;
      });
      defineSetter(this, "placeholder3Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder3Txt=val;
        }
      });
      defineGetter(this, "placeholder3Txt", function(){
        return this._placeholder3Txt;
      });
      defineSetter(this, "label4Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label4Txt=val;
        }
      });
      defineGetter(this, "label4Txt", function(){
        return this._label4Txt;
      });
      defineSetter(this, "placeholder4Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder4Txt=val;
        }
      });
      defineGetter(this, "placeholder4Txt", function(){
        return this._placeholder4Txt;
      });
      defineSetter(this, "label5Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label5Txt=val;
        }
      });
      defineGetter(this, "label5Txt", function(){
        return this._label5Txt;
      });
      defineSetter(this, "placeholder5Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder5Txt=val;
        }
      });
      defineGetter(this, "placeholder5Txt", function(){
        return this._placeholder5Txt;
      });
      defineSetter(this, "label6Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label6Txt=val;
        }
      });
      defineGetter(this, "label6Txt", function(){
        return this._label6Txt;
      });
      defineSetter(this, "placeholder6Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder6Txt=val;
        }
      });
      defineGetter(this, "placeholder6Txt", function(){
        return this._placeholder6Txt;
      });
      defineSetter(this, "label7Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label7Txt=val;
        }
      });
      defineGetter(this, "label7Txt", function(){
        return this._label7Txt;
      });
      defineSetter(this, "placeholder7Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder7Txt=val;
        }
      });
      defineGetter(this, "placeholder7Txt", function(){
        return this._placeholder7Txt;
      });
      defineSetter(this, "label8Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label8Txt=val;
        }
      });
      defineGetter(this, "label8Txt", function(){
        return this._label8Txt;
      });
      defineSetter(this, "placeholder8Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder8Txt=val;
        }
      });
      defineGetter(this, "placeholder8Txt", function(){
        return this._placeholder8Txt;
      });
      defineSetter(this, "label9Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label9Txt=val;
        }
      });
      defineGetter(this, "label9Txt", function(){
        return this._label9Txt;
      });
      defineSetter(this, "placeholder9Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder9Txt=val;
        }
      });
      defineGetter(this, "placeholder9Txt", function(){
        return this._placeholder9Txt;
      });
      defineSetter(this, "label10Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label10Txt=val;
        }
      });
      defineGetter(this, "label10Txt", function(){
        return this._label10Txt;
      });
      defineSetter(this, "placeholder10Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder10Txt=val;
        }
      });
      defineGetter(this, "placeholder10Txt", function(){
        return this._placeholder10Txt;
      });
      defineSetter(this, "label11Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label11Txt=val;
        }
      });
      defineGetter(this, "label11Txt", function(){
        return this._label11Txt;
      });
      defineSetter(this, "placeholder11Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder11Txt=val;
        }
      });
      defineGetter(this, "placeholder11Txt", function(){
        return this._placeholder11Txt;
      });
      defineSetter(this, "label12Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label12Txt=val;
        }
      });
      defineGetter(this, "label12Txt", function(){
        return this._label12Txt;
      });
      defineSetter(this, "placeholder12Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder12Txt=val;
        }
      });
      defineGetter(this, "placeholder12Txt", function(){
        return this._placeholder12Txt;
      });
      defineSetter(this, "label13Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label13Txt=val;
        }
      });
      defineGetter(this, "label13Txt", function(){
        return this._label13Txt;
      });
      defineSetter(this, "placeholder13Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder13Txt=val;
        }
      });
      defineGetter(this, "placeholder13Txt", function(){
        return this._placeholder13Txt;
      });
      defineSetter(this, "label14Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label14Txt=val;
        }
      });
      defineGetter(this, "label14Txt", function(){
        return this._label14Txt;
      });
      defineSetter(this, "placeholder14Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder14Txt=val;
        }
      });
      defineGetter(this, "placeholder14Txt", function(){
        return this._placeholder14Txt;
      });
      defineSetter(this, "label15Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._label15Txt=val;
        }
      });
      defineGetter(this, "label15Txt", function(){
        return this._label15Txt;
      });
      defineSetter(this, "placeholder15Txt", function(val){
        if((typeof val=="string") && (val != "")){
          this._placeholder15Txt=val;
        }
      });
      defineGetter(this, "placeholder15Txt", function(){
        return this._placeholder15Txt;
      });
      defineSetter(this, "descLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._descLblTxt=val;
        }
      });
      defineGetter(this, "descLblTxt", function(){
        return this._descLblTxt;
      });
      defineSetter(this, "descPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._descPlaceholderTxt=val;
        }
      });
      defineGetter(this, "descPlaceholderTxt", function(){
        return this._descPlaceholderTxt;
      });
      defineSetter(this, "detailedDescLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._detailedDescLblTxt=val;
        }
      });
      defineGetter(this, "detailedDescLblTxt", function(){
        return this._detailedDescLblTxt;
      });
      defineSetter(this, "detailedDescPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._detailedDescPlaceholderTxt=val;
        }
      });
      defineGetter(this, "detailedDescPlaceholderTxt", function(){
        return this._detailedDescPlaceholderTxt;
      });
      defineSetter(this, "notesLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._notesLblTxt=val;
        }
      });
      defineGetter(this, "notesLblTxt", function(){
        return this._notesLblTxt;
      });
      defineSetter(this, "notesPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._notesPlaceholderTxt=val;
        }
      });
      defineGetter(this, "notesPlaceholderTxt", function(){
        return this._notesPlaceholderTxt;
      });
      defineSetter(this, "notesToolTipTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._notesToolTipTxt=val;
        }
      });
      defineGetter(this, "notesToolTipTxt", function(){
        return this._notesToolTipTxt;
      });
      defineSetter(this, "disclosureLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._disclosureLblTxt=val;
        }
      });
      defineGetter(this, "disclosureLblTxt", function(){
        return this._disclosureLblTxt;
      });
      defineSetter(this, "disclosurePlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._disclosurePlaceholderTxt=val;
        }
      });
      defineGetter(this, "disclosurePlaceholderTxt", function(){
        return this._disclosurePlaceholderTxt;
      });
      defineSetter(this, "tandCLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._tandCLblTxt=val;
        }
      });
      defineGetter(this, "tandCLblTxt", function(){
        return this._tandCLblTxt;
      });
      defineSetter(this, "tandCPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._tandCPlaceholderTxt=val;
        }
      });
      defineGetter(this, "tandCPlaceholderTxt", function(){
        return this._tandCPlaceholderTxt;
      });
      defineSetter(this, "featureHeadingLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureHeadingLblTxt=val;
        }
      });
      defineGetter(this, "featureHeadingLblTxt", function(){
        return this._featureHeadingLblTxt;
      });
      defineSetter(this, "addFeatureBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._addFeatureBtnTxt=val;
        }
      });
      defineGetter(this, "addFeatureBtnTxt", function(){
        return this._addFeatureBtnTxt;
      });
      defineSetter(this, "noFeatureDescTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._noFeatureDescTxt=val;
        }
      });
      defineGetter(this, "noFeatureDescTxt", function(){
        return this._noFeatureDescTxt;
      });
      defineSetter(this, "featureNameLblSegTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureNameLblSegTxt=val;
        }
      });
      defineGetter(this, "featureNameLblSegTxt", function(){
        return this._featureNameLblSegTxt;
      });
      defineSetter(this, "featureGroupLblSegTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureGroupLblSegTxt=val;
        }
      });
      defineGetter(this, "featureGroupLblSegTxt", function(){
        return this._featureGroupLblSegTxt;
      });
      defineSetter(this, "featureTypeLblSegTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureTypeLblSegTxt=val;
        }
      });
      defineGetter(this, "featureTypeLblSegTxt", function(){
        return this._featureTypeLblSegTxt;
      });
      defineSetter(this, "featureAddPopupHeadingLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureAddPopupHeadingLblTxt=val;
        }
      });
      defineGetter(this, "featureAddPopupHeadingLblTxt", function(){
        return this._featureAddPopupHeadingLblTxt;
      });
      defineSetter(this, "featureTypeLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureTypeLblTxt=val;
        }
      });
      defineGetter(this, "featureTypeLblTxt", function(){
        return this._featureTypeLblTxt;
      });
      defineSetter(this, "featureTypeDropdownPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureTypeDropdownPlaceholderTxt=val;
        }
      });
      defineGetter(this, "featureTypeDropdownPlaceholderTxt", function(){
        return this._featureTypeDropdownPlaceholderTxt;
      });
      defineSetter(this, "featureTypeErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureTypeErrorMsgTxt=val;
        }
      });
      defineGetter(this, "featureTypeErrorMsgTxt", function(){
        return this._featureTypeErrorMsgTxt;
      });
      defineSetter(this, "featureNameLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureNameLblTxt=val;
        }
      });
      defineGetter(this, "featureNameLblTxt", function(){
        return this._featureNameLblTxt;
      });
      defineSetter(this, "featureNamePlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureNamePlaceholderTxt=val;
        }
      });
      defineGetter(this, "featureNamePlaceholderTxt", function(){
        return this._featureNamePlaceholderTxt;
      });
      defineSetter(this, "featureNameErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureNameErrorMsgTxt=val;
        }
      });
      defineGetter(this, "featureNameErrorMsgTxt", function(){
        return this._featureNameErrorMsgTxt;
      });
      defineSetter(this, "featureGroupLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureGroupLblTxt=val;
        }
      });
      defineGetter(this, "featureGroupLblTxt", function(){
        return this._featureGroupLblTxt;
      });
      defineSetter(this, "featureGroupPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureGroupPlaceholderTxt=val;
        }
      });
      defineGetter(this, "featureGroupPlaceholderTxt", function(){
        return this._featureGroupPlaceholderTxt;
      });
      defineSetter(this, "featureGroupErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureGroupErrorMsgTxt=val;
        }
      });
      defineGetter(this, "featureGroupErrorMsgTxt", function(){
        return this._featureGroupErrorMsgTxt;
      });
      defineSetter(this, "featureDescLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureDescLblTxt=val;
        }
      });
      defineGetter(this, "featureDescLblTxt", function(){
        return this._featureDescLblTxt;
      });
      defineSetter(this, "featureDescPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureDescPlaceholderTxt=val;
        }
      });
      defineGetter(this, "featureDescPlaceholderTxt", function(){
        return this._featureDescPlaceholderTxt;
      });
      defineSetter(this, "sequenceNumLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._sequenceNumLblTxt=val;
        }
      });
      defineGetter(this, "sequenceNumLblTxt", function(){
        return this._sequenceNumLblTxt;
      });
      defineSetter(this, "sequenceNumPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._sequenceNumPlaceholderTxt=val;
        }
      });
      defineGetter(this, "sequenceNumPlaceholderTxt", function(){
        return this._sequenceNumPlaceholderTxt;
      });
      defineSetter(this, "sequenceNumErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._sequenceNumErrorMsgTxt=val;
        }
      });
      defineGetter(this, "sequenceNumErrorMsgTxt", function(){
        return this._sequenceNumErrorMsgTxt;
      });
      defineSetter(this, "mandatoryLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._mandatoryLblTxt=val;
        }
      });
      defineGetter(this, "mandatoryLblTxt", function(){
        return this._mandatoryLblTxt;
      });
      defineSetter(this, "optionDetailsSubHeadingLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDetailsSubHeadingLblTxt=val;
        }
      });
      defineGetter(this, "optionDetailsSubHeadingLblTxt", function(){
        return this._optionDetailsSubHeadingLblTxt;
      });
      defineSetter(this, "optionDisplayTypeLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDisplayTypeLblTxt=val;
        }
      });
      defineGetter(this, "optionDisplayTypeLblTxt", function(){
        return this._optionDisplayTypeLblTxt;
      });
      defineSetter(this, "optionDisplayPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDisplayPlaceholderTxt=val;
        }
      });
      defineGetter(this, "optionDisplayPlaceholderTxt", function(){
        return this._optionDisplayPlaceholderTxt;
      });
      defineSetter(this, "optionDisplayTypeErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDisplayTypeErrorMsgTxt=val;
        }
      });
      defineGetter(this, "optionDisplayTypeErrorMsgTxt", function(){
        return this._optionDisplayTypeErrorMsgTxt;
      });
      defineSetter(this, "valueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._valueLblTxt=val;
        }
      });
      defineGetter(this, "valueLblTxt", function(){
        return this._valueLblTxt;
      });
      defineSetter(this, "optionDescLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDescLblTxt=val;
        }
      });
      defineGetter(this, "optionDescLblTxt", function(){
        return this._optionDescLblTxt;
      });
      defineSetter(this, "optionDefaultValueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDefaultValueLblTxt=val;
        }
      });
      defineGetter(this, "optionDefaultValueLblTxt", function(){
        return this._optionDefaultValueLblTxt;
      });
      defineSetter(this, "optionValuePlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionValuePlaceholderTxt=val;
        }
      });
      defineGetter(this, "optionValuePlaceholderTxt", function(){
        return this._optionValuePlaceholderTxt;
      });
      defineSetter(this, "optionDefaultValueErrorMsgTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDefaultValueErrorMsgTxt=val;
        }
      });
      defineGetter(this, "optionDefaultValueErrorMsgTxt", function(){
        return this._optionDefaultValueErrorMsgTxt;
      });
      defineSetter(this, "optionDescPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDescPlaceholderTxt=val;
        }
      });
      defineGetter(this, "optionDescPlaceholderTxt", function(){
        return this._optionDescPlaceholderTxt;
      });
      defineSetter(this, "addValueBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._addValueBtnTxt=val;
        }
      });
      defineGetter(this, "addValueBtnTxt", function(){
        return this._addValueBtnTxt;
      });
      defineSetter(this, "featureCancelBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureCancelBtnTxt=val;
        }
      });
      defineGetter(this, "featureCancelBtnTxt", function(){
        return this._featureCancelBtnTxt;
      });
      defineSetter(this, "featureSaveBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureSaveBtnTxt=val;
        }
      });
      defineGetter(this, "featureSaveBtnTxt", function(){
        return this._featureSaveBtnTxt;
      });
      defineSetter(this, "featureUpdateBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureUpdateBtnTxt=val;
        }
      });
      defineGetter(this, "featureUpdateBtnTxt", function(){
        return this._featureUpdateBtnTxt;
      });
      defineSetter(this, "featureViewEditBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewEditBtnTxt=val;
        }
      });
      defineGetter(this, "featureViewEditBtnTxt", function(){
        return this._featureViewEditBtnTxt;
      });
      defineSetter(this, "featureViewProductGroupLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewProductGroupLblTxt=val;
        }
      });
      defineGetter(this, "featureViewProductGroupLblTxt", function(){
        return this._featureViewProductGroupLblTxt;
      });
      defineSetter(this, "featureViewSequenceNumberLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewSequenceNumberLblTxt=val;
        }
      });
      defineGetter(this, "featureViewSequenceNumberLblTxt", function(){
        return this._featureViewSequenceNumberLblTxt;
      });
      defineSetter(this, "featureViewMandatoryLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewMandatoryLblTxt=val;
        }
      });
      defineGetter(this, "featureViewMandatoryLblTxt", function(){
        return this._featureViewMandatoryLblTxt;
      });
      defineSetter(this, "featureViewDefaultValueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewDefaultValueLblTxt=val;
        }
      });
      defineGetter(this, "featureViewDefaultValueLblTxt", function(){
        return this._featureViewDefaultValueLblTxt;
      });
      defineSetter(this, "featureViewDescValueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewDescValueLblTxt=val;
        }
      });
      defineGetter(this, "featureViewDescValueLblTxt", function(){
        return this._featureViewDescValueLblTxt;
      });
      defineSetter(this, "featureViewOptionDetailsLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewOptionDetailsLblTxt=val;
        }
      });
      defineGetter(this, "featureViewOptionDetailsLblTxt", function(){
        return this._featureViewOptionDetailsLblTxt;
      });
      defineSetter(this, "featureViewOptionDisplayTypeLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewOptionDisplayTypeLblTxt=val;
        }
      });
      defineGetter(this, "featureViewOptionDisplayTypeLblTxt", function(){
        return this._featureViewOptionDisplayTypeLblTxt;
      });
      defineSetter(this, "featureViewValueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewValueLblTxt=val;
        }
      });
      defineGetter(this, "featureViewValueLblTxt", function(){
        return this._featureViewValueLblTxt;
      });
      defineSetter(this, "featureViewDescriptionLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._featureViewDescriptionLblTxt=val;
        }
      });
      defineGetter(this, "featureViewDescriptionLblTxt", function(){
        return this._featureViewDescriptionLblTxt;
      });
      defineSetter(this, "imageTypeLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._imageTypeLblTxt=val;
        }
      });
      defineGetter(this, "imageTypeLblTxt", function(){
        return this._imageTypeLblTxt;
      });
      defineSetter(this, "imageTypeDropdownPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._imageTypeDropdownPlaceholderTxt=val;
        }
      });
      defineGetter(this, "imageTypeDropdownPlaceholderTxt", function(){
        return this._imageTypeDropdownPlaceholderTxt;
      });
      defineSetter(this, "imageURLLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._imageURLLblTxt=val;
        }
      });
      defineGetter(this, "imageURLLblTxt", function(){
        return this._imageURLLblTxt;
      });
      defineSetter(this, "imageURLPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._imageURLPlaceholderTxt=val;
        }
      });
      defineGetter(this, "imageURLPlaceholderTxt", function(){
        return this._imageURLPlaceholderTxt;
      });
      defineSetter(this, "addImageBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._addImageBtnTxt=val;
        }
      });
      defineGetter(this, "addImageBtnTxt", function(){
        return this._addImageBtnTxt;
      });
      defineSetter(this, "attributeKeyLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._attributeKeyLblTxt=val;
        }
      });
      defineGetter(this, "attributeKeyLblTxt", function(){
        return this._attributeKeyLblTxt;
      });
      defineSetter(this, "attributeKeyPlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._attributeKeyPlaceholderTxt=val;
        }
      });
      defineGetter(this, "attributeKeyPlaceholderTxt", function(){
        return this._attributeKeyPlaceholderTxt;
      });
      defineSetter(this, "attributeValueLblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._attributeValueLblTxt=val;
        }
      });
      defineGetter(this, "attributeValueLblTxt", function(){
        return this._attributeValueLblTxt;
      });
      defineSetter(this, "attributeValuePlaceholderTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._attributeValuePlaceholderTxt=val;
        }
      });
      defineGetter(this, "attributeValuePlaceholderTxt", function(){
        return this._attributeValuePlaceholderTxt;
      });
      defineSetter(this, "addAttributeBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._addAttributeBtnTxt=val;
        }
      });
      defineGetter(this, "addAttributeBtnTxt", function(){
        return this._addAttributeBtnTxt;
      });
      defineSetter(this, "option1LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._option1LblTxt=val;
        }
      });
      defineGetter(this, "option1LblTxt", function(){
        return this._option1LblTxt;
      });
      defineSetter(this, "subOption1LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._subOption1LblTxt=val;
        }
      });
      defineGetter(this, "subOption1LblTxt", function(){
        return this._subOption1LblTxt;
      });
      defineSetter(this, "subOption2LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._subOption2LblTxt=val;
        }
      });
      defineGetter(this, "subOption2LblTxt", function(){
        return this._subOption2LblTxt;
      });
      defineSetter(this, "option2LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._option2LblTxt=val;
        }
      });
      defineGetter(this, "option2LblTxt", function(){
        return this._option2LblTxt;
      });
      defineSetter(this, "option3LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._option3LblTxt=val;
        }
      });
      defineGetter(this, "option3LblTxt", function(){
        return this._option3LblTxt;
      });
      defineSetter(this, "option4LblTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._option4LblTxt=val;
        }
      });
      defineGetter(this, "option4LblTxt", function(){
        return this._option4LblTxt;
      });
      defineSetter(this, "optionalFieldTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionalFieldTxt=val;
        }
      });
      defineGetter(this, "optionalFieldTxt", function(){
        return this._optionalFieldTxt;
      });
      defineSetter(this, "cancelBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._cancelBtnTxt=val;
        }
      });
      defineGetter(this, "cancelBtnTxt", function(){
        return this._cancelBtnTxt;
      });
      defineSetter(this, "nextBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._nextBtnTxt=val;
        }
      });
      defineGetter(this, "nextBtnTxt", function(){
        return this._nextBtnTxt;
      });
      defineSetter(this, "addProductBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._addProductBtnTxt=val;
        }
      });
      defineGetter(this, "addProductBtnTxt", function(){
        return this._addProductBtnTxt;
      });
      defineSetter(this, "updateProductBtnTxt", function(val){
        if((typeof val=="string") && (val != "")){
          this._updateProductBtnTxt=val;
        }
      });
      defineGetter(this, "updateProductBtnTxt", function(){
        return this._updateProductBtnTxt;
      });
      defineSetter(this, "deleteIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._deleteIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "deleteIcon", function(){
        return this._deleteIcon;
      });
      defineSetter(this, "editIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._editIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "editIcon", function(){
        return this._editIcon;
      });
      defineSetter(this, "selCheckboxIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._selCheckboxIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "selCheckboxIcon", function(){
        return this._selCheckboxIcon;
      });
      defineSetter(this, "unselCheckboxIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._unselCheckboxIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "unselCheckboxIcon", function(){
        return this._unselCheckboxIcon;
      });
      defineSetter(this, "tooltipInfoIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._tooltipInfoIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "tooltipInfoIcon", function(){
        return this._tooltipInfoIcon;
      });
      defineSetter(this, "errorIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._errorIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "errorIcon", function(){
        return this._errorIcon;
      });
      defineSetter(this, "sortUpDownIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._sortUpDownIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "sortUpDownIcon", function(){
        return this._sortUpDownIcon;
      });
      defineSetter(this, "downArrowIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._downArrowIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "downArrowIcon", function(){
        return this._downArrowIcon;
      });
      defineSetter(this, "upArrowIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._upArrowIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "upArrowIcon", function(){
        return this._upArrowIcon;
      });
      defineSetter(this, "segMoreDetailsIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._segMoreDetailsIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "segMoreDetailsIcon", function(){
        return this._segMoreDetailsIcon;
      });
      defineSetter(this, "menuRightArrowIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._menuRightArrowIcon=JSON.parse(val);
        }
      });
      defineGetter(this, "menuRightArrowIcon", function(){
        return this._menuRightArrowIcon;
      });
      defineSetter(this, "leftVBarBgSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarBgSkin=val;
        }
      });
      defineGetter(this, "leftVBarBgSkin", function(){
        return this._leftVBarBgSkin;
      });
      defineSetter(this, "leftVBarBtnUnselectedSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarBtnUnselectedSkin=val;
        }
      });
      defineGetter(this, "leftVBarBtnUnselectedSkin", function(){
        return this._leftVBarBtnUnselectedSkin;
      });
      defineSetter(this, "leftVBarBtnSelectedSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarBtnSelectedSkin=val;
        }
      });
      defineGetter(this, "leftVBarBtnSelectedSkin", function(){
        return this._leftVBarBtnSelectedSkin;
      });
      defineSetter(this, "leftVBarBtnHoverSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarBtnHoverSkin=val;
        }
      });
      defineGetter(this, "leftVBarBtnHoverSkin", function(){
        return this._leftVBarBtnHoverSkin;
      });
      defineSetter(this, "leftVBarOptionalLblSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarOptionalLblSkin=val;
        }
      });
      defineGetter(this, "leftVBarOptionalLblSkin", function(){
        return this._leftVBarOptionalLblSkin;
      });
      defineSetter(this, "leftVBarSeparatorSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarSeparatorSkin=val;
        }
      });
      defineGetter(this, "leftVBarSeparatorSkin", function(){
        return this._leftVBarSeparatorSkin;
      });
      defineSetter(this, "leftVBarCollapseIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarCollapseIconSkin=val;
        }
      });
      defineGetter(this, "leftVBarCollapseIconSkin", function(){
        return this._leftVBarCollapseIconSkin;
      });
      defineSetter(this, "leftVBarRightArrowIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._leftVBarRightArrowIconSkin=val;
        }
      });
      defineGetter(this, "leftVBarRightArrowIconSkin", function(){
        return this._leftVBarRightArrowIconSkin;
      });
      defineSetter(this, "bottomBarBgSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarBgSkin=val;
        }
      });
      defineGetter(this, "bottomBarBgSkin", function(){
        return this._bottomBarBgSkin;
      });
      defineSetter(this, "bottomBarCancelBtnSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarCancelBtnSkin=val;
        }
      });
      defineGetter(this, "bottomBarCancelBtnSkin", function(){
        return this._bottomBarCancelBtnSkin;
      });
      defineSetter(this, "bottomBarCancelBtnHoverSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarCancelBtnHoverSkin=val;
        }
      });
      defineGetter(this, "bottomBarCancelBtnHoverSkin", function(){
        return this._bottomBarCancelBtnHoverSkin;
      });
      defineSetter(this, "bottomBarBtnLightSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarBtnLightSkin=val;
        }
      });
      defineGetter(this, "bottomBarBtnLightSkin", function(){
        return this._bottomBarBtnLightSkin;
      });
      defineSetter(this, "bottomBarBtnDarkSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarBtnDarkSkin=val;
        }
      });
      defineGetter(this, "bottomBarBtnDarkSkin", function(){
        return this._bottomBarBtnDarkSkin;
      });
      defineSetter(this, "commonLabelSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._commonLabelSkin=val;
        }
      });
      defineGetter(this, "commonLabelSkin", function(){
        return this._commonLabelSkin;
      });
      defineSetter(this, "tbxNormalSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbxNormalSkin=val;
        }
      });
      defineGetter(this, "tbxNormalSkin", function(){
        return this._tbxNormalSkin;
      });
      defineSetter(this, "tbxErrorSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbxErrorSkin=val;
        }
      });
      defineGetter(this, "tbxErrorSkin", function(){
        return this._tbxErrorSkin;
      });
      defineSetter(this, "tbxDisabledSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbxDisabledSkin=val;
        }
      });
      defineGetter(this, "tbxDisabledSkin", function(){
        return this._tbxDisabledSkin;
      });
      defineSetter(this, "tbxAreaNormalSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbxAreaNormalSkin=val;
        }
      });
      defineGetter(this, "tbxAreaNormalSkin", function(){
        return this._tbxAreaNormalSkin;
      });
      defineSetter(this, "tbxAreaErrorSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbxAreaErrorSkin=val;
        }
      });
      defineGetter(this, "tbxAreaErrorSkin", function(){
        return this._tbxAreaErrorSkin;
      });
      defineSetter(this, "lstboxNormalSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._lstboxNormalSkin=val;
        }
      });
      defineGetter(this, "lstboxNormalSkin", function(){
        return this._lstboxNormalSkin;
      });
      defineSetter(this, "lstboxErrorSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._lstboxErrorSkin=val;
        }
      });
      defineGetter(this, "lstboxErrorSkin", function(){
        return this._lstboxErrorSkin;
      });
      defineSetter(this, "lstboxDisabledSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._lstboxDisabledSkin=val;
        }
      });
      defineGetter(this, "lstboxDisabledSkin", function(){
        return this._lstboxDisabledSkin;
      });
      defineSetter(this, "calendarNormalSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._calendarNormalSkin=val;
        }
      });
      defineGetter(this, "calendarNormalSkin", function(){
        return this._calendarNormalSkin;
      });
      defineSetter(this, "calendarErrorSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._calendarErrorSkin=val;
        }
      });
      defineGetter(this, "calendarErrorSkin", function(){
        return this._calendarErrorSkin;
      });
      defineSetter(this, "commonRightBackgroundSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._commonRightBackgroundSkin=val;
        }
      });
      defineGetter(this, "commonRightBackgroundSkin", function(){
        return this._commonRightBackgroundSkin;
      });
      defineSetter(this, "errorMsgIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._errorMsgIconSkin=val;
        }
      });
      defineGetter(this, "errorMsgIconSkin", function(){
        return this._errorMsgIconSkin;
      });
      defineSetter(this, "errorMsgTextSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._errorMsgTextSkin=val;
        }
      });
      defineGetter(this, "errorMsgTextSkin", function(){
        return this._errorMsgTextSkin;
      });
      defineSetter(this, "txtCounterLabelSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._txtCounterLabelSkin=val;
        }
      });
      defineGetter(this, "txtCounterLabelSkin", function(){
        return this._txtCounterLabelSkin;
      });
      defineSetter(this, "segmentOptionsIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._segmentOptionsIconSkin=val;
        }
      });
      defineGetter(this, "segmentOptionsIconSkin", function(){
        return this._segmentOptionsIconSkin;
      });
      defineSetter(this, "popupCloseCrossBtnSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCloseCrossBtnSkin=val;
        }
      });
      defineGetter(this, "popupCloseCrossBtnSkin", function(){
        return this._popupCloseCrossBtnSkin;
      });
      defineSetter(this, "AddSegmentRowBtnSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._AddSegmentRowBtnSkin=val;
        }
      });
      defineGetter(this, "AddSegmentRowBtnSkin", function(){
        return this._AddSegmentRowBtnSkin;
      });
      defineSetter(this, "sortconsSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._sortconsSkin=val;
        }
      });
      defineGetter(this, "sortconsSkin", function(){
        return this._sortconsSkin;
      });
      defineSetter(this, "deleteIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._deleteIconSkin=val;
        }
      });
      defineGetter(this, "deleteIconSkin", function(){
        return this._deleteIconSkin;
      });
      defineSetter(this, "checkboxSelIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._checkboxSelIconSkin=val;
        }
      });
      defineGetter(this, "checkboxSelIconSkin", function(){
        return this._checkboxSelIconSkin;
      });
      defineSetter(this, "checkboxUnselIconSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._checkboxUnselIconSkin=val;
        }
      });
      defineGetter(this, "checkboxUnselIconSkin", function(){
        return this._checkboxUnselIconSkin;
      });
      defineSetter(this, "prodPurposeSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._prodPurposeSkin=val;
        }
      });
      defineGetter(this, "prodPurposeSkin", function(){
        return this._prodPurposeSkin;
      });
      defineSetter(this, "prodPurposeDropdownIConSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._prodPurposeDropdownIConSkin=val;
        }
      });
      defineGetter(this, "prodPurposeDropdownIConSkin", function(){
        return this._prodPurposeDropdownIConSkin;
      });
      defineSetter(this, "dateDisabledFlexSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._dateDisabledFlexSkin=val;
        }
      });
      defineGetter(this, "dateDisabledFlexSkin", function(){
        return this._dateDisabledFlexSkin;
      });
      defineSetter(this, "dateDisabledIconLblSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._dateDisabledIconLblSkin=val;
        }
      });
      defineGetter(this, "dateDisabledIconLblSkin", function(){
        return this._dateDisabledIconLblSkin;
      });
      defineSetter(this, "dateDisabledLblTxtSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._dateDisabledLblTxtSkin=val;
        }
      });
      defineGetter(this, "dateDisabledLblTxtSkin", function(){
        return this._dateDisabledLblTxtSkin;
      });
      defineSetter(this, "screenHeaderLblSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._screenHeaderLblSkin=val;
        }
      });
      defineGetter(this, "screenHeaderLblSkin", function(){
        return this._screenHeaderLblSkin;
      });
      defineSetter(this, "addFeatureBtnSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._addFeatureBtnSkin=val;
        }
      });
      defineGetter(this, "addFeatureBtnSkin", function(){
        return this._addFeatureBtnSkin;
      });
      defineSetter(this, "noFeatureScreenDescSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._noFeatureScreenDescSkin=val;
        }
      });
      defineGetter(this, "noFeatureScreenDescSkin", function(){
        return this._noFeatureScreenDescSkin;
      });
      defineSetter(this, "segHeaderLblTxtSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._segHeaderLblTxtSkin=val;
        }
      });
      defineGetter(this, "segHeaderLblTxtSkin", function(){
        return this._segHeaderLblTxtSkin;
      });
      defineSetter(this, "popupCmnBgSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCmnBgSkin=val;
        }
      });
      defineGetter(this, "popupCmnBgSkin", function(){
        return this._popupCmnBgSkin;
      });
      defineSetter(this, "popupCmnTopBarSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCmnTopBarSkin=val;
        }
      });
      defineGetter(this, "popupCmnTopBarSkin", function(){
        return this._popupCmnTopBarSkin;
      });
      defineSetter(this, "popupCmnHeadingLblSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCmnHeadingLblSkin=val;
        }
      });
      defineGetter(this, "popupCmnHeadingLblSkin", function(){
        return this._popupCmnHeadingLblSkin;
      });
      defineSetter(this, "optionDetailsSegBgSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._optionDetailsSegBgSkin=val;
        }
      });
      defineGetter(this, "optionDetailsSegBgSkin", function(){
        return this._optionDetailsSegBgSkin;
      });      
      defineSetter(this, "bottomBarBgSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._bottomBarBgSkin=val;
        }
      });
      defineGetter(this, "bottomBarBgSkin", function(){
        return this._bottomBarBgSkin;
      });
      defineSetter(this, "lblKeySkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._lblKeySkin=val;
        }
      });
      defineGetter(this, "lblKeySkin", function(){
        return this._lblKeySkin;
      });
      defineSetter(this, "addAttrTextBoxSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._addAttrTextBoxSkin=val;
        }
      });
      defineGetter(this, "addAttrTextBoxSkin", function(){
        return this._addAttrTextBoxSkin;
      });
    },

    addProductPreShow: function(){
      this.setConfigChanges();
      this.setTextFromi18n();
      this.assignDefaultText();
      this.assignDefaultSkins();
      this.setDesginTimeConfigs();
      this.addCustomDateWidget();
      this.setFlowActions();
    },

    setConfigChanges:function(){
      const scopeObj = this;
      let configurationSvc = kony.sdk.getCurrentInstance().getConfigurationService();
      configurationSvc.getAllClientAppProperties(function(response) {
        if(response && response.MARKETING_CATALOG_CONFIG && response.MARKETING_CATALOG_CONFIG.toUpperCase()==="FALSE"){
          scopeObj.isConfig2 = false;
        } else {
          scopeObj.isConfig2 = true;
        }
      },function(){}); 
    },

    setTextFromi18n: function(){
      // Fetches i18n from Component Properties and saves to variable. In case i18n is not available uses Text Value given in Properties as it is.

      // Property Values for Product Details Screen Text Fields
      this._productLineLblTxt = this.getStringFromi18n(this._productLineLblTxt);
      this._productLineDropdownPlaceholderTxt = this.getStringFromi18n(this._productLineDropdownPlaceholderTxt);
      this._productLineErrorMsgTxt = this.getStringFromi18n(this._productLineErrorMsgTxt);
      this._productGroupLblTxt = this.getStringFromi18n(this._productGroupLblTxt);
      this._productGroupDropdownPlaceholderTxt = this.getStringFromi18n(this._productGroupDropdownPlaceholderTxt);
      this._productGroupErrorMsgTxt = this.getStringFromi18n(this._productGroupErrorMsgTxt);
      this._productNameLblTxt = this.getStringFromi18n(this._productNameLblTxt);
      this._productNamePlaceholderTxt = this.getStringFromi18n(this._productNamePlaceholderTxt);
      this._productNameErrorMsgTxt = this.getStringFromi18n(this._productNameErrorMsgTxt);
      this._productRefLblTxt = this.getStringFromi18n(this._productRefLblTxt);
      this._productRefPlaceholderTxt = this.getStringFromi18n(this._productRefPlaceholderTxt);
      this._productRefErrorMsgTxt = this.getStringFromi18n(this._productRefErrorMsgTxt);
      this._startDateLblTxt = this.getStringFromi18n(this._startDateLblTxt);
      this._startDateErrorMsgTxt = this.getStringFromi18n(this._startDateErrorMsgTxt);
      this._endDateLblTxt = this.getStringFromi18n(this._endDateLblTxt);
      this._endDateErrorMsgTxt = this.getStringFromi18n(this._endDateErrorMsgTxt);
      this._purposeLblTxt = this.getStringFromi18n(this._purposeLblTxt);
      this._purposeDropdownPlaceholderTxt = this.getStringFromi18n(this._purposeDropdownPlaceholderTxt);
      this._purposeErrorMsgTxt = this.getStringFromi18n(this._purposeErrorMsgTxt);
      // Property Values for Product Details Screen Additional Text Fields
      this._label1Txt = this.getStringFromi18n(this._label1Txt);
      this._placeholder1Txt = this.getStringFromi18n(this._placeholder1Txt);
      this._label2Txt = this.getStringFromi18n(this._label2Txt);
      this._placeholder2Txt = this.getStringFromi18n(this._placeholder2Txt);      
      this._label3Txt = this.getStringFromi18n(this._label3Txt);
      this._placeholder3Txt = this.getStringFromi18n(this._placeholder3Txt);
      this._label4Txt = this.getStringFromi18n(this._label4Txt);
      this._placeholder4Txt = this.getStringFromi18n(this._placeholder4Txt);
      this._label5Txt = this.getStringFromi18n(this._label5Txt);
      this._placeholder5Txt = this.getStringFromi18n(this._placeholder5Txt);
      this._label6Txt = this.getStringFromi18n(this._label6Txt);
      this._placeholder6Txt = this.getStringFromi18n(this._placeholder6Txt);
      this._label7Txt = this.getStringFromi18n(this._label7Txt);
      this._placeholder7Txt = this.getStringFromi18n(this._placeholder7Txt);
      this._label8Txt = this.getStringFromi18n(this._label8Txt);
      this._placeholder8Txt = this.getStringFromi18n(this._placeholder8Txt);
      this._label9Txt = this.getStringFromi18n(this._label9Txt);
      this._placeholder9Txt = this.getStringFromi18n(this._placeholder9Txt);
      this._label10Txt = this.getStringFromi18n(this._label10Txt);
      this._placeholder10Txt = this.getStringFromi18n(this._placeholder10Txt);
      this._label11Txt = this.getStringFromi18n(this._label11Txt);
      this._placeholder11Txt = this.getStringFromi18n(this._placeholder11Txt);
      this._label12Txt = this.getStringFromi18n(this._label12Txt);
      this._placeholder12Txt = this.getStringFromi18n(this._placeholder12Txt);      
      this._label13Txt = this.getStringFromi18n(this._label13Txt);
      this._placeholder13Txt = this.getStringFromi18n(this._placeholder13Txt);
      this._label14Txt = this.getStringFromi18n(this._label14Txt);
      this._placeholder14Txt = this.getStringFromi18n(this._placeholder14Txt);      
      this._label15Txt = this.getStringFromi18n(this._label15Txt);
      this._placeholder15Txt = this.getStringFromi18n(this._placeholder15Txt);
      // Property Values for Product Description Screen Text Fields
      this._descLblTxt = this.getStringFromi18n(this._descLblTxt);
      this._descPlaceholderTxt = this.getStringFromi18n(this._descPlaceholderTxt);
      this._detailedDescLblTxt = this.getStringFromi18n(this._detailedDescLblTxt);
      this._detailedDescPlaceholderTxt = this.getStringFromi18n(this._detailedDescPlaceholderTxt);
      this._notesLblTxt = this.getStringFromi18n(this._notesLblTxt);
      this._notesPlaceholderTxt = this.getStringFromi18n(this._notesPlaceholderTxt);
      this._notesToolTipTxt = this.getStringFromi18n(this._notesToolTipTxt);
      this._disclosureLblTxt = this.getStringFromi18n(this._disclosureLblTxt);
      this._disclosurePlaceholderTxt = this.getStringFromi18n(this._disclosurePlaceholderTxt);
      this._tandCLblTxt = this.getStringFromi18n(this._tandCLblTxt);
      this._tandCPlaceholderTxt = this.getStringFromi18n(this._tandCPlaceholderTxt);
      // Property Values for Product Features Screen Text Fields
      this._featureHeadingLblTxt = this.getStringFromi18n(this._featureHeadingLblTxt);
      this._addFeatureBtnTxt = this.getStringFromi18n(this._addFeatureBtnTxt);
      this._noFeatureDescTxt = this.getStringFromi18n(this._noFeatureDescTxt);
      this._featureNameLblSegTxt = this.getStringFromi18n(this._featureNameLblSegTxt);
      this._featureGroupLblSegTxt = this.getStringFromi18n(this._featureGroupLblSegTxt);
      this._featureTypeLblSegTxt = this.getStringFromi18n(this._featureTypeLblSegTxt);
      // Property Values for Popup Add Product Features Screen Text Fields
      this._featureAddPopupHeadingLblTxt = this.getStringFromi18n(this._featureAddPopupHeadingLblTxt);
      this._featureTypeLblTxt = this.getStringFromi18n(this._featureTypeLblTxt);
      this._featureTypeDropdownPlaceholderTxt = this.getStringFromi18n(this._featureTypeDropdownPlaceholderTxt);
      this._featureTypeErrorMsgTxt = this.getStringFromi18n(this._featureTypeErrorMsgTxt);
      this._featureNameLblTxt = this.getStringFromi18n(this._featureNameLblTxt);
      this._featureNamePlaceholderTxt = this.getStringFromi18n(this._featureNamePlaceholderTxt);
      this._featureNameErrorMsgTxt = this.getStringFromi18n(this._featureNameErrorMsgTxt);
      this._featureGroupLblTxt = this.getStringFromi18n(this._featureGroupLblTxt);
      this._featureGroupPlaceholderTxt = this.getStringFromi18n(this._featureGroupPlaceholderTxt);
      this._featureGroupErrorMsgTxt = this.getStringFromi18n(this._featureGroupErrorMsgTxt);
      this._featureDescLblTxt = this.getStringFromi18n(this._featureDescLblTxt);
      this._featureDescPlaceholderTxt = this.getStringFromi18n(this._featureDescPlaceholderTxt);
      this._sequenceNumLblTxt = this.getStringFromi18n(this._sequenceNumLblTxt);
      this._sequenceNumPlaceholderTxt = this.getStringFromi18n(this._sequenceNumPlaceholderTxt);
      this._sequenceNumErrorMsgTxt = this.getStringFromi18n(this._sequenceNumErrorMsgTxt);
      this._mandatoryLblTxt = this.getStringFromi18n(this._mandatoryLblTxt);
      this._optionDetailsSubHeadingLblTxt = this.getStringFromi18n(this._optionDetailsSubHeadingLblTxt);
      this._optionDisplayTypeLblTxt = this.getStringFromi18n(this._optionDisplayTypeLblTxt);
      this._optionDisplayPlaceholderTxt = this.getStringFromi18n(this._optionDisplayPlaceholderTxt);
      this._optionDisplayTypeErrorMsgTxt = this.getStringFromi18n(this._optionDisplayTypeErrorMsgTxt);
      this._valueLblTxt = this.getStringFromi18n(this._valueLblTxt);
      this._optionDescLblTxt = this.getStringFromi18n(this._optionDescLblTxt);
      this._optionDefaultValueLblTxt = this.getStringFromi18n(this._optionDefaultValueLblTxt);
      this._optionValuePlaceholderTxt = this.getStringFromi18n(this._optionValuePlaceholderTxt);
      this._optionDefaultValueErrorMsgTxt = this.getStringFromi18n(this._optionDefaultValueErrorMsgTxt);
      this._optionDescPlaceholderTxt = this.getStringFromi18n(this._optionDescPlaceholderTxt);
      this._addValueBtnTxt = this.getStringFromi18n(this._addValueBtnTxt);
      this._featureCancelBtnTxt = this.getStringFromi18n(this._featureCancelBtnTxt);
      this._featureSaveBtnTxt = this.getStringFromi18n(this._featureSaveBtnTxt);
      this._featureUpdateBtnTxt = this.getStringFromi18n(this._featureUpdateBtnTxt);
      // Property Values for Popup View Product Features Screen Text Fields
      this._featureViewEditBtnTxt = this.getStringFromi18n(this._featureViewEditBtnTxt);
      this._featureViewProductGroupLblTxt = this.getStringFromi18n(this._featureViewProductGroupLblTxt);
      this._featureViewSequenceNumberLblTxt = this.getStringFromi18n(this._featureViewSequenceNumberLblTxt);
      this._featureViewMandatoryLblTxt = this.getStringFromi18n(this._featureViewMandatoryLblTxt);
      this._featureViewDefaultValueLblTxt = this.getStringFromi18n(this._featureViewDefaultValueLblTxt);
      this._featureViewDescValueLblTxt = this.getStringFromi18n(this._featureViewDescValueLblTxt);
      this._featureViewOptionDetailsLblTxt = this.getStringFromi18n(this._featureViewOptionDetailsLblTxt);
      this._featureViewOptionDisplayTypeLblTxt = this.getStringFromi18n(this._featureViewOptionDisplayTypeLblTxt);
      this._featureViewValueLblTxt = this.getStringFromi18n(this._featureViewValueLblTxt);
      this._featureViewDescriptionLblTxt = this.getStringFromi18n(this._featureViewDescriptionLblTxt);
      // Property Values for Image Details Screen Text Fields
      this._imageTypeLblTxt = this.getStringFromi18n(this._imageTypeLblTxt);
      this._imageTypeDropdownPlaceholderTxt = this.getStringFromi18n(this._imageTypeDropdownPlaceholderTxt);
      this._imageURLLblTxt = this.getStringFromi18n(this._imageURLLblTxt);
      this._imageURLPlaceholderTxt = this.getStringFromi18n(this._imageURLPlaceholderTxt);
      this._addImageBtnTxt = this.getStringFromi18n(this._addImageBtnTxt);
      // Property Values for Additional Attributes Screen Text Fields
      this._attributeKeyLblTxt = this.getStringFromi18n(this._attributeKeyLblTxt);
      this._attributeKeyPlaceholderTxt = this.getStringFromi18n(this._attributeKeyPlaceholderTxt);
      this._attributeValueLblTxt = this.getStringFromi18n(this._attributeValueLblTxt);
      this._attributeValuePlaceholderTxt = this.getStringFromi18n(this._attributeValuePlaceholderTxt);
      this._addAttributeBtnTxt = this.getStringFromi18n(this._addAttributeBtnTxt);
      // Property Values for Navigation Bar Text Fields
      this._option1LblTxt = this.getStringFromi18n(this._option1LblTxt);
      this._subOption1LblTxt = this.getStringFromi18n(this._subOption1LblTxt);
      this._subOption2LblTxt = this.getStringFromi18n(this._subOption2LblTxt);
      this._option2LblTxt = this.getStringFromi18n(this._option2LblTxt);
      this._option3LblTxt = this.getStringFromi18n(this._option3LblTxt);
      this._option4LblTxt = this.getStringFromi18n(this._option4LblTxt);
      this._optionalFieldTxt = this.getStringFromi18n(this._optionalFieldTxt);
      this._cancelBtnTxt = this.getStringFromi18n(this._cancelBtnTxt);
      this._nextBtnTxt = this.getStringFromi18n(this._nextBtnTxt);
      this._addProductBtnTxt = this.getStringFromi18n(this._addProductBtnTxt);
      this._updateProductBtnTxt = this.getStringFromi18n(this._updateProductBtnTxt);
      // Property Values for Icon Texts
      this._deleteIcon = this._deleteIcon.text ? this._deleteIcon.text : this._deleteIcon;
      this._editIcon= this._editIcon.text ? this._editIcon.text : this._editIcon;
      this._selCheckboxIcon= this._selCheckboxIcon.text ? this._selCheckboxIcon.text : this._selCheckboxIcon;
      this._unselCheckboxIcon= this._unselCheckboxIcon.text ? this._unselCheckboxIcon.text : this._unselCheckboxIcon;
      this._tooltipInfoIcon= this._tooltipInfoIcon.text ? this._tooltipInfoIcon.text : this._tooltipInfoIcon;
      this._errorIcon= this._errorIcon.text ? this._errorIcon.text : this._errorIcon;
      this._sortUpDownIcon= this._sortUpDownIcon.text ? this._sortUpDownIcon.text : this._sortUpDownIcon;
      this._downArrowIcon= this._downArrowIcon.text ? this._downArrowIcon.text : this._downArrowIcon;
      this._upArrowIcon= this._upArrowIcon.text ? this._upArrowIcon.text : this._upArrowIcon;
      this._segMoreDetailsIcon= this._segMoreDetailsIcon.text ? this._segMoreDetailsIcon.text : this._segMoreDetailsIcon;
      this._menuRightArrowIcon= this._menuRightArrowIcon.text ? this._menuRightArrowIcon.text : this._menuRightArrowIcon;
    },

    getStringFromi18n: function(stringValue){
      return  kony.i18n.getLocalizedString(stringValue) ? kony.i18n.getLocalizedString(stringValue) : stringValue;
    },

    assignDefaultText: function(){
      // Assigning Property Values for Product Details Screen Text Fields
      this.view.lblProductLine.text = this._productLineLblTxt;
      this.view.lblErrorText1.text = this._productLineErrorMsgTxt;
      this.view.lblProductGroup.text = this._productGroupLblTxt;
      this.view.lblErrorText2.text = this._productGroupErrorMsgTxt;
      this.view.lblProductName.text = this._productNameLblTxt;
      this.view.tbxProductName.placeholder = this._productNamePlaceholderTxt;
      this.view.lblErrorText3.text = this._productNameErrorMsgTxt;
      this.view.lblProductReference.text = this._productRefLblTxt;
      this.view.tbxProductReference.placeholder = this._productRefPlaceholderTxt;
      this.view.lblErrorText4.text = this._productRefErrorMsgTxt;
      this.view.lblAvailableFromDate.text = this._startDateLblTxt;
      this.view.blAvailableToDate.text = this._endDateLblTxt;
      this.view.lblProductPurpose.text = this._purposeLblTxt;
      this.view.lblSelectedValue.text = this._purposeDropdownPlaceholderTxt;
      // Assigning Property Values for Product Details Screen Additional Text Fields
      for(let widgetNum=1; widgetNum<=15; widgetNum++){
        this.view["lblAdditionalField"+widgetNum].text = this["_label"+ widgetNum +"Txt"];
        this.view["tbxAdditionalField"+widgetNum].placeholder = this["_placeholder"+ widgetNum +"Txt"];
      }
      // Assigning Property Values for Product Description Screen Text Fields
      this.view.lblDescription.text = this._descLblTxt;
      this.view.txtAreaDescription.placeholder = this._descPlaceholderTxt;
      this.view.lblDetailedDescription.text = this._detailedDescLblTxt;
      this.view.txtAreaDetailedDescription.placeholder = this._detailedDescPlaceholderTxt;
      this.view.lblNotes.text = this._notesLblTxt;
      this.view.txtAreaNotes.placeholder = this._notesPlaceholderTxt;
      this.view.lblFontIconNotesInfo.text = this._tooltipInfoIcon;
      this.view.lblFontIconNotesInfo.toolTip = this._notesToolTipTxt;
      this.view.lblDisclosure.text = this._disclosureLblTxt;
      this.view.txtAreaDisclosure.placeholder = this._disclosurePlaceholderTxt;
      this.view.lblTermsAndConditions.text = this._tandCLblTxt;
      this.view.txtAreaTermsAndConditions.placeholder = this._tandCPlaceholderTxt;
      // Assigning Property Values for Product Features Screen Text Fields
      this.view.lblFeaturesHeader.text = this._featureHeadingLblTxt + "(0)";
      this.view.btnAddFeature1.text = this._addFeatureBtnTxt;
      this.view.lblFeaturesCanBeAdded.text = this._noFeatureDescTxt;
      this.view.btnAddFeature2.text = this._addFeatureBtnTxt;
      this.view.lblFeatureName.text = this._featureNameLblSegTxt;
      this.view.lblFeatureGroup.text = this._featureGroupLblSegTxt;
      this.view.lblType.text = this._featureTypeLblSegTxt;
      // Assigning Property Values for Popup Add Product Features Screen Text Fields
      this.view.lblAddFeatureHeading.text = this._featureAddPopupHeadingLblTxt;
      this.view.lblAddFeatureType.text = this._featureTypeLblTxt;
      this.view.lblErrorTextFeature1.text = this._featureTypeErrorMsgTxt;
      this.view.lblAddFeatureName.text = this._featureNameLblTxt;
      this.view.tbxAddFeatureName.placeholder = this._featureNamePlaceholderTxt;
      this.view.lblErrorTextFeature2.text = this._featureNameErrorMsgTxt;
      this.view.lblAddFeatureGroup.text = this._featureGroupLblTxt;
      this.view.tbxAddFeatureGroup.placeholder = this._featureGroupPlaceholderTxt;
      this.view.lblErrorTextFeature3.text = this._featureGroupErrorMsgTxt;
      this.view.lblAddFeatureDescription.text = this._featureDescLblTxt;
      this.view.txtAreaFeatureDescription.placeholder = this._featureDescPlaceholderTxt;
      this.view.lblAddFeatureSequenceNumber.text = this._sequenceNumLblTxt;
      this.view.tbxAddFeatureSequenceNumber.placeholder = this._sequenceNumPlaceholderTxt;
      this.view.lblMandatorySequenceNumber.text = this._mandatoryLblTxt;
      this.view.lblOptionDetails.text = this._optionDetailsSubHeadingLblTxt;
      this.view.lblOptionDisplayType.text = this._optionDisplayTypeLblTxt;
      this.view.lblErrorTextFeature4.text = this._optionDisplayTypeErrorMsgTxt;
      this.view.lblOptionValue.text = this._valueLblTxt;
      this.view.lblOptionDescription.text = this._optionDescLblTxt;
      this.view.btnAddValue.text = this._addValueBtnTxt;
      this.view.btnCancel.text = this._featureCancelBtnTxt;
      this.view.btnSaveAndClose.text = this._featureSaveBtnTxt;
      // Assigning Property Values for Popup View Product Features Screen Text Fields
      this.view.btnEditFeature.text = this._featureViewEditBtnTxt;
      this.view.lblFeatureGroupHeading.text = this._featureViewProductGroupLblTxt;
      this.view.lblSequenceNumHeading.text = this._featureViewSequenceNumberLblTxt;
      this.view.lblIsMandatoryHeading.text = this._featureViewMandatoryLblTxt;
      this.view.lblDefaultValueHeading.text = this._featureViewDefaultValueLblTxt;
      this.view.lblViewDescriptionHeading.text = this._featureViewDescValueLblTxt;
      this.view.lblViewFeatureOptions.text = this._featureViewOptionDetailsLblTxt;
      this.view.lblOptionDisplayTypeHeading.text = this._featureViewOptionDisplayTypeLblTxt;
      this.view.lblViewFeatureValHeading.text = this._featureViewValueLblTxt;
      this.view.lblViewFeatureDescHeading.text = this._featureViewDescriptionLblTxt;
      // Assigning Property Values for Image Details Screen Text Fields
      this.view.btnAddImage.text = this._addImageBtnTxt;
      // Assigning Property Values for Additional Attributes Screen Text Fields
      this.view.lblAttribute.text = this._attributeKeyLblTxt;
      this.view.lblAttributeValue.text = this._attributeValueLblTxt;
      this.view.btnAddAttribute.text = this._addAttributeBtnTxt;
      // Assigning Property Values for Navigation Bar Text Fields
      this.view.lblOption1.text = this._option1LblTxt;
      this.view.lblSubOption1.text = this._subOption1LblTxt;
      this.view.lblSubOption2.text = this._subOption2LblTxt;
      this.view.lblOption2.text = this._option2LblTxt;
      this.view.lblOption3.text = this._option3LblTxt;
      this.view.lblOption4.text = this._option4LblTxt;
      this.view.lblOptionalIndicator.text = this._optionalFieldTxt;
      this.view.btnCancelAddProduct.text = this._cancelBtnTxt;
      this.view.btnNext.text = this._nextBtnTxt;
      this.view.btnAddProduct.text = this._addProductBtnTxt;
      this.view.lblFontIconRightArrow1.text = this._menuRightArrowIcon;
      this.view.lblFontIconRightArrow2.text = this._menuRightArrowIcon;
      this.view.lblFontIconRightArrow3.text = this._menuRightArrowIcon;
      this.view.lblFontIconRightArrow4.text = this._menuRightArrowIcon;
      this.view.lblFontIconRightArrow5.text = this._menuRightArrowIcon;
    },

    assignDefaultSkins: function(){
      // Assigning Property Values for Navigation Bar Skins
      this.view.flxVerticalTabs.skin = this._leftVBarBgSkin;
      this.view.lblOption1.skin = this._leftVBarBtnSelectedSkin;
      this.view.lblSubOption1.skin = this._leftVBarBtnSelectedSkin;
      this.view.lblSubOption2.skin = this._leftVBarBtnUnselectedSkin;
      this.view.lblOption2.skin = this._leftVBarBtnUnselectedSkin;
      this.view.lblOption3.skin = this._leftVBarBtnUnselectedSkin;
      this.view.lblOption4.skin = this._leftVBarBtnUnselectedSkin;
      this.view.lblOptionalIndicator.skin = this._leftVBarOptionalLblSkin;
      this.view.flxTabSeparator1.skin = this._leftVBarSeparatorSkin;
      this.view.flxTabSeparator2.skin = this._leftVBarSeparatorSkin;
      this.view.flxTabSeparator3.skin = this._leftVBarSeparatorSkin;
      this.view.lblFontIconArrow.skin = this._leftVBarCollapseIconSkin;
      this.view.lblFontIconRightArrow1.skin = this._leftVBarRightArrowIconSkin;
      this.view.lblFontIconRightArrow2.skin = this._leftVBarRightArrowIconSkin;
      this.view.lblFontIconRightArrow3.skin = this._leftVBarRightArrowIconSkin;
      this.view.lblFontIconRightArrow4.skin = this._leftVBarRightArrowIconSkin;
      this.view.lblFontIconRightArrow5.skin = this._leftVBarRightArrowIconSkin;
      this.view.flxNavigationBarButtons.skin = this._bottomBarBgSkin;
      this.view.btnCancelAddProduct.skin = this._bottomBarCancelBtnSkin;
      this.view.btnCancelAddProduct.focusSkin = this._bottomBarCancelBtnSkin;
      this.view.btnCancelAddProduct.hoverSkin = this._bottomBarCancelBtnHoverSkin;
      this.view.btnNext.skin = this._bottomBarBtnLightSkin;
      this.view.btnNext.focusSkin = this._bottomBarBtnLightSkin;
      this.view.btnAddProduct.skin = this._bottomBarBtnDarkSkin;
      this.view.btnAddProduct.focusSkin  = this._bottomBarBtnDarkSkin;
      //Assigning Common Skins
      this.view.flxAddProductMain.skin = this._commonRightBackgroundSkin;
      // Assigning Property Values for Product Details Skins
      this.view.lblProductLine.skin = this._commonLabelSkin;
      this.view.lstboxProductLine.skin = this._lstboxNormalSkin;
      this.view.lblProductGroup.skin = this._commonLabelSkin;
      this.view.lstboxProductGroup.skin = this._lstboxNormalSkin;
      this.view.lblProductName.skin = this._commonLabelSkin;
      this.view.tbxProductName.skin = this._tbxNormalSkin;
      this.view.lblProductReference.skin = this._commonLabelSkin;
      this.view.tbxProductReference.skin = this._tbxNormalSkin;
      this.view.lblAvailableFromDate.skin = this._commonLabelSkin;
      this.view.flxShowStartDate.skin = this._dateDisabledFlexSkin;
      this.view.lblIconCalendar1.skin = this._dateDisabledIconLblSkin;
      this.view.lblStartDate.skin = this._dateDisabledLblTxtSkin;
      this.view.blAvailableToDate.skin = this._commonLabelSkin;
      this.view.flxShowEndDate.skin = this._dateDisabledFlexSkin;
      this.view.lblIconCalendar2.skin = this._dateDisabledIconLblSkin;
      this.view.lblEndDate.skin = this._dateDisabledLblTxtSkin;
      this.view.lblProductPurpose.skin = this._commonLabelSkin;
      this.view.lblSelectedValue.skin = this._commonLabelSkin;
      this.view.flxSelectedText.skin = this._prodPurposeSkin;
      this.view.lblIconDropdown.skin = this._prodPurposeDropdownIConSkin;
      this.view.lblErrorIcon1.skin = this._errorMsgIconSkin;
      this.view.lblErrorText1.skin = this._errorMsgTextSkin;
      for(let widgetNum=1; widgetNum<=7; widgetNum++){
        this.view["lblErrorIcon"+widgetNum].skin = this._errorMsgIconSkin;
        this.view["lblErrorText"+widgetNum].skin = this._errorMsgTextSkin;
      }
      for(let widgetNum=1; widgetNum<=15; widgetNum++){
        this.view["lblAdditionalField"+widgetNum].skin = this._commonLabelSkin;
        this.view["tbxAdditionalField"+widgetNum].skin = this._tbxNormalSkin;
      }
      // Assigning Property Values for Product Description Skins
      this.view.lblDescription.skin = this._commonLabelSkin;
      this.view.lblDescTextCounter.skin = this._txtCounterLabelSkin;
      this.view.txtAreaDescription.skin = this._tbxAreaNormalSkin;
      this.view.lblDetailedDescription.skin = this._commonLabelSkin;
      this.view.lblDetailedDescTextCounter.skin = this._txtCounterLabelSkin;
      this.view.txtAreaDetailedDescription.skin = this._tbxAreaNormalSkin;
      this.view.lblNotes.skin = this._commonLabelSkin;
      this.view.txtAreaNotes.skin = this._tbxAreaNormalSkin;
      this.view.lblDisclosure.skin = this._commonLabelSkin;
      this.view.txtAreaDisclosure.skin = this._tbxAreaNormalSkin;
      this.view.lblTermsAndConditions.skin = this._commonLabelSkin;
      this.view.txtAreaTermsAndConditions.skin = this._tbxAreaNormalSkin;
      // Assigning Property Values for Product Feature Skins
      this.view.lblFeaturesHeader.skin = this._screenHeaderLblSkin;
      this.view.btnAddFeature1.skin = this._addFeatureBtnSkin;
      this.view.lblFeaturesCanBeAdded.skin = this._noFeatureScreenDescSkin;
      this.view.btnAddFeature2.skin = this._addFeatureBtnSkin;
      this.view.lblFeatureName.skin = this._segHeaderLblTxtSkin;
      this.view.lblFeatureGroup.skin = this._segHeaderLblTxtSkin;
      this.view.lblType.skin = this._segHeaderLblTxtSkin;
      // Popup Common Skins
      this.view.flxAddProductPopups.skin = this._popupCmnBgSkin;
      // Add Feature Popup Skins
      this.view.flxAddFeatureHeader.skin = this._popupCmnTopBarSkin;
      this.view.lblCloseIcon.skin = this._popupCloseCrossBtnSkin;
      this.view.lblAddFeatureHeading.skin = this._popupCmnHeadingLblSkin;
      this.view.lblAddFeatureType.skin = this._commonLabelSkin;
      this.view.lstBoxAddFeatureType.skin = this._lstboxNormalSkin;
      this.view.lblErrorIconFeature1.skin = this._errorMsgIconSkin;
      this.view.lblErrorTextFeature1.skin = this._errorMsgTextSkin;
      this.view.lblAddFeatureName.skin = this._commonLabelSkin;
      this.view.tbxAddFeatureName.skin = this._tbxNormalSkin;
      this.view.lblErrorIconFeature2.skin = this._errorMsgIconSkin;
      this.view.lblErrorTextFeature2.skin = this._errorMsgTextSkin;
      this.view.lblAddFeatureGroup.skin = this._commonLabelSkin;
      this.view.tbxAddFeatureGroup.skin = this._tbxDisabledSkin;
      this.view.lblErrorIconFeature3.skin = this._errorMsgIconSkin;
      this.view.lblErrorTextFeature3.skin = this._errorMsgTextSkin;
      this.view.lblAddFeatureDescription.skin = this._commonLabelSkin;
      this.view.lblFeatureDescTextCounter.skin = this._txtCounterLabelSkin;
      this.view.txtAreaFeatureDescription.skin = this._tbxAreaNormalSkin;
      this.view.lblAddFeatureSequenceNumber.skin = this._commonLabelSkin;
      this.view.tbxAddFeatureSequenceNumber.skin = this._tbxNormalSkin;
      this.view.lblErrorIconFeature5.skin = this._errorMsgIconSkin;
      this.view.lblErrorTextFeature5.skin = this._errorMsgTextSkin;
      this.view.lblMandatorySequenceNumber.skin = this._commonLabelSkin;
      this.view.lblOptionDetails.skin = this._popupCmnHeadingLblSkin;
      this.view.lblOptionDisplayType.skin = this._commonLabelSkin;
      this.view.lstBoxOptionDisplayType.skin = this._lstboxNormalSkin;
      this.view.lblErrorIconFeature4.skin = this._errorMsgIconSkin;
      this.view.lblErrorTextFeature4.skin = this._errorMsgTextSkin;
      this.view.flxOptionContainer.skin = this._optionDetailsSegBgSkin;
      this.view.lblOptionValue.skin = this._commonLabelSkin;
      this.view.lblOptionDescription.skin = this._commonLabelSkin;
      this.view.btnAddValue.skin = this._AddSegmentRowBtnSkin;
      this.view.flxAddFeatureBottomBar.skin = this._bottomBarBgSkin;
      this.view.btnCancel.skin = this._bottomBarCancelBtnSkin;
      this.view.btnCancel.focusSkin = this._bottomBarCancelBtnSkin;
      this.view.btnCancel.hoverSkin = this._bottomBarCancelBtnHoverSkin;
      this.view.btnSaveAndClose.skin = this._bottomBarBtnDarkSkin;
      this.view.btnSaveAndClose.focusSkin = this._bottomBarBtnDarkSkin;
      // View Feature Popup Skins
      this.view.flxViewFeatureHeader.skin = this._popupCmnTopBarSkin;
      this.view.lblCloseIcon2.skin = this._popupCloseCrossBtnSkin;
      this.view.lblViewFeatureName.skin = this._popupCmnHeadingLblSkin;
      // this.view.btnEditFeature.skin = "sknbtnf7f7faLatoReg13px485c75Rad20px";
      this.view.lblFeatureGroupHeading.skin = this._lblKeySkin;
      this.view.lblFeatureGroupVal.skin = this._commonLabelSkin;
      this.view.lblSequenceNumHeading.skin = this._lblKeySkin;
      this.view.lblSeqNumVal.skin = this._commonLabelSkin;
      this.view.lblIsMandatoryHeading.skin = this._lblKeySkin;
      this.view.lblIsMandatoryVal.skin = this._commonLabelSkin;
      this.view.lblDefaultValueHeading.skin = this._lblKeySkin;
      this.view.lblViewDefaultValue.skin = this._commonLabelSkin;
      this.view.lblViewDescriptionHeading.skin = this._lblKeySkin;
      this.view.lblViewDescriptionVal.skin = this._commonLabelSkin;
      this.view.lblViewFeatureOptions.skin = this._popupCmnHeadingLblSkin;
      this.view.lblOptionDisplayTypeHeading.skin = this._lblKeySkin;
      this.view.lblOptionDisplayTypeValue.skin = this._commonLabelSkin;
      this.view.lblViewFeatureValHeading.skin = this._lblKeySkin;
      this.view.lblViewFeatureDescHeading.skin = this._lblKeySkin;
      // Image Details Screen Skins
      this.view.btnAddImage.skin = this._AddSegmentRowBtnSkin;
      // Additional Attributes Screen Skins
      this.view.lblAttribute.skin = this._commonLabelSkin;
      this.view.lblAttributeValue.skin = this._commonLabelSkin;
      this.view.btnAddAttribute.skin = this._AddSegmentRowBtnSkin;
    },

    setDesginTimeConfigs: function(){
      // Design Time Values taken from Component Properties are set here
      /*
      	1. Product Purposes: [Onboarding, Campaigns]
        2. Option Display Type: [SingleSelection, CheckBox]
        3. Image Types: [Banner, Icon, Content, LargeBanner]
        4. Maxm Additional Attributes Allowed: 10 - Not used in this method.
      */
      if(!Array.isArray(this._productPurposes)){
        this._productPurposes = JSON.parse(this._productPurposes);
        this._productPurposes = this._productPurposes.values;
      }
      this.productPurposes = this._productPurposes;
      if(!Array.isArray(this._optionDisplayType)){
        this._optionDisplayType = JSON.parse(this._optionDisplayType);
        this._optionDisplayType = this._optionDisplayType.values;
      }            
      this.optionDisplayTypeMasterdata = this._optionDisplayType.map(item => [item.backendValue, item.displayValue]);
      this.optionDisplayTypeMasterdata.unshift(["SELECT", this._optionDisplayPlaceholderTxt]);
      if(!Array.isArray(this._imageTypes)){
        this._imageTypes = JSON.parse(this._imageTypes);
        this._imageTypes = this._imageTypes.values;
      }
      this.imageTypesMasterdata = this._imageTypes.map(item => [item.imageTypeBackend, item.imageTypeDisplay]);
      this.imageTypesMasterdata.unshift(["SELECT", this._imageTypeDropdownPlaceholderTxt]);
    },

    setContext: function(context){
      // METHOD_EXPOSED
      // This method should be called everytime the component is opened.
      if(context.isNewProduct){
        this.resetUI(false, null);
        this.isEditFlow = false;
        this.editDataGlobal = {};
      } else if(context.isEdit){
        this.isEditFlow = true;
        this.editDataGlobal = context.editProductData;
        let editProductData = context.editProductData;
        this.resetUI(true, editProductData);
      }
      this.fetchProductLines();
      this.resetGlobalVariables();
    },

    resetGlobalVariables: function(){
      this.currentScreen = 1;
      this.maxScreenNumNavAllowed = 1;
      this.productLinesMasterData = [];
      this.productGroupsMasterData = [];
      this.featureTypeMasterData = [];
      this.sortColName = "";
      this.selRowFeatureData = {};
      this.prevIndex=-1;
      this.isFeatureEdit = false;
      this.areProductLinesFetched=false;
      this.isScreenValidatedArr = [false,false,false,false,false];
    },

    /* #################################### RESET UI #################################### START */
    resetUI: function(isEditFlow, editFlowData){
      this.prevIndex=-1;
      this.resetNavigationBarsUI();
      this.resetProductDetailsUI();
      this.resetProductFeaturesUI();
      this.resetImageDetailsUI();
      this.resetAdditionalDetailsUI();
      this.view.flxProductDetails.setVisibility(true);
      this.view.flxProductDescription.setVisibility(false);
      this.view.flxProductFeatures.setVisibility(false);
      this.view.flxImageDetails.setVisibility(false);
      this.view.flxAdditionalAttributes.setVisibility(false);
      if(isEditFlow){
        this.setDataForEditFlow(editFlowData);
      }      
    },

    resetNavigationBarsUI: function(){
      let activeSkin = this._leftVBarBtnSelectedSkin;
      let inactiveSkin = this._leftVBarBtnUnselectedSkin;
      let btnDarkSkin = this._bottomBarBtnDarkSkin;
      let btnLightSkin = this._bottomBarBtnLightSkin;
      this.view.lblFontIconArrow.text = this._downArrowIcon;//Down Arrow
      this.view.lblOption1.skin = activeSkin;
      this.view.flxSubOptions.isVisible = true;
      this.view.lblSubOption1.skin = activeSkin;
      this.view.lblFontIconRightArrow1.isVisible = true;
      this.view.lblSubOption2.skin = inactiveSkin;
      this.view.lblFontIconRightArrow2.isVisible = false;
      this.view.lblOption2.skin = inactiveSkin;
      this.view.lblFontIconRightArrow3.isVisible = false;
      this.view.lblOption3.skin = inactiveSkin;
      this.view.lblFontIconRightArrow4.isVisible = false;
      this.view.lblOption4.skin = inactiveSkin;
      this.view.lblFontIconRightArrow5.isVisible = false;
      this.view.btnNext.skin = btnDarkSkin;
      this.view.btnNext.focusSkin = btnDarkSkin;
      this.view.btnCancelAddProduct.isVisible = true;
      this.view.btnNext.isVisible = true;
      this.view.btnAddProduct.isVisible = false;
      this.view.btnAddProduct.text = this._addProductBtnTxt;
      this.view.forceLayout();
    },

    resetProductDetailsUI: function(){
      this.resetSubScreen1UI();
      this.resetSubScreen2UI();
    },

    resetSubScreen1UI: function(){
      this.view.lstboxProductLine.setEnabled(true);
      this.view.lstboxProductLine.skin = this._lstboxNormalSkin;
      this.view.lstboxProductLine.masterData = [["SELECT","Select Product Line"]];
      this.view.lstboxProductLine.selectedKey = "SELECT";
      this.view.lstboxProductGroup.setEnabled(true);
      this.view.lstboxProductGroup.skin = this._lstboxNormalSkin;
      this.view.lstboxProductGroup.masterData = [["SELECT", "Select Product Group"]];
      this.view.lstboxProductGroup.selectedKey = "SELECT";
      this.view.tbxProductName.setEnabled(true);
      this.view.tbxProductName.skin = this._tbxNormalSkin;
      this.view.tbxProductName.text = "";
      this.view.tbxProductReference.setEnabled(true);
      this.view.tbxProductReference.skin = this._tbxNormalSkin;
      this.view.tbxProductReference.text = "";
      this.view.customDateField1.value = "";
      this.view.customDateField1.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_date");
      this.view.customDateField2.value = "";
      this.view.customDateField2.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_date");
      this.view.flxCustomProductDate1.isVisible = true;
      this.view.flxCustomProductDate1.skin = this._calendarNormalSkin;
      this.view.flxCustomProductDate2.isVisible = true;
      this.view.flxCustomProductDate2.skin = this._calendarNormalSkin;
      this.view.flxShowStartDate.isVisible = false;
      this.view.lblStartDate.text = "";
      this.view.flxShowEndDate.isVisible = false;
      this.view.lblEndDate.text = "";
      this.view.lblSelectedValue.text = "Select Product Purpose";
      let segProductPurposeData = this.productPurposes.map(this.mappingSegProductPurposeData);
      this.setDataToSegProductPurpose(segProductPurposeData);
      // Hiding Flexes from 15 Extra TextFields whose value is not given in component properties
      for(let widgetNum=1; widgetNum<=15; widgetNum++){
        let numUtility = Math.floor(widgetNum/3);
        if(this["_label"+widgetNum+"Txt"]==="" || this["_placeholder"+widgetNum+"Txt"]===""){
          if(widgetNum===1 || widgetNum%3===0){
            this.disableExtraAdditionalFields(3+numUtility);
          } else if(widgetNum===2){
            this.view.flxProductDetailsContainer9.isVisible = false;
            this.disableExtraAdditionalFields(4);
          } else if(widgetNum%3===1){
            this.view["flxProductDetailsContainer"+(widgetNum+7)].isVisible = false;
            this.view["flxProductDetailsContainer"+(widgetNum+8)].isVisible = false;            
            this.disableExtraAdditionalFields(4+numUtility);
          } else if(widgetNum%3===2){
            this.view["flxProductDetailsContainer"+(widgetNum+7)].isVisible = false;
            this.disableExtraAdditionalFields(4+numUtility);
          }
          break;
        }
      }
      for(let widgetNum=1; widgetNum<=15; widgetNum++){
        this.view["tbxAdditionalField"+widgetNum].text = "";
      }
      for(let widgetNum=1; widgetNum<=7; widgetNum++){        
        this.view["flxErrorMsg"+widgetNum].isVisible = false;
      }
    },

    disableExtraAdditionalFields: function(start){
      const scopeObj = this;
      for(let widgetNum=start; widgetNum<=8; widgetNum++){
        scopeObj.view["flxProductDetails"+widgetNum].isVisible = false;
      }
    },

    resetSubScreen2UI: function(){
      this.view.lblDescTextCounter.setVisibility(false);
      this.view.txtAreaDescription.text = "";
      this.view.lblDetailedDescTextCounter.setVisibility(false);
      this.view.txtAreaDetailedDescription.text = "";
      this.view.txtAreaNotes.text = "";
      this.view.txtAreaDisclosure.text = "";
      this.view.txtAreaTermsAndConditions.text = "";
    },

    resetProductFeaturesUI: function(){      
      this.view.lblFeaturesHeader.text = this._featureHeadingLblTxt + "(0)";
      this.view.btnAddFeature1.setVisibility(false);
      this.view.flxNoSelectedFeatures.setVisibility(true);
      // this.view.flxNoFeaturesError.setVisibility(false);
      this.view.flxViewSelectedFeatures.setVisibility(false);
      this.view.segAddedFeatures.data = [];
      this.resetDataFromAddFeaturePopup();
    },

    resetDataFromAddFeaturePopup: function(){
      this.resetTextFieldsAddFeaturePopup();
      this.view.lstBoxAddFeatureType.setEnabled(true);
      this.view.lstBoxAddFeatureType.skin = this._lstboxNormalSkin;
      this.view.flxErrorMessageFeature1.setVisibility(false);
      this.view.tbxAddFeatureName.setEnabled(true);
      this.view.tbxAddFeatureName.skin = this._tbxNormalSkin;
      this.view.flxErrorMessageFeature2.setVisibility(false);
      this.view.tbxAddFeatureGroup.setEnabled(false);
      this.view.tbxAddFeatureGroup.skin = this._tbxDisabledSkin;
      this.view.flxErrorMessageFeature3.setVisibility(false);
      this.view.lblFeatureDescTextCounter.isVisible = false;
      this.view.txtAreaFeatureDescription.skin = this._tbxAreaNormalSkin;
      this.view.tbxAddFeatureSequenceNumber.skin = this._tbxNormalSkin;
      this.view.flxErrorMessageFeature5.setVisibility(false);
      this.view.fontIconSmsChannelSelectOption.skin = this._checkboxSelIconSkin;
      this.view.lstBoxOptionDisplayType.skin = this._lstboxNormalSkin;
      this.view.flxErrorMessageFeature4.setVisibility(false);
      this.view.btnAddValue.setVisibility(true);      
      this.view.btnSaveAndClose.text = this._featureSaveBtnTxt;
    },

    resetTextFieldsAddFeaturePopup: function(){
      this.view.lstBoxAddFeatureType.selectedKey = "SELECT";
      this.view.tbxAddFeatureName.text = "";
      this.view.tbxAddFeatureGroup.text = "";
      this.view.txtAreaFeatureDescription.text = "";
      this.view.tbxAddFeatureSequenceNumber.text = "";
      this.view.fontIconSmsChannelSelectOption.text=this._selCheckboxIcon;
      this.view.lstBoxOptionDisplayType.masterData = this.optionDisplayTypeMasterdata;
      this.view.lstBoxOptionDisplayType.selectedKey = "SELECT";
      this.setSegFeatureOptionValuesData(); //Initialize single row in segment
    },

    resetImageDetailsUI: function(){
      //this.view.flxNoImageDetailsError.setVisibility(false);
      this.setSegImageDetailsData();
    },

    resetAdditionalDetailsUI: function(){      
      this.setSegAddAttributeData();
    },
    /* #################################### RESET UI #################################### END */

    /* #################################### SET EDIT DATA #################################### START */
    setDataForEditFlow: function(productData){
      this.setEditDataForProductDetails(productData);
      this.setEditDataForProductDescription(productData);
      this.setEditDataForProductFeatures(productData);
      this.setEditDataForImageDetails(productData);
      this.setEditDataForAdditionalAttributes(productData);
      this.view.btnAddProduct.text = this._updateProductBtnTxt;
      this.view.forceLayout();
    },

    setEditDataForProductDetails: function(productData){
      let startDate="";
      let endDate="";
      if(productData.externalIndicator){
        this._externalIndicator =productData.externalIndicator;
      }
      if(productData.availableFrom){
        startDate = productData.availableFrom;
        startDate = startDate.split("-");
        startDate = startDate[1]+"-"+startDate[2]+"-"+startDate[0];
      }
      if(productData.availableTo){
        endDate = productData.availableTo;
        endDate = endDate.split("-");
        endDate = endDate[1]+"-"+endDate[2]+"-"+endDate[0]; 
      }
      if(this.isConfig2===false){
        this.view.tbxProductName.setEnabled(false);
        this.view.tbxProductName.skin = this._tbxDisabledSkin;
        this.view.flxCustomProductDate1.isVisible = false;
        this.view.flxCustomProductDate2.isVisible = false;
        this.view.flxShowStartDate.isVisible = true;
        this.view.flxShowEndDate.isVisible = true;
        this.view.lblStartDate.text = startDate || "N/A";
        this.view.lblEndDate.text = endDate || "N/A";
      } else if(this.isConfig2){
        this.view.customDateField1.value = startDate;
        this.view.customDateField1.resetData = startDate ? startDate : kony.i18n.getLocalizedString("i18n.frmLogsController.Select_date");
        this.view.customDateField2.value = endDate;
        this.view.customDateField2.resetData = endDate ? endDate : kony.i18n.getLocalizedString("i18n.frmLogsController.Select_date");
      }
      this.view.lstboxProductLine.setEnabled(false);
      this.view.lstboxProductLine.skin = this._lstboxDisabledSkin;
      this.view.lstboxProductGroup.setEnabled(false);
      this.view.lstboxProductGroup.skin = this._lstboxDisabledSkin;
      this.view.tbxProductReference.setEnabled(false);
      this.view.tbxProductReference.skin = this._tbxDisabledSkin;
      this.view.lstboxProductLine.selectedKey = productData.productLineRef;      
      this.view.lstboxProductGroup.selectedKey = productData.productGroupRef;
      this.view.tbxProductName.text = productData.productName;
      this.view.tbxProductReference.text = productData.productRef;
      this.view.lblSelectedValue.text = productData.purposes.length + " Selected";
      let purposesSelectedIndices = [];
      for (let index = 0; index < this.productPurposes.length; index++) {
        if (productData.purposes.includes(this.productPurposes[index])){
          purposesSelectedIndices.push(index);
        }
      }
      this.view.segProductPurpose.selectedIndices = [[0,purposesSelectedIndices]];    
      this.view.forceLayout();      
      // TODO: Set Data for 15 Additional Fields if there are any.
    },

    setEditDataForProductDescription: function(productData){      
      this.view.txtAreaDescription.text = productData.description;
      this.view.txtAreaDetailedDescription.text = productData.detailedDesc;
      this.view.txtAreaNotes.text = productData.notes;
      this.view.txtAreaDisclosure.text = productData.disclosure;
      this.view.txtAreaTermsAndConditions.text = productData.termsConditions;
    },

    setEditDataForProductFeatures: function(productData){      
      let productFeatures = productData.productFeatures;
      this.view.lblFeaturesHeader.text = this._featureHeadingLblTxt + "(" + productFeatures.length + ")";
      if(productFeatures.length!==0){
        this.addFeatureDataToSeg(productFeatures);        
      } else {
        this.view.segAddedFeatures.data = [];
      }
      let areFeaturesPresent = productFeatures.length===0 ? false : true;
      this.view.btnAddFeature1.setVisibility(areFeaturesPresent);
      this.view.flxNoSelectedFeatures.setVisibility(!areFeaturesPresent);
      this.view.flxViewSelectedFeatures.setVisibility(areFeaturesPresent);      
      if(this.isConfig2===false){
        this.view.btnAddFeature1.setVisibility(false);
        this.view.btnAddFeature2.setVisibility(false);
        this.view.lstBoxAddFeatureType.setEnabled(false);
        this.view.lstBoxAddFeatureType.skin = this._lstboxDisabledSkin;
        this.view.fontIconSmsChannelSelectOption.skin = this._checkboxUnselIconSkin;
        this.view.btnAddValue.setVisibility(false);
      }
    },

    setEditDataForImageDetails: function(productData){
      let imageDetails = productData.imageDetails;
      if(imageDetails.length===0)
        this.setSegImageDetailsData();
      else{
        let data = [];
        for(let index=0; index<imageDetails.length; index++){
          let imagedata = this.getNewRowDataSegImageDetails(imageDetails[index]);
          data.push(imagedata);
        }
        this.view.segImageDetails.widgetDataMap = this.getDataMapForSegImageDetails();
        this.view.segImageDetails.setData(data);
        this.view.forceLayout();
      }
    },

    setEditDataForAdditionalAttributes: function(productData){
      let additionalAttributes = productData.extensionData;
      let addAttrLength = Object.keys(additionalAttributes).length;
      if(addAttrLength===0){
        this.setSegAddAttributeData();
      } else {
        let data = [];
        for(let index = 0; index<addAttrLength; index++){
          let key = Object.keys(additionalAttributes)[index];
          let val = additionalAttributes[key];
          let attrData = {
            "key":key,
            "value":val
          };
          let addAttrData = this.getNewRowDataSegAddAttribute(attrData);
          data.push(addAttrData);
        }
        this.view.segAddAttribute.widgetDataMap = this.getDataMapForSegAddAttribute();
        this.view.segAddAttribute.setData(data);
        this.view.forceLayout();
      }
    },
    /* #################################### SET EDIT DATA #################################### END */

    /* #################################### SET FLOW ACTIONS #################################### START */
    setFlowActions: function(){
      this.setFlowActionsNavigationBars();
      this.setFlowActionsProductDetails();
      this.setFlowActionsProductFeatures();
      this.setFlowActionsImageDetails();
      this.setFlowActionsAdditionalAttributes();      
    },

    setFlowActionsNavigationBars: function(){
      const scopeObj = this;
      this.view.lblFontIconArrow.onTouchEnd = function(){
        let subscreenVisibility = scopeObj.view.flxSubOptions.isVisible===false;
        scopeObj.view.flxSubOptions.setVisibility(subscreenVisibility);
        scopeObj.view.lblFontIconArrow.text = subscreenVisibility ? scopeObj._downArrowIcon : scopeObj._upArrowIcon;
        scopeObj.view.forceLayout();
      };
      this.view.lblOption1.onTouchEnd = function(){
        let subscreenVisibility = scopeObj.view.flxSubOptions.isVisible===false;
        scopeObj.view.flxSubOptions.setVisibility(subscreenVisibility);
        scopeObj.view.lblFontIconArrow.text = subscreenVisibility ? scopeObj._downArrowIcon : scopeObj._upArrowIcon;
        scopeObj.view.forceLayout();
      };
      this.view.lblSubOption1.onTouchEnd = function(){
        let proceed = true;
        if(scopeObj.isNavigationAllowed(1)){
          if(scopeObj.isValidationRequired(1)) proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
          if(proceed) scopeObj.navigateToScreen(1);
        }
      };
      this.view.lblSubOption2.onTouchEnd = function(){
        let proceed = true;
        if(scopeObj.isNavigationAllowed(2)){          
          if(scopeObj.isValidationRequired(2)) proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
          if(proceed) scopeObj.navigateToScreen(2);
        }
      };
      this.view.lblOption2.onTouchEnd = function(){
        let proceed = true;
        if(scopeObj.isNavigationAllowed(3)){
          if(scopeObj.isValidationRequired(3)) proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
          if(proceed) scopeObj.navigateToScreen(3);
        }
      };
      this.view.lblOption3.onTouchEnd = function(){
        let proceed = true;
        if(scopeObj.isNavigationAllowed(4)){
          if(scopeObj.isValidationRequired(4)) proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
          if(proceed) scopeObj.navigateToScreen(4);
        }
      };
      this.view.lblOption4.onTouchEnd = function(){
        let proceed = true;
        if(scopeObj.isNavigationAllowed(5)){          
          if(scopeObj.isValidationRequired(5)) proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
          if(proceed) scopeObj.navigateToScreen(5);
        }
      };
      this.view.lblSubOption2.onHover = function(){
        let hoverSkin = scopeObj.isNavigationAllowed(2) ? scopeObj._leftVBarBtnHoverSkin : scopeObj._leftVBarBtnUnselectedSkin;
        scopeObj.view.lblSubOption2.hoverSkin = hoverSkin;
      };
      this.view.lblOption2.onHover = function(){
        let hoverSkin = scopeObj.isNavigationAllowed(3) ? scopeObj._leftVBarBtnHoverSkin : scopeObj._leftVBarBtnUnselectedSkin;        
        scopeObj.view.lblOption2.hoverSkin = hoverSkin;
      };
      this.view.lblOption3.onHover = function(){
        let hoverSkin = scopeObj.isNavigationAllowed(4) ? scopeObj._leftVBarBtnHoverSkin : scopeObj._leftVBarBtnUnselectedSkin;        
        scopeObj.view.lblOption3.hoverSkin = hoverSkin;
      };
      this.view.lblOption4.onHover = function(){
        let hoverSkin = scopeObj.isNavigationAllowed(5) ? scopeObj._leftVBarBtnHoverSkin : scopeObj._leftVBarBtnUnselectedSkin;        
        scopeObj.view.lblOption4.hoverSkin = hoverSkin;
      };
      this.view.btnCancelAddProduct.onClick = function(){
        scopeObj.resetUI(false, null);
        if(scopeObj.closeAddProductComponent) scopeObj.closeAddProductComponent();
      };
      this.view.btnNext.onClick = function(){
        let proceed = scopeObj.validateFieldsWrapper(scopeObj.currentScreen);
        if(proceed) scopeObj.navigateToScreen(scopeObj.currentScreen+1);
      };
      this.view.btnAddProduct.onClick = function(){
        if(scopeObj.validateFieldsWrapper(scopeObj.currentScreen)){
          let createProductJSON = scopeObj.fetchFinalAddProductJSON();
          if (scopeObj.isEditFlow) scopeObj.editProduct(createProductJSON);
          else scopeObj.createProduct(createProductJSON);
        }
      };
    },

    setFlowActionsProductDetails: function(){
      const scopeObj = this;
      this.view.lstboxProductLine.onSelection = function(){
        if(scopeObj.view.lstboxProductLine.selectedKey!=="SELECT"){
          scopeObj.view.lstboxProductLine.skin = scopeObj._lstboxNormalSkin;
          scopeObj.view.flxErrorMsg1.setVisibility(false);
          scopeObj.fetchProductGroupsByProductLine();
        }
      };
      this.view.lstboxProductGroup.onSelection = function(){
        if(scopeObj.view.lstboxProductGroup.selectedKey!=="SELECT"){
          scopeObj.view.lstboxProductGroup.skin = scopeObj._lstboxNormalSkin;
          scopeObj.view.flxErrorMsg2.setVisibility(false);
        }
      };
      this.view.tbxProductName.onKeyUp = function(){
        if(scopeObj.view.tbxProductName.text!==""){
          scopeObj.view.tbxProductName.skin = scopeObj._tbxNormalSkin;
          scopeObj.view.flxErrorMsg3.setVisibility(false);
        }
      };
      this.view.tbxProductReference.onKeyUp = function(){
        if(scopeObj.view.tbxProductReference.text!==""){
          scopeObj.view.tbxProductReference.skin = scopeObj._tbxNormalSkin;
          scopeObj.view.flxErrorMsg4.setVisibility(false);
        }
      };
      this.view.flxCustomProductDate1.onTouchStart = function(){
        scopeObj.view.flxCustomProductDate1.skin = scopeObj._calendarNormalSkin;
        scopeObj.view.flxErrorMsg5.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.flxCustomProductDate2.onTouchStart = function(){
        scopeObj.view.flxCustomProductDate2.skin = scopeObj._calendarNormalSkin;
        scopeObj.view.flxErrorMsg6.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.flxSelectedText.onTouchEnd = function(){
        scopeObj.view.flxSegmentList.setVisibility(scopeObj.view.flxSegmentList.isVisible === false);
        scopeObj.view.flxSelectedText.skin = scopeObj._tbxNormalSkin;
        scopeObj.view.forceLayout();
      };
      this.view.segProductPurpose.onRowClick = function(){
        scopeObj.segProductPurposeOnRowClick();
        scopeObj.view.flxSelectedText.skin = scopeObj._tbxNormalSkin;
        scopeObj.view.forceLayout();
      };
      this.view.flxSegmentList.onHover = function(widget, context){
        if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
          scopeObj.view.flxSegmentList.setVisibility(true);
        } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE){
          scopeObj.view.flxSegmentList.setVisibility(false);
        }
        scopeObj.view.forceLayout();
      };
      this.view.txtAreaDescription.onKeyUp = function(){
        scopeObj.view.lblDescTextCounter.isVisible = true;
        let textLen = scopeObj.view.txtAreaDescription.text.length;
        let maxLen = scopeObj.view.txtAreaDescription.maxTextLength;
        scopeObj.view.lblDescTextCounter.text = textLen + "/" + maxLen;
        scopeObj.view.flxDescriptionContainer.forceLayout();
      };
      this.view.txtAreaDescription.onEndEditing = function(){
        scopeObj.view.lblDescTextCounter.isVisible = false;
      };
      this.view.txtAreaDetailedDescription.onKeyUp = function(){
        scopeObj.view.lblDetailedDescTextCounter.isVisible = true;
        let textLen = scopeObj.view.txtAreaDetailedDescription.text.length;
        let maxLen = scopeObj.view.txtAreaDetailedDescription.maxTextLength;
        scopeObj.view.lblDetailedDescTextCounter.text = textLen + "/" + maxLen;
        scopeObj.view.flxDetailedDescriptionContainer.forceLayout();
      };
      this.view.txtAreaDetailedDescription.onEndEditing = function(){
        scopeObj.view.lblDetailedDescTextCounter.isVisible = false;
      };
    },

    setFlowActionsProductFeatures: function(){
      const scopeObj = this;
      this.view.btnAddFeature1.onClick = function(){  
        scopeObj.view.flxAddProductPopups.isVisible = true;
        scopeObj.view.flxAddFeaturePopup.isVisible = true;
        scopeObj.resetDataFromAddFeaturePopup();
        scopeObj.alignFlxManageProducts();        
      };
      this.view.btnAddFeature2.onClick = function(){
        scopeObj.view.flxAddProductPopups.isVisible = true;
        scopeObj.view.flxAddFeaturePopup.isVisible = true;
        scopeObj.resetDataFromAddFeaturePopup();
        scopeObj.alignFlxManageProducts();        
      };
      this.view.flxClose.onTouchEnd = function(){
        scopeObj.closePopup();
      };      
      this.view.txtAreaFeatureDescription.onKeyUp = function(){
        scopeObj.view.lblFeatureDescTextCounter.isVisible = true;
        let textLength = scopeObj.view.txtAreaFeatureDescription.text.length;        
        let maxLen = scopeObj.view.txtAreaFeatureDescription.maxTextLength;
        scopeObj.view.lblFeatureDescTextCounter.text = ""+textLength+"/"+maxLen;
        scopeObj.view.forceLayout();
      };
      this.view.txtAreaFeatureDescription.onEndEditing = function(){
        scopeObj.view.lblFeatureDescTextCounter.isVisible = false;
      };
      this.view.tbxAddFeatureSequenceNumber.onKeyUp = function() {
        let totalText = scopeObj.view.tbxAddFeatureSequenceNumber.text;
        scopeObj.view.tbxAddFeatureSequenceNumber.text = totalText.replace(/\D/g, "");
        let inputText = scopeObj.view.tbxAddFeatureSequenceNumber.text.trim();
        if(inputText!=="" && parseInt(inputText)!==0){
          scopeObj.view.flxErrorMessageFeature5.setVisibility(false);
          scopeObj.view.tbxAddFeatureSequenceNumber.skin = scopeObj._tbxNormalSkin;
        }
      };
      this.view.fontIconSmsChannelSelectOption.onTouchEnd = function(){
        if(scopeObj.isConfig2){
          let checkboxCode = scopeObj.view.fontIconSmsChannelSelectOption.text===scopeObj._selCheckboxIcon ? scopeObj._unselCheckboxIcon : scopeObj._selCheckboxIcon;
          scopeObj.view.fontIconSmsChannelSelectOption.text = checkboxCode;
        }
      };
      this.view.lblMandatorySequenceNumber.onTouchEnd = function(){
        if(scopeObj.isConfig2){
          let checkboxCode = scopeObj.view.fontIconSmsChannelSelectOption.text===scopeObj._selCheckboxIcon ? scopeObj._unselCheckboxIcon : scopeObj._selCheckboxIcon;
          scopeObj.view.fontIconSmsChannelSelectOption.text = checkboxCode;
        }
      };
      this.view.lstBoxOptionDisplayType.onSelection = function(){
        if(scopeObj.view.lstBoxOptionDisplayType.selectedKey!=="SELECT"){
          scopeObj.view.lstBoxOptionDisplayType.skin = scopeObj._lstboxNormalSkin;
        }
      };
      this.view.btnAddValue.onTouchEnd = function(){
        scopeObj.addOptionValueRow();
      };
      this.view.btnCancel.onClick = function(){
        scopeObj.closePopup();
      };
      this.view.btnSaveAndClose.onClick = function(){        
        let proceed = scopeObj.validateDataAddFeaturePopup();
        if(proceed){
          //TODO: Check whether Feature with existing FeatureName is already present, if yes don't create feature.
          let data = scopeObj.getNewFeatureData();
          scopeObj.addFeatureDataToSeg(data);
          let dataLength = scopeObj.view.segAddedFeatures.data.length;
          scopeObj.view.lblFeaturesHeader.text = scopeObj._featureHeadingLblTxt + "(" + dataLength + ")";
          scopeObj.resetDataFromAddFeaturePopup();
          if(dataLength!==0){
            scopeObj.view.flxNoSelectedFeatures.setVisibility(false);
            scopeObj.view.flxViewSelectedFeatures.setVisibility(true);
            scopeObj.view.btnAddFeature1.isVisible = scopeObj.isConfig2 ? true : false;
            // scopeObj.view.flxNoFeaturesError.setVisibility(false);
          }          
          scopeObj.closePopup();
          let toastMsg = "";
          if(scopeObj.isFeatureEdit === false) toastMsg = "New Feature Added Successfully.";
          else toastMsg = "Feature Edited Successfully.";
          scopeObj.isFeatureEdit = false;
          scopeObj.willUpdateUI({
            toastModel:{
              status : "success",
              message: toastMsg
            }
          });
        }
      };
      this.view.lblFeatureName.onTouchEnd = function(){        
        let sortIconWidget = scopeObj.view.fontIconSortFeatureName;
        scopeObj.sortColName = "featureName";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.fontIconSortFeatureName.onTouchEnd = function(){
        let sortIconWidget = scopeObj.view.fontIconSortFeatureName;
        scopeObj.sortColName = "featureName";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.lblFeatureGroup.onTouchEnd = function(){
        let sortIconWidget = scopeObj.view.fontIconSortFeatureGroup;
        scopeObj.sortColName = "featureGroup";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.fontIconSortFeatureGroup.onTouchEnd = function(){
        let sortIconWidget = scopeObj.view.fontIconSortFeatureGroup;
        scopeObj.sortColName = "featureGroup";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.lblType.onTouchEnd = function(){
        let sortIconWidget = scopeObj.view.fontIconSortType;
        scopeObj.sortColName = "type";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.fontIconSortType.onTouchEnd = function(){
        let sortIconWidget = scopeObj.view.fontIconSortType;
        scopeObj.sortColName = "type";
        scopeObj.sortFeatures(sortIconWidget);
      };
      this.view.segAddedFeatures.onRowClick = function(){
        scopeObj.viewFeaturePopup();
      };
      this.view.flxClose2.onTouchEnd = function(){
        scopeObj.closePopup();
      };
      this.view.btnEditFeature.onClick = function(){
        scopeObj.closePopup();
        scopeObj.editProductFeature();
      };
      this.view.flxOption1.onTouchEnd = function(){
        // Edit Product Feature
        let selRow = scopeObj.view.segAddedFeatures.selectedRowIndex[1];
        scopeObj.selRowFeatureData = scopeObj.view.segAddedFeatures.data[selRow];
        scopeObj.editProductFeature();
      };
      this.view.flxOption2.onTouchEnd = function(){
        scopeObj.setDeleteFeatureMessage();
        scopeObj.alignFlxManageProducts();
      };
      this.view.popUp.btnPopUpDelete.onClick = function(){
        // Delete Product Feature
        scopeObj.deleteProductFeature();
        scopeObj.view.popUp.setVisibility(false);
      };
      this.view.popUp.flxPopUpClose.onTouchEnd = function(){
        scopeObj.view.popUp.setVisibility(false);
      };
      this.view.popUp.btnPopUpCancel.onClick = function(){
        scopeObj.view.popUp.setVisibility(false);
      };
      this.view.lstBoxAddFeatureType.onSelection = function(){
        if(scopeObj.view.lstBoxAddFeatureType.selectedKey!=="SELECT"){
          scopeObj.view.flxErrorMessageFeature1.setVisibility(false);
          scopeObj.view.lstBoxAddFeatureType.skin = scopeObj._lstboxNormalSkin;
          scopeObj.assignFeatureNameAndGroup();
        }
      };
      this.view.lstBoxOptionDisplayType.onSelection = function(){
        if(scopeObj.view.lstBoxOptionDisplayType.selectedKey!=="SELECT"){
          scopeObj.view.flxErrorMessageFeature4.setVisibility(false);
          scopeObj.view.lstBoxOptionDisplayType.skin = scopeObj._lstboxNormalSkin;
        }
      };
    },

    setFlowActionsImageDetails: function(){
      const scopeObj = this;
      this.view.btnAddImage.onClick = function(){
        scopeObj.addImageDetailsRow();
      };
    },

    setFlowActionsAdditionalAttributes: function(){
      const scopeObj = this;
      this.view.btnAddAttribute.onClick = function(){
        scopeObj.addAdditionalAttributesRow();
      };
    },
    /* #################################### SET_FLOW_ACTIONS #################################### END */


    isNavigationAllowed: function(targetScreenNum){
      if(this.isEditFlow) return true;
      return targetScreenNum <= this.maxScreenNumNavAllowed;
    },

    isValidationRequired: function(targetScreenNum){
      // If moving to screen grater than current screen, validation is required.
      // IF moving to screen less than current screen, validation for current screen will be required only if it has been previously validated,
      // to handle the case in which user deletes something from current screen.
      if(this.currentScreen < targetScreenNum)
        return true;
      else if((this.currentScreen > targetScreenNum) && this.isScreenValidatedArr[this.currentScreen]===true){
        return true;
      } else{
        return false;
      }
    },

    validateFieldsWrapper: function(screenNum){
      // When user is trying to navigate to a different screen, this method is called
      let proceed = false;      
      if(screenNum===1) proceed = this.validateFieldsScreen1();
      if(screenNum===2) proceed = true;
      if(screenNum===3) proceed = true;
      if(screenNum===4) proceed = true;//this.validateFieldsScreen4();
      if(screenNum===5) proceed = true;
      this.isScreenValidatedArr[screenNum] = true;
      return proceed;
    },

    validateFieldsScreen1: function(){
      let proceed = true;
      if(this.view.lstboxProductLine.selectedKey==="SELECT"){
        this.view.lstboxProductLine.skin = this._lstboxErrorSkin;
        this.view.flxErrorMsg1.isVisible = true;
        proceed = false;
      }
      if(this.view.lstboxProductGroup.selectedKey==="SELECT"){
        this.view.lstboxProductGroup.skin = this._lstboxErrorSkin;
        this.view.flxErrorMsg2.isVisible = true;
        proceed = false;
      }
      if(this.view.tbxProductName.text===""){
        this.view.tbxProductName.skin = this._tbxErrorSkin;
        this.view.flxErrorMsg3.isVisible = true; 
        proceed = false;
      }
      if(this.view.tbxProductReference.text===""){
        this.view.tbxProductReference.skin = this._tbxErrorSkin;
        this.view.flxErrorMsg4.isVisible = true; 
        proceed = false;
      }
      let startDate = this.view.customDateField1.value.trim().replace(/-/g,"/");
      startDate = new Date(startDate);
      let endDate = this.view.customDateField2.value.trim().replace(/-/g,"/");
      endDate = new Date(endDate);
      // 1. Either both dates should be filled or none
      // 2. startDate<endDate
      if(isNaN(startDate) && !isNaN(endDate)){
        this.view.flxErrorMsg5.setVisibility(true);
        this.view.lblErrorText5.text = "Start date cannot be empty.";
        this.view.flxCustomProductDate1.skin = this._calendarErrorSkin;
        proceed = false;
      } else if(!isNaN(startDate) && isNaN(endDate)){
        this.view.flxErrorMsg6.setVisibility(true);
        this.view.lblErrorText6.text = "End date cannot be empty.";
        this.view.flxCustomProductDate2.skin = this._calendarErrorSkin;
        proceed = false;
      } else if(!isNaN(startDate) && !isNaN(endDate) && this.isInValidDate(startDate,endDate, true)){
        this.view.flxErrorMsg6.setVisibility(true);
        this.view.lblErrorText6.text = "End date cannot be less than or equal to start date.";
        this.view.flxCustomProductDate2.skin = this._calendarErrorSkin;
        proceed = false;
      }
      /* 
      // Validations for Start Date: Removed since not mentioned in story
      let maxStartDate = new Date(new Date().setDate(new Date().getDate()+90));
      if(this.isInValidDate(new Date(),startDate)){
        this.view.flxErrorMsg5.setVisibility(true);
        this.view.lblErrorText5.text = "Start date cannot be earlier than today.";
        this.view.flxCustomProductDate1.skin = this._calendarErrorSkin;
        proceed = false;
      } else if(this.isInValidDate(startDate,maxStartDate)){
        this.view.flxErrorMsg5.setVisibility(true);
        this.view.lblErrorText5.text = "Start date should not be more than 90 days from today.";
        this.view.flxCustomProductDate1.skin = this._calendarErrorSkin;
        proceed = false;
      }
      */
      /* 
      // Validations for End Date: Removed since not mentioned in story
       if(this.isInValidDate(endDate,new Date(startDate.setDate(startDate.getDate()+90)))){
        this.view.flxErrorMsg6.setVisibility(true);
        this.view.lblErrorText6.text = "End date should not be more than 90 days from the start date.";
        this.view.flxCustomProductDate2.skin = this._calendarErrorSkin;
        proceed = false;
      }
      */
      return proceed;
    },

    validateFieldsScreen4: function(){
      let proceed = true;      
      let imgSegData = this.view.segImageDetails.data;
      for(let row=0; row<imgSegData.length; row++){
        let imgURL = imgSegData[row].tbxImageURL.text;
        if(imgSegData[row].lstBoxImageType.selectedKey!=="SELECT"){
          if(imgURL.trim() === ""){
            imgSegData[row].tbxImageURL.skin = this._tbxErrorSkin;
            imgSegData[row].flxErrorMsgURL.isVisible = true;
            imgSegData[row].lblErrorText.text = "Image source URL cannot be empty.";
            proceed = false;
          } 
          else if(imgURL.indexOf("http:") === 0){
            imgSegData[row].tbxImageURL.skin = this._tbxErrorSkin;
            imgSegData[row].flxErrorMsgURL.isVisible = true;
            imgSegData[row].lblErrorText.text = "The URL should start with https:// or ftp://";
            proceed = false;   
          } 
          else if(!this.imageValidationRegex.test(imgURL)){
            imgSegData[row].tbxImageURL.skin = this._tbxErrorSkin;
            imgSegData[row].flxErrorMsgURL.isVisible = true;
            imgSegData[row].lblErrorText.text = "Invalid image URL format.";
            proceed = false;
          }
        }
      }
      this.view.segImageDetails.setData(imgSegData);
      return proceed;
    },

    validateDataAddFeaturePopup: function(){
      let proceed = true;
      if(this.view.lstBoxAddFeatureType.selectedKey==="SELECT"){
        this.view.lstBoxAddFeatureType.skin = this._lstboxErrorSkin;
        this.view.flxErrorMessageFeature1.isVisible = true;
        proceed = false;
      }
      if(this.view.lstBoxOptionDisplayType.selectedKey==="SELECT"){
        this.view.lstBoxOptionDisplayType.skin = this._lstboxErrorSkin;
        this.view.flxErrorMessageFeature4.isVisible = true;
        proceed = false;
      }
      if(typeof this.view.tbxAddFeatureSequenceNumber.text === "string"){
        let seqNumInputText = this.view.tbxAddFeatureSequenceNumber.text.trim();
        if(seqNumInputText==="" || parseInt(seqNumInputText)===0){
          this.view.tbxAddFeatureSequenceNumber.skin = this._tbxErrorSkin;
          this.view.flxErrorMessageFeature5.isVisible = true;
          proceed = false;
        }
      } else if(typeof this.view.tbxAddFeatureSequenceNumber.text === "number"){
        if(this.view.tbxAddFeatureSequenceNumber.text===0){
          this.view.tbxAddFeatureSequenceNumber.skin = this._tbxErrorSkin;
          this.view.flxErrorMessageFeature5.isVisible = true;
          proceed = false;
        }
      }
      let segData = this.view.segFeatureOptionValues.data;
      for(let segRow=0; segRow<segData.length; segRow++){
        if(segData[segRow].fontIconSelectOptionDefaultValue.text===this._selCheckboxIcon && segData[segRow].tbxValue.text.trim()===""){
          segData[segRow].tbxValue.skin = this._tbxErrorSkin;
          segData[segRow].flxErrorMessageFeature6.isVisible = true;
          proceed = false;
        } else {
          segData[segRow].tbxValue.skin = this._tbxNormalSkin;
          segData[segRow].flxErrorMessageFeature6.isVisible = false;
        }        
      }
      this.view.segFeatureOptionValues.setData(segData);
      return proceed;
    },

    isInValidDate: function(date1, date2, isEndDateValidation){
      let isInvalid = false;
      if(isNaN(date1.getDate()) || isNaN(date2.getDate())){
        isInvalid = true;
      }
      date1.setHours(0, 0, 0, 0);
      date2.setHours(0, 0, 0, 0);
      if(date1 - date2 > 0 || (isEndDateValidation && date1 - date2 === 0)) {
        isInvalid = true;
      }
      return isInvalid;
    },

    navigateToScreen: function(screenNumber){
      const scopeObj = this;
      this.currentScreen = screenNumber;
      this.changeNavigationBarSkins(screenNumber);
      this.maxScreenNumNavAllowed = this.maxScreenNumNavAllowed<this.currentScreen ? this.currentScreen : this.maxScreenNumNavAllowed;
      this.view.flxProductDetails.setVisibility(screenNumber===1);
      this.view.flxProductDescription.setVisibility(screenNumber===2);
      this.view.flxProductFeatures.setVisibility(screenNumber===3);
      this.view.flxImageDetails.setVisibility(screenNumber===4);
      this.view.flxAdditionalAttributes.setVisibility(screenNumber===5);
      if(screenNumber===3){
        if(scopeObj.view.segAddedFeatures.data.length===0){
          scopeObj.view.flxNoSelectedFeatures.setVisibility(true);
          scopeObj.view.flxViewSelectedFeatures.setVisibility(false);
          scopeObj.view.btnAddFeature1.isVisible = false;
        } else {
          scopeObj.view.flxNoSelectedFeatures.setVisibility(false);
          scopeObj.view.flxViewSelectedFeatures.setVisibility(true);
          scopeObj.view.btnAddFeature1.isVisible = scopeObj.isConfig2 ? true : false;
        }
        this.fetchProductFeatures();
      }
      this.view.forceLayout();
    },

    changeNavigationBarSkins: function(screenNumber){
      let activeSkin = this._leftVBarBtnSelectedSkin;
      let inactiveSkin = this._leftVBarBtnUnselectedSkin;
      let btnDarkSkin = this._bottomBarBtnDarkSkin;
      let btnLightSkin = this._bottomBarBtnLightSkin;      

      this.view.lblFontIconArrow.text = (screenNumber===1 || screenNumber===2) ? this._downArrowIcon : this._upArrowIcon;
      this.view.lblOption1.skin = (screenNumber===1 || screenNumber===2) ? activeSkin : inactiveSkin;
      this.view.flxSubOptions.isVisible = (screenNumber===1 || screenNumber===2) ? true : false;

      this.view.lblSubOption1.skin = (screenNumber===1) ? activeSkin : inactiveSkin;
      this.view.lblFontIconRightArrow1.isVisible = (screenNumber===1) ? true : false;
      this.view.lblSubOption2.skin = (screenNumber===2) ? activeSkin : inactiveSkin;
      this.view.lblFontIconRightArrow2.isVisible = (screenNumber===2) ? true: false;

      this.view.lblOption2.skin = (screenNumber===3) ? activeSkin : inactiveSkin;
      this.view.lblFontIconRightArrow3.isVisible = (screenNumber===3) ? true: false;      
      this.view.lblOption3.skin = (screenNumber===4) ? activeSkin : inactiveSkin;
      this.view.lblFontIconRightArrow4.isVisible = (screenNumber===4) ? true: false;
      this.view.lblOption4.skin = (screenNumber===5) ? activeSkin : inactiveSkin;
      this.view.lblFontIconRightArrow5.isVisible = (screenNumber===5) ? true: false;

      if(screenNumber<=3){
        this.view.btnNext.skin = btnDarkSkin;
        this.view.btnNext.focusSkin = btnDarkSkin;
        this.view.btnNext.isVisible = true;
        this.view.btnAddProduct.isVisible = false;
      } else if(screenNumber===4){
        this.view.btnNext.skin = btnLightSkin;
        this.view.btnNext.focusSkin = btnLightSkin;
        this.view.btnNext.isVisible = true;
        this.view.btnAddProduct.isVisible = true;
      } else if(screenNumber===5){
        this.view.btnNext.isVisible = false;
        this.view.btnAddProduct.isVisible = true;
      }
      this.view.forceLayout();
    },

    assignFeatureNameAndGroup: function(){
      const scopeObj = this;
      let featureType = scopeObj.view.lstBoxAddFeatureType.selectedKey;
      let featureName = scopeObj.allFeatureType_NameGroupMap.get(featureType)[0];
      let featureGroup = scopeObj.allFeatureType_NameGroupMap.get(featureType)[1];
      scopeObj.view.tbxAddFeatureName.text = featureName;
      scopeObj.view.tbxAddFeatureGroup.text = featureGroup;
    },

    mappingSegProductPurposeData: function(data){
      // PRODUCT_DETAILS
      return{
        "purpose": data,
        "imgCheckBox": {
          "src": "checkboxnormal.png"
        },
        "lblDescription": {
          "text": data
        }
      };
    },

    getDataMapForProductPurpose: function(){
      // PRODUCT_DETAILS
      let dataMap = {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "imgCheckBox",
        "lblDescription": "lblDescription"
      };
      return dataMap;
    },

    setDataToSegProductPurpose: function(data){
      // PRODUCT_DETAILS
      this.view.segProductPurpose.widgetDataMap = this.getDataMapForProductPurpose();
      this.view.segProductPurpose.setData(data);
    },

    segProductPurposeOnRowClick: function(){ 
      // PRODUCT_DETAILS
      let selRows = this.view.segProductPurpose.selectedRowItems;
      selRows = selRows ? selRows.length : 0;      
      this.view.lblSelectedValue.text = selRows + " Selected";
      this.view.forceLayout();
    },

    editProductFeature: function(){
      // PRODUCT_FEATURES
      this.view.flxAddProductPopups.isVisible = true;
      this.view.flxAddFeaturePopup.isVisible = true;
      this.alignFlxManageProducts();
      this.populateDataForFeatureEdit();
    },

    deleteProductFeature: function(){
      // PRODUCT_FEATURES
      let rowIndex = this.view.segAddedFeatures.selectedRowIndex[1];
      this.view.segAddedFeatures.removeAt(rowIndex);
      this.prevIndex = -1;
      let segData = this.view.segAddedFeatures.data;
      this.view.lblFeaturesHeader.text = this._featureHeadingLblTxt + "(" + segData.length + ")";
      if(segData.length===0){        
        this.view.flxNoSelectedFeatures.setVisibility(true);
        this.view.flxViewSelectedFeatures.setVisibility(false);
        this.view.flxContextualMenu.setVisibility(false);
        this.view.btnAddFeature1.setVisibility(false);
      }
      this.willUpdateUI({
        toastModel:{
          status : "success",
          message: "Deleted Successfully"
        }
      });
    },

    setDeleteFeatureMessage: function(){
      // PRODUCT_FEATURES: Show Popup Message before Deleting Feature
      this.view.popUp.setVisibility(true);
      this.view.popUp.lblPopUpMainMessage.text = "Delete Feature";
      let segData = this.view.segAddedFeatures.data;
      let selRow = this.view.segAddedFeatures.selectedRowIndex[1];
      let line1 = "Are you sure you want to delete" + segData[selRow].featureName + "?<br>";
      let line2 = "Upon deleting, this product won’t be available.";
      this.view.popUp.rtxPopUpDisclaimer.text = line1 + line2;
      this.view.popUp.btnPopUpCancel.text = "CANCEL";
      this.view.popUp.btnPopUpDelete.text = "DELETE";
    },

    getNewFeatureData: function(){
      // PRODUCT_FEATURES: Fetch Data from AddFeatures Popup and return it.
      const scopeObj = this;
      let segData = this.view.segFeatureOptionValues.data;
      let defaultValue="";
      let optionValues = [];
      let optionDispType = scopeObj.view.lstBoxOptionDisplayType.selectedKey==="SELECT" ? "" : scopeObj.view.lstBoxOptionDisplayType.selectedKey;
      for(let segRow=0; segRow<segData.length; segRow++){
        if(defaultValue==="" && segData[segRow].fontIconSelectOptionDefaultValue.text===scopeObj._selCheckboxIcon){
          defaultValue = segData[segRow].tbxValue.text;
        }
        let optionValue = {
          "value": segData[segRow].tbxValue.text,
          "desc": segData[segRow].txtAreaFeatureDescription.text,
          "extensionData": {}
        };
        if(optionValue.value!=="")
          optionValues.push(optionValue);
      }
      let sequenceNo = "";
      if(scopeObj.view.tbxAddFeatureSequenceNumber.text!==""){
        sequenceNo = parseInt(scopeObj.view.tbxAddFeatureSequenceNumber.text);
      }
      let newFeatureData=[{
        "type": scopeObj.view.lstBoxAddFeatureType.selectedKey,
        "featureName": scopeObj.view.tbxAddFeatureName.text,
        "featureGroup": scopeObj.view.tbxAddFeatureGroup.text,
        "description": scopeObj.view.txtAreaFeatureDescription.text,
        "sequenceNo": sequenceNo,
        "isMandatory": scopeObj.view.fontIconSmsChannelSelectOption.text===scopeObj._selCheckboxIcon,
        "defaultValue": defaultValue,
        "option": "",
        "optionDispType": optionDispType,
        "optionValues": optionValues,
        "extensionData": {}
      }];
      return newFeatureData;
    },

    mappingViewOptionValues: function(optionValue){
      let mappedOptionValue = {
        "lblOptionValue":{
          "text":optionValue.value,
          "skin": this._commonLabelSkin
        },
        "lblOptionDescription":{
          "text":optionValue.desc,
          "skin": this._commonLabelSkin
        }
      };
      return mappedOptionValue;
    },

    viewFeaturePopup: function(){
      // PRODUCT_FEATURES: View Feature Details Popup
      this.view.flxAddProductPopups.isVisible = true;
      this.view.flxViewFeatureDetailsPopup.isVisible = true;
      this.alignFlxManageProducts();
      let selRow = this.view.segAddedFeatures.selectedRowIndex[1];
      this.selRowFeatureData = this.view.segAddedFeatures.data[selRow];
      this.view.lblViewFeatureName.text = this.selRowFeatureData.featureName;
      this.view.lblFeatureGroupVal.text = this.selRowFeatureData.featureGroup;
      this.view.lblSeqNumVal.text = this.selRowFeatureData.sequenceNo;
      this.view.lblIsMandatoryVal.text = this.selRowFeatureData.isMandatory ? "True" : "False";
      this.view.lblViewDefaultValue.text = this.selRowFeatureData.defaultValue;
      this.view.lblViewDescriptionVal.text = this.selRowFeatureData.description;
      this.view.lblOptionDisplayTypeValue.text = this.selRowFeatureData.optionDispType;
      let dataMap = {
        "flxSegViewFeatureOptions":"flxSegViewFeatureOptions",
        "lblOptionValue":"lblOptionValue",
        "lblOptionDescription":"lblOptionDescription"
      };
      this.view.segViewFeature.widgetDataMap = dataMap;
      this.view.segViewFeature.data = this.selRowFeatureData.optionValues.map(this.mappingViewOptionValues)
    },

    addFeatureDataToSeg: function(newFeatureData){
      // PRODUCT_FEATURES: Add data fetched from AddFeatures Popup to 'segAddedFeatures'
      let segData = this.view.segAddedFeatures.data;
      newFeatureData = newFeatureData.map(this.mappingDataForSegAddedFeatures);
      if(this.isFeatureEdit === true){
        let selRow = this.view.segAddedFeatures.selectedRowIndex[1];
        segData[selRow] = newFeatureData[0];
      } else {
        Array.prototype.push.apply(segData,newFeatureData);
      }
      let dataMap = this.getWidgetDataMapForSegAddedFeatures();
      this.view.segAddedFeatures.widgetDataMap = dataMap;
      this.view.segAddedFeatures.setData(segData);
      this.view.forceLayout();
    },

    getWidgetDataMapForSegAddedFeatures: function(){
      // PRODUCT_FEATURES: Returns widgetDataMap for 'segAddedFeatures'
      let dataMap = {
        "flxProductAddedFeatures":"flxProductAddedFeatures",
        "lblSegFeatureName":"lblSegFeatureName",
        "lblSegFeatureGroup":"lblSegFeatureGroup",
        "lblSegType":"lblSegType",
        "flxOptions":"flxOptions",
        "lblIconOptions":"lblIconOptions"
      };
      return dataMap;
    },

    mappingDataForSegAddedFeatures: function(data){
      // PRODUCT_FEATURES: Maps Data before being populated to 'segAddedFeatures'
      const scopeObj = this;
      let onClickMethod = this.showContextualMenu;
      let optionsLabelText = this._segMoreDetailsIcon;
      let trimCharactersLength = this.view.flxViewSelectedFeatures.frame.width < 1000 ? 28 : 50;
      if(this.isConfig2===false){        
        onClickMethod = (widget, context)=>{
          scopeObj.selRowFeatureData = this.view.segAddedFeatures.data[context.rowIndex];
          scopeObj.editProductFeature();
        };
        optionsLabelText = this._editIcon;
      }
      let mappedRowData = {        
        "lblSegFeatureName": {
          "text": scopeObj.trimSegText(data.featureName, trimCharactersLength),
          "tooltip": data.featureName.length > trimCharactersLength ? data.featureName : ""
        },
        "lblSegFeatureGroup": {
          "text": scopeObj.trimSegText(data.featureGroup, trimCharactersLength),
          "tooltip": data.featureGroup.length > trimCharactersLength ? data.featureGroup : ""
        },
        "lblSegType": {
          "text": scopeObj.trimSegText(data.type, trimCharactersLength),
          "tooltip": data.type.length > trimCharactersLength ? data.type : ""
        },
        "flxOptions":{
          "onClick": onClickMethod
        },
        "lblIconOptions":{
          "text": optionsLabelText
        }
      };
      mappedRowData = Object.assign(mappedRowData, data);
      return mappedRowData;
    },

    trimSegText: function(text,len) {
      let final_text = text ? text : "";
      if (final_text.length > len) final_text = final_text.substr(0, len) + "...";
      return final_text;
    },

    showContextualMenu: function(){
      let left = 0;
      let top = 0;
      let heightTillSegTop = 104;
      let contextualMenuHeight  = 98; // Height of ContextualMenuWidget
      let segmentWidget = this.view.segAddedFeatures;
      let contextualWidget = this.view.flxContextualMenu;
      let segData = segmentWidget.data;
      let selectedIndex = segmentWidget.selectedRowIndex[1];
      let templateArray = segmentWidget.clonedTemplates;

      if (this.prevIndex === -1) {
        this.prevIndex = selectedIndex;
      } else if (this.prevIndex !== -1) {
        segData[this.prevIndex].flxOptions.skin = "sknFlxBorffffff1pxRound";
        segmentWidget.setDataAt(segData[this.prevIndex], this.prevIndex);
        this.prevIndex = selectedIndex;
      }

      for (let i = 0; i < selectedIndex; i++) {
        top += templateArray[i].flxProductAddedFeatures.frame.height;
      }
      this.view.flxArrowImage.setVisibility(true);
      this.view.flxDownArrowImage.setVisibility(false);
      this.view.flxSelectOptionsInner.top = "-1px";
      top = top + 45 - segmentWidget.contentOffsetMeasured.y;
      let maxHeight = this.view.flxProductContainer.frame.height - this.view.flxNavigationBarButtons.frame.height - heightTillSegTop;
      if (top - 10 + contextualMenuHeight > maxHeight) {
        top = top - contextualMenuHeight - 8;
        this.view.flxArrowImage.setVisibility(false);
        this.view.flxDownArrowImage.setVisibility(true);
        this.view.flxSelectOptionsInner.top = "0px"; 
      }
      top = top + heightTillSegTop + "px";
      left = templateArray[selectedIndex].flxOptions.frame.x + 60 - 115 + "px";
      if ((contextualWidget.isVisible === false) || (contextualWidget.isVisible === true && contextualWidget.top !== height)) {
        contextualWidget.top = top;
        contextualWidget.left = left;
        contextualWidget.setVisibility(true);
        contextualWidget.onHover = this.onHoverEventCallback;
      } else {
        contextualWidget.setVisibility(false);
      }
      this.view.forceLayout();
    },

    onHoverEventCallback : function(widget, context) {
      const scopeObj = this;
      let widGetId = widget.id;
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        scopeObj.view[widGetId].setVisibility(true);
      } else if(context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        let segData = this.view.segAddedFeatures.data;
        scopeObj.view[widGetId].setVisibility(false);
        segData = scopeObj.view.segAddedFeatures.data;
        segData[this.prevIndex].flxOptions.skin = "sknFlxBorffffff1pxRound";
      }
    },

    contextualMenuOff: function(context) {
      var scopeObj = this;
      if(scopeObj.view.flxContextualMenu.isVisible){
        scopeObj.view.flxContextualMenu.isVisible = false;
        var Segment =scopeObj.view.segAddedFeatures;
        var segData = Segment.data;
        segData[this.prevIndex].flxOptions.skin = "sknFlxBorffffff1pxRound";
        Segment.setDataAt(segData[this.prevIndex],this.prevIndex);
      }
    },

    populateDataForFeatureEdit: function(){
      const scopeObj = this;
      this.isFeatureEdit = true;
      let featureData = this.selRowFeatureData;
      this.resetTextFieldsAddFeaturePopup();
      this.view.lstBoxAddFeatureType.selectedKey = featureData.type;
      this.view.tbxAddFeatureName.text = featureData.featureName;
      this.view.tbxAddFeatureGroup.text = featureData.featureGroup;      
      this.view.txtAreaFeatureDescription.text = featureData.description;
      this.view.tbxAddFeatureSequenceNumber.text = featureData.sequenceNo;
      this.view.fontIconSmsChannelSelectOption.text = featureData.isMandatory ? scopeObj._selCheckboxIcon : scopeObj._unselCheckboxIcon;
      this.view.lstBoxOptionDisplayType.selectedKey = featureData.optionDispType ? featureData.optionDispType : "SELECT";
      this.view.btnSaveAndClose.text = this._featureUpdateBtnTxt;
      let optionsData = featureData.optionValues;
      let optionsDataMappedData = [];
      for(let index=0; index<optionsData.length; index++){
        let data = this.getNewRowDataSegFeatureOptionValues(false, optionsData[index]);
        if(optionsData[index].value===featureData.defaultValue){
          data.fontIconSelectOptionDefaultValue.text = scopeObj._selCheckboxIcon;
        }
        optionsDataMappedData.push(data);
      }
      this.view.segFeatureOptionValues.setData(optionsDataMappedData);
      if(scopeObj.isConfig2 && optionsDataMappedData.length===0){
        this.setSegFeatureOptionValuesData(); //Initialize single row in segment
      }
    },

    getNewRowDataSegFeatureOptionValues: function(isFirstRow, optionData){
      // PRODUCT_FEATURES: Return mapped row data to create a row in 'segFeatureOptionValues'.
      const scopeObj = this;      
      let value = (optionData && optionData.value) ? optionData.value : "";
      let desc = (optionData && optionData.desc) ? optionData.desc : "";
      let changeIconMethod = scopeObj.changeCheckboxIcon;
      if(scopeObj.isConfig2===false){
        changeIconMethod = null;
      }
      let data = {
        "fontIconSelectOptionDefaultValue":{
          "text": isFirstRow ? scopeObj._selCheckboxIcon : scopeObj._unselCheckboxIcon,
          "skin": scopeObj.isConfig2 ? scopeObj._checkboxSelIconSkin : scopeObj._checkboxUnselIconSkin,
          "onClick": changeIconMethod
        },
        "lblDefaultValue":{
          "text": scopeObj._optionDefaultValueLblTxt,
          "skin": scopeObj._commonLabelSkin,
          "onClick": changeIconMethod
        },
        "tbxValue":{
          "text": value,
          "placeholder": scopeObj._optionValuePlaceholderTxt,
          "skin": scopeObj.isConfig2 ? scopeObj._tbxNormalSkin : scopeObj._tbxDisabledSkin,
          "onEndEditing": scopeObj.hideDefValErrorMsg,
          "enable": scopeObj.isConfig2 ? true : false,
        },
        "flxErrorMessageFeature6":{
          "isVisible": false
        },
        "lblErrorIconFeature6":{
          "text": scopeObj._errorIcon
        },
        "lblErrorTextFeature6":{
          "text": scopeObj._optionDefaultValueErrorMsgTxt
        },
        "lblFeatureDescTextCounter":{
          "text": "0/100",
          "skin": scopeObj._txtCounterLabelSkin,
          "isVisible": false
        },
        "txtAreaFeatureDescription":{
          "text": desc,
          "placeholder": scopeObj._optionDescPlaceholderTxt,
          "skin": scopeObj._tbxAreaNormalSkin,
          "onTextChange": scopeObj.updateTextCount,
          "onEndEditing": scopeObj.hideTextCount
        },        
        "flxDeleteIcon":{
          "onClick": scopeObj.deleteRowSegFeatureOptionValues,
          "isVisible": scopeObj.isConfig2 ? true : false
        },
        "lblIconDelete":{
          "text": scopeObj._deleteIcon,
          "skin": scopeObj._deleteIconSkin
        }
      };
      return data;
    },

    setSegFeatureOptionValuesData: function(){
      // PRODUCT_FEATURES: Add a row to 'segFeatureOptionValues' for the FIRST time onPreShow
      let dataMap = {
        "flxSegProductFeatureOption":"flxSegProductFeatureOption",
        "flxValueContainer":"flxValueContainer",
        "flxDescriptionContainer":"flxDescriptionContainer",
        "flxDeleteIcon":"flxDeleteIcon",
        "fontIconSelectOptionDefaultValue":"fontIconSelectOptionDefaultValue",
        "lblDefaultValue":"lblDefaultValue",
        "tbxValue":"tbxValue",
        "flxErrorMessageFeature6":"flxErrorMessageFeature6",
        "lblErrorIconFeature6":"lblErrorIconFeature6",
        "lblErrorTextFeature6":"lblErrorTextFeature6",
        "lblFeatureDescTextCounter":"lblFeatureDescTextCounter",
        "txtAreaFeatureDescription":"txtAreaFeatureDescription",
        "lblIconDelete":"lblIconDelete"
      };
      this.view.segFeatureOptionValues.data = [];
      let data = this.getNewRowDataSegFeatureOptionValues(true,null);
      this.view.segFeatureOptionValues.widgetDataMap = dataMap;
      this.view.segFeatureOptionValues.setData([data]);
      this.view.forceLayout();
    },

    addOptionValueRow: function(){
      // PRODUCT_FEATURES: Add row to 'segFeatureOptionValues' onClick of btnAddValue
      let segData = this.view.segFeatureOptionValues.data;
      if(segData.length===1){
        segData[0].flxDeleteIcon.isVisible = true;
      }
      let newRowData = this.getNewRowDataSegFeatureOptionValues(false,null);
      segData.push(newRowData);
      this.view.segFeatureOptionValues.setData(segData);
      this.view.forceLayout();
    },

    deleteRowSegFeatureOptionValues: function(widget){
      // PRODUCT_FEATURES: Delete Row in segFeatureOptionValues
      let data = this.view.segFeatureOptionValues.data;
      let rowIndex = this.view.segFeatureOptionValues.selectedRowIndex[1];
      if (data.length === 1) {
        data[0].flxDeleteIcon.isVisible = false;
        data[0].tbxValue.text = "";
        data[0].txtAreaFeatureDescription.text = "";
        this.view.segFeatureOptionValues.setData(data);
      } else if(data.length === 2){
        this.view.segFeatureOptionValues.removeAt(rowIndex);
        data = this.view.segFeatureOptionValues.data;
        data[0].flxDeleteIcon.isVisible = false;
        this.view.segFeatureOptionValues.setData(data);
      } else {
        this.view.segFeatureOptionValues.removeAt(rowIndex);
      }
      this.view.forceLayout();
    },

    changeCheckboxIcon: function(widget){
      // PRODUCT_FEATURES: Check or Uncheck checkbox for mandatory 
      let segData = this.view.segFeatureOptionValues.data;
      let rowIndex = this.view.segFeatureOptionValues.selectedRowIndex[1];
      for(let segRow=0; segRow<segData.length; segRow++){
        if(segRow===rowIndex){          
          segData[segRow].fontIconSelectOptionDefaultValue.text = this._selCheckboxIcon;
        } else {
          segData[segRow].fontIconSelectOptionDefaultValue.text = this._unselCheckboxIcon;
        }
      }
      this.view.segFeatureOptionValues.setData(segData);
    },

    hideDefValErrorMsg: function(widget){
      // PRODUCT_FEATURES
      let segData = this.view.segFeatureOptionValues.data;
      let rowIndex = this.view.segFeatureOptionValues.selectedRowIndex[1];
      segData[rowIndex].tbxValue.skin = this._tbxNormalSkin;
      segData[rowIndex].flxErrorMessageFeature6.isVisible = false;
      this.view.segFeatureOptionValues.setData(segData);
    },

    updateTextCount: function(widget){
      // PRODUCT_FEATURES
      let segData = this.view.segFeatureOptionValues.data;
      let rowIndex = this.view.segFeatureOptionValues.selectedRowIndex[1];
      segData[rowIndex].lblFeatureDescTextCounter.text = segData[rowIndex].txtAreaFeatureDescription.text.length + "/100";
      segData[rowIndex].lblFeatureDescTextCounter.isVisible = true;
      this.view.segFeatureOptionValues.setData(segData);
      this.view.forceLayout();
    },

    hideTextCount: function(widget){
      // PRODUCT_FEATURES
      let segData = this.view.segFeatureOptionValues.data;
      let rowIndex = this.view.segFeatureOptionValues.selectedRowIndex[1];
      segData[rowIndex].lblFeatureDescTextCounter.isVisible = false;
      this.view.segFeatureOptionValues.setData(segData);
      this.view.forceLayout();
    },

    sortFeatures: function(sortIconWidget){
      // PRODUCT_FEATURES: Sort Features in 'segAddedFeatures'.
      let segData = this.view.segAddedFeatures.data;
      let firstVal = -1;
      let secVal = 1;
      let isAscending = false;
      let iconText = sortIconWidget.text;            
      this.view.fontIconSortFeatureName.text = this._sortUpDownIcon;
      this.view.fontIconSortFeatureGroup.text = this._sortUpDownIcon;
      this.view.fontIconSortType.text = this._sortUpDownIcon;
      if (iconText === this._sortUpDownIcon || iconText === this._downArrowIcon) {
        isAscending = true;
        sortIconWidget.text = this._upArrowIcon;
      } else {
        isAscending = false;
        sortIconWidget.text = this._downArrowIcon;
      }            
      if (!isAscending) {
        firstVal = 1;
        secVal = -1;
      }
      let colName = this.sortColName;
      function compare(obj1, obj2) {
        let val1 = obj1[colName];
        let val2 = obj2[colName];
        if (val1 < val2) return firstVal;
        else if (val1 > val2) return secVal;
        else return 0;
      }
      segData.sort(compare);
      this.view.segAddedFeatures.setData(segData);
    },

    getNewRowDataSegImageDetails: function(editFlowImageData){
      // IMAGE_DETAILS: Returns mapped row data for an Empty row in 'segImageDetails'.
      const scopeObj = this;
      let selectedImageType = editFlowImageData ? editFlowImageData.imageType : "SELECT";
      let imageURL = editFlowImageData ? editFlowImageData.imageUrl : "";
      let data = {
        "lblImgType":{
          "text": scopeObj._imageTypeLblTxt,
          "skin": scopeObj._commonLabelSkin
        }, 
        "lstBoxImageType":{
          "masterdata": scopeObj.imageTypesMasterdata,
          "selectedKey": selectedImageType,          
          "skin": scopeObj._lstboxNormalSkin      
        },
        "lblImgURL":{
          "text": scopeObj._imageURLLblTxt,
          "skin": scopeObj._commonLabelSkin
        },
        "lblFontIconInformation":{
          "text": scopeObj._tooltipInfoIcon,
          "onHover": scopeObj.adjustToolTip
        },
        "tbxImageURL":{
          "text": imageURL,
          "placeholder": scopeObj._imageURLPlaceholderTxt,
          "skin": scopeObj._tbxNormalSkin,
          "onEndEditing": scopeObj.hideImageURLErrorMsg
        },
        "flxErrorMsgURL":{
          "isVisible":false
        },
        "lblErrorIcon":{
          "isVisible":true,
          "text":scopeObj._errorIcon,
          "skin":scopeObj._errorMsgIconSkin
        },
        "lblErrorText":{
          "isVisible":true,
          "text": "Not a valid image URL.",
          "skin": scopeObj._errorMsgTextSkin
        },
        "flxDeleteIcon":{
          "isVisible": true,
          "onClick": scopeObj.deleteRowSegImageDetails
        },
        "lblIconDelete":{
          "text": scopeObj._deleteIcon,
          "skin":scopeObj._deleteIconSkin
        }
      };
      return data;
    },

    getDataMapForSegImageDetails: function(){
      // IMAGE_DETAILS: Returns widgetDataMap for 'segImageDetails'
      let dataMap = {
        "flxSegImageDetails": "flxSegImageDetails",
        "flxImageType": "flxImageType",
        "lblImgType": "lblImgType",
        "lstBoxImageType": "lstBoxImageType",
        "flxImageURL": "flxImageURL",
        "flxImageURLTop": "flxImageURLTop",
        "lblImgURL": "lblImgURL",
        "lblFontIconInformation": "lblFontIconInformation",
        "tbxImageURL": "tbxImageURL",
        "flxErrorMsgURL":"flxErrorMsgURL",
        "lblErrorIcon": "lblErrorIcon",
        "lblErrorText": "lblErrorText",
        "flxDeleteIcon": "flxDeleteIcon",
        "lblIconDelete": "lblIconDelete"
      };
      return dataMap;
    },

    setSegImageDetailsData: function(){
      // IMAGE_DETAILS: Add a row to 'segImageDetails' for the FIRST time during resetUI.
      this.view.segImageDetails.data = [];
      let data = this.getNewRowDataSegImageDetails(null);
      this.view.segImageDetails.widgetDataMap = this.getDataMapForSegImageDetails();
      this.view.segImageDetails.setData([data]);
      this.view.forceLayout();
    },

    hideImageURLErrorMsg: function(widget, context){
      let data = this.view.segImageDetails.data;
      let rowIndex = this.view.segImageDetails.selectedRowIndex[1];
      data[rowIndex].tbxImageURL.skin = this._tbxNormalSkin;
      data[rowIndex].flxErrorMsgURL.isVisible = false;
      this.view.segImageDetails.setData(data);
      this.view.forceLayout();
    },

    addImageDetailsRow: function(){
      // IMAGE_DETAILS: Add row to 'segImageDetails' onClick of btnAddImage
      let segData = this.view.segImageDetails.data;
      if(segData.length===1){
        segData[0].flxDeleteIcon.isVisible = true;
      }
      let newRowData = this.getNewRowDataSegImageDetails(null);
      segData.push(newRowData);
      this.view.segImageDetails.setData(segData);
      this.view.forceLayout();
    },

    deleteRowSegImageDetails: function(widget){
      // IMAGE_DETAILS: Delete Segment Row
      let data = this.view.segImageDetails.data;
      let rowIndex = this.view.segImageDetails.selectedRowIndex[1];
      if (data.length === 1) {
        data[0].flxDeleteIcon.isVisible = false;
        data[0].lstBoxImageType.selectedKey = "SELECT";
        data[0].tbxImageURL.text = "";
        this.view.segImageDetails.setData(data);
      } else if(data.length === 2){
        this.view.segImageDetails.removeAt(rowIndex);
        data = this.view.segImageDetails.data;
        data[0].flxDeleteIcon.isVisible = false;
        this.view.segImageDetails.setData(data);
      } else {
        this.view.segImageDetails.removeAt(rowIndex);
      }
      this.view.forceLayout();
    },

    adjustToolTip: function(widget, context){
      let segData = this.view.segImageDetails.data;
      let selRowIndex = context.rowIndex;
      let selectedImageType = segData[selRowIndex].lstBoxImageType.selectedKey;
      let toolTipWidget = this.view.ToolTip;
      let segmentWidget = this.view.segImageDetails;
      let tooltipText = "";
      let imgHeight=720;
      let imgwWidth=120;
      let reqImageType = this._imageTypes;
      if(selectedImageType==="SELECT"){
        toolTipWidget.setVisibility(false);
      } else {
        toolTipWidget.setVisibility(true);
        reqImageType = reqImageType.filter(item => item.imageTypeBackend===selectedImageType);
        imgwWidth = reqImageType[0].width;
        imgHeight = reqImageType[0].height;
        tooltipText = reqImageType[0].imageTypeDisplay + " Image should be " + imgwWidth + "x" + imgHeight + " Px";
        toolTipWidget.lblNoConcentToolTip.text = tooltipText!=="" ? tooltipText : "";
        let height = 20; //Height till segment top 
        let segRowHeight = 0;
        let templateArray = segmentWidget.clonedTemplates;
        for (let rowIndex = 0; rowIndex < selRowIndex; rowIndex++) {
          segRowHeight += templateArray[rowIndex].flxSegImageDetails.frame.height;
        }        
        height = height + segRowHeight - this.view.flxImgDetailsScrollContainer.contentOffsetMeasured.y;
        toolTipWidget.lblarrow.setVisibility(true);
        toolTipWidget.lblDownArrow.setVisibility(false);
        toolTipWidget.flxToolTipMessage.top = "9px";
        if(height + toolTipWidget.frame.height > this.view.flxAddProductMain.frame.height){
          height = height - toolTipWidget.frame.height - 5;
          toolTipWidget.lblarrow.setVisibility(false);
          toolTipWidget.lblDownArrow.setVisibility(true);
          toolTipWidget.flxToolTipMessage.top = "0px";
          toolTipWidget.lblDownArrow.top = "24px";
        }
        height = height + "px";
        if (tooltipText!=="") {
          toolTipWidget.top = height;
          toolTipWidget.left = templateArray[selRowIndex].flxSegImageDetails.frame.x - 361 + "px";;
          toolTipWidget.setVisibility(true);
          toolTipWidget.onHover = this.onHoverToolTipEventCallback;
        } else {
          toolTipWidget.setVisibility(false);
        }
      }
      this.view.forceLayout();
    },

    onHoverToolTipEventCallback: function(widget, context){
      const scopeObj = this;
      let widGetId = widget.id;
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        this.view.ToolTip.setVisibility(true);
      } else if(context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        this.view.ToolTip.setVisibility(false);       
      }
    },

    getNewRowDataSegAddAttribute: function(editFlowAddAttrData){
      // ADDITIONAL_ATTRIBUTES: Returns Mapped data for an Empty row in 'segAddAttribute'.
      const scopeObj = this;
      let key = editFlowAddAttrData ? editFlowAddAttrData.key : "";
      let val = editFlowAddAttrData ? editFlowAddAttrData.value : "";
      let data = {
        "tbxAttribute":{
          "text": key,
          "placeholder": scopeObj._attributeKeyPlaceholderTxt,
          "skin": scopeObj._addAttrTextBoxSkin
        },
        "tbxAttributeValue":{
          "text": val,
          "placeholder": scopeObj._attributeValuePlaceholderTxt,
          "skin": scopeObj._addAttrTextBoxSkin
        },
        "flxDeleteIcon":{
          "isVisible": true,
          "onClick": scopeObj.deleteRowSegAdditionalAttributes
        },
        "lblIconDelete":{
          "isVisible": true,
          "text":scopeObj._deleteIcon,
          "skin":scopeObj._deleteIconSkin
        }
      };
      return data;
    },

    getDataMapForSegAddAttribute: function(){
      // ADDITIONAL_ATTRIBUTES: Returns widgetDataMap for 'segAddAttribute'.
      let dataMap = {
        "flxSegAddAttribute":"flxSegAddAttribute",
        "flxAttribute":"flxAttribute",
        "tbxAttribute":"tbxAttribute",
        "flxAttributeValue":"flxAttributeValue",
        "tbxAttributeValue":"tbxAttributeValue",
        "flxDeleteIcon":"flxDeleteIcon",
        "lblIconDelete":"lblIconDelete"
      };
      return dataMap;
    },

    setSegAddAttributeData: function(){
      // ADDITIONAL_ATTRIBUTES: Add a row to 'segAddAttribute' for the FIRST time during resetUI.
      let data = this.getNewRowDataSegAddAttribute(null);
      this.view.segAddAttribute.widgetDataMap = this.getDataMapForSegAddAttribute();
      this.view.segAddAttribute.setData([data]);
      this.view.forceLayout();
    },

    addAdditionalAttributesRow: function(){
      // ADDITIONAL_ATTRIBUTES: Add row to 'segAddAttribute' onClick of btnAddAttribute
      let segData = this.view.segAddAttribute.data;
      this._maxAddAttrAllowed = parseInt(this._maxAddAttrAllowed);
      if(segData.length<this._maxAddAttrAllowed){
        if(segData.length===1){
          segData[0].flxDeleteIcon.isVisible = true;
        }
        let newRowData = this.getNewRowDataSegAddAttribute(null);
        segData.push(newRowData);
        this.view.segAddAttribute.setData(segData);
      }
      if(this.view.segAddAttribute.data.length===this._maxAddAttrAllowed){
        this.view.btnAddAttribute.setVisibility(false);
      }
      this.view.forceLayout();
    },

    deleteRowSegAdditionalAttributes: function(){
      // ADDITIONAL_ATTRIBUTES: Delete Segment Row for 'segAddAttribute'
      let data = this.view.segAddAttribute.data;
      let rowIndex = this.view.segAddAttribute.selectedRowIndex[1];
      if (data.length === 1) {
        data[0].flxDeleteIcon.isVisible = false;
        data[0].tbxAttribute.text = "";
        data[0].tbxAttributeValue.text = "";
        this.view.segAddAttribute.setData(data);
      } else if(data.length === 2){
        this.view.segAddAttribute.removeAt(rowIndex);
        data = this.view.segAddAttribute.data;
        data[0].flxDeleteIcon.isVisible = false;
        this.view.segAddAttribute.setData(data);
      } else {
        this.view.segAddAttribute.removeAt(rowIndex);
      }
      if(data.length<10){
        this.view.btnAddAttribute.setVisibility(true);
      }
      this.view.forceLayout();
    },

    addCustomDateWidget: function(){
      // Functionality to add Custom Date Widget Dynamically, since custom widgets cannot be dropped in Component
      let startDateWidget = this.createDateField("customDateField", 1);
      let endDateWidget = this.createDateField("customDateField", 2);
      if(this.view.flxProductDetailsContainer5.children.includes('flxCustomProductDate1'))
        this.view.flxProductDetailsContainer5.remove(this.view.flxCustomProductDate1);
      if(this.view.flxProductDetailsContainer6.children.includes('flxCustomProductDate2'))
        this.view.flxProductDetailsContainer6.remove(this.view.flxCustomProductDate2);
      this.view.flxProductDetailsContainer5.addAt(startDateWidget, 1);
      this.view.flxProductDetailsContainer6.addAt(endDateWidget, 1);
      this.view.forceLayout();
    },

    createDateField: function(idString, id){
      // Returns dynamically created Custom Date Field Widget.
      var flxCustomProductDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxCustomProductDate"+id,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxCalendar",
        "top": "15dp",
        "width": "100%"
      }, {}, {});
      flxCustomProductDate.setDefaultUnit(kony.flex.DP);
      var customProductDate = new kony.ui.CustomWidget({
        "id": idString+id,
        "isVisible": true,
        "left": "0px",
        "right": "0px",
        "bottom": "0px",
        "top": "5px",
        "width": "100%",
        "height": "40px",
        "zIndex": 10
      }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
      }, {
        "widgetName": "dateRangePicker",
        "drops": "up",
        "event": null,
        "rangeType": "",
        "resetData": null,
        "type": "single",
        "value": ""
      });
      flxCustomProductDate.add(customProductDate);
      this.view.forceLayout();
      return flxCustomProductDate;
    },

    alignFlxManageProducts: function() {
      // To be invoked when showing the popups on complete screen.
      if(this.alignPopups){        
        this.view.flxAddProductMainContainer.left = "340px";
        this.view.flxAddProductMainContainer.right = "35px";
        this.view.flxAddProductMainContainer.top = "150px";
        this.view.btnAddFeature1.right = "10px";
        this.view.forceLayout();
        this.alignPopups(true);
      }
      this.view.forceLayout();
    },

    closePopup: function() {
      // To be invoked when hiding the popups.
      this.view.flxAddFeaturePopup.setVisibility(false);
      this.view.flxAddProductPopups.setVisibility(false);
      this.view.flxViewFeatureDetailsPopup.setVisibility(false);
      if(this.alignPopups)
        this.alignPopups(false);
      this.view.flxAddProductMainContainer.left = "0px";
      this.view.flxAddProductMainContainer.right = "0px";
      this.view.flxAddProductMainContainer.top = "0px";      
      this.view.btnAddFeature1.right = "10px";
      this.view.forceLayout();
    },

    getMappedFeaturesData: function(featureData){
      let mappedFeatureData = {
        "type": featureData.type,
        "featureName": featureData.featureName,
        "featureGroup": featureData.featureGroup,
        "description": featureData.description,
        "sequenceNo": featureData.sequenceNo,
        "isMandatory": featureData.isMandatory,
        "defaultValue": featureData.defaultValue,
        "option": featureData.option,
        "optionDispType": featureData.optionDispType,
        "optionValues": featureData.optionValues,
        "extensionData": featureData.extensionData
      };
      return mappedFeatureData;
    },

    getMappedImageDetailsData: function(imageDetailsData){
      let imageType = imageDetailsData.lstBoxImageType.selectedKey;
      let height=720, width=120; // Giving Default values
      let reqImageType = this._imageTypes;
      reqImageType = reqImageType.filter(item => item.imageTypeBackend===imageType);
      height = parseInt(reqImageType[0].height);
      width = parseInt(reqImageType[0].width);
      let mappedImageDetailsData = {
        "imageType": imageType,
        "height": height,
        "width": width,
        "imageUrl": imageDetailsData.tbxImageURL.text,
        "extensionData": {}
      };
      return mappedImageDetailsData;
    },

    fetchFinalProductFeatures: function(){
      let productFeatures = [];
      let segProductFeaturesData = this.view.segAddedFeatures.data;
      if(segProductFeaturesData!==[] && segProductFeaturesData!=="" && segProductFeaturesData!==undefined && segProductFeaturesData!==null){
        productFeatures = segProductFeaturesData.map(this.getMappedFeaturesData);
      }
      return productFeatures;
    },

    fetchFinalImageDetails: function(){
      let imageDetails = [];
      let segimageDetailsData = this.view.segImageDetails.data;
      // Filtering data for Rmpty Rows
      segimageDetailsData = segimageDetailsData.filter(item => {
        if(item.lstBoxImageType.selectedKey==="SELECT" || item.tbxImageURL.text==="") return false;
        else return true;
      });
      if(segimageDetailsData.length!==0 && segimageDetailsData!=="" && segimageDetailsData!==null && segimageDetailsData!==undefined){
        imageDetails = segimageDetailsData.map(this.getMappedImageDetailsData);
      }
      return imageDetails;
    },

    fetchFinalAdditionalAttributes: function(){
      let additionalAttributes = [];
      let additioanAttributesData = this.view.segAddAttribute.data;
      // Filtering Data for Empty Rows
      additioanAttributesData = additioanAttributesData.filter(item => {
        if(item.tbxAttribute.text==="" || item.tbxAttributeValue.text==="") return false;
        else return true;
      });
      if(additioanAttributesData.length!==0 && additioanAttributesData!=="" && additioanAttributesData!==null && additioanAttributesData!==undefined){
        additionalAttributes = additioanAttributesData.map(attribute => {
          return {"key":attribute.tbxAttribute.text, "value":attribute.tbxAttributeValue.text};
        });
      }
      return additionalAttributes;
    },

    fetchProductDetails: function(){
      // TODO: Need to be changed after Demo
      // Need to be configured according to requirements
      const scopeObj = this;
      let productDetails = [];
      if(scopeObj.view.tbxAdditionalField1.text==="" || scopeObj.view.tbxAdditionalField2.text==="" || scopeObj.view.tbxAdditionalField3.text===""){
        productDetails = [];
      } else {
        productDetails = [{
          "currency": scopeObj.view.tbxAdditionalField1.text,
          "debitInterests": [
            {
              "type": scopeObj.view.tbxAdditionalField2.text,
              "periodicIndex": scopeObj.view.tbxAdditionalField3.text
            }
          ],
          "creditInterests": [
            {
              "type": scopeObj.view.tbxAdditionalField2.text,
              "periodicIndex": scopeObj.view.tbxAdditionalField3.text
            }
          ]
        }];
      }
      return productDetails;
    },

    fetchFinalAddProductJSON: function(){
      // Returns final JSON to be sent as PAYLOAD for Creating a Product.
      const scopeObj = this;
      let startDate="";
      let endDate="";
      if(scopeObj.isConfig2===false){
        startDate = this.view.lblStartDate.text.replace(/\//g, "-").split("-");
        startDate = startDate[2] + "-" + startDate[0] + "-" + startDate[1];
        endDate = this.view.lblEndDate.text.replace(/\//g, "-").split("-");
        endDate = endDate[2] + "-" + endDate[0] + "-" + endDate[1];
      } else if(this.view.customDateField1.value!=="" || this.view.customDateField2.value!==""){
        startDate = startDate = this.view.customDateField1.value.replace(/\//g, "-").split("-");
        startDate = startDate[2] + "-" + startDate[0] + "-" + startDate[1];
        endDate = this.view.customDateField2.value.replace(/\//g, "-").split("-");
        endDate = endDate[2] + "-" + endDate[0] + "-" + endDate[1];
      }
      let segPurposesData = this.view.segProductPurpose.selectedRowItems;
      let purposes = [];
      if(segPurposesData!=="" && segPurposesData!==null && segPurposesData!==undefined && segPurposesData.length!==0){
        purposes = segPurposesData.map(purpose => {
          return {"type": purpose.lblDescription.text};
        });
      }
      let productFeatures = this.fetchFinalProductFeatures();
      let imageDetails = this.fetchFinalImageDetails();
      let additionalAttributes = this.fetchFinalAdditionalAttributes();
      // let productDetails = this.fetchProductDetails();
      let finalObj = {
        "productName": scopeObj.view.tbxProductName.text,
        "productLine": scopeObj.view.lstboxProductLine.selectedKey,
        "productGroup": scopeObj.view.lstboxProductGroup.selectedKey,
        "productRef": scopeObj.view.tbxProductReference.text,
        "branchRef":"GB0010001",  
        "externalIndicator":this._externalIndicator,
        "availableFrom": startDate,
        "availableTo": endDate,
        "purposes": purposes,
        "description": scopeObj.view.txtAreaDescription.text,
        "detailedDesc": scopeObj.view.txtAreaDetailedDescription.text,
        "notes": scopeObj.view.txtAreaNotes.text,
        "termsConditions": scopeObj.view.txtAreaTermsAndConditions.text,
        "disclosure": scopeObj.view.txtAreaDisclosure.text,        
        "productFeatures": productFeatures,
        "imageDetails": imageDetails,
        "extensionData": additionalAttributes     
      };
      return finalObj;
    },

    willUpdateUI: function(context){
      const scopeObj = this;
      if(context){
        if(context.progressBar){
          scopeObj.formUpdateUI(context);
        }
        if(context.toastModel){
          scopeObj.formUpdateUI(context);
        }
        if(context.action){
          if(context.action === scopeObj._operationName1){
            scopeObj.fetchProductLinesSuccess(context);
          } 
          else if(context.action === scopeObj._operationName2){
            scopeObj.fetchProductGroupsSuccess(context);            
          } 
          else if(context.action === scopeObj._operationName3){
            scopeObj.fetchProductFeaturesSuccess(context);
          }
          else if(context.action === scopeObj._operationName4 || context.action === scopeObj._operationName5){
            scopeObj.createEditProductSuccess(context);
          }
        }
      }
    },

    fetchProductLinesSuccess: function(context){
      const scopeObj = this;
      let productLinesData = context.productLines;
      productLinesData = productLinesData.map(item => [item.productLineRef,item.productLineName || "N/A"]);
      productLinesData.unshift(["SELECT",scopeObj._productLineDropdownPlaceholderTxt]);
      scopeObj.productLinesMasterData = productLinesData;
      scopeObj.view.lstboxProductLine.masterData = scopeObj.productLinesMasterData;
      scopeObj.view.lstboxProductLine.selectedKey = this.isEditFlow ? this.editDataGlobal.productLineRef : "SELECT";
      if(this.isEditFlow && this.areProductLinesFetched===false) {
        this.areProductLinesFetched = true;
        this.fetchProductGroupsByProductLine();
      }
    },

    fetchProductGroupsSuccess: function(context){
      const scopeObj = this;
      let productGroupsData = context.productGroups;
      if(this.isEditFlow){
        let productLine = this.editDataGlobal.productLineRef;
        productGroupsData = productGroupsData.filter(item => item.productLineRef === productLine);
        productGroupsData = productGroupsData.map(item => [item.productGroupRef,item.productGroupName || "N/A"]);
        productGroupsData.unshift(["SELECT",scopeObj._productGroupDropdownPlaceholderTxt]);
        scopeObj.productGroupsMasterData = productGroupsData;
        scopeObj.view.lstboxProductGroup.masterData = scopeObj.productGroupsMasterData;
        scopeObj.view.lstboxProductGroup.selectedKey = this.editDataGlobal.productGroupRef;
      } else {
        let productLine = scopeObj.view.lstboxProductLine.selectedKey;
        productGroupsData = productGroupsData.filter(item => item.productLineRef === productLine);
        productGroupsData = productGroupsData.map(item => [item.productGroupRef,item.productGroupName || "N/A"]);
        productGroupsData.unshift(["SELECT",scopeObj._productGroupDropdownPlaceholderTxt]);
        scopeObj.productGroupsMasterData = productGroupsData;
        scopeObj.view.lstboxProductGroup.masterData = scopeObj.productGroupsMasterData;
        scopeObj.view.lstboxProductGroup.selectedKey = "SELECT";
      }
    },

    fetchProductFeaturesSuccess: function(context){
      const scopeObj = this;
      let featureTypeData = context.FeatureType;
      for(let feature of featureTypeData){
        let key = feature.featureType;
        let val = [feature.featureName, feature.featureGroup];
        scopeObj.allFeatureType_NameGroupMap.set(key, val);
      }
      let existingFeatureNames = featureTypeData.map(item => item.featureName);
      existingFeatureNames = [...new Set(existingFeatureNames)];
      let featureTypes = featureTypeData.map(item => item.featureType);
      featureTypes = [...new Set(featureTypes)];
      featureTypes = featureTypes.map(item => [item,item || "N/A"]);
      featureTypes.unshift(["SELECT",scopeObj._featureTypeDropdownPlaceholderTxt]);
      scopeObj.featureTypeMasterData = featureTypes;
      scopeObj.view.lstBoxAddFeatureType.masterData = scopeObj.featureTypeMasterData;
      scopeObj.view.lstBoxAddFeatureType.selectedKey = "SELECT";
    },

    createEditProductSuccess: function(context){
      const scopeObj = this;
      scopeObj.resetUI(false, null);
      if(scopeObj.closeAddProductComponent) scopeObj.closeAddProductComponent();
    },

    fetchProductLines: function(){
      this.AddProductDAO.fetchProductInformation(this._objectServiceName1,this._objectName1,this._operationName1,"",this.willUpdateUI,null);
    },

    fetchProductGroupsByProductLine: function(){
      this.AddProductDAO.fetchProductInformation(this._objectServiceName2,this._objectName2,this._operationName2,"",this.willUpdateUI,null);
    },

    fetchProductFeatures: function(){
      this.AddProductDAO.fetchProductInformation(this._objectServiceName3,this._objectName3,this._operationName3,"",this.willUpdateUI,null);
    },

    createProduct: function(payload){
      let isCreateEditJSON = {"isCreateProduct":true,"isEditProduct":false};
      this.AddProductDAO.fetchProductInformation(this._objectServiceName4,this._objectName4,this._operationName4,payload,this.willUpdateUI,isCreateEditJSON);
    },

    editProduct: function(payload){
      let isCreateEditJSON = {"isCreateProduct":false,"isEditProduct":true};
      this.AddProductDAO.fetchProductInformation(this._objectServiceName5,this._objectName5,this._operationName5,payload,this.willUpdateUI, isCreateEditJSON);
    },

  };
});