define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for txtDescription **/
    AS_TextArea_c64086f11dcc487a8444bdf0a18e9a02: function AS_TextArea_c64086f11dcc487a8444bdf0a18e9a02(eventobject, x, y) {
        var self = this;
        return self.onDescriptionTouch.call(this, null);
    },
    /** onKeyUp defined for txtDescription **/
    AS_TextArea_c708c44f39cf4c0b82b43ff1b7e72655: function AS_TextArea_c708c44f39cf4c0b82b43ff1b7e72655(eventobject) {
        var self = this;
        return self.onDescriptionKeyUp.call(this, null);
    }
});