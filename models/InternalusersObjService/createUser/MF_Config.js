/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"AddressLine1": "AddressLine1",
		"AddressLine2": "AddressLine2",
		"City_id": "City_id",
		"currUser": "currUser",
		"Email": "Email",
		"FirstName": "FirstName",
		"LastName": "LastName",
		"MiddleName": "MiddleName",
		"permission_ids": "permission_ids",
		"Region_id": "Region_id",
		"role_id": "role_id",
		"Status_id": "Status_id",
		"Username": "Username",
		"vizServerURL": "vizServerURL",
		"WorkID": "WorkID",
		"ZipCode": "ZipCode",
	};

	Object.freeze(mappings);

	var typings = {
		"AddressLine1": "string",
		"AddressLine2": "string",
		"City_id": "string",
		"currUser": "string",
		"Email": "string",
		"FirstName": "string",
		"LastName": "string",
		"MiddleName": "string",
		"permission_ids": "string",
		"Region_id": "string",
		"role_id": "string",
		"Status_id": "string",
		"Username": "string",
		"vizServerURL": "string",
		"WorkID": "string",
		"ZipCode": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"Username",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "InternalusersObjService",
		tableName: "createUser"
	};

	Object.freeze(config);

	return config;
})