/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "createUser", "objectService" : "InternalusersObjService"};

    var setterFunctions = {
        AddressLine1: function(val, state) {
            context["field"] = "AddressLine1";
            context["metadata"] = (objectMetadata ? objectMetadata["AddressLine1"] : null);
            state['AddressLine1'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        AddressLine2: function(val, state) {
            context["field"] = "AddressLine2";
            context["metadata"] = (objectMetadata ? objectMetadata["AddressLine2"] : null);
            state['AddressLine2'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        City_id: function(val, state) {
            context["field"] = "City_id";
            context["metadata"] = (objectMetadata ? objectMetadata["City_id"] : null);
            state['City_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        currUser: function(val, state) {
            context["field"] = "currUser";
            context["metadata"] = (objectMetadata ? objectMetadata["currUser"] : null);
            state['currUser'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Email: function(val, state) {
            context["field"] = "Email";
            context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
            state['Email'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        FirstName: function(val, state) {
            context["field"] = "FirstName";
            context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
            state['FirstName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        LastName: function(val, state) {
            context["field"] = "LastName";
            context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
            state['LastName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        MiddleName: function(val, state) {
            context["field"] = "MiddleName";
            context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
            state['MiddleName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        permission_ids: function(val, state) {
            context["field"] = "permission_ids";
            context["metadata"] = (objectMetadata ? objectMetadata["permission_ids"] : null);
            state['permission_ids'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Region_id: function(val, state) {
            context["field"] = "Region_id";
            context["metadata"] = (objectMetadata ? objectMetadata["Region_id"] : null);
            state['Region_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        role_id: function(val, state) {
            context["field"] = "role_id";
            context["metadata"] = (objectMetadata ? objectMetadata["role_id"] : null);
            state['role_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Status_id: function(val, state) {
            context["field"] = "Status_id";
            context["metadata"] = (objectMetadata ? objectMetadata["Status_id"] : null);
            state['Status_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Username: function(val, state) {
            context["field"] = "Username";
            context["metadata"] = (objectMetadata ? objectMetadata["Username"] : null);
            state['Username'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        vizServerURL: function(val, state) {
            context["field"] = "vizServerURL";
            context["metadata"] = (objectMetadata ? objectMetadata["vizServerURL"] : null);
            state['vizServerURL'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        WorkID: function(val, state) {
            context["field"] = "WorkID";
            context["metadata"] = (objectMetadata ? objectMetadata["WorkID"] : null);
            state['WorkID'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        ZipCode: function(val, state) {
            context["field"] = "ZipCode";
            context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
            state['ZipCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function createUser(defaultValues) {
        var privateState = {};
        context["field"] = "AddressLine1";
        context["metadata"] = (objectMetadata ? objectMetadata["AddressLine1"] : null);
        privateState.AddressLine1 = defaultValues ?
            (defaultValues["AddressLine1"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["AddressLine1"], context) :
                null) :
            null;

        context["field"] = "AddressLine2";
        context["metadata"] = (objectMetadata ? objectMetadata["AddressLine2"] : null);
        privateState.AddressLine2 = defaultValues ?
            (defaultValues["AddressLine2"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["AddressLine2"], context) :
                null) :
            null;

        context["field"] = "City_id";
        context["metadata"] = (objectMetadata ? objectMetadata["City_id"] : null);
        privateState.City_id = defaultValues ?
            (defaultValues["City_id"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["City_id"], context) :
                null) :
            null;

        context["field"] = "currUser";
        context["metadata"] = (objectMetadata ? objectMetadata["currUser"] : null);
        privateState.currUser = defaultValues ?
            (defaultValues["currUser"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["currUser"], context) :
                null) :
            null;

        context["field"] = "Email";
        context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
        privateState.Email = defaultValues ?
            (defaultValues["Email"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Email"], context) :
                null) :
            null;

        context["field"] = "FirstName";
        context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
        privateState.FirstName = defaultValues ?
            (defaultValues["FirstName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["FirstName"], context) :
                null) :
            null;

        context["field"] = "LastName";
        context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
        privateState.LastName = defaultValues ?
            (defaultValues["LastName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["LastName"], context) :
                null) :
            null;

        context["field"] = "MiddleName";
        context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
        privateState.MiddleName = defaultValues ?
            (defaultValues["MiddleName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["MiddleName"], context) :
                null) :
            null;

        context["field"] = "permission_ids";
        context["metadata"] = (objectMetadata ? objectMetadata["permission_ids"] : null);
        privateState.permission_ids = defaultValues ?
            (defaultValues["permission_ids"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["permission_ids"], context) :
                null) :
            null;

        context["field"] = "Region_id";
        context["metadata"] = (objectMetadata ? objectMetadata["Region_id"] : null);
        privateState.Region_id = defaultValues ?
            (defaultValues["Region_id"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Region_id"], context) :
                null) :
            null;

        context["field"] = "role_id";
        context["metadata"] = (objectMetadata ? objectMetadata["role_id"] : null);
        privateState.role_id = defaultValues ?
            (defaultValues["role_id"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["role_id"], context) :
                null) :
            null;

        context["field"] = "Status_id";
        context["metadata"] = (objectMetadata ? objectMetadata["Status_id"] : null);
        privateState.Status_id = defaultValues ?
            (defaultValues["Status_id"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Status_id"], context) :
                null) :
            null;

        context["field"] = "Username";
        context["metadata"] = (objectMetadata ? objectMetadata["Username"] : null);
        privateState.Username = defaultValues ?
            (defaultValues["Username"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Username"], context) :
                null) :
            null;

        context["field"] = "vizServerURL";
        context["metadata"] = (objectMetadata ? objectMetadata["vizServerURL"] : null);
        privateState.vizServerURL = defaultValues ?
            (defaultValues["vizServerURL"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["vizServerURL"], context) :
                null) :
            null;

        context["field"] = "WorkID";
        context["metadata"] = (objectMetadata ? objectMetadata["WorkID"] : null);
        privateState.WorkID = defaultValues ?
            (defaultValues["WorkID"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["WorkID"], context) :
                null) :
            null;

        context["field"] = "ZipCode";
        context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
        privateState.ZipCode = defaultValues ?
            (defaultValues["ZipCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ZipCode"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "AddressLine1": {
                get: function() {
                    context["field"] = "AddressLine1";
                    context["metadata"] = (objectMetadata ? objectMetadata["AddressLine1"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.AddressLine1, context);
                },
                set: function(val) {
                    setterFunctions['AddressLine1'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "AddressLine2": {
                get: function() {
                    context["field"] = "AddressLine2";
                    context["metadata"] = (objectMetadata ? objectMetadata["AddressLine2"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.AddressLine2, context);
                },
                set: function(val) {
                    setterFunctions['AddressLine2'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "City_id": {
                get: function() {
                    context["field"] = "City_id";
                    context["metadata"] = (objectMetadata ? objectMetadata["City_id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.City_id, context);
                },
                set: function(val) {
                    setterFunctions['City_id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "currUser": {
                get: function() {
                    context["field"] = "currUser";
                    context["metadata"] = (objectMetadata ? objectMetadata["currUser"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.currUser, context);
                },
                set: function(val) {
                    setterFunctions['currUser'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Email": {
                get: function() {
                    context["field"] = "Email";
                    context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Email, context);
                },
                set: function(val) {
                    setterFunctions['Email'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "FirstName": {
                get: function() {
                    context["field"] = "FirstName";
                    context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.FirstName, context);
                },
                set: function(val) {
                    setterFunctions['FirstName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "LastName": {
                get: function() {
                    context["field"] = "LastName";
                    context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.LastName, context);
                },
                set: function(val) {
                    setterFunctions['LastName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "MiddleName": {
                get: function() {
                    context["field"] = "MiddleName";
                    context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.MiddleName, context);
                },
                set: function(val) {
                    setterFunctions['MiddleName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "permission_ids": {
                get: function() {
                    context["field"] = "permission_ids";
                    context["metadata"] = (objectMetadata ? objectMetadata["permission_ids"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.permission_ids, context);
                },
                set: function(val) {
                    setterFunctions['permission_ids'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Region_id": {
                get: function() {
                    context["field"] = "Region_id";
                    context["metadata"] = (objectMetadata ? objectMetadata["Region_id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Region_id, context);
                },
                set: function(val) {
                    setterFunctions['Region_id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "role_id": {
                get: function() {
                    context["field"] = "role_id";
                    context["metadata"] = (objectMetadata ? objectMetadata["role_id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.role_id, context);
                },
                set: function(val) {
                    setterFunctions['role_id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Status_id": {
                get: function() {
                    context["field"] = "Status_id";
                    context["metadata"] = (objectMetadata ? objectMetadata["Status_id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Status_id, context);
                },
                set: function(val) {
                    setterFunctions['Status_id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Username": {
                get: function() {
                    context["field"] = "Username";
                    context["metadata"] = (objectMetadata ? objectMetadata["Username"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Username, context);
                },
                set: function(val) {
                    setterFunctions['Username'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "vizServerURL": {
                get: function() {
                    context["field"] = "vizServerURL";
                    context["metadata"] = (objectMetadata ? objectMetadata["vizServerURL"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.vizServerURL, context);
                },
                set: function(val) {
                    setterFunctions['vizServerURL'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "WorkID": {
                get: function() {
                    context["field"] = "WorkID";
                    context["metadata"] = (objectMetadata ? objectMetadata["WorkID"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.WorkID, context);
                },
                set: function(val) {
                    setterFunctions['WorkID'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "ZipCode": {
                get: function() {
                    context["field"] = "ZipCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ZipCode, context);
                },
                set: function(val) {
                    setterFunctions['ZipCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.AddressLine1 = value ? (value["AddressLine1"] ? value["AddressLine1"] : null) : null;
            privateState.AddressLine2 = value ? (value["AddressLine2"] ? value["AddressLine2"] : null) : null;
            privateState.City_id = value ? (value["City_id"] ? value["City_id"] : null) : null;
            privateState.currUser = value ? (value["currUser"] ? value["currUser"] : null) : null;
            privateState.Email = value ? (value["Email"] ? value["Email"] : null) : null;
            privateState.FirstName = value ? (value["FirstName"] ? value["FirstName"] : null) : null;
            privateState.LastName = value ? (value["LastName"] ? value["LastName"] : null) : null;
            privateState.MiddleName = value ? (value["MiddleName"] ? value["MiddleName"] : null) : null;
            privateState.permission_ids = value ? (value["permission_ids"] ? value["permission_ids"] : null) : null;
            privateState.Region_id = value ? (value["Region_id"] ? value["Region_id"] : null) : null;
            privateState.role_id = value ? (value["role_id"] ? value["role_id"] : null) : null;
            privateState.Status_id = value ? (value["Status_id"] ? value["Status_id"] : null) : null;
            privateState.Username = value ? (value["Username"] ? value["Username"] : null) : null;
            privateState.vizServerURL = value ? (value["vizServerURL"] ? value["vizServerURL"] : null) : null;
            privateState.WorkID = value ? (value["WorkID"] ? value["WorkID"] : null) : null;
            privateState.ZipCode = value ? (value["ZipCode"] ? value["ZipCode"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(createUser);

    //Create new class level validator object
    BaseModel.Validator.call(createUser);

    var registerValidatorBackup = createUser.registerValidator;

    createUser.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(createUser.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    var relations = [];

    createUser.relations = relations;

    createUser.prototype.isValid = function() {
        return createUser.isValid(this);
    };

    createUser.prototype.objModelName = "createUser";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    createUser.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("InternalusersObjService", "createUser", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    createUser.clone = function(objectToClone) {
        var clonedObj = new createUser();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return createUser;
});