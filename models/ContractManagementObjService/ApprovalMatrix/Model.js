/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "ApprovalMatrix", "objectService" : "ContractManagementObjService"};

    var setterFunctions = {
    };

    //Create the Model Class
    function ApprovalMatrix(defaultValues) {
        var privateState = {};

        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(ApprovalMatrix);

    //Create new class level validator object
    BaseModel.Validator.call(ApprovalMatrix);

    var registerValidatorBackup = ApprovalMatrix.registerValidator;

    ApprovalMatrix.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(ApprovalMatrix.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'updateApprovalMatrixStatus' with service id 'updateApprovalMatrixStatus7430'
     ApprovalMatrix.updateApprovalMatrixStatus = function(params, onCompletion){
        return ApprovalMatrix.customVerb('updateApprovalMatrixStatus', params, onCompletion);
     };

    //For Operation 'getApprovalMatrix' with service id 'getApprovalMatrix5781'
     ApprovalMatrix.getApprovalMatrix = function(params, onCompletion){
        return ApprovalMatrix.customVerb('getApprovalMatrix', params, onCompletion);
     };

    //For Operation 'isApprovalMatrixDisabled' with service id 'isApprovalMatrixDisabled2687'
     ApprovalMatrix.isApprovalMatrixDisabled = function(params, onCompletion){
        return ApprovalMatrix.customVerb('isApprovalMatrixDisabled', params, onCompletion);
     };

    var relations = [];

    ApprovalMatrix.relations = relations;

    ApprovalMatrix.prototype.isValid = function() {
        return ApprovalMatrix.isValid(this);
    };

    ApprovalMatrix.prototype.objModelName = "ApprovalMatrix";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    ApprovalMatrix.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("ContractManagementObjService", "ApprovalMatrix", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    ApprovalMatrix.clone = function(objectToClone) {
        var clonedObj = new ApprovalMatrix();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return ApprovalMatrix;
});