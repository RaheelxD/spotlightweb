define({
  segCountry : [],
  segState : [],
  segLocationCity : [],
  features : [],
  bulkUpdateListboxData: [],
  bulkUpdateAllFeaturesList : [],
  completeContractDetails :[],
  AccountTrasactions : null,
  selectedServiceCard : null,
  completeCompanyDetails :{},
  accountsUnlinkPayload : {},
  monetaryLimits : {},
  tabsConfig:{
    accounts:1,
    businessUsers:2
  },
  searchResult :{
    isFeatureMatched : false,
    isLimitMatched : false,
    isAcctMatched : false,
    isActionMatched : false
  },
  contractDetailsMap:{},
  recordsSize : 20,
  prevContractSelected:{
    contractNo : -1,
    segRowNo : -1
  },
  prevBreadCrumb : {
    "Action" : "",
    "text" : "",
    "enterif" : false
  },
  paginationDetails:{
    currSegContractData:[]
  },  
  currViewContractsTab :"",
  isSearchPerformedViewCont : false,
  assignedAccount:[],
  microBusinessBankingAccounts:[],
  microBusinessInputs:{
    MembershipId:null,
    TIN:null,
    CompanyType:null
  },
  actionConfig: {
    create: "CREATE",
    edit: "EDIT"
  },
  action:"CREATE",
  allFeatures : [],
  requiredFeatures : {
    additional : [],
    mandatory : [] 
  },
  viewContractServiceDef :[],
  monetaryActionsData :[],
  featureDetailsActionsData :[],
  refreshLimits : true,
  visitedFeatureOnceWhileCreate : false,
  intialFeatureList : [],
  initialLimitList : [],
  dataForSearch : false,
  dataForMandatorySearch : false,
  currentSegFeatureData  : [],
  currentMandatorySegFeatureData : [],
  prevIndex:-1,
  isAccountCentricConfig : true,
  inAuthorizedSignatoriesUi : true,
  maxCustomersCount : -1,
  lastSelected : -1,
  aboutToAddUser : {
    data : [],
    type : ""
  },
  countsentence : "",
  recentCustomerSearchTab : "",
  selAccCount:0,
  actionsAccountJSON :{},
  limitId :{
    PRE_APPROVED_DAILY_LIMIT:"PRE_APPROVED_DAILY_LIMIT",
    AUTO_DENIED_DAILY_LIMIT:"AUTO_DENIED_DAILY_LIMIT",
    PRE_APPROVED_WEEKLY_LIMIT:"PRE_APPROVED_WEEKLY_LIMIT",
    AUTO_DENIED_WEEKLY_LIMIT:"AUTO_DENIED_WEEKLY_LIMIT",
    PRE_APPROVED_TRANSACTION_LIMIT:"PRE_APPROVED_TRANSACTION_LIMIT",
    AUTO_DENIED_TRANSACTION_LIMIT:"AUTO_DENIED_TRANSACTION_LIMIT",
    WEEKLY_LIMIT:"WEEKLY_LIMIT",
    MAX_TRANSACTION_LIMIT:"MAX_TRANSACTION_LIMIT",
    DAILY_LIMIT:"DAILY_LIMIT"
  },
  recentContractDetails :{
    "contractName" : "",
    "contractId" : "",
    "contractServiceDef" : "",
    "contractServiceDefId" : "",
    "roles" : [],
    "customers" : [],
    "contractUsers" : []
  },
  approvalsCountMap :{
    ALL: kony.i18n.getLocalizedString("i18n.frmCompanies.AllApprovals"),
    ANY_ONE:kony.i18n.getLocalizedString("i18n.frmCompanies.AnyOneApproval"),
    ANY_THREE: kony.i18n.getLocalizedString("i18n.frmCompanies.AnyTwoApprovals"),
    ANY_TWO:kony.i18n.getLocalizedString("i18n.frmCompanies.AnyThreeApprovals"),},
  willUpdateUI: function (context) {
    this.updateLeftMenu(context);
    this.resetFilterWidgetsForAcct();
    this.view.forceLayout();
    if(context){
      if(context.action === "fetch"){
        this.showSearchScreen("fetch");
        this.getAllFeatures(context.features);
        kony.adminConsole.utils.hideProgressBar(this.view);
      }else if(context.action === "getAllFeatures"){
        this.getAllFeatures(context.features);
      }else if(context.action === "createCustomer"){
        this.hideRequiredMainScreens(this.view.flxCompanyDetails);
        if(context.features) 
          this.getAllFeatures(context.features);
        this.getCompanyAllDetails(context.id,4);
        //this.getCompanyCustomers(context.id,this.setCompanyCustomers);
        //this.backToCompanyDetails(this.tabsConfig.businessUsers);
      }else if (context.action === "hideLoadingScreen") {
        kony.adminConsole.utils.hideProgressBar(this.view);
        if(this.view.flxLoading2.isVisible)
          this.view.flxLoading2.setVisibility(false);
      }else if (context.contractCustomers) {
        this.contractCustomersCallBack(context);
      }else if(context.AllEligibleRelationalCustomers){
        this.coreRelativeCustomersCallBack(context.AllEligibleRelationalCustomers);
      }else if(context.customerSearch){
        this.customerSearchCallBack(context.customerSearch);
      }else if(context.AllEligibleRelationalCustomersError){
        this.coreRelativeCustomersErrorCallBack(context.AllEligibleRelationalCustomersError)
      }else if(context.serviceDefinitionRoles){
        this.serviceDefinitionRolesCallBack(context.serviceDefinitionRoles)
      }else if(context.serviceDefRoles){
        this.recentContractDetails.roles = context.serviceDefRoles
      }else if(context.createInfinityUser){
        this.createInfinityUserCallBack(context.createInfinityUser);
      }else if(context.editInfinityUser){
        this.createInfinityUserCallBack(context.editInfinityUser);
      }else if (context.loadingScreen) {
        if (context.loadingScreen.focus)
          kony.adminConsole.utils.showProgressBar(this.view);
        else
          kony.adminConsole.utils.hideProgressBar(this.view);
      }else if(context.createCompanySuccess){
        this.createCompaySuccess(context.createCompanySuccess);
      }else if (context.toastMessage) {
        if (context.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success")) {
          this.view.toastMessage.showToastMessage(context.toastMessage.message, this);
        } else if (context.toastMessage.status === kony.i18n.getLocalizedString("i18n.frmGroupsController.error")) {
          this.view.toastMessage.showErrorToastMessage(context.toastMessage.message, this);
        }
      }else if(context.coreTypeConfig){
        this.isAccountCentricConfig = context.coreTypeConfig.value;
       // this.isAccountCentricConfig = false;
      }else if(context.tinValidation){
        this.updateTINValidationStatus(context.tinValidation);
      }else if(context.accountSearch){
        this.microBusinessBankingAccounts = context.accountSearch;
        this.accountSearch(context.accountSearch);
      } else if(context.searchData) {
        this.setDatatoSearchSegment(context.searchData.contracts);
      } else if(context.editCompanySuccess){
        this.editCompanySuccess(context.editCompanySuccess);
      } else if(context.AccountTrasactions){
        this.AccountTrasactions = context.AccountTrasactions;
        this.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"));
      } else if(context.action === "companyDetails"){
        this.maxCustomersCount=-1;
        // method to set the users from the above data which is used for contract information and getting accounts
        this.getCompleteDataForCreatedCompany(context.companyDetails);
        this.backToCompanyDetails(context.selectedTabNo);
      } else if(context.action === "contractFeatureActionLimits"){
        this.completeCompanyDetails.accountsFeatures = context.contractDetails.accountsFeatures ? context.contractDetails.accountsFeatures : [];
        // method to set the users from the above data which is used for features and action
        let resData = [];
        if(context.selectTab!=="edit"){
          if(context.selectTab == 'Features'){
            resData = this.completeCompanyDetails.accountsFeatures.features;
          }else{
            resData = this.completeCompanyDetails.accountsFeatures.limits;
          } 
          this.createDynamicFlexForContract(resData ,context.selectTab);
        }else{
          this.setEditContractFeaturesLimits();
        }
      }else if(context.action === "contractInfinityUsers"){
        this.completeCompanyDetails.signatoryUsers = context.contractDetails;
        // method to set the users from the above data
        this.setSignatories(); 
      }else if(context.action === "contractApprvMatrix"){
        this.completeCompanyDetails.approvalsContext = context.contractDetails.approvalsContext ? context.contractDetails.approvalsContext : [];
        this.toggleApprovalsAccountsTab(true);
        // method to set the users from the above data
        this.populateAccountsSegment();
        this.onAccountNumSelectionApprovals(true);
      }else if(context.action === "contractDetails"){        
        this.getCompleteDataForcontractDetails(context.contractDetails);  
        // navigating to accounts tab
        this.backToCompanyDetails(1);
      } else if (context.action === "hideScreens"){
        this.view.flxSearchCompanies.setVisibility(false);
        this.view.flxCreateCompany.setVisibility(false);
        this.view.flxCompanyDetails.setVisibility(false);        
      } else if(context.businessTypes){
        this.setBusinessTypesListBoxData(context.businessTypes);
      } else if(context.membershipDetails){
        this.fillDetailsForSelectedCIF(context.membershipDetails);
      } else if (context.businessTypeRecords){
        if(this.inAuthorizedSignatoriesUi === true){
        this.maxCustomersCount=context.businessTypeRecords[0].maxAuthSignatory;
        this.showCustomers();
        }
      }else if(context.coreCustomerSearch){
        this.view.btnBackToMain.info=context.coreCustomerSearch;
        this.setCoreCustomersList(context.coreCustomerSearch);
      }else if(context.coreRelatedCustomers){
        this.setSelectedCustomerData(context.coreRelatedCustomers,context.coreCustomerId);
      }else if(context.serviceDefinitions){
        this.setContractServiceCards(context.serviceDefinitions);
        this.setDataToServiceTypeFilter(context.serviceDefinitions);
        this.view.flxContractServiceCards.info={"totalRecords":context.serviceDefinitions,"filteredRecords":context.serviceDefinitions};
      }else if(context.ServiceDefinitionRecords){
        
        /// resetting the search on loading the screen
        this.view.btnAdvSearch.text=kony.i18n.getLocalizedString("i18n.Group.AdvancedSearch");
        this.view.flxContractSpecificParams.setVisibility(false);
        this.view.flxSecondRow.setVisibility(false);
        this.view.flxThirdRow.setVisibility(false);
        this.view.fonticonrightarrow.text="";

        this.viewContractServiceDef = context.ServiceDefinitionRecords;
        // this.setServiceDefType();

        this.setServiceTypeStatusData(this.viewContractServiceDef);
        
        this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex=null;
      }
      else if(context.coreCustomerAccounts){
        this.setContractAccountsData(context.coreCustomerAccounts);
      }else if(context.serviceDefinitionFeatures){
        this.bulkUpdateAllFeaturesList =context.serviceDefinitionFeatures;
      }else if(context.serviceDefinitionFeaturesLimits){
        this.setServiceDefinitionFAData(context.serviceDefinitionFeaturesLimits);
      }else if(context.serviceDefinitionMonetaryActions){
        this.setGlobalMonetaryActions(context.serviceDefinitionMonetaryActions.limits);
      }
      else if(context.createContractSuccess){
        this.createEditContractSuccess(context.createContractSuccess);
      }else if(context.editContractSuccess){
        this.createEditContractSuccess(context.editContractSuccess);
      }
      else if(context.customerStatusList){
        this.setCustomerStatusData(context.customerStatusList.value);
      }else if(context.autoSyncAccountsFlag){
        this.autoSyncAccountsFlag=context.autoSyncAccountsFlag.value==="IMPLICIT"?"true":"false";
      }
    }
  },
  setWidgetDataMap : function(segAccountFeatures){
      var widgetDataMap = {
        // segContractsAccountView template widgets
        'flxContractsAccountContainer': 'flxContractsAccountContainer',
        'flxContractsAccountView': 'flxContractsAccountView',
        'flxStatus': 'flxStatus',
        'fontIconStatus': 'fontIconStatus',
        'lbAccountName': 'lbAccountName',
        'lblAccountHolderName': 'lblAccountHolderName',
        'lblAccountNumber': 'lblAccountNumber',
        'lblAccountType': 'lblAccountType',
        'lblSeperator': 'lblSeperator',
        'lblStatus': 'lblStatus',
        
        // segContractsAccountHeaderView template widgets
      'flxAccHolderNameHeader': 'flxAccHolderNameHeader',   
      'flxAccountNameHeader': 'flxAccountNameHeader',
      'flxAccountNumberHeader': 'flxAccountNumberHeader',
      'flxAccountTypeHeader': 'flxAccountTypeHeader',
      'flxContractsAccountHeaderView': 'flxContractsAccountHeaderView',
      'flxHeader': 'flxHeader',
      'flxHeaderAccountStatus': 'flxHeaderAccountStatus',
      'fontIconAccHoldNameSort': 'fontIconAccHoldNameSort',
      'fontIconAccNameSort': 'fontIconAccNameSort',
      'fontIconAccNumberSort': 'fontIconAccNumberSort',
      'fontIconAccTypeFilter': 'fontIconAccTypeFilter',
      'fontIconFilterStatus': 'fontIconFilterStatus',
      'lblAccHolderNameHeader': 'lblAccHolderNameHeader',
      'lblAccountNameHeader': 'lblAccountNameHeader',
      'lblAccountNumberHeader': 'lblAccountNumberHeader',
      'lblAccountStatusHeader': 'lblAccountStatusHeader',
      'lblAccountTypeHeader': 'lblAccountTypeHeader',
      'lblHeaderSeperator': 'lblHeaderSeperator',
        
      // segContractsFABodyView template
      "flxContractsFABodyView": "flxContractsFABodyView",
      "flxFeatureStatus": "flxFeatureStatus",
      "flxViewActionBody": "flxViewActionBody",
      "lblActionDesc": "lblActionDesc",
      "lblActionName": "lblActionName",
      "statusIcon": "statusIcon",
      "statusValue": "statusValue",
      "flxArrow" :"flxArrow",
      "lblArrow": "lblArrow",
        // segContractsFAHeaderView template
      "flxContractsFAHeaderView": "flxContractsFAHeaderView",
      "flxFeatureDetails": "flxFeatureDetails",
      "flxFeatureStatus": "flxFeatureStatus",
      "flxHeader": "flxHeader",
      "flxRow1": "flxRow1",
      "flxSelectedActions": "flxSelectedActions",
      "flxViewActionHeader": "flxViewActionHeader",
      "lblActionDescHeader": "lblActionDescHeader",
      "lblActionHeader": "lblActionHeader",
      "lblActionStatusHeader": "lblActionStatusHeader",
      "lblAvailableActions": "lblAvailableActions",
      "lblCountActions": "lblCountActions",
      "lblFASeperator1": "lblFASeperator1",
      "lblFASeperator2": "lblFASeperator2",
      "lblFASeperator3": "lblFASeperator3",
      "lblFeatureName": "lblFeatureName",
      "lblTotalActions": "lblTotalActions",

      // segContractsLimitsBodyView tempalte
      "flxContractsLimitsBodyView": "flxContractsLimitsBodyView",
      "flxDailyLimitTextBox": "flxDailyLimitTextBox",
      "flxPerLimitTextBox": "flxPerLimitTextBox",
      "flxViewLimits": "flxViewLimits",
      "flxWeeklyLimitTextBox": "flxWeeklyLimitTextBox",
      "lblAction": "lblAction",
      "lblCurrencySymbol1": "lblCurrencySymbol1",
      "lblCurrencySymbol2": "lblCurrencySymbol2",
      "lblCurrencySymbol3": "lblCurrencySymbol3",
      "lblDailyTransactionLimit": "lblDailyTransactionLimit",
      "lblLimitsSeperator": "lblLimitsSeperator",
      "flxPerLimit": "flxPerLimit",
      "lblCurrencyPer": "lblCurrencyPer",
      "lblPerTransactionLimit": "lblPerTransactionLimit",
      "flxDailyLimit": "flxDailyLimit",
      "lblCurrencyDaily": "lblCurrencyDaily",
      "flxWeeklyLimit": "flxWeeklyLimit",
      "lblCurrencyWeekly": "lblCurrencyWeekly",
      "lblWeeklyTransactionLimit": "lblWeeklyTransactionLimit",
      "tbxDailyValue": "tbxDailyValue",
      "tbxPerValue": "tbxPerValue",
      "tbxWeeklyValue": "tbxWeeklyValue",

      // segContractsLimitsHeaderView template
      "flxActionDetails": "flxActionDetails",
      "flxActionStatus": "flxActionStatus",
      "flxContractsLimitsHeaderView": "flxContractsLimitsHeaderView",
      "flxDailyLimitHeader": "flxDailyLimitHeader",
      "flxHeader": "flxHeader",
      "flxLimitInfo1": "flxLimitInfo1",
      "flxLimitInfo2": "flxLimitInfo2",
      "flxLimitInfo3": "flxLimitInfo3",
      "flxPerLimitHeader": "flxPerLimitHeader",
      "flxRow1": "flxRow1",
      "flxViewLimitsHeader": "flxViewLimitsHeader",
      "flxWeeklyLimitHeader": "flxWeeklyLimitHeader",
      "fontIconInfo1": "fontIconInfo1",
      "fontIconInfo2": "fontIconInfo2",
      "fontIconInfo3": "fontIconInfo3",
      "lblActionHeader": "lblActionHeader",
      "lblActionName": "lblActionName",
      "lblDailyLimitHeader": "lblDailyLimitHeader",
      "lblFASeperator1": "lblFASeperator1",
      "lblFASeperator2": "lblFASeperator2",
      "lblPerLimitHeader": "lblPerLimitHeader",
      "lblWeeklyLimitHeader": "lblWeeklyLimitHeader",
      "statusIcon": "statusIcon",
      "statusValue": "statusValue",
        "lblLimitsSeperator3" :"lblLimitsSeperator3"
      };
      segAccountFeatures.widgetDataMap = widgetDataMap;
  },
  resetSectionsForViewContracts :  function(rowData , tabName){
    // we expand/collapse the rowData
    let  check = false;
    if (tabName == 'Limits') {
          // for limits tab updating the data
          check = rowData[0]["flxViewLimitsHeader"]["isVisible"];
          rowData[0]["flxViewLimitsHeader"]["isVisible"] = !check;
          rowData[0]['lblFASeperator1']["isVisible"] = !check;
          rowData[0]['lblLimitsSeperator3']["isVisible"] = check;
    } else {
        // for actions tab updating the data
        check = rowData[0]["flxViewActionHeader"]["isVisible"];
        rowData[0]["flxViewActionHeader"]["isVisible"] = !check;
        rowData[0]['lblFASeperator3']["isVisible"] = !check;
        rowData[0]['lblFASeperator1']["isVisible"] = !check;
    }
      
    rowData[0]['lblArrow'] = !check?"\ue915":"\ue922"; 
    // ue915 is the down arrow
    
  },
  setServiceDefType:function(){
    let data = [["SELECT" , "Select a Service Type"]];
    // first index is key and second index is value which will be rendered
    data = data.concat( this.viewContractServiceDef.map(function(rec) {
        let arr =[];
        arr.push(rec.id);
        arr.push(rec.name);
        return arr;
      })
    );
    this.view.lstbxSearchServiceDef.masterData = data;
    this.view.lstbxSearchServiceDef.selectedKey = "SELECT" ;
    this.view.forceLayout();
  },
  collapseSegmentSection: function(segment , tabName){
    let prevInd = this.prevContractSelected.segRowNo;
    // if prevIndex is empty we reset to -1
    if(prevInd == -1){
      return;
    }
    let rowData = segment.data[prevInd];
    if(rowData[1].template=== "flxContractsFAHeaderView"){
      // the segment doesn't have section , returning from he function
      return;
    }
    
    rowData[1] = [
      {
        "template": "flxContractsFAHeaderView"
      }
    ];
    // collapsing the rowdata
    this.resetSectionsForViewContracts(rowData , tabName);
    
    // updating the index 
    segment.setSectionAt(rowData , prevInd);
    // segment.setData(segment.data);
  },
  /*
   * Created by :kaushik mesala
   * function to expand/collapse the feature / limits in the contracts
   * @param: component , component Index , tabname (which can be features/limits) 
   */
  contractComponentArrowToggle : function(accountsFeaturesCard , compInd , tabName){
    
    var visibility = accountsFeaturesCard.flxCardBottomContainer.isVisible === true ? false : true;

    // reset previos selected component and segment
    let prevInd = this.prevContractSelected.contractNo;
    if(prevInd != -1){
      let components = this.view.flxContractContainer.widgets();      
      components[prevInd].toggleCollapseArrow( false);
      
      // reset segment data
      this.collapseSegmentSection( components[prevInd].segAccountFeatures , tabName);

      this.prevContractSelected.segRowNo = -1;      
    }
    accountsFeaturesCard.toggleCollapseArrow( visibility);
    this.prevContractSelected.contractNo = compInd;
  },
  /*
   * Created by :kaushik mesala
   * function to create dynamic component to show the accounts features
   * @param: result of service call
   */
  createDynamicFlexForContract: function( resData , tabName){
    this.resetFilterWidgetsForAcct();
     this.view.flxAccountsSegmentPart.setVisibility(false);
     this.view.flxContractAccountsDetail.setVisibility(true);
     // resetting the index 
    this.prevContractSelected ={
                  contractNo : -1,
                  segRowNo : -1 
    };
    this.currViewContractsTab = tabName;
    // emptying the widget everytime
    this.view.flxContractContainer.removeAll();

    var noOfContracts = 0;
    if(tabName == 'Accounts'){
      noOfContracts = resData.length;
    }else{
      noOfContracts = resData.length;
    }
    if(noOfContracts === 0){
      //show no record found
      this.view.flxNoAccountResults.setVisibility(true);
    }else{
      this.view.flxNoAccountResults.setVisibility(false);
    }

    var channelName = 'accountsFeaturesCard';
    var width = "100";
    
    try {
        for (let i = 0; i < noOfContracts; i++) {
            let accountsFeaturesCard = new com.adminConsole.contracts.accountsFeaturesCard({
                "clipBounds": true,
                "id": channelName + "" + i,
                "isVisible": true,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "width": width + "%",
                "top": "20dp"
            }, {}, {});
            accountsFeaturesCard.toggleCollapseArrow(false);
            //Generic  onclick should be placed at the time of component creation
            //  overriding of onclick is done in respective tab if-else closure

            accountsFeaturesCard.flxArrow.onClick = this.contractComponentArrowToggle.bind(this  , accountsFeaturesCard , i , tabName);
            
          	// setting widget data map
            this.setWidgetDataMap(accountsFeaturesCard.segAccountFeatures);
            
            this.view.tbxTransactionSearch.placeholder = kony.i18n.getLocalizedString("i18n.frmCompanies.searchByComp");
            
            let acctDetail = resData[i];
            let contractName ="",contractId="";

            contractId = acctDetail.coreCustomerId;
            contractName = acctDetail.coreCustomerName;
            
          // setting the data to segment
            if(tabName == 'Accounts'){
              this.contractDetailsMap[contractId]  = {};  
              this.contractDetailsMap[contractId].contractTaxId = acctDetail.taxId;
              this.contractDetailsMap[contractId].email = acctDetail.email ? acctDetail.email : 'N/A';
              this.contractDetailsMap[contractId].phone = acctDetail.phone ? acctDetail.phone : 'N/A';

              this.contractDetailsMap[contractId].addressLine1 =  acctDetail.addressLine1;
              this.contractDetailsMap[contractId].addressLine2 =  acctDetail.addressLine2;
              
              this.view.tbxTransactionSearch.placeholder = kony.i18n.getLocalizedString("i18n.frmCompanies.searchByCont");
              this.setDataToContractAccts(accountsFeaturesCard, resData[i].accounts , i);              

            }else if (tabName == 'Features'){
                this.setDataToContractFetaures(accountsFeaturesCard, acctDetail.contractCustomerFeatures);
            }else{
              this.setDataToContractLimits(accountsFeaturesCard, acctDetail.contractCustomerLimits);
            }

            if(acctDetail.isPrimary === "true"){
              accountsFeaturesCard.flxPrimary.setVisibility(true);
            }
            accountsFeaturesCard.lblName.text = contractName;
            
            accountsFeaturesCard.lblData1.text = contractId;
            accountsFeaturesCard.lblData2.text = this.contractDetailsMap[contractId].contractTaxId;
            accountsFeaturesCard.lblData3.text = this.contractDetailsMap[contractId].addressLine1 + ","+this.contractDetailsMap[contractId].addressLine2;  
			
            // onTouchStart on the below label is not smooth updating to onClick
            accountsFeaturesCard.lblName.onClick = function(){
              let details = {"id": contractId,
                "name":contractName,
                "industry":this.contractDetailsMap[contractId].industry,
                "email":this.contractDetailsMap[contractId].email,
                "phone":this.contractDetailsMap[contractId].phone,
                "address": accountsFeaturesCard.lblData3.text
              };
              this.view.contractDetailsPopup.setDataForPopup(details);

              this.view.contractDetailsPopup.showBackButton(false);
              this.view.flxContractDetailsPopup.setVisibility(true);
            }.bind(this);

            this.view.flxContractContainer.add(accountsFeaturesCard);
        }
    } catch (e) {
        console.log("Exception in dynamic widget creation :" + e);
    }
  },
  companiesPreShow: function(){
    this.view.mainHeader.lblUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
    this.view.map1.mapLocations.mapKey = this.getMapInitiationKey();
    this.setFlowActions();
    this.view.mainHeader.btnAddNewOption.setVisibility(true);
    this.view.mainHeader.btnAddNewOption.focusSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.mainHeader.btnAddNewOption.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsDetails.btnNext.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsDetails.btnNext.hoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsAccounts.btnNext.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsAccounts.btnNext.focusSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsAccounts.btnNext.hoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
    this.subTabsButtonWithBgUtilFunction([this.view.tabs.btnTab1,this.view.tabs.btnTab2,this.view.tabs.btnTab3],this.view.tabs.btnTab1); 
    this.view.tabs.btnTab3.focusSkin ="sknbtnBgffffffLato485c75Radius3Px12Px";
    this.view.mainHeader.btnDropdownList.skin = "sknBtnBg4A77A0R28pxF13pxLatoReg";
    this.view.mainHeader.btnDropdownList.focusSkin = "sknBtnBg4A77A0R28pxF13pxLatoReg";
    this.view.mainHeader.btnDropdownList.hoverSkin = "sknBtnBg4A77A0R28pxF13pxLatoReg";
    if(!this.view.mainHeader.btnAddNewOption.isVisible)
        this.view.mainHeader.btnDropdownList.right="0px";
    this.clearSearchFeilds();
    this.view.flxSearchBusinessTypeFilter.setVisibility(false);
    this.view.flxCustomerSearchPopUp.setVisibility(false);
    this.defaultCurrencyCodeSymbol =this.defaultCurrencyCode();
    //customization for taxid custom listbox component
    this.view.customListboxTaxid.segList.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
    this.view.customListboxTaxid.segList.selectionBehaviorConfig = {};
    this.view.customListboxTaxid.flxSegmentList.skin = "sknFlxBgFFFFFFbrD7D9E01pxRd3pxShd0D0D11";
    this.currencyValue=this.defaultCurrencyCode();
  },
  showSearchScreen : function(action){
    this.hideRequiredMainScreens(this.view.flxSearchCompanies);
    if(action === "fetch"){
      this.view.tbxName.text = "";
      this.view.tbxEmailId.text = "";
      this.view.segSearchResults.setVisibility(false);
      this.view.lblNoResults.setVisibility(true);
      this.view.flxNoResultsFound.setVisibility(true);
      this.view.lblCreateCompanyLink.setVisibility(false);
      this.view.lblNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.NoSearchResultsFound");
    }
    var screenHeight = kony.os.deviceInfo().screenHeight;
    this.view.flxMainContent.height = (screenHeight -90) +"px";
    this.view.flxSearchResults.height = (screenHeight - 290) +"px";
    
    this.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
    this.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
    this.view.flxNameError.setVisibility(false);
    this.view.flxEmailIdError.setVisibility(false);
    
    this.view.mainHeader.btnAddNewOption.setVisibility(true);
    this.view.mainHeader.btnDropdownList.setVisibility(true);
    this.view.flxSettings.setVisibility(false);
    // this.view.flxHeaderPermissions.setVisibility(true);
    // this.view.segSearchResults.setVisibility(true);
    this.view.flxBreadcrumb.setVisibility(false);
    
     //add user visibilty changes
    this.view.flxAddUser.setVisibility(false);
    
    this.view.flxSearchUser.setVisibility(true);
    this.view.flxSearchInner.setVisibility(true);
    this.view.flxUserAdded.setVisibility(true);
    this.view.flxBottomButtonContianer.setVisibility(true);
    
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.view.flxEditUserContainer.setVisibility(true);
    this.view.flxAddeUserEditVerticalTabs.setVisibility(true);
    this.view.flxEditUserRightContainer.setVisibility(true);
    this.view.flxAccountTypesFilter.setVisibility(false);
    this.view.forceLayout();
  },
  downloadTransactionsAsCSV: function () {
    var segData = this.view.segTransactionHistory.data;
    var list = segData.map(function (record) {
      return {
        "RefNo": record.lblRefNo,
        "Type": record.lblType,
        "Description": record.lblTransctionDescription,
        "DateAndTime": record.lblDateAndTime,
        "Amount": record.lblAmountOriginal
      };
    });
    this.commonDownloadCSV(list, "Transactions.csv");
  },
  searchFilter: function(features) {
    var searchText = this.view.subHeader.tbxSearchBox.text;
    if (typeof searchText === "string" && searchText.length > 0) {
      return features.lblFeature.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    } else {
      return true;
    }
  },
  featureSearch : function(){
    var scopeObj = this;
    if(!(scopeObj.dataForSearch)){
      scopeObj.currentSegFeatureData = scopeObj.view.segFeatures.data;
      scopeObj.dataForSearch = true;
    }
    var data = scopeObj.currentSegFeatureData.filter(scopeObj.searchFilter);
    if (scopeObj.view.subHeader.tbxSearchBox.text === "") {
      scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
    } else {
      scopeObj.view.subHeader.flxClearSearchImage.setVisibility(true);
    }
    if (data.length === 0) {
      scopeObj.view.flxFeaturesContainer.setVisibility(false);
      scopeObj.view.flxFeatureNoResultfound.setVisibility(true);
      scopeObj.view.forceLayout();
    } else {
      scopeObj.view.flxFeaturesContainer.setVisibility(true);
      scopeObj.view.flxFeatureNoResultfound.setVisibility(false);
      scopeObj.view.segFeatures.setData(data.filter(scopeObj.searchFilter));
      scopeObj.view.forceLayout();
    }
  },
  searchMandatoryFilter: function(features) {
    var searchText = this.view.mandatoryFeatureHeader.tbxSearchBox.text;
    if (typeof searchText === "string" && searchText.length > 0) {
      return features.lblFeature.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    } else {
      return true;
    }
  },
  mandatoryfeatureSearch : function(){
    var scopeObj = this;
    if(!(scopeObj.dataForMandatorySearch)){
      scopeObj.currentMandatorySegFeatureData = scopeObj.view.segmandatoryFeatures.data;
      scopeObj.dataForMandatorySearch = true;
    }    
    var data = scopeObj.currentMandatorySegFeatureData.filter(scopeObj.searchMandatoryFilter);
    if (scopeObj.view.mandatoryFeatureHeader.tbxSearchBox.text === "") {
      scopeObj.view.mandatoryFeatureHeader.flxClearSearchImage.setVisibility(false);
    } else {
      scopeObj.view.mandatoryFeatureHeader.flxClearSearchImage.setVisibility(true);
    }
    if (data.length === 0) {
      scopeObj.view.mandatoryFeatureContainer.setVisibility(false);
      scopeObj.view.flxMandatoryFeatureNoResultfound.setVisibility(true);
      scopeObj.view.forceLayout();
    } else {
      scopeObj.view.mandatoryFeatureContainer.setVisibility(true);
      scopeObj.view.flxMandatoryFeatureNoResultfound.setVisibility(false);
      scopeObj.view.segmandatoryFeatures.setData(data.filter(scopeObj.searchFilter));
      scopeObj.view.forceLayout();
    }
  },
  togglePopUpScreens : function(){
  	if(this.view.flxAddExistingUserRelatatedCustomer.isVisible === true){
      this.view.flxAddExistingUserRelatatedCustomer.setVisibility(false);
      this.view.flxAddExistingUserOtherCustomer.setVisibility(true);
      this.view.btntoggleRelatedOther.text = "Search Related Customers";
      this.view.lblAddExsitingUserPopUpSubHeader.text = "Search Customer/Users to be associated to the Contract"+" \""+this.recentContractDetails.contractName+"\"";
      this.recentCustomerSearchTab = "other";
    }else if(this.view.flxAddExistingUserOtherCustomer.isVisible === true){
      this.view.flxAddExistingUserRelatatedCustomer.setVisibility(true);
      this.view.flxAddExistingUserOtherCustomer.setVisibility(false);
      this.view.btntoggleRelatedOther.text = "Search Other Customers/Users";
      this.view.lblAddExsitingUserPopUpSubHeader.text = "Search Related Customer to be associated to the Contract"+" \""+this.recentContractDetails.contractName+"\"";
      this.recentCustomerSearchTab = "Existing";
    }
  },
  showAddExistingPopUp : function(){
    this.setContractPreDataExsitingPopUp();
    this.view.flxAddUserFromExistingCustomerContainer.setVisibility(true);
  },
  setContractPreDataExsitingPopUp : function(){
    //global data
    this.view.lblAddExsitingUserPopUpSubHeader.text = "Search Related Customer to be associated to the Contract"+" \""+this.recentContractDetails.contractName+"\"";
    this.view.btntoggleRelatedOther.text = "Search Other Customers/Users";
    this.view.addExistingcommonButtons.btnSave.skin = "sknBtn7B7B7BRad20px";
    this.view.addExistingcommonButtons.btnSave.setEnabled(false);

    //search existing customer screen data
    this.view.contractCustomers.lblSelectedValue.text = "Select a Customer"
    this.view.contractCustomers.lblSelectedValue.info = "";
    this.view.flxSearchForExistingUser.setVisibility(true);
    this.view.flxExistingUserListContainer.setVisibility(false);
    this.view.flxAddExistingUserRelatatedCustomer.setVisibility(true);
    this.view.flxAddExistingUserOtherCustomer.setVisibility(false);
    this.view.contractCustomers.flxSegmentList.setVisibility(false);
    this.view.contractCustomers.segList.selectionBehavior = 1;
    this.recentCustomerSearchTab = "Existing";
    
    //search other customer screen data
    this.view.textBoxOtherCustomerEntry11.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry12.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry13.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry13CustomerDOB.value = "",
    this.view.textBoxOtherCustomerEntry22.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry23.tbxEnterValue.text = "";
    this.view.flxSearchForOtherCustomer.setVisibility(true);
    this.view.flxOtherCusotmerListContainer.setVisibility(false);
  },
  getContractCustomers : function(){
    var payload = {
      "contractId" : this.recentContractDetails.contractId
    };
    this.presenter.getContractDetails(payload);
  },
  contractCustomersCallBack : function(context){
    var customers = context.contractCustomers;
    var customerList = []
    for(var i=0;i<customers.length;i++){
      customerList.push(
        {
          "coreCustomerId" : customers[i].coreCustomerId,
          "coreCustomerName" : customers[i].coreCustomerName,
          "id" : customers[i].contractId,
          "isPrimary" : customers[i].isPrimary
        })}
    this.recentContractDetails.contractServiceDef = context.serviceDef.name;
    this.recentContractDetails.contractServiceDefId = context.serviceDef.id;
    this.recentContractDetails.customers = customers;
    this.recentContractDetails.formattedCustomers = customerList;
    this.recentContractDetails.contractUsers = this.presenter.getContractUser();
    this.getRolesWRTSeerviceDef();
    this.setSerchContractCustomers(customerList);
  },
  getRolesWRTSeerviceDef : function(){
    var payload = {
      "serviceDefinitionId" : this.recentContractDetails.contractServiceDefId
    }
    this.presenter.getServiceDefinitionRoles(payload);
  },
  setSerchContractCustomers : function(data){
    var self = this;
    var widgetDataMap = {
    	"lblCustomerName" :  "lblCustomerName",
      	"coreCustomerName" : "coreCustomerName",
		"coreCustomerId" : "coreCustomerId",
      	"isPrimary" : "isPrimary",
      	"id" : "id"
    }
    var finaldata = data.map(self.mappingCustomers);
    this.view.contractCustomers.segList.widgetDataMap = widgetDataMap;
    this.view.contractCustomers.segList.setData(finaldata);
    this.view.contractCustomers.segList.info = {
      "records" : []
    };
    this.view.contractCustomers.segList.info.records = finaldata;
    this.view.contractCustomers.segList.onRowClick = this.contractCustomerRowClick;
  },
  mappingCustomers : function(data){
    return {
      "lblCustomerName" :  {
        "text" : data.coreCustomerName + "(" + data.coreCustomerId + ")"
      },
      "coreCustomerName" : data.coreCustomerName,
      "coreCustomerId" : data.coreCustomerId,
      "id" : data.id,
      "isPrimary" : data.isPrimary
    }
  },
  toggleCustomerDropDown : function(){
    if(this.view.contractCustomers.flxSegmentList.isVisible === true){
      this.view.contractCustomers.flxSegmentList.setVisibility(false);
      this.view.contractCustomers.lblIconDropdown.text = "\ue915";
    }else if(this.view.contractCustomers.flxSegmentList.isVisible === false){
      this.view.contractCustomers.flxSegmentList.setVisibility(true);
      this.view.contractCustomers.lblIconDropdown.text = "\ue986";
    }
    this.view.forceLayout();
  },
  contractCustomerRowClick : function(){
    var data = this.view.contractCustomers.segList.selectedRowItems;
    this.view.contractCustomers.lblSelectedValue.text = data[0].lblCustomerName.text;
    this.view.contractCustomers.lblSelectedValue.info = data[0].coreCustomerId;
    this.toggleCustomerDropDown()
  },
  searchRelatedCustomers : function(){
    if(this.validateEntryForListBox()){
      this.view.flxAddUseFromExistingCustomerLoading.setVisibility(true);
      this.view.flxAddExistingUserInlineError.setVisibility(false);
      var payload = {
        "coreCustomerId" : this.view.contractCustomers.lblSelectedValue.info
      }
      this.presenter.getAllEligibleRelationalCustomers(payload);
    }else{
      this.view.flxAddExistingUserInlineError.setVisibility(true);
    }
  },
  validateEntryForListBox : function(){
    if(this.view.contractCustomers.lblSelectedValue.info === ""){
      return false;
    }else{
      return true;
    }
  },
  searchOtherCustomers : function(){
    if(this.validateEntriesForParamSearch()){
      this.view.flxAddUseFromExistingCustomerLoading.setVisibility(true);
      this.view.flxOtherUserInlineError.setVisibility(false);
      var updateDate = "";
      var dateText = this.view.textBoxOtherCustomerEntry13CustomerDOB.value
      if(dateText)
        updateDate = this.getTransactionDateForServiceCall(new Date(dateText));
      var payload = {
        "_customerId": this.view.textBoxOtherCustomerEntry11.tbxEnterValue.text.trim() || "",
        "_name": this.view.textBoxOtherCustomerEntry12.tbxEnterValue.text.trim() || "",
        "_username" : this.view.textBoxOtherCustomerEntry13.tbxEnterValue.text.trim() || "",
        "_dateOfBirth": updateDate.substring(0,10) || "",
        "_SSN" : this.view.textBoxOtherCustomerEntry22.tbxEnterValue.text.trim() || "",
        "_email": this.view.textBoxOtherCustomerEntry23.tbxEnterValue.text.trim() || "",    
        "_searchType":"CUSTOMER_SEARCH",
        "_pageOffset":"0",
        "_pageSize":"",
        "_sortVariable":"FirstName",
        "_sortDirection":"ASC"
      };
      this.createCountSentence(payload);
      this.presenter.customerSearch(payload);
    }else{
      this.view.flxOtherUserInlineError.setVisibility(true);
    }
  },
  createCountSentence : function(payload){
    var sentence = "Showing results for ";
    if(payload._customerId !== ""){
      sentence = sentence + "Customer ID " + payload._customerId;
    }else if(payload._name !== ""){
      sentence = sentence + "Customer Name " + payload._name;
    }else if(payload._username !== ""){
      sentence = sentence + "User Name " + payload._username;
    }else if(payload._dateOfBirth !== ""){
      sentence = sentence + "Date of Birth " + payload._dateOfBirth;
    }else if(payload._SSN !== ""){
      sentence = sentence + "Tax ID " + payload._SSN;
    }else if(payload._email !== ""){
      sentence = sentence + "Email ID " + payload._email;
    }
    this.countsentence = sentence;
  },
  validateEntriesForParamSearch : function(){
    if(this.view.textBoxOtherCustomerEntry11.tbxEnterValue.text.trim() === "" &&
       this.view.textBoxOtherCustomerEntry12.tbxEnterValue.text.trim() === "" &&
       this.view.textBoxOtherCustomerEntry13.tbxEnterValue.text.trim() === "" &&
       this.view.textBoxOtherCustomerEntry13CustomerDOB.value === "" &&
       this.view.textBoxOtherCustomerEntry22.tbxEnterValue.text.trim() === "" &&
       this.view.textBoxOtherCustomerEntry23.tbxEnterValue.text.trim() === ""){
	  return false;
    }else{
      return true;
    }
  },
  clearAllinfoOtherCustomerSearch : function(){
    this.view.textBoxOtherCustomerEntry11.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry12.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry13.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry13CustomerDOB.value = "";
    this.view.textBoxOtherCustomerEntry13CustomerDOB.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
    this.view.textBoxOtherCustomerEntry22.tbxEnterValue.text = "";
    this.view.textBoxOtherCustomerEntry23.tbxEnterValue.text = "";
  },
  coreRelativeCustomersCallBack : function(data){
    this.lastSelected = -1;
    if(this.view.flxNoResultsFoundExisting.isVisible)
      this.view.flxNoResultsFoundExisting.setVisibility(false);
    this.setUserListData(data,this.view.segExistingUserListContainer);
    this.setExistingUserCount(data.length,this.view.lblExistingUserRelatedCustomerCount);
  },
  customerSearchCallBack : function(data){
    this.lastSelected = -1;
    this.view.flxAddUseFromExistingCustomerLoading.setVisibility(false);
    var formatted_data = []
    for(var i=0;i<data.length;i++){
      var temp = {
        addressLine1: data[i].addressLine1 ? data[i].addressLine1 : "",
        addressLine2: data[i].addressLine2 ? data[i].addressLine2 : "",
        coreCustomerId: data[i].primaryCustomerId ? data[i].primaryCustomerId : "", 
        id : data[i].id ? data[i].id : "", 
        coreCustomerName: data[i].name ? data[i].name : "",
        dateOfBirth: data[i].DateOfBirth ? data[i].DateOfBirth : "",
        email: data[i].PrimaryEmailAddress ? data[i].PrimaryEmailAddress : "",
        firstName: data[i].FirstName ? data[i].FirstName : "",
        isAssociated: data[i].isAssociated ? data[i].isAssociated : "false",
        isProfileExists: data[i].isProfileExist === "true" ? data[i].isProfileExist : "false",
        lastName: data[i].LastName ? data[i].LastName : "",
        phone: data[i].PrimaryPhoneNumber ? data[i].PrimaryPhoneNumber : "",
        taxId: data[i].Ssn ? data[i].Ssn : "",
        zipCode: data[i].zipCode ? data[i].zipCode : ""
      };
      formatted_data.push(temp);
    }
    if(this.view.flxNoResultsFoundExisting.isVisible)
      this.view.flxNoResultsFoundExisting.setVisibility(false);
    this.setUserListData(formatted_data,this.view.segOtherRelatedCustList);
    this.setOtherUserCount(data.length,this.view.lblOtherRelatedCustomerCount);
  },
  customerSearchErrorCallBack : function(){
	this.view.flxAddUseFromExistingCustomerLoading.setVisibility(false);
  },
  coreRelativeCustomersErrorCallBack : function(data){
    this.view.flxAddUseFromExistingCustomerLoading.setVisibility(false);
    if(this.recentCustomerSearchTab === "other"){
      this.otherUserVisibiltyChanges(false);
    }else{
      this.existingUserVisibiltyChanges(false);
    }
  },
  setExistingUserCount : function(count,textBox){
    textBox.text = "Related Customers (" + count + ") - "+ this.view.contractCustomers.lblSelectedValue.text;
  },
  setOtherUserCount : function(count,textBox){
    textBox.text = this.countsentence + "-" + "(" + count + ")";
  },
  setUserListData : function(data,seg){
    var widgetDataMap = this.widgetMapRelatedCustList();
    var finalData = data.map(this.mappingRelatedCustList);
    seg.widgetDataMap = widgetDataMap;
    seg.setData(finalData);
    seg.info = {
      records : []
    };
    seg.info.records= finalData;
    if(seg === this.view.segExistingUserListContainer){
      seg.onRowClick = this.existngUserRowClick;
      this.existingUserVisibiltyChanges(true);
    }else{
      seg.onRowClick = this.otherUserRowClick;
      this.otherUserVisibiltyChanges(true);
    }
  },
  existingUserVisibiltyChanges : function(condition){
    if(condition){
      this.view.flxSearchForExistingUser.setVisibility(false);
      this.view.flxExistingUserListContainer.setVisibility(true);
    }else{
      this.view.flxSearchForExistingUser.setVisibility(true);
      this.view.flxExistingUserListContainer.setVisibility(false);
    }
    this.view.flxAddUseFromExistingCustomerLoading.setVisibility(false);
  },
  otherUserVisibiltyChanges : function(condition){
    if(condition){
      this.view.flxSearchForOtherCustomer.setVisibility(false);
      this.view.flxOtherCusotmerListContainer.setVisibility(true);
    }else{
      this.view.flxSearchForOtherCustomer.setVisibility(true);
      this.view.flxOtherCusotmerListContainer.setVisibility(false);
    }
    this.view.flxAddUseFromExistingCustomerLoading.setVisibility(false);
  },
  widgetMapRelatedCustList : function(){
  	var widgetDataMap = {
      "flxExistingUserListHeader" : "flxExistingUserListHeader",
      "lblCustomRadioBox" : "lblCustomRadioBox",
      "flxExistingUserHeaderListInner" : "flxExistingUserHeaderListInner",
      "flxExistingUserName" : "flxExistingUserName",
      "lblExistingUserName" : "lblExistingUserName",
      "flxExistingUserCustId" : "flxExistingUserCustId",
      "lblExistingUserCustId" : "lblExistingUserCustId",
      "flxExistingUserTaxId" : "flxExistingUserTaxId",
      "lblExistingUserTaxId" : "lblExistingUserTaxId",
      "flxExistingUserDOB" : "flxExistingUserDOB",
      "lblExistingUserDOB" : "lblExistingUserDOB",
      "flxExistingUserPhoneNumber" : "flxExistingUserPhoneNumber",
      "lblExistingUserPhoneNumber" : "lblExistingUserPhoneNumber",
      "flxExistingUserEmailId" : "flxExistingUserEmailId",
      "lblExistingUserEmailId" : "lblExistingUserEmailId",
      "lblReasonToDisable" : "lblReasonToDisable",
      "lblHeaderSeparator" : "lblHeaderSeparator",
      "imgRadioSelectionBox" : "imgRadioSelectionBox",
      "isSelected" : "isSelected"
    }
    return widgetDataMap;
  },
  mappingRelatedCustList : function(data){
    //set font icons for radio box and reason to disable 
    if(!data.taxId)
      data.taxId ="";
    if(!data.dateOfBirth)
      data.dateOfBirth ="";
    if(data.userId){
      data.id = data.userId;
    }
    var self = this;
    if(this.recentCustomerSearchTab === "other")
      var data = self.getCorrectAssociatedFlag(data);
    var rowData = self.setDisbledRowsData(data);
    return {
      "flxExistingUserListHeader" : {
         "enable" : rowData.block ? false : true
      },
      "lblCustomRadioBox" : "lblCustomRadioBox",
      "flxExistingUserHeaderListInner" : "flxExistingUserHeaderListInner",
      "flxExistingUserName" : "flxExistingUserName",
      "lblExistingUserName" : {
        "text" : data.firstName + " " + data.lastName,
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "flxExistingUserCustId" : "flxExistingUserCustId",
      "lblExistingUserCustId" : {
        "text" : data.coreCustomerId,
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "flxExistingUserTaxId" : "flxExistingUserTaxId",
      "lblExistingUserTaxId" : {
        "text" : data.taxId === ""? "NA" : data.taxId,
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "flxExistingUserDOB" : "flxExistingUserDOB",
      "lblExistingUserDOB" : {
        "text" : data.dateOfBirth === ""? "NA" : data.dateOfBirth,
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "flxExistingUserPhoneNumber" : "flxExistingUserPhoneNumber",
      "lblExistingUserPhoneNumber" : {
        "text" : data.phone ? data.phone : "N/A",
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "flxExistingUserEmailId" : "flxExistingUserEmailId",
      "lblExistingUserEmailId" : {
        "text" : data.email ? data.email : "N/A",
        "skin" : rowData.block ? "sknLblLato485c7513pxOp60" : "sknLblLato485c7513px"
      },
      "lblReasonToDisable" : {
        "text" : "",
        "tooltip" : rowData.reason,
        "isVisible" : rowData.visibilty,
        "skin" : "sknlbl485C7518px" // icomoon skin
      },
      "lblHeaderSeparator" : "lblHeaderSeparator",
      "imgRadioSelectionBox" : {
        "src" : rowData.src,
      	"enable" : rowData.block ? false : true
      }, 
      "isSelected" : "false",
      "isProfile" : data.isProfileExists,
      "orignalData" : data
    }
  },
  getCorrectAssociatedFlag : function(data){
    var userList = this.recentContractDetails.contractUsers;
    data.isAssociated = "false";
    for(var i=0;i<userList.length;i++){
      if((data.id && userList[i].customerId &&  data.id === userList[i].customerId) || (data.primaryCustomerId && userList[i].primaryCoreCustomerId && data.primaryCustomerId === userList[i].primaryCoreCustomerId)){
        data.isAssociated = "true";
      }
    }
    return data;
  },
  sortSegmentData : function(columnName,segPath){
    var segData = segPath.data;
    this.sortBy.column(columnName);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserName.text",this.view.lblFontExistingUserNameSort);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserCustId.text",this.view.lblFontExistingUserCustIdSort);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserTaxId.text",this.view.lblFontExistingUserTaxIdSort);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserDOB.text",this.view.lblFontExistingUserDOBSort);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserPhoneNumber.text",this.view.lblFontExistingUserPhoneNumberSort);
    this.determineSortFontIcon(this.sortBy,"lblExistingUserEmailId.text",this.view.lblFontExistingUserEmailIdSort);
    segPath.setData(segData.sort(this.sortBy.sortData));
  },
  setDisbledRowsData : function(data){
    var src = "radio_notselected.png",block = false ,reason = "",visibilty = false;
     if (data.phone === "" || data.email === "") {
       src = "radio_notselected.png";
       block = true;
       reason = "Please enter a Phone number and\nEmail ID before adding " + data.firstName + " " + data.lastName + "\nto the contract";
       visibilty = true;
     }else if(data.isAssociated === "true"){
       src = "radio_disabled.png";
       block = true;
       reason = data.firstName + " " + data.lastName + " is already added to\nthis contract.To edit his details please\nopen his profile";
       visibilty = true;
     }
    return {
      "src" : src,
      "block" : block,
      "reason" : reason,
      "visibilty" : visibilty
    }
  },
  existngUserRowClick : function(){
    this.recentCustomerSearchTab = "Existing";
    this.userRowClick(this.view.segExistingUserListContainer);
  },
  otherUserRowClick : function(){
    this.recentCustomerSearchTab = "other";
    this.userRowClick(this.view.segOtherRelatedCustList);
  },
  userRowClick : function(seg){
    var scope = this;    
    var SelectedData = seg.data;
    var rowIndex = seg.selectedRowIndex[1];
    if(rowIndex === scope.lastSelected){
      
      // changin selection only for prev selected row
      if(SelectedData[rowIndex].imgRadioSelectionBox.src === "radio_selected.png"){
        SelectedData[rowIndex].imgRadioSelectionBox.src = "radio_notselected.png";
        SelectedData[rowIndex].isSelected = "false";
      }
      else if(SelectedData[rowIndex].imgRadioSelectionBox.src === "radio_notselected.png"){
        SelectedData[rowIndex].imgRadioSelectionBox.src = "radio_selected.png";
        SelectedData[rowIndex].isSelected = "true";
      }
      seg.setDataAt(SelectedData[rowIndex],rowIndex);
    }else{
	  // changin selection for differently selected row 
      
      if(scope.lastSelected != -1){
        SelectedData[scope.lastSelected].imgRadioSelectionBox.src = "radio_notselected.png";
        SelectedData[scope.lastSelected].isSelected = "false";

        seg.setDataAt(SelectedData[scope.lastSelected],scope.lastSelected);
      }
      SelectedData[rowIndex].imgRadioSelectionBox.src = "radio_selected.png";
      SelectedData[rowIndex].isSelected = "true";
      
      seg.setDataAt(SelectedData[rowIndex],rowIndex);
    }
    this.lastSelected = rowIndex
    this.EnableDisbleAddButton(seg);
  },
  EnableDisbleAddButton : function(seg){
    this.view.addExistingcommonButtons.btnSave.skin = "sknBtn7B7B7BRad20px";
    this.view.addExistingcommonButtons.btnSave.setEnabled(false);
    var segData = seg.data;
    for(var i =0;i<segData.length;i++){
      if(segData[i].isSelected === "true"){
        this.view.addExistingcommonButtons.btnSave.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
        this.view.addExistingcommonButtons.btnSave.setEnabled(true);
        break;
      }}	
  },
  existingUserAddOnClick : function(){
    var scope = this;var segData;
    this.view.flxAddUserFromExistingCustomerContainer.setVisibility(false);
    if(this.recentCustomerSearchTab === "Existing"){
      segData = this.view.segExistingUserListContainer.data;
    }else if(this.recentCustomerSearchTab === "other"){
      segData = this.view.segOtherRelatedCustList.data;
    }
    scope.aboutToAddUser.data = [];
    for(var i =0;i<segData.length;i++){
      if(segData[i].isSelected === "true"){
        scope.aboutToAddUser.data.push(segData[i]);
    }}
    this.aboutToAddUser.type = "enrolledUser";
    this.view.flxSearchInner.setVisibility(false);
    this.view.flxUserAdded.setVisibility(true);
    if(scope.aboutToAddUser.data[0].isProfile === "true")
      this.userDeatilsScenerios("1");
    else
      this.userDeatilsScenerios("2");
  },
  visbiltyChangesForAddUserButton : function(){
    this.recentContractDetails.contractName = this.view.lblCompanyDetailName.text;
    this.recentContractDetails.contractId = this.view.lblDetailsValue11.text;
    this.fetchAutoSyncAccountsFlag();
    this.getContractCustomers();
    this.view.flxSearchCompanies.setVisibility(false);
    this.view.flxCreateCompany.setVisibility(false);
    this.view.flxCreateContract.setVisibility(false);
    this.view.flxCompanyDetails.setVisibility(false);
    this.view.flxAddUser.setVisibility(true);
    this.view.flxSearchUser.setVisibility(true);
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.view.flxSearchInner.setVisibility(true);
    this.view.flxUserAdded.setVisibility(false);
    this.view.flxBottomButtonContianer.setVisibility(true);
    this.view.btnAddUser.skin = "sknBtn7B7B7BRad20px";
    this.view.btnAddUser.setEnabled(false);
    var conractname = this.view.lblCompanyDetailName.text
    this.prevBreadCrumb.Action = this.view.breadcrumbs.btnBackToMain.onClick;
    this.prevBreadCrumb.text = this.view.breadcrumbs.btnBackToMain.text;
    this.prevBreadCrumb.enterif = true;
    this.view.breadcrumbs.btnBackToMain.text = conractname.toUpperCase();
    this.view.breadcrumbs.btnBackToMain.onClick = this.view.btnSearchUserCancel.onClick;
    this.view.breadcrumbs.lblCurrentScreen.text = "ADD USER";

  },
  collectAddNewUserInfo : function(){
    var payload = {
    	"firstName" : this.view.textBoxAddOtherUserEntry11.tbxEnterValue.text,
    	"middleName" : this.view.textBoxAddOtherUserEntry12.tbxEnterValue.text,
    	"lastName" : this.view.textBoxAddOtherUserEntry13.tbxEnterValue.text,
    	"email" : this.view.textBoxAddOtherUserEntry21.tbxEnterValue.text,
    	"phone" : this.view.textBoxAddOtherUserEntry22.txtContactNumber.text,
		"isdCode" : this.view.textBoxAddOtherUserEntry22.txtISDCode.text,
    	"dateOfBirth" : this.view.calAddOtherUserEntry23DOB.value,
    	"taxId" : this.view.textBoxAddOtherUserEntry31.tbxEnterValue.text
    };
    return payload;
  },
  validateNewUserInfo : function(){
    var self = this;
    var isValid = true;
    
    //first name
    if(this.view.textBoxAddOtherUserEntry11.tbxEnterValue.text.trim() === ""){
      self.view.textBoxAddOtherUserEntry11.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxAddOtherUserEntry11.flxInlineError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry11.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.FirstNameMissing");
      isValid = false;   
    }
    //last name 
    if(this.view.textBoxAddOtherUserEntry13.tbxEnterValue.text.trim() === ""){
      self.view.textBoxAddOtherUserEntry13.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxAddOtherUserEntry13.flxInlineError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry13.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.LastNameMissing");
      isValid = false;      
    }
    //email-id
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(this.view.textBoxAddOtherUserEntry21.tbxEnterValue.text.trim() === ""){
      self.view.textBoxAddOtherUserEntry21.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxAddOtherUserEntry21.flxInlineError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailId_cannot_be_empty");
      isValid = false;      
    }else if (emailRegex.test(self.view.textBoxAddOtherUserEntry21.tbxEnterValue.text.trim()) === false) {
      self.view.textBoxAddOtherUserEntry21.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxAddOtherUserEntry21.flxInlineError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
      isValid = false;
    }
    //ISD code
    var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
    if (!self.view.textBoxAddOtherUserEntry22.txtISDCode.text ||
        !self.view.textBoxAddOtherUserEntry22.txtISDCode.text.trim() ||
        (self.view.textBoxAddOtherUserEntry22.txtISDCode.text === "+")) {
      self.view.textBoxAddOtherUserEntry22.txtISDCode.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "0dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    } else if (self.view.textBoxAddOtherUserEntry22.txtISDCode.text.trim().length > 4) {
      self.view.textBoxAddOtherUserEntry22.txtISDCode.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "0dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    } else if (ISDRegex.test(self.view.textBoxAddOtherUserEntry22.txtISDCode.text) === false) {
      self.view.textBoxAddOtherUserEntry22.txtISDCode.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "0dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    }
	
    //contact num
    var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (!self.view.textBoxAddOtherUserEntry22.txtContactNumber.text || !self.view.textBoxAddOtherUserEntry22.txtContactNumber.text.trim()) {
      self.view.textBoxAddOtherUserEntry22.txtContactNumber.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "80dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_be_empty");
      isValid = false;
    } else if (self.view.textBoxAddOtherUserEntry22.txtContactNumber.text.trim().length > 15) {
      self.view.textBoxAddOtherUserEntry22.txtContactNumber.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "80dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
      isValid = false;
    } else if (phoneRegex.test(self.view.textBoxAddOtherUserEntry22.txtContactNumber.text) === false) {
      self.view.textBoxAddOtherUserEntry22.txtContactNumber.skin = "skinredbg";
      self.view.textBoxAddOtherUserEntry22.flxError.left = "80dp";
      self.view.textBoxAddOtherUserEntry22.flxError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      isValid = false;
    }
    
    //DOB
    if (this.view.calAddOtherUserEntry23DOB.value.trim() === "") {
      self.view.textBoxAddOtherUserEntry23Cal.skin = "sknFlxCalendarError";
      self.view.textBoxAddOtherUserEntry23.flxEnterValue.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.DOB_cannot_be_empty");
      isValid = false;
    }
    //SSN
    if (self.view.textBoxAddOtherUserEntry31.tbxEnterValue.text.trim() === "") {
      self.view.textBoxAddOtherUserEntry31.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxAddOtherUserEntry31.flxInlineError.setVisibility(true);
      self.view.textBoxAddOtherUserEntry31.lblErrorText.text = "Tax ID can not be empty";
      isValid = false;
    }
    return isValid;
  },
  clearValidationOnNewUser : function(widgetKeyUpOn,flex, error){
    flex.skin = "sknflxEnterValueNormal";
    error.setVisibility(false);
  },
  addUserNextFunction : function(){
    if(this.validateNewUserInfo()){
      this.view.flxAddOtherUserContainer.setVisibility(false);
      this.view.flxSearchInner.setVisibility(false);
      this.view.flxUserAdded.setVisibility(true);
      this.userDeatilsScenerios("3");
    }
  },
  showAddOtherUserContainer : function(){
    this.view.flxAddOtherUserContainer.setVisibility(true);
    this.view.lblAddOtherUserPopUpSubHeader.text = "Please enter the User details to be associated to the contract"+" \""+
      												this.recentContractDetails.contractName+"\"";
    this.view.textBoxAddOtherUserEntry11.tbxEnterValue.text = "";
    this.view.textBoxAddOtherUserEntry12.tbxEnterValue.text = "";
    this.view.textBoxAddOtherUserEntry13.tbxEnterValue.text = "";
    this.view.textBoxAddOtherUserEntry21.tbxEnterValue.text = "";
    this.view.textBoxAddOtherUserEntry22.txtISDCode.text = "";
    this.view.textBoxAddOtherUserEntry22.txtContactNumber.text = "";
    this.view.calAddOtherUserEntry23DOB.value = "";
    this.view.textBoxAddOtherUserEntry31.tbxEnterValue.text = "";
  },
  userDeatilsScenerios : function(scenrio){
    var self = this;
    // user with profile 
    if(scenrio === "1"){
      this.view.lblAddUserUserName.text = this.aboutToAddUser.data[0].lblExistingUserName.text;
      this.view.flxUserProfileFlag.setVisibility(true);
      this.view.flxAddedUserCustomerId.setVisibility(true);
      this.view.lblAddedUserPrimaryFlag.setVisibility(false);
      this.view.lblAddedUserCustomerIdValue.text = this.aboutToAddUser.data[0].lblExistingUserCustId.text;
      this.view.flxAddeDUserProfileDetails.setVisibility(true);
      this.view.lblAddedUserProfileDetails.text = "PROFILE DETAILS";
      this.view.lblAddedUserViewProfile.text = "View Profile";
      this.view.lblAddedUserViewProfile.onTouchStart = function(){
        var param = {
          "Customer_id": self.aboutToAddUser.data[0].lblExistingUserCustId.text
        };
        kony.adminConsole.utils.showProgressBar(self.view);
        self.presenter.navigateToCustomerPersonal(param,{"name":"frmCompanies"});
      }
      this.view.lblAddedUserAssociateMessage.text = "Associate "+ this.aboutToAddUser.data[0].lblExistingUserName.text+
        " to the below Customer ID’s of the Contract "+ 
        "\"" +this.recentContractDetails.contractName+ "\"";
      this.view.flxAddedUserList.height =  this.view.flxSearchInner.frame.height -(160 + 20 + 50 + 40) + "dp";
    }
    // user without profile 
    else if(scenrio === "2"){
      this.view.lblAddUserUserName.text = this.aboutToAddUser.data[0].lblExistingUserName.text;
      this.view.flxUserProfileFlag.setVisibility(false);
      this.view.flxAddedUserCustomerId.setVisibility(true);
      this.view.lblAddedUserPrimaryFlag.setVisibility(false);
      this.view.lblAddedUserCustomerIdValue.text = this.aboutToAddUser.data[0].lblExistingUserCustId.text;
      this.view.flxAddeDUserProfileDetails.setVisibility(false);
      this.view.lblAddedUserAssociateMessage.text = "Associate "+ this.aboutToAddUser.data[0].lblExistingUserName.text+
        											" to the below Customer ID’s of the Contract "+ 
        											"\"" +this.recentContractDetails.contractName+ "\"";
      this.view.flxAddedUserList.height =  this.view.flxSearchInner.frame.height -(160 + 20 + 50) + "dp";
    }
    // newly added user 
    else if(scenrio === "3"){
      this.view.lblAddUserUserName.text = this.view.textBoxAddOtherUserEntry11.tbxEnterValue.text;
      this.view.flxUserProfileFlag.setVisibility(false);
      this.view.flxAddedUserCustomerId.setVisibility(false);
      this.view.lblAddedUserProfileDetails.text = "USER DETAILS";
      this.view.lblAddedUserViewProfile.text = "Edit";
      this.view.lblAddedUserViewProfile.onTouchStart = function(){
        self.view.flxAddOtherUserContainer.setVisibility(true);
      }
      this.view.lblAddedUserAssociateMessage.text = "Associate "+ this.view.textBoxAddOtherUserEntry11.tbxEnterValue.text+
        											" to the below Customer ID’s of the Contract "+ 
        											"\"" +this.recentContractDetails.contractName+ "\"";
      this.view.flxAddedUserList.height =  this.view.flxSearchInner.frame.height -(160 + 20 + 50) + "dp";
      this.aboutToAddUser.data = [];
      this.aboutToAddUser.data[0] = this.collectAddNewUserInfo();
      this.aboutToAddUser.type = "newUser";
    }
    this.changeLayoutForCustSeg("hideBtn");
    this.addCustomerListOnUserPage(this.recentContractDetails.customers);
    this.view.btnAddUser.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.btnAddUser.setEnabled(true);
  },
  addCustomerListOnUserPage : function(data){
    var orignalData = data;
    var widgetDataMap = {
	  "flxEnrollCustomerList" : "flxEnrollCustomerList",
      "lblCustomerName":"lblCustomerName",
      "lblCustomerId":"lblCustomerId",
      "flxPrimary":"flxPrimary",
      "lblPrimary":"lblPrimary",
      "lstBoxService":"lstBoxService",
      "lstBoxRole":"lstBoxRole",
      "flxRoleError":"flxRoleError",
      "lblIconRoleError":"lblIconRoleError",
      "lblRoleErrorMsg":"lblRoleErrorMsg",
      "lblSeperator":"lblSeperator",
      "flxOptions":"flxOptions" ,
      "lblOptions":"lblOptions"
    };
    var finalData = orignalData.map(this.mapCustomerListAddUserPage)
    this.view.segEnrollCustList.widgetDataMap = widgetDataMap;
    this.view.segEnrollCustList.setData(finalData);
  },
  mapCustomerListAddUserPage : function(data){
    var rolesData = this.getRoleListBoxMappedData()
    var self = this;
    return {
      "lblCustomerName":data.coreCustomerName,
      "lblCustomerId":data.coreCustomerId,
      "flxPrimary":{
        "isVisible" : data.isPrimary ==="true" ? true : false
      },
      "lblPrimary":{
        "isVisible" : data.isPrimary === "true" ? true : false
      },
      "lstBoxService":{ 
        "skin":"sknLbxborderd7d9e03pxradius",//blocked skin
        "masterData":[[this.recentContractDetails.contractServiceDefId, this.recentContractDetails.contractServiceDef]],
        "selectedKey": this.recentContractDetails.contractServiceDefId,
        "enable": false,
        "toolTip" : this.recentContractDetails.contractServiceDef
      },
      "lstBoxRole":{"onSelection":self.collectEditDataForSingleCompany,
                    "skin":"sknLbxborderd7d9e03pxradius",
                    "masterData":rolesData,
                    "selectedKey": data.selectedKey? data.selectedKey : "select",
                    "enable": true},
      "flxRoleError":{"isVisible":false},
      "lblIconRoleError":"\ue94c",
      "lblRoleErrorMsg":kony.i18n.getLocalizedString("i18n.frmCompanies.Please_select_a_role"),
      "lblSeperator":"-",
      "flxOptions":{
        "isVisible":true,
        "skin":"sknFlxBorffffff1pxRound",
        "hoverSkin":"sknflxffffffop100Border424242Radius100px",
        "onClick":self.toggleContextualMenu
      },
      "lblOptions":"\ue91f",
      "orignalData" : data
    }
  },
  toggleContextualMenu:function(eventObj,context){
    var rowIndex = context.rowIndex;
    this.enrollSegRowInd = context.rowIndex;
    if (this.view.flxContextualMenu.isVisible===true){
      this.view.flxContextualMenu.setVisibility(false);
    }
    else{
      this.view.flxContextualMenu.setVisibility(true);
    } 
    this.view.forceLayout();
    var heightVal= 0;
    var templateArray = this.view.segEnrollCustList.clonedTemplates;
    for (var i = 0; i < rowIndex; i++) {
      heightVal = heightVal + templateArray[i].flxEnrollCustomerList.frame.height;
    }
    var scrollWidget = this.view.flxAddedUserList;
    var contextualWidget = this.view.flxContextualMenu;
    heightVal = heightVal + 106 - scrollWidget.contentOffsetMeasured.y;
    if ((heightVal + contextualWidget.frame.height) > (scrollWidget.frame.height+80)){
      this.view.flxContextualMenu.top=((heightVal-this.view.flxContextualMenu.frame.height)-25)+"px";
      this.view.flxAddedUserUpArrowImage.setVisibility(false);
      this.view.flxDownArrowImage.setVisibility(true);
      this.view.flxContextualMenuOptions.top = "0px";    
    }else{
      this.view.flxContextualMenu.top=(heightVal)+"px";
      this.view.flxAddedUserUpArrowImage.setVisibility(true);
      this.view.flxDownArrowImage.setVisibility(false);
      this.view.flxContextualMenuOptions.top = "-1px";
    }
  },
  getRoleListBoxMappedData : function(){
    var roleList = [];
    roleList = this.recentContractDetails.roles.reduce(
      function (list, record) {
        return list.concat([[record.id, record.name]]);
      }, [["select", kony.i18n.getLocalizedString("i18n.frmCompanies.Select_a_role")]]);
    return roleList;
  },
  onHoverEventCallback:function(widget, context) {
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      widget.setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      widget.setVisibility(false);
    }
  },
  removeRowFromCustSeg : function(){
    var rowIndex = this.view.segEnrollCustList.selectedRowIndex[1];
    var rowData = this.view.segEnrollCustList.data[rowIndex];
    this.presenter.deleteAccountsFeaturesForAddUser(rowData.orignalData.coreCustomerId)
    this.view.segEnrollCustList.removeAt(rowIndex);
    this.view.flxContextualMenu.setVisibility(false);
    this.changeLayoutForCustSeg("showBtn");
  },
  changeLayoutForCustSeg : function(context){
    if(context === "showBtn"){
      this.view.flxAddedUserList.height = this.view.flxUserAdded.frame.height -(160 + 20 + 50 + 30) + "dp";
      this.view.btnAddCustomerId.top = parseInt(this.view.flxAddedUserList.height) + 55 + "dp";
      this.view.btnAddCustomerId.setVisibility(true);
    }else if("hideBtn"){
      this.view.btnAddCustomerId.setVisibility(false);
    }
  },
  addCustomerID : function(){
    var data = this.recentContractDetails.customers;
    this.view.flxAddCustomerContainer.setVisibility(true);
	this.view.lblCustomerUserPopUpSubHeader.texzt = "Select Customer ID to be associated to "+this.view.lblAddUserUserName.text;
    var widgetDataMap = {
      "flxsegCustomersList" : "flxsegCustomersList",
      "flxCheckBox" : "flxCheckBox",
      "imgCheckBox" : "imgCheckBox",
      "lblCustomerId" : "lblCustomerId",
      "lblCustomerName" : "lblCustomerName"
    };
    var finalData = data.map(this.mapAddCustomerdata);
    this.view.segAddCust.widgetDataMap = widgetDataMap;
    this.view.segAddCust.setData(finalData);
    
  },
  mapAddCustomerdata : function(data){
    var self = this;
    var selectedData = self.getSelected(data)
    return {
      "flxsegCustomersList" : "flxsegCustomersList",
      "flxCheckBox" : {
        "onClick" : self.toggleCustomerSelection
      },
      "imgCheckBox" : selectedData.src,
      "lblCustomerId" : data.coreCustomerId,
      "lblCustomerName" : data.coreCustomerName,
      "isSelected" : selectedData.selected,
      "selectedRole" : selectedData.selectedRole
    };
  },
  getSelected : function(data){
    var src = "checkboxnormal.png", selectedRole;
    var segData = this.view.segEnrollCustList.data;
    for(var i= 0;i<segData.length;i++){
      if(data.coreCustomerId === segData[i].lblCustomerId){
        src =  "checkboxselected.png";
        selectedRole = segData[i].lstBoxRole.selectedKey;
      }
    }
    return {
      "src" : src,
      "selectedRole" : selectedRole,
      "selected" : true
    };
  },
  checkForPartialSelection : function(){
    var isPartial = false;
    var segData = this.view.segAddCust.data;
    for(var i=0;i<segData.length;i++){
      if(segData[i].isSelected === false){
        isPartial = true
        break
      }
    }
    return isPartial;
  },
  toggleCustomerSelection : function(eventObj,context){
    var rowIndex = context.rowIndex;
   	var rawData = this.view.segAddCust.data[rowIndex];
    if(rawData.imgCheckBox === "checkboxselected.png"){
      rawData.imgCheckBox = "checkboxnormal.png";
      rawData.isSelected = false;
    }else if(rawData.imgCheckBox === "checkboxnormal.png"){
      rawData.imgCheckBox = "checkboxselected.png";
      rawData.isSelected = true;
    }
    this.view.segAddCust.setDataAt(rawData, rowIndex);
    
    if(this.checkForPartialSelection()){
      this.view.imgAddCustHeaderCheckBox.src = "checkboxpartial.png"
    }else{
      this.view.imgAddCustHeaderCheckBox.src = "checkboxselected.png"
    }
  },
  addModifiedCustomerList : function(){
    var segData = this.view.segAddCust.data;
    var refData = []
    
    // get selected data
    for(var index = 0;index<segData.length;index++){
      if(segData[index].isSelected)
        refData.push(segData[index]);
    }
    
    //matching selected data ids with orignal list
    var ModifiedList = [];
    var orignalData = this.recentContractDetails.customers;
    for(var i=0;i<refData.length ; i++){
      for(var j=0;j<orignalData.length ;j++){
        if(refData[i].lblCustomerId === orignalData[j].coreCustomerId){
          if(refData[i].selectedRole){
            orignalData[j].selectedKey = refData[i].selectedRole
          }
          ModifiedList.push(orignalData[j]);
        }else{
          orignalData[j].selectedKey = "select";
        }}}
    this.addCustomerListOnUserPage(ModifiedList);
    this.view.flxAddCustomerContainer.setVisibility(false);
  },
  hideAddCustomerID : function(){
    this.view.flxAddCustomerContainer.setVisibility(false);
  },
  currCustomer : {},
  createAddUSerPayload : function(){
    var callCreate = true;
    if(this.validateCreateinfinityUserInfo()){
      var addUserPayload = {
        "userDetails" : "",
        "companyList" : "",
        "accountLevelPermissions" : "",
        "globalLevelPermissions"  : "",
        "transactionLimits" : ""
      }
      var user_details = this.getUserDetials();
      if(user_details.isProfile === true){
        callCreate = false;
        delete user_details.isProfile;
      }
      var company_details = this.getCompanyList();
      var features = this.getAddUserFeaturesList();
      var limits = this.getAddUserLimitsList();
	  for(var i=0 ;i<company_details.length;i++){
        for(var j=0;j<features.accountLevelPermissions.length;j++){
          if(features.accountLevelPermissions[j].cif === company_details[i].cif){
            var featureAcc = features.accountLevelPermissions[j].accounts;
            var companylistacc = company_details[i].accounts;
            var excludedAccounts=[];
            var includedAccounts=[];
            // mark false values
            for(var x=0;x<companylistacc.length;x++){
              for(var y=0;y<featureAcc.length;y++){
                if(companylistacc[x].accountId === featureAcc[y].accountId){
                  companylistacc[x].isEnabled = "true";
                }
              }
              if(companylistacc[x].isEnabled==="true"){
                includedAccounts.push(companylistacc[x]);
              }else{
                excludedAccounts.push(companylistacc[x]);
              }
            }
            company_details[i].accounts = includedAccounts;
            company_details[i].excludedAccounts = excludedAccounts;
          }
        }
      }
      addUserPayload.userDetails = JSON.stringify(user_details);
      addUserPayload.companyList = JSON.stringify(company_details);
      addUserPayload.accountLevelPermissions = JSON.stringify(features.accountLevelPermissions);
      addUserPayload.globalLevelPermissions = JSON.stringify(features.globalLevelPermissions);
      addUserPayload.transactionLimits = JSON.stringify(limits);
      if(callCreate)
        this.presenter.createInfinityUser(addUserPayload);
      else
        this.presenter.editInfinityUser(addUserPayload);
    }
  },
  validateCreateinfinityUserInfo : function(){
    var isValid = true;
    var data = this.view.segEnrollCustList.data;
    for(var i =0 ;i<data.length;i++){
      if(data[i].lstBoxRole.selectedKey === "select"){
        isValid = false;
        data[i].lstBoxRole.skin = "sknLstBoxeb3017Bor3px";
        data[i].flxRoleError.isVisible = true;  
      }
      this.view.segEnrollCustList.setDataAt(data[i],i);
    }
    return isValid;
  },
  createInfinityUserCallBack : function(){
    this.view.flxCompanyDetails.setVisibility(true);
    this.view.flxAddUser.setVisibility(false);
    this.view.flxSearchUser.setVisibility(true);
    this.view.flxSearchInner.setVisibility(true);
    this.view.flxUserAdded.setVisibility(false);
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.showSignatories();
  },
  /*
  * form the company list payload for enroll customer request param
  * @returns: account level, global level featurse actions
  */
  getAddUserFeaturesList : function(){
    var self = this;
    var accountLevelPermissions = [], globalLevelPermissions = [];
    var enrollCustAccountsFeatures = this.presenter.getAccountsFeaturesForAddUser();
    
    var enrollSegData = this.view.segEnrollCustList.data;
    for(var i=0; i<enrollSegData.length; i++){
      var accFeaturesObj = {"companyName": enrollSegData[i].lblCustomerName,
                            "cif": enrollSegData[i].lblCustomerId,
                            "accounts":[]};
      var accListMap = enrollCustAccountsFeatures[enrollSegData[i].lblCustomerId].accounts;
      var allAccFeaturesArr = enrollCustAccountsFeatures[enrollSegData[i].lblCustomerId].accountMapFeatures;
      var allOtherFeaturesArr = enrollCustAccountsFeatures[enrollSegData[i].lblCustomerId].nonAccLevelFeatures;
      //map features and actions for enroll payload format
      var otherLevelFeatures = this.mapFeaturesActionsForAddUserPayload(allOtherFeaturesArr);
      //append features for each account
      accListMap.forEach(function(accObj,key){
        var currAccFeatures = JSON.parse(allAccFeaturesArr.get(accObj.accountId).features);
        if(accObj.isEnabled === "true"){
          accFeaturesObj.accounts.push({
            "accountName": accObj.accountName,
            "accountId": accObj.accountId,
            "accountType": accObj.accountType,
            "featurePermissions": self.mapFeaturesActionsForAddUserPayload(currAccFeatures)
          });
        }
        
      });
      var globalFeaturesObj = {"companyName": enrollSegData[i].lblCustomerName,
                               "cif": enrollSegData[i].lblCustomerId,
                               "features":otherLevelFeatures};
      accountLevelPermissions.push(accFeaturesObj);
      globalLevelPermissions.push(globalFeaturesObj);
    }
    return {
      "accountLevelPermissions":accountLevelPermissions,
      "globalLevelPermissions":globalLevelPermissions
    };
  },
  /*
  * format features and actions list for add user payload
  * @param: features array
  * @return: formatted features list
  */
  mapFeaturesActionsForAddUserPayload : function(featuresArr){
    var features = featuresArr.map(function(feature){
      var featureObj = {"featureId": feature.featureId,
                        "featureDescription": feature.featureDescription,
                        "featureName": feature.featureName,
                        "isEnabled": feature.isEnabled,
                        "permissions":[]};
      var actions = feature.actions.map(function(action){
        return {
          "id": action.actionId,
          "isEnabled":action.isEnabled
        };
      });
      featureObj.permissions = actions;
      return featureObj;
    });
    return features;
  },
  /*
  * form limits related payload for enroll customer
  */
  getAddUserLimitsList : function(){
    var self = this;
    var companyList = [];
    var transactionLimits = [];
    var enrollCustAccountsFeatures = this.presenter.getAccountsFeaturesForAddUser();
    var enrollSegData = this.view.segEnrollCustList.data;
    for(var i=0; i<enrollSegData.length; i++){
      var accListMap = enrollCustAccountsFeatures[enrollSegData[i].lblCustomerId].accounts;
      var accLimitsMap = enrollCustAccountsFeatures[enrollSegData[i].lblCustomerId].accLevelLimits;
      var limitsObj = {"companyName": enrollSegData[i].lblCustomerName,
                       "cif": enrollSegData[i].lblCustomerId,
                       "limitGroups":[
                         {
                           "limitGroupId": "SINGLE_PAYMENT",
                           "limits": [
                             {
                               "id": "DAILY_LIMIT",
                               "value": "100"
                             },
                             {
                               "id": "WEEKLY_LIMIT",
                               "value": "500"
                             },
                             {
                               "id": "MAX_TRANSACTION_LIMIT",
                               "value": "50"
                             }
                           ]
                         },
                         {
                           "limitGroupId": "BULK_PAYMENT",
                           "limits": [
                             {
                               "id": "DAILY_LIMIT",
                               "value": "100"
                             },
                             {
                               "id": "WEEKLY_LIMIT",
                               "value": "500"
                             },
                             {
                               "id": "MAX_TRANSACTION_LIMIT",
                               "value": "50"
                             }
                           ]
                         }
                       ],
                       "accounts":[]};
      //append limits for each selected account
      accListMap.forEach(function(accObj,key){
		var currAccLimits = accLimitsMap.get(accObj.accountId).limits;
        if(accObj.isEnabled === "true"){
        limitsObj.accounts.push({
          "accountName": accObj.accountName,
          "accountId": accObj.accountId,
          "accountType": accObj.accountType,
          "featurePermissions":self.mapDefaultAccLimitsForAddUserPayload(JSON.parse(currAccLimits))
        });
	}
      });
    transactionLimits.push(limitsObj);
    }
    return transactionLimits;
  },
  /*
  * format limits list for enroll payload
  * @param: features array
  * @return: formatted limits list
  */
  mapDefaultAccLimitsForAddUserPayload : function(featuresArr){
    var self = this;
    var actionList = featuresArr.map(function(feature){
      for(var x=0; x<feature.actions.length; x++){
        var action = feature.actions[x];
        var actionObj =  {
          "featureId":feature.featureId,
          "actionId": action.actionId,
          "actionDescription": action.actionDescription,
          "actionName": action.actionName,
          "limitGroupId": action.limitgroupId || action.limitGroupId,
          "limits":[]
        };
        var limitsJson = action.limits ? Object.keys(action.limits) : [];
        var limitsArr = limitsJson.map(function(key){
          var limitObj = {"id":key,"value":action.limits[key]};
          return limitObj;
        });
        actionObj.limits = limitsArr;
        return actionObj;
      }
    });
    return actionList;
  },
  collectEditDataForSingleCompany : function(eventObj,context){
    var segData = this.view.segEnrollCustList.data
    var data = segData[context.rowIndex];
    this.enrollSegRowInd = context.rowIndex;
    //get compnay details for one company/customer
    var companyDetail = this.getCompanyDetails(data);
    this.currCustomer.companyDetail = companyDetail;
    //get action and limit for one company/customer
    if(data.lstBoxRole.selectedKey === "select"){
      data.lstBoxRole.skin = "sknLstBoxeb3017Bor3px";
      data.flxRoleError.isVisible = true;  
    }else{
      this.getActionsAndLimits(data);
      data.lstBoxRole.skin = "sknLbxborderd7d9e03pxradius";
      data.flxRoleError.isVisible = false;  
    }
    this.view.segEnrollCustList.setDataAt(data,context.rowIndex);
  },
  getActionsAndLimits : function(segData){
    var list = []
    var temp = {
      "coreCustomerId":segData.lblCustomerId,
      "serviceDefinitionId":segData.lstBoxService.selectedKey,
      "roleId":segData.lstBoxRole.selectedKey
    }
    list.push(temp);
    var payload = {
      "coreCustomerRoleIdList": list
    }
    this.presenter.getCoreCustomerRoleFeatureActionLimits(payload);
  },
  serviceDefinitionRolesCallBack : function(response){
    var self = this;
    var accFeatureLimits = {"accounts" : self.currCustomer.accounts,
                            "features" : response.features.length > 0 ? response.features[0].coreCustomerFeatures : [],
                            "limits": response.limits.length > 0 ? response.limits[0].coreCustomerLimits : [],
                            "isPrimary" : self.currCustomer.companyDetail.isPrimary,
                            "custId": self.currCustomer.companyDetail.cif };
    this.assignDefaultEnableFlagsEditUser(accFeatureLimits);
  },
  getUserDetials : function(){
    var data = this.aboutToAddUser.data[0];
    var type = this.aboutToAddUser.type;
    var details;
    if(data.orignalData){
      var newdata = data.orignalData;
      var phoneArr = newdata.phone.split("-");
      newdata.phoneNumber = phoneArr[1];
      newdata.isdCode = phoneArr[0];
      if(newdata.isdCode[0] !== "+" && newdata.isdCode.length <= 3){
        if(newdata.isdCode.length >= 2){
          var firstTwoDigit = newdata.isdCode[0]+newdata.isdCode[1];
          if(firstTwoDigit !== "00")
            newdata.isdCode = "+" + newdata.isdCode;
        }
      }
      details = {
        "firstName": newdata.firstName ? newdata.firstName : "",
        "lastName": newdata.lastName ? newdata.lastName : "",
        "middleName": newdata.middleName ? newdata.middleName : "",
        "phoneNumber": newdata.phoneNumber ? newdata.phoneNumber : "",
        "phoneCountryCode": newdata.isdCode ? newdata.isdCode.trim() : "",
        "dob": newdata.dateOfBirth ? newdata.dateOfBirth : "",
        "drivingLicenseNumber": "",
        "email": newdata.email ? newdata.email : "",
        "isEnrolled": type === "newUser" ? false : true,
        "isProfile" : data.isProfile === "true" ? true : false
      }
      if(details.isProfile){
        details.id = newdata.id;
      }else{
        details.coreCustomerId = newdata.coreCustomerId ? newdata.coreCustomerId : "";
      }
    }else{
      details = {
        "firstName": data.firstName ? data.firstName : "",
        "lastName": data.lastName ? data.lastName : "",
        "middleName": data.middleName ? data.middleName : "",
        "phoneNumber": data.phone ? data.phone : "",
        "phoneCountryCode": data.isdCode ? data.isdCode.trim() : "",
        "ssn": data.taxId ? data.taxId :"",
        "dob": data.dateOfBirth ? data.dateOfBirth : "",
        "drivingLicenseNumber": "",
        "coreCustomerId":data.coreCustomerId ? data.coreCustomerId.text : "",
        "email": data.email ? data.email : "",
        "isEnrolled": type === "newUser"? false : true,
        "isProfile" : false
      }
    }  
	return details;    
  },
  getCompanyList : function(){
    var self = this;
    var finalList = [];
    var segData = this.view.segEnrollCustList.data;
    for(var i = 0; i < segData.length ;i++){
      var temp = self.getCompanyDetails(segData[i]);
      finalList.push(temp);
    }
    return finalList;
  },
  getCompanyDetails : function(data){
    var self = this;
    var finalResponse = [];
      var segData = data
      var orignalData = data.orignalData;

      //forming account array
      var tempAccountArr = []
      for(var arr =0; arr<orignalData.accounts.length; arr++){
        var acc = orignalData.accounts[arr];
        var tempAccObject = {
          "accountName":acc.accountName,
          "accountId":acc.accountId,
          "isEnabled":"false",
          "accountType":acc.accountType,
		  "ownerType" : acc.ownerType}
        tempAccountArr.push(tempAccObject);
      }
      this.currCustomer.accounts = tempAccountArr;
      //forming whole obect for on company 
      var tempObject = {
        "companyName":orignalData.coreCustomerName,
        "contractId":self.recentContractDetails.contractId,
        "contractName":self.recentContractDetails.contractName,
        "cif":orignalData.coreCustomerId,
        "isPrimary":orignalData.isPrimary,
        "autoSyncAccounts":orignalData.autoSyncAccounts?orignalData.autoSyncAccounts:self.autoSyncAccountsFlag,
        "serviceDefinition":segData.lstBoxService.selectedKey,
        "roleId":segData.lstBoxRole.selectedKey,
      	"accounts":tempAccountArr}
      
    return tempObject;
  },
   /*
  * add isEnabled key to track the selection while edit
  * @param: {"accounts":[],"features":[],"limits":[],"isPrimary":true/false,"custId":""}
  */
  assignDefaultEnableFlagsEditUser : function(accFeatureLimitsObj){
   var featuresMap = new Map();
    featuresMap = this.getFeaturesMapToFormEditObject(accFeatureLimitsObj,1);
    var limitsArr = this.getLimitsMapToFormEditObject(accFeatureLimitsObj,2);
	
    this.actionsAccountJSON[accFeatureLimitsObj.custId] = {};
    var categorizedFeatures = this.getAccountLevelFeatures(featuresMap,accFeatureLimitsObj.custId);
    var accFeaturesMap = new Map();
    var accLimitsMap = new Map();
    
    var accounts = accFeatureLimitsObj.accounts;
    var accountsMap = new Map();
    for(var i=0;i<accounts.length;i++){
      accounts[i]["isEnabled"] = "true";
      accounts[i]["isAssociated"] = "true";
      accountsMap.set(accounts[i].accountId,accounts[i]); 
      //assign features for each accounts
      var accFeatureObj = {"accountDetails":accounts[i], "features":JSON.stringify(categorizedFeatures.accountLevelFeatures)}
      accFeaturesMap.set(accounts[i].accountId,accFeatureObj);
      
      var accLimitObj = {"accountDetails":accounts[i], "limits":JSON.stringify(limitsArr)};
      accLimitsMap.set(accounts[i].accountId,accLimitObj);
    }
    //store all features list for bulkupdate
    this.bulkUpdateAllFeaturesList = JSON.parse(JSON.stringify(categorizedFeatures.accountLevelFeatures));
    //store all featureslimits list for bulkupdate
    this.bulkUpdateAllFeaturesLimits = JSON.parse(JSON.stringify(limitsArr));

    var limitGroupsArr = this.caluclateTransactionLimitGroupValue(accFeaturesMap, accLimitsMap, true);
    var editUserObj = {"accounts" : accountsMap,
                       "features" : featuresMap, //Map
                       "accLevelFeatures": categorizedFeatures.accountLevelFeatures, //Array
                       "nonAccLevelFeatures": categorizedFeatures.otherFeatures, //Array
                       "accountMapFeatures": accFeaturesMap, //Map(features - stringified)
                       "limits": this.getLimitsMapToFormEditObject(accFeatureLimitsObj,1), //Map
                       "limitGroups": limitGroupsArr, //Array
                       "accLevelLimits":accLimitsMap, //Map(limits - stringified)
                       "isPrimary" : accFeatureLimitsObj.isPrimary,
                       "custId": accFeatureLimitsObj.custId
                      };
    this.presenter.setAccountsFeaturesForAddUser(accFeatureLimitsObj.custId,editUserObj);  
  },
  caluclateTransactionLimitGroupValue : function(accFeaturesMap,accLimitsMap,isFirstTime){
    var self =this;
    var activeFeatures = {};
    //fetch all the enabled actions for all the selected accounts
    accFeaturesMap.forEach(function(accountObj,key){
      if(accountObj.accountDetails.isEnabled === "true" || accountObj.accountDetails.isAssociated === "true"){
        activeFeatures[key] = [];
        var features = JSON.parse(accountObj.features);
        for(var i=0; i<features.length; i++){
          var actions = features[i].actions || features[i].permissions;
          for(var j=0; j<actions.length; j++){
            if(actions[j].isEnabled === "true"){
              var featureActionId = features[i].featureId +"$" + actions[j].actionId;
              activeFeatures[key].push(featureActionId);
            }
          }
        }
      }
    });

    //caluclate the max limit values for limits groups
    var limitGroups = {};
    accLimitsMap.forEach(function(accountObj,key){
      var featureLimits = JSON.parse(accountObj.limits);
      for(var l=0; l<featureLimits.length; l++){
        var actions = featureLimits[l].actions || featureLimits[l].permissions;
        for(var m=0; m< actions.length; m++ ){
          if (activeFeatures[key].includes(featureLimits[l].featureId + '$' + actions[m].actionId)) {
            var limitGroupId = actions[m].limitGroupId || actions[m].limitgroupId;
            if (!limitGroups.hasOwnProperty(limitGroupId)) {
              limitGroups[limitGroupId] = {
                'limitGroupName':actions[m].limitgroup || "",
                'limitGroupId': limitGroupId,
                "limits": [{"id": self.limitId.MAX_TRANSACTION_LIMIT,"maxValue":0.0},
                           {"id": self.limitId.DAILY_LIMIT, "maxValue" :0.0},
                           {"id": self.limitId.WEEKLY_LIMIT, "maxValue":0.0}]
              };
              //caluclate the max values for limit groups
              limitGroups[limitGroupId].limits[2].maxValue += parseFloat(actions[m].limits[self.limitId.WEEKLY_LIMIT]);
              let value = parseFloat(actions[m].limits[self.limitId.MAX_TRANSACTION_LIMIT]);
              if (limitGroups[limitGroupId].limits[0].maxValue < value) {
                limitGroups[limitGroupId].limits[0].maxValue = value;
              }
              limitGroups[limitGroupId].limits[1].maxValue += parseFloat(actions[m].limits[self.limitId.DAILY_LIMIT]);
              if(isFirstTime){ // fro first time assign max values to values
                limitGroups[limitGroupId].limits[0]["value"] = limitGroups[limitGroupId].limits[0].maxValue;
                limitGroups[limitGroupId].limits[1]["value"] = limitGroups[limitGroupId].limits[1].maxValue;
                limitGroups[limitGroupId].limits[2]["value"] = limitGroups[limitGroupId].limits[2].maxValue;
              }
            }
          }
        }
      }

    });
    var limitGroupArr = [];
    for (var key in limitGroups) {
      limitGroupArr.push(limitGroups[key]);
    }
    return limitGroupArr;
  },
  getFeaturesMapToFormEditObject : function(accFeatureLimitsObj,option){
    var featuresMap = new Map();
    //add isEnable key for all features and action
    var features = accFeatureLimitsObj.features;  
    for(var j=0; j<features.length; j++){
      features[j]["isEnabled"] = "true";
      for(var k=0; k<features[j].actions.length; k++){
        features[j].actions[k]["isEnabled"] = "true";
        features[j].actions[k]["isPartial"] = "false";
      }
      var featureId = features[j].featureId  || features[j].id;
      featuresMap.set(featureId,features[j]);
    }
    return (option === 1 ? featuresMap :features) ;
  },
  /*
  * add actuallimit key to all limits to construct edit user access object
  * @param: accounts,features,limits data,
  * @returns: limits map 
  */
  getLimitsMapToFormEditObject : function(accFeatureLimitsObj, option){
    var limitsMap = new Map();
    var limits = accFeatureLimitsObj.limits || [];  
    for(var i=0; i<limits.length; i++){
      for(var j=0; j<limits[i].actions.length; j++){
        limits[i].actions[j]["actualLimits"] = limits[i].actions[j].limits;
        limits[i].actions[j].limits = this.addNewLimitsToExistingLimits(limits[i].actions[j].limits);
      }
      var featureId = limits[i].featureId;
      limitsMap.set(featureId,limits[i]);
    }
    return (option === 1 ? limitsMap :limits);
  },
    /*
  * add new limit values to existing limits
  * @param: existing limit values array
  * @return: new limit values json
  */
  addNewLimitsToExistingLimits : function(limitVal){
    var allLimits = [], allLimitsJson;;
    if(limitVal && limitVal.length > 0){
      var limitJson = this.getLimitValuesJsonFromArray(limitVal);
      var newLimits = [{"id": this.limitId.PRE_APPROVED_DAILY_LIMIT, "value": "0"},
                       {"id": this.limitId.PRE_APPROVED_WEEKLY_LIMIT, "value": "0"},
                       {"id": this.limitId.PRE_APPROVED_TRANSACTION_LIMIT, "value": "0"},
                       {"id": this.limitId.AUTO_DENIED_DAILY_LIMIT, "value": limitJson[this.limitId.DAILY_LIMIT]},
                       {"id": this.limitId.AUTO_DENIED_WEEKLY_LIMIT, "value": limitJson[this.limitId.WEEKLY_LIMIT]},
                       {"id": this.limitId.AUTO_DENIED_TRANSACTION_LIMIT, "value": limitJson[this.limitId.MAX_TRANSACTION_LIMIT]}];
      allLimits = limitVal.concat(newLimits);
      allLimitsJson = this.getLimitValuesJsonFromArray(allLimits)
    }
    return allLimitsJson;
  },
    /*
  * create liit values json from array
  * @param: limit values array
  * @return: limit values json
  */
  getLimitValuesJsonFromArray : function(limitValueArr){
    var limitsJson = {};
    if (limitValueArr && limitValueArr.length > 0) {
      limitsJson = limitValueArr.reduce(function (mapJson, rec) {
        mapJson[rec.id] = rec.value;
        return mapJson;
      }, {});
    }
    return limitsJson;
  },
  /*
  * seperate account/non-account features actions
  * @param: features actions map
  * @param: account level and other features action json obj
  */
  getAccountLevelFeatures : function(featuresListMap, custId){
    var self =this;
    var accFeaturesArr =[],otherFeaturesArr = [];
    var accLimits = [];
    var featuresArr1 = new Map(JSON.parse(JSON.stringify(Array.from(featuresListMap))));
    var featuresArr2 = new Map(JSON.parse(JSON.stringify(Array.from(featuresListMap))));
    featuresArr1.forEach(function(featureObj,key){
      var currFeature = featureObj;
      var accActions = [],otherActions = [];
      for(var j=0; j<currFeature.actions.length; j++){
        if(currFeature.actions[j].isAccountLevel === "true" || currFeature.actions[j].isAccountLevel === "1"){
          accActions.push(currFeature.actions[j]);
          self.actionsAccountJSON[custId][currFeature.actions[j].actionId] =[];
        }else{
          otherActions.push(currFeature.actions[j]);
        }
      }
      if(accActions.length > 0){
        featureObj.actions = accActions;
        accFeaturesArr.push(featureObj);
      }
      if(otherActions.length > 0){
        var nonAccFeature = featuresArr2.get(key);
        nonAccFeature.actions = otherActions;
        otherFeaturesArr.push(nonAccFeature);
      }
    });
    return {
      "accountLevelFeatures" : accFeaturesArr,
      "otherFeatures": otherFeaturesArr
    };
  },
  validateRoleSelection : function(){
    var isValid = true;
    var rowIndex = this.view.segEnrollCustList.selectedRowIndex[1]
    var rowdata = this.view.segEnrollCustList.data[rowIndex];
    if(rowdata.lstBoxRole.selectedKey === "select"){
        rowdata.lstBoxRole.skin = "sknLstBoxeb3017Bor3px";
        rowdata.flxRoleError.isVisible = true;  
      	isValid = false;
      }
      this.view.segEnrollCustList.setDataAt(rowdata,rowIndex);
    return isValid;
  },
  editUserAccessOnClick : function(option){
    this.setEditUserAccessTitleCustInfo();
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserDetails = this.presenter.getAccountsFeaturesForAddUser(enrollUserData.orignalData.coreCustomerId);
    this.view.commonButtonsEditAccounts.btnCancel.info = {"previousData":editUserDetails, "previousSyncFlag":JSON.parse(JSON.stringify(enrollUserData.orignalData.autoSyncAccounts))};
    var accounts = editUserDetails.accounts;
    this.setAccountsSegmentData(accounts);
    this.view.enrollEditAccountsCard.lblHeading.text =kony.i18n.getLocalizedString("i18n.frmCompanies.Selected_Accounts") + ":";
    this.view.addedUserEditFeaturesCard.segAccountFeatures.info = {"parentId":"addedUserEditFeaturesCard","segData":[],"featuresType":1, "segDataJSON":{}};
    this.setFeaturesCardSegmentData(this.view.addedUserEditFeaturesCard.segAccountFeatures, editUserDetails.accLevelFeatures);
    this.storeActionForAccountSelection();
  },
  revertEditUserChangesOnCancel : function(){
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var previousChanges = this.view.commonButtonsEditAccounts.btnCancel.info.previousData;
    enrollUserData.orignalData.autoSyncAccounts=this.view.commonButtonsEditAccounts.btnCancel.info.previousSyncFlag;
    this.view.segEnrollCustList.setDataAt(enrollUserData,this.enrollSegRowInd);
    this.presenter.setAccountsFeaturesForAddUser(enrollUserData.orignalData.coreCustomerId,previousChanges);
  },
  setEditUserAccessTitleCustInfo : function(){
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var username =  this.aboutToAddUser.data[0].lblExistingUserName ? this.aboutToAddUser.data[0].lblExistingUserName.text : (this.aboutToAddUser.data[0].firstName +" "+this.aboutToAddUser.data[0].lastName);
    this.view.lblAccountsCustHeader.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UserColon")+" "+ username;
    this.view.lblFeaturesCustHeader.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UserColon")+" "+ username;
    this.view.lblOtherFeaturesCustHeader.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UserColon")+" "+ username;
    this.view.lblLimitsCustHeader.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UserColon")+" "+ username;
    this.setCustDetailsInCardEditUserAccess(this.view.customersDropdownAccounts,this.view.enrollEditAccountsCard);
    this.setCustDetailsInCardEditUserAccess(this.view.customersDropdownFeatures,this.view.addedUserEditFeaturesCard);
    this.setCustDetailsInCardEditUserAccess(this.view.customersDropdownOF,this.view.addedUserEditOtherFeaturesCard);
    this.setCustDetailsInCardEditUserAccess(this.view.addedUserLimitsDropdown,this.view.addedUserEditLimitsCard);
    this.view.forceLayout();
  },
  setCustDetailsInCardEditUserAccess : function(listboxCompPath,cardWidgetPath){
    var rowIndex = this.view.segEnrollCustList.selectedRowIndex[1]
    var rowdata = this.view.segEnrollCustList.data[rowIndex];
    listboxCompPath.lblSelectedValue.text = rowdata.lblCustomerName+"("+rowdata.lblCustomerId+")";
    listboxCompPath.setEnabled(false);
    cardWidgetPath.lblName.text = rowdata.lblCustomerName;
    cardWidgetPath.lblData1.text = rowdata.lblCustomerId;
    cardWidgetPath.lblData2.text = rowdata.orignalData.taxId || kony.i18n.getLocalizedString("i18n.Applications.NA");
    cardWidgetPath.lblData3.text = rowdata.orignalData.cityName +","+ rowdata.orignalData.country;
    if(cardWidgetPath.id==="enrollEditAccountsCard"){
      rowdata.orignalData.autoSyncAccounts=rowdata.orignalData.autoSyncAccounts?rowdata.orignalData.autoSyncAccounts:this.autoSyncAccountsFlag;
      cardWidgetPath.imgCheckboxOptions.src=rowdata.orignalData.autoSyncAccounts==="true"?"checkboxselected.png":"checkboxnormal.png";
      var scopeObj=this;
      cardWidgetPath.flxCheckboxOptions.onClick = function(){
        var isChecked="true";
        var selectedCustId=scopeObj.view.customersDropdownAccounts.lblSelectedValue.info.customerId;
        if(cardWidgetPath.imgCheckboxOptions.src==="checkboxnormal.png"){
          cardWidgetPath.imgCheckboxOptions.src="checkboxselected.png";
        }else{
          cardWidgetPath.imgCheckboxOptions.src="checkboxnormal.png";
          isChecked="false";
        }
        rowdata.orignalData.autoSyncAccounts=isChecked;
        scopeObj.view.segEnrollCustList.setDataAt(rowdata,rowIndex);
        scopeObj.view.forceLayout();
      };
    }
  },
  showEditUserScreen : function(){
    //     this.showBreadcrumbsForScreens(2);
    this.view.flxSearchUser.setVisibility(false);
    this.view.flxEditAddedUserDetails.setVisibility(true);
    this.view.flxAddedUserEditAccountsContainer.setVisibility(true);
    this.view.flxAddedUserEditFeaturesContainer.setVisibility(false);
    this.view.flxAddedUserEditOtherFeaturesCont.setVisibility(false);
    this.view.flxAddedUserEditLimitsContainer.setVisibility(false);
    this.view.flxAccountTypesFilter.setVisibility(false);    
    this.view.customersDropdownAccounts.setEnabled(false);
    this.view.customersDropdownAccounts.flxSelectedText.skin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.customersDropdownAccounts.flxSelectedText.hoverSkin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.customersDropdownFeatures.setEnabled(false);
    this.view.customersDropdownFeatures.flxSelectedText.skin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.customersDropdownFeatures.flxSelectedText.hoverSkin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.customersDropdownOF.setEnabled(false);
    this.view.customersDropdownOF.flxSelectedText.skin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.customersDropdownOF.flxSelectedText.hoverSkin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.addedUserLimitsDropdown.setEnabled(false);
    this.view.addedUserLimitsDropdown.flxSelectedText.skin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.addedUserLimitsDropdown.flxSelectedText.hoverSkin = "sknFlxbgF3F3F3bdrd7d9e0";
    //enable all buttons
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditOF.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditOF.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditLimits.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditLimits.btnNext,false,true);
    this.enableOrDisableVerticalTabEditUser(true);
    this.view.forceLayout();
  },
  enableOrDisableVerticalTabEditUser : function(isEnable){
    this.view.AddedUserEditVerticalTabs.setEnabled(isEnable);
  },
  navigatetoEditAccountsPage : function(){
    if(this.validateRoleSelection()){
      this.showEditUserScreen();
      this.editUserAccessOnClick();
      this.showEditAccountsScreen();
    }
  },
  navigatetoEditAccountLevelFeaturePage : function(){
    if(this.validateRoleSelection()){
      this.showEditUserScreen();
      this.editUserAccessOnClick();
      this.showEditFeaturesScreen();
    }
  },
  navigateToLimitScreen : function(){
    if(this.validateRoleSelection()){
      this.showEditUserScreen();
      this.editUserAccessOnClick();
      this.showEditLimitsScreen();
    }    
  },
  setAccountsSegmentData : function(accountsList){
    var self = this;
    var rowData = [];
    var sectionData = {};
    this.selAccCount = 0;
    accountsList.forEach(function(rec,key){
      rowData.push({
        "id":rec.accountId,
        "flxCheckbox":{
          "onClick":self.onClickAccountsEditUserAccess.bind(self,self.view.enrollEditAccountsCard.segAccountFeatures,false)
        },
        "imgCheckbox":{
          "src":rec.isAssociated === "true" ? self.AdminConsoleCommonUtils.checkboxSelected : self.AdminConsoleCommonUtils.checkboxnormal
        },
        "lblAccountNumber": {"text": rec.accountId},
        "lblAccountType": {"text": rec.accountType},
        "lblAccountName": {"text": rec.accountName},
        "lblAccountHolder": {"text": rec.ownerType || ""},
        "lblSeperator":"-",
        "template":"flxContractEnrollAccountsEditRow"
      });
      if(rec.isAssociated === "true") 
        self.selAccCount = self.selAccCount +1;
    });
   var sectionData = {
      "flxCheckbox":{
        "onClick": this.onCheckAccountsCheckboxAddUser.bind(this,this.view.enrollEditAccountsCard.segAccountFeatures,true)
      },
      "flxAccountNumCont":{
        "onClick":this.sortAndSetData.bind(this,"lblAccountNumber.text",this.view.enrollEditAccountsCard.segAccountFeatures, 1)
      },
      "lblIconSortAccName":{
        "text": "\ue92a",
        "skin": "sknIcon12pxBlack","hoverSkin" :"sknIcon12pxBlackHover",
        "left" : "10px"
      },
      "lblAccountNumber": kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNUMBER"),
      "imgSectionCheckbox": {
        "src":this.getHeaderCheckboxImage(rowData,true,true)
      },
      "flxAccountType":{
         "onClick": this.showFilterForAccountsSectionClickAddUser.bind(this,1)
      },
      "lblAccountType": kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE"),
      "lblIconFilterAccType":"\ue916",
      "lblAccountName": kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME"),
      "lblIconAccNameSort":{
        "text": "\ue92b",
        "skin": "sknIcon15px","hoverSkin":"sknlblCursorFont",
        "left" : "5px"
      },
      "flxAccountName":{
        "onClick": this.sortAndSetData.bind(this,"lblAccountName.text", this.view.enrollEditAccountsCard.segAccountFeatures, 1)
      },
      "flxAccountHolder":{
         "onClick": this.showFilterForAccountsSectionClickAddUser.bind(this,2)
      },
      "lblAccountHolder": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP_TYPE"),
      "lblIconSortAccHolder":"\ue916",
      "lblSeperator":"-",
      "template":"flxContractEnrollAccountsEditSection",
    };
    this.sortBy = this.getObjectSorter("lblAccountNumber.text");
    this.sortBy.inAscendingOrder = true;
    rowData = rowData.sort(this.sortBy.sortData);
    this.view.enrollEditAccountsCard.lblCount.text = this.getSelectedItemsCount(rowData, true);
    this.view.enrollEditAccountsCard.lblTotalCount.text = "of " + this.getTwoDigitNumber(rowData.length);
    this.view.enrollEditAccountsCard.segAccountFeatures.widgetDataMap = {
      "flxHeaderContainer":"flxHeaderContainer",
      "flxAccountNumCont":"flxAccountNumCont",
      "lblAccountNumber":"lblAccountNumber",
      "lblIconSortAccName":"lblIconSortAccName",
      "flxCheckbox":"flxCheckbox",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxAccountType":"flxAccountType",
      "lblAccountType":"lblAccountType",
      "lblIconFilterAccType":"lblIconFilterAccType",
      "lblAccountName":"lblAccountName",
      "flxAccountName":"flxAccountName",
      "lblIconAccNameSort":"lblIconAccNameSort",
      "flxAccountHolder":"flxAccountHolder",
      "lblAccountHolder":"lblAccountHolder",
      "lblIconSortAccHolder":"lblIconSortAccHolder",
      "lblSeperator":"lblSeperator",
      "flxContractEnrollAccountsEditSection":"flxContractEnrollAccountsEditSection",
      "id":"id",
      "imgCheckbox":"imgCheckbox",
      "flxContractEnrollAccountsEditRow":"flxContractEnrollAccountsEditRow"
    };
    this.view.enrollEditAccountsCard.segAccountFeatures.setData([[sectionData,rowData]]);
    this.view.enrollEditAccountsCard.segAccountFeatures.info = {"segData":[[sectionData,rowData]], "segDataJSON":{}};
	 this.setDataToAccountsTabFilters();
    this.view.forceLayout();
  },
  setDataToAccountsTabFilters : function(){
    var self =this;
    var accountsData = this.view.enrollEditAccountsCard.segAccountFeatures.info.segData;
    var rowsData = accountsData.length > 0 ? accountsData[0][1]:[]
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var typesList =[],ownerTypeList =[],maxSizeTypeText ="",maxSizeOwnerTypeText ="";
    for(var i=0;i<rowsData.length;i++){
      if(!typesList.includes(rowsData[i].lblAccountType.text))
        typesList.push(rowsData[i].lblAccountType.text);
      if(!ownerTypeList.includes(rowsData[i].lblAccountHolder.text)){
        ownerTypeList.push(rowsData[i].lblAccountHolder.text);
      }    
    }
    var typesData = typesList.map(function(rec){
      maxSizeTypeText=rec.length > maxSizeTypeText.length ? rec: maxSizeTypeText;
      return {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": self.AdminConsoleCommonUtils.checkboxnormal,
        "lblDescription": rec
      };
    });
    var ownershipData = ownerTypeList.map(function(rec){
      maxSizeOwnerTypeText=rec.length > maxSizeOwnerTypeText.length ? rec: maxSizeOwnerTypeText;
      return {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": self.AdminConsoleCommonUtils.checkboxnormal,
        "lblDescription": rec
      }; 
    });
    this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.widgetDataMap = widgetMap;
    this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.widgetDataMap = widgetMap;

    this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.setData(typesData);
    this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.setData(ownershipData);

    this.view.flxAccountsFilterAddUser.width=this.AdminConsoleCommonUtils.getLabelWidth(maxSizeTypeText)+55+"px";
    this.view.flxOwnershipFilterAddUser.width=this.AdminConsoleCommonUtils.getLabelWidth(maxSizeOwnerTypeText)+55+"px";
    var selTypeInd = [],selOwnerInd = [];
    for(var j=0;j<typesList.length;j++){
      selTypeInd.push(j);
    }
    for(var k=0;k<ownerTypeList.length;k++){
      selOwnerInd.push(k);
    }
    self.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.selectedIndices = [[0,selTypeInd]];
    self.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.selectedIndices = [[0,selOwnerInd]];
    this.view.forceLayout();
  },
  searchFilterForAccounts : function(){
    var searchText = this.view.searchEditAccounts.tbxSearchBox.text;
    var accountsData = this.view.enrollEditAccountsCard.segAccountFeatures.info.segData;
    var sectionData = accountsData.length > 0 ? accountsData[0][0] :[];
    var rowsData = this.filterAccountsOnTypeOwnership();
    var filteredData = [], dataToSet;
    if(searchText.length >= 0){
      for(var i=0;i<rowsData.length;i++){
        if(rowsData[i].lblAccountName.text.toLowerCase().indexOf(searchText) >= 0 ||
           (rowsData[i].lblAccountNumber.text.indexOf(searchText) >= 0)){
          filteredData.push(rowsData[i]);
        }
      }
      sectionData.imgSectionCheckbox.src = this.getHeaderCheckboxImage(filteredData,true,true);
      if(filteredData.length > 0){
        dataToSet = [[sectionData,filteredData]];
        this.view.enrollEditAccountsCard.lblCount.text = this.getSelectedItemsCount(filteredData, true);
        this.view.enrollEditAccountsCard.lblTotalCount.text ="of "+ this.getTwoDigitNumber(filteredData.length);
      }else{
        dataToSet = [];
        this.view.enrollEditAccountsCard.lblCount.text = "0";
        this.view.enrollEditAccountsCard.lblTotalCount.text = "of 0";
      }
      this.view.enrollEditAccountsCard.segAccountFeatures.rowTemplate = "flxContractEnrollAccountsEditRow";
      this.view.enrollEditAccountsCard.segAccountFeatures.setData(dataToSet);
    } else{
      this.view.enrollEditAccountsCard.segAccountFeatures.setData(accountsData);
    }
    this.view.flxEditUserContainer.forceLayout();
  },
  filterAccountsOnTypeOwnership : function(){
    var selFilter = [[]];
    var dataToShow = [];
    var accountsData = this.view.enrollEditAccountsCard.segAccountFeatures.info.segData[0][1];
    var typeIndices = this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.selectedIndices;
    var ownershipIndices = this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.selectedIndices;
    selFilter[0][1] = [];
    selFilter[0][0] = [];    
    var selTypeInd = null;
    var selOwnershipInd = null;
    //get selected types
    var types = "";
    selTypeInd = typeIndices ? typeIndices[0][1] : [];
    for (var i = 0; i < selTypeInd.length; i++) {
      selFilter[0][0].push(this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.data[selTypeInd[i]].lblDescription);
    }
    //get ownership types
    var types = "";
    selOwnershipInd = ownershipIndices ? ownershipIndices[0][1] : [];
    for (var j = 0; j < selOwnershipInd.length; j++) {
      selFilter[0][1].push(this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.data[selOwnershipInd[j]].lblDescription);
    }
    if (selFilter[0][0].length === 0 || selFilter[0][1].length === 0) { //none selected - show no results
      dataToShow = [];
    }else if(selFilter[0][0].length ===this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.data.length && selFilter[0][1].length==this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.data.length)
      dataToShow= accountsData;
    else if (selFilter[0][0].length > 0 && selFilter[0][1].length > 0) {//both filters selected
      dataToShow = accountsData.filter(function(rec){
        if(selFilter[0][0].indexOf(rec.lblAccountType.text) >= 0 && selFilter[0][1].indexOf(rec.lblAccountHolder.text) >= 0){
          return rec;
        }
      });
    } else { //single filter selected
    }
    return dataToShow;
  },
  showFilterForAccountsSectionClickAddUser : function(option,event,context){
    if(option === 1)
      this.view.flxOwnershipFilterAddUser.setVisibility(false);
    else if(option === 2)
      this.view.flxAccountsFilterAddUser.setVisibility(false);
    var filterWidget = (option === 1) ? this.view.flxAccountsFilterAddUser :this.view.flxOwnershipFilterAddUser;
    var filterIcon = (option === 1) ? "lblIconFilterAccType":"lblIconSortAccHolder" ;
    
    var flxRight = context.widgetInfo.frame.width - event.frame.x - event.frame.width;
    var iconRight = event.frame.width - event[filterIcon].frame.x;
    filterWidget.right = (flxRight + iconRight - 22) + "dp";
    filterWidget.top =(this.view.enrollEditAccountsCard.flxCardBottomContainer.frame.y + 40) +"dp";
    if(filterWidget.isVisible){
      filterWidget.setVisibility(false);
    } else{
      filterWidget.setVisibility(true);
    }
  },
  onClickAccountsEditUserAccess : function(segmentPath, isHeader,eventObj,context){
    this.onCheckAccountsCheckboxAddUser(this.view.enrollEditAccountsCard.segAccountFeatures,false,eventObj,context);
    this.updateAccountSelectionEditUser(eventObj);
  },
  onCheckAccountsCheckboxAddUser : function(segmentPath, isHeader,eventObj,context) {
    var selSecInd = context.sectionIndex;
    var segData = segmentPath.data;
    var segSecData = segData[selSecInd][0];
    var rowsData = segData[selSecInd][1];
    var selectedRowsCount = 0;
    //on section checkbox click
    if(isHeader){
      var img = (segSecData.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal) ?
          this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
      segSecData.imgSectionCheckbox.src = img;
      var count = 0;
      for(var i=0; i<rowsData.length; i++){
        rowsData[i].imgCheckbox.src =img;
        if(img === this.AdminConsoleCommonUtils.checkboxSelected)
          count =count +1;
      }
      //get selcted accounts count only for accounts of bulk update
      if(this.view.flxBulkUpdateFeaturesPopupForAddUser.isVisible === true){
        var selectedRowsCount = img === this.AdminConsoleCommonUtils.checkboxnormal ? "0" : "" + rowsData.length;
        segSecData.lblCountActions = selectedRowsCount;
      }else if(this.view.flxAddedUserEditAccountsContainer.isVisible === true){ //in edit user accounts
        this.view.enrollEditAccountsCard.lblCount.text = this.getTwoDigitNumber(count);
      }
      segmentPath.setSectionAt([segSecData,rowsData],selSecInd);
    } 
    //on row checkbox click
    else{ 
      var selInd = segmentPath.selectedRowIndex[1];
      rowsData[selInd].imgCheckbox.src = (rowsData[selInd].imgCheckbox.src === "checkboxnormal.png") ? "checkboxselected.png" : "checkboxnormal.png";
      segSecData.imgSectionCheckbox.src = this.getHeaderCheckboxImage(rowsData,true,true);
      //get selcted accounts count only for accounts of bulk update
      var selectedRowsCount = this.getSelectedActionsCount(rowsData);
      if(this.view.flxBulkUpdateFeaturesPopupForAddUser.isVisible === true){
        segSecData.lblCountActions = selectedRowsCount;
      }else if(this.view.flxAddedUserEditAccountsContainer.isVisible === true){ //in edit user accounts
        this.view.enrollEditAccountsCard.lblCount.text = this.getTwoDigitNumber(selectedRowsCount);
      }
      segmentPath.setSectionAt([segSecData,rowsData],selSecInd);
    } 
    //enable/disable save buttons
    var isValid = this.validateCheckboxSelections(segmentPath,true);
    if(this.view.flxBulkUpdateFeaturesPopupForAddUser.isVisible === true){ //in bulk update popup
      this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen1.btnSave,true,isValid);
    } else if(segmentPath.id === "segRelatedContractsList"){ //for select related contracts seg
      this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsRelatedDetails.btnSave,true,isValid);
    } else{ // for edit accounts
      this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnSave,true,isValid);
      this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnNext,false,isValid);
      this.enableOrDisableVerticalTabEditUser(isValid);
    }   
  },
  updateAccountSelectionEditUser : function(eventObj){
    var selectedCustomerData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var accountSegData = this.view.enrollEditAccountsCard.segAccountFeatures.data[0][1];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selectedCustomerData.orignalData.coreCustomerId);
    var currUserAccMap = editUserObj.accounts;
    var count =0;
    for(var i=0; i< accountSegData.length; i++){
      var accObj = currUserAccMap.get(accountSegData[i].id);
      if(accountSegData[i].imgCheckbox.src === this.AdminConsoleCommonUtils.checkboxSelected){
        accObj.isEnabled = "true";
        accObj.isAssociated = "true";
        count =count+1;
      }else{
        accObj.isEnabled = "false";
        accObj.isAssociated = "false";
      }
      currUserAccMap.set(accountSegData[i].id,accObj);
    }
    this.selAccCount = count;
    editUserObj.accounts = currUserAccMap;
    this.presenter.setAccountsFeaturesForAddUser(selectedCustomerData.orignalData.coreCustomerId,editUserObj);
  },
  toggleFeaturesCustomerLevel : function(){
    this.view.toggleButtonsFeatures.info.selectedTab = 1;
    this.view.addedUserEditFeaturesCard.lblHeading.text =  kony.i18n.getLocalizedString("i18n.frmEnrollCustomersController.SelectedFeatures")+":";
    this.toggleButtonsUtilFunction([this.view.toggleButtonsFeatures.btnToggleLeft,this.view.toggleButtonsFeatures.btnToggleRight],1);
    this.view.btnBulkUpdateFeatures.setVisibility(false);
    this.view.flxAddedUserEditFeaturesList.setVisibility(true);
    this.view.flxAddedUserEditAccFeaturesList.setVisibility(false);
    this.view.flxAddedUserEditFeatureFilter.setVisibility(false);
    this.view.forceLayout();
    this.view.flxAddedUserEditFeaturesList.height = (this.view.flxAddedUserEditFeatureButtons.frame.y - 170)+"dp" ;
    this.view.flxAddedUserEditFeaturesList.setContentOffset({x:0,y:0});
    this.createFeatureCardForCustomers();
  },
  toggleFeaturesAccountLevel : function(){
    this.view.toggleButtonsFeatures.info.selectedTab = 2;
    this.toggleButtonsUtilFunction([this.view.toggleButtonsFeatures.btnToggleLeft,this.view.toggleButtonsFeatures.btnToggleRight],2);
    this.view.btnBulkUpdateFeatures.setVisibility(true);
    this.view.flxAddedUserEditFeaturesList.setVisibility(false);
    this.view.flxAddedUserEditAccFeaturesList.setVisibility(true);
    this.view.flxAddedUserEditFeatureFilter.setVisibility(true);
    this.createFeatureCardForAccounts();
    this.setAccountTypesFilter(1);
    this.view.forceLayout();
    this.view.flxAddedUserEditAccFeaturesList.height = (this.view.flxAddedUserEditFeatureButtons.frame.y - 170)+"dp" ;
    this.view.flxAddedUserEditAccFeaturesList.setContentOffset({x:0,y:0});
    this.setFilterDataInFeaturesLimitsTab();
  },
  toggleLimitsCustomerLevel : function(){
    this.view.toggleButtonsLimits.info.selectedTab = 1;
    this.toggleButtonsUtilFunction([this.view.toggleButtonsLimits.btnToggleLeft,this.view.toggleButtonsLimits.btnToggleRight],1);
    this.view.btnBulkUpdateLimits.setVisibility(false);
    this.view.flxAddedUserEditLimitsList.setVisibility(true);
    this.view.flxAddedUserEditAccLimitsList.setVisibility(false);
    this.view.flxAddedUserEditLimitsFilter.setVisibility(false);
    this.view.flxAddedUserEditLimitsSearchFilterCont.setVisibility(false);
    this.view.forceLayout();
    this.view.flxAddedUserEditLimitsList.height = (this.view.flxAddedUserEditLimitsButtons.frame.y - 170)+"dp" ;
    this.view.flxAddedUserEditLimitsList.setContentOffset({x:0,y:0});
    this.createLimitCardForCustomers();
  },
  toggleLimitsAccountLevel : function(){
    this.view.toggleButtonsLimits.info.selectedTab = 2;
    this.toggleButtonsUtilFunction([this.view.toggleButtonsLimits.btnToggleLeft,this.view.toggleButtonsLimits.btnToggleRight],2);
    this.view.btnBulkUpdateLimits.setVisibility(true);
    this.view.flxAddedUserEditLimitsList.setVisibility(false);
    this.view.flxAddedUserEditAccLimitsList.setVisibility(true);
    this.view.flxAddedUserEditLimitsFilter.setVisibility(true);
    this.view.flxAddedUserEditLimitsSearchFilterCont.setVisibility(true);
    this.view.forceLayout();
    this.setAccountTypesFilter(2);
    this.view.flxAddedUserEditAccLimitsList.height = (this.view.flxAddedUserEditLimitsButtons.frame.y - 170)+"dp" ;
    this.view.flxAddedUserEditAccLimitsList.setContentOffset({x:0,y:0});
    this.createLimitsCardForAccounts();
    this.setFilterDataInFeaturesLimitsTab(2);
  },
  createFeatureCardForCustomers : function(){
    this.view.addedUserEditFeaturesCard.toggleCollapseArrow(true);
    this.view.addedUserEditFeaturesCard.flxArrow.setVisibility(false);
    this.view.addedUserEditFeaturesCard.flxSelectAllOption.setVisibility(true);
    this.view.addedUserEditFeaturesCard.lblName.skin = "sknLbl117EB0LatoReg14px";
    this.view.addedUserEditFeaturesCard.lblName.hoverSkin = "sknLbl117EB0LatoReg14pxHov";
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    this.storeActionForAccountSelection();
    this.view.addedUserEditFeaturesCard.lblTotalCount.text = "of "+ this.getTwoDigitNumber(editUserObj.accLevelFeatures.length);
    this.view.addedUserEditFeaturesCard.segAccountFeatures.info = {"parentId":"addedUserEditFeaturesCard","segData":[],"featuresType":1, "segDataJSON":{}};
    this.setFeaturesCardSegmentData(this.view.addedUserEditFeaturesCard.segAccountFeatures, editUserObj.accLevelFeatures);
    this.view.addedUserEditFeaturesCard.flxCheckbox.onClick = this.onSelectAllFeaturesClickAddUser.bind(this,this.view.addedUserEditFeaturesCard);
    this.view.addedUserEditFeaturesCard.imgSectionCheckbox.src = this.getHeaderCheckboxImage(this.view.addedUserEditFeaturesCard.segAccountFeatures.data,false, true);
  },
  createFeatureCardForAccounts : function(){
    var self =this;
    var i=0;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var accLevelFeaturesMap = editUserObj.accountMapFeatures;
    this.view.flxAddedUserEditAccFeaturesList.removeAll();
    var compWidth = this.view.flxAddedUserEditFeaturesContainer.frame.width -40;
    accLevelFeaturesMap.forEach(function(valueObj,key){
      //show only accounts that have been selected
      if(valueObj.accountDetails.isEnabled === "true" || valueObj.accountDetails.isAssociated === "true"){
        var num = i>10 ? i : "0"+i;
        var featureCardToAdd = new com.adminConsole.contracts.accountsFeaturesCard({
          "id": "featureCard" +num,
          "isVisible": true,
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "left":"20dp",
          "width":compWidth+"dp",
          "top": "15dp"
        }, {}, {});
        i=i+1;
        featureCardToAdd.flxArrow.onClick = self.toggleCardListVisibility.bind(self,featureCardToAdd,self.view.flxAddedUserEditAccFeaturesList);
        featureCardToAdd.flxSelectAllOption.isVisible = true;
        featureCardToAdd.flxCheckbox.onClick = self.onSelectAllFeaturesClickAddUser.bind(self,featureCardToAdd);
        self.view.flxAddedUserEditAccFeaturesList.add(featureCardToAdd);
        featureCardToAdd.segAccountFeatures.info = {"parentId":featureCardToAdd.id,"segData":[],"featuresType":2, "segDataJSON":{}};
        self.setAccountFeatureCardData(featureCardToAdd, valueObj);
      }
    });
  },
  createLimitCardForCustomers : function(){
    this.view.addedUserEditLimitsCard.flxCardBottomContainer.setVisibility(true);
    this.view.addedUserEditLimitsCard.flxArrow.setVisibility(false);
    this.view.addedUserEditLimitsCard.toggleCollapseArrow(true);
    this.view.addedUserEditLimitsCard.lblName.skin = "sknLbl117EB0LatoReg14px";
    this.view.addedUserEditLimitsCard.lblName.hoverSkin = "sknLbl117EB0LatoReg14pxHov";
    this.createLimitsRowsForCustLevel();
  },
  createLimitsRowsForCustLevel : function(){
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var limitGroups = editUserObj.limitGroups;
    this.view.addedUserEditLimitsCard.btnReset.onClick = this.resetLimitGroupValuesEditUser;
    this.view.addedUserEditLimitsCard.segAccountFeatures.setVisibility(false);
    this.view.addedUserEditLimitsCard.reportPagination.setVisibility(false);
    this.view.addedUserEditLimitsCard.flxDynamicWidgetsContainer.setVisibility(true);
    this.view.addedUserEditLimitsCard.flxDynamicWidgetsContainer.removeAll();
    for(var i=0; i<limitGroups.length; i++){
      //ignore third type of limit group
      if(limitGroups[i].limitGroupId.indexOf("SINGLE_PAYMENT") < 0){
      var num = i>10 ? i : "0"+i;
      var limitRowToAdd = this.view.flxEnrollEditLimitsTemplate.clone(num);
      limitRowToAdd.isVisible = true;  
        this.view.addedUserEditLimitsCard.flxDynamicWidgetsContainer.add(limitRowToAdd);
        this.setDataToCustLimitTextBox(limitGroups[i], num);
      this.view.addedUserEditLimitsCard[num+"lblLimitsHeading"].text = (limitGroups[i].limitGroupId.indexOf("SINGLE_PAYMENT") >= 0) ?
          "SINGLE TRANSACTION LIMITS" :"BULK TRANSACTION LIMITS";
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue1"].onTouchStart = this.showHideRange.bind(this,num,"1",true);
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue1"].onEndEditing = this.showHideRange.bind(this,num,"1",false);
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue2"].onTouchStart = this.showHideRange.bind(this,num,"2",true);
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue2"].onEndEditing = this.showHideRange.bind(this,num,"2",false);
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue3"].onTouchStart = this.showHideRange.bind(this,num,"3",true);
      this.view.addedUserEditLimitsCard[num+"tbxLimitValue3"].onEndEditing = this.showHideRange.bind(this,num,"3",false);
       /* this.view.enrollEditLimitsCard[num+"tbxLimitValue1"].onTextChange = this.updateCustLimitGroupValueEditUser.bind(this,num,"1");
        this.view.enrollEditLimitsCard[num+"tbxLimitValue2"].onTextChange = this.updateCustLimitGroupValueEditUser.bind(this,num,"2");
        this.view.enrollEditLimitsCard[num+"tbxLimitValue3"].onTextChange = this.updateCustLimitGroupValueEditUser.bind(this,num,"3");*/
      }
    }
  },
  showHideRange : function(id,rowNum,option){
    if(option === true){
      this.view.addedUserEditLimitsCard[id+"flxRangeCont"+rowNum].isVisible = true;
      this.view.addedUserEditLimitsCard[id+"flxLimitValue"+rowNum].skin = "sknFlxBorder117eb0radius3pxbgfff"; //set focus skin
    } else{
      this.view.addedUserEditLimitsCard[id+"flxRangeCont"+rowNum].isVisible = false;
      this.view.addedUserEditLimitsCard[id+"flxLimitValue"+rowNum].skin = "sknFlxbgFFFFFFbdrD7D9E0rd3px";
    }
    this.view.forceLayout();
  },
  setDataToCustLimitTextBox : function(limitsGroup, num){
    for(var i=0; i<limitsGroup.limits.length; i++){
      if(limitsGroup.limits[i].id === this.limitId.MAX_TRANSACTION_LIMIT){
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue1"].text = limitsGroup.limits[i].value;
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue1"].info = {"maxValue":limitsGroup.limits[i].maxValue,
                                                                     "id":limitsGroup.limitGroupId,
                                                                     "type": limitsGroup.limits[i].id};
        this.view.addedUserEditLimitsCard[num+"lblRangeValue1"].text = " 0 - " +limitsGroup.limits[i].maxValue;
      } 
      else if(limitsGroup.limits[i].id === this.limitId.DAILY_LIMIT){
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue2"].text = limitsGroup.limits[i].value;
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue2"].info = {"maxValue":limitsGroup.limits[i].maxValue,
                                                                     "id":limitsGroup.limitGroupId,
                                                                     "type": limitsGroup.limits[i].id};
        this.view.addedUserEditLimitsCard[num+"lblRangeValue2"].text = " 0 - " +limitsGroup.limits[i].maxValue;
      } 
      else if(limitsGroup.limits[i].id === this.limitId.WEEKLY_LIMIT){
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue3"].text = limitsGroup.limits[i].value;
        this.view.addedUserEditLimitsCard[num+"tbxLimitValue3"].info = {"maxValue":limitsGroup.limits[i].maxValue,
                                                                     "id":limitsGroup.limitGroupId,
                                                                     "type": limitsGroup.limits[i].id};
        this.view.addedUserEditLimitsCard[num+"lblRangeValue3"].text = " 0 - " +limitsGroup.limits[i].maxValue;
      }
    }
  },
  updateCustLimitGroupValueEditUser : function(flxId, rowId, eventObj){
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var limitGroup = editUserObj.limitGroups;
    for(var i=0;i<limitGroup.length;i++){
      if(limitGroup[i].limitGroupId === eventObj.info.id){
        for(var j=0; j<limitGroup[i].limits.length; j++){
          if(limitGroup[i].limits[j].id === eventObj.info.type)
            limitGroup[i].limits[j].value = parseFloat(eventObj.text);
        }
       /* if(eventObj.info.type === this.limitId.MAX_TRANSACTION_LIMIT){
          
        } else if(eventObj.info.type === this.limitId.DAILY_LIMIT){
          limitGroup[i].limits[1].value = parseFloat(eventObj.text);
        } else if(eventObj.info.type === this.limitId.WEEKLY_LIMIT){
          limitGroup[i].limits[2].value = parseFloat(eventObj.text);
        }*/
      }
    }
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  validateLimitGroupEditUser : function(){
    var errorCustId =[];
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var limitGroups = editUserObj.limitGroups;
    var custId = selCustData.orignalData.coreCustomerId;
    var childFlex = this.view.addedUserEditLimitsCard.flxDynamicWidgetsContainer.widgets();
    //var id = childFlex.id.substr(0,2);
    var limitValidationObj = {};
    limitValidationObj[custId] = {};
    for(var i=0;i <limitGroups.length; i++){
      var errorJson = this.validateLimitGroupValuesEditUserObj(limitGroups[i]);
      limitValidationObj[custId][limitGroups[i].limitGroupId] = errorJson.error;
      //save the cust id with error
      if(errorJson.isValid === false && errorCustId.indexOf(custId) < 0){
        errorCustId.push(custId);
      }
    }
    this.limitGroupsValidationObject = limitValidationObj;
    //not valid
    if(errorCustId.length > 0){
      
    }else{ //valid
      
    }
    var allLimitsValid = errorCustId.length > 0 ? false : true;
//     this.enableOrDisableVerticalTabEditUser(allLimitsValid);
//     this.setErrorLimitGroupEditUser();
    return allLimitsValid;
  },
  validateAllLimitsEditUser : function(){
    var custArr =[],isValid = true;
    var dropdownSelectedCust = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    //get all the customer id's available
    if(this.enrollAction === this.actionConfig.editUser){
      var limitsDropdown = this.view.customersDropdownLimits.segList.data;
      for(var i=0;i<limitsDropdown.length; i++){
        custArr.push(limitsDropdown[i].id);
      }
    }else{
       custArr.push(dropdownSelectedCust);
    }
    //validate for each customer id
    for(var j=0; j<custArr.length;j++){
      var isLimitGroupValid = this.validateLimitGroupEditUser(custArr[j]);
      var isLimitValid = this.validateAccLimitEditUser(custArr[j]);
      if(isLimitGroupValid === false ){ //customer tab in limits
        if(this.enrollAction === this.actionConfig.editUser){  //update the customer selected in dropdown
          this.setSelectedTextFromDropdownEditUser(this.view.customersDropdownLimits, custArr[j]);
        }
        //show the limits screen and respective tab
        if(this.view.flxEnrollEditAccLimitsList.isVisible === false){
          this.showEditLimitsScreen(1);
        }else{
          this.toggleLimitsCustomerLevel();
        }
        this.setErrorLimitGroupEditUser(custArr[j]);
        isValid = false;
        break;
      } else if(isLimitValid === false){ //accounts tab in limits
        if(this.enrollAction === this.actionConfig.editUser){ //update the customer selected in dropdown
          this.setSelectedTextFromDropdownEditUser(this.view.customersDropdownLimits, custArr[j]);
        }
        //show the limits screen and respective tab
        if(this.view.flxEnrollEditAccLimitsList.isVisible === false){
          this.showEditLimitsScreen(2);
        } else{
          this.toggleLimitsAccountLevel()
        }
        this.setErrorForLimitValuesAfterValidation(custArr[j]);
        isValid = false;
        break;
      }
    }
    return isValid;
  },
  resetLimitGroupValuesEditUser : function(){
    var self = this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var limitGroups = editUserObj.limitGroups;
    //update the actual values in edit obj
    for(var i=0; i<limitGroups.length; i++){
      for(var j=0; j<limitGroups[i].limits.length; j++){
        limitGroups[i].limits[j].value = limitGroups[i].actualLimits[j].value;
      }
    }
    editUserObj.limitGroups =limitGroups;
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
    //update the limit values in segment
    this.createLimitsRowsForCustLevel();
  },
  validateLimitGroupValuesEditUserObj : function(limitGroup){
    var errorObj={}, isValid = true;
    var limitGroupId = limitGroup.limitGroupId;
    var limits = limitGroup.limits;
    for(var i=0;i<limits.length;i++){
      if(parseFloat(limits[i].value) > parseFloat(limits[i].maxValue)){
        errorObj[limits[i].id]= {"error":"Value cannot exceed max limit",
                                  "id":limits[i].id};
        isValid = false;
      }else if(limits[i].value === ""){
        errorObj[limits[i].id] = {"error":"Value cannot be empty",
                                  "id":limits[i].id};
        isValid = false;
      }else if(parseFloat(limits[i].value < 0)){
        errorObj[limits[i].id] = {"error":"Value cannot be negative",
                                  "id":limits[i].id};
        isValid = false;
      }
    }
    return {"error":errorObj, "isValid":isValid};
  },
  setErrorLimitGroupEditUser : function(){
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var custId = selCustData.orignalData.coreCustomerId;
    var currCustErrorObj = this.limitGroupsValidationObject[custId];
    var childFlex = this.view.addedUserEditLimitsCard.flxDynamicWidgetsContainer.widgets();
    
    for(var i=0; i<childFlex.length; i++){
      var id = childFlex[i].id.substr(0,2);
      var limitGroupId = this.view.addedUserEditLimitsCard[id+"tbxLimitValue1"].info.id;
      var typeIdArr = Object.keys(currCustErrorObj[limitGroupId]);
      for(var j=0; j<typeIdArr.length; j++){
        if(typeIdArr[j] === this.limitId.MAX_TRANSACTION_LIMIT){
          this.view.addedUserEditLimitsCard[id+"flxLimitError1"].setVisibility(true);
          this.view.addedUserEditLimitsCard[id+"lblLimitErrorMsg1"].text = currCustErrorObj[limitGroupId][typeIdArr[j]].error;
          this.view.addedUserEditLimitsCard[id+"tbxLimitValue1"].skin = "sknFlxCalendarError";                                                                                           
        }else if(typeIdArr[j] === this.limitId.DAILY_LIMIT){
          this.view.addedUserEditLimitsCard[id+"flxLimitError2"].setVisibility(true);
          this.view.addedUserEditLimitsCard[id+"lblLimitErrorMsg2"].text = currCustErrorObj[limitGroupId][typeIdArr[j]].error;
          this.view.addedUserEditLimitsCard[id+"tbxLimitValue2"].skin = "sknFlxCalendarError";     
        }else if(typeIdArr[j] === this.limitId.WEEKLY_LIMIT){
          this.view.addedUserEditLimitsCard[id+"flxLimitError3"].setVisibility(true);
          this.view.addedUserEditLimitsCard[id+"lblLimitErrorMsg3"].text = currCustErrorObj[limitGroupId][typeIdArr[j]].error;
          this.view.addedUserEditLimitsCard[id+"tbxLimitValue3"].skin = "sknFlxCalendarError";     
        }
      }
    }
    this.view.forceLayout();
  },
  validateAccLimitEditUser : function(custId){
    var self = this;
    var limitsValidation = {},errorCustIdArr =[];
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var custId = selCustData.orignalData.coreCustomerId;
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(custId);
    var currUserLimitsMap = editUserObj.accLevelLimits;
    limitsValidation[custId] = {};
    currUserLimitsMap.forEach(function(accountObj, key){
      limitsValidation[custId][key] = {};
      if(accountObj.accountDetails.isEnabled === "true" || accountObj.accountDetails.isAssociated === "true"){
        var featuresLimit = JSON.parse(accountObj.limits);
        for(var i=0;i<featuresLimit.length;i++){
          var currActions = featuresLimit[i].actions || featuresLimit[i].permissions;
          for(var j=0;j<currActions.length; j++){
            var errorObj = self.validateAccLimitValuesEditUserObj(currActions[j].limits);
            limitsValidation[custId][key][currActions[j].actionId] = errorObj.error;
            //save custId is invalid
            if(errorObj.isValid === false && errorCustIdArr.indexOf(custId) < 0){
              errorCustIdArr.push(custId)
            }
          }
        }
      }
    });
    this.limitsValidationObject = limitsValidation;
    //not valid
    if(errorCustIdArr.length > 0){
      
    }else{ //valid
      
    }
    var allActionsValid = errorCustIdArr.length > 0 ? false : true;
    return allActionsValid;
  },
  validateAccLimitValuesEditUserObj : function(limitObj){
    var errorObj = {}, isValid = true;;
    var negativeErrorText=kony.i18n.getLocalizedString("i8n.frmCreateCustomerController.warning.value_cannot_negative");
    var emptyValueText= kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Value_cannot_be_empty");
    // cunvert currency format to float
    var max_trans_limit=parseFloat((limitObj[this.limitId.MAX_TRANSACTION_LIMIT]).replace(/[^0-9.-]+/g,"")); 
    var max_daily_limit=parseFloat((limitObj[this.limitId.DAILY_LIMIT]).replace(/[^0-9.-]+/g,"")); 
    var max_weekly_limit=parseFloat((limitObj[this.limitId.WEEKLY_LIMIT]).replace(/[^0-9.-]+/g,""));
    
    //per transaction - auto deny
    var perTranAD ={};
    if(limitObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT] === "" || parseFloat(limitObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT])<0){
      perTranAD.error = limitObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT]===""? emptyValueText : negativeErrorText;
      isValid=false;
      perTranAD.typeId = this.limitId.AUTO_DENIED_TRANSACTION_LIMIT;
      errorObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT] = perTranAD;
    }
    else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT])> max_trans_limit){
      perTranAD.typeId = this.limitId.AUTO_DENIED_TRANSACTION_LIMIT;
      perTranAD.error = "Value cannot be greater than transaction limit";
      isValid = false;
      errorObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT] = perTranAD;
    }
    else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT]) < parseFloat(limitObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT])){
      perTranAD.typeId = this.limitId.AUTO_DENIED_TRANSACTION_LIMIT;
      perTranAD.error = "Value cannot be less than transaction limit";
      isValid = false;
      errorObj[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT] = perTranAD;
    }
    
    //per transaction - pre approved
    var perTranPA ={};
    if(limitObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT] === "" || parseFloat(limitObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT])<0){
      perTranPA.typeId = this.limitId.PRE_APPROVED_TRANSACTION_LIMIT;
      perTranPA.error = limitObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT]===""? emptyValueText : negativeErrorText;
      isValid=false;
      errorObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT] = perTranPA;
    }
    else if(parseFloat(limitObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT])> max_trans_limit){
      perTranPA.typeId = this.limitId.PRE_APPROVED_TRANSACTION_LIMIT;
      perTranPA.error = "Value cannot be greater than transaction limit";
      isValid = false;
      errorObj[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT] = perTranPA;
    }
    
     //daily - auto deny
    var dailyAD = {};
    if(limitObj[this.limitId.AUTO_DENIED_DAILY_LIMIT]==="" || parseFloat(limitObj[this.limitId.AUTO_DENIED_DAILY_LIMIT])<0){
      dailyAD.typeId = this.limitId.AUTO_DENIED_DAILY_LIMIT;
      dailyAD.error = limitObj[this.limitId.AUTO_DENIED_DAILY_LIMIT]===""?emptyValueText : negativeErrorText;
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_DAILY_LIMIT] = dailyAD;
    }else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_DAILY_LIMIT])>max_daily_limit){
      dailyAD.typeId = this.limitId.AUTO_DENIED_DAILY_LIMIT;
      dailyAD.error = "Value cannot be greater than transaction limit";
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_DAILY_LIMIT] = dailyAD;
    }else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_DAILY_LIMIT])<parseFloat(limitObj[this.limitId.PRE_APPROVED_DAILY_LIMIT])){
      dailyAD.typeId = this.limitId.AUTO_DENIED_DAILY_LIMIT;
      dailyAD.error = "Value cannot be less than transaction limit";
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_DAILY_LIMIT] = dailyAD;
    }

    //daily - pre approved
    var dailyPA = {};
    if(limitObj[this.limitId.PRE_APPROVED_DAILY_LIMIT]==="" || parseFloat(limitObj[this.limitId.PRE_APPROVED_DAILY_LIMIT])<0){
      dailyPA.error = limitObj[this.limitId.PRE_APPROVED_DAILY_LIMIT]==="" ? emptyValueText : negativeErrorText;
      dailyPA.typeId = this.limitId.PRE_APPROVED_DAILY_LIMIT;
      isValid=false;
      errorObj[this.limitId.PRE_APPROVED_DAILY_LIMIT] = dailyPA;
    }else if(parseFloat(limitObj[this.limitId.PRE_APPROVED_DAILY_LIMIT]) > max_daily_limit){
      dailyPA.typeId = this.limitId.PRE_APPROVED_DAILY_LIMIT;
      dailyPA.error = "Value cannot be greater than transaction limit";
      isValid=false;
      errorObj[this.limitId.PRE_APPROVED_DAILY_LIMIT] = dailyPA;
    }
    
    //weekly- auto deny
    var weeklyAD = {};
    if(limitObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT]==="" || parseFloat(limitObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT])<0){
      weeklyAD.typeId = this.limitId.AUTO_DENIED_WEEKLY_LIMIT;
      weeklyAD.error = limitObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT]==="" ? emptyValueText : negativeErrorText;
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT] = weeklyAD;
    }else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT])>max_weekly_limit){
      weeklyAD.typeId = this.limitId.AUTO_DENIED_WEEKLY_LIMIT;
      weeklyAD.error = "Value cannot be greater than transaction limit";
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT] = weeklyAD;
    }else if(parseFloat(limitObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT])<parseFloat(limitObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT])){
      weeklyAD.typeId = this.limitId.AUTO_DENIED_WEEKLY_LIMIT;
      weeklyAD.error = "Value cannot be less than transaction limit";
      isValid=false;
      errorObj[this.limitId.AUTO_DENIED_WEEKLY_LIMIT] = weeklyAD;
    }
    
    //weekly- pre approved
    var weeklyPA = {};
    if(limitObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT]==="" || parseFloat(limitObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT])<0){
      weeklyPA.typeId = this.limitId.PRE_APPROVED_WEEKLY_LIMIT;
      weeklyPA.error = limitObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT]==="" ? emptyValueText : negativeErrorText;
      isValid=false;
      errorObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT] = weeklyPA;
    }else if(parseFloat(limitObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT])>max_weekly_limit){
      weeklyPA.typeId = this.limitId.PRE_APPROVED_WEEKLY_LIMIT;
      weeklyPA.error = "Value cannot be greater than transaction limit";
      isValid=false;
      errorObj[this.limitId.PRE_APPROVED_WEEKLY_LIMIT] = weeklyPA;
    }
    
    return {"error":errorObj,"isValid":isValid};
  },
  setErrorForLimitValuesAfterValidation : function(){
    var isValid = true;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var custId = selCustData.orignalData.coreCustomerId;
    var limitErrorForCust = this.limitsValidationObject[custId];
    var accountFlx = this.view.flxAddedUserEditAccLimitsList.widgets();
    for(var i=0;i<accountFlx.length;i++){
      var accfeatures = limitErrorForCust[accountFlx[i].info.accDetails.accountId];
      var segData = accountFlx[i].segAccountFeatures.data;
      for(var j=0; j<segData.length; j++){
        var errorActionsId = Object.keys(accfeatures);
        for(var a=0; a<segData[j][1].length; a++){
          var updatedRowJson;
          //var actionHasError = Object.keys(accfeatures[segData[j][1][a].id]).length > 0 ? true : false;
          if(errorActionsId.indexOf(segData[j][1][a].id) >= 0){
            updatedRowJson = this.updatedRowWithErrorCheck(segData[j][1][a],accfeatures);
            segData[j][1][a] = updatedRowJson;
            isValid = false;
          }
        }
      }
      accountFlx[i].segAccountFeatures.setData(segData);
    }
    this.view.forceLayout();
    return isValid;
  },
  updatedRowWithErrorCheck : function(rowData, errorObj){
    var currActionError = errorObj[rowData.id];
    var currErrorLimitArr = Object.keys(currActionError);
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.PRE_APPROVED_DAILY_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.PRE_APPROVED_DAILY_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.AUTO_DENIED_DAILY_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.AUTO_DENIED_DAILY_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.PRE_APPROVED_DAILY_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.AUTO_DENIED_WEEKLY_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.AUTO_DENIED_WEEKLY_LIMIT].error;
    }
    if(currErrorLimitArr.indexOf(currActionError[this.limitId.PRE_APPROVED_WEEKLY_LIMIT]) >= 0){
      rowData.flxColumn11.isVisible =true;
      rowData.lblErrorMsg11.text = currActionError[this.limitId.PRE_APPROVED_WEEKLY_LIMIT].error;
    }
    return rowData;
  },
  createLimitsCardForAccounts : function(){
    var self =this;
    this.view.flxAddedUserEditAccLimitsList.removeAll();
    var i=0;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var accLevelLimitsMap = editUserObj.accLevelLimits;
    var compWidth = this.view.flxAddedUserEditLimitsContainer.frame.width -40;
    accLevelLimitsMap.forEach(function(accountObj,accKey) {
      //show only accounts that have been selected
      if(accountObj.accountDetails.isEnabled === "true" || accountObj.accountDetails.isAssociated === "true"){
        var num = i>10 ? i : "0"+i;
        var limitCardToAdd = new com.adminConsole.contracts.accountsFeaturesCard({
          "id": "limitCard" +num,
          "isVisible": true,
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "top": "10px",
          "left":"20dp",
          "width": compWidth
        }, {}, {});
        i=i+1;
        limitCardToAdd.flxCardBottomContainer.isVisible = false;
        limitCardToAdd.toggleCollapseArrow(false);
        limitCardToAdd.flxArrow.onClick = self.toggleCardListVisibility.bind(self,limitCardToAdd,self.view.flxAddedUserEditAccLimitsList);
        limitCardToAdd.segAccountFeatures.info = {"parentId":limitCardToAdd.id,"segData":[], "segDataJSON":{}};
        self.view.flxAddedUserEditAccLimitsList.add(limitCardToAdd);
        self.setAccountLimitCardData(limitCardToAdd,accountObj);
      }
    });
    this.view.forceLayout();
  },
  setAccountLimitCardData : function(limitCardToAdd, accLevelLimitsMap){
    limitCardToAdd.info = {"accDetails":accLevelLimitsMap.accountDetails};
    limitCardToAdd.lblName.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNUMBERColon") + " "+
    accLevelLimitsMap.accountDetails.accountId;
    limitCardToAdd.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE");
    limitCardToAdd.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME");
    limitCardToAdd.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP_TYPE");
    limitCardToAdd.lblData1.text = accLevelLimitsMap.accountDetails.accountType;
    limitCardToAdd.lblData2.text = accLevelLimitsMap.accountDetails.accountName;
    limitCardToAdd.lblData3.text = accLevelLimitsMap.accountDetails.ownerType;
    limitCardToAdd.lblHeading.text =  kony.i18n.getLocalizedString("i18n.frmServiceManagement.Limits");
    limitCardToAdd.lblCount.setVisibility(false);
    limitCardToAdd.lblTotalCount.setVisibility(false);
    limitCardToAdd.toggleCollapseArrow(false);
    limitCardToAdd.lblName.skin = "sknLbl192B45LatoRegular14px";
    limitCardToAdd.lblName.hoverSkin = "sknLbl192B45LatoRegular14px";
    limitCardToAdd.flxArrow.isVisible = true;
    this.setLimitsAtAccountLevel(limitCardToAdd.segAccountFeatures, JSON.parse(accLevelLimitsMap.limits));
  },
  setLimitsAtAccountLevel : function(segmentPath,limitsArr){
    var self =this;
    var limitsSegData = limitsArr.map(function(rec){
      var segRowData = [];
      for(var i=0;i < rec.actions.length; i++){
        var limitValues = rec.actions[i].limits;
        segRowData.push({
          "id":rec.actions[i].actionId,
          "lblFeatureName":rec.actions[i].actionName,
          "isRowVisible": false,
          "flxAssignLimits":{"isVisible":false},
          "lblTransactionType":kony.i18n.getLocalizedString("i18n.frmMFAScenarios.Transaction_Type_Caps"),
          "lblTransactionLimits":"TRANSACTION LIMIT",
          "lblPreApprovedLimit":kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.PreApproved_UC"),
          "lblAutoDenyLimits":kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.AutoDeny_UC"),
          "lblperTransactionLimits":kony.i18n.getLocalizedString("i18n.frmServiceManagement.PerTransactionLimitLC"),
          "lblDailyTransactionLimits":kony.i18n.getLocalizedString("i18n.frmServiceManagement.DailyTransactionLimitLC"),
          "lblWeeklyTransactionLimits":kony.i18n.getLocalizedString("i18n.frmServiceManagement.WeeklyTransLimitLC"),
          "lblPerTransactionLimitValue": {"text":limitValues[self.limitId.MAX_TRANSACTION_LIMIT]},
          "txtPerTransPreApprovedLimit": {"text":limitValues[self.limitId.PRE_APPROVED_TRANSACTION_LIMIT],
                                          "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                          "info":{"name": self.limitId.PRE_APPROVED_TRANSACTION_LIMIT, "rowNum":1}
                                         },
          "txtPerTransAutoDenyLimits": {"text":limitValues[self.limitId.AUTO_DENIED_TRANSACTION_LIMIT],
                                        "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                        "info":{"name": self.limitId.AUTO_DENIED_TRANSACTION_LIMIT, "rowNum":1}
                                       },
          "lblDailyTransactionLimitValue": {"text":limitValues[self.limitId.DAILY_LIMIT]},
          "txtDailyTransPreApprovedLimit": {"text":limitValues[self.limitId.PRE_APPROVED_DAILY_LIMIT],
                                            "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                            "info":{"name": self.limitId.PRE_APPROVED_DAILY_LIMIT, "rowNum":2}
                                           },
          "txtDailyTransAutoDenyLimits": {"text":limitValues[self.limitId.AUTO_DENIED_DAILY_LIMIT],
                                          "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                          "info":{"name": self.limitId.AUTO_DENIED_DAILY_LIMIT, "rowNum":2}
                                         },
          "lblWeeklyTransactionLimitValue": {"text":limitValues[self.limitId.WEEKLY_LIMIT]},
          "txtWeeklyTransPreApprovedLimit": {"text":limitValues[self.limitId.PRE_APPROVED_WEEKLY_LIMIT],
                                             "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                             "info":{"name": self.limitId.PRE_APPROVED_WEEKLY_LIMIT, "rowNum":3}
                                            },
          "txtWeeklyTransAutoDenyLimits": {"text":limitValues[self.limitId.AUTO_DENIED_WEEKLY_LIMIT],
                                           "onTextChange":self.updateAccLimitsValueEditUser.bind(self,segmentPath),
                                           "info":{"name": self.limitId.AUTO_DENIED_WEEKLY_LIMIT, "rowNum":3}
                                          },
          "lblPerTransactionLimitDollar":{"text":self.currencyValue},
          "lblDailyTransactionLimitDollar":{"text":self.currencyValue},
          "lblWeeklyTransactionLimitDollar":{"text":self.currencyValue},
          "lblPerCurrencyPreLimit": {"text":self.currencyValue},
          "lblPerCurrencyADLimits": {"text":self.currencyValue},
          "lblDailyCurrencyPreLimit": {"text":self.currencyValue},
          "lblDailyCurrencyADLimits": {"text":self.currencyValue},
          "lblWeeklyCurrencyADLimits": {"text":self.currencyValue},
          "lblWeeklyCurrencyPreLimit": {"text":self.currencyValue},
          "lblErrorMsg11": {"text":"Error"},
          "lblErrorMsg12": {"text":"Error"},
          "lblErrorMsg21": {"text":"Error"},
          "lblErrorMsg22": {"text":"Error"},
          "lblErrorMsg31" : {"text":"Error"},
          "lblErrorMsg32" : {"text":"Error"},
          "lblErrorIcon11": {"text":"\ue94c"},
          "lblErrorIcon12":{"text":"\ue94c"},
          "lblErrorIcon21":{"text":"\ue94c"},
          "lblErrorIcon22":{"text":"\ue94c"},
          "lblErrorIcon31":{"text":"\ue94c"},
          "lblErrorIcon32":{"text":"\ue94c"},
          "flxErrorRow1":{"isVisible":true},
          "flxErrorRow2":{"isVisible":true},
          "flxErrorRow3":{"isVisible":true},
          "flxColumn11": {"isVisible": false},
          "flxColumn12": {"isVisible": false},
          "flxColumn21": {"isVisible": false},
          "flxColumn22": {"isVisible": false},
          "flxColumn31" : {"isVisible": false},
          "flxColumn32" : {"isVisible": false},
          "lblSeperator1":"-",
          "lblSeperator2":"-",
          "lblSeperator":"-",
          "flxRangeIcon1":{"isVisible":false,"onHover":"" },//self.showRangeTooltip},
          "flxRangeIcon2":{"isVisible":false,"onHover":""},//self.showRangeTooltip},
          "flxRangeIcon3":{"isVisible":false,"onHover":""},//self.showRangeTooltip},
          "lblIconRangeInfo1":{"text":"\ue94d"},
          "lblIconRangeInfo2":{"text":"\ue94d"},
          "lblIconRangeInfo3":{"text":"\ue94d"},
          "template":"flxAssignLimits"});
      }
      var segSecData = {
        "id":rec.featureId,
        "lblTopSeperator": {"isVisible":false},
        "flxCheckbox": {"isVisible":false},
        "flxToggleArrow": {"onClick": self.toggleSegmentSectionArrowAddUser.bind(self,segmentPath)},
        "lblIconToggleArrow": {"text":"\ue922","skin":"sknfontIconDescRightArrow14px"},
        "lblFeatureName": rec.featureName,
        "lblStatusValue": {"text": rec.featureStatus === self.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE ?
                          kony.i18n.getLocalizedString("i18n.permission.Active") : kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive")},
        "lblIconStatusTop": {"text":"\ue921",
                            "skin":rec.featureStatus === self.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE ? "sknFontIconActivate" : "sknfontIconInactive"},
        "lblBottomSeperator": {"isVisible":true,"text":"-"},
        "lblAvailableActions": kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.MonetaryActions")+":",
        "lblCountActions": {"text":rec.actions.length},
        "lblTotalActions":{"isVisible":false},
        "template":"flxContractEnrollFeaturesEditSection"
      };
      return [segSecData, segRowData];
    });
    segmentPath.widgetDataMap = this.getWidgetMapForLimitsAccountLevel();
    segmentPath.info = {"segData":[]};
    limitsSegData[limitsSegData.length-1][0].lblBottomSeperator.isVisible = false;
    segmentPath.setData(limitsSegData);
    this.view.forceLayout();
  },
  getWidgetMapForLimitsAccountLevel : function(){
    var widgetMap = {
      "isRowVisible": "isRowVisible",
      "lblTopSeperator":"lblTopSeperator",
      "flxCheckbox":"flxCheckbox",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxToggleArrow":"flxToggleArrow",
      "lblIconToggleArrow":"lblIconToggleArrow",
      "lblFeatureName":"lblFeatureName",
      "lblStatusValue":"lblStatusValue",
      "lblIconStatusTop":"lblIconStatusTop",
      "lblBottomSeperator":"lblBottomSeperator",
      "lblAvailableActions":"lblAvailableActions",
      "lblCountActions":"lblCountActions",
      "lblTotalActions":"lblTotalActions",
      "flxContractEnrollFeaturesEditSection":"flxContractEnrollFeaturesEditSection",
      "lblFeatureName":"lblFeatureName",
      "lblTransactionType":"lblTransactionType",
      "lblTransactionLimits":"lblTransactionLimits",
      "lblPreApprovedLimit":"lblPreApprovedLimit",
      "lblAutoDenyLimits":"lblAutoDenyLimits",
      "lblPerTransactionLimitDollar":"lblPerTransactionLimitDollar",
      "lblDailyTransactionLimitDollar":"lblDailyTransactionLimitDollar",
      "lblWeeklyTransactionLimitDollar":"lblWeeklyTransactionLimitDollar",
      "lblperTransactionLimits":"lblperTransactionLimits",
      "lblDailyTransactionLimits":"lblDailyTransactionLimits",
      "lblWeeklyTransactionLimits":"lblWeeklyTransactionLimits",
      "lblPerTransactionLimitValue": "lblPerTransactionLimitValue",
      "txtPerTransPreApprovedLimit": "txtPerTransPreApprovedLimit",
      "txtPerTransAutoDenyLimits":"txtPerTransAutoDenyLimits",
      "lblDailyTransactionLimitValue":"lblDailyTransactionLimitValue",
      "txtDailyTransPreApprovedLimit":"txtDailyTransPreApprovedLimit",
      "txtDailyTransAutoDenyLimits": "txtDailyTransAutoDenyLimits",
      "lblWeeklyTransactionLimitValue": "lblWeeklyTransactionLimitValue",
      "txtWeeklyTransPreApprovedLimit": "txtWeeklyTransPreApprovedLimit",
      "txtWeeklyTransAutoDenyLimits":"txtWeeklyTransAutoDenyLimits",
      "flxColumn11": "flxColumn11",
      "flxColumn12": "flxColumn12",
      "flxColumn21": "flxColumn21",
      "flxColumn22": "flxColumn22",
      "flxColumn31" : "flxColumn31",
      "flxColumn32" : "flxColumn32",
      "flxErrorRow1":"flxErrorRow1",
      "flxErrorRow2":"flxErrorRow2",
      "flxErrorRow3":"flxErrorRow3",
      "lblErrorMsg11": "lblErrorMsg11",
      "lblErrorMsg12": "lblErrorMsg12",
      "lblErrorMsg21": "lblErrorMsg21",
      "lblErrorMsg22": "lblErrorMsg22",
      "lblErrorMsg31" : "lblErrorMsg31",
      "lblErrorMsg32" : "lblErrorMsg32",
      "lblErrorIcon11":"lblErrorIcon11",
      "lblErrorIcon12":"lblErrorIcon12",
      "lblErrorIcon21":"lblErrorIcon21",
      "lblErrorIcon22":"lblErrorIcon22",
      "lblErrorIcon31":"lblErrorIcon31",
      "lblErrorIcon32":"lblErrorIcon32",
      "lblPerCurrencyPreLimit":"lblPerCurrencyPreLimit",
      "lblPerCurrencyADLimits":"lblPerCurrencyADLimits",
      "lblDailyCurrencyPreLimit":"lblDailyCurrencyPreLimit",
      "lblDailyCurrencyADLimits":"lblDailyCurrencyADLimits",
      "lblWeeklyCurrencyADLimits":"lblWeeklyCurrencyADLimits",
      "lblWeeklyCurrencyPreLimit":"lblWeeklyCurrencyPreLimit",
      "statusValue": "statusValue",
      "statusIcon" :"statusIcon",
      "lblSeperator1":"lblSeperator1",
      "lblSeperator2":"lblSeperator2",
      "lblSeperator":"lblSeperator",
      "lblIconRangeInfo1":"lblIconRangeInfo1",
      "lblIconRangeInfo2":"lblIconRangeInfo2",
      "lblIconRangeInfo3":"lblIconRangeInfo3",
      "flxRangeIcon1":"flxRangeIcon1",
      "flxRangeIcon2":"flxRangeIcon2",
      "flxRangeIcon3":"flxRangeIcon3",
      "flxPerTransactionCont":"flxPerTransactionCont",
      "flxDailyTransactionCont":"flxDailyTransactionCont",
      "flxWeeklyTransactionCont":"flxWeeklyTransactionCont",
      "flxAssignLimits":"flxAssignLimits"
    };
    return widgetMap;
  },
  updateAccLimitsValueEditUser : function(segmentWidPath,eventObj){
    var self = this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var featureData = segmentWidPath.data[eventObj.rowContext.sectionIndex][0];
    var actionsSegData = segmentWidPath.data[eventObj.rowContext.sectionIndex][1]
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var currUserLimitsMap = new Map(editUserObj.accLevelLimits);
    var currLimitTextbox = eventObj.info ? eventObj.info.name : "";


    currUserLimitsMap.forEach(function(accountObj,accKey){ 
      var limits = JSON.parse(accountObj.limits);
      var selFeature = self.getFeatureObjFromArray(featureData.id, limits);

        for(var i=0; i< selFeature.actions.length; i++){
          for(var j=0; j<actionsSegData.length; j++){
            //compare with segment action ,action from list and update limit value
            if(selFeature.actions[i].actionId === actionsSegData[j].id){
              if(selFeature.actions[i].limits && currLimitTextbox){
                selFeature.actions[i].limits[currLimitTextbox] = eventObj.text;
              }
              break;
            }
          }
        }
        accountObj.limits = JSON.stringify(limits);
        currUserLimitsMap.set(accKey,accountObj);
      });
      editUserObj.accLevelLimits = currUserLimitsMap;
      this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  toggleCardListVisibility : function(cardWidget,parentFlexCont){
    var listArr = parentFlexCont.widgets();
    for(var i=0; i<listArr.length; i++){
      if(listArr[i].id === cardWidget.id){
        var visibilityCheck = cardWidget.flxCardBottomContainer.isVisible;
        cardWidget.toggleCollapseArrow(!visibilityCheck);
        //collapses segment section inside the card
        var segData = cardWidget.segAccountFeatures.data;
        for(var j=0;j< segData.length;j++){
          segData[j][0].lblTopSeperator.isVisible = false;
          if(segData[j][0].lblIconToggleArrow.skin !== "sknfontIconDescRightArrow14px"){
            segData[j][0].lblIconToggleArrow.text = "\ue922";
            segData[j][0].lblIconToggleArrow.skin = "sknfontIconDescRightArrow14px";
            segData[j][1] = this.showHideSegRowFlexAddUser(segData[j][1],false);
            if(j === segData.length-1){
              segData[j][0].lblBottomSeperator.isVisible = false;
            }
          }
        }
        cardWidget.segAccountFeatures.setData(segData);
      }
      else{
        this.view[listArr[i].id].toggleCollapseArrow(false);
      }
    }
  },
  setAccountFeatureCardData : function(featureCardToAdd, accLevelFeaturesMap){
    featureCardToAdd.info = {"accDetails":accLevelFeaturesMap.accountDetails};
    featureCardToAdd.lblName.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNUMBERColon") + " "+
    accLevelFeaturesMap.accountDetails.accountId;
    featureCardToAdd.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE");
    featureCardToAdd.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME");
    featureCardToAdd.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP_TYPE");
    featureCardToAdd.lblData1.text = accLevelFeaturesMap.accountDetails.accountType;
    featureCardToAdd.lblData2.text = accLevelFeaturesMap.accountDetails.accountName;
    featureCardToAdd.lblData3.text = accLevelFeaturesMap.accountDetails.ownerType;
    featureCardToAdd.lblHeading.text =  kony.i18n.getLocalizedString("i18n.frmEnrollCustomersController.SelectedFeatures")+":";
    featureCardToAdd.lblTotalCount.setVisibility(true);
    var count = JSON.parse(accLevelFeaturesMap.features).length;
    featureCardToAdd.lblTotalCount.text =  "of "+ this.getTwoDigitNumber(count);
    featureCardToAdd.toggleCollapseArrow(false);
    featureCardToAdd.lblName.skin = "sknLbl192B45LatoRegular14px";
    featureCardToAdd.lblName.hoverSkin = "sknLbl192B45LatoRegular14px";
    featureCardToAdd.flxArrow.isVisible = true;
    this.setFeaturesCardSegmentData(featureCardToAdd.segAccountFeatures, JSON.parse(accLevelFeaturesMap.features));
    featureCardToAdd.imgSectionCheckbox.src = this.getHeaderCheckboxImage(featureCardToAdd.segAccountFeatures.data,false, true);   
  },
  setAccountTypesFilter : function(option){
    var self = this;
    var widgetMap = {
      "id": "id",
      "lblDescription": "lblDescription",
      "imgCheckBox": "imgCheckBox",
      "flxCheckBox":"flxCheckBox",
      "flxSearchDropDown": "flxSearchDropDown"
    };
    var accTypes = [{"name":"Savings","id":"1"},
               {"name":"Checkings","id":"2"},
               {"name":"Credit Card","id":"3"},
               {"name":"Deposits","id":"4"},
               {"name":"Loan","id":"5"},
               {"name":"Current","id":"6"}];
    var segData = accTypes.map(function(rec) {
        return {
          "id": rec.id,
          "lblDescription": {"text":rec.name},
          "imgCheckBox": {"src":self.AdminConsoleCommonUtils.checkboxnormal},
          "template": "flxSearchDropDown"
        };
      });
    this.view.customListBoxAccounts.segList.widgetDataMap = widgetMap;
    this.view.customListBoxAccounts.segList.setData(segData);
    this.view.customListBoxAccounts.segList.info = {"features":segData,"limits":segData};
    var arr = [];
    for(var i= 0; i< segData.length; i++){
      arr.push(i);
    }
    this.view.customListBoxAccounts.segList.selectedIndices = [[0,arr]];
    this.view.customListBoxAccounts.lblSelectedValue.text = segData.length + " Selected";
  },
  setFilterDataInFeaturesLimitsTab : function(){
    var self =this;
    var accTypes = [], listBoxData = [];
    var widgetDataMap = {
      "lblDescription": "lblDescription",
      "imgCheckBox": "imgCheckBox",
      "flxCheckBox":"flxCheckBox",
      "flxSearchDropDown": "flxSearchDropDown"
    };
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var userDetailsObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var accounts = userDetailsObj.accounts;
    //get the accoun types available
    accounts.forEach(function(accountObj,key){
      if(accountObj.isEnabled === "true" || accountObj.isAssociated === "true"){
        if(accTypes.indexOf(accountObj.accountType) < 0){
          accTypes.push(accountObj.accountType);
        }
      }
    });
    //map filtered account types to segmnet
    listBoxData = accTypes.map(function(rec){
      return {
        "lblDescription": rec,
        "imgCheckBox": {"src":self.AdminConsoleCommonUtils.checkboxnormal},
        "template": "flxSearchDropDown"
      };
    });
    this.view.customListBoxAccounts.segList.widgetMap = widgetDataMap;
    this.view.customListBoxAccounts.segList.setData(listBoxData);
    var arr = [];
    for(var i= 0; i< listBoxData.length; i++){
      arr.push(i);
    }
    this.view.customListBoxAccounts.segList.setData.info = {"prevSelInd": JSON.stringify(arr),
                                                            "selectedText": arr.length+" "+kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Selected")};
    this.view.customListBoxAccounts.segList.selectedIndices = [[0,arr]];
    this.view.customListBoxAccounts.lblSelectedValue.text = this.view.customListBoxAccounts.segList.setData.info.selectedText;
    this.view.forceLayout();
  },
  setFeaturesCardSegmentData : function(segmentPath,featuresArr){
    var self =this;
    var segFeaturesData =[];
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var featuresSegData = featuresArr.map(function(rec){
      var segRowData = [], selActionCount = 0;
      for(var i=0;i < rec.actions.length; i++){
        var rowJson = {
          "id":rec.actions[i].actionId,
          "isRowVisible": false,
          "flxContractEnrollFeaturesEditRow":{"isVisible":false},
          "flxFeatureNameCont":{"isVisible":true},
          "imgCheckbox":{"src": rec.actions[i].isEnabled === "true" ? self.AdminConsoleCommonUtils.checkboxSelected :self.AdminConsoleCommonUtils.checkboxnormal},
          "flxCheckbox":{"onClick":self.onClickFeaturesRowCheckboxAddUser.bind(self,segmentPath)},
          "lblFeatureName":{"text":rec.actions[i].actionName},
          "lblStatus":{"text":rec.actions[i].actionStatus === self.AdminConsoleCommonUtils.constantConfig.ACTION_ACTIVE ?"Active" : "Inactive"},
          "lblIconStatus":{"skin":rec.actions[i].actionStatus === self.AdminConsoleCommonUtils.constantConfig.ACTION_ACTIVE ?
                           "sknFontIconActivate" :"sknfontIconInactive"},
          "lblCustom":{"isVisible":false},
          "template":"flxContractEnrollFeaturesEditRow",
        };
        //to set partial selection for action incase of customer level features
        if(segmentPath.info.featuresType === 1){
          var accCount =  self.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][rec.actions[i].actionId] ? self.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][rec.actions[i].actionId].length : 0;
          rowJson.imgCheckbox.src = (accCount === 0)? self.AdminConsoleCommonUtils.checkboxnormal : 
          (accCount < self.selAccCount ? self.AdminConsoleCommonUtils.checkboxPartial : self.AdminConsoleCommonUtils.checkboxSelected);
          if(rowJson.imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxSelected || rowJson.imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxPartial)
            selActionCount = selActionCount +1;
          //show/hide the custom label
          rowJson.lblCustom.isVisible = (rowJson.imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxPartial)?true : false;
        }else {
          if(rec.actions[i].isEnabled === "true") selActionCount = selActionCount +1;
        }
       
        segRowData.push(rowJson);
      }
      var segSecData = {
        "id":rec.featureId,
        "lblTopSeperator":{"isVisible":false},
        "flxCheckbox":{"onClick": self.onSectionCheckboxClickAddUser.bind(self,segmentPath)},
        "imgSectionCheckbox":{"src": self.getHeaderCheckboxImage(segRowData,true,true)},
        "lblIconToggleArrow":{"text":"\ue922","skin":"sknfontIconDescRightArrow14px"},
        "flxToggleArrow":{"onClick": self.toggleSegmentSectionArrowAddUser.bind(self,segmentPath)},
        "lblFeatureName": rec.featureName || rec.name ,
        "lblStatusValue":{"text":(rec.featureStatus || rec.status) === self.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE ?
                          "Active" : "Inactive"},
        "lblIconStatusTop":{"text":"\ue921",
                            "skin":(rec.featureStatus || rec.status) === self.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE ?
                            "sknFontIconActivate" : "sknfontIconInactive"},
        "lblBottomSeperator":{"isVisible":true,"text":"-"},
        "lblAvailableActions":kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.SelectedActionsColon"),
        "lblCountActions": {"text":selActionCount},
        "lblTotalActions":"of "+ rec.actions.length,
        "lblCustom":{"isVisible":false},
        "template":"flxContractEnrollFeaturesEditSection"
      };
      //changes specific to customer level features
      if(segmentPath.info.featuresType === 1){
         segSecData.lblCustom.isVisible = self.checkFeatureCustomLabel(segRowData);
      }
      if(segRowData.length > 0){
        segmentPath.info.segDataJSON[rec.featureName] = [segSecData, segRowData];
        segFeaturesData.push([segSecData, segRowData]);
      }
      return [segSecData, segRowData];
    });
    segmentPath.widgetDataMap = this.getWidgetDataMapForFeaturesEditUser();
    if(segFeaturesData.length > 0){
      segFeaturesData[segFeaturesData.length-1][0].lblBottomSeperator.isVisible = false;
    }
    segmentPath.setData(segFeaturesData);
    segmentPath.info.segData = segFeaturesData;
    var cardWidgetId = segmentPath.info.parentId;
    this.view[cardWidgetId].lblCount.text = this.getSelectedItemsCount(segmentPath.data, false);
    this.view.forceLayout();
  },
  /*
  * show/hide custom label for feature 
  * @param: actions segment list
  * @return: true/false
  */
  checkFeatureCustomLabel : function(rowsData){
    for(var i=0; i<rowsData.length; i++){
      if(rowsData[i].lblCustom.isVisible === true){
        return true;
      }
    }
    return false;
  },
  onClickFeaturesRowCheckboxAddUser: function(segmentWidPath,event){
    var selSecInd = event.rowContext.sectionIndex;
    var selRowInd = event.rowContext.rowIndex;
    var segData = segmentWidPath.data;
    segData[selSecInd][1][selRowInd].imgCheckbox.src = (segData[selSecInd][1][selRowInd].imgCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal) ?
      this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
    //hide custom label in customer level actions
    segData[selSecInd][1][selRowInd].lblCustom.isVisible = false;
    
    segData[selSecInd][0].imgSectionCheckbox.src = this.getHeaderCheckboxImage(segData[selSecInd][1],true, true);
    segData[selSecInd][0].lblCountActions.text = this.getSelectedActionsCount(segData[selSecInd][1]);
    segData[selSecInd][0].lblCustom.isVisible = this.checkFeatureCustomLabel(segData[selSecInd][1]);
    segmentWidPath.setSectionAt(segData[selSecInd],selSecInd);
    //set image for select all features image
    if(segmentWidPath.info && segmentWidPath.info.parentId){
      this.view[segmentWidPath.info.parentId].imgSectionCheckbox.src = this.getHeaderCheckboxImage(segData,false, true);
      this.view[segmentWidPath.info.parentId].lblCount.text = this.getSelectedItemsCount(segData, false);
    }
    
    //update the selected features based on features type
    if(segmentWidPath.info.featuresType === 1){ //customer level features
      this.updateCustFeaturesSelectionEditUser(segmentWidPath, event);
    } else if(segmentWidPath.info.featuresType === 2){ //account level features
      this.updateAccFeaturesSelectionEditUser(segmentWidPath, event);
    } else if(segmentWidPath.info.featuresType === 3){ //other features
      this.updateOtherFeaturesSelectionEditUser(segmentWidPath, event);
    }
    //enable/disable save buttons
    var isValid = this.validateSelectionForMultipleCardsAddUser(segmentWidPath);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnNext,false,isValid);
  },
  getSelectedActionsCount : function(actionsList){
    var actionCount = 0;
    for(var i=0;i < actionsList.length; i++){
      if(actionsList[i].imgCheckbox.src === this.AdminConsoleCommonUtils.checkboxSelected)
        actionCount = actionCount +1;
    }
    return (actionCount+"");
  },
  getFeatureObjFromArray : function(featureId, featuresArr){
    var featureObj = featuresArr.filter(function(feature) { return feature.featureId === featureId });
    return (featureObj.length > 0 ? featureObj[0] : null);
  },
  updateCustFeaturesSelectionEditUser : function(segmentWidPath, event){
    var self = this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var featureData = segmentWidPath.data[event.rowContext.sectionIndex][0];
    var actionsSegData = segmentWidPath.data[event.rowContext.sectionIndex][1]
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var currUserFeaturesMap = new Map(editUserObj.accountMapFeatures);
    var count = 0;
    currUserFeaturesMap.forEach(function(accountObj,accKey){ 
      var features = JSON.parse(accountObj.features);
      var selFeature = self.getFeatureObjFromArray(featureData.id, features);

      for(var i=0; i< selFeature.actions.length; i++){
        for(var j=0; j<actionsSegData.length; j++){
          //compare with segment action ,action from list and update data
          if(selFeature.actions[i].actionId === actionsSegData[j].id){
            if(actionsSegData[i].imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxSelected){
              selFeature.actions[i].isEnabled = "true";
              count = count+1;
              self.addAccountIdForAction(selFeature.actions[i].actionId, (accKey+""));
            }else{
              selFeature.actions[i].isEnabled = "false";
              self.removeAccountIdForAction(selFeature.actions[i].actionId, (accKey+""),1);
            }
            break;
          }
        }
      }
      selFeature.isEnabled = count === 0 ? "false" : "true";
      accountObj.features = JSON.stringify(features);
      currUserFeaturesMap.set(accKey,accountObj);
    });
    editUserObj.accountMapFeatures = currUserFeaturesMap;
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  updateAccFeaturesSelectionEditUser : function(segmentWidPath, event){
    var self = this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var featureData = segmentWidPath.data[event.rowContext.sectionIndex][0];
    var actionsSegData = segmentWidPath.data[event.rowContext.sectionIndex][1]
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);

    var count = 0;
    var accountCardComponent = this.view[segmentWidPath.info.parentId];
    var currAccountNum = accountCardComponent.info ? accountCardComponent.info.accDetails.accountId : "";
    var currAccountObj = editUserObj.accountMapFeatures.get(currAccountNum); 
    var features = JSON.parse(currAccountObj.features);
    var selFeature = self.getFeatureObjFromArray(featureData.id, features);
    for(var i=0; i< selFeature.actions.length; i++){
      for(var j=0; j<actionsSegData.length; j++){
        //compare with segment action ,action from list and update data
        if(selFeature.actions[i].actionId === actionsSegData[j].id){
          if(actionsSegData[i].imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxSelected){
            selFeature.actions[i].isEnabled = "true";
            count = count+1;
            self.addAccountIdForAction(selFeature.actions[i].actionId, (currAccountNum+""));
          }else{
            selFeature.actions[i].isEnabled = "false";
            self.removeAccountIdForAction(selFeature.actions[i].actionId, (currAccountNum+""),1);
          }
          break;
        }
      }
    }
    selFeature.isEnabled = count === 0 ? "false" : "true";
    currAccountObj.features = JSON.stringify(features);
    editUserObj.accountMapFeatures.set(currAccountNum, currAccountObj);
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  updateOtherFeaturesSelectionEditUser : function(segmentWidPath, event){
    var self = this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var featureData = segmentWidPath.data[event.rowContext.sectionIndex][0];
    var actionsSegData = segmentWidPath.data[event.rowContext.sectionIndex][1]
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var currUserOtherFeatures = editUserObj.nonAccLevelFeatures;
    var count = 0;
    var selFeature = self.getFeatureObjFromArray(featureData.id, currUserOtherFeatures);
    for(var i=0; i<selFeature.actions.length; i++){
      for(var j=0; j<actionsSegData.length; j++){
        //compare with segment action ,action from list and update data
        if(selFeature.actions[i].actionId === actionsSegData[j].id){
          if(actionsSegData[i].imgCheckbox.src === self.AdminConsoleCommonUtils.checkboxSelected){
            selFeature.actions[i].isEnabled = "true";
            count = count+1;
          }else{
            selFeature.actions[i].isEnabled = "false";
          }
          break;
        }
      }
    }
    selFeature.isEnabled = count === 0 ? "false" : "true";
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  validateSelectionForMultipleCardsAddUser : function(segmentPath){
    var parentFlxCont = "";
    if(segmentPath.info.featuresType === 1)
      parentFlxCont = this.view.flxAddedUserEditFeaturesList;
    else if(segmentPath.info.featuresType === 2)
      parentFlxCont = this.view.flxAddedUserEditAccFeaturesList;
    else if(segmentPath.info.featuresType === 3)
      parentFlxCont = this.view.flxAddedUserEditOtherFeaturesList
    
    var childWid = parentFlxCont.widgets();
    var count =0, isValid = true;
    for(var i=0;i<childWid.length;i++){
      if(childWid[i].imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal)
        count = count + 1;
    }
    if(count === childWid.length)
      return false;
    else
      return true;
  },
  onSectionCheckboxClickAddUser : function(segmentWidPath,event){
    var selSecInd = event.rowContext.sectionIndex;
    var segData = segmentWidPath.data;
    var img = (segData[selSecInd][0].imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal) ?
        this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
    segData[selSecInd][0].imgSectionCheckbox.src = img;
    for(var i =0;i<segData[selSecInd][1].length; i++){
      segData[selSecInd][1][i].imgCheckbox.src = img;
      segData[selSecInd][1][i].lblCustom.isVisible = false;
    }
    segData[selSecInd][0].lblCountActions.text = this.getSelectedActionsCount(segData[selSecInd][1]);
    segData[selSecInd][0].lblCustom.isVisible = false;
    segmentWidPath.setSectionAt(segData[selSecInd],selSecInd);
    //set image for select all features image
    if(segmentWidPath.info && segmentWidPath.info.parentId){
      this.view[segmentWidPath.info.parentId].imgSectionCheckbox.src = this.getHeaderCheckboxImage(segData,false, true);
      this.view[segmentWidPath.info.parentId].lblCount.text = this.getSelectedItemsCount(segData, false);
    }
    
    //update the selected features based on features type
    if(segmentWidPath.info.featuresType === 1){ //customer level features
      this.updateCustFeaturesSelectionEditUser(segmentWidPath, event);
    } else if(segmentWidPath.info.featuresType === 2){ //account level features
      this.updateAccFeaturesSelectionEditUser(segmentWidPath, event);
    } else if(segmentWidPath.info.featuresType === 3){ //other features
      this.updateOtherFeaturesSelectionEditUser(segmentWidPath, event);
    }
    
    //enable/disable save buttons
   // var isValid = this.validateCheckboxSelections(segmentWidPath,true);
    var isValid = this.validateSelectionForMultipleCardsAddUser(segmentWidPath);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnNext,false,isValid);
  },
  toggleSegmentSectionArrowAddUser : function(segmentWidgetPath,event){
    var segData = segmentWidgetPath.data;
    var selectedSecInd = event.rowContext.sectionIndex;
    //update remaining sections to collapse
    for(var i=0;i< segData.length;i++){
      segData[i][0].lblTopSeperator.isVisible = false;
      segData[i][0].lblBottomSeperator.isVisible = true;
      if(selectedSecInd !== i){
        segData[i][0].lblIconToggleArrow.text = "\ue922";
        segData[i][0].lblIconToggleArrow.skin = "sknfontIconDescRightArrow14px";
        segData[i][1] = this.showHideSegRowFlexAddUser(segData[i][1],false);
      }
      //hide bottom seperator for last row
      if(i === segData.length-1){
        segData[i][0].lblBottomSeperator.isVisible = false;
      }
    }
    //update selected section
    if(segData[selectedSecInd][1][0].isRowVisible === false){
      segData[selectedSecInd][0].lblIconToggleArrow.text = "\ue915";
      segData[selectedSecInd][0].lblIconToggleArrow.skin = "sknfontIconDescDownArrow12px";
      segData[selectedSecInd][1] = this.showHideSegRowFlexAddUser(segData[selectedSecInd][1],true);
      if(selectedSecInd < (segData.length-1)){
        segData[selectedSecInd+1][0].lblTopSeperator.isVisible = true;
      }
      segData[selectedSecInd][0].lblBottomSeperator.isVisible = (this.view.flxAddedUserEditLimitsContainer.isVisible === true) ? false : true;
    } else{
      segData[selectedSecInd][0].lblIconToggleArrow.text = "\ue922";
      segData[selectedSecInd][0].lblIconToggleArrow.skin = "sknfontIconDescRightArrow14px";
      segData[selectedSecInd][1] = this.showHideSegRowFlexAddUser(segData[selectedSecInd][1],false);
      if(selectedSecInd < (segData.length-1)){
        segData[selectedSecInd+1][0].lblTopSeperator.isVisible = false;
      }
      if(selectedSecInd ===(segData.length-1) ){
        segData[selectedSecInd][0].lblBottomSeperator.isVisible = false;
      }
    }
    segmentWidgetPath.setData(segData);
  },
  getWidgetDataMapForFeaturesEditUser : function(){
    var widgetMap = {
      "id":"id",
      "isRowVisible":"isRowVisible",
      "flxFeatureNameCont":"flxFeatureNameCont",
      "imgCheckbox":"imgCheckbox",
      "flxCheckbox":"flxCheckbox",
      "lblStatus":"lblStatus",
      "flxStatus":"flxStatus",
      "lblIconStatus":"lblIconStatus",
      "lblCustom":"lblCustom",
      "flxContractEnrollFeaturesEditRow":"flxContractEnrollFeaturesEditRow",
      "lblTopSeperator":"lblTopSeperator",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxToggleArrow":"flxToggleArrow",
      "lblIconToggleArrow":"lblIconToggleArrow",
      "lblFeatureName":"lblFeatureName",
      "lblStatusValue":"lblStatusValue",
      "lblIconStatusTop":"lblIconStatusTop",
      "lblBottomSeperator":"lblBottomSeperator",
      "lblAvailableActions":"lblAvailableActions",
      "lblCountActions":"lblCountActions",
      "lblTotalActions":"lblTotalActions",
      "flxContractEnrollFeaturesEditSection":"flxContractEnrollFeaturesEditSection"
    };
    return widgetMap;
  },
  showEditAccountsScreen(){
    this.view.flxAddedUserEditAccountsContainer.setVisibility(true);
    this.view.flxAddedUserEditFeaturesContainer.setVisibility(false);
    this.view.flxAddedUserEditOtherFeaturesCont.setVisibility(false);
    this.view.flxAddedUserEditLimitsContainer.setVisibility(false);
    var widgetBtnArr = [this.view.AddedUserEditVerticalTabs.btnOption1,this.view.AddedUserEditVerticalTabs.btnOption2,this.view.AddedUserEditVerticalTabs.btnOption3,this.view.AddedUserEditVerticalTabs.btnOption4];
    var widgetArrowArr = [this.view.AddedUserEditVerticalTabs.flxImgArrow1,this.view.AddedUserEditVerticalTabs.flxImgArrow2,this.view.AddedUserEditVerticalTabs.flxImgArrow3, this.view.AddedUserEditVerticalTabs.flxImgArrow4];
    this.tabUtilVerticleButtonFunction(widgetBtnArr,this.view.AddedUserEditVerticalTabs.btnOption1);
    this.tabUtilVerticleArrowVisibilityFunction(widgetArrowArr,this.view.AddedUserEditVerticalTabs.flxImgArrow1);
    this.view.searchEditAccounts.tbxSearchBox.text = "";
    this.view.searchEditAccounts.flxSearchCancel.setVisibility(false);
    this.view.enrollEditAccountsCard.flxArrow.setVisibility(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditAccounts.btnNext,false,true);
    this.enableOrDisableVerticalTabEditUser(true);
    this.view.flxAddedUserEditAccountsList.setContentOffset({x:0,y:0});
    this.view.enrollEditAccountsCard.lblHeading.text =kony.i18n.getLocalizedString("i18n.frmCompanies.Selected_Accounts") + ":";
    //this.setAccountsSegmentData();
  },
  showEditFeaturesScreen : function(){
    this.view.flxAddedUserEditAccountsContainer.setVisibility(false);
    this.view.flxAddedUserEditFeaturesContainer.setVisibility(true);
    this.view.flxAddedUserEditOtherFeaturesCont.setVisibility(false);
    this.view.flxAddedUserEditLimitsContainer.setVisibility(false);
    var widgetBtnArr = [this.view.AddedUserEditVerticalTabs.btnOption1,this.view.AddedUserEditVerticalTabs.btnOption2,this.view.AddedUserEditVerticalTabs.btnOption3,this.view.AddedUserEditVerticalTabs.btnOption4];
    var widgetArrowArr = [this.view.AddedUserEditVerticalTabs.flxImgArrow1,this.view.AddedUserEditVerticalTabs.flxImgArrow2,this.view.AddedUserEditVerticalTabs.flxImgArrow3, this.view.AddedUserEditVerticalTabs.flxImgArrow4];
    this.tabUtilVerticleButtonFunction(widgetBtnArr,this.view.AddedUserEditVerticalTabs.btnOption2);
    this.tabUtilVerticleArrowVisibilityFunction(widgetArrowArr,this.view.AddedUserEditVerticalTabs.flxImgArrow2); 
    this.view.toggleButtonsFeatures.info = {"selectedTab":1};
    this.view.searchEditFeatures.tbxSearchBox.text = "";
    this.view.searchEditFeatures.flxSearchCancel.setVisibility(false);
    this.view.flxAccountTypesFilter.setVisibility(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnNext,false,true);
    this.enableOrDisableVerticalTabEditUser(true);
    this.toggleFeaturesCustomerLevel();
  },
  showEditOtherFeaturesScreen : function(){
    this.view.flxAddedUserEditAccountsContainer.setVisibility(false);
    this.view.flxAddedUserEditFeaturesContainer.setVisibility(false);
    this.view.flxAddedUserEditOtherFeaturesCont.setVisibility(true);
    this.view.flxAddedUserEditLimitsContainer.setVisibility(false);
    var widgetBtnArr = [this.view.AddedUserEditVerticalTabs.btnOption1,this.view.AddedUserEditVerticalTabs.btnOption2,this.view.AddedUserEditVerticalTabs.btnOption3,this.view.AddedUserEditVerticalTabs.btnOption4];
    var widgetArrowArr = [this.view.AddedUserEditVerticalTabs.flxImgArrow1,this.view.AddedUserEditVerticalTabs.flxImgArrow2,this.view.AddedUserEditVerticalTabs.flxImgArrow3, this.view.AddedUserEditVerticalTabs.flxImgArrow4];
    this.tabUtilVerticleButtonFunction(widgetBtnArr,this.view.AddedUserEditVerticalTabs.btnOption3);
    this.tabUtilVerticleArrowVisibilityFunction(widgetArrowArr,this.view.AddedUserEditVerticalTabs.flxImgArrow3); 
    this.view.searchEditOtherFeatures.tbxSearchBox.text = "";
    this.view.searchEditOtherFeatures.flxSearchCancel.setVisibility(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditOF.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditOF.btnNext,false,true);
    this.createOtherFeaturesCard();
  },
  showEditLimitsScreen : function(){
    this.view.flxAddedUserEditAccountsContainer.setVisibility(false);
    this.view.flxAddedUserEditFeaturesContainer.setVisibility(false);
    this.view.flxAddedUserEditOtherFeaturesCont.setVisibility(false);
    this.view.flxAddedUserEditLimitsContainer.setVisibility(true);
    var widgetBtnArr = [this.view.AddedUserEditVerticalTabs.btnOption1,this.view.AddedUserEditVerticalTabs.btnOption2,this.view.AddedUserEditVerticalTabs.btnOption3, this.view.AddedUserEditVerticalTabs.btnOption4];
    var widgetArrowArr = [this.view.AddedUserEditVerticalTabs.flxImgArrow1,this.view.AddedUserEditVerticalTabs.flxImgArrow2,this.view.AddedUserEditVerticalTabs.flxImgArrow3, this.view.AddedUserEditVerticalTabs.flxImgArrow4];
    this.tabUtilVerticleButtonFunction(widgetBtnArr,this.view.AddedUserEditVerticalTabs.btnOption4);
    this.tabUtilVerticleArrowVisibilityFunction(widgetArrowArr,this.view.AddedUserEditVerticalTabs.flxImgArrow4);
    this.view.toggleButtonsLimits.info = {"selectedTab":1};
    this.view.searchEditLimits.tbxSearchBox.text = "";
    this.view.searchEditLimits.flxSearchCancel.setVisibility(false);
    this.view.flxAccountTypesFilter.setVisibility(false);
    this.toggleLimitsCustomerLevel();
  },
  createOtherFeaturesCard : function(){
    this.view.addedUserEditOtherFeaturesCard.toggleCollapseArrow(true);
    this.view.addedUserEditOtherFeaturesCard.flxArrow.setVisibility(false);
    this.view.addedUserEditOtherFeaturesCard.flxSelectAllOption.setVisibility(true);
    this.view.addedUserEditOtherFeaturesCard.flxCardBottomContainer.setVisibility(true);
    this.view.addedUserEditOtherFeaturesCard.segAccountFeatures.info = {"parentId":"addedUserEditOtherFeaturesCard","segData":[], "featuresType":3, "segDataJSON":{}};
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var enrollCustOtherFeatures = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    this.view.addedUserEditOtherFeaturesCard.lblTotalCount.text = "of " + this.getTwoDigitNumber(enrollCustOtherFeatures.nonAccLevelFeatures.length);
    this.setFeaturesCardSegmentData(this.view.addedUserEditOtherFeaturesCard.segAccountFeatures, enrollCustOtherFeatures.nonAccLevelFeatures);
    this.view.addedUserEditOtherFeaturesCard.flxCheckbox.onClick = this.onSelectAllFeaturesClickAddUser.bind(this,this.view.addedUserEditOtherFeaturesCard);
    this.view.addedUserEditOtherFeaturesCard.imgSectionCheckbox.src = this.getHeaderCheckboxImage(this.view.addedUserEditOtherFeaturesCard.segAccountFeatures.data,false, true);
  },
  storeActionForAccountSelection : function(){
    var self =this;
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserDetails = this.presenter.getAccountsFeaturesForAddUser(enrollUserData.orignalData.coreCustomerId);
    var accLevelfeatures = editUserDetails.accountMapFeatures;
    accLevelfeatures.forEach(function(account,accKey){
      if(account.accountDetails.isEnabled === "true" || account.accountDetails.isAssociated === "true"){
        var accFeatures = JSON.parse(account.features);
        for(var j=0; j< accFeatures.length; j++){
          for(var i=0; i<accFeatures[j].actions.length; i++){
            //push enabled actions for that account
            if(accFeatures[j].actions[i].isEnabled === "true"){
              self.addAccountIdForAction(accFeatures[j].actions[i].actionId,(accKey+""));
            } else if(accFeatures[j].actions[i].isEnabled === "false"){
              self.removeAccountIdForAction(accFeatures[j].actions[i].actionId,(accKey+""),1);
            }
          }
        }
      }  //remove acc num from all accounts when account is removed 
      else if(account.accountDetails.isEnabled === "false" || account.accountDetails.isAssociated === "false"){
        self.removeAccountIdForAction(null,(accKey+""),2);
      } 
    });
  },
  onSelectAllFeaturesClickAddUser : function(cardWidgetPath){
    var segData = cardWidgetPath.segAccountFeatures.data;
    var img = (cardWidgetPath.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal) ?
        this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
    cardWidgetPath.imgSectionCheckbox.src = img;
    cardWidgetPath.lblCount.text = (img === this.AdminConsoleCommonUtils.checkboxnormal ? "0": this.getTwoDigitNumber(segData.length));
    for(var i=0;i<segData.length; i++){
      segData[i][0].imgSectionCheckbox.src = img;
      for(var j=0;j<segData[i][1].length; j++){
        segData[i][1][j].imgCheckbox.src = img;
      }
    }
    cardWidgetPath.segAccountFeatures.setData(segData);
    //update all features based on features type
    if(cardWidgetPath.segAccountFeatures.info.featuresType === 1){ //customer level features
      this.updateCustAllFeaturesSelectionEdit(cardWidgetPath.segAccountFeatures);
    } else if(cardWidgetPath.segAccountFeatures.info.featuresType === 2){ //account level features
      this.updateAccAllFeaturesSelectionEdit(cardWidgetPath.segAccountFeatures);
    } else if(cardWidgetPath.segAccountFeatures.info.featuresType === 3){ //other features
      this.updateOtherAllFeaturesSelectionEdit(cardWidgetPath.segAccountFeatures);
    }
    //enable/disable save buttons
    var isValid = this.validateSelectionForMultipleCardsAddUser(cardWidgetPath.segAccountFeatures);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsEditFeatures.btnNext,false,isValid);
  },
  updateCustAllFeaturesSelectionEdit : function(segmentWidPath){
    var self =this;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var featureCardComponent = this.view.addedUserEditFeaturesCard;
    var currUserFeaturesMap = new Map(editUserObj.accountMapFeatures);
    currUserFeaturesMap.forEach(function(accountObj,accKey){ 
      var features = JSON.parse(accountObj.features);
      for(var i=0; i<features.length; i++){
        features[i].isEnabled = featureCardComponent.imgSectionCheckbox.src === self.AdminConsoleCommonUtils.checkboxnormal ? false:true;
        for(var j=0;j<features[i].actions.length; j++){
          if(featureCardComponent.imgSectionCheckbox.src === self.AdminConsoleCommonUtils.checkboxnormal){
            features[i].actions[j].isEnabled = "false";
            self.removeAccountIdForAction(features[i].actions[j].actionId, (accKey+""), 1);
          } else{
            features[i].actions[j].isEnabled = "true";
            self.addAccountIdForAction(features[i].actions[j].actionId, (accKey+""));
          }

        }
      }
      accountObj.features = JSON.stringify(features);
      currUserFeaturesMap.set(accKey,accountObj);
    });
    editUserObj.accountMapFeatures = currUserFeaturesMap;
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  updateAccAllFeaturesSelectionEdit : function(segmentWidPath){
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var accountCardComponent = this.view[segmentWidPath.info.parentId];
    var currAccountNum = accountCardComponent.info ? accountCardComponent.info.accDetails.accountId : "";
    var currAccountObj = editUserObj.accountMapFeatures.get(currAccountNum); 
    var features = JSON.parse(currAccountObj.features);
    for(var i=0; i<features.length; i++){
      features[i].isEnabled = accountCardComponent.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal ? false:true;
      for(var j=0;j<features[i].actions.length; j++){
        if( accountCardComponent.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal){
          features[i].actions[j].isEnabled = "false";
          this.removeAccountIdForAction(features[i].actions[j].actionId, currAccountNum, 1);
        } else{
          features[i].actions[j].isEnabled = "true";
          this.addAccountIdForAction(features[i].actions[j].actionId, currAccountNum);
        }
        
      }
    }
    currAccountObj.features = JSON.stringify(features);
    editUserObj.accountMapFeatures.set(currAccountNum, currAccountObj);
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  updateOtherAllFeaturesSelectionEdit : function(segmentWidPath){
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId);
    var featureCardComponent = this.view.addedUserEditOtherFeaturesCard;
    var features = editUserObj.nonAccLevelFeatures;
    for(var i=0; i<features.length; i++){
      features[i].isEnabled = featureCardComponent.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal ? false:true;
      for(var j=0;j<features[i].actions.length; j++){
        features[i].actions[j].isEnabled = featureCardComponent.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal ? false:true;
      }
    }
    editUserObj.nonAccLevelFeatures = features;
    this.presenter.setAccountsFeaturesForAddUser(selCustData.orignalData.coreCustomerId,editUserObj);
  },
  addAccountIdForAction : function(actionId,accountId){
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var associatedAccounts = this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][actionId];
    if(associatedAccounts && associatedAccounts.indexOf(accountId) < 0){
      associatedAccounts.push(accountId);
    }
  },
  removeAccountIdForAction : function(actionId,accountId,option){
    var enrollUserData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    if(option ===1){ //remove the account number for given action id
      var associatedAccounts = this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][actionId];
      var filterAcc = associatedAccounts.filter(function(accId) { return accId !== accountId });
      this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][actionId] = filterAcc;
    } else if(option ===2){ //remove the account number for all actions available
      var actionsArr = Object.keys(this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId]);
      var accountsArr = Object.values(this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId]);
      for(var i=0; i<accountsArr.length; i++){
        var filterAcc = accountsArr[i].filter(function(accId) { return accId !== accountId });
        this.actionsAccountJSON[enrollUserData.orignalData.coreCustomerId][actionsArr[i]] = filterAcc;
      }
    }
  },
  collectAllInfoAndSave : function(){
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.view.flxSearchUser.setVisibility(true);
    var toastText = this.aboutToAddUser.data[0].orignalData.coreCustomerName + " Permissions and Limits have been updated successfully.";
    this.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),toastText);
  },
  hideEditScreen : function(){
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.view.flxSearchUser.setVisibility(true);
    this.revertEditUserChangesOnCancel();
  },
  showAccountTypesFilter : function(opt){
    var top = 150;
    if(opt === 1){
      top = this.view.flxAddedUserEditFeaturesSearch.frame.y + this.view.flxAddedUserEditFeatureFilter.frame.y +30;
    } else{
      top = this.view.flxAddedUserEditLimitSearch.frame.y + this.view.flxAddedUserEditLimitsFilter.frame.y +30;
    }
    this.view.customListBoxAccounts.flxSegmentList.setVisibility(false);
    this.view.flxAccountTypesFilter.top = top +"dp";
    this.view.flxAccountTypesFilter.setVisibility(true);
  },
  /*
  * show bulk update features/limits popup
  * @param: option for feature/limits - 1/2
  */
  showBulkUpdatePopupEditUser: function(option){
    this.view.flxBulkUpdateFeaturesPopupForAddUser.setVisibility(true);
    this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info ={"option" : option};
    this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.setEnabled(false);
    this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.flxSelectedText.skin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.flxSelectedText.hoverSkin = "sknFlxbgF3F3F3bdrd7d9e0";
    this.view.bulkUpdateFeaturesPopupForAddUser.flxSegmentContainer.top = "106dp";
    this.view.bulkUpdateFeaturesPopupForAddUser.flxSearchContainer.setVisibility(true);
    this.setAccountsListInBulkUpdatePopup(option);
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.text = "";
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.flxSearchCancel.setVisibility(false);
    this.showBulkUpdatePopupScreenAddUser1();
    this.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.info = {"added":[]};
    this.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.removeAll();
    this.setRadioGroupDataAddUser(option);
    this.setFeatureLimitsBulkUpdateUI(option);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen1.btnSave,true,true);
  },
  setAccountsListInBulkUpdatePopup : function(option){
    var self =this;
    var custId ="";
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    custId = selCustData.orignalData.coreCustomerId;
    var custEditUserObj  = this.presenter.getAccountsFeaturesForAddUser(custId);
    var allAccountsMap = custEditUserObj.accounts;
    var accountsJson = this.getAccountsBasedOnAccType(allAccountsMap);
    var accList = Object.values(accountsJson);
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.info = {"allData":{}};
    var segData = accList.map(function(accArr){
      var rowsData = [];
      for(var i=0;i< accArr.length; i++){
        rowsData.push({
          "isRowVisible":false,
          "id": accArr[i].accountId,
          "flxContractEnrollAccountsEditRow":{"isVisible":false,
                                              "skin":"sknFlxBgFFFFFFbrD7D9E0r1pxLeftRight"},
          "flxCheckbox":{"onClick": self.onCheckAccountsCheckboxAddUser.bind(self,self.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList,false)},
          "imgCheckbox":{"src":self.AdminConsoleCommonUtils.checkboxSelected},
          "lblAccountNumber": {"text":accArr[i].accountId},
          "lblAccountType": {"text":accArr[i].accountType},
          "lblAccountName": {"text":accArr[i].accountName},
          "lblAccountHolder": {"text":accArr[i].ownerType},
          "lblSeperator":"-",
          "template":"flxContractEnrollAccountsEditRow"
        });
        self.sortBy = self.getObjectSorter("lblAccountNumber.text");
        self.sortBy.inAscendingOrder = true;
        rowsData = rowsData.sort(self.sortBy.sortData);
      }
      
      var sectionData = {
        "lblFeatureName": accArr[0].accountType,
        "flxToggleArrow":{"onClick": self.toggleSelectAccountsArrow},
        "lblIconToggleArrow": {"text":"\ue922","skin":"sknIcon00000015px"},
        "flxLeft":"flxLeft",
        "lblSectionLine":"-",
        "lblAvailableActions":"Selected accounts:",
        "lblCountActions": (rowsData.length+""),
        "lblTotalActions": "of "+rowsData.length,
        "flxAccountSectionCont":{"skin":"sknFlxBgF5F6F8BrD7D9E0Br1pxRd3px"},
        "flxHeaderContainer":{"isVisible":false},
        "flxAccountNumCont":{"onClick":self.sortAndSetData.bind(self,"lblAccountNumber.text",self.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList, 2)},
        "flxAccountType":{"onClick":""},
        "flxAccountName":{"onClick":self.sortAndSetData.bind(self,"lblAccountName.text",self.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList, 2)},
        "flxAccountHolder":{"onClick":self.showAccountsFilterInBulkUpdate},
        "lblSeperator":"-",
        "imgSectionCheckbox":{"src":self.AdminConsoleCommonUtils.checkboxSelected},
        "flxCheckbox":{"onClick":self.onCheckAccountsCheckboxAddUser.bind(self,self.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList,true)},
        "lblAccountNumber": kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNUMBER"),
        "lblIconSortAccName":{
          "text": "\ue92a","left" : "10px",
          "skin": "sknIcon12pxBlack","hoverSkin" :"sknIcon12pxBlackHover"
        },
        "lblAccountType": kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE"),
        "lblAccountName":kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME"),
        "lblIconAccNameSort":{
          "text": "\ue92b","left" : "5px",
          "skin": "sknIcon15px","hoverSkin" :"sknlblCursorFont"
        },
        "lblAccountHolder":"OWNERSHIP TYPE",
        "lblIconSortAccHolder":"\ue916",
        "template":"flxEnrollSelectedAccountsSec",
      };
      
      self.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.info.allData[accArr[0].accountType] = rowsData;
      return [sectionData, rowsData];
    });
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.widgetDataMap = this.widgetMapforBulkUpdateAccounts();
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.setData(segData);
    this.view.forceLayout();
  },
  widgetMapforBulkUpdateAccounts : function(){
    var widgetMap = {
      "id":"id",
      "lblFeatureName":"lblFeatureName",
      "flxToggleArrow":"flxToggleArrow",
      "lblIconToggleArrow":"lblIconToggleArrow",
      "flxLeft":"flxLeft",
      "lblSectionLine":"lblSectionLine",
      "lblAvailableActions":"lblAvailableActions",
      "lblCountActions":"lblCountActions",
      "lblTotalActions":"lblTotalActions",
      "flxAccountSectionCont":"flxAccountSectionCont",
      "flxHeaderContainer":"flxHeaderContainer",
      "flxAccountNumCont":"flxAccountNumCont",
      "flxAccountType":"flxAccountType",
      "flxAccountName":"flxAccountName",
      "flxAccountHolder":"flxAccountHolder",
      "lblSeperator":"lblSeperator",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxCheckbox":"flxCheckbox",
      "lblAccountNumber":"lblAccountNumber",
      "lblIconSortAccName":"lblIconSortAccName",
      "lblAccountType":"lblAccountType",
      "lblAccountName":"lblAccountName",
      "lblIconAccNameSort":"lblIconAccNameSort",
      "lblAccountHolder":"lblAccountHolder",
      "lblIconSortAccHolder":"lblIconSortAccHolder",
      "flxEnrollSelectedAccountsSec":"flxEnrollSelectedAccountsSec",
      "imgCheckbox":"imgCheckbox",
      "isRowVisible":"isRowVisible",
      "flxContractEnrollAccountsEditRow":"flxContractEnrollAccountsEditRow"
      
    };
    return widgetMap;
  },
  showBulkUpdatePopupScreenAddUser1 : function(){
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateScreen1.setVisibility(true);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateScreen2.setVisibility(false);
  },
  setRadioGroupDataAddUser : function(opt){
    var radioBtnData=[];
    if(opt===1){
      this.view.bulkUpdateFeaturesPopupForAddUser.lblRadioGroupTitle.text="Permission Type";
      radioBtnData = [{src:this.AdminConsoleCommonUtils.radioSelected, value:"Enable", selectedImg:this.AdminConsoleCommonUtils.radioSelected, unselectedImg: this.AdminConsoleCommonUtils.radioNotSelected},
                      {src:this.AdminConsoleCommonUtils.radioNotSelected, value:"Disable", selectedImg:this.AdminConsoleCommonUtils.radioSelected, unselectedImg: this.AdminConsoleCommonUtils.radioNotSelected}];
      this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.flxRadioButton1.width="100px";
      this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.flxRadioButton2.width="100px";
    }else{
      this.view.bulkUpdateFeaturesPopupForAddUser.lblRadioGroupTitle.text="Transaction Type";
      radioBtnData = [{src: this.AdminConsoleCommonUtils.radioSelected, value:"Per Transaction", selectedImg:this.AdminConsoleCommonUtils.radioSelected, unselectedImg: this.AdminConsoleCommonUtils.radioNotSelected},
                      {src:this.AdminConsoleCommonUtils.radioNotSelected, value:"Daily Transaction", selectedImg: this.AdminConsoleCommonUtils.radioSelected, unselectedImg: this.AdminConsoleCommonUtils.radioNotSelected},
                      {src:this.AdminConsoleCommonUtils.radioNotSelected, value:"Weekly Transaction", selectedImg: this.AdminConsoleCommonUtils.radioSelected, unselectedImg: this.AdminConsoleCommonUtils.radioNotSelected}];
      this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.flxRadioButton1.width="150px";
      this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.flxRadioButton2.width="150px";
      this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.flxRadioButton3.width="150px";
    }
    this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.setData(radioBtnData);
    this.view.forceLayout();
  },
  setFeatureLimitsBulkUpdateUI : function(option){
    if(option === 1){
      this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.lblSelectedValue.text = this.view.customersDropdownFeatures.lblSelectedValue.text;
      this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.btnPrimary.isVisible = this.view.customersDropdownFeatures.btnPrimary.isVisible;
      this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UpdatePermissionsInBulk");
      this.view.bulkUpdateFeaturesPopupForAddUser.lblTitle.text = "Add Permissions";
    }else{
      this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.lblSelectedValue.text = this.view.addedUserLimitsDropdown.lblSelectedValue.text;
      this.view.bulkUpdateFeaturesPopupForAddUser.customersDropdownBulkUpdate.btnPrimary.isVisible = this.view.addedUserLimitsDropdown.btnPrimary.isVisible;
      this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.text =  kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UpdateLimitsInBulk");
      this.view.bulkUpdateFeaturesPopupForAddUser.lblTitle.text = "Add Limits";
    }
  },
  getSelectedAccountTypesCount : function(){
    var accountTypes = [];
    var segData = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.data;
    for(var i=0;i<segData.length;i++){
      var segImg = segData[i][0].imgSectionCheckbox ? segData[i][0].imgSectionCheckbox.src : segData[i][0].imgCheckbox.src
      if(segImg !== this.AdminConsoleCommonUtils.checkboxnormal){
        var count =0;
        for(var j=0;j<segData[i][1].length; j++){
          if(segData[i][1][j].imgCheckbox.src === this.AdminConsoleCommonUtils.checkboxSelected)
            count = count + 1;
        }
        var accJson = {"accType":segData[i][0].lblFeatureName,
                       "count":count};
        accountTypes.push(accJson);
      }
    }
    this.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.removeAll();
    for(var k=0;k<accountTypes.length;k++){
      this.addAccountsTag(accountTypes[k]);
    }
  },
  showBulkUpdatePopupScreenAddUser2 : function(){
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateScreen1.setVisibility(false);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateScreen2.setVisibility(true);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.setVisibility(true);
    this.view.bulkUpdateFeaturesPopupForAddUser.btnAddNewRow.setVisibility(true);
    var flxChildren = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.removeAll();
    for(var i=0;i<flxChildren.length;i++){
      this.view.bulkUpdateFeaturesPopupForAddUser.remove(this.view.bulkUpdateFeaturesPopupForAddUser[flxChildren[i].id]);
    }
    this.view.forceLayout();
    var height = this.view.bulkUpdateFeaturesPopupForAddUser.flxContractDetailsPopupContainer.frame.height - (70 + this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateListContainer.frame.y + 80);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateListContainer.height = height + "dp";
    if(this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info.option === 1){
      this.view.bulkUpdateFeaturesPopupForAddUser.btnAddNewRow.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.plusAddNewPermissions");
      this.bulkUpdateListboxData = this.getListForListBoxAddUser(1);
      this.addNewFeatureRowBulkUpdateAddUser("enroll");
      this.getFeaturesForBulkUpdateAddUser(1, "enroll");
    } else{
      this.view.bulkUpdateFeaturesPopupForAddUser.btnAddNewRow.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.plusAddNewLimits");
      this.bulkUpdateListboxData = this.getListForListBoxAddUser(2);
      this.addNewLimitRowBulkUpdateAddUser("enroll");
      this.getFeaturesForBulkUpdateAddUser(2, "enroll");
    }
  },
  addNewFeatureRowBulkUpdateAddUser : function(category){
    var flxChildren = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    //caluclate the value for id to suffix
    var num = flxChildren.length > 0 ? flxChildren[flxChildren.length-1].id.split("Z")[1] : "0";
    num = parseInt(num,10) +1;
    var id = num > 9 ? ""+num: "0"+num;
    var rowWidth = this.view.bulkUpdateFeaturesPopupForAddUser.flxContractDetailsPopupContainer.frame.width - 40;
    var featureRowToAdd = new com.adminConsole.contracts.bulkUpdateAddNewFeatureRow({
        "id": "bulkFeatureRowZ" +id,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"20dp",
        "width":rowWidth + "dp",
        "top": "20dp"
      }, {}, {});
    featureRowToAdd.flxErrorField21.setVisibility(false);
    featureRowToAdd.flxErrorField22.setVisibility(false);
    this.setNewFeatureLimitRowDataAddUser(featureRowToAdd,1, category);
  },
  setNewFeatureLimitRowDataAddUser : function(newRowWidget, option, category){
    var self =this;
    var allRows = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    if(allRows.length === 0){
      newRowWidget.flxDelete.isVisible = false;
    } else{
      allRows[0].flxDelete.isVisible = true;
      newRowWidget.flxDelete.isVisible = true;
    }
    var listboxData = this.getUnselectedFeaturesListAddUser(option, category);
    newRowWidget.lstBoxFieldValue11.masterData = listboxData;
    newRowWidget.lstBoxFieldValue11.selectedKey = listboxData[0][0];
    newRowWidget.lblFieldValue12.text = "Select an action";
    //assigning actions
    newRowWidget.lstBoxFieldValue11.onSelection = function(){
      newRowWidget.lstBoxFieldValue11.skin="sknLbxborderd7d9e03pxradius";
      newRowWidget.flxErrorField11.setVisibility(false);
      self.setActionsListboxDataAddUser(newRowWidget,option);
      self.updateExistingRowsListboxDataAddUser(newRowWidget,option,category);
    }
    newRowWidget.flxDelete.onClick = this.deleteAddNewFeatureRow.bind(this,newRowWidget, option, category);
    newRowWidget.tbxValue21.onKeyUp = function(){
      newRowWidget.flxValue21.skin = "sknFlxbgFFFFFFbdrD7D9E0rd3px";
      newRowWidget.flxErrorField21.setVisibility(false);
    }
    newRowWidget.tbxValue22.onKeyUp = function(){
      newRowWidget.flxValue22.skin = "sknFlxbgFFFFFFbdrD7D9E0rd3px";
      newRowWidget.flxErrorField22.setVisibility(false);
    }
    this.setFeatureLimitRowUIAddUser(option,newRowWidget);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.addAt(newRowWidget, allRows.length);
    this.updateExistingRowsListboxDataAddUser(newRowWidget, option,category);
    this.view.forceLayout();
  },
  setFeatureLimitRowUIAddUser : function(opt,widgetPath){
    if(opt === 1){  //for feature
      widgetPath.flxRow2.setVisibility(false);
      widgetPath.flxFieldColumn13.setVisibility(false);
      widgetPath.flxFieldColumn21.setVisibility(false);
    } else{ //for limits
      widgetPath.flxRow2.setVisibility(true);
      widgetPath.flxFieldColumn13.setVisibility(false);
      widgetPath.flxFieldColumn21.setVisibility(true);
      widgetPath.lblFieldName21.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.PreApproved");
      widgetPath.lblFieldName22.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.AutoDeny");
    }
  },
  setActionsListboxDataAddUser : function(widgetPath, option){
    var self=this;
    var allBulkFeatures=this.bulkUpdateList;
    var selectedFeatureId=widgetPath.lstBoxFieldValue11.selectedKey;
    var featureActions=[{"id":"select","name":"Select"}];
    var actionListData=[["select","Select"]];
    if(selectedFeatureId!=="select"){
      featureActions=allBulkFeatures[selectedFeatureId].actions;
      widgetPath.lblFieldValue12.text=featureActions.length+ " Selected";
      widgetPath.flxFieldValueContainer12.setEnabled(true);
      widgetPath.flxFieldValueContainer12.onClick = function(){
      widgetPath.flxDropdownField12.setVisibility(!widgetPath.flxDropdownField12.isVisible);
      }
    }
    var widgetMap = {
      "serviceType": "serviceType",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription",
      "dependentActions": "dependentActions"
    };
    var i=0;
    var actionsSegData=featureActions.map(function(action){
      self.bulkUpdateList[selectedFeatureId].actions[i].isChecked=true;
      i=i+1;
      return{
        "actionId": action.actionId,
        "flxSearchDropDown": "flxSearchDropDown",
        "imgCheckBox":"checkboxselected.png",
        "lblDescription": action.actionName,
        "dependentActions": action.dependentActions
      }
    });
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.widgetDataMap=widgetMap;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(actionsSegData);
    //to select all the actions by default
    var selectInd=[];
    for(let x=0;x<actionsSegData.length;x++){
      selectInd.push(x);
    }
    var selectionProp = {
      imageIdentifier: "imgCheckBox",
      selectedStateImage: "checkboxselected.png",
      unselectedStateImage: "checkboxnormal.png"
    };
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehaviorConfig = selectionProp;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices = [[0,selectInd]];
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      var segData=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.data;
      var selInd=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices[0][1];
      widgetPath.lblFieldValue12.text=selInd.length==1?segData[selInd[0]].lblDescription:selInd.length+" Selected";
      var feature=self.bulkUpdateList[widgetPath.lstBoxFieldValue11.selectedKey];
      for(let i=0;i<feature.actions.length;i++){
        if(feature.actions[i].actionId===segData[selInd[0]].actionId)
          self.bulkUpdateList[widgetPath.lstBoxFieldValue11.selectedKey].actions[i].isChecked=(segData[selInd[0]].imgCheckBox==="checkboxnormal.png")?false:true;
      }      
      self.view.forceLayout();
    };
    widgetPath.flxDropdownField12.onHover = this.onHoverEventCallback;
    this.view.forceLayout();
  },
  addNewLimitRowBulkUpdateAddUser : function(category){
    var flxChildren = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    //caluclate the value for id to suffix
    var num = flxChildren.length > 0 ? flxChildren[flxChildren.length-1].id.split("Z")[1] : "0";
    num = parseInt(num,10) +1;
    var id = num > 9 ? ""+num: "0"+num;
    var rowWidth = this.view.bulkUpdateFeaturesPopupForAddUser.flxContractDetailsPopupContainer.frame.width - 40;
    var limitRowToAdd = new com.adminConsole.contracts.bulkUpdateAddNewFeatureRow({
        "id": "bulkLimitRowZ" +id,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"20dp",
        "width":rowWidth + "dp",
        "top": "20dp"
      }, {}, {});
    if(category === "enroll"){
      limitRowToAdd.flxRow2.isVisible = true;
      limitRowToAdd.flxFieldColumn21.isVisible = true;
      limitRowToAdd.flxFieldColumn22.isVisible = true;
      limitRowToAdd.tbxValue21.text ="";
      limitRowToAdd.tbxValue22.text ="";
    }else{
      limitRowToAdd.flxRow2.isVisible = false;
      limitRowToAdd.flxFieldColumn21.isVisible = true;
      limitRowToAdd.tbxValue21.text ="";
    }
    this.setNewFeatureLimitRowDataAddUser(limitRowToAdd,2, category);
  },
  getFeaturesForBulkUpdateAddUser : function(option,category){
    var bulkUpdateFeatures={};
    var featureJSON={};
    var actionsJSON={};
    var actions;
    var featuresList = category === "enroll" ? (option === 1 ? this.bulkUpdateAllFeaturesList : this.bulkUpdateAllFeaturesLimits) :
                                               this.bulkUpdateAllFeaturesListContract;
    for(var i=0;i<featuresList.length;i++){
      featureJSON={};
      featureJSON.featreName=featuresList[i].featureName;
      featureJSON.actions=[];
      var actions = featuresList[i].actions || featuresList[i].permissions;
      for(var j=0;j<actions.length;j++){
        actionsJSON={};
        actionsJSON.actionName=actions[j].actionName;
        actionsJSON.actionId=actions[j].actionId;
        actionsJSON.isChecked=true;
        actionsJSON.type=actions[j].isAccountLevel;
        actionsJSON.limitVal="0";
        actionsJSON.dependentActions=[];
        if(featuresList[i].actions[j].dependentActions && featuresList[i].actions[j].dependentActions.length>0){
          if(typeof featuresList[i].actions[j].dependentActions==="string")//as we are getting string format in edit flow and object format in create flow
            actionsJSON.dependentActions=(featuresList[i].actions[j].dependentActions.substring(1,featuresList[i].actions[j].dependentActions.length-1)).split(",");
          else
            actionsJSON.dependentActions=featuresList[i].actions[j].dependentActions.map(function(rec){return rec.id});
        }
        featureJSON.actions.push(actionsJSON);
      }
      bulkUpdateFeatures[featuresList[i].featureId]=featureJSON;
    }
    this.bulkUpdateList = bulkUpdateFeatures;
  },
  getListForListBoxAddUser: function(option,data) {
    var self = this;
    var finalList = [];
    var selectOption = [["select", "Select a feature"]];
    if(option ===1){
      for (var i = 0; i < self.bulkUpdateAllFeaturesList.length; i++) {
        var check = data ? data.contains(self.bulkUpdateAllFeaturesList[i].featureId) : (!finalList.contains(self.bulkUpdateAllFeaturesList[i].featureId));
        if(check)
          finalList.push([self.bulkUpdateAllFeaturesList[i].featureId,self.bulkUpdateAllFeaturesList[i].featureName]);
      }
    } else{
      var limitFeatures=[];
      var limitActions=[];
      var dataToSet=[];
      for(var a=0;a<self.bulkUpdateAllFeaturesLimits.length;a++){
        var actions = self.bulkUpdateAllFeaturesLimits[a].actions || self.bulkUpdateAllFeaturesLimits[a].permissions;
        if(actions.length>0){
          limitFeatures=JSON.parse(JSON.stringify(self.bulkUpdateAllFeaturesLimits[a]));
          limitActions = actions.filter(function(item) {
            return ((item.isAccountLevel === "true" || item.isAccountLevel === "1") && item.limits);
          });
          if(limitActions.length>0){
            limitFeatures.actions=limitActions;
            dataToSet.push(limitFeatures);
          }
        }
      }
      for (var i = 0; i < dataToSet.length; i++) {
        var check = data ? data.contains(self.bulkUpdateAllFeaturesLimits[i].featureId) : (!finalList.contains(self.bulkUpdateAllFeaturesLimits[i].featureId));
        if (check) {
          finalList.push([dataToSet[i].featureId,dataToSet[i].featureName]);
        }
      }
    }
    finalList = selectOption.concat(finalList);
    return finalList;
  },
  getAccountsBasedOnAccType : function(accountsMap){
    var accountJson = {};
    accountsMap.forEach(function(account,key){
      (accountJson[account.accountType] = accountJson[account.accountType] || []).push(account);
    });
    return accountJson;
  },
  toggleSelectAccountsArrow : function(context){
    var selSecInd = context.rowContext.sectionIndex;
    var selRowInd = context.rowContext.rowIndex;
    var segData = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.data;
    for(var i=0; i<segData.length; i++){
      if(selSecInd !== i){
        segData[i][0].lblIconToggleArrow.text = "\ue922"; //right-arrow
        segData[i][0].lblIconToggleArrow.skin = "sknIcon00000015px";
        segData[i][0].flxHeaderContainer.isVisible = false;
        segData[i][1] = this.showHideSegRowFlexAddUser(segData[i][1],false);
        segData[i][0].flxAccountSectionCont.skin = "sknFlxBgF5F6F8BrD7D9E0Br1pxRd3px";

      }
    }
    if(segData[selSecInd][0].lblIconToggleArrow.skin === "sknIcon00000015px"){
      segData[selSecInd][0].lblIconToggleArrow.text = "\ue915"; //down-arrow
      segData[selSecInd][0].lblIconToggleArrow.skin = "sknIcon00000014px";
      segData[selSecInd][0].flxHeaderContainer.isVisible = true;
      segData[selSecInd][1] = this.showHideSegRowFlexAddUser(segData[selSecInd][1],true);
      segData[selSecInd][0].flxAccountSectionCont.skin = "sknFlxBgF5F6F8BrD7D9E0rd3pxTopRound";
      this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.info.allData[segData[selSecInd][0].lblFeatureName] = segData[selSecInd][1];
      this.setDataForOwnershipFilterBulk(segData[selSecInd][1]);
    } else{
      segData[selSecInd][0].lblIconToggleArrow.text = "\ue922"; //right-arrow
      segData[selSecInd][0].lblIconToggleArrow.skin = "sknIcon00000015px";
      segData[selSecInd][0].flxHeaderContainer.isVisible = false;
      segData[selSecInd][1] = this.showHideSegRowFlexAddUser(segData[selSecInd][1],false);
      segData[selSecInd][0].flxAccountSectionCont.skin = "sknFlxBgF5F6F8BrD7D9E0Br1pxRd3px";
    }
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.setData(segData);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxFilterMenu.setVisibility(false);
  },
  setDataForOwnershipFilterBulk : function(accountsData){
    var self = this;
    var widgetMap = {
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var ownershipList =[], maxSizeOwnerTypeText ="";
    for(var i=0;i<accountsData.length;i++){
      if(!ownershipList.includes(accountsData[i].lblAccountHolder.text))
        ownershipList.push(accountsData[i].lblAccountHolder.text);
    }
    var ownershipData = ownershipList.map(function(rec){
      maxSizeOwnerTypeText=rec.length > maxSizeOwnerTypeText.length ? rec: maxSizeOwnerTypeText;
      return {
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": self.AdminConsoleCommonUtils.checkboxnormal,
        "lblDescription": rec
      };
    });
    this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.setData(ownershipData);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxFilterMenu.width=this.AdminConsoleCommonUtils.getLabelWidth(maxSizeOwnerTypeText)+55+"px";
    var selOwnerInd = [];
    for(var j=0;j<ownershipList.length;j++){
      selOwnerInd.push(j);
    }
    this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.selectedIndices = [[0,selOwnerInd]];
    this.view.forceLayout();
  },
  showHideSegRowFlexAddUser : function(rowsData,visibility){
    for(var i=0;i<rowsData.length;i++){
      if(rowsData[i].flxContractEnrollFeaturesEditRow){  // edit features
        rowsData[i].isRowVisible =visibility;
        rowsData[i].flxContractEnrollFeaturesEditRow.isVisible = visibility;
      }else if(rowsData[i].flxAssignLimits){ //edit limits
        rowsData[i].isRowVisible =visibility;
        rowsData[i].flxAssignLimits.isVisible =visibility;
      } else{ //accounts in bulk update
        rowsData[i].isRowVisible =visibility;
        rowsData[i].flxContractEnrollAccountsEditRow.isVisible = visibility;
      }
    }
    return rowsData;
  },
  updateExistingRowsListboxDataAddUser : function(currRowPath,option,category){
    var currRowData;
    var allRows = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    var updatedList = this.getUnselectedFeaturesListAddUser(option,category);
    for(var i=0;i<allRows.length;i++){
      if(currRowPath.id !== allRows[i].id){
        currRowData = [];
        currRowData.push(allRows[i].lstBoxFieldValue11.selectedKeyValue);
        var lstMasterData = currRowData.concat(updatedList);
        allRows[i].lstBoxFieldValue11.masterData = lstMasterData;
      }
    }
    if(updatedList.length === 0){
      this.view.bulkUpdateFeaturesPopupForAddUser.btnAddNewRow.setVisibility(false);
    }
  },
  getUnselectedFeaturesListAddUser: function(option, category) {
    var assignedData = [],
        allFeaturesId = [],diffList = [],commonList = [];
    var rowsList = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    //get all assigned feature id's
    if(option === 1){//features bulk update list
      for (var i = 0; i < rowsList.length; i++) {
        if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select") assignedData.push(rowsList[i].lstBoxFieldValue11.selectedKey);
      }   
    }else{//limits bulk update list
      for (var i = 0; i < rowsList.length; i++) {
        if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select"){
          if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems&&rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems.length===rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.data.length)
            assignedData.push(rowsList[i].lstBoxFieldValue11.selectedKey);
        }
      }
    }
     //all exsisting feature id's
    var allFeaturesList = category === "enroll" ? (option === 1 ? this.bulkUpdateAllFeaturesList : this.bulkUpdateAllFeaturesLimits) :
                                                   this.bulkUpdateAllFeaturesListContract;
    allFeaturesId = allFeaturesList.map(function(rec) {
      return rec.featureId;
    });
    //differentiate common and diff id's
    for (var j = 0; j < allFeaturesId.length; j++) {
      if (assignedData.contains(allFeaturesId[j])) {
        commonList.push(allFeaturesId[j]);
      } else {
        diffList.push(allFeaturesId[j]);
      }
    }
    var finalList = this.getListForListBoxAddUser(option, diffList);
    return finalList;
  },
  updateFeatureLimitsBulkChangesAddUser : function(option){
    var rowsList = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    var featureId="";
    var actionIds=[];
    var isEnable=false;
    var bulkUpdateList=[];
    var typeValue=this.getSelectedTypeAddUser(option);
    var selAccId = [];
    var accSegData = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.data;
    //get all selected accounts
    for(var i=0;i<accSegData.length ;i++){
      for(var j=0; j<accSegData[i][1].length; j++){
        if(accSegData[i][1][j].imgCheckbox.src === this.AdminConsoleCommonUtils.checkboxSelected)
          selAccId.push(accSegData[i][1][j].id)
      }
    }
    //get all assigned feature,action id's
    for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select")
        featureId=rowsList[i].lstBoxFieldValue11.selectedKey;
      actionIds=[];
      if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems){
        var selItems=rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems;
        for(let i=0;i<selItems.length;i++){
          actionIds.push(selItems[i].actionId);
        }
      }
      if(option=== 1){
        bulkUpdateList.push({"featureId":featureId,"actionIds":actionIds,"isEnabled":typeValue==="enable"?"true":"false"});
      }else{
        bulkUpdateList.push({"featureId":featureId,"actionIds":actionIds,"limitId":typeValue,
                             "limitVal1":rowsList[i].tbxValue21.text, "limitVal2":rowsList[i].tbxValue22.text});
      }
    }
    var isUpdate = option === 1 ? this.updateBulkFeaturesInEditUserObj(selAccId,bulkUpdateList) :
                                  this.updateBulkLimitsInEditUserObj(selAccId,bulkUpdateList);
    if(isUpdate){
      this.view.flxBulkUpdateFeaturesPopupForAddUser.setVisibility(false);
      if(option === 1){
        this.toggleFeaturesAccountLevel();
        this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.BulkPermissionsUpdateSuccess"), this);
      }
      else{
        this.toggleLimitsAccountLevel();
        this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.BulkLimitsUpdateSuccess"), this);
      }
    }
  },
  getSelectedTypeAddUser : function(option) {
    var radBtn =  this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton1.src;
    var radBtn1 = this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton2.src;
    var radBtn2 = this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton3.src;
    if(option === 1){
      if(radBtn === this.AdminConsoleCommonUtils.radioSelected) 
        return "enable";
      else
        return "disable";
    }else{
      if(radBtn === this.AdminConsoleCommonUtils.radioSelected) 
        return this.limitId.MAX_TRANSACTION_LIMIT;
      if( radBtn1 === this.AdminConsoleCommonUtils.radioSelected) 
        return this.limitId.DAILY_LIMIT;
      if(radBtn2 === this.AdminConsoleCommonUtils.radioSelected)
        return this.limitId.WEEKLY_LIMIT;
    }
  },
  updateBulkFeaturesInEditUserObj : function(selAccId, bulkUpdateList){
    var count = 0;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var custId = selCustData.orignalData.coreCustomerId;
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(custId);
    var currCustAccFeatures = editUserObj.accountMapFeatures;
    for(var i=0; i<selAccId.length ;i++){  
      //get features of the acc id
      var currSelAccObj = editUserObj.accountMapFeatures.get(selAccId[i]);
      var accountFeatures = JSON.parse(currSelAccObj.features);
      for(var m=0; m<accountFeatures.length; m++){
        for(var j=0; j<bulkUpdateList.length; j++){  
          //get the selected feature obj
          if(accountFeatures[m].featureId === bulkUpdateList[j].featureId){
            count =0;
            var currActions = accountFeatures[m].actions || accountFeatures[m].permissions;
            for(var l=0; l<currActions.length; l++){
              for(var k=0; k<bulkUpdateList[j].actionIds.length; k++){
                //update the selected actions
                if(currActions[l].actionId === bulkUpdateList[j].actionIds[k]){
                  if(bulkUpdateList[j].isEnabled === "true"){
                    currActions[l].isEnabled = "true";
                    count = count+1;
                    this.addAccountIdForAction(currActions[l].actionId, selAccId[i]);
                  }else{
                    currActions[l].isEnabled = "false";
                    this.removeAccountIdForAction(currActions[l].actionId, selAccId[i], 1);
                  }
                  break;
                }
              }
            }
            accountFeatures[m].isEnabled = count === 0 ? "false" : "true";
            break;
          }
        }
      }
      currSelAccObj.features = JSON.stringify(accountFeatures);
      editUserObj.accountMapFeatures.set(selAccId[i], currSelAccObj);
    }
    this.presenter.setAccountsFeaturesForAddUser(custId,editUserObj);
    return true;
  },
  updateBulkLimitsInEditUserObj : function(selAccId, bulkUpdateList){
    var count = 0;
    var selCustData = this.view.segEnrollCustList.data[this.enrollSegRowInd];
    var custId = selCustData.orignalData.coreCustomerId;
    var editUserObj = this.presenter.getAccountsFeaturesForAddUser(custId);
    var currCustAccFeatures = editUserObj.accLevelLimits;
    for(var i=0; i<selAccId.length ;i++){  
      //get features of the acc id
      var currSelAccObj = editUserObj.accLevelLimits.get(selAccId[i]);
      var accountFeatures = JSON.parse(currSelAccObj.limits);
      for(var m=0; m<accountFeatures.length; m++){
        for(var j=0; j<bulkUpdateList.length; j++){  
          //get the selected feature obj
          if(accountFeatures[m].featureId === bulkUpdateList[j].featureId){
            count =0;
            var currActions = accountFeatures[m].actions || accountFeatures[m].permissions;
            for(var l=0; l<currActions.length; l++){
              for(var k=0; k<bulkUpdateList[j].actionIds.length; k++){
                //get the selected actions
                if(currActions[l].actionId === bulkUpdateList[j].actionIds[k]){
                  if(bulkUpdateList[j].limitId === this.limitId.MAX_TRANSACTION_LIMIT){
                    currActions[l].limits[this.limitId.PRE_APPROVED_TRANSACTION_LIMIT] = bulkUpdateList[j].limitVal1;
                    currActions[l].limits[this.limitId.AUTO_DENIED_TRANSACTION_LIMIT] = bulkUpdateList[j].limitVal2;
                    
                  }else if(bulkUpdateList[j].limitId === this.limitId.DAILY_LIMIT){
                   currActions[l].limits[this.limitId.PRE_APPROVED_DAILY_LIMIT] = bulkUpdateList[j].limitVal1;
                    currActions[l].limits[this.limitId.AUTO_DENIED_DAILY_LIMIT] = bulkUpdateList[j].limitVal2;

                  } else if(bulkUpdateList[j].limitId === this.limitId.WEEKLY_LIMIT){
                    currActions[l].limits[this.limitId.PRE_APPROVED_WEEKLY_LIMIT] = bulkUpdateList[j].limitVal1;
                    currActions[l].limits[this.limitId.AUTO_DENIED_WEEKLY_LIMIT] = bulkUpdateList[j].limitVal2;
                  }
                  break;
                }
              }
            }
            break;
          }
        }
      }
      currSelAccObj.limits = JSON.stringify(accountFeatures);
      editUserObj.accLevelLimits.set(selAccId[i], currSelAccObj);
    }
    this.presenter.setAccountsFeaturesForAddUser(custId,editUserObj);
    return true;
  },
  addAccountsTag : function(accountType){
    var self = this;
    var tagsCount=self.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.widgets().length;
    var newTextTag = self.view.bulkUpdateFeaturesPopupForAddUser.flxSelectionTag.clone(tagsCount.toString());
    var lblname = tagsCount + "lblTagName";
    var imgname = tagsCount + "flxCross";
    var textToSet = accountType.accType + " ("+ accountType.count + ")";
    var flexWidth= this.AdminConsoleCommonUtils.getLabelWidth(textToSet,"12px Lato-Regular");
    self.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.info.added.push(accountType);
    newTextTag[lblname].text = textToSet;
    newTextTag[lblname].tooltip = textToSet;
    newTextTag.isVisible = true;
    newTextTag.width=flexWidth+10+10+15+"px";//labelwidth+left padding+right padding+ cross image width
    var parentWidth= self.view.bulkUpdateFeaturesPopupForAddUser.flxContractDetailsPopupContainer.frame.width - 40;
    var leftVal=20;
    var topVal=0;
    var lineCount=0;
    for(var a=tagsCount-1;a>=0;a--){
      var childWid = self.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.widgets();
      var i= childWid[a].id;
      leftVal=leftVal+(self.view.bulkUpdateFeaturesPopupForAddUser[i].frame.width+15);
      if((leftVal+flexWidth+50)>parentWidth){
        leftVal=20;
        lineCount=lineCount+1;
      }
    }
    newTextTag.left=leftVal+"px";
    if(lineCount>1){
      newTextTag.top=(lineCount-1)*40+"px";
    }else{
      newTextTag.top=topVal+"px";
    }
    self.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.addAt(newTextTag, -1);
    newTextTag[imgname].onTouchStart = function () {
      self.removeTagEditUser(tagsCount);
    };
    self.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.setVisibility(true);
    this.view.forceLayout();
  },
  validateBulkSelectionEditUser : function(){
    var rowsList = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    var isValid=true;
    var selCount=0;
    for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select"){
        selCount=selCount+1;
        //validation for action selection
        if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems===null){
          isValid=false;
          rowsList[i].flxFieldValueContainer12.skin = "sknFlxCalendarError";
          rowsList[i].lblErrorMsg12.text = "Select atleast one action";
          rowsList[i].flxErrorField12.setVisibility(true);
        }
        //validation for limits
        if(this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info.option ===2){
          if(rowsList[i].tbxValue21.text.trim().length===0){
            isValid=false;
            rowsList[i].lblErrorMsg21.text = "Value cannot be empty";
            rowsList[i].flxErrorField21.setVisibility(true);
            rowsList[i].flxValue21.skin = "sknFlxCalendarError";
          }
          if(rowsList[i].tbxValue22.text.trim().length===0){
            isValid=false;
            rowsList[i].lblErrorMsg.text = "Value cannot be empty";
            rowsList[i].flxErrorField22.setVisibility(true);
            rowsList[i].flxValue22.skin = "sknFlxCalendarError";
          }
        }
      }
    }
    //validation for feature selection
    if(selCount===0){
      isValid=false;
      rowsList[0].lstBoxFieldValue11.skin="sknLstBoxeb3017Bor3px";
      rowsList[0].lblErrorMsg11.text="Select atleast one feature";
      rowsList[0].flxErrorField11.setVisibility(true);
    }
    this.view.forceLayout();
    return isValid;
  },
  searchForAccountsInBulkUpdate : function(){
    var searchText = this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.text;
    var searchResults = [];
    if(searchText.length > 0){
      var filteredRows = [];
      var accountCards = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.data;
      for(var i=0;i<accountCards.length;i++){
        filteredRows =[];
        for(var j=0;j<accountCards[i][1].length;j++){
          if(accountCards[i][1][j].lblAccountNumber.text.indexOf(searchText) >= 0 ||
             accountCards[i][1][j].lblAccountName.text.indexOf(searchText) >= 0){
            accountCards[i][1][j].flxContractEnrollAccountsEditRow.isVisible = true;
            filteredRows.push(accountCards[i][1][j]);
          }
        }
        if(filteredRows.length > 0){ // show the account section expanded if it contains rows
          accountCards[i][0].lblIconToggleArrow.text = "\ue915"; //down-arrow
          accountCards[i][0].lblIconToggleArrow.skin = "sknIcon00000014px";
          accountCards[i][0].flxHeaderContainer.isVisible = true;
          accountCards[i][0].flxAccountSectionCont.skin = "sknFlxBgF5F6F8BrD7D9E0rd3pxTopRound";
          searchResults.push([accountCards[i][0],filteredRows]);
        }
      }
    } else{
      searchResults = accountCards;
    }
    if(searchResults.length > 0){
      this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.setData(searchResults);
    }
  },
  resetAddedRowsAddUser : function(){
    var flxChildren = this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.widgets();
    this.view.bulkUpdateFeaturesPopupForAddUser.flxAddNewRowListCont.removeAll();
    for(var i=0;i<flxChildren.length;i++){
      this.view.bulkUpdateFeaturesPopupForAddUser.remove(this.view.bulkUpdateFeaturesPopupForAddUser[flxChildren[i].id]);
    }
    var height = this.view.bulkUpdateFeaturesPopupForAddUser.flxContractDetailsPopupContainer.frame.height - (70 + this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateListContainer.frame.y + 80);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxBulkUpdateListContainer.height = height + "dp";
    
    var bulkUpdateOption = this.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info.option;
    this.bulkUpdateListboxData = this.getListForListBoxAddUser(bulkUpdateOption);
    if(bulkUpdateOption === 1){
      this.addNewFeatureRowBulkUpdateAddUser("enroll");
    }else{
      this.addNewLimitRowBulkUpdateAddUser("enroll");
    }  
    this.view.forceLayout();
  },
  filterAccountRowsInBulkUpdate : function(){
    var selFilter =[], dataToShow =[],count = 0;
    var selInd = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.selectedsectionindex;
    var segData = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.info.allData;
    var sectionData = this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.data[selInd][0];
    var accountsData = segData[sectionData.lblFeatureName];
    var ownershipIndices = this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.selectedIndices;
    var selOwnershipInd = ownershipIndices ? ownershipIndices[0][1] : [];
    for (var j = 0; j < selOwnershipInd.length; j++) {
      selFilter.push(this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.data[selOwnershipInd[j]].lblDescription);
    }
    for(var i=0;i<accountsData.length; i++){
      if (selFilter.indexOf(accountsData[i].lblAccountHolder.text) >= 0){
        accountsData[i].flxContractEnrollAccountsEditRow.isVisible = true;
        count = count +1;
      }
      else
        accountsData[i].flxContractEnrollAccountsEditRow.isVisible = false;
    }
    var headerChecboxImg = count === 0 ? this.AdminConsoleCommonUtils.checkboxnormal :
        (count === accountsData.length ? this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxPartial);
    sectionData.imgSectionCheckbox.src= headerChecboxImg;
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.rowTemplate = "flxContractEnrollAccountsEditRow";
    this.view.bulkUpdateFeaturesPopupForAddUser.segSelectionList.setSectionAt([sectionData,accountsData], selInd);
    this.view.bulkUpdateFeaturesPopupForAddUser.flxFilterMenu.setVisibility(false);
    this.view.forceLayout();
  },
  searchFeaturesLimitsAccountLevel : function(option){
    var allAccountCards = (option ===1) ? this.view.flxAddedUserEditAccFeaturesList.widgets() : this.view.flxAddedUserEditAccLimitsList.widgets();
    var searchText = (option ===1) ?this.view.searchEditFeatures.tbxSearchBox.text : this.view.searchEditLimits.tbxSearchBox.text;
    var accountCard = this.filterFeaturesLimitsBasedOnAccType(option);
    //hide the accounts based on the filter result
    for(var p=0; p<allAccountCards.length; p++){
      for(var q=0; q<accountCard.length; q++){
        if(accountCard[q].info.accDetails.accountId === allAccountCards[p].info.accDetails.accountId){
          allAccountCards[p].isVisible = true;
        }else{
          allAccountCards[p].isVisible = false;
        }
      }
    }
    // has filtered accounts
    if(accountCard.length > 0){ 
      if(searchText.length > 0){ //filter for search text if any
        for(var i=0;i<accountCard.length;i++){ 
          if(accountCard[i]["lblName"].text.indexOf(searchText) >= 0){
            accountCard[i].isVisible = true;
          } else{
            accountCard[i].isVisible = false
          }
        }
      } /*else{ //show only filter accounts
        for(var i=0;i<accountCard.length;i++){
          accountCard[i].isVisible = true;
        }
      }*/
      if(option === 1){
        this.view.flxAddedUserEditAccFeaturesList.setVisibility(true);
        this.view.flxEnrollEditNoResultAccFeatures.setVisibility(false);
      } else{
        this.view.flxAddedUserEditAccLimitsList.setVisibility(true);
        this.view.flxEnrollEditNoResultAccLimits.setVisibility(false);
      }
    } 
    //no accounts with selected filter - hide all accounts
    else { 
      for(var i=0;i<allAccountCards.length;i++){
          allAccountCards[i].isVisible = false;
        }
      if(option === 1){
        this.view.flxAddedUserEditAccLimitsList.setVisibility(false);
        this.view.flxEnrollEditNoResultAccFeatures.setVisibility(true);
      } else{
        this.view.flxAddedUserEditAccLimitsList.setVisibility(false);
        this.view.flxEnrollEditNoResultAccLimits.setVisibility(true);
      }
    }
    this.view.forceLayout();
  },
  searchFeaturesCustomerLevel : function(option){
    var searchWidgetPath,cardWidgetPath;
    if(option === 1){
      searchWidgetPath =this.view.searchEditFeatures;
      cardWidgetPath = this.view.addedUserEditFeaturesCard;
    }else{
      searchWidgetPath =this.view.searchEditOtherFeatures;
      cardWidgetPath = this.view.addedUserEditOtherFeaturesCard;
    }
    var searchText = searchWidgetPath.tbxSearchBox.text;
    var actualData = cardWidgetPath.segAccountFeatures.info.segDataJSON;
    var featureNamesList = Object.keys(actualData);
    var filterData = [],filteredSection = [],filteredRowData = [],updateDetailsObj =[];
    if(searchText.length > 0){
      for(var i=0; i<featureNamesList.length;i++){
        filteredRowData = [];
        //search for action first
        for(var j=0; j<actualData[featureNamesList[i]][1].length;j++){
          updateDetailsObj =[]
          updateDetailsObj.push(actualData[featureNamesList[i]][0]);
          updateDetailsObj.push(actualData[featureNamesList[i]][1]);
          if(updateDetailsObj[1][j].lblFeatureName.text.toLowerCase().indexOf(searchText) >= 0){       
            filteredRowData.push(updateDetailsObj[1][j]);
          }
        }
        if(filteredRowData.length >0){
          updateDetailsObj[0].imgSectionCheckbox.src = this.getHeaderCheckboxImage(filteredRowData,true, true);
          filterData.push([updateDetailsObj[0],filteredRowData]);  
        }else{ // filter for only feature
          if(updateDetailsObj[0].lblFeatureName.toLowerCase().indexOf(searchText) >= 0){
             updateDetailsObj[1] = this.showHideSegRowFlex(updateDetailsObj[1],false);
             filterData.push(updateDetailsObj);
          }
        }
      }
    } else{
      filterData = Object.values(actualData);
    }
    cardWidgetPath.imgSectionCheckbox.src = this.getHeaderCheckboxImage(cardWidgetPath.segAccountFeatures.data,false, true);
    cardWidgetPath.lblCount.text = this.getSelectedItemsCount(filterData, false);
    cardWidgetPath.lblTotalCount.text = "of "+ this.getTwoDigitNumber(filterData.length);
    cardWidgetPath.segAccountFeatures.rowTemplate = "flxContractEnrollFeaturesEditRow";
    cardWidgetPath.segAccountFeatures.setData(filterData);
    this.view.forceLayout();
  },
  setFlowActions : function(){
    var scopeObj = this;
    this.view.searchEditAccounts.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.searchEditAccounts.setSearchBoxFocus(true);
    };
    this.view.searchEditAccounts.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.searchEditAccounts.setSearchBoxFocus(false);
    };
    this.view.searchEditAccounts.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.searchEditAccounts.tbxSearchBox.text === ""){
        scopeObj.view.searchEditAccounts.clearSearchBox();
      } else{
        scopeObj.view.searchEditAccounts.setSearchBoxFocus(true);
        scopeObj.view.searchEditAccounts.flxSearchCancel.setVisibility(true);
        scopeObj.view.searchEditAccounts.forceLayout();
      }
      scopeObj.searchFilterForAccounts();
    };
    this.view.searchEditAccounts.flxSearchCancel.onClick = function(){
      scopeObj.view.searchEditAccounts.clearSearchBox();
      scopeObj.searchFilterForAccounts();
    };
    this.view.searchEditFeatures.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.searchEditFeatures.setSearchBoxFocus(true);
    };
    this.view.searchEditFeatures.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.searchEditFeatures.setSearchBoxFocus(false);
    };
    this.view.searchEditFeatures.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.searchEditFeatures.tbxSearchBox.text === ""){
        scopeObj.view.searchEditFeatures.clearSearchBox();
      } else{
        scopeObj.view.searchEditFeatures.setSearchBoxFocus(true);
        scopeObj.view.searchEditFeatures.flxSearchCancel.setVisibility(true);
        scopeObj.view.searchEditFeatures.forceLayout();
      }
      if(scopeObj.view.flxAddedUserEditFeaturesList.isVisible === true){
        scopeObj.searchFeaturesCustomerLevel(1);
      }else{
        scopeObj.searchFeaturesLimitsAccountLevel(1);
      }
    };
    this.view.searchEditFeatures.flxSearchCancel.onClick = function(){
      scopeObj.view.searchEditFeatures.clearSearchBox();
      if(scopeObj.view.flxAddedUserEditFeaturesList.isVisible === true){
        scopeObj.searchFeaturesCustomerLevel(1);
      }else{
        scopeObj.searchFeaturesLimitsAccountLevel(1);
      }
    };
    this.view.searchEditOtherFeatures.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.searchEditOtherFeatures.setSearchBoxFocus(true);
    };
    this.view.searchEditOtherFeatures.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.searchEditOtherFeatures.setSearchBoxFocus(false);
    };
    this.view.searchEditOtherFeatures.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.searchEditOtherFeatures.tbxSearchBox.text === ""){
        scopeObj.view.searchEditOtherFeatures.clearSearchBox();
      } else{
        scopeObj.view.searchEditOtherFeatures.setSearchBoxFocus(true);
        scopeObj.view.searchEditOtherFeatures.flxSearchCancel.setVisibility(true);
        scopeObj.view.searchEditOtherFeatures.forceLayout();
      }
      scopeObj.searchFeaturesCustomerLevel(2);
    };
    this.view.searchEditOtherFeatures.flxSearchCancel.onClick = function(){
      scopeObj.view.searchEditOtherFeatures.clearSearchBox();
      scopeObj.searchFeaturesCustomerLevel(2);
    };
    this.view.searchEditLimits.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.searchEditLimits.setSearchBoxFocus(true);
    };
    this.view.searchEditLimits.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.searchEditLimits.setSearchBoxFocus(false);
    };
    this.view.searchEditLimits.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.searchEditLimits.tbxSearchBox.text === ""){
        scopeObj.view.searchEditLimits.clearSearchBox();
      } else{
        scopeObj.view.searchEditLimits.setSearchBoxFocus(true);
        scopeObj.view.searchEditLimits.flxSearchCancel.setVisibility(true);
        scopeObj.view.searchEditLimits.forceLayout();
      }
      if(scopeObj.view.flxEnrollEditLimitsList.isvisible === true){  
      }else{ //acc level tab
        scopeObj.searchFeaturesLimitsAccountLevel(2);
      }
    };
    this.view.searchEditLimits.flxSearchCancel.onClick = function(){
      scopeObj.view.searchEditLimits.clearSearchBox();
      if(scopeObj.view.flxEnrollEditLimitsList.isvisible === true){
      }else{ //acc level tab
        scopeObj.searchFeaturesLimitsAccountLevel(2);
      }
    };
    this.view.btnBulkUpdateFeatures.onClick = function(){
      scopeObj.showBulkUpdatePopupEditUser(1);
    };
    this.view.btnBulkUpdateLimits.onClick = function(){
      scopeObj.showBulkUpdatePopupEditUser(2);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen1.btnSave.onClick = function(){
      scopeObj.getSelectedAccountTypesCount();
      scopeObj.showBulkUpdatePopupScreenAddUser2();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.flxFilterMenu.onHover = this.onHoverEventCallback;
    this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen1.btnCancel.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopupForAddUser.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.flxPopUpClose.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopupForAddUser.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen2.btnSave.onClick = function(){
        if(scopeObj.validateBulkSelectionEditUser())
          scopeObj.updateFeatureLimitsBulkChangesAddUser(scopeObj.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info.option);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.commonButtonsScreen2.btnCancel.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopupForAddUser.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.btnAddNewRow.onClick = function(){
      if(scopeObj.view.bulkUpdateFeaturesPopupForAddUser.lblDetailsHeading.info.option === 1){
        scopeObj.addNewFeatureRowBulkUpdateAddUser("enroll");
      } else{
        scopeObj.addNewLimitRowBulkUpdateAddUser("enroll");
      }  
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.setSearchBoxFocus(true);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.setSearchBoxFocus(false);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.tbxSearchBox.text === ""){
        scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.clearSearchBox();
      } else{
        scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.setSearchBoxFocus(true);
        scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.flxSearchCancel.setVisibility(true);
        scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.forceLayout();
      }
      scopeObj.searchForAccountsInBulkUpdate();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.flxSearchCancel.onClick = function(){
      scopeObj.view.bulkUpdateFeaturesPopupForAddUser.searchBoxScreen1.clearSearchBox();
      scopeObj.searchForAccountsInBulkUpdate();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton1.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton2.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.customRadioButtonGroup.imgRadioButton3.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.btnModifySearch.onClick = function(){
        scopeObj.showBulkUpdatePopupScreenAddUser1();
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.flxArrow.onClick = function(){
      scopeObj.view.bulkUpdateFeaturesPopupForAddUser.flxTagsContainer.setVisibility(!scopeObj.view.bulkUpdateFeaturesPopupForAddUser.isVisible);
    };
    this.view.bulkUpdateFeaturesPopupForAddUser.filterMenu.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.filterAccountRowsInBulkUpdate();
    };
    this.view.flxAddedUserEditFeatureFilter.onClick = function(){
      scopeObj.showAccountTypesFilter(1);
      var selInd = scopeObj.view.customListBoxAccounts.segList.selectedRowIndices;
      var indicesToSet = (selInd && selInd.length > 0) ? JSON.stringify(selInd[0][1]) : JSON.stringify([]);
      scopeObj.view.customListBoxAccounts.segList.info.prevSelInd = indicesToSet;
    };
    this.view.flxAddedUserEditLimitsFilter.onClick = function(){
      scopeObj.showAccountTypesFilter(2);
      var selInd = scopeObj.view.customListBoxAccounts.segList.selectedRowIndices;
      var indicesToSet = (selInd && selInd.length > 0) ? JSON.stringify(selInd[0][1]) : JSON.stringify([]);
      scopeObj.view.customListBoxAccounts.segList.info.prevSelInd = indicesToSet;
    };
    this.view.btnApplyFilter.onClick = function(){
      if(scopeObj.view.flxAddedUserEditFeaturesContainer.isVisible === true){
        scopeObj.searchFeaturesLimitsAccountLevel(1);
      } else{
        scopeObj.searchFeaturesLimitsAccountLevel(2);
      }
      scopeObj.view.flxAccountTypesFilter.setVisibility(false);
    };
    this.view.flxImage.onClick = function(){
      var prevSelInd = JSON.parse(scopeObj.view.customListBoxAccounts.segList.info.prevSelInd);
      scopeObj.view.customListBoxAccounts.segList.selectedRowIndices = [[0,prevSelInd]];
      scopeObj.view.customListBoxAccounts.lblSelectedValue.text = scopeObj.view.customListBoxAccounts.segList.setData.info.selectedText;
      scopeObj.view.flxAccountTypesFilter.setVisibility(false);
    };
    this.view.customListBoxAccounts.flxSelectedText.onClick = function(){
      var isVisible = scopeObj.view.customListBoxAccounts.flxSegmentList.isVisible;
      scopeObj.view.customListBoxAccounts.flxSegmentList.setVisibility(!isVisible);
    };
    this.view.customListBoxAccounts.segList.onRowClick = function(){
      var segData = scopeObj.view.customListBoxAccounts.segList.data;
      var selInd = scopeObj.view.customListBoxAccounts.segList.selectedRowIndices;
      var selRows = (selInd && selInd.length > 0) ? selInd[0][1] : [];
      var selText = (selRows.length > 1 || selRows.length === 0) ?
          selRows.length+" "+kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Selected") : segData[selRows[0]].lblDescription; 
      scopeObj.view.customListBoxAccounts.lblSelectedValue.text = selText;
    };
    this.view.accountTypesFilterMenuAddUser.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.searchFilterForAccounts();
    };
    this.view.ownershipFilterMenuAddUser.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.searchFilterForAccounts();
    };
    this.view.btnClearOtherCustomer.onClick = function(){
      scopeObj.clearAllinfoOtherCustomerSearch();
    };
    this.view.flxExistingUserName.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserName.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxExistingUserCustId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserCustId.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxExistingUserTaxId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserTaxId.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxExistingUserDOB.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserDOB.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxExistingUserPhoneNumber.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserPhoneNumber.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxExistingUserEmailId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserEmailId.text", scopeObj.view.segExistingUserListContainer);
    };
    this.view.flxOtherRelatedCustName.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserName.text", scopeObj.view.this.view.segOtherRelatedCustList);
    };
    this.view.flxOtherRelatedCustCustId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserCustId.text", scopeObj.view.segOtherRelatedCustList);
    };
    this.view.flxOtherRelatedCustTaxId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserTaxId.text", scopeObj.view.segOtherRelatedCustList);
    };
    this.view.flxOtherRelatedCustDOB.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserDOB.text", scopeObj.view.segOtherRelatedCustList);
    };
    this.view.flxOtherRelatedCustPhoneNumber.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserPhoneNumber.text", scopeObj.view.segOtherRelatedCustList);
    };
    this.view.flxOtherRelatedCustEmailId.onClick = function(){
      scopeObj.sortSegmentData("lblExistingUserEmailId.text", scopeObj.view.segOtherRelatedCustList);
    };
    this.view.toggleButtonsLimits.btnToggleRight.onClick = function(){
      scopeObj.toggleLimitsAccountLevel();
    };
    this.view.toggleButtonsLimits.btnToggleLeft.onClick = function(){
      scopeObj.toggleLimitsCustomerLevel();
    };
    this.view.toggleButtonsFeatures.btnToggleLeft.onClick = function(){
      scopeObj.toggleFeaturesCustomerLevel();
    };
    this.view.toggleButtonsFeatures.btnToggleRight.onClick = function(){
      scopeObj.toggleFeaturesAccountLevel();
    };
    this.view.commonButtonsEditAccounts.btnSave.onClick = function(){
      var isValid = scopeObj.validateAllLimitsEditUser();
      if(isValid){
        scopeObj.collectAllInfoAndSave();
      }
    };
    this.view.commonButtonsEditFeatures.btnSave.onClick = function(){
      var isValid = scopeObj.validateAllLimitsEditUser();
      if(isValid){
        scopeObj.collectAllInfoAndSave();
      }
    };
    this.view.commonButtonsEditOF.btnSave.onClick = function(){
      var isValid = scopeObj.validateAllLimitsEditUser();
      if(isValid){
        scopeObj.collectAllInfoAndSave();
      }
    };
    this.view.commonButtonsEditLimits.btnSave.onClick = function(){
      var isValid = scopeObj.validateAllLimitsEditUser();
      if(isValid){
        scopeObj.collectAllInfoAndSave();
      }
    };
    this.view.AddedUserEditVerticalTabs.btnOption1.onClick = function(){
      scopeObj.showEditAccountsScreen();
    };
    this.view.AddedUserEditVerticalTabs.btnOption2.onClick = function(){
      scopeObj.showEditFeaturesScreen();
    };
    this.view.AddedUserEditVerticalTabs.btnOption3.onClick = function(){
      scopeObj.showEditOtherFeaturesScreen();
    };
    this.view.AddedUserEditVerticalTabs.btnOption4.onClick = function(){
      scopeObj.showEditLimitsScreen();
    };
    this.view.commonButtonsEditAccounts.btnNext.onClick = function(){
      scopeObj.showEditFeaturesScreen();
    };
    this.view.commonButtonsEditFeatures.btnNext.onClick = function(){
      scopeObj.showEditOtherFeaturesScreen()
    };
    this.view.commonButtonsEditOF.btnNext.onClick = function(){
      scopeObj.showEditLimitsScreen();
    }
    this.view.commonButtonsEditAccounts.btnCancel.onClick = function(){
      scopeObj.hideEditScreen();
    };
    this.view.commonButtonsEditFeatures.btnCancel.onClick = function(){
      scopeObj.hideEditScreen();
    };
    this.view.commonButtonsEditLimits.btnCancel.onClick = function(){
      scopeObj.hideEditScreen();
    };
    this.view.commonButtonsEditOF.btnCancel.onClick = function(){
      scopeObj.hideEditScreen();
    };
    this.view.btnLink1.onClick = function(){
      scopeObj.navigatetoEditAccountsPage();
    };
    this.view.btnLink2.onClick = function(){
      scopeObj.navigatetoEditAccountLevelFeaturePage();
    };
    this.view.btnLink3.onClick = function(){
      scopeObj.navigateToLimitScreen()
    }; 
	this.view.textBoxAddOtherUserEntry11.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationOnNewUser(scopeObj.view.textBoxAddOtherUserEntry11tbxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry11.flxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry11.flxInlineError);
    };
    this.view.textBoxAddOtherUserEntry13.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationOnNewUser(scopeObj.view.textBoxAddOtherUserEntry11tbxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry13.flxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry13.flxInlineError);
    };
    this.view.textBoxAddOtherUserEntry21.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationOnNewUser(scopeObj.view.textBoxAddOtherUserEntry11tbxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry21.flxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry21.flxInlineError);
    };
    this.view.textBoxAddOtherUserEntry22.txtISDCode.onEndEditing = function() {
      if (scopeObj.view.textBoxAddOtherUserEntry22.txtISDCode.text[0] !== "+") {
        scopeObj.view.textBoxAddOtherUserEntry22.txtISDCode.text = "+" + scopeObj.view.textBoxAddOtherUserEntry22.txtISDCode.text;
      }
    };
    this.view.textBoxAddOtherUserEntry22.txtISDCode.onKeyUp = function(){
      scopeObj.view.textBoxAddOtherUserEntry22.hideErrorMsg(1);
      if(scopeObj.view.textBoxAddOtherUserEntry22.txtContactNumber.text === ""){
        scopeObj.view.textBoxAddOtherUserEntry22.hideErrorMsg(2);
      }
    };
    this.view.textBoxAddOtherUserEntry22.txtContactNumber.onKeyUp = function(){
      scopeObj.view.textBoxAddOtherUserEntry22.hideErrorMsg(2);
    };
    this.view.calAddOtherUserEntry23DOB.event = function(){
      scopeObj.view.textBoxAddOtherUserEntry23Cal.skin = "sknflxffffffoptemplateop3px";
      scopeObj.view.textBoxAddOtherUserEntry23.flxInlineError.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.textBoxAddOtherUserEntry31.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidationOnNewUser(scopeObj.view.textBoxAddOtherUserEntry31tbxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry31.flxEnterValue,
                                        scopeObj.view.textBoxAddOtherUserEntry31.flxInlineError);
    };    
    this.view.btnAddUser.onClick = function(){
      scopeObj.createAddUSerPayload();
    };
    this.view.commonButtonsAddCustomer.btnCancel.onClick = function(){
      scopeObj.hideAddCustomerID();
    };
    this.view.commonButtonsAddCustomer.btnSave.onClick = function(){
	  scopeObj.addModifiedCustomerList();     
    };
    this.view.flxAddCustomerCross.onClick = function(){
      scopeObj.hideAddCustomerID();
    };
    this.view.btnAddCustomerId.onClick = function(){
      scopeObj.addCustomerID();
    };
    this.view.flxOption2.onClick = function(){
      scopeObj.removeRowFromCustSeg();
    };
    this.view.flxContextualMenu.onHover = this.onHoverEventCallback;
    this.view.commonButtonsAddOtherUser.btnSave.onClick = function(){
      scopeObj.addUserNextFunction();
    };
    this.view.btnAddUsers.onClick = function(){
      scopeObj.visbiltyChangesForAddUserButton();
    };
    this.view.addExistingcommonButtons.btnSave.onClick = function(){
       scopeObj.existingUserAddOnClick();
    };
    this.view.btnSearchAddExistingUserRelatatedCustomer.onClick = function(){
      scopeObj.searchRelatedCustomers();
    };
    this.view.btnSearchOtherCustomer.onClick = function(){
      scopeObj.searchOtherCustomers();
    };
    this.view.contractCustomers.flxDropdown.onClick = function(){
      scopeObj.toggleCustomerDropDown();
    };
    this.view.btnSearchUserCancel.onClick = function(){
      scopeObj.view.flxSearchCompanies.setVisibility(false);
      scopeObj.view.flxCreateCompany.setVisibility(false);
      scopeObj.view.flxCreateContract.setVisibility(false);
      scopeObj.view.flxCompanyDetails.setVisibility(true);
      scopeObj.view.flxAddUser.setVisibility(false);
      if(scopeObj.prevBreadCrumb.enterif)
      {
        scopeObj.view.breadcrumbs.btnBackToMain.onClick = scopeObj.prevBreadCrumb.Action;
        scopeObj.view.breadcrumbs.btnBackToMain.text = scopeObj.prevBreadCrumb.text;
        scopeObj.prevBreadCrumb.enterif = false;
      }
    }
    this.view.btntoggleRelatedOther.onClick = function(){
      scopeObj.togglePopUpScreens();
    }
    this.view.btnAddExistingUser.onClick = function(){
      scopeObj.showAddExistingPopUp();
    }
    this.view.btnAddNewUser.onClick = function(){
      scopeObj.showAddOtherUserContainer();
    }
    this.view.flxAddExistingUserCross.onClick = function(){
     scopeObj.view.flxAddUserFromExistingCustomerContainer.setVisibility(false);
    }
    this.view.addExistingcommonButtons.btnCancel.onClick = function(){
     scopeObj.view.flxAddUserFromExistingCustomerContainer.setVisibility(false);
    }
    this.view.flxAddOtherUserCross.onClick = function(){
      scopeObj.view.flxAddOtherUserContainer.setVisibility(false);
    }
    this.view.commonButtonsAddOtherUser.btnCancel.onClick = function(){
      scopeObj.view.flxAddOtherUserContainer.setVisibility(false);
    }
    this.view.btnMandatoryFeatures.onClick = function(){
      scopeObj.suspendFeatureHideShow("mandatory_feature_screen");
    };
    this.view.btnAdditionalFeatures.onClick = function(){
      scopeObj.suspendFeatureHideShow("additional_feature_screen");
    };
    this.view.lblMandatoryFeatures.onTouchStart = function(){
      scopeObj.suspendFeatureEditHideShow("mandatory_feature_screen");
    };
    this.view.lblAdditionalFeatures.onTouchStart = function(){
      scopeObj.suspendFeatureEditHideShow("additional_feature_screen");    
    };
    this.view.commonButtonsEditFeatureSuspend.btnSave.onClick = function(){
      scopeObj.view.flxSuspendEditFeaturePopup.setVisibility(true);
    };
    this.view.btnShowSuspendFeatureInEdit.onClick = function(){
      scopeObj.suspendFeatureEditHideShow("suspend_screen");
    };
    this.view.commonButtonsEditFeatureSuspend.btnCancel.onClick = function(){
      scopeObj.suspendFeatureEditHideShow("additional_feature_screen");    
    };
    this.view.suspenFeatureEditPopup.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxSuspendEditFeaturePopup.setVisibility(false);
    };
    this.view.suspenFeatureEditPopup.btnPopUpDelete.onClick = function(){
      scopeObj.view.flxSuspendEditFeaturePopup.setVisibility(false);
      scopeObj.changeFeatureStatusLocaly();
      scopeObj.suspendFeatureEditHideShow("additional_feature_screen");      
    };
    this.view.backToEditFeautres.onTouchStart = function(){
      scopeObj.suspendFeatureEditHideShow("additional_feature_screen");    
    };
    this.view.subHeader.tbxSearchBox.onKeyUp = function(){
      scopeObj.featureSearch();
    };
    this.view.mandatoryFeatureHeader.tbxSearchBox.onKeyUp = function(){
      scopeObj.mandatoryfeatureSearch();
    };
    this.view.subHeader.flxClearSearchImage.onTouchStart = function(){
       scopeObj.view.subHeader.tbxSearchBox.text = "";
       scopeObj.view.subHeader.flxClearSearchImage.setVisibility(false);
       scopeObj.view.segFeatures.setData(scopeObj.currentSegFeatureData.filter(scopeObj.searchFilter));
    };
    this.view.mandatoryFeatureHeader.flxClearSearchImage.onTouchStart = function(){
       scopeObj.view.mandatoryFeatureHeader.tbxSearchBox.text = "";
       scopeObj.view.mandatoryFeatureHeader.flxClearSearchImage.setVisibility(false);
       scopeObj.view.segmandatoryFeatures.setData(scopeObj.currentMandatorySegFeatureData.filter(scopeObj.searchFilter));
    };
    this.view.commonButtonsFeatureSuspend.btnSave.onClick = function(){
      scopeObj.view.flxSuspendFeaturePopup.setVisibility(true);
    };
    this.view.suspendFeaturePopup.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxSuspendFeaturePopup.setVisibility(false);
    };
    this.view.suspendFeaturePopup.btnPopUpDelete.onClick = function(){
      scopeObj.view.flxSuspendFeaturePopup.setVisibility(false);
      scopeObj.suspendFeatureRequest();
    };
    this.view.btnShowSuspendFeature.onClick = function(){
      scopeObj.suspendFeatureHideShow("suspend_screen");
    };
    this.view.backToFeautresList.btnBack.onClick = function(){
      scopeObj.suspendFeatureHideShow("additional_feature_screen");
    };
    this.view.commonButtonsFeatureSuspend.btnCancel.onClick = function(){
      scopeObj.suspendFeatureHideShow("additional_feature_screen");
    };
    this.view.lblSelectionOptions.onTouchStart = function(){
      scopeObj.selectUnselectAllFeatures();
    };
    this.view.transactionHistorySearch.flxDownload.onClick = function () {
      scopeObj.downloadTransactionsAsCSV();
    };
    this.view.addAndRemoveAccounts.segSearchType.onRowClick = function(){
      scopeObj.dispalySelectedType();
    };
    this.view.popUp.btnPopUpCancel.onClick = function(){
      scopeObj.view.flxUnlinkAccount.setVisibility(false);
    };
    this.view.popUp.flxPopUpClose.onTouchEnd = function(){
      scopeObj.view.flxUnlinkAccount.setVisibility(false);
    };
    this.view.popUp.btnPopUpDelete.onClick = function(){
      scopeObj.accountUnlink();
    };
    this.view.addAndRemoveAccounts.flxSearchClick.onClick = function(){
      var searchText = scopeObj.view.addAndRemoveAccounts.tbxFilterSearch.text;
      if(searchText){
        var payload = {
        };
        var filter = "";
        if(scopeObj.isAccountCentricConfig === true){
          filter = "Account Id";
        } else{
          filter = "Membership id";
        }
        switch(filter){
          case 'Membership id': payload.Membership_id = searchText;
            break;
          case 'Account Id': payload.Account_id = searchText;
            break;
          default: payload.Account_id = searchText;
        }
        scopeObj.showAccountSearchLoading();
        scopeObj.presenter.getAllAccounts(payload);
      }
    };
    this.view.backToAccounts.flxBack.onTouchStart = function(){
      scopeObj.view.flxCompanyAccountsDetail.setVisibility(false);

// commented as we are not using flxAccountSegment flex for viewing contracts
//       scopeObj.view.flxAccountSegment.setVisibility(true);
      scopeObj.view.forceLayout();     
    };
    this.view.backToAccounts.btnBack.onClick = function(){
      scopeObj.view.flxCompanyAccountsDetail.setVisibility(false);
// commented as we are not using flxAccountSegment flex for viewing contracts
//       scopeObj.view.flxAccountSegment.setVisibility(true);
      scopeObj.view.forceLayout();
    };
    this.view.segCompanyDetailAccount.onRowClick = function(){
      scopeObj.view.flxAccountSegment.setVisibility(false);
      scopeObj.view.flxCompanyAccountsDetail.setVisibility(true);
      scopeObj.view.btnTransactionHistory.onClick();
      var acctNo = scopeObj.view.segCompanyDetailAccount.selecteditems[0].lblAccountNumber.text;
      var endDate = scopeObj.getTransactionDateForServiceCall(new Date());
      var startDate = scopeObj.getTransactionDateCustom(7, kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DAYS"));
      scopeObj.setDataForProductDetailsScreen(scopeObj.presenter.getCustomerAccountInfo(acctNo));    
      scopeObj.presenter.getAccountTransactions({
        "AccountNumber": acctNo,
        "StartDate": startDate,
        "EndDate": endDate
      });
      var from = startDate.split(" ");
      var to = endDate.split(" ");
      scopeObj.view.customCalCreatedDate.resetData = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Last_7_days");
      scopeObj.view.customCalCreatedDate.rangeType = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Last_7_days");
      var sDate = scopeObj.calWidgetFormatDate(from[0]);
      var eDate = scopeObj.calWidgetFormatDate(to[0]);
      scopeObj.view.customCalCreatedDate.value = sDate+ " - "+eDate;
      scopeObj.tabUtilLabelFunction([ scopeObj.view.lblRecent,
                                     scopeObj.view.lblPending,scopeObj.view.lblScheduled],scopeObj.view.lblRecent);
    };
    this.view.flxRecent.onTouchEnd = function(){
      scopeObj.tabUtilLabelFunction([ scopeObj.view.lblRecent,
                                     scopeObj.view.lblPending,scopeObj.view.lblScheduled],scopeObj.view.lblRecent);
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful"));
      kony.adminConsole.utils.hideProgressBar(scopeObj.view);
    };
    this.view.flxPending.onTouchEnd = function(){
      scopeObj.tabUtilLabelFunction([ scopeObj.view.lblRecent,
                                     scopeObj.view.lblPending,scopeObj.view.lblScheduled],scopeObj.view.lblPending);
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending"));
      kony.adminConsole.utils.hideProgressBar(scopeObj.view);
    };
    this.view.flxScheduled.onTouchEnd = function(){
      scopeObj.tabUtilLabelFunction([ scopeObj.view.lblRecent,
                                     scopeObj.view.lblPending,scopeObj.view.lblScheduled],scopeObj.view.lblScheduled);
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.setDataForProductTransactionSegment(kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled"));
      kony.adminConsole.utils.hideProgressBar(scopeObj.view);
    };
    this.view.btnTransactionHistory.onClick = function(){
      scopeObj.view.flxTransactionContainer.setVisibility(true);
      scopeObj.view.flxProductDetailContainer.setVisibility(false);
      scopeObj.changeSubTabSkin([scopeObj.view.btnTransactionHistory,scopeObj.view.btnProductsDetails], scopeObj.view.btnTransactionHistory);
    };
    this.view.btnProductsDetails.onClick = function(){
      scopeObj.view.flxTransactionContainer.setVisibility(false);
      scopeObj.view.flxProductDetailContainer.setVisibility(true);
      scopeObj.changeSubTabSkin([scopeObj.view.btnTransactionHistory,scopeObj.view.btnProductsDetails], scopeObj.view.btnProductsDetails);
    };
    this.view.btnCreateCustomer.onClick = function(){
      if(!(scopeObj.inAuthorizedSignatoriesUi===true && scopeObj.view.btnCreateCustomer.isVisible && scopeObj.view.btnCreateCustomer.skin === "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20pxOp50" )){
      var companyContext = scopeObj.completeCompanyDetails.CompanyContext[0];
      var context = {
        companyID : companyContext.id,
        companyName : companyContext.Name,
        membershipID : companyContext.MembershipId,
        TIN : companyContext.Tin,
        type : companyContext.TypeId,
        inAuthorizedSignatoriesUi : scopeObj.inAuthorizedSignatoriesUi,
        isAccountCentricConfig : scopeObj.isAccountCentricConfig,
        businessTypeId : companyContext.businessTypeId
      };
      scopeObj.presenter.navigateToCreateCustomerScreen(context);
      }
    };
    this.view.btnAddCustomer.onClick = function(){
      scopeObj.view.btnCreateCustomer.onClick();
    };
    this.view.flxViewTab1.onTouchEnd = function(){
      scopeObj.showAccounts();
    };
    this.view.flxViewTab2.onTouchEnd = function(){
      scopeObj.showFeatures();
    };
    this.view.flxViewTab3.onTouchEnd = function(){
      scopeObj.showLimits();
    };
    this.view.flxViewTab4.onTouchEnd = function(){
//       scopeObj.presenter.geMaxAuthSignatories({"id":scopeObj.completeCompanyDetails.CompanyContext[0].businessTypeId});
//       scopeObj.inAuthorizedSignatoriesUi = true;
      // scopeObj.showCustomers();
      scopeObj.showSignatories();
    };
    this.view.flxViewTab5.onTouchEnd = function(){
      scopeObj.tabUtilLabelFunction([ scopeObj.view.lblTabName1,scopeObj.view.lblTabName2,scopeObj.view.lblTabName3,
                                     scopeObj.view.lblTabName4,scopeObj.view.lblTabName5],scopeObj.view.lblTabName5);
      scopeObj.hideAllTabsDetails();
      scopeObj.showApprovalMatrix();
    };
    this.view.flxViewTab6.onTouchEnd = function(){
      scopeObj.showSignatoryGroups();
    };
    this.view.tbxSearch.onKeyUp = function(){
      scopeObj.getGoogleSuggestion(scopeObj.view.tbxSearch.text);
    };
    
    const searchInternalUsers1 = function() {
      scopeObj.view.flxBackground.onClick();
    };
    const debounce = function(func, delay) {
        var self = this;
        let timer;
        return function() {
            let context = self,
                args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function() {
                func.apply(context, args);
            }, delay);
        };
    };
    const searchInternalUsersCall = debounce(searchInternalUsers1, 300);

    this.view.tbxTransactionSearch.onKeyUp = function(){
      if(scopeObj.view.tbxTransactionSearch.text === ""){
        scopeObj.view.flxClearTransactionSearch.onClick();
      }else{
        scopeObj.view.flxClearTransactionSearch.setVisibility(true);
      }
      searchInternalUsersCall();
      scopeObj.view.forceLayout();
    };
    this.view.contractDetailsPopup.flxPopUpClose.onClick = function(){
      scopeObj.view.flxContractDetailsPopup.setVisibility(false);
      if(scopeObj.view.contractDetailsPopup.flxBackOption.isVisible)
      	scopeObj.view.flxCustomerSearchPopUp.setVisibility(true);
    };
    this.view.flxClearTransactionSearch.onClick = function(){
      scopeObj.view.tbxTransactionSearch.text = "";
      scopeObj.view.flxClearTransactionSearch.setVisibility(false);

      // if search performed than we reset the data
      // searching in the contracts
      if(scopeObj.currViewContractsTab == 'Accounts'){
        scopeObj.createDynamicFlexForContract(scopeObj.completeContractDetails.contractCustomers , 'Accounts');
      }else if(scopeObj.currViewContractsTab== 'Features'){
        scopeObj.createDynamicFlexForContract(scopeObj.completeCompanyDetails.accountsFeatures.features , 'Features');
      }
      else{
        scopeObj.createDynamicFlexForContract(scopeObj.completeCompanyDetails.accountsFeatures.limits , 'Limits');
      }
          
    };
    this.view.flxBackground.onClick = function(){
      // if text is empty we won't perform the search
      if(scopeObj.view.tbxTransactionSearch.text === ""){
            
        return;
      }  
      scopeObj.isSearchPerformedViewCont = true;
      
      // reset the flags
      scopeObj.searchResult = {
        isFeatureMatched : false,
        isLimitMatched : false,
        isAcctMatched : false,
        isActionMatched : false
      };

      let searchTxt = scopeObj.view.tbxTransactionSearch.text;
      let filteredData = [];
      // searching in the contracts
      if(scopeObj.currViewContractsTab == 'Accounts'){
        filteredData = scopeObj.filterAccountsDataForSearch(searchTxt);
      }else if(scopeObj.currViewContractsTab== 'Features'){
        filteredData = scopeObj.filterFeaturesDataForSearch(searchTxt);
      }
      else{
        filteredData = scopeObj.filterLimitsDataForSearch(searchTxt);
      }
      scopeObj.createDynamicFlexForContract(filteredData , scopeObj.currViewContractsTab );
      
      // first searched element should be expanded
      if(scopeObj.searchResult.isAcctMatched || scopeObj.searchResult.isFeatureMatched || 
        scopeObj.searchResult.isLimitMatched || scopeObj.searchResult.isActionMatched ){
          let widgets = scopeObj.view.flxContractContainer.widgets();
          if(widgets.length > 0){
            widgets[0].flxArrow.onClick();
            if(scopeObj.searchResult.isActionMatched ){
              // expand the 1st row in segment

              let segData = widgets[0].segAccountFeatures.data;
              if (segData.length > 0 && segData[0][0] && segData[0][0].flxArrow) {
                segData[0][0].flxArrow.onClick();
              }  
            }
          }
      }  
      scopeObj.view.forceLayout();
    };
    this.view.companyContactNumber.txtContactNumber.onKeyUp = function(){
      scopeObj.view.companyContactNumber.hideErrorMsg(2);
    };
    this.view.companyContactNumber.txtContactNumber.onTouchStart = function(){
      scopeObj.AdminConsoleCommonUtils.restrictTextFieldToPhoneNumber('frmCompanies_companyContactNumber_txtContactNumber');
    };
    this.view.companyContactNumber.txtISDCode.onKeyUp = function(){
      scopeObj.view.companyContactNumber.hideErrorMsg(1);
      if(scopeObj.view.companyContactNumber.txtContactNumber.text === ""){
        scopeObj.view.companyContactNumber.hideErrorMsg(2);
      }
    };
    this.view.companyContactNumber.txtISDCode.onTouchStart = function(){
      scopeObj.AdminConsoleCommonUtils.restrictTextFieldToISDCode('frmCompanies_companyContactNumber_txtISDCode');
    };
    this.view.companyContactNumber.txtISDCode.onEndEditing = function(){
      scopeObj.view.companyContactNumber.txtISDCode.text = scopeObj.view.companyContactNumber.addingPlus(scopeObj.view.companyContactNumber.txtISDCode.text);
    };
    this.view.companyDetailsEntry32.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.companyDetailsEntry32.flxEnterValue, scopeObj.view.companyDetailsEntry32.flxInlineError,3);
    };
    this.view.companyDetailsEntry72.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.companyDetailsEntry72.flxEnterValue, scopeObj.view.companyDetailsEntry72.flxInlineError,3);
    };               
    this.view.companyDetailsEntry11.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.companyDetailsEntry11.flxEnterValue, scopeObj.view.companyDetailsEntry11.flxInlineError,3);
    };
    this.view.lstBoxCompanyDetails12.onSelection = function(){
      if(scopeObj.view.lstBoxCompanyDetails12.selectedKey !== "select"){
        scopeObj.clearValidation(scopeObj.view.lstBoxCompanyDetails12, scopeObj.view.flxComapnyDetailsEntryError12,2);
      }
    };
    this.view.companyDetailsEntry21.tbxEnterValue.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.companyDetailsEntry21.flxEnterValue, scopeObj.view.companyDetailsEntry21.flxInlineError,3);
    };      
    this.view.typeHeadCountry.tbxSearchKey.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.typeHeadCountry.tbxSearchKey, scopeObj.view.flxNoCountry, 1);
      scopeObj.view.typeHeadCountry.tbxSearchKey.info.isValid = false;
      scopeObj.hideAddressSegments(scopeObj.view.typeHeadCountry);
      scopeObj.view.flxCountry.zIndex = 2;
      scopeObj.searchForAddress(scopeObj.view.typeHeadCountry.tbxSearchKey, scopeObj.view.typeHeadCountry.segSearchResult, scopeObj.view.typeHeadCountry.flxNoResultFound, 1);
      if(scopeObj.view.typeHeadCountry.tbxSearchKey.text === ""){
        scopeObj.view.typeHeadState.tbxSearchKey.setEnabled(false);
        scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(false);
      }else{
        scopeObj.view.typeHeadState.tbxSearchKey.setEnabled(true);
      }
      scopeObj.view.typeHeadState.tbxSearchKey.text = "";
      scopeObj.view.typeHeadCity.tbxSearchKey.text = "";
      scopeObj.view.forceLayout();
    };
    this.view.typeHeadState.tbxSearchKey.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.typeHeadState.tbxSearchKey, scopeObj.view.flxNoState, 1);
      scopeObj.view.typeHeadState.tbxSearchKey.info.isValid = false;
      if(scopeObj.view.typeHeadState.tbxSearchKey.text === ""){
        scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(false);
      }else{
        scopeObj.view.typeHeadCity.tbxSearchKey.setEnabled(true);
      }
      scopeObj.hideAddressSegments(scopeObj.view.typeHeadState);
      scopeObj.searchForAddress(scopeObj.view.typeHeadState.tbxSearchKey, scopeObj.view.typeHeadState.segSearchResult, scopeObj.view.typeHeadState.flxNoResultFound, 2);
      scopeObj.view.typeHeadCity.tbxSearchKey.text = "";
      scopeObj.view.forceLayout();
    };
    this.view.typeHeadCountry.tbxSearchKey.onEndEditing = function(){
      if (scopeObj.view.typeHeadCountry.flxNoResultFound.isVisible) {
        scopeObj.view.typeHeadCountry.flxNoResultFound.setVisibility(false);
      }
    };
    this.view.typeHeadState.tbxSearchKey.onEndEditing = function(){
      if (scopeObj.view.typeHeadState.flxNoResultFound.isVisible) {
        scopeObj.view.typeHeadState.flxNoResultFound.setVisibility(false);
      }
    };
    this.view.typeHeadCountry.segSearchResult.onRowClick = function(){
      scopeObj.assingText(scopeObj.view.typeHeadCountry.segSearchResult, scopeObj.view.typeHeadCountry.tbxSearchKey);
      scopeObj.clearValidation(scopeObj.view.typeHeadCountry.tbxSearchKey,scopeObj.view.flxNoCountry,1);
    };
    this.view.typeHeadState.segSearchResult.onRowClick = function(){
      scopeObj.assingText(scopeObj.view.typeHeadState.segSearchResult, scopeObj.view.typeHeadState.tbxSearchKey);
      scopeObj.clearValidation(scopeObj.view.typeHeadState.tbxSearchKey,scopeObj.view.flxNoState,1);
    };
    this.view.companyContactNumber.txtISDCode.onEndEditing = function(){
      scopeObj.view.companyContactNumber.txtISDCode.text = scopeObj.view.companyContactNumber.addingPlus(scopeObj.view.companyContactNumber.txtISDCode.text);
    };
    this.view.textBoxOwnerDetailsEntry31.txtISDCode.onEndEditing = function(){
      scopeObj.view.textBoxOwnerDetailsEntry31.txtISDCode.text = 
        scopeObj.view.textBoxOwnerDetailsEntry31.addingPlus(scopeObj.view.textBoxOwnerDetailsEntry31.txtISDCode.text);
    };
    this.view.segSearch.onRowClick = function(){
      scopeObj.mappingRowToWidgets();
    };
    this.view.commonButtonsDetails.btnNext.onClick = function(){
      scopeObj.featureTabandDetailsNextHandler();
    };
    this.view.commonButtonsDetails.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showSearchScreen("detailsCancel");
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.verticalTabsCompany.btnOption1.onClick = function(){
      scopeObj.showAddAccountsScreen();
    };
    this.view.verticalTabsCompany.btnOption2.onClick = function(){
      scopeObj.createCompanyAccountsNextClickHandler();
    };
    this.view.verticalTabsCompany.btnOption3.onClick = function(){
      scopeObj.featureTabandDetailsNextHandler();
    };
    this.view.verticalTabsCompany.btnOption4.onClick = function(){
	  scopeObj.limitTabandFeatureNextHandler();
    };
    this.view.verticalTabsCompany.btnOption5.onClick = function(){
      scopeObj.ownerTabLimitnextHandler();
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onKeyUp = function(){
      var searchText = scopeObj.view.addAndRemoveAccounts.tbxSearchBox.text;
      if(searchText){
        var accounts = scopeObj.microBusinessBankingAccounts.filter(function(account){
          return account.Account_id.indexOf(searchText)>=0;
        });
        scopeObj.accountSearch(accounts);
      }else{
        scopeObj.accountSearch(scopeObj.microBusinessBankingAccounts);
      }
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onTouchStart = function(){
      scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "slFbox0ebc847fa67a243Search";
    };
    this.view.addAndRemoveAccounts.tbxSearchBox.onEndEditing = function(){
      scopeObj.view.addAndRemoveAccounts.flxSearchContainer.skin = "sknflxd5d9ddop100";
    };
    this.view.addAndRemoveAccounts.tbxFilterSearch.onTouchStart = function(){
      if(scopeObj.view.addAndRemoveAccounts.flxSegSearchType.isVisible){
        scopeObj.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
      }
    };
    this.view.commonButtonsAccounts.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showSearchScreen("accountsCancel");
        scopeObj.view.flxAccountError.setVisibility(false);
        scopeObj.view.flxAccountsAddAndRemove.top = "0dp";
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.commonButtonsOwnerDetails.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showSearchScreen("ownerDetailCancel");
        scopeObj.view.flxAccountError.setVisibility(false);
        scopeObj.view.flxAccountsAddAndRemove.top = "0dp";
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.commonButtonsAccounts.btnNext.onClick = function(){
      scopeObj.createCompanyAccountsNextClickHandler();
    };
    this.view.commonButtonsFeaturesTab.btnSave.onClick = function(){
      var isvalidLimits = scopeObj.assignLimitValidations();
      if(isvalidLimits)
        scopeObj.requestCreateUpdateComapny();
    };
    this.view.commonButtonsFeaturesTab.btnNext.onClick = function(){
      scopeObj.limitTabandFeatureNextHandler();
    };
    this.view.commonButtonsFeaturesTab.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showSearchScreen("FeatureTabCancel");
        scopeObj.view.flxAccountError.setVisibility(false);
        scopeObj.view.flxAccountsAddAndRemove.top = "0dp";
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.commonButtonsLimitTab.btnSave.onClick = function(){
      var validLimits = scopeObj.assignLimitValidations();
      if(validLimits)
        scopeObj.requestCreateUpdateComapny();
    };
    this.view.commonButtonsLimitTab.btnNext.onClick = function(){
      scopeObj.ownerTabLimitnextHandler();
    };
    this.view.commonButtonsLimitTab.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showSearchScreen("LimitTabCancel");
        scopeObj.view.flxAccountError.setVisibility(false);
        scopeObj.view.flxAccountsAddAndRemove.top = "0dp";
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.commonButtonsOwnerDetails.btnSave.onClick = function (){
      var isValid = scopeObj.validateOwnerDetailsScreen();
      if(isValid){
        scopeObj.requestCreateUpdateComapny();
      }
    };
    this.view.addAndRemoveAccounts.btnRemoveAll.onClick = function(){
      scopeObj.resetAccounts();
    };
    this.view.tbxName.onKeyUp = function() {
      scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
      scopeObj.view.flxNameError.setVisibility(false);
    };
    this.view.tbxEmailId.onKeyUp = function() {
      scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
      scopeObj.view.flxEmailIdError.setVisibility(false);
    };
    this.view.btnSearch.onClick = function(){
      var validationOfSearch = scopeObj.validateInputFields();
      if(validationOfSearch) {
        scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.flxEmailIdError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.companiesSearch();
      }
    };
    this.view.tbxName.onDone = function(){
      var validationOfSearch = scopeObj.validateInputFields();
      if(validationOfSearch) {
        scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.flxEmailIdError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.companiesSearch();
      }      
    };
    this.view.tbxEmailId.onDone = function(){
      var validationOfSearch = scopeObj.validateInputFields();
      if(validationOfSearch) {
        scopeObj.view.tbxName.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.tbxEmailId.skin = "sknTbxFFFFFFBorDEDEDE13pxKA";
        scopeObj.view.flxEmailIdError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.view.flxNameError.setVisibility(false);
        scopeObj.companiesSearch();
      }            
    };
    this.view.btnAddOldCompany.onClick = function() {
      scopeObj.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.COMPANIES");
      scopeObj.view.breadcrumbs.lblCurrentScreen.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CreateCompany");
      scopeObj.action = scopeObj.actionConfig.create;
      scopeObj.hideRequiredMainScreens(scopeObj.view.flxCreateCompany);
      scopeObj.view.addAndRemoveAccounts.segSelectedOptions.info = {"segData":[]};
      scopeObj.view.typeHeadCity.tbxSearchKey.info = {"isValid":false,"data":""};
      scopeObj.view.typeHeadCountry.tbxSearchKey.info = {"isValid":false,"data":""};
      scopeObj.view.typeHeadState.tbxSearchKey.info = {"isValid":false,"data":""};
      scopeObj.showAddAccountsScreen();
      scopeObj.view.mainHeader.btnAddNewOption.setVisibility(false);
      scopeObj.view.mainHeader.btnDropdownList.setVisibility(false);
      scopeObj.view.flxSettings.setVisibility(false);
      scopeObj.clearDataForOwnerDetails();
      scopeObj.clearDataForCreateCompanyDetails();      
      scopeObj.view.commonButtonsFeaturesTab.btnSave.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CreateCompany");
      scopeObj.view.commonButtonsLimitTab.btnSave.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CreateCompany");
      scopeObj.view.flxAccountError.setVisibility(false);
      scopeObj.view.flxAccountsAddAndRemove.top = "0px";
      scopeObj.changeUIAccessBasedOnEditCreateCompanyFlow();
      scopeObj.view.tbxSearch.text = "";
      scopeObj.visitedFeatureOnceWhileCreate = false;
      scopeObj.view.segFeatures.setData([]);
      scopeObj.view.segAssignLimits.setData([]);
      scopeObj.view.segSearch.setVisibility(false);
      scopeObj.getAddressSegmentData();
      scopeObj.dataForSearch = false;
      scopeObj.dataForMandatorySearch = false;
      scopeObj.view.segEditSuspendedFeature.setData([]);
      scopeObj.completeCompanyDetails = {};
      scopeObj.view.settingsMenu.setVisibility(false);
    };
    this.view.mainHeader.btnAddNewOption.onClick = function() {
      scopeObj.view.breadcrumbs.btnBackToMain.text = "CONTRACTS"
      scopeObj.view.breadcrumbs.lblCurrentScreen.text = "CREATE CONTRACT";
      scopeObj.view.tbxRecordsSearch.text="";
      scopeObj.selectedServiceCard= null;
      scopeObj.action = scopeObj.actionConfig.create;
      scopeObj.limitsActualJSON={};
      scopeObj.updateButtonsText(false);
      scopeObj.fetchCustomerStatusList();
      scopeObj.hideRequiredMainScreens(scopeObj.view.flxCreateContract);
      scopeObj.view.flxBreadcrumb.setVisibility(true);
      scopeObj.setContractButtonsSkin(false);
      scopeObj.enableAllTabs(false);
      scopeObj.presenter.getServiceDefinitionsForContracts({});
      scopeObj.view.segAddedCustomers.setVisibility(false);
      scopeObj.view.btnSelectCustomers.setVisibility(false);
      scopeObj.view.typeHeadContractCountry.tbxSearchKey.info = {"isValid":false,"data":""};
      scopeObj.view.lblContractCustomersHeader.text="Select Customers";
      scopeObj.view.flxNoCustomersAdded.setVisibility(true);
      scopeObj.showContractServiceScreen();
      scopeObj.monetaryLimits={};
      scopeObj.createContractRequestParam={
      "contractName": "",
      "serviceDefinitionName": "",
      "serviceDefinitionId": "",
      "faxId": "",
      "communication": [],
      "address": [],
      "contractCustomers": []
      };
      scopeObj.view.mainHeader.btnAddNewOption.setVisibility(false);
      scopeObj.view.mainHeader.btnDropdownList.setVisibility(false);
      scopeObj.view.flxSettings.setVisibility(false);
      scopeObj.view.settingsMenu.setVisibility(false);
      scopeObj.getCountrySegmentData();
      scopeObj.view.forceLayout();
    };
    this.view.breadcrumbs.btnBackToMain.onClick = function() {
      if(scopeObj.view.breadcrumbs.btnBackToMain.text === kony.i18n.getLocalizedString("i18n.frmCompanies.SearchContract") ||
         scopeObj.view.breadcrumbs.btnBackToMain.text === "CONTRACTS"){
        scopeObj.showSearchScreen("breadCrumb");
      }else{
        scopeObj.backToCompanyDetails(1);
      }
    };
    this.view.breadcrumbs.btnPreviousPage.onClick = function(){
      scopeObj.backToCompanyDetails(1);
    };
    this.view.segSearchResults.onRowClick = function(){
      scopeObj.onSerchSegRowClick();
    };
    this.view.flxCompanyContactHeader.onClick = function(){
      var check = scopeObj.view.flxCompanyContactDetails.isVisible;
      if(scopeObj.action === scopeObj.actionConfig.create)
        scopeObj.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
      scopeObj.view.flxCompanyContactDetails.setVisibility(!check);
      scopeObj.view.lblArrowIcon.text= !check?"\ue915":"\ue922";
      scopeObj.view.lblArrowIcon.skin = "sknfontIconDescRightArrow14px";
      scopeObj.view.forceLayout();
    };
//     this.view.btnCompanyDetailEdit.onClick = function(){
//       scopeObj.action = scopeObj.actionConfig.edit;
//       scopeObj.view.typeHeadCity.tbxSearchKey.info = {"isValid":false,"data":""};
//       scopeObj.view.typeHeadCountry.tbxSearchKey.info = {"isValid":false,"data":""};
//       scopeObj.view.typeHeadState.tbxSearchKey.info = {"isValid":false,"data":""};
//       scopeObj.featureFilter();
//       scopeObj.getAddressSegmentData();
//       scopeObj.view.segEditSuspendedFeature.setData([]);
//       scopeObj.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All");
//       scopeObj.view.commonButtonsFeaturesTab.btnSave.text = kony.i18n.getLocalizedString("i18n.frmCompanies.UpdateCompany_UC");
//       scopeObj.view.commonButtonsLimitTab.btnSave.text = kony.i18n.getLocalizedString("i18n.frmCompanies.UpdateCompany_UC");
//       scopeObj.showEditCompany(scopeObj.completeCompanyDetails);
//       scopeObj.changeUIAccessBasedOnEditCreateCompanyFlow();
//     };
    this.view.btnCompanyDetailEdit.onClick = function(){
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.action = scopeObj.actionConfig.edit;
      scopeObj.view.breadcrumbs.btnBackToMain.text = "CONTRACTS"
      scopeObj.view.breadcrumbs.lblCurrentScreen.text = "EDIT";
      scopeObj.view.tbxRecordsSearch.text="";
      scopeObj.selectedServiceCard= null;
      scopeObj.limitsActualJSON={};
      scopeObj.createContractRequestParam={
        "contractId":scopeObj.completeContractDetails.id,
        "contractName": scopeObj.completeContractDetails.name,
        "serviceDefinitionName": scopeObj.completeContractDetails.servicedefinitionName,
        "serviceDefinitionId": scopeObj.completeContractDetails.servicedefinitionId,
        "faxId": scopeObj.completeContractDetails.faxId,
        "communication": scopeObj.completeContractDetails.communication,
        "address": scopeObj.completeContractDetails.address,
        "contractCustomers": []
      };
      scopeObj.presenter.getServiceDefinitionMonetaryActions({"serviceDefinitionId":scopeObj.completeContractDetails.servicedefinitionId});
      if(scopeObj.view.AdvancedSearchDropDown01.sgmentData.info===undefined||scopeObj.view.AdvancedSearchDropDown01.sgmentData.info===null)
      	scopeObj.fetchCustomerStatusList();
      scopeObj.updateButtonsText(true);
      scopeObj.view.flxClearRecordsSearch.setVisibility(false);
      scopeObj.hideRequiredMainScreens(scopeObj.view.flxCreateContract);
      scopeObj.view.flxBreadcrumb.setVisibility(true);
      scopeObj.setContractButtonsSkin(true);
      scopeObj.enableAllTabs(true);
      scopeObj.view.segAddedCustomers.setVisibility(true);
      scopeObj.view.btnSelectCustomers.setVisibility(true);
      scopeObj.view.flxNoCustomersAdded.setVisibility(false);
      scopeObj.showContractServiceScreen();
      scopeObj.setEditContractCustomersData();
      if(scopeObj.viewContractServiceDef.length===0)
      	scopeObj.presenter.getServiceDefinitionsForContracts({});
      else{
        scopeObj.setContractServiceCards(scopeObj.viewContractServiceDef);
        scopeObj.setDataToServiceTypeFilter(scopeObj.viewContractServiceDef);
        scopeObj.view.flxContractServiceCards.info={"totalRecords":scopeObj.viewContractServiceDef,"filteredRecords":scopeObj.viewContractServiceDef};
      }
      scopeObj.getCountrySegmentData();
      scopeObj.view.forceLayout();
    };
    this.view.btnApply.onClick = function(){
      scopeObj.getTransactionsBasedOnDate();
    };
    //search on transaction history
    this.view.transactionHistorySearch.tbxSearchBox.onBeginEditing = function () {
      scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflx0cc44f028949b4cradius30px";
    };
    this.view.transactionHistorySearch.tbxSearchBox.onEndEditing = function () {
      scopeObj.view.transactionHistorySearch.flxSearchContainer.skin = "sknflxBgffffffBorderc1c9ceRadius30px";
    };
    this.view.transactionHistorySearch.tbxSearchBox.onKeyUp = function () {
      scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(true);
      var searchParameters = [{
        "searchKey": "lblRefNo",
        "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
      },
                              {
                                "searchKey": "lblDateAndTime",
                                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
                              },
                              {
                                "searchKey": "lblTransctionDescription",
                                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
                              },
                              {
                                "searchKey": "lblType",
                                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
                              },
                              {
                                "searchKey": "lblAmount",
                                "searchValue": scopeObj.view.transactionHistorySearch.tbxSearchBox.text
                              }

                             ];
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
      scopeObj.search(scopeObj.view.segTransactionHistory, searchParameters, scopeObj.view.rtxMsgTransctions,
                      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);

    };
    this.view.transactionHistorySearch.flxClearSearchImage.onClick = function () {
      scopeObj.view.transactionHistorySearch.tbxSearchBox.text = "";
      scopeObj.view.transactionHistorySearch.flxClearSearchImage.setVisibility(false);
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      var listOfWidgetsToHide = [scopeObj.view.flxTransactionHistorySegmentHeader, scopeObj.view.flxSeperator4];
      scopeObj.clearSearch(scopeObj.view.segTransactionHistory, scopeObj.view.rtxMsgTransctions,
                           kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.TOEND"), scopeObj.recordsSize, toAdd, listOfWidgetsToHide);
    };
    /*Search header*/
    this.view.flxCompanyName.onClick = function () {
      var segData = scopeObj.view.segSearchResults.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblCompanyName.info.value", "searchList");
      scopeObj.view.segSearchResults.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxCompanyId.onClick = function () {
      var segData = scopeObj.view.segSearchResults.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblCompanyId", "searchList");
      scopeObj.view.segSearchResults.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxEmailHeader.onClick = function () {
      var segData = scopeObj.view.segSearchResults.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblEmail", "searchList");
      scopeObj.view.segSearchResults.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxContactHeader.onClick = function(){
      var flxRight = scopeObj.view.flxWrapper.frame.width - scopeObj.view.flxContactHeader.frame.x - scopeObj.view.flxContactHeader.frame.width;
      var iconRight = scopeObj.view.flxContactHeader.frame.width - scopeObj.view.lblIconTypeFilter.frame.x;
      scopeObj.view.flxSearchBusinessTypeFilter.right = (flxRight + iconRight - 8) +"px";
      if(scopeObj.view.flxSearchBusinessTypeFilter.isVisible){
        scopeObj.view.flxSearchBusinessTypeFilter.setVisibility(false);
      } else{
        scopeObj.view.flxSearchBusinessTypeFilter.setVisibility(true);
      }
    };
    /*Accounts header*/
    this.view.flxStatus.onClick = function(){
      var flxRight = scopeObj.view.flxAccountsHeader.frame.width - scopeObj.view.flxStatus.frame.x - scopeObj.view.flxStatus.frame.width;
      var iconRight = scopeObj.view.flxStatus.frame.width - scopeObj.view.lblStatusSortIcon.frame.x;
      scopeObj.view.flxAccountStatusFilter.right = (flxRight + iconRight - 28) +"px";
      if(scopeObj.view.flxAccountStatusFilter.isVisible){
        scopeObj.view.flxAccountStatusFilter.setVisibility(false);
      } else{
        scopeObj.view.flxAccountStatusFilter.setVisibility(true);
      }
    };
    /*business user header*/
    this.view.flxCustomerName.onClick = function(){
      var segData = scopeObj.view.segCompanyDetailCustomer.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblName.text", "businessUsers");
      scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxRole.onClick = function(){
      var segData = scopeObj.view.segCompanyDetailCustomer.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblRole.text", "businessUsers");
      scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxUserName.onClick = function(){
      var segData = scopeObj.view.segCompanyDetailCustomer.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblUsername.text", "businessUsers");
      scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxCustomerEmailID.onClick = function(){
      var segData = scopeObj.view.segCompanyDetailCustomer.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblEmail.text", "businessUsers");
      scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    this.view.flxCustomerStatus.onClick = function(){
      var segData = scopeObj.view.segCompanyDetailCustomer.data;
      var sortedData = scopeObj.sortAndGetData(segData, "lblCustomerStatus.text", "businessUsers");
      scopeObj.view.segCompanyDetailCustomer.setData(sortedData);
      scopeObj.view.forceLayout();
    };
    /*transactions sorting*/
    this.view.flxTranasctionRefNo.onClick = function () {
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "flxAmountOriginal" : "",
        "lblAmountOriginalSign" :"", 
        "lblAmountOriginalSymbol" : "",
        "lblAmountOriginal" : "",
        "flxAmountConverted" : "",
        "lblAmountConvertedSign" : "",
        "lblAmountConvertedSymbol" : "",
        "lblAmountConverted" : "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblRefNo", scopeObj.view.fonticonSortTranasctionRefNo, "TOEND", scopeObj.recordsSize, toAdd);
    };
    this.view.flxTransactionDateAndTime.onClick = function () {
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "flxAmountOriginal" : "",
        "lblAmountOriginalSign" :"", 
        "lblAmountOriginalSymbol" : "",
        "lblAmountOriginal" : "",
        "flxAmountConverted" : "",
        "lblAmountConvertedSign" : "",
        "lblAmountConvertedSymbol" : "",
        "lblAmountConverted" : "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblDateAndTime", scopeObj.view.fonticonSortTransactionDateAndTime, "TOEND", scopeObj.recordsSize, toAdd);
    };
    this.view.flxTransactionType.onClick = function () {
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "flxAmountOriginal" : "",
        "lblAmountOriginalSign" :"", 
        "lblAmountOriginalSymbol" : "",
        "lblAmountOriginal" : "",
        "flxAmountConverted" : "",
        "lblAmountConvertedSign" : "",
        "lblAmountConvertedSymbol" : "",
        "lblAmountConverted" : "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblType", scopeObj.view.fonticonSortTransactionType, "TOEND", scopeObj.recordsSize, toAdd);
    };
    this.view.flxTransactionAmountOriginal.onClick = function () {
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "flxAmountOriginal" : "",
        "lblAmountOriginalSign" :"", 
        "lblAmountOriginalSymbol" : "",
        "lblAmountOriginal" : "",
        "flxAmountConverted" : "",
        "lblAmountConvertedSign" : "",
        "lblAmountConvertedSymbol" : "",
        "lblAmountConverted" : "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblAmountOriginal", scopeObj.view.fonticonSortTransactionAmountOriginal, "TOEND", scopeObj.recordsSize, toAdd);
    };
    this.view.flxTransactionAmountConverted.onClick = function () {
      var toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmount": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "flxAmountOriginal" : "",
        "lblAmountOriginalSign" :"", 
        "lblAmountOriginalSymbol" : "",
        "lblAmountOriginal" : "",
        "flxAmountConverted" : "",
        "lblAmountConvertedSign" : "",
        "lblAmountConvertedSymbol" : "",
        "lblAmountConverted" : "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      scopeObj.sortForTransactions(scopeObj.view.segTransactionHistory, "lblAmountConverted", scopeObj.view.fonticonSortTransactionAmountConverted, "TOEND", scopeObj.recordsSize, toAdd);
    };
    this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.onKeyUp = function() {
      scopeObj.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueNormal";
      scopeObj.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(false);
    };
    this.view.lblCreateCompanyLink.onClick = function() {
      scopeObj.view.mainHeader.btnAddNewOption.onClick();
    };
    this.view.customCalOwnerDOB.event = function(){
      scopeObj.view.flxCalendarDOB.skin = "sknflxffffffoptemplateop3px";
      scopeObj.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.flxFeatureDetailsClose.onClick = function(){
      scopeObj.view.flxFeatureDetails.setVisibility(false);
    };
    this.view.flxAccount.onClick = function(){
      scopeObj.view.flxhideDisplay.setVisibility(scopeObj.view.flxhideDisplay.isVisible);
    };
    this.view.tabs.btnTab1.onClick = function(){
      scopeObj.view.flxPerTransactionLimitsContent.setVisibility(true);
      scopeObj.view.flxDailyTransactionLimitsContent.setVisibility(false);
      scopeObj.view.flxWeeklyTransactionLimitsContent.setVisibility(false);
      scopeObj.subTabsButtonWithBgUtilFunction([scopeObj.view.tabs.btnTab1,scopeObj.view.tabs.btnTab2,scopeObj.view.tabs.btnTab3],scopeObj.view.tabs.btnTab1);
      scopeObj.showPerTransactionLimitsTab();

    };
    this.view.tabs.btnTab2.onClick = function(){
      scopeObj.view.flxPerTransactionLimitsContent.setVisibility(false);
      scopeObj.view.flxDailyTransactionLimitsContent.setVisibility(true);
      scopeObj.view.flxWeeklyTransactionLimitsContent.setVisibility(false);
      scopeObj.subTabsButtonWithBgUtilFunction([scopeObj.view.tabs.btnTab1,scopeObj.view.tabs.btnTab2,scopeObj.view.tabs.btnTab3],scopeObj.view.tabs.btnTab2);
      scopeObj.showDailyLimitsTab();
    };
    this.view.tabs.btnTab3.onClick = function(){
      scopeObj.view.flxPerTransactionLimitsContent.setVisibility(false);
      scopeObj.view.flxDailyTransactionLimitsContent.setVisibility(false);
      scopeObj.view.flxWeeklyTransactionLimitsContent.setVisibility(true);
      scopeObj.subTabsButtonWithBgUtilFunction([scopeObj.view.tabs.btnTab1,scopeObj.view.tabs.btnTab2,scopeObj.view.tabs.btnTab3],scopeObj.view.tabs.btnTab3);
      scopeObj.showWeeklyTransactionLimitsTab();
    };
    this.view.segCompanyAccounts.onRowClick = function(){
      scopeObj.onAccountNumSelectionApprovals(false);
      scopeObj.view.forceLayout();
    };
    this.view.btnAccountType.onClick = function(){
      if(scopeObj.view.flxhideDisplay.isVisible)
        scopeObj.toggleApprovalsAccountsTab(false);
      else
        scopeObj.toggleApprovalsAccountsTab(true);
    };
    this.view.flxSettings.onClick = function(){
      scopeObj.view.settingsMenu.setVisibility(!scopeObj.view.settingsMenu.isVisible);
    };
    this.view.settingsMenuOptions.flxOption1.onClick = function(){
      scopeObj.presenter.navigateToBusinessTypesUi(); 
      scopeObj.view.settingsMenu.setVisibility(false);
	};
    this.view.settingsMenu.onHover = this.onHoverSettings;
    this.view.flxAuthSignatoriesToolTip.onHover = this.onHoverSettings;
    this.view.btnCreateCustomer.onHover = function(){
      if(scopeObj.view.btnCreateCustomer.skin === "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20pxOp50")
        scopeObj.view.flxAuthSignatoriesToolTip.setVisibility(true);
      scopeObj.view.forceLayout();
    };
    this.view.btnAuthSignatories.onClick = function(){
      scopeObj.inAuthorizedSignatoriesUi=true;
      scopeObj.resetBusinessUsers(true);
    };
    this.view.btnOtherBusinessUsers.onClick = function(){
      scopeObj.inAuthorizedSignatoriesUi=false;
      scopeObj.resetBusinessUsers(false);
    };
    this.view.mainHeader.btnDropdownList.onClick = function(){
      scopeObj.presenter.getAllEnrollmentRequests();
    };
    this.view.accountStatusFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.performAccountsStatusFilter();
    };
    this.view.businessTypeFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.performBusinessTypeFilter();
    };
    this.view.customListboxTaxid.flxSelectedText.onClick = function(){
      scopeObj.view.customListboxTaxid.flxSegmentList.setVisibility(scopeObj.view.customListboxTaxid.flxSegmentList.isVisible === false);
    };
    this.view.lblDetailsMore.onHover = this.onHoverTaxIdMoreText;
    this.view.flxSearchBusinessTypeFilter.onHover = this.onHoverSettings;
    this.view.flxAccountStatusFilter.onHover = this.onHoverSettings;
    this.view.segCompanyDetailCustomer.onHover=this.saveScreenY;
    
    // contracts actions
    this.view.tbxRecordsSearch.onKeyUp = function(){
      if(scopeObj.view.tbxRecordsSearch.text.trim().length!==0){
        scopeObj.view.flxClearRecordsSearch.setVisibility(true);
        scopeObj.searchServiceCards();
      }else{
        scopeObj.view.flxNoServiceSearchResults.setVisibility(false);
        scopeObj.view.flxContractServiceCards.setVisibility(true);
        scopeObj.view.flxClearRecordsSearch.setVisibility(false);
        if(scopeObj.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices.length===scopeObj.view.serviceTypeFilterMenu.segStatusFilterDropdown.data.length)
          scopeObj.setContractServiceCards(scopeObj.view.flxContractServiceCards.info.totalRecords,true);
        else{
          scopeObj.performServiceTypeFilter();
        }
      }      
    };
    this.view.flxClearRecordsSearch.onClick = function(){
      scopeObj.view.flxNoServiceSearchResults.setVisibility(false);
      scopeObj.view.flxContractServiceCards.setVisibility(true);
      scopeObj.view.tbxRecordsSearch.text="";
      scopeObj.setContractServiceCards(scopeObj.view.flxContractServiceCards.info.totalRecords,true);
      scopeObj.view.flxClearRecordsSearch.setVisibility(false);
    };
    this.view.flxServiceFilter.onClick = function(){
      scopeObj.view.flxServiceTypeFilter.setVisibility(!scopeObj.view.flxServiceTypeFilter.isVisible);
    };
    this.view.serviceTypeFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      scopeObj.performServiceTypeFilter();
    };
    this.view.accountTypesFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      var filteredData=scopeObj.performAccountOwnerFilters();
      scopeObj.setAccountsDataCustomers(filteredData);
    };
    this.view.ownershipFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      var filteredData=scopeObj.performAccountOwnerFilters();
      scopeObj.setAccountsDataCustomers(filteredData);
    };
    this.view.tbxAccountsSearch.onKeyUp = function(){
      if(scopeObj.view.tbxAccountsSearch.text.trim().length!==0){
        scopeObj.view.flxClearAccountsSearch.setVisibility(true);
        scopeObj.setCustSelectedData("customersDropdown",true);
      }else{
        scopeObj.view.flxClearAccountsSearch.setVisibility(false);
        scopeObj.setCustSelectedData("customersDropdown",false);
      }      
    };
    this.view.flxClearAccountsSearch.onClick = function(){
      scopeObj.view.tbxAccountsSearch.text="";
      scopeObj.setCustSelectedData("customersDropdown",false);
      scopeObj.view.flxClearAccountsSearch.setVisibility(false);
    };
    //Adding timer for featres search as the features&actions count is large
    const searchContractFeaturesList = function () {
      scopeObj.setCustSelectedData("customersDropdownFA",true);
    };
    const searchContractFeatures = debounce(searchContractFeaturesList,300);
    this.view.tbxContractFASearch.onKeyUp = function(){
      if(scopeObj.view.tbxContractFASearch.text.trim().length!==0){
        scopeObj.view.flxClearContractFASearch.setVisibility(true);
        searchContractFeatures();
      }else{
        scopeObj.view.flxClearContractFASearch.setVisibility(false);
        scopeObj.setCustSelectedData("customersDropdownFA",false);
      }      
    };
    this.view.flxClearContractFASearch.onClick = function(){
      kony.adminConsole.utils.showProgressBar(scopeObj.view);
      scopeObj.view.tbxContractFASearch.text="";
      scopeObj.setCustSelectedData("customersDropdownFA",false);
      scopeObj.view.flxClearContractFASearch.setVisibility(false);
    };
    this.view.tbxContractLimitsSearch.onKeyUp = function(){
      if(scopeObj.view.tbxContractLimitsSearch.text.trim().length!==0){
        scopeObj.view.flxClearContractLimitsSearch.setVisibility(true);
        scopeObj.setCustSelectedData("customersDropdownLimits",true);
      }else{
        scopeObj.view.flxClearContractLimitsSearch.setVisibility(false);
        scopeObj.setCustSelectedData("customersDropdownLimits",false);
      }      
    };
    this.view.flxClearContractLimitsSearch.onClick = function(){
      scopeObj.view.tbxContractLimitsSearch.text="";
      scopeObj.setCustSelectedData("customersDropdownLimits",false);
      scopeObj.view.flxClearContractLimitsSearch.setVisibility(false);
    };
    this.view.commonButtonsServiceDetails.btnSave.onClick = function(){
      scopeObj.showContractCustomersScreen(true);
    };
    this.view.commonButtonsServiceDetails.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractServiceTab);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.btnSearchContractCustomers.onClick = function(){
      scopeObj.showCustomerSearchPopup(true);
    };
    this.view.btnSelectCustomers.onClick = function(){
      scopeObj.showCustomerSearchPopup(false);
    };
    this.view.flxRightImage.onClick = function(){
      scopeObj.revertAddedCustomers();
      scopeObj.view.flxCustomerSearchPopUp.setVisibility(false);
    };
    this.view.commonButtonsCustomers.btnNext.onClick = function(){
      scopeObj.showContractDetailsScreen(true);
    };
    this.view.commonButtonsCustomers.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractCustomersTab);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.commonButtonsCustomers.btnSave.onClick = function(){
      scopeObj.createContract();
    };
    this.view.contractDetailsCommonButtons.btnNext.onClick = function(){
      if(scopeObj.validateContractDetails()){
        scopeObj.showContractAccountsScreen(true);
      }
    };
    this.view.contractDetailsEntry1.tbxEnterValue.onKeyUp = function(){
      if(scopeObj.view.contractDetailsEntry1.flxInlineError.isVisible){
      scopeObj.view.contractDetailsEntry1.tbxEnterValue.skin="sknflxEnterValueNormal";
      scopeObj.view.contractDetailsEntry1.flxInlineError.setVisibility(false);
      }
    };
    this.view.commonButtonsContractAccounts.btnNext.onClick = function(){
      scopeObj.showContractFAScreen(true);
    };
    this.view.commonButtonsContractFA.btnNext.onClick = function(){
      scopeObj.showContractLimitsScreen(true);
    };
    this.view.contractDetailsCommonButtons.btnSave.onClick = function(){
      if(scopeObj.validateContractDetails()){
      scopeObj.createContract();
      }
    };
    this.view.commonButtonsContractAccounts.btnSave.onClick = function(){
      scopeObj.createContract();
    };
    this.view.commonButtonsContractFA.btnSave.onClick = function(){
      scopeObj.createContract();
    };
    this.view.commonButtonsContractLimits.btnSave.onClick = function(){
      if(scopeObj.validateAllLimits())
      	scopeObj.createContract();
    };
    this.view.contractDetailsCommonButtons.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractDetailsTab);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.commonButtonsContractAccounts.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractAccounts);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.commonButtonsContractFA.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractFA);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.commonButtonsContractLimits.btnCancel.onClick = function(){
      if(scopeObj.action === scopeObj.actionConfig.create){
        scopeObj.showContractSearch(scopeObj.view.flxContractLimits);
      }else{
        scopeObj.getCompanyAllDetails(scopeObj.createContractRequestParam.contractId,1);
      }
    };
    this.view.btnSave.onClick = function(){
      scopeObj.searchForContracts();
    };
    this.view.verticalTabsContract.btnOption0.onClick = function(){
      scopeObj.showContractServiceScreen();
    };
    this.view.verticalTabsContract.btnOption1.onClick = function(){
      if(scopeObj.isValidSelection())
      	scopeObj.showContractCustomersScreen(false);
    };
    this.view.verticalTabsContract.btnOption2.onClick = function(){
      if(scopeObj.isValidSelection())
      	scopeObj.showContractDetailsScreen(false);
    };
    this.view.verticalTabsContract.btnOption3.onClick = function(){
      if(scopeObj.isValidSelection())
      	scopeObj.showContractAccountsScreen(false);
    };
    this.view.verticalTabsContract.btnOption4.onClick = function(){
      if(scopeObj.isValidSelection())
      	scopeObj.showContractFAScreen(false);
    };
    this.view.verticalTabsContract.btnOption5.onClick = function(){
      if(scopeObj.isValidSelection())
      	scopeObj.showContractLimitsScreen(false);
    };
    this.view.btnUpdateInBulkFA.onClick = function(){
      scopeObj.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.info="FA";
      scopeObj.showBulkUpdatePopup();
    };
    this.view.btnUpdateInBulkLimits.onClick = function(){
      scopeObj.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.info="Limits";
      scopeObj.showBulkUpdatePopup();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen1.btnSave.onClick = function(){
      scopeObj.showBulkUpdatePopupScreen2();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen1.btnCancel.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopup.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen2.btnSave.onClick = function(){
      if(scopeObj.validateBulkSelection())
      scopeObj.updateFeatureLimitsBulkChanges();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen2.btnCancel.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopup.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesLimitsPopup.flxPopUpClose.onClick = function(){
      scopeObj.view.flxBulkUpdateFeaturesPopup.setVisibility(false);
    };
    this.view.btnShowHideAdvSearch.onClick = function(){
      if(scopeObj.view.flxRow2.isVisible){
        scopeObj.view.btnShowHideAdvSearch.text="Advanced Search";
        scopeObj.view.fonticonrightarrowSearch.text="";//right arrow
        scopeObj.view.flxColumn13.setVisibility(false);
        scopeObj.view.flxRow2.setVisibility(false);
        scopeObj.view.flxRow3.setVisibility(false);
      }else{
        scopeObj.view.btnShowHideAdvSearch.text="Hide Advanced Search";
        scopeObj.view.fonticonrightarrowSearch.text="";//up arrow
        scopeObj.view.flxColumn13.setVisibility(true);
        scopeObj.view.flxRow2.setVisibility(true);
        scopeObj.view.flxRow3.setVisibility(true);
      }
      scopeObj.view.forceLayout();
    };
    this.view.btnSearchCustomers.onClick = function(){
      if(scopeObj.validateCoreCustSearch()){
        scopeObj.setNormalSkinToCoreSearch();
      	scopeObj.searchCoreCustomers();
      }
    };
    this.view.btnClearAll.onClick = function(){
      scopeObj.resetCoreCustomerSearch();
    };
    this.view.btnAdvSearch.onClick = function(){
      if(scopeObj.view.flxSecondRow.isVisible){
        scopeObj.view.btnAdvSearch.text=kony.i18n.getLocalizedString("i18n.Group.AdvancedSearch");
        scopeObj.view.flxContractSpecificParams.setVisibility(false);
        scopeObj.view.flxSecondRow.setVisibility(false);
        scopeObj.view.flxThirdRow.setVisibility(false);
        scopeObj.view.fonticonrightarrow.text="";
      }else{
        scopeObj.view.btnAdvSearch.text="Hide Advanced Search";
        scopeObj.view.flxContractSpecificParams.setVisibility(true);
        scopeObj.view.flxSecondRow.setVisibility(true);
        scopeObj.view.flxThirdRow.setVisibility(true);
        scopeObj.view.fonticonrightarrow.text="";
      }
    };
    this.view.btnReset.onClick = function(){
      scopeObj.clearSearchFeilds();
    };
    this.view.flxSelectedCustomersArrow.onClick = function(){
      if(scopeObj.view.flxSearchFilter.isVisible){
        scopeObj.view.lblIconArrow.text="";
        scopeObj.view.flxSearchFilter.setVisibility(false);
      }else{
      	scopeObj.view.flxSearchFilter.setVisibility(true);
        scopeObj.view.lblIconArrow.text="";
      }
    };
    //customer search popup button actions
    this.view.commonButtons.btnNext.onClick = function(){
      //add tags and back to search page navigation
      scopeObj.view.commonButtons.btnNext.setVisibility(false);
      scopeObj.view.flxCustomerSearchHeader.setVisibility(true);
      scopeObj.view.flxCustomersBreadcrumb.setVisibility(false);
      scopeObj.view.flxCustomerAdvSearch.setVisibility(true);
      scopeObj.view.flxCustomerDetailsContainer.setVisibility(true);
      scopeObj.view.flxSelectedCustomerInfo.setVisibility(false);
      scopeObj.view.lblNoCustomersSearched.text="Search for a Customer to see the results";
      scopeObj.view.flxNoCustomersSearched.setVisibility(true);
      scopeObj.view.lblRelatedcustSubHeading.setVisibility(false);
      scopeObj.view.flxRelatedCustomerSegment.setVisibility(false);
      scopeObj.view.flxSearchFilter.setVisibility(true);
      scopeObj.view.lblIconArrow.text="";
      scopeObj.view.flxSearchBreadcrumb.info.added=[];
      var i=scopeObj.view.flxSearchBreadcrumb.widgets().concat([]);
      scopeObj.view.segBreadcrumbs.setData([]);
      for(var x=2;x<i.length-1;x++)
        scopeObj.view.flxSearchBreadcrumb.remove(i[x]);
      scopeObj.resetCoreCustomerSearch();
    };
    this.view.commonButtons.btnSave.onClick = function(){
      scopeObj.view.flxCustomerSearchPopUp.setVisibility(false);
      scopeObj.view.flxNoCustomersAdded.setVisibility(false);
      scopeObj.view.segAddedCustomers.setVisibility(true);
      if(scopeObj.view.btnSelectCustomers.isVisible){
        scopeObj.setAddedCustDataInRequest();
      }
      else
        scopeObj.view.btnSelectCustomers.setVisibility(true);
      scopeObj.setSelectedCustomersData();
    };
    this.view.commonButtons.btnCancel.onClick = function(){
      scopeObj.revertAddedCustomers();
      scopeObj.view.flxCustomerSearchPopUp.setVisibility(false);
    };
    this.view.bulkUpdateFeaturesLimitsPopup.flxArrow.onClick = function(){
      scopeObj.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.setVisibility(!scopeObj.view.bulkUpdateFeaturesLimitsPopup.isVisible);
    };
    this.view.bulkUpdateFeaturesLimitsPopup.btnModifySearch.onClick = function(){
      scopeObj.showBulkUpdatePopupScreen1(true);
    };
    this.view.txtSearchParam1.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.txtSearchParam2.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.txtSearchParam3.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.txtSearchParam4.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.txtSearchParam5.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.txtSearchParam6.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.textBoxSearchContactNumber.txtContactNumber.onBeginEditing = function(){
      scopeObj.setNormalSkinToAllFeilds();
    };
    this.view.btnBackToMain.onClick = function(){
      scopeObj.view.commonButtons.btnNext.setVisibility(false);
      scopeObj.view.flxCustomerSearchHeader.setVisibility(true);
      scopeObj.view.flxCustomersBreadcrumb.setVisibility(false);
      scopeObj.view.flxAddedCustomerInfo.setVisibility(false);
      scopeObj.view.flxCustomerAdvSearch.setVisibility(true);
      scopeObj.view.btnShowHideAdvSearch.text="Show Advanced Search";
      scopeObj.view.flxRow2.setVisibility(false);
      scopeObj.view.flxRow3.setVisibility(false);
      scopeObj.view.flxColumn13.setVisibility(false);
      scopeObj.view.flxCustomerDetailsContainer.setVisibility(true);
      scopeObj.view.flxSearchFilter.removeAll();
      scopeObj.setCoreCustomersList(scopeObj.view.btnBackToMain.info);
      scopeObj.view.flxSearchBreadcrumb.info.added=[];
      scopeObj.view.segBreadcrumbs.setData([]);
      var i=scopeObj.view.flxSearchBreadcrumb.widgets();
      var addedCust=0;
      for(let x=0;x<i.length;x++){
        if(i[x].id.indexOf("btnBackToMain")>=0)//dynamically created buttons
          addedCust++;
      }
      scopeObj.selectedCustomers.splice(scopeObj.selectedCustomers.length-addedCust,scopeObj.selectedCustomers.length);
      scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnSave,true,false);
      scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnNext,false,false);
    };
    this.view.ContractLimitsList.btnReset.onClick = function(){
      scopeObj.showResetAllLimitsPopup();
    };
    this.view.flxCheckboxCustomerInfo.onClick = function(){
      var custId=scopeObj.view.lblDataInfo11.text;
      var custIndex=0;
      var tags=scopeObj.view.flxSearchFilter.widgets();
      for(var k=0;k<scopeObj.selectedCustomers.length;k++){
        if(scopeObj.selectedCustomers[k].coreCustomerId===custId){
          custIndex=k;
          break;
        }
      }
      if(scopeObj.view.imgCheckbox.src==="checkboxselected.png"){
        scopeObj.view.imgCheckbox.src="checkboxnormal.png";
        scopeObj.selectedCustomers[custIndex].isSelected=false;
        scopeObj.removeTag(scopeObj.view.flxSearchFilter,custId);
        if(tags.length===1){
          scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnSave,true,false);
          scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnNext,false,false);
        }
      }else{
        scopeObj.view.imgCheckbox.src="checkboxselected.png";
        scopeObj.selectedCustomers[custIndex].isSelected=true;
        scopeObj.addCustomerTag(scopeObj.view.flxSearchFilter,scopeObj.selectedCustomers[custIndex].coreCustomerName,scopeObj.selectedCustomers[custIndex].coreCustomerId);
        scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnSave,true,true);
        scopeObj.AdminConsoleCommonUtils.setEnableDisableSkinForButton(scopeObj.view.commonButtons.btnNext,false,true);
      }
    };
    this.view.textBoxEntry11.tbxEnterValue.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry13.tbxEnterValue.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry21.txtContactNumber.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry21.txtISDCode.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry31.tbxEnterValue.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry32.tbxEnterValue.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.textBoxEntry33.tbxEnterValue.onBeginEditing  =function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
    };
    this.view.flxDropDown23.onClick = function(){
      if(scopeObj.view.flxDropDownDetail23.isVisible)
        scopeObj.view.flxDropDownDetail23.setVisibility(false);
      else{
        scopeObj.view.AdvancedSearchDropDown01.flxSearchCancel.onClick();
        scopeObj.view.flxDropDownDetail23.setVisibility(true);
      }
    };
    this.view.flxDropDownServType.onClick = function(){
      if(scopeObj.view.flxDropDownDetailServType.isVisible)
        scopeObj.view.flxDropDownDetailServType.setVisibility(false);
      else{
        scopeObj.view.AdvancedSearchDropDownServType.flxSearchCancel.onClick();
        scopeObj.view.flxDropDownDetailServType.setVisibility(true);
      }
    };
    this.view.AdvancedSearchDropDown01.sgmentData.onRowClick = function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
      var rowInd=scopeObj.view.AdvancedSearchDropDown01.sgmentData.selectedRowIndex[1];
      var segData=scopeObj.view.AdvancedSearchDropDown01.sgmentData.data;
      scopeObj.view.lblSelectedRows.text=segData[rowInd].lblDescription;
      scopeObj.view.flxDropDownDetail23.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.AdvancedSearchDropDownServType.sgmentData.onRowClick = function(){
      if(scopeObj.view.lblCustomerSearchError.isVisible)
      	scopeObj.setNormalSkinToCoreSearch();
      var rowInd=scopeObj.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex[1];
      var segData=scopeObj.view.AdvancedSearchDropDownServType.sgmentData.data;
      scopeObj.view.lblSelectedRowsServType.text=segData[rowInd].lblDescription;
      scopeObj.view.flxDropDownDetailServType.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    
    this.view.customersDropdown.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.customersDropdown.tbxSearchBox.text.trim().length!==0){
        scopeObj.view.customersDropdown.flxClearSearchImage.setVisibility(true);
        scopeObj.searchCustomersDropDownList("customersDropdown");
      }else{
        scopeObj.view.customersDropdown.flxClearSearchImage.setVisibility(false);
        scopeObj.setCustomersDropDownList("customersDropdown");
        scopeObj.view.customersDropdown.flxSegmentList.setVisibility(true);
      }  
    };
    this.view.customersDropdown.flxClearSearchImage.onClick = function(){
      scopeObj.view.customersDropdown.tbxSearchBox.text="";
      scopeObj.setCustomersDropDownList("customersDropdown");
      scopeObj.view.customersDropdown.flxClearSearchImage.setVisibility(false);
      scopeObj.view.customersDropdown.flxSegmentList.setVisibility(true);
    };
    //contract search
    this.view.contractCustomers.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.contractCustomers.tbxSearchBox.text.trim().length!==0){
        scopeObj.view.contractCustomers.flxClearSearchImage.setVisibility(true);
        scopeObj.searchCustomersDropDownList("contractCustomers");
      }else{
        scopeObj.view.contractCustomers.flxClearSearchImage.setVisibility(false);
        scopeObj.setSerchContractCustomers(scopeObj.recentContractDetails.formattedCustomers)
        scopeObj.view.contractCustomers.flxSegmentList.setVisibility(true);
      } 
    };
    this.view.contractCustomers.flxClearSearchImage.onClick = function(){
      scopeObj.view.contractCustomers.tbxSearchBox.text="";
      scopeObj.setSerchContractCustomers(scopeObj.recentContractDetails.formattedCustomers)
      scopeObj.view.contractCustomers.flxClearSearchImage.setVisibility(false);
      scopeObj.view.contractCustomers.flxSegmentList.setVisibility(true);
    };   
       //searchExistingUserRelatedCustomer
    
    this.view.searchExistingUserRelatedCustomer.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.searchExistingUserRelatedCustomer.tbxSearchBox.text.trim().length!==0){
        scopeObj.view.searchExistingUserRelatedCustomer.flxSearchCancel.setVisibility(true);
        scopeObj.searchExistingUserDropDownList();
      }else{
        scopeObj.view.searchExistingUserRelatedCustomer.flxSearchCancel.setVisibility(false);
        scopeObj.setSerchContractCustomers(scopeObj.recentContractDetails.formattedCustomers);
        if(scopeObj.view.flxNoResultsFoundExisting.isVisible)
        scopeObj.view.flxNoResultsFoundExisting.setVisibility(false);
        var seg;
        if(scopeObj.recentCustomerSearchTab === "Existing"){
          seg = scopeObj.view.segExistingUserListContainer;
        }else if(scopeObj.recentCustomerSearchTab === "other"){
          seg = scopeObj.view.segOtherRelatedCustList;
        }
        var segdata = [];
        for(var i=0;i<seg.info.records.length;i++){
          segdata.push(seg.info.records[i].orignalData)
        }
        scopeObj.setUserListData(segdata, seg);
      } 
    };
    this.view.searchExistingUserRelatedCustomer.flxSearchCancel.onClick = function(){
      scopeObj.view.searchExistingUserRelatedCustomer.tbxSearchBox.text="";
      if(scopeObj.view.flxNoResultsFoundExisting.isVisible)
      scopeObj.view.flxNoResultsFoundExisting.setVisibility(false);
      var seg;
      var seg;
        if(scopeObj.recentCustomerSearchTab === "Existing"){
          seg = scopeObj.view.segExistingUserListContainer;
        }else if(scopeObj.recentCustomerSearchTab === "other"){
          seg = scopeObj.view.segOtherRelatedCustList;
        }
        var segdata = [];
        for(var i=0;i<seg.info.records.length;i++){
          segdata.push(seg.info.records[i].orignalData)
        }
        scopeObj.setUserListData(segdata, seg);
      scopeObj.view.searchExistingUserRelatedCustomer.flxSearchCancel.setVisibility(false);
    };    
    this.view.customersDropdownFA.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.customersDropdownFA.tbxSearchBox.text.trim().length!==0){
        scopeObj.view.customersDropdownFA.flxClearSearchImage.setVisibility(true);
        scopeObj.searchCustomersDropDownList("customersDropdownFA");
      }else{
        scopeObj.view.customersDropdownFA.flxClearSearchImage.setVisibility(false);
        scopeObj.setCustomersDropDownList("customersDropdownFA");
        scopeObj.view.customersDropdownFA.flxSegmentList.setVisibility(true);
      }  
    };
    this.view.customersDropdownFA.flxClearSearchImage.onClick = function(){
      scopeObj.view.customersDropdownFA.tbxSearchBox.text="";
      scopeObj.setCustomersDropDownList("customersDropdownFA");
      scopeObj.view.customersDropdownFA.flxClearSearchImage.setVisibility(false);
      scopeObj.view.customersDropdownFA.flxSegmentList.setVisibility(true);
    };
    this.view.customersDropdownLimits.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.customersDropdownLimits.tbxSearchBox.text.trim().length!==0){
        scopeObj.view.customersDropdownLimits.flxClearSearchImage.setVisibility(true);
        scopeObj.searchCustomersDropDownList("customersDropdownLimits");
      }else{
        scopeObj.view.customersDropdownLimits.flxClearSearchImage.setVisibility(false);
        scopeObj.setCustomersDropDownList("customersDropdownLimits");
        scopeObj.view.customersDropdownLimits.flxSegmentList.setVisibility(true);
      }  
    };
    this.view.customersDropdownLimits.flxClearSearchImage.onClick = function(){
      scopeObj.view.customersDropdownLimits.tbxSearchBox.text="";
      scopeObj.setCustomersDropDownList("customersDropdownLimits");
      scopeObj.view.customersDropdownLimits.flxClearSearchImage.setVisibility(false);
      scopeObj.view.customersDropdownLimits.flxSegmentList.setVisibility(true);
    };
    this.view.contractDetailsPopup.flxBackOption.onClick = function(){
      scopeObj.view.flxCustomerSearchPopUp.setVisibility(true);
      scopeObj.view.flxContractDetailsPopup.setVisibility(false);
      scopeObj.view.forceLayout();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.onClick = function(){
      if(scopeObj.view.flxContractFA.isVisible){
        scopeObj.addNewFeatureRowBulkUpdate();
      } else{
        scopeObj.addNewLimitRowBulkUpdate();
      } 
    };
	this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton1.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton2.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton3.onTouchStart = function(){
      scopeObj.resetAddedRows();
    };
    this.view.bulkUpdateFeaturesLimitsPopup.lblIconArrow.onTouchStart = function(){
      if(scopeObj.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.isVisible){
        scopeObj.view.bulkUpdateFeaturesLimitsPopup.lblIconArrow.text="";
        scopeObj.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.setVisibility(false);
      }else{
      	scopeObj.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.setVisibility(true);
        scopeObj.view.bulkUpdateFeaturesLimitsPopup.lblIconArrow.text="";
      }
      scopeObj.view.forceLayout();
    };
    this.view.typeHeadContractCountry.tbxSearchKey.onKeyUp = function(){
      scopeObj.clearValidation(scopeObj.view.typeHeadContractCountry.tbxSearchKey, scopeObj.view.flxNoCountry, 1);
      scopeObj.view.typeHeadContractCountry.tbxSearchKey.info.isValid = false;
      scopeObj.hideAddressSegments(scopeObj.view.typeHeadContractCountry);
      scopeObj.view.flxCountry.zIndex = 2;
      scopeObj.searchForAddress(scopeObj.view.typeHeadContractCountry.tbxSearchKey, scopeObj.view.typeHeadContractCountry.segSearchResult, scopeObj.view.typeHeadContractCountry.flxNoResultFound, 1);
      if(scopeObj.view.flxNoContractCountry.isVisible){
        scopeObj.view.flxNoContractCountry.isVisible = false;
        scopeObj.view.typeHeadContractCountry.tbxSearchKey.skin = "skntbxLato35475f14px";
      }
      scopeObj.view.forceLayout();
    };
    this.view.typeHeadContractCountry.tbxSearchKey.onEndEditing = function(){
      if (scopeObj.view.typeHeadContractCountry.flxNoResultFound.isVisible) {
        scopeObj.view.typeHeadContractCountry.flxNoResultFound.setVisibility(false);
      }
    };
    this.view.typeHeadContractCountry.segSearchResult.onRowClick = function(){
      scopeObj.assingText(scopeObj.view.typeHeadContractCountry.segSearchResult, scopeObj.view.typeHeadContractCountry.tbxSearchKey);
      scopeObj.clearValidation(scopeObj.view.typeHeadContractCountry.tbxSearchKey,scopeObj.view.flxNoCountry,1);
    };
    this.view.contractDetailsEntry3.tbxEnterValue.onKeyUp = function(){
      if(scopeObj.view.contractDetailsEntry3.flxInlineError.isVisible){
        scopeObj.view.contractDetailsEntry3.flxEnterValue.skin = "sknflxEnterValueNormal";
        scopeObj.view.contractDetailsEntry3.flxInlineError.isVisible = false;
      }
    };
    this.view.contractDetailsEntry6.tbxEnterValue.onKeyUp = function(){
      if(scopeObj.view.contractDetailsEntry6.flxInlineError.isVisible){
        scopeObj.view.contractDetailsEntry6.flxEnterValue.skin = "sknflxEnterValueNormal";
        scopeObj.view.contractDetailsEntry6.flxInlineError.isVisible = false;
      }
    };
    this.view.contractContactNumber.txtISDCode.onKeyUp = function(){
      if(scopeObj.view.contractContactNumber.flxError.isVisible){
        scopeObj.view.contractContactNumber.flxError.isVisible = false;
        scopeObj.view.contractContactNumber.txtISDCode.skin = "skntxtbxDetails0bbf1235271384a";
      }
    };
    this.view.contractContactNumber.txtContactNumber.onKeyUp = function(){
      if(scopeObj.view.contractContactNumber.flxError.isVisible){
        scopeObj.view.contractContactNumber.flxError.isVisible = false;
        scopeObj.view.contractContactNumber.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
      }
    };
    this.view.AdvancedSearchDropDown01.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.AdvancedSearchDropDown01.tbxSearchBox.text.trim().length>0){
        scopeObj.view.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(true);
        var segData=scopeObj.view.AdvancedSearchDropDown01.sgmentData.data;
        var searchText=scopeObj.view.AdvancedSearchDropDown01.tbxSearchBox.text;
        var statusName="";
        var filteredData=segData.filter(function(rec){
          statusName=rec.lblDescription.toLowerCase();
          if(statusName.indexOf(searchText)>=0)
            return rec;
        });
        if(filteredData.length===0){
          scopeObj.view.AdvancedSearchDropDown01.sgmentData.setVisibility(false);
          scopeObj.view.AdvancedSearchDropDown01.richTexNoResult.setVisibility(true);
        }else{
          scopeObj.view.AdvancedSearchDropDown01.sgmentData.setData(filteredData);
          scopeObj.view.AdvancedSearchDropDown01.sgmentData.setVisibility(true);
          scopeObj.view.AdvancedSearchDropDown01.richTexNoResult.setVisibility(false);
        }
      }else{
        scopeObj.view.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(false);
        var totalRecords=scopeObj.view.AdvancedSearchDropDown01.sgmentData.info.data;
        scopeObj.view.AdvancedSearchDropDown01.sgmentData.setData(totalRecords);
      }
      scopeObj.view.forceLayout();
    };
    this.view.AdvancedSearchDropDown01.flxSearchCancel.onClick = function(){
      scopeObj.view.AdvancedSearchDropDown01.flxSearchCancel.setVisibility(false);
      scopeObj.view.AdvancedSearchDropDown01.tbxSearchBox.text="";
      var totalRecords=scopeObj.view.AdvancedSearchDropDown01.sgmentData.info.data;
      scopeObj.view.AdvancedSearchDropDown01.sgmentData.setData(totalRecords);
      scopeObj.view.forceLayout();
    };

    this.view.AdvancedSearchDropDownServType.tbxSearchBox.onKeyUp = function(){
      if(scopeObj.view.AdvancedSearchDropDownServType.tbxSearchBox.text.trim().length>0){
        scopeObj.view.AdvancedSearchDropDownServType.flxSearchCancel.setVisibility(true);
        var segData=scopeObj.view.AdvancedSearchDropDownServType.sgmentData.data;
        var searchText=scopeObj.view.AdvancedSearchDropDownServType.tbxSearchBox.text;
        var statusName="";
        var filteredData=segData.filter(function(rec){
          statusName=rec.lblDescription.toLowerCase();
          if(statusName.indexOf(searchText)>=0)
            return rec;
        });
        if(filteredData.length===0){
          scopeObj.view.AdvancedSearchDropDownServType.sgmentData.setVisibility(false);
          scopeObj.view.AdvancedSearchDropDownServType.richTexNoResult.setVisibility(true);
        }else{
          scopeObj.view.AdvancedSearchDropDownServType.sgmentData.setData(filteredData);
          scopeObj.view.AdvancedSearchDropDownServType.sgmentData.setVisibility(true);
          scopeObj.view.AdvancedSearchDropDownServType.richTexNoResult.setVisibility(false);
        }
      }else{
        scopeObj.view.AdvancedSearchDropDownServType.flxSearchCancel.setVisibility(false);
        var totalRecords=scopeObj.view.AdvancedSearchDropDownServType.sgmentData.info.data;
        scopeObj.view.AdvancedSearchDropDownServType.sgmentData.setData(totalRecords);
      }
      scopeObj.view.forceLayout();
    };
    this.view.AdvancedSearchDropDownServType.flxSearchCancel.onClick = function(){
      scopeObj.view.AdvancedSearchDropDownServType.flxSearchCancel.setVisibility(false);
      scopeObj.view.AdvancedSearchDropDownServType.tbxSearchBox.text="";
      var totalRecords=scopeObj.view.AdvancedSearchDropDownServType.sgmentData.info.data;
      scopeObj.view.AdvancedSearchDropDownServType.sgmentData.setData(totalRecords);
      scopeObj.view.forceLayout();
    };
    // Add signatory groups actions
    this.view.verticalTabs.btnOption1.onClick = function(){
      scopeObj.view.flxSelectedUsers.setVisibility(false);
      scopeObj.view.flxGroupDetails.setVisibility(true);
      scopeObj.toggleAddGroupVerticalTabs(scopeObj.view.verticalTabs.flxImgArrow1,scopeObj.view.verticalTabs.btnOption1);
      scopeObj.view.forceLayout();
    };
    this.view.verticalTabs.btnOption2.onClick = function(){
      scopeObj.view.flxSelectedUsers.setVisibility(true);
      scopeObj.view.flxGroupDetails.setVisibility(false);
      scopeObj.toggleAddGroupVerticalTabs(scopeObj.view.verticalTabs.flxImgArrow2,scopeObj.view.verticalTabs.btnOption2);
      scopeObj.view.forceLayout();
    };
    /**Approval matrix actions - start **/
    this.view.btnViewAccountsAM.onClick = function(){
      scopeObj.showApprovalMatrixAccountLevel(true);
    };
    this.view.flxBackBtnAM.onClick = function(){
      scopeObj.showApprovalMatrixNew(false);
    };
    this.view.flxSegMore.onHover = this.onHoverSettings;
    this.view.flxServiceTypeFilter.onHover = this.onHoverSettings;
    this.view.flxDropDownDetail23.onHover = this.onHoverSettings;
    this.view.flxAccountsFilter.onHover = this.onHoverSettings;
	this.view.flxAccountsFilterAddUser.onHover = this.onHoverSettings;
    this.view.flxOwnershipFilter.onHover = this.onHoverSettings;
	this.view.flxOwnershipFilterAddUser.onHover = this.onHoverSettings;
  },
  suspendFeatureEditHideShow : function(show){
    if(show === "suspend_screen"){
      this.view.flxFeatureTabs.setVisibility(false);
      this.view.flxAdditionalFeatures.setVisibility(false);
      this.view.flxMandatoryFeatures.setVisibility(false);
      this.view.flxSuspendFeatures.setVisibility(true);
      this.view.flxAssignFeaturesBottomButtons.setVisibility(false);
      this.setEditSuspendFeatures();
    }else if (show === "additional_feature_screen"){
      this.view.flxFeatureTabs.setVisibility(true);
      this.view.flxAdditionalFeatures.setVisibility(true);
      this.view.flxMandatoryFeatures.setVisibility(false);
      this.view.flxSuspendFeatures.setVisibility(false);
      this.view.flxAssignFeaturesBottomButtons.setVisibility(true);
      this.tabUtilLabelFunction([ this.view.lblMandatoryFeatures,this.view.lblAdditionalFeatures],this.view.lblAdditionalFeatures);
    }else if (show === "mandatory_feature_screen"){
      this.view.flxFeatureTabs.setVisibility(true);
      this.view.flxAdditionalFeatures.setVisibility(false);
      this.view.flxMandatoryFeatures.setVisibility(true);
      this.view.flxSuspendFeatures.setVisibility(false);
      this.view.flxAssignFeaturesBottomButtons.setVisibility(true);
      this.tabUtilLabelFunction([ this.view.lblMandatoryFeatures,this.view.lblAdditionalFeatures],this.view.lblMandatoryFeatures);
    }    
    this.view.forceLayout();
  },
  suspendFeatureHideShow : function(show){
    if(show === "suspend_screen"){
      this.view.flxFeaturesTabs.setVisibility(false);
      this.view.flxViewAdditionalFeature.setVisibility(false);
      this.view.flxViewMandatoryFeature.setVisibility(false);
      this.view.flxFeatureSuspensionContainer.setVisibility(true);
      var segHeight = this.view.flxCompanyDetails.frame.height -(this.view.flxMainDetails.frame.height +this.view.flxViewTabs.frame.height +200);
      this.view.flxSegSuspendedContainer.height = (segHeight < 200) ? "200px": segHeight +"px";
      this.setSuspendFeatures();
    }else if (show === "additional_feature_screen"){
      this.view.flxFeaturesTabs.setVisibility(true);
      this.view.flxViewAdditionalFeature.setVisibility(true);
      this.view.flxViewMandatoryFeature.setVisibility(false);
      this.view.flxFeatureSuspensionContainer.setVisibility(false);
      this.changeSubTabSkin([this.view.btnMandatoryFeatures,this.view.btnAdditionalFeatures], this.view.btnAdditionalFeatures);
      this.setCompanyFeatures();
    }else if(show === "mandatory_feature_screen"){
      this.view.flxFeaturesTabs.setVisibility(true);
      this.view.flxViewAdditionalFeature.setVisibility(false);
      this.view.flxViewMandatoryFeature.setVisibility(true);
      this.view.flxFeatureSuspensionContainer.setVisibility(false);
      this.changeSubTabSkin([this.view.btnMandatoryFeatures,this.view.btnAdditionalFeatures], this.view.btnMandatoryFeatures);      
    }
	this.view.forceLayout();	
  },
  featureTabandDetailsNextHandler : function(){
    if(this.action === this.actionConfig.create)
      this.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
    var isValidCompanyDetails = this.validatingCompanyDetails();
    var isValidAccounts  = this.checkAccountValidation();
    if(!isValidAccounts){
        this.view.flxAccountError.setVisibility(true);
        this.view.flxAccountsAddAndRemove.top = "60dp";
    }else{
        this.view.flxAccountError.setVisibility(false);
        this.view.flxAccountsAddAndRemove.top = "0dp";
    }
    this.showHideAddRemoveSegment();
    if(isValidCompanyDetails && isValidAccounts){
      this.showAssingFeaturesScreen();
    }
  },
  limitTabandFeatureNextHandler : function(){
    var isValidCompanyDetails = this.validatingCompanyDetails();
    var isValidAccounts = this.checkAccountValidation();
    if(!isValidAccounts){
        this.view.flxAccountError.setVisibility(true);
        this.view.flxAccountsAddAndRemove.top = "60dp";
    }else{
        this.view.flxAccountError.setVisibility(false);
        this.view.flxAccountsAddAndRemove.top = "0dp";
    }
    this.showHideAddRemoveSegment();
    if(isValidCompanyDetails && isValidAccounts){
      this.showAssingLimitsScreen();
    }       
  },
  ownerTabLimitnextHandler : function(){
    var isValidCompanyDetails = this.validatingCompanyDetails();
    var isValidAccounts = this.checkAccountValidation();
    var isvalidLimits = this.assignLimitValidations();
    if(!isValidAccounts){
        this.view.flxAccountError.setVisibility(true);
        this.view.flxAccountsAddAndRemove.top = "60dp";
    }else{
        this.view.flxAccountError.setVisibility(false);
        this.view.flxAccountsAddAndRemove.top = "0dp";
    }
    this.showHideAddRemoveSegment();
    if(isValidCompanyDetails && isValidAccounts && isvalidLimits){
      this.showCreateOwnerDetails();
    }
  },
  showAssingLimitsScreen : function(){
    this.hideAllOptionsButtonImages();
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      this.view.verticalTabsCompany.flxImgArrow4
    );
    this.view.flxCompanyDetailsTab.setVisibility(false);
    this.view.flxAccountsTab.setVisibility(false);
    this.view.flxAssignFeaturesTab.setVisibility(false);
    this.view.flxAssignLimitsTab.setVisibility(true);
    this.view.flxOwnerDetailsTabs.setVisibility(false);
    var widgetArray = [this.view.verticalTabsCompany.btnOption1,this.view.verticalTabsCompany.btnOption2,this.view.verticalTabsCompany.btnOption3,this.view.verticalTabsCompany.btnOption4];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.verticalTabsCompany.btnOption4);
    this.view.commonButtonsLimitTab.btnSave.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsLimitTab.btnSave.focusSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsLimitTab.btnSave.hoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px"; 
    if(this.action === this.actionConfig.edit){
   		this.changeLimitStatusLocally();
    }
  },
  showAssingFeaturesScreen : function(){
    this.hideAllOptionsButtonImages();
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      this.view.verticalTabsCompany.flxImgArrow3
    );
    this.view.flxCompanyDetailsTab.setVisibility(false);
    this.view.flxAccountsTab.setVisibility(false);
    this.view.flxAssignFeaturesTab.setVisibility(true);
    this.view.flxAssignLimitsTab.setVisibility(false);
    this.view.flxOwnerDetailsTabs.setVisibility(false);    
    var widgetArray = [this.view.verticalTabsCompany.btnOption1,this.view.verticalTabsCompany.btnOption2,this.view.verticalTabsCompany.btnOption3,this.view.verticalTabsCompany.btnOption4];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.verticalTabsCompany.btnOption3);
    this.view.commonButtonsFeaturesTab.btnSave.skin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsFeaturesTab.btnSave.focusSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsFeaturesTab.btnSave.hoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsFeaturesTab.btnNext.skin = "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsFeaturesTab.btnNext.focusSkin = "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px";
    this.view.commonButtonsFeaturesTab.btnNext.hoverSkin = "sknBtn2D5982LatoRegular13pxFFFFFFRad20px";
    if(this.action === this.actionConfig.create){
      this.view.btnShowSuspendFeatureInEdit.setVisibility(false);
      this.featureFilter();
    }else if(this.action === this.actionConfig.edit){
      this.view.btnShowSuspendFeatureInEdit.setVisibility(true);
      this.setEditSuspendFeatures();
    }
    this.suspendFeatureEditHideShow("mandatory_feature_screen");
  },
  getFeatureStatusAndSkin : function(featureStatus){
    var status,statusIconSkin;
    if(featureStatus === "SID_FEATURE_ACTIVE"){
      status = kony.i18n.getLocalizedString("i18n.frmAlertsManagement.Active");
      statusIconSkin = "sknFontIconActivate";
    }else if(featureStatus === "SID_FEATURE_INACTIVE"){
      status = kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive");
      statusIconSkin = "sknfontIconInactive";        
    }else if(featureStatus === "SID_FEATURE_UNAVAILABLE"){
      status = kony.i18n.getLocalizedString("i18n.common.Unavailable");
      statusIconSkin = "sknFontIconError";        
    }else if(featureStatus === "SID_FEATURE_SUSPENDED"){
      status = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Suspended");
      statusIconSkin = "sknFontIconSuspend";                
    }
    return{
      "status" : status,
      "statusIconSkin" : statusIconSkin
    };
  },
  getAllFeatures : function(res){
      this.allFeatures = res;
      this.featureFilter();
  },
  featureFilter : function(){
    var companyType = this.AdminConsoleCommonUtils.constantConfig.BUSINESS_TYPE;
    var i;
    var temp_features=[];
    this.requiredFeatures.additional = [];
    this.requiredFeatures.mandatory = [];
    for(i=0;i<this.allFeatures.groups.length;i++){
    	if(this.allFeatures.groups[i].groupid === companyType){
          temp_features = this.allFeatures.groups[i].features;
        }
    }
    for(var j=0;j<temp_features.length;j++){
      if(temp_features[j].isPrimary === "false"){
        this.requiredFeatures.additional.push(temp_features[j]);
      }
      else if(temp_features[j].isPrimary === "true"){
        this.requiredFeatures.mandatory.push(temp_features[j]);
      }
    }
    if(this.action === this.actionConfig.create){
      if(!this.visitedFeatureOnceWhileCreate){
        this.setFeaturesData(this.requiredFeatures);
        this.visitedFeatureOnceWhileCreate = true;
      }
    }else if(this.action === this.actionConfig.edit){
      this.setFeaturesData(this.requiredFeatures);
    }
  },
  setFeaturesData : function(dataParam){
    this.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All");
    var dataAdditional = (dataParam.additional).map(this.featureMapping);
    var dataMandatory = (dataParam.mandatory).map(this.featureMapping);
    var widgetDataMapAdditional = {
      "flxTempFeatures":"flxTempFeatures",
      "flxFeaturesContainer":"flxFeaturesContainer",
      "flxFeatureUpper":"flxFeatureUpper",
      "flxCheckBox":"flxCheckBox",
      "flxStatus": "flxStatus",
      "flxFeaturesLower":"flxFeaturesLower",
      "ImgCheckBox":"ImgCheckBox",
      "lblFeature":"lblFeature",
      "flxViewDetails":"flxViewDetails",
      "lblViewFeatureDetails":"lblViewFeatureDetails",
      "lblUsersStatus":"lblUsersStatus",
	  "fontIconStatusImg":"fontIconStatusImg",
      "featureId":"featureId",
      "featureType":"featureType",
      "selected":"selected"
    };
    var widgetDataMapMandatory = {
      "flxMandatoryFeatures":"flxMandatoryFeatures",
      "flxFeaturesContainer":"flxFeaturesContainer",
      "flxFeatureUpper":"flxFeatureUpper",
      "flxStatus": "flxStatus",
      "flxFeaturesLower":"flxFeaturesLower",
      "lblFeature":"lblFeature",
      "flxViewDetails":"flxViewDetails",
      "lblViewFeatureDetails":"lblViewFeatureDetails",
      "lblUsersStatus":"lblUsersStatus",
	  "fontIconStatusImg":"fontIconStatusImg",
      "featureId":"featureId",
      "featureType":"featureType",
      "selected" :"selected"
    };
    this.view.segFeatures.widgetDataMap = widgetDataMapAdditional;
    this.view.segFeatures.setData(dataAdditional);
    this.setAdditionfeatureCount();
    
    this.view.segmandatoryFeatures.widgetDataMap = widgetDataMapMandatory;
    this.view.segmandatoryFeatures.setData(dataMandatory);
    this.setMandaotryfeatureCount();
    
    this.payloadGenerationForMonetaryActions();
    this.view.forceLayout();
  },
  featureMappingMandaotry : function(data){
    var self = this;
    var statusInfo = {};
    var featureStatus = data.status;
	statusInfo = self.getFeatureStatusAndSkin(featureStatus);
    return{
      "flxTempFeatures":"flxTempFeatures",
      "flxFeaturesContainer":"flxFeaturesContainer",
      "flxFeatureUpper":"flxFeatureUpper",
      "flxStatus": "flxStatus",
      "flxFeaturesLower":"flxFeaturesLower",
      "lblFeature":data.name,
      "lblViewFeatureDetails":{
        "text" : "View Details",
      },
      "flxViewDetails":{
        "hoverSkin": "sknFlxPointer",
        "onClick": function(){self.createEditViewFeatureDetail(data);}
      },
      "lblUsersStatus":{
        "text": statusInfo.status,
        "skin": "sknlblLato5bc06cBold14px"
      },
	  "fontIconStatusImg":{
        "skin": statusInfo.statusIconSkin
      },
      "featureId" : data.id,
      "featureType" : data.type,
      "selected" : "true",
      "statusId" : data.status,
    };
  },
  featureMapping : function(data){
    var self = this;
    var statusInfo = {};
    var featureStatus = data.status;
	statusInfo = self.getFeatureStatusAndSkin(featureStatus);
    return{
      "flxTempFeatures":"flxTempFeatures",
      "flxFeaturesContainer":"flxFeaturesContainer",
      "flxFeatureUpper":"flxFeatureUpper",
      "flxCheckBox":{
        onClick : self.selectionImageChange
      },
      "flxStatus": "flxStatus",
      "flxFeaturesLower":"flxFeaturesLower",
      "ImgCheckBox":"checkboxnormal.png",
      "lblFeature":data.name,
      "lblViewFeatureDetails":{
        "text" : "View Details",
      },
      "flxViewDetails":{
        "hoverSkin": "sknFlxPointer",
        "onClick": function(){self.createEditViewFeatureDetail(data);}
      },
      "lblUsersStatus":{
        "text": statusInfo.status,
        "skin": "sknlblLato5bc06cBold14px"
      },
	  "fontIconStatusImg":{
        "skin": statusInfo.statusIconSkin
      },
      "featureId" : data.id,
      "featureType" : data.type,
      "selected":"false",
      // added for suspend feature 
      "featureData" : data,
      "statusId" : data.status,
      "localStatus" : data.status
    };
  },
  createEditViewFeatureDetail : function(data){
    var actionArray = [];
    for(var i=0;i<data.actions.length;i++){
      var temp = {
        "actionType": data.actions[i].type,
        "actionId": data.actions[i].id,
        "actionDescription": data.actions[i].description,
        "actionName": data.actions[i].name
      };
      actionArray.push(temp);
    }
  	var requiredData = {
      "featureName" : data.name,
      "status" : data.status ,
      "featureDescription" : data.description,      
      "Actions":actionArray
    };
    this.fillFeatureDetails(requiredData);
  },
  selectionImageChange : function(all){
	var data = this.view.segFeatures.data;
    var index;
    if(all === "add"){
      for(var i =0;i<data.length;i++){
        data[i].ImgCheckBox = "checkboxselected.png";
        data[i].selected = "true";
      }
      this.view.segFeatures.setData(data);
    }else if(all === "remove"){
      for(var i =0;i<data.length;i++){
        data[i].ImgCheckBox = "checkboxnormal.png";
        data[i].selected = "false";
      }
      this.view.segFeatures.setData(data);
    }else{
      index = this.view.segFeatures.selectedRowIndex[1];
      var rowData = data[index];
      if(rowData.ImgCheckBox === "checkboxnormal.png"){
        rowData.ImgCheckBox = "checkboxselected.png";
        rowData.selected = "true";
      }else{
        rowData.ImgCheckBox = "checkboxnormal.png";
        rowData.selected = "false";
      }
      this.view.segFeatures.setDataAt(rowData, index, 0);
    }
    if(this.action === this.actionConfig.edit){
      this.setEditSuspendFeatures();
    }
	this.setAdditionfeatureCount();
    this.payloadGenerationForMonetaryActions();
    this.view.forceLayout();
  },
  setMandaotryfeatureCount : function(){
    var selectedCount = this.view.segmandatoryFeatures.data.length;
    this.view.mandatoryFeatureHeader.lblRecords.text = kony.i18n.getLocalizedString("i18n.companies.totalFeatures")+" "+selectedCount;
  },
  setAdditionfeatureCount : function(){
    var selectedCount = 0;
    for(var i=0;i<this.view.segFeatures.data.length;i++){
      if(this.view.segFeatures.data[i].selected === "true")
        selectedCount++;
    } 
    if(selectedCount === 1)
      this.view.subHeader.lblRecords.text = selectedCount + " "+kony.i18n.getLocalizedString("i18n.companies.featureSelected");
    else
      this.view.subHeader.lblRecords.text = selectedCount + " "+kony.i18n.getLocalizedString("i18n.companies.featuresSelected");
    //show addAll or removeAll based on count
    if(selectedCount === this.view.segFeatures.data.length){
      this.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.SelectedOptions.RemoveAll");
    } else{
      this.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All");
    }
  },
  selectUnselectAllFeatures : function(){
    if(this.view.lblSelectionOptions.text === kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All")){
      this.selectionImageChange("add");
      this.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.SelectedOptions.RemoveAll");
    }else if (this.view.lblSelectionOptions.text === kony.i18n.getLocalizedString("i18n.SelectedOptions.RemoveAll")){
      this.selectionImageChange("remove");
	  this.view.lblSelectionOptions.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.Add_All");
    }
  },  
  selectedFeaturesforEdit : function(companyFeatures){
    var allFeatures = this.view.segFeatures.data;
    var statusInfo = {};
    var featureStatus;
    for(var i=0;i<allFeatures.length;i++){
      for(var j=0;j<companyFeatures.length;j++){
        if(companyFeatures[j].featureId === allFeatures[i].featureId){
          allFeatures[i].ImgCheckBox = "checkboxselected.png";
          allFeatures[i].selected = "true";  
          if(companyFeatures[j].featureStatus === "SID_FEATURE_SUSPENDED"){
            featureStatus = companyFeatures[j].featureStatus;
            statusInfo = this.getFeatureStatusAndSkin(featureStatus);
            allFeatures[i].lblUsersStatus.text = statusInfo.status;
            allFeatures[i].lblUsersStatus.skin = "sknlblLato5bc06cBold14px";
            allFeatures[i].fontIconStatusImg.skin = statusInfo.statusIconSkin;
            allFeatures[i].statusId = "SID_FEATURE_SUSPENDED";
            allFeatures[i].localStatus = "SID_FEATURE_SUSPENDED";
          }
        }
      }
    }
    this.intialFeatureList = companyFeatures;
    allFeatures = this.moveSelectedFeaturesToTop(allFeatures);
    this.view.segFeatures.setData(allFeatures);
    this.setAdditionfeatureCount();
    var companyActionLimits = [];
    for(var k=0;k<companyFeatures.length;k++){
      var temp  = {
        "Actions" : companyFeatures[k].Actions,
        "featureId" : companyFeatures[k].featureId,
        "featureStatus" : companyFeatures[k].featureStatus
      };
      companyActionLimits.push(temp);//(companyFeatures[k].Actions);
    }
	this.companyLimitsforEdit(companyActionLimits);
    this.view.forceLayout();
  },
  moveSelectedFeaturesToTop : function(data){
    var temp = [];
    var dataLength = data.length;
    for(var j=0;j<dataLength;j++){
      for(var i=0;i<dataLength-j-1;i++){
        if(data[i].selected !== "true"){
          temp = data[i];
          data[i] = data[i+1];
          data[i+1] = temp;
        }
      }
    }
    return data;
  },
  companyLimitsforEdit : function(companyActionLimits){
    var actionLimitData = [];
    for(var i =0 ;i<companyActionLimits.length;i++){
      for(var j=0;j<companyActionLimits[i].Actions.length;j++){
        
        if(companyActionLimits[i].Actions[j].actionType === "MONETARY"){
          actionLimitData.push({
            "id" : companyActionLimits[i].featureId,
            "status" : companyActionLimits[i].featureStatus,
            "actions" : companyActionLimits[i].Actions[j]
          });
        }
      }
    }
    this.initialLimitList = actionLimitData;
	var finalList = this.getFYILevelLimits();
	this.setMonetaryActionsData(finalList);
  },
  getFYILevelLimits : function(){
    var self = this;
    var initialSet = self.initialLimitList;
    for(var i=0;i<self.requiredFeatures.additional.length;i++){
      for(var j=0;j<initialSet.length;j++){
        if(self.requiredFeatures.additional[i].id === initialSet[j].id){
          for(var k=0;k<self.requiredFeatures.additional[i].actions.length;k++){
            if(self.requiredFeatures.additional[i].actions[k].type === "MONETARY"){
              initialSet[j].actions.MaxWeeklyLimit = self.requiredFeatures.additional[i].actions[k].limits[0].value;
              initialSet[j].actions.MaxTransactionLimit = self.requiredFeatures.additional[i].actions[k].limits[1].value;
              initialSet[j].actions.MaxDailyLimit = self.requiredFeatures.additional[i].actions[k].limits[2].value;
            }}}}
    }
    return initialSet;
  },
  payloadGenerationForMonetaryActions : function(){
    var data = this.view.segFeatures.data;
    var selectedData =[];
    var temp = {};
    for(var i = 0;i<data.length;i++){
      if(data[i].selected === "true"){
        temp = {
          "id" : data[i].featureId
        };
        selectedData.push(temp);
      }
      temp = {};
    }
    if(selectedData.length === 0){
      this.monetaryActionsData = [];
      this.setMonetaryActionsData(this.monetaryActionsData);      
    }else{
      this.getMonetaryActions({"features":selectedData});
    }
  },
  getMonetaryActions : function(payload){
    var self = this;
    var success = function(res){      
      self.monetaryActionsData = res.features;
      self.setMonetaryActionsData(self.monetaryActionsData);
    };
    var error = function(err){
      kony.print("error while fetching all limits"+err);
    };
    this.presenter.getMonetaryActions(payload,success,error);
  },
  setMonetaryActionsData : function(dataParam){
    if(dataParam.length === 0){
      this.view.flxLimitsContainer.setVisibility(false);
      this.view.flxNoLimitsAssociated.setVisibility(true);
      this.view.segAssignLimits.setData([]);
      this.view.forceLayout();
    }else{
      this.view.flxLimitsContainer.setVisibility(true);
      this.view.flxNoLimitsAssociated.setVisibility(false);
      var data = dataParam.map(this.mappingMonetaryActionsData);  
      var widgetDataMap = {
        "flLimitsCompanies" : "flLimitsCompanies",
        "flxLimitHeader" : "flxLimitHeader",
        "flxStatus":"flxStatus",
        "flxLimitLower":"flxLimitLower",
        
        "flxPerTransactionLimit":"flxPerTransactionLimit",
        "lblPerTransactionLimit":"lblPerTransactionLimit",
        "flxPerTransactionLimitError":"flxPerTransactionLimitError",
        "lblPerTransactionLimitErronIcon":"lblPerTransactionLimitErronIcon",
        "lblPerTransactionLimitError":"lblPerTransactionLimitError",
        "lblPerMaxIcon":"lblPerMaxIcon",
        "flxPerCurrency":"flxPerCurrency",
        
        "flxDailyTransactionLimit":"flxDailyTransactionLimit",
        "lblDailyTransactionLimit":"lblDailyTransactionLimit",
        "flxDailyTransactionLimitError":"flxDailyTransactionLimitError",
        "lblDailyTransactionLimitErrorIcon":"lblDailyTransactionLimitErrorIcon",
        "lblDailyTransactionLimitError":"lblDailyTransactionLimitError",
        "lblDailyMaxIcon":"lblDailyMaxIcon",
        "flxDailyCurrency":"flxDailyCurrency",
        
        "flxWeeklyTransactionLimit":"flxWeeklyTransactionLimit",
        "lblWeeklyTransactionLimit":"lblWeeklyTransactionLimit",
        "flxWeeklyTransactionLimitError":"flxWeeklyTransactionLimitError",
        "lblWeeklyTransactionLimitErrorIcon":"lblWeeklyTransactionLimitErrorIcon",
        "lblWeeklyTransactionLimitError":"lblWeeklyTransactionLimitError",
        "lblWeeklyMaxIcon":"lblWeeklyMaxIcon",
        "flxWeeklyCurrency":"flxWeeklyCurrency",
        
        "lblFeature":"lblFeature",
        "lblUsersStatus":"lblUsersStatus",
        "fontIconStatusImg":"fontIconStatusImg",
        "tbxPerTransactionLimitValue":"tbxPerTransactionLimitValue",
        "tbxDailyTransactionLimitValue":"tbxDailyTransactionLimitValue",
        "tbxWeeklyTransactionLimitValue":"tbxWeeklyTransactionLimitValue",
        "flxPerMax":"flxPerMax",
        "flxDailyMax":"flxDailyMax",
        "flxWeeklyMax":"flxWeeklyMax",
        "lblPerCurrency":"lblPerCurrency",
        "lblDailyCurrency":"lblDailyCurrency",
        "lblWeeklyCurrency":"lblWeeklyCurrency",
        "actionId" : "actionId"
      };
      this.view.segAssignLimits.widgetDataMap = widgetDataMap;
      this.view.segAssignLimits.setData(data);
      this.view.forceLayout();
    }
  },
  onSegmentPaginationChange: function(accountsFeaturesCard , currentValue , segment){
    currentValue = parseInt(currentValue);
    let totalDataLen = this.paginationDetails.currSegContractData[1].length;
    let startVal = (currentValue - 1) * 10;
    
    // exceeded the limit
    if(startVal >totalDataLen){
        return;
    }
    
    accountsFeaturesCard.reportPagination.lblNumber.text = currentValue;
    let endVal = currentValue  * 10;
    endVal = endVal > totalDataLen ? totalDataLen : endVal;
    accountsFeaturesCard.reportPagination.lblShowing.text = "Showing" + " " + startVal + " - " + endVal + " " + "Of " + totalDataLen;
    let paginData = this.paginationDetails.currSegContractData[1].slice(startVal, endVal);
    let segData = segment.data;
    segData[0][1] = paginData;

    segment.setData(segData);
  },
  paginationActionsForAcct:function(accountsFeaturesCard , segment , totalDataLen){
    accountsFeaturesCard.reportPagination.setVisibility(true);
    var scopeObj = this;

    scopeObj.totalPages = Math.ceil( totalDataLen/10);
    
    // initially setting the pagination values
    accountsFeaturesCard.reportPagination.lblNumber.text = "1";
    accountsFeaturesCard.reportPagination.lblShowing.text = "Showing 1 - 10 Of " + totalDataLen;

    accountsFeaturesCard.reportPagination.flxnext.onClick = function(){
    
      var currentValue=parseInt(accountsFeaturesCard.reportPagination.lblNumber.text)+1;
      scopeObj.onSegmentPaginationChange(accountsFeaturesCard , currentValue , segment);
    };
    accountsFeaturesCard.reportPagination.flxPrevious.onClick = function(){
      var currentValue=accountsFeaturesCard.reportPagination.lblNumber.text;
      let prevVal = parseInt(currentValue)-1 ;
      if(prevVal> 0){
        scopeObj.onSegmentPaginationChange(accountsFeaturesCard , prevVal , segment);
      }      
    };
    accountsFeaturesCard.reportPagination.flxGo.onClick = function(){
      var currentValue= accountsFeaturesCard.reportPagination.tbxPageNumber.text;
      if(currentValue && currentValue.toString().trim()!=="" && parseInt(currentValue)>0 && parseInt(currentValue)<=scopeObj.totalPages){
                                                                                                                            
        scopeObj.onSegmentPaginationChange(accountsFeaturesCard , currentValue , segment);
      }
    };
    accountsFeaturesCard.reportPagination.tbxPageNumber.onDone = function(){
      var currentValue= accountsFeaturesCard.reportPagination.tbxPageNumber.text;
      if(currentValue && currentValue.toString().trim()!=="" && parseInt(currentValue)>0 && parseInt(currentValue)<=scopeObj.totalPages){
                                                                                                                
        scopeObj.onSegmentPaginationChange(accountsFeaturesCard , currentValue , segment);
      }
    };
  },
  /*
   * Created By :Kaushik Mesala
   * function to set service data for viewing contract accounts
   * @param: component 
   * @param: result of service call to be mapped
   */
  setDataToContractAccts : function(accountsFeaturesCard , accounts , component_id){
    var self = this;
    accountsFeaturesCard.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Accounts");
    accountsFeaturesCard.flxCheckboxOptions.setVisibility(false);
    // There will be atlest 1 account in valid scenario. The below condition to handle the scenario if there is 0 accounts 
    var data=[];
    if(accounts && accounts.length > 0){
      data = accounts.map(this.mappingContractAccountsData);  
    }    
    
    // for(let a_x = 0 ;a_x <12;a_x++){
    //   data.push(data[0]);
    // }
    let datLen = data.length ;
    accountsFeaturesCard.lblCount.text = datLen<10? "(0"+datLen+")" : "("+datLen+")";
    
    let headerData = this.getHeaderDataForContractAccts(accountsFeaturesCard , accountsFeaturesCard.segAccountFeatures );
    
    // removing the last stroke at the bottom of the box
    if(datLen > 0 && data[datLen -1].lblSeperator){
      data[datLen -1].lblSeperator = '';
    }
    if(datLen > 10 ){
      this.paginationActionsForAcct(accountsFeaturesCard , accountsFeaturesCard.segAccountFeatures , datLen);
    }
    var sectionData = datLen > 0 ? [ headerData , data.slice(0,10)] :headerData;

    accountsFeaturesCard.segAccountFeatures.setData([sectionData]);
    
    // we do the pagination and sorting logic when the segment is on
    accountsFeaturesCard.flxArrow.onClick = function() {
      var visibility = accountsFeaturesCard.flxCardBottomContainer.isVisible ? false : true;
      if (visibility) {
          self.paginationDetails.currSegContractData = [headerData, data];
      }
      accountsFeaturesCard.toggleCollapseArrow(visibility);
      // reset previos selected component and segment
      let prevInd = this.prevContractSelected.contractNo;
      if (prevInd != -1) {
          let components = this.view.flxContractContainer.widgets();
          components[prevInd].toggleCollapseArrow(false);
      }
      this.setDataToContractAccountsFilter(this.view.statusTypeFilterMenu ,headerData, data , 'lblStatus'  , accountsFeaturesCard);
      this.setDataToContractAccountsFilter(this.view.acctTypeFilterMenu ,headerData, data , 'lblAccountType' , accountsFeaturesCard );
      this.setDataToContractAccountsFilter(this.view.ownerShipTypeFilterMenu ,headerData, data , 'lblAccountHolderName' , accountsFeaturesCard );
      
      if(!visibility){
        this.prevContractSelected.contractNo = -1;
      }else{
        this.prevContractSelected.contractNo = component_id;
      }      
    }.bind(this);
  },

  /*
   * Created By :Kaushik Mesala
   * function to map the contract accounts 
   * @param:  contract accounts result which will be mapped
   */
  mappingContractAccountsData : function(account){
    return {
        'fontIconStatus': {
          "text":"",
          'isVisible':true,
          "skin": account['statusDesc'].toLowerCase() === "active" ? "sknFontIconActivate" : "sknfontIconInactive"
        },
        'template' : 'flxContractsAccountView',
        'lbAccountName': account['accountName'],
        'lblAccountHolderName': account["ownerType"] ? account["ownerType"] :"N/A",
        'lblAccountNumber': account["accountId"],
        'lblAccountType': account["accountType"],
        'lblSeperator': '.',
        'lblStatus': account['statusDesc'] && account['statusDesc'].trim().length !==0 ?account['statusDesc'] : kony.i18n.getLocalizedString("i18n.common.Unavailable")
    };
  },
    mappingMonetaryActionsData : function(data){
      var statusInfo = {};
      var featureStatus = data.status;
      statusInfo = this.getFeatureStatusAndSkin(featureStatus);
      var flag;
      var daily, per, weekly,min;
      var maxTansactionLimit, maxWeeklyLimit, maxDailyLimit;
      var currencySymbol = this.currencyValue;
    if(data.actions[0]){
      weekly = data.actions[0].limits[0].value;
      per = data.actions[0].limits[1].value;
      daily = data.actions[0].limits[2].value;
      min = data.actions[0].limits[3].value;
      flag = true;
    }else if(data.actions.limits){
      weekly = data.actions.limits[0].value;
      per = data.actions.limits[1].value;
      daily = data.actions.limits[2].value;
      min = data.actions.limits[3].value;
      flag = false;
    }
    maxTansactionLimit = data.actions.MaxTransactionLimit ? data.actions.MaxTransactionLimit : per;
    maxWeeklyLimit =  data.actions.MaxWeeklyLimit ? data.actions.MaxWeeklyLimit : weekly;
    maxDailyLimit = data.actions.MaxDailyLimit ? data.actions.MaxDailyLimit : daily;
    return{
      "flLimitsCompanies" : "flLimitsCompanies",
      "flxLimitHeader" : "flxLimitHeader",
      "flxStatus":"flxStatus",
      "flxLimitLower":"flxLimitLower",
      "flxPerTransactionLimit":"flxPerTransactionLimit",
      "flxPerTransactionLimitError":{
        "isVisible" : false
      },
      "lblPerTransactionLimitErronIcon":"",
      "lblPerTransactionLimitError":{
        "text" : ""},
      "flxDailyTransactionLimit":"flxDailyTransactionLimit",
      "flxDailyTransactionLimitError":{
        "isVisible" : false
      },
      "lblDailyTransactionLimitErrorIcon":"",
      "lblDailyTransactionLimitError":{
        "text" : ""},
      "flxWeeklyTransactionLimit":"flxWeeklyTransactionLimit",
      "flxWeeklyTransactionLimitError":{
        "isVisible" : false
      },
      "lblWeeklyTransactionLimitErrorIcon":"",
      "lblWeeklyTransactionLimitError":{
        "text" : ""},
      
      "lblPerTransactionLimit":"Per Transactions Limit",
      "lblDailyTransactionLimit":"Daily Transaction Limit",
	  "lblWeeklyTransactionLimit":"Weekly Transaction Limit",
      "lblFeature":flag ? data.name : data.actions.actionName,
      "lblUsersStatus":{
        "text": statusInfo.status,
        "skin": "sknlblLato5bc06cBold14px"
      },
	  "fontIconStatusImg":{
        "skin": statusInfo.statusIconSkin
      },
      "tbxPerTransactionLimitValue":{"text" : per},
      "tbxDailyTransactionLimitValue":{"text" : daily},
      "tbxWeeklyTransactionLimitValue":{"text" : weekly},
      "minLimit": min,
      "maxLimit": maxTansactionLimit,
      "maxDailyLimit" : maxDailyLimit,
      "maxWeeklyLimit" : maxWeeklyLimit,
      "lblPerCurrency":currencySymbol,
      "lblDailyCurrency":currencySymbol,
      "lblWeeklyCurrency":currencySymbol,
      "lblPerMaxIcon":{
        "toolTip":"Maximum Limit: " + currencySymbol + maxTansactionLimit + "\n"+
					"Minimum Limit: " + currencySymbol + min,
        "text" : ""
      },
      "lblDailyMaxIcon":{
        "toolTip":"Maximum Limit: " + currencySymbol + maxDailyLimit + "\n"+
					"Minimum Limit: " + currencySymbol + min,
        "text" : ""
      },
      "lblWeeklyMaxIcon":{
        "toolTip":"Maximum Limit: " + currencySymbol + maxWeeklyLimit + "\n"+
					"Minimum Limit: " + currencySymbol + min,
        "text" : ""
      },
      "actionId" : flag ? data.actions[0].id : data.actions.actionId,
      "featureId" : data.id
    };
  },
  assignLimitValidations : function(){
    var perTransaction = false, dailyTransaction = false, weeklyTransaction = false;
    var data = this.view.segAssignLimits.data;
    var decimalsOnly = /^([0-9]*[.])?[0-9]+$/;
    if(data.length>0){
      for(var i=0;i<data.length;i++){

        // for per transaction
        if(decimalsOnly.test(data[i].tbxPerTransactionLimitValue.text) === false){
          data[i].flxPerTransactionLimitError.isVisible = true;
          data[i].lblPerTransactionLimitError.text = "Enter valid Limit";
          perTransaction = false;
          break;
        }else{
          data[i].lblPerTransactionLimitError.text = "";
          data[i].flxPerTransactionLimitError.isVisible = false;
          perTransaction = true;
        }
        if(parseFloat(data[i].tbxPerTransactionLimitValue.text) > parseFloat(data[i].maxLimit)){
          data[i].flxPerTransactionLimitError.isVisible = true;
          data[i].lblPerTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " $" + data[i].maxLimit;
          perTransaction = false;
          break;
        }else if (parseFloat(data[i].tbxPerTransactionLimitValue.text) < parseFloat(data[i].minLimit)){
          data[i].lblPerTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " $" + data[i].minLimit;
          data[i].flxPerTransactionLimitError.isVisible = true;
          perTransaction = false;
          break;
        }else if(data[i].tbxPerTransactionLimitValue.text === ""){
          data[i].lblPerTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.perLimitEmpty");
          data[i].flxPerTransactionLimitError.isVisible = true;
          perTransaction = false;
          break;
        }else{
          data[i].lblPerTransactionLimitError.text = "";
          data[i].flxPerTransactionLimitError.isVisible = false;
          perTransaction = true;
        }

        // for daily limit
        if(decimalsOnly.test(data[i].tbxDailyTransactionLimitValue.text) === false){
          data[i].flxDailyTransactionLimitError.isVisible = true;
          data[i].lblDailyTransactionLimitError.text = "Enter valid Limit";
          perTransaction = false;
          break;
        }else{
          data[i].lblDailyTransactionLimitError.text = "";
          data[i].flxDailyTransactionLimitError.isVisible = false;
          dailyTransaction = true;
        }  
        if(data[i].tbxDailyTransactionLimitValue.text === ""){
          data[i].lblDailyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.DailyLimitEmpty");
          data[i].flxDailyTransactionLimitError.isVisible = true;
          dailyTransaction = false;
          break;
        }else if(parseFloat(data[i].tbxDailyTransactionLimitValue.text) > parseFloat(data[i].maxDailyLimit)){
          data[i].lblDailyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " $" + data[i].maxDailyLimit;
          data[i].flxDailyTransactionLimitError.isVisible = true;
          dailyTransaction = false;
          break;        
        }else if(parseFloat(data[i].tbxDailyTransactionLimitValue.text) < parseFloat(data[i].minLimit)){
          data[i].lblDailyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " $" + data[i].minLimit;
          data[i].flxDailyTransactionLimitError.isVisible = true;
          dailyTransaction = false;
          break;        
        }else if(parseFloat(data[i].tbxDailyTransactionLimitValue.text) < parseFloat(data[i].tbxPerTransactionLimitValue.text)){
          data[i].lblDailyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ValueCannotBeLessThanPerTransactionLimit");
          data[i].flxDailyTransactionLimitError.isVisible = true;
          perTransaction = false;
          break;
        }else{
          data[i].lblDailyTransactionLimitError.text = "";
          data[i].flxDailyTransactionLimitError.isVisible = false;
          dailyTransaction = true;
        }     

        //for weekly limit
        if(decimalsOnly.test(data[i].tbxWeeklyTransactionLimitValue.text) === false){
          data[i].flxWeeklyTransactionLimitError.isVisible = true;
          data[i].lblWeeklyTransactionLimitError.text = "Enter valid Limit";
          perTransaction = false;
          break;
        }else{
          data[i].lblWeeklyTransactionLimitError.text = "";
          data[i].flxWeeklyTransactionLimitError.isVisible = false;
          weeklyTransaction = true;
        }  
        if(data[i].tbxWeeklyTransactionLimitValue.text === ""){
          data[i].lblWeeklyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.WeeklyLimitEmpty");
          data[i].flxWeeklyTransactionLimitError.isVisible = true;
          weeklyTransaction = false;
          break;
        }else if(parseFloat(data[i].tbxWeeklyTransactionLimitValue.text) > parseFloat(data[i].maxWeeklyLimit)){
          data[i].lblWeeklyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_more_than") + " $" + data[i].maxWeeklyLimit;
          data[i].flxWeeklyTransactionLimitError.isVisible = true;
          dailyTransaction = false;
          break;        
        }else if (parseFloat(data[i].tbxWeeklyTransactionLimitValue.text) < parseFloat(data[i].minLimit)){
          data[i].lblWeeklyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmMFAConfigurations.Value_cannot_be_less_than") + " $" + data[i].minLimit;
          data[i].flxWeeklyTransactionLimitError.isVisible = true;
          perTransaction = false;
          break;
        }else if (parseFloat(data[i].tbxWeeklyTransactionLimitValue.text) < parseFloat(data[i].tbxDailyTransactionLimitValue.text)){
          data[i].lblWeeklyTransactionLimitError.text = kony.i18n.getLocalizedString("i18n.frmGroupsController.ValueCannotBeLessThanDailyLimit");
          data[i].flxWeeklyTransactionLimitError.isVisible = true;
          weeklyTransaction = false;
          break;
        }else{
          data[i].lblWeeklyTransactionLimitError.text = "";
          data[i].flxWeeklyTransactionLimitError.isVisible = false;
          weeklyTransaction = true;
        }       
      }
      this.view.segAssignLimits.setData(data);
      this.view.forceLayout();
      return (perTransaction && dailyTransaction && weeklyTransaction);
    }else{
      return true;
    }
  },
  accountUnlink : function(){
    var self = this;
    var success = function(res){
      kony.print(res);
      self.view.flxUnlinkAccount.setVisibility(false);
      self.getCompanyAccounts(self.completeCompanyDetails.CompanyContext[0].id,self.setCompanyAccounts);
    };
    var error = function(res){
      kony.print("error while unlinking the accounts",res);
    };
    this.presenter.unlinkAccounts(self.accountsUnlinkPayload,success,error);
  },
  onSerchSegRowClick : function(){
    var rowData = this.view.segSearchResults.selectedRowItems[0];
    this.showCreatedCompanyDetails(rowData.contractId, 1);
    this.view.flxAccountSegment.setVisibility(true);
    this.view.flxCompanyAccountsDetail.setVisibility(false);
    this.view.settingsMenu.setVisibility(false);
  },
  changeSubTabSkin : function(list,widget){
    for(var i = 0 ;i< list.length;i++){
      list[i].skin = "sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
    }
    widget.skin = "sknbtnBgffffffLato485c75Radius3Px12Px";
  },
  assingText : function(segment,textBox){
    var selectedRow = segment.data[segment.selectedRowIndex[1]];
    textBox.text =  selectedRow.lblAddress.text;
    textBox.info.isValid = true;
    textBox.info.data = selectedRow;
    segment.setVisibility(false);
    this.view.flxCountry.zIndex = 1;
    this.view.forceLayout();
  },
  showAccounts : function(){
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(false);
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName1);
    this.hideAllTabsDetails();
    this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
    // this.setDataToAccountsStatusFilter();    
    this.createDynamicFlexForContract(this.completeContractDetails.contractCustomers , 'Accounts');
    
    this.view.forceLayout();
  },  
   /*
   * Created by :kaushik mesala
   * function to set the header to the segment which will only be visible in case of accounts tab
   * @param: segment to which the header is added
   */
  getHeaderDataForContractAccts : function(accountsFeaturesCard , segment, component_id){
    return {
        'template' : 'flxContractsAccountHeaderView',
        'isAcctHeaderRow':'true',
        'fontIconAccNumberSort' : {
          'text' :'\ue92b',
          "onClick":function(){
            this.sortSegForContractsByColName(accountsFeaturesCard ,  segment,  'lblAccountNumber');
          }.bind(this)
        },
        'lblAccountNameHeader':  kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME"),
        'lblAccountNumberHeader':  kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNUMBER"),
        'lblAccountTypeHeader':  kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE"),
        'lblAccHolderNameHeader':  kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP_TYPE"),

        'lblAccountStatusHeader':  kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.STATUS"),
        'fontIconAccNameSort' : {
          'text' :'\ue92b',
          "onClick":function(){
            this.sortSegForContractsByColName(accountsFeaturesCard , segment, 'lbAccountName');
          }.bind(this)
        },
        'lblHeaderSeperator':'-',
        'fontIconAccHoldNameSort': {
          "onClick": function(widget, context) {
              this.setFilterPosition(this.view.ownerShipTypeFilterMenu, 'fontIconAccHoldNameSort', accountsFeaturesCard);
          }.bind(this)
      },
      'fontIconAccTypeFilter': {
          "onClick": function(widget, context) {
              this.setFilterPosition(this.view.acctTypeFilterMenu, 'fontIconAccTypeFilter', accountsFeaturesCard);
          }.bind(this)
      },
      'fontIconFilterStatus': {
          "onClick": function(widget, context) {
              this.setFilterPosition(this.view.statusTypeFilterMenu, 'fontIconFilterStatus', accountsFeaturesCard);
          }.bind(this)
      }
    };
  },
  iterate: function(obj) {
    if (obj.hasOwnProperty('parent')) {
        return obj.frame.y + this.iterate(obj.parent);
    } else {
        return 0;
    }
  },
  horIterate: function(obj) {
    if (obj.hasOwnProperty('parent')) {
        return obj.frame.x + this.horIterate(obj.parent);
    } else {
        return 0;
    }
  },
  resetFilterWidgetsForAcct : function(){
    // reset all widgets
    let wid = this.view.flxViewContractFilter.widgets();
    for (let i = 0; i < wid.length; i++) {
        wid[i].isVisible = false;
    }
  },
  setFilterPosition: function(filterComponent, widget , accountsFeaturesCard) {
    
    let check = filterComponent.isVisible;
    // we save the value and reset all the components of filtering
    this.resetFilterWidgetsForAcct();
    filterComponent.isVisible = !check;
    this.view.flxViewContractFilter.isVisible = !check;
    
    if(check){
      // we're turning off the popup and returning from function
      return;
    }
     // the below force layout will adjust the up arrow position which will be used in finding difference
     this.view.flxViewContractFilter.left='0dp';
     this.view.flxViewContractFilter.top='0dp';
     this.view.forceLayout();
 
     
     // 3 positions based on widget
     let e = document.getElementById('flxContractsAccountHeaderView_'+widget);
     // Header icon
     var rect = e.getBoundingClientRect(); 
     console.log(rect.top, rect.right, rect.bottom, rect.left);
  
     // getting the arrow position of the component should be around 0 px
     var rect2  = document.getElementById('frmCompanies_'+filterComponent.id+'_imgUpArrow').getBoundingClientRect();
     var diff = rect.left - rect2.left;
     
     this.view.flxViewContractFilter.left = diff + 'px';
     var diff = rect.top - rect2.top;
    this.view.flxViewContractFilter.top = diff+7 + "px";
    this.view.forceLayout();
  },
   /*
   * Created by :kaushik mesala
   * function to sort the segment data by column name
   * @param: segment to which the header is added
   * @param: column name on which  sorting takes place
   */
  sortSegForContractsByColName:function(accountsFeaturesCard ,segment , sortColumn){
      let sectionData = segment.data[0];
      let secData = this.paginationDetails.currSegContractData[1];
      // if secData is empty or zero size
        if(!secData || secData.length <=1){
          return;
        }
      var scopeObj = this;  
      var sortOrder = (scopeObj.sortBy && sortColumn === scopeObj.sortBy.columnName) ? !scopeObj.sortBy.inAscendingOrder : true;  
      scopeObj.sortBy = scopeObj.getObjectSorter(sortColumn);
      scopeObj.sortBy.inAscendingOrder = sortOrder;
      scopeObj.getObjectSorter().column(sortColumn);
      var sortedData = secData.sort(scopeObj.sortBy.sortData);
      
      scopeObj.determineSortFontIcon(scopeObj.sortBy,'lbAccountName',sectionData[0]['fontIconAccNameSort']);
      scopeObj.determineSortFontIcon(scopeObj.sortBy, 'lblAccountNumber',sectionData[0]['fontIconAccNumberSort']);
      
      this.paginationDetails.currSegContractData[1] = sortedData;
      sectionData[1] = sortedData.slice(0,10);
      segment.data[0] = sectionData;
      // resetting the pagination labels
      
    accountsFeaturesCard.reportPagination.lblNumber.text = "1";
    accountsFeaturesCard.reportPagination.lblShowing.text = "Showing 1 - 10 Of " + this.paginationDetails.currSegContractData[1].length;

    segment.setData(segment.data);
    this.view.forceLayout();
  }, 
/*
* set the collapse arrow images based on visibility
*/
  toggleCollapseArrowForSeg : function(component , segInd , tabName , resData){
      let segment = component.segAccountFeatures;
      let rowData = segment.data[segInd];
      let arr = [
        {
          "template": "flxContractsFAHeaderView"
        }
      ];  
      let  check = false;

      if (tabName == 'Limits') {
        check = rowData[0]["flxViewLimitsHeader"]["isVisible"];
        
        // check indicates is the segment row is collapse or not
        // if its collapsed than we push the data to this section
      
        if(!check){          
          arr = this.getRowDataForLimits(resData);
        }          
      } else {  
          // for actions tab updating the data
        check = rowData[0]["flxViewActionHeader"]["isVisible"];
        if(!check){
          arr = this.getRowDataForFeatures(resData);
        }
      }
      // for features we have a bottom gap of 20dp
      if(arr.length > 0 && arr[arr.length-1]["flxViewActionBody"]){
        arr[arr.length-1]["flxViewActionBody"]={"left":"57dp","bottom":"20dp"};
      }
      rowData[1] = arr;

      // updating the current index  to expand
      this.resetSectionsForViewContracts(rowData , tabName);  
      
      // The previous row will collapse only if it's not matching current row 
      if (this.prevContractSelected.segRowNo != segInd) {
          this.collapseSegmentSection(segment, tabName);
          this.prevContractSelected.segRowNo = segInd;
      }
      
      // if the section is collapse we reset the previous tab
      if(check) {
          this.prevContractSelected.segRowNo = -1;
      }
      
      segment.setSectionAt(rowData, segInd);
  },
  getRowDataForFeatures: function(feature){
     var self = this;
     // below filter is to show only selected actions
     let filteredActions = feature.actions.filter( function(action) {
      return action.isEnabled !== 'false'
    });
    return 	filteredActions.map(function(action) {
      return {
          "flxViewActionBody":{"left":"57dp"},
          "lblActionName": action.actionName,
          "template": "flxContractsFABodyView",
          "lblFASeperator3": ".",
          "lblActionDesc": action.actionDescription,
          'statusValue' :action.actionStatus == 'SID_ACTION_ACTIVE' ? "Active" : "InActive",
          "statusIcon": {
                'text':'',
                "skin": action.actionStatus == 'SID_ACTION_ACTIVE' ? "sknFontIconActivate" : "sknfontIconInactive"
          }
        }
		}); 
  },
  getObjectFromArrayOfObjects : function(arr){
    ob = {};
    arr.map(function(ele){
      ob[ele.id] = ele.value;
    });
    return ob;
  },
  getRowDataForLimits: function(limit){
    var self = this;
    let filteredActions = limit.actions.filter( function(action) {
      return action.isEnabled !== 'false'
    });
    return 	filteredActions.map(function(action) {
      let limitOb = self.getObjectFromArrayOfObjects(action.limits);
      return {
			  "flxViewLimits":{"left":"57dp"},
			  "lblAction": action.actionName,
              "lblCurrencyPer": self.defaultCurrencyCodeSymbol,
          	  "lblCurrencyDaily": self.defaultCurrencyCodeSymbol,
        	  "lblCurrencyWeekly": self.defaultCurrencyCodeSymbol,
			  "lblPerTransactionLimit": limitOb['MAX_TRANSACTION_LIMIT'] ? limitOb['MAX_TRANSACTION_LIMIT'] :"",
			  "template": "flxContractsLimitsBodyView",
			  "lblDailyTransactionLimit": limitOb['DAILY_LIMIT'] ? limitOb['DAILY_LIMIT'] :"",
        	  "lblLimitsSeperator":".",
        	  "lblFASeperator1":{"isVisible" : false},
			  "lblWeeklyTransactionLimit": limitOb['WEEKLY_LIMIT'] ? limitOb['WEEKLY_LIMIT'] :""
      }
		}); 
  },
  setDataToContractFetaures: function(accountsFeaturesCard, features) {
    var self = this;
    accountsFeaturesCard.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CompanyFeatures");
    let segData = [];    
    let segInd = -1;
    // removing the features whose action is not enabled
    features = features.filter(function (feature) { 
      return feature.actions.filter( function(action) {
           return action.isEnabled !== 'false'
          }).length != 0 ; 
    });

    accountsFeaturesCard.lblCount.text = features.length <10 ? "(0"+ features.length  +")" : "("+ features.length  +")";
    
    accountsFeaturesCard.flxNoFilterResults.setVisibility(false);
    if(features.length  == 0){
      accountsFeaturesCard.flxNoFilterResults.setVisibility(true);
      accountsFeaturesCard.flxCardBottomContainer.height='75dp';
      accountsFeaturesCard.lblNoFilterResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.noFeaturesMsg"); 
    }
    segData = features.map(function(feature) {
        segInd += 1;
        return [{
            "flxViewActionHeader": {
                "isVisible": false,
                "left": "57dp"
            },
            /////////////////////////////////////////////////////////
            'flxActionDetails': {
                "left": "27dp"
            },
            'flxHeader': {
                "left": "30dp"
            },
            /////////////////////////////////////////////////////////
            "lblActionDesc": feature.featureDescription ? feature.featureDescription : "" ,
            "lblActionDescHeader": kony.i18n.getLocalizedString("i18n.frmTrackApplication.DescriptionCAPS"),
            "lblActionStatusHeader": kony.i18n.getLocalizedString("i18n.permission.STATUS"),
            "lblActionHeader": kony.i18n.getLocalizedString("i18n.frmMfascenarios.ACTION_CAP"),
            "lblActionName": feature.featureName ? feature.featureName :"",
            "lblArrow": "\ue922", // side arrow
            "lblAvailableActions": "Selected Actions:",
            "lblCountActions": feature.actions.filter(function(action) {return action.isEnabled !== 'false'}).length,
            "lblFASeperator2": ".",
            "lblFASeperator1": {
                "text": ".",
                "isVisible": false
            },
            "statusValue": "Active",
            "template": "flxContractsFAHeaderView",
            "lblFASeperator3": ".",
            "lblFeatureName": feature.featureName,
            "lblTotalActions": "of "+feature.actions.length,
            "statusIcon": {
                "skin": "sknFontIconActivate",
                "text": kony.i18n.getLocalizedString("i18n.frmAlertsManagement.lblViewAlertStatusKey")
            },
            "flxArrow": {
                "isVisible": true,
                "onClick": self.toggleCollapseArrowForSeg.bind(self, accountsFeaturesCard, segInd, 'Features' , feature)
            }
        },   
        [
          {
            "template": "flxContractsFAHeaderView"
          }
        ]
      ];
    });
    // to avoid double line overlap for first row
    if(segData.length > 0 && segData[0][0]){
      segData[0][0]["lblFASeperator3"]='';
    }
    accountsFeaturesCard.segAccountFeatures.setData(segData);     
  },
  setTemplateToAllRowsInSegment: function(segData , template){
    let len = segData.length;
    for( let i=0;i<len;i++){
      segData[i].template = template;
    }
  },
  setDataToContractLimits:function(accountsFeaturesCard, limits){
    
    var self = this;
    
    accountsFeaturesCard.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmServiceManagement.Limits");
    accountsFeaturesCard.lblCount.text = "";
    
    let segData = "";
    let segInd = -1;
    // removing the limits whose action is not enabled
    limits = limits.filter(function (limit) { 
      return limit.actions.filter( function(action) {
           return action.isEnabled !== 'false'
          }).length != 0 ; 
    });
    
    accountsFeaturesCard.flxNoFilterResults.setVisibility(false);
    if(limits.length == 0){
      accountsFeaturesCard.flxNoFilterResults.setVisibility(true);
      accountsFeaturesCard.flxCardBottomContainer.height='75dp';
      accountsFeaturesCard.lblNoFilterResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.noLimitsMsg"); 
    }
    segData = limits.map( function(limit){
      segInd = segInd +1;
      return  [
        {  
          /////////////////////////////////////////////////////////
          'flxActionDetails' :{"left" :"27dp"},
          'flxHeader' :{"left" :"30dp"},
          'flxViewLimitsHeader' :{
                "isVisible": false , 
                "left" :"57dp"
              },
          /////////////////////////////////////////////////////////
          "lblActionDesc": limit.featureDescription,
          "lblFASeperator1": {
            "text": ".",
            "isVisible": false
          },
          "lblLimitsSeperator3": ".",
          'fontIconInfo1':'',
          "flxLimitInfo1":{"onHover" : function(widget, context) {
              let info = "This is daily transaction value for " + limit.featureName + " submitted over the online banking channel.";
              self.onHoverCallBack(widget, context,info , true , accountsFeaturesCard);
            }
          },
          "flxLimitInfo2":{"onHover" : function(widget, context) {
              let info = "This is daily transaction value for " + limit.featureName + " submitted over the online banking channel.";
              self.onHoverCallBack(widget, context,info, true , accountsFeaturesCard);
            }
          },
          "flxLimitInfo3":{"onHover" : function(widget, context) {
              let info = "This is daily transaction value for " + limit.featureName + " submitted over the online banking channel.";
              self.onHoverCallBack(widget, context,info, true , accountsFeaturesCard);
            }
          },
          'fontIconInfo2':'',
          'fontIconInfo3':'',
          "lblActionName": limit.featureName,          
          "lblActionHeader": kony.i18n.getLocalizedString("i18n.frmMfascenarios.ACTION_CAP"),
          "lblPerLimitHeader": "PER TRANSACTION",
          "lblDailyLimitHeader": "DAILY TRANSACTION",
    	    "lblWeeklyLimitHeader":"WEEKLY TRANSACTION",
          "lblAvailableActions": "Monetary Actions:",

          "lblCountActions":{
            'text':limit.actions.length - limit.actions.filter( function(action) {
                    return action.isEnabled === 'false'
                  }).length,
            'isVisible' : true
          },
          'template':'flxContractsLimitsHeaderView',
          'statusValue' : limit.featureStatus == 'SID_FEATURE_ACTIVE' ?'Active':'InActive',
          'statusIcon' : {
          "text" :  kony.i18n.getLocalizedString("i18n.frmAlertsManagement.lblViewAlertStatusKey"),
            "skin": "sknFontIconActivate"
          },
          'flxViewLimits':{"left" :"57dp"},
          "lblArrow": "\ue922", // side arrow
          "flxArrow": {
            "isVisible":true,
              "onClick": self.toggleCollapseArrowForSeg.bind(self, accountsFeaturesCard, segInd , 'Limits' , limit)
          }
        },[
          {
            "template": "flxContractsFAHeaderView"
          }
        ]];
      });
      
      accountsFeaturesCard.segAccountFeatures.setData(segData);    
  },

  showFeatures : function(){
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(false);
      this.tabUtilLabelFunction([ this.view.lblTabName1, this.view.lblTabName2, this.view.lblTabName3,
        this.view.lblTabName4, this.view.lblTabName5,this.view.lblTabName6
    ], this.view.lblTabName2);
    this.suspendFeatureHideShow("mandatory_feature_screen");
    this.hideAllTabsDetails();
    this.presenter.getContractFeatureActionLimits(this.recentContractDetails.contractId , 'Features');
    this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
    this.view.forceLayout();
  },
  showCustomers : function(){
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName4);
    this.hideAllTabsDetails();
    this.view.flxCompanyDetailaCustomerContainer.setVisibility(true);
    this.inAuthorizedSignatoriesUi=true;
    this.resetBusinessUsers(true);
  },
  showLimits : function(){
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(false);
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName3);
    this.hideAllTabsDetails();
    this.presenter.getContractFeatureActionLimits(this.recentContractDetails.contractId , 'Limits');
    this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
    this.view.forceLayout();
  },
  showSignatories: function(){
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(false);
    this.tabUtilLabelFunction([ this.view.lblTabName1, this.view.lblTabName2, this.view.lblTabName3,
      this.view.lblTabName4, this.view.lblTabName5,this.view.lblTabName6], this.view.lblTabName4);
    this.hideAllTabsDetails();
    this.presenter.getContractInfinityUsers(this.recentContractDetails.contractId);
	
    this.view.flxCompanyDetailAccountsContainer.setVisibility(false);
    this.view.flxCompanyDetailaCustomerContainer.setVisibility(true);
    this.view.flxBusinessUsersTabs.setVisibility(false);
    this.view.flxAddCustomers.setVisibility(false);
  },
  setSignatories : function(){
    let customers = this.completeCompanyDetails.signatoryUsers;
    if(customers && customers.length !=0 ){
      
        this.view.flxAddSignatories.setVisibility(false);
        // flxUsersContent has segment to populate data
        this.setSignatoriesContent(customers);
        this.view.flxUsersContentSignatories.setVisibility(true); 
    }else{
        this.view.flxAddSignatories.setVisibility(true);
        // flxUsersContent has segment to populate data
        this.view.flxUsersContentSignatories.setVisibility(false);
    }
    var scopeObj = this;
    this.view.btnCreateUser.onClick = function(){
      scopeObj.visbiltyChangesForAddUserButton();
    };
    this.view.forceLayout();
  },
  sortSignatories:function(sortColumn){
    var scopeObj  = this;
    var sortOrder = (scopeObj.sortBy && sortColumn === scopeObj.sortBy.columnName) ? !scopeObj.sortBy.inAscendingOrder : true;  
    scopeObj.sortBy = scopeObj.getObjectSorter(sortColumn);
    scopeObj.sortBy.inAscendingOrder = sortOrder;
    scopeObj.getObjectSorter().column(sortColumn);
    let sectionData = this.view.segSignatoriesDetail.data;
    var sortedData = sectionData.sort(scopeObj.sortBy.sortData);
    
    scopeObj.view.lblSignatoryRoleSortIcon.text = scopeObj.returnSortFontIconValue(scopeObj.sortBy,'lblRole');
    scopeObj.view.lblSignatoriesNameSortIcon.text = scopeObj.returnSortFontIconValue(scopeObj.sortBy, 'lblName.text');
    scopeObj.view.lblSignatoryUserNameSortIcon.text = scopeObj.returnSortFontIconValue(scopeObj.sortBy, 'lblUsername');
    scopeObj.view.lblSignCustomerEmailIDSortIcon.text = scopeObj.returnSortFontIconValue(scopeObj.sortBy, 'lblEmail.text');
    scopeObj.view.lblSignCustomerStatusSortIcon.text = scopeObj.returnSortFontIconValue(scopeObj.sortBy, 'lblCustomerStatus');

    scopeObj.view.segSignatoriesDetail.setData(sortedData);
    scopeObj.view.forceLayout();
  },
  setSignatoriesContent :function(customers){
     
      let finalData = customers.map(this.mappingSignatoryAccounts);
      let widgetDataMap = {
        "flxCompanyCustomer": "flxCompanyCustomer",
        "flxContainer": "flxContainer",
        "flxCustomerStatus": "flxCustomerStatus",
        "flxOptions": "flxOptions",
        "lblCustomerStatus": "lblCustomerStatus",
        "lblEmail": "lblEmail",
        "lblFontIconOptions": "lblFontIconOptions",
        "lblIconStatus": "lblIconStatus",
        "lblName": "lblName",
        "lblRole": "lblRole",
        "lblSepartor": "lblSepartor",
        "lblUsername": "lblUsername"
       };
      this.view.segSignatoriesDetail.widgetDataMap = widgetDataMap;
      this.view.segSignatoriesDetail.setData(finalData);
    var scopeObj = this;
    /*business user header*/
    this.view.flxSignatoriesName.onClick = function(){
      scopeObj.sortSignatories("lblName.text");
    };
    this.view.flxSignatoryRole.onClick = function(){
      scopeObj.sortSignatories("lblRole");
    };
    this.view.flxSignatoryUserName.onClick = function(){
      scopeObj.sortSignatories("lblUsername");
    };
    this.view.flxSignCustEmail.onClick = function(){
      scopeObj.sortSignatories("lblEmail.text");
    };
    this.view.flxSignCustomerStatus.onClick = function(){
      scopeObj.sortSignatories("lblCustomerStatus");
    };
  },
  getName : function(data){
    if (!data.firstName){
        return  kony.i18n.getLocalizedString("i18n.Applications.NA");
    }
    if (data.middleName)
        return data.firstName +" "+ data.middleName+" "+ data.lastName;
    else
        return data.firstName +" "+ (data.lastName || "");
  },
  mappingSignatoryAccounts: function(data) {
    var self = this;
    let statusText =  kony.i18n.getLocalizedString("i18n.frmPermissionsController.InActive");
    let iconSkin = "sknfontIconInactive";

    if(data.statusId  === "SID_CUS_ACTIVE"){
      iconSkin = "sknFontIconActivate";
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Active");
    }else if(data.statusId  === "SID_CUS_NEW"){
      iconSkin = "sknIcon039dffBlue12px";
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.New");
    }

    return {
      'customerId': data.customerId ,
        "lblRole": data.primaryCoreCustomerId ? data.primaryCoreCustomerId : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblEmail": data.Email ? {"text": self.AdminConsoleCommonUtils.getTruncatedString(data.Email, 19, 17),
                          "tooltip":data.Email} : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblUsername": data.userName ? data.userName : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblName": {"text":self.getName(data).trim(),
                    "onClick" : self.onClickSignUserNavigateToProfile },
      "lblSeperator": ".",
      "lblIconStatus": {
        "text":"\ue921",
        "skin": iconSkin
      },
      "lblCustomerStatus": {
        "text": statusText,
        "skin": "sknlblLatoReg485c7513px"
      },  
      "template": "flxCompanyCustomer"
    };
  },
  showApprovalMatrixTab : function(){
    var data = this.completeCompanyDetails.approvalsContext;
    var show = data ? (data.length > 0 ? true : false) : false ;
    this.view.flxViewTab5.setVisibility(show);
    this.view.forceLayout();
  },
  showApprovalMatrix : function(){
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName5);
    this.hideAllTabsDetails();
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(true);
    
    this.presenter.getContractApprovalMatrix(this.recentContractDetails.contractId);
    this.view.forceLayout();
  },
  getCompanyAllDetails : function(id,tabNum){
    
    // var payLoad = {
    //   "contractId" : id, 
    //   "selectTab": tabNum
    // };
    this.recentContractDetails.contractId = id;
    this.presenter.getContractInformation(id);
  },
  getCompanyAccounts : function(id,success){
    var self = this;
    var accountsSuccess = function(accountContext){
      self.completeCompanyDetails.accountContext = accountContext.accountContext;
      success();
    };
    var payLoad = {
      "Organization_id" : id
    };
    self.presenter.getCompanyAccounts(payLoad,accountsSuccess,self.createdCompanyDetailError);
  },
  getCompanyCustomers : function(id,success){
    var self = this;
    var accountsSuccess = function(customerContext){
      self.completeCompanyDetails.customerContext = customerContext.customerContext;
      success();
    };
    var payLoad = {
      "Organization_id" : id
    };
    self.presenter.getCompanyCustomers(payLoad,accountsSuccess,self.createdCompanyDetailError);
  },
  createdCompanyDetailError : function(error){
    kony.print("getCompanyAccounts(form controller)not able to fetch company details",error);
  },

  getCompleteDataForCreatedCompany : function(context){
    var self = this;
    var detailSuccess = function(){
      self.setCompanyDetails();
      self.setCompanyAccounts();
      // self.setCompanyFeatures();
      // self.setCompanyLimits();
      self.showApprovalMatrixTab();
    };
    self.completeCompanyDetails.CompanyContext = context.CompanyContext ? context.CompanyContext : [];
    self.completeCompanyDetails.OwnerContext = context.OwnerContext ? context.OwnerContext : [];
    self.completeCompanyDetails.CompanyType = context.CompanyContext ? context.CompanyContext[0].TypeId : "";
    self.completeCompanyDetails.contractDetails = context.contractDetails ? context.accountContext : [];
    self.completeCompanyDetails.customerContext = context.customerContext ? context.customerContext : [];
    self.completeCompanyDetails.featuresContext = context.featuresContext ? context.featuresContext : [];
    self.completeCompanyDetails.approvalsContext = context.approvalsContext ? context.approvalsContext : [];
    self.featureFilter();
    detailSuccess();
  },
  getCompleteDataForcontractDetails: function(context){    
    var self = this;
    
    self.completeContractDetails = context.contractDetails ? context.contractDetails : [];    
    self.completeCompanyDetails.customerContext = context.customerContext ? context.customerContext : [];
    self.view.contractDetailsPopup.flxBackOption.onClick = function(){
      self.view.flxContractDetailsPopup.setVisibility(false);
    };
    self.setViewContractDetails();
  },
  showCreatedCompanyDetails : function(id){
    this.getCompanyAllDetails(id,1);
  },
  detailScreenVisibilties : function(){
    this.view.flxCompanyDetailAccountsContainer.setVisibility(true);
    
//     this.view.flxAccountSegment.setVisibility(true);
    
    this.view.flxCompanyAccountsDetail.setVisibility(false);
    this.view.backToAccounts.setVisibility(true);
    this.view.AccountsHeader.setVisibility(true);
    this.view.flxAccountsTabs.setVisibility(true);
    this.view.flxTransactionContainer.setVisibility(true);
    this.view.flxProductDetailContainer.setVisibility(false);
    this.view.flxCompanyDetailaCustomerContainer.setVisibility(false);
  },
  setViewContractDetails : function(){
    this.completeCompanyDetails.accountsFeatures=null;
    var companyDetails = this.completeContractDetails['address']?this.completeContractDetails['address'][0]:{};
    this.detailScreenVisibilties();
    var contractName = this.completeContractDetails.name;
    this.view.breadcrumbs.lblCurrentScreen.text = contractName ? (contractName).toUpperCase() : "CONTRACT";
    this.view.lblTypeValue.text = this.completeContractDetails.serviceType === "TYPE_ID_BUSINESS" ? "Business Banking" :this.completeContractDetails.serviceType==="TYPE_ID_WEALTH"?"Wealth Banking": "Retail Banking";

    this.view.lblCompanyDetailName.text = this.AdminConsoleCommonUtils.getParamValueOrNA(contractName);
    this.view.flxFlagBackground.skin = "sknflxCustomertagPurple";
    var addr = kony.i18n.getLocalizedString("i18n.Applications.NA");
    if(companyDetails && companyDetails.addressLine1 && companyDetails.addressLine2){
      addr = companyDetails.addressLine1+" "+ companyDetails.addressLine2;
    } else if(companyDetails && (companyDetails.addressLine1 || companyDetails.addressLine2)){
      addr = companyDetails.addressLine1 !== "" ? companyDetails.addressLine1 : companyDetails.addressLine2;
    }
    
    this.view.lblDetailsValue11.text = this.AdminConsoleCommonUtils.getParamValueOrNA(this.completeContractDetails.id);
    this.view.lblDetailsValue12.text = this.completeContractDetails.serviceType === "TYPE_ID_BUSINESS" ? "Business Banking" :this.completeContractDetails.serviceType==="TYPE_ID_WEALTH"?"Wealth Banking": "Retail Banking";
    

    let communication =this.completeContractDetails["communication"][0];
    this.view.lblContactDetailsValue11.text = this.AdminConsoleCommonUtils.getParamValueOrNA(communication.phoneNumber);
    this.view.lblContactDetailsValue12.text = this.AdminConsoleCommonUtils.getParamValueOrNA(communication.email);
    this.view.lblContactDetailsValue13.text = addr;

    // this.view.lblContactDetailsValue21.text = addr;
    // this.view.lblContactDetailsValue22.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.cityName);
    // this.view.lblContactDetailsValue23.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.state); 

    this.view.lblContactDetailsValue31.text =  this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails ? companyDetails.country :companyDetails);
    this.view.lblContactDetailsValue32.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails ? companyDetails.zipCode:companyDetails);
    this.view.flxCompanyDetailsContainer.setVisibility(true);
    this.view.lblArrowIcon.text= "\ue922";
    this.view.flxCompanyContactDetails.setVisibility(false);
    //set taxId field
    
    this.view.lblDetailsValue13.text = this.AdminConsoleCommonUtils.getParamValueOrNA(this.completeContractDetails.servicedefinitionName);
    this.setContractPopupHeadings();
    this.view.forceLayout();
  },
  setContractPopupHeadings:function(){
    this.view.contractDetailsPopup.lblDetailsHeading.text = kony.i18n.getLocalizedString("i18n.frmCompanies.CUSTOMER_DETAILS");

    this.view.contractDetailsPopup.detailsRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.CustomerName_UC");
    this.view.contractDetailsPopup.detailsRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID");
    this.view.contractDetailsPopup.detailsRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCompanies.INDUSTRY");

    this.view.contractDetailsPopup.detailsRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.View.EMAILID_UC");
    this.view.contractDetailsPopup.detailsRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerCare.lblPhoneNumber");

    this.view.contractDetailsPopup.detailsRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.View.ADDRESS");
    
    this.view.contractDetailsPopup.detailsRow2.flxColumn3.setVisibility(false);
    this.view.contractDetailsPopup.detailsRow3.flxColumn2.setVisibility(false);
    this.view.contractDetailsPopup.detailsRow3.flxColumn3.setVisibility(false);
  },
  setCompanyDetails : function(){
    var companyDetails = this.completeCompanyDetails.CompanyContext[0];
    this.detailScreenVisibilties();
    var companyName = this.completeCompanyDetails.CompanyContext[0].Name;
    this.view.breadcrumbs.lblCurrentScreen.text = companyName ? (companyName).toUpperCase() : "COMPANY";
    this.view.lblTypeValue.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.TypeName);
    this.view.lblCompanyDetailName.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Name);
    this.view.flxFlagBackground.skin = "sknflxCustomertagPurple";
    var addr = kony.i18n.getLocalizedString("i18n.Applications.NA");
    if(companyDetails.addressLine1 && companyDetails.addressLine2){
      addr = companyDetails.addressLine1+" "+ companyDetails.addressLine2;
    } else if(companyDetails.addressLine1 || companyDetails.addressLine2){
      addr = companyDetails.addressLine1 !== "" ? companyDetails.addressLine1 : companyDetails.addressLine2;
    }
    
    this.view.lblDetailsValue11.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.id);
    this.view.lblDetailsValue12.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.businessType);
    
    this.view.lblContactDetailsValue11.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Phone);
    this.view.lblContactDetailsValue12.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.faxId);
    this.view.lblContactDetailsValue13.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.Email);
    this.view.lblContactDetailsValue21.text = addr;
    this.view.lblContactDetailsValue22.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.cityName);
    this.view.lblContactDetailsValue23.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.state); 
    this.view.lblContactDetailsValue31.text =  this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.country);
    this.view.lblContactDetailsValue32.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.zipCode);
    this.view.flxCompanyDetailsContainer.setVisibility(true);
    this.view.lblArrowIcon.text= "\ue922";
    this.view.flxCompanyContactDetails.setVisibility(false);
    //set taxId field
    if(this.isAccountCentricConfig === true){
      this.view.lblDetailsValue13.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.taxId);
    } else{
      this.view.lblDetailsValue13.text = this.AdminConsoleCommonUtils.getParamValueOrNA(companyDetails.taxId);
      if(this.view.lblDetailsValue13.text !== kony.i18n.getLocalizedString("i18n.Applications.NA"))
        this.setDataForTaxIdHoverTooltip(this.view.lblDetailsValue13.text);
    }
    this.view.forceLayout();
  },
  
  /*
   * Created by :kaushik mesala
   * function to filter accounts data based on search text
   * @param: result of filtered data
   */
  filterAccountsDataForSearch: function(searchTxt ){
    let filteredData = JSON.parse(JSON.stringify(this.completeContractDetails.contractCustomers));
    var self =this;
    return filteredData.filter( function(contractCust){
      // search by customer Id &  by customer name
      if (contractCust.coreCustomerName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ||
       contractCust.contractId.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1) {
        return true;
      }      
      // search by account name
      // nested loop for account names
      contractCust.accounts = contractCust.accounts.filter(function(account){
        self.searchResult.isAcctMatched = true;
        return account['accountName'].toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ;
      });
      return contractCust.accounts.length !== 0;
    });
  },
  /*
   * Created by :kaushik mesala
   * function to filter features data based on search text
   * @param: result of filtered data
   */
  filterFeaturesDataForSearch: function(searchTxt ){
    let filteredData =  JSON.parse(JSON.stringify(this.completeCompanyDetails.accountsFeatures.features));
    var self =this;
    return filteredData.filter( function(contractCust){
      
       // search by customer Id &  by customer name
       if (contractCust.coreCustomerName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ||
       contractCust.coreCustomerId.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1) {
        return true;
      }    

      // removing is enabled false
      custFeat = contractCust.contractCustomerFeatures.filter(function(feature) {
        return feature.actions.filter(function(action) {
            return action.isEnabled !== 'false'
        }).length != 0;
      });

      // search by feature name
      contractCust.contractCustomerFeatures = custFeat.filter(function(feature){
        if(feature.featureName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ){
          self.searchResult.isFeatureMatched = true;
          return true;
        }

      // search by Action name
        feature.actions = feature.actions.filter(function(action){
          self.searchResult.isActionMatched = true;
          return action.actionName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 && action.isEnabled !== 'false';
        });
        return feature.actions.length !== 0;
      });
      
      return contractCust.contractCustomerFeatures.length !== 0;
      
    });
  },
  /*
   * Created by :kaushik mesala
   * function to filter limits data based on search text
   * @param: result of filtered data
   */
  filterLimitsDataForSearch: function(searchTxt ){
    let filteredData = JSON.parse(JSON.stringify(this.completeCompanyDetails.accountsFeatures.limits));
    var self =this;
    return filteredData.filter(function(contractCust) {
    
       // search by customer Id &  by customer name
       if (contractCust.coreCustomerName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ||
       contractCust.coreCustomerId.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1) {
        return true;
      }    

      // removing is enabled false
      custFeat = contractCust.contractCustomerLimits.filter(function(limits) {
        return limits.actions.filter(function(action) {
            return action.isEnabled !== 'false'
        }).length != 0;
      });

      // search by feature name
      contractCust.contractCustomerLimits = custFeat.filter(function(limits){
        if(limits.featureName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 ){
          self.searchResult.isLimitMatched = true;
          return true;
        }

      // search by Action name
        limits.actions = limits.actions.filter(function(action){
          self.searchResult.isActionMatched = true;
          return action.actionName.toLowerCase().indexOf(searchTxt.toLowerCase()) !== -1 && action.isEnabled !== 'false';
        });
        return limits.actions.length !== 0;
      });
      
      return contractCust.contractCustomerLimits.length !== 0;
    });
  },
  /*
   * Created by :kaushik mesala
   * function to set response data under account 
   * @param: result of service call to be mapped
   */
  setCompanyAccounts : function(accData){
    var accountContext = accData ? accData : this.completeCompanyDetails.accountContext;
    if(accountContext && accountContext.length > 0){
	/*
   * Commented By :Kaushik Mesala
   * commenting the segment mapping and flex handling as we are not using the below segCompanyDetailAccount for the functionality
   */

      // var finalData = accountContext.map(this.mappingCompanyAccounts);
      // var widgetDataMap = {
      //   "flxCompanyAccounts": "flxCompanyAccounts",
      //   "flxCompanyDetailsAccounts": "flxCompanyDetailsAccounts",
      //   "flxStatus": "flxStatus",
      //   "lblAccountNumber": "lblAccountNumber",
      //   "lblAccountName":"lblAccountName",
      //   "lblAccountType":"lblAccountType",
      //   "lblSeperator": "lblSeperator",
      //   "fontIconStatus": "fontIconStatus",
      //   "lblStatus": "lblStatus",
      //   "flxUnlink" : "flxUnlink",
      //   "flblUnlink" : "flblUnlink",
      //   "flxAccountOwner" : "flxAccountOwner",
      //   "Membership_id":"Membership_id",
      //   "Status_id":"Status_id"
      // };
      // this.view.segCompanyDetailAccount.widgetDataMap = widgetDataMap;
      // if(this.isAccountCentricConfig === false){ //for customer centric
      //   finalData = this.groupAccountsByCIF(finalData,2);
      //   var segData = [].concat.apply([], Object.values(finalData));
      //   this.view.segCompanyDetailAccount.setData(segData);
      //   this.view.flxCustomerIdPart.setVisibility(true);

      //   this.view.flxAccountsSegmentPart.left = "18%";
      //   this.view.flxAccountsSegmentPart.width = "82%";
      //   this.view.flxAccountType.width = "23%";
      //   this.view.flxAccountType.left = "35dp";
      //   this.view.flxAccountName.left = "30%";
      //   this.view.flxAccountName.width = "21.5%";

      //   this.generateCustomerIdRows(finalData);

      // } else{ // for account centric
      //   this.view.flxCustomerIdPart.setVisibility(false);
      //   this.view.flxAccountsSegmentPart.left = "0dp";
      //   this.view.flxAccountsSegmentPart.width = "100%";
      //   this.view.flxAccountType.width = "23%";
      //   this.view.flxAccountType.left = "10dp";
      //   this.view.flxAccountName.left = "27%";
      //   this.view.flxAccountName.width = "24.5%";
      //   this.view.segCompanyDetailAccount.setData(finalData);
      // }
      this.showHideCompanyAccountsData(1);
    } else{
      this.showHideCompanyAccountsData(2);
    }
    
   // this.sortBy = this.getObjectSorter("lblAccountType");
    //this.resetSortImages("accounts");
    //var sortedData = finalData.sort(this.sortBy.sortData);
    this.view.forceLayout();
  },
  mappingCompanyAccounts : function(data){
    var self = this;
    return{
      "flxCompanyDetailsAccounts": {
        "onHover" : self.onHoverAccountsSeg
      },
      "lblAccountNumber": {
        "text" : data.Account_id ? data.Account_id : kony.i18n.getLocalizedString("i18n.Applications.NA")
      },
      "lblAccountType":this.isAccountCentricConfig === true ? 
      {"width":"25%",
       "left":"10dp",
       "text":data.accountType ? data.accountType : kony.i18n.getLocalizedString("i18n.Applications.NA")} :
      {"width":"23%",
       "left":"35dp",
       "text":data.accountType ? data.accountType : kony.i18n.getLocalizedString("i18n.Applications.NA")},
      "lblAccountName":this.isAccountCentricConfig === true ? 
      {"width":"24.5%",
       "left":"27%",
       "text" : data.AccountName ? data.AccountName : kony.i18n.getLocalizedString("i18n.Applications.NA")} :
      {"width":"21.5%",
       "left":"30%",
       "tooltip": data.AccountName,
       "text" : data.AccountName ? self.AdminConsoleCommonUtils.getTruncatedString(data.AccountName,18,16) : kony.i18n.getLocalizedString("i18n.Applications.NA")},
      "lblSeperator": ".",
      "fontIconStatus": {
        "skin": data.StatusDesc?data.StatusDesc.toLowerCase() === "active" ? "sknFontIconActivate" : "sknfontIconInactive":"sknFontIconWhite"
      },
      "lblStatus": {
        "text": data.StatusDesc ? data.StatusDesc : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "skin": data.StatusDesc ? data.StatusDesc.toLowerCase() === "active" ? "sknlblLato5bc06cBold14px" : "sknlblLatocacacaBold12px": "sknlblLato5bc06cBold14px"
      },
      "flxUnlink" : {
        isVisible : false,
      },
      "flblUnlink" : {
        "text" : "\ue974",
        "tooltip": "Unlink"
      },
      "Status_id": data.Status_id || "",
      "Membership_id":data.Membership_id || "",
      "template": "flxCompanyDetailsAccounts"
    };
  },
  generateCustomerIdRows: function(data){
    var custList = Object.keys(data);
    var newRow ="";
    this.view.flxAccCustomerIdColumn.removeAll();
    for(var i=0;i<custList.length;i++){
      var rowCount = data[custList[i]].length;
      newRow = this.view.flxAccountsCustRow.clone("c"+i);
      newRow.isVisible = true;
      var lblname = "c"+i + "lblCustomerId";
      newRow[lblname].text = custList[i];
      newRow.top = "0px";
      newRow.height = (rowCount*45) +"px";
      this.view.flxAccCustomerIdColumn.addAt(newRow,i);
    } 
    this.view.forceLayout();
  },
  /*
  * show accounts list or no results found based on opt
  * @param: opt - 1/2
  */
  showHideCompanyAccountsData : function(opt){
    if(opt === 1){ //show accounts list
      this.view.flxNoAccountResults.setVisibility(false);
      this.view.segCompanyDetailAccount.setVisibility(true);
      if(this.isAccountCentricConfig === false){ //for customer centric
        this.view.flxAccountCustVerticalLine.setVisibility(true);
        this.view.flxAccCustomerIdColumn.setVisibility(true);
        this.view.segCompanyDetailAccount.setVisibility(true);
      }
    } else if(opt === 2){ //show no record found
      this.view.flxNoAccountResults.setVisibility(true);
      this.view.segCompanyDetailAccount.setVisibility(false);
      if(this.isAccountCentricConfig === false){ //for customer centric
        this.view.flxAccountCustVerticalLine.setVisibility(false);
        this.view.flxAccCustomerIdColumn.setVisibility(false);
      }
    }
    this.view.forceLayout();
  },
  setCompanyCustomers : function(){
    var customerContext = this.completeCompanyDetails.customerContext;
    var signatories=[];
    var businessUsers=[];
    for(var s=0;s<customerContext.length;s++){
      if(customerContext[s].isAuthSignatory==="true")
        signatories.push(customerContext[s]);
      else if(customerContext[s].isAuthSignatory==="false")
        businessUsers.push(customerContext[s]);
    }
    this.view.flxAddSignatories.setVisibility(false);
    this.view.flxUsersContentSignatories.setVisibility(false);
    if(signatories.length===0){
      
      this.view.flxAddCustomers.setVisibility(true);
      this.view.flxBusinessUsersTabs.setVisibility(false);
      this.view.flxUsersContent.setVisibility(false);
    }
    else{
    if(signatories.length>=parseInt(this.maxCustomersCount) && this.inAuthorizedSignatoriesUi===true){
      this.view.btnCreateCustomer.skin="sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20pxOp50";
      this.view.btnCreateCustomer.hoverskin="sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20pxOp50";
    }else{
      this.view.btnCreateCustomer.skin="sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px";
      this.view.btnCreateCustomer.hoverskin="sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px";
    }
    this.view.flxBusinessUsersTabs.setVisibility(true);
    this.view.flxUsersContent.setVisibility(true);
    var data=this.inAuthorizedSignatoriesUi===true?signatories:businessUsers;
    if(data.length>0){
      var finalData = data.map(this.mappingCompanyCustomers);
      var widgetMap = {
        "lblName" : "lblName",
        "lblRole" : "lblRole",
        "lblUsername" : "lblUsername",
        "lblCustomerStatus" : "lblCustomerStatus",
        "lblEmail" : "lblEmail",
        "lblSepartor" : "lblSepartor",
        "customerEditInfo" : "customerEditInfo",
        "flxOptions" : "flxOptions",
        "lblFontIconOptions" : "lblFontIconOptions",
        "lblIconStatus":"lblIconStatus",
        "signatorytypeId":"signatorytypeId"
      };
      this.view.segCompanyDetailCustomer.widgetDataMap = widgetMap;
      this.sortBy = this.getObjectSorter("lblName.text");
      this.resetSortImages("businessUsers");
      var sortedData = finalData.sort(this.sortBy.sortData);
      this.view.segCompanyDetailCustomer.setData(sortedData);
      this.view.flxAddCustomers.setVisibility(false);
      this.view.btnCreateCustomer.setVisibility(true);
      this.view.flxCustomerHeader.setVisibility(true);
      this.view.segCompanyDetailCustomer.setVisibility(true);
      this.view.forceLayout();
    }else{
      this.view.flxAddCustomers.setVisibility(true);
      this.view.btnCreateCustomer.setVisibility(false);
      this.view.flxCustomerHeader.setVisibility(false);
      this.view.segCompanyDetailCustomer.setVisibility(false);
      this.view.forceLayout();
    }
    }
  },
  mappingCompanyCustomers : function(data){
    var self = this;
    var statusText;
    var statusSkin;
    var statusIcon;
    if(data.statusId === "SID_CUS_NEW") {
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.New");
      statusSkin = "sknlblLato5bc06cBold14px";
      statusIcon = "sknFontIconNew";
    } else if(data.statusId === "SID_CUS_ACTIVE") {
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Active");
      statusSkin = "sknlblLato5bc06cBold14px";
      statusIcon = "sknFontIconActivate";
    } else if(data.statusId === "SID_CUS_SUSPENDED") {
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended");
      statusSkin = "sknlblLatoeab55d12px";
      statusIcon = "sknFontIconSuspend";
    } else if(data.statusId === "SID_CUS_LOCKED") {
      statusText = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Locked");
      statusSkin = "sknlblCustMngLocked";
      statusIcon = "sknfontIconLock12px";
    } else if(data.statusId === "SID_CUS_PENDING_VERIFICATION") {
      statusText = kony.i18n.getLocalizedString("i18n.accounts.pending");
      statusSkin = "sknlblLatoeab55d12px";
      statusIcon = "sknFontIconSuspend";
    }
    var statusKeys = data.RiskStatus ? data.RiskStatus.split(",") : data.RiskStatus;
    var lblName= self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.FirstName)+self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.MiddleName)+self.AdminConsoleCommonUtils.getParamValueOrEmptyString(data.LastName);
    return{
      "lblName" :  {"text": self.AdminConsoleCommonUtils.getTruncatedString(lblName,19,16),
                    "tooltip": lblName,
                    "onClick" : self.onClickUserNavigateToProfile},
      "lblRole" : {"text": self.AdminConsoleCommonUtils.getTruncatedString(data.role_name ? data.role_name : kony.i18n.getLocalizedString("i18n.Applications.NA"),17,14),
                    "tooltip": data.role_name||kony.i18n.getLocalizedString("i18n.Applications.NA")},
      "lblUsername" : {"text": self.AdminConsoleCommonUtils.getTruncatedString(data.UserName ? data.UserName : kony.i18n.getLocalizedString("i18n.Applications.NA"),15,12),
                    "tooltip": data.UserName||kony.i18n.getLocalizedString("i18n.Applications.NA")},
      "lblCustomerStatus" : {
        "text" : statusText,
        "skin" : statusSkin
      },
      "lblIconStatus":{
        "text":"\ue921",
        "skin" : statusIcon
      },
      "lblEmail" : {"text": self.AdminConsoleCommonUtils.getTruncatedString(data.Email ? data.Email : kony.i18n.getLocalizedString("i18n.Applications.NA"),18,15),
                    "tooltip": data.Email||kony.i18n.getLocalizedString("i18n.Applications.NA")},
      "lblFontIconOptions" : "",
      "flxOptions" : {
        "onHover" : self.showEditTooltip,
        "onClick" : self.editSignatoryOrUser,
        "isVisible" : data.statusId === "SID_CUS_NEW" && data.UserName==="N/A"?false:true
      },
      "lblSepartor" : {"text":"-"},
      "customerEditInfo" : {
        "id" : data.customerId,
        "DateOfBirth" : data.DateOfBirth,
        "DrivingLicenseNumber" : data.DrivingLicenseNumber,
        "FirstName": data.FirstName,
        "LastName": data.LastName,
        "Lastlogintime": data.Lastlogintime,
        "MiddleName": data.MiddleName,
        "Ssn": data.Ssn,
        "UserName": data.UserName,
        "createdts": data.createdts,
        "role_name": data.role_name,
        "Group_id" : data.Group_id,
        "status": data.statusId,
        "Email" : data.Email,
        "Phone" : data.Phone,
        "isEAgreementRequired" :data.isEAgreementRequired,
        "isEagreementSigned" : data.isEagreementSigned,
        "customerFlags":statusKeys,
        "company" :self.completeCompanyDetails.CompanyContext[0],
        "signatorytypeId":data.signatorytypeId
      }
    };
  },
  onHoverAccountsSeg: function (widget, context) {
    var self = this;
    var rowData = self.view.segCompanyDetailAccount.data[context.rowIndex];
    var Organization_id = self.completeCompanyDetails.CompanyContext[0].id;
    var unlinkFunction = function(){
      self.view.popUp.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Unlink_Account");
      self.view.popUp.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Unlink_accounts_message");
      self.view.popUp.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.frmCompanies.UNLINK");
      self.view.flxUnlinkAccount.setVisibility(true);
      self.accountsUnlinkPayload = {
        "Organization_id": Organization_id,
        "AccountsList":[
          {
            "Account_id":rowData.lblAccountNumber.text,
          }]};
    };
    if(self.view.segCompanyDetailAccount.data.length >1 ){
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        if (rowData.flxUnlink.isVisible === false) {
          rowData.flxUnlink.isVisible = true;
          rowData.flxUnlink.onClick = unlinkFunction;
          self.view.segCompanyDetailAccount.setDataAt(rowData, context.rowIndex);
        }
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        if (rowData.flxUnlink.isVisible === true) {
          rowData.flxUnlink.isVisible = false;
          self.view.segCompanyDetailAccount.setDataAt(rowData, context.rowIndex);
        }
      }
    }
  },
  validatingCompanyDetails : function(){
    // company name
    var validation = true
    if(this.view.companyDetailsEntry11.tbxEnterValue.text.trim() === ""){
      this.view.companyDetailsEntry11.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmLocationsController.Enter_Company_Name");
      this.view.companyDetailsEntry11.flxInlineError.isVisible = true;
      this.view.companyDetailsEntry11.flxEnterValue.skin = "sknFlxCalendarError";
      validation = false;
    }
    if(this.view.lstBoxCompanyDetails12.selectedKey === "select"){
      this.view.lstBoxCompanyDetails12.skin = "sknLstBoxeb3017Bor3px";
      this.view.flxComapnyDetailsEntryError12.setVisibility(true);
      this.view.lblErrorText.text ="Select a Business Type";
      validation = false;
    }
    // contact number
    var phoneRegex=/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    var phn = this.view.companyContactNumber.txtContactNumber.text.trim();
    if (phn && this.view.companyContactNumber.txtContactNumber.text.trim().length > 15) {
      this.view.companyContactNumber.flxError.width = "61%";
      this.view.companyContactNumber.flxError.isVisible = true;
      this.view.companyContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
      this.view.companyContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if (phn && phoneRegex.test(this.view.companyContactNumber.txtContactNumber.text) === false) {
      this.view.companyContactNumber.flxError.width = "61%";
      this.view.companyContactNumber.flxError.isVisible = true;
      this.view.companyContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      this.view.companyContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if(phn === "" && this.view.companyContactNumber.txtISDCode.text.trim()){
      this.view.companyContactNumber.flxError.width = "61%";
      this.view.companyContactNumber.flxError.isVisible = true;
      this.view.companyContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      this.view.companyContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if(phn && (!this.view.companyContactNumber.txtISDCode.text.trim() ||(this.view.companyContactNumber.txtISDCode.text === "+"))){
      this.view.companyContactNumber.flxError.width = "98%";
      this.view.companyContactNumber.flxError.isVisible = true;
      this.view.companyContactNumber.txtISDCode.skin = "skinredbg";
      this.view.companyContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      validation = false;
    }

    //ISD code
    var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
    if(this.view.companyContactNumber.txtISDCode.text.trim() && (this.view.companyContactNumber.txtISDCode.text.trim().length > 4 || ISDRegex.test(this.view.companyContactNumber.txtISDCode.text) === false)){
      this.view.companyContactNumber.flxError.width = "98%";
      this.view.companyContactNumber.flxError.isVisible = true;
      this.view.companyContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      this.view.companyContactNumber.txtISDCode.skin = "skinredbg";
      validation = false;
    }


    // email id
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.view.companyDetailsEntry32.tbxEnterValue.text.trim() && emailRegex.test(this.view.companyDetailsEntry32.tbxEnterValue.text.trim()) === false) {
      this.view.companyDetailsEntry32.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
      this.view.companyDetailsEntry32.flxInlineError.isVisible = true;
      this.view.companyDetailsEntry32.flxEnterValue.skin = "sknFlxCalendarError";
      validation = false;
    }

    //ZipCode
    if (this.view.companyDetailsEntry72.tbxEnterValue.text.trim() && /^([a-zA-Z0-9_]+)$/.test(this.view.companyDetailsEntry72.tbxEnterValue.text) === false  ) {
      this.view.companyDetailsEntry72.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorZipCode");
      this.view.companyDetailsEntry72.flxInlineError.setVisibility(true);
      this.view.companyDetailsEntry72.flxEnterValue.skin = "skinredbg";
      validation = false;
    }

    //State
    if (this.view.typeHeadState.tbxSearchKey.text.trim() === "" && (this.view.typeHeadState.segSearchResult.isVisible || this.view.typeHeadState.flxNoResultFound.isVisible)) {
      this.view.lblNoStateError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectState"); 
      this.view.flxNoState.isVisible = true;
      this.view.typeHeadState.tbxSearchKey.skin = "skinredbg";
      validation = false;
    } else if(this.view.typeHeadState.tbxSearchKey.text && this.view.typeHeadState.tbxSearchKey.info.isValid === false){
      this.view.lblNoStateError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorState");
      this.view.flxNoState.isVisible = true;
      this.view.typeHeadState.tbxSearchKey.skin = "skinredbg";
      validation = false;
    }

    //Country
    if (this.view.typeHeadCountry.tbxSearchKey.text.trim() === "" && (this.view.typeHeadCountry.segSearchResult.isVisible || this.view.typeHeadCountry.flxNoResultFound.isVisible)) {
      this.view.lblNoCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectCountry"); 
      this.view.flxNoCountry.isVisible = true;
      this.view.typeHeadCountry.tbxSearchKey.skin = "skinredbg";
      validation = false;
    } else if(this.view.typeHeadCountry.tbxSearchKey.text && this.view.typeHeadCountry.tbxSearchKey.info.isValid === false){
      this.view.lblNoCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorCountry");
      this.view.flxNoCountry.isVisible = true;
      this.view.typeHeadCountry.tbxSearchKey.skin = "skinredbg";
      validation = false;
    }
    return validation;
  },
  clearValidation : function(widget,errorFlex,type){
    if (type === 1)
      widget.skin = "skntxtbxDetails0bbf1235271384a";
    else if (type === 2)
      widget.skin = "sknLbxborderd7d9e03pxradius";
    else if (type === 3)
      widget.skin = "sknflxEnterValueNormal";
    errorFlex.setVisibility(false);
  },
  getGoogleSuggestion : function(text){
    var self = this;
    function onSuccess(response) {
      self.view.segSearch.setVisibility(true);
      self.setSerchSegmentData(response);
      self.view.forceLayout();
    }
    function onError(response) {
      kony.print("Error",response);
    }
    this.presenter.getAddressSuggestion(text, onSuccess, onError);

  },
  mapping : function(data){
    return{
      "lblId" : data.place_id,
      "lblAddress" : data.description,
      "lblPinIcon" : "",
      "lat" : "17.4947934",
      "long" : "78.3996441"
    };
  },
  setSerchSegmentData : function(data){
    var self = this;
    var finalData;
    if(data.predictions){
      finalData = data.predictions.map(self.mapping);
      var dataMap = {
        lblAddress : "lblAddress",
        lblPinIcon : "lblPinIcon"
      };
      this.view.segSearch.widgetDataMap = dataMap;
      this.view.segSearch.setData(finalData);
    }
  },
  //Map functionality is not used currently
  /*mappingRowToWidgets : function(){
    var self = this;
    var data = this.view.segSearch.data;
    var index = this.view.segSearch.selectedRowIndex[1];
    var id = data[index].lblId;
    function onSuccess(response){
      self.fillingData(response,data[index]);
    }
    function onError(reponse){
      kony.print("Error",reponse);
    }
    this.presenter.getPlaceDetails(id, onSuccess, onError);

  },
  fillingData : function(response,rowData){
    this.clearAllAddressFields();
    var self = this;
    if(response.result){
      var finalresponse = response.result;
      self.view.tbxSearch.text = "";
      self.view.segSearch.setVisibility(false);
      var pin = {
        id: rowData.lblId,
        lat: finalresponse[0].latitude,
        lon: finalresponse[0].longitude,
        name: "",
        image: "pinb.png",
        focusImage: "pinb.png",
        desc: "",
        showCallout: false
      };
      var locationData = {
        lat: finalresponse[0].latitude,
        lon: finalresponse[0].longitude,
      };
      self.view.typeHeadState.tbxSearchKey.setEnabled(true);
      self.view.typeHeadCity.tbxSearchKey.setEnabled(true);
      self.view.map1.mapLocations.addPin(pin);
      self.view.map1.mapLocations.navigateToLocation(locationData,false,true);
      self.view.tbxStreetName.text = rowData.lblAddress;
      for(var i = 0;i<finalresponse[0].address_components.length ;i++){
        if (finalresponse[0].address_components[i].types.search("postal_code") !== -1) {
          self.view.companyDetailsEntry72.tbxEnterValue.text = finalresponse[0].address_components[i].long_name;
        }else if(finalresponse[0].address_components[i].types.search("country") !== -1) {
          if(self.checkAvailabilty(finalresponse[0].address_components[i].long_name,self.view.typeHeadCountry.segSearchResult.data)){
            self.view.typeHeadCountry.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
            self.view.typeHeadCountry.tbxSearchKey.info.isValid = true;
          }else{
            self.view.flxNoCountry.setVisibility(true);
            self.view.lblNoCountryError.text = "invalid country";
            self.view.forceLayout();
          }
        }else if(finalresponse[0].address_components[i].types.search("locality") !== -1){
          if(self.checkAvailabilty(finalresponse[0].address_components[i].long_name,self.view.typeHeadCity.segSearchResult.data)){
            self.view.typeHeadCity.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
            self.view.typeHeadCity.tbxSearchKey.info.isValid = true;
          }else{
            self.view.flxNoCity.setVisibility(true);
            self.view.lblNoCityError.text = "invalid city";
            self.view.forceLayout();
          }
        }else if(finalresponse[0].address_components[i].types.search("administrative_area_level_1") !== -1){
          if(self.checkAvailabilty(finalresponse[0].address_components[i].long_name,self.view.typeHeadState.segSearchResult.data)){
            self.view.typeHeadState.tbxSearchKey.text = finalresponse[0].address_components[i].long_name;
            self.view.typeHeadState.tbxSearchKey.info.isValid = true;
          }else{
            self.view.flxNoState.setVisibility(true);
            self.view.lblNoStateError.text = "invalid state";
            self.view.forceLayout();
          }
        }
      }
      self.getAddressCodes([self.view.typeHeadCountry.tbxSearchKey.text,
                            self.view.typeHeadState.tbxSearchKey.text,
                            self.view.typeHeadCity.tbxSearchKey.text]);
    }
  },*/
  checkAvailabilty : function(key,list){
    for(var i=0;i<list.length;i++){
      if((list[i].lblAddress.text).toLowerCase().indexOf(key.toLowerCase()))
        return true;
    }
    return false;
  },
  clearAllAddressFields : function(){
    this.view.companyDetailsEntry41.tbxEnterValue.text = "";
    this.view.companyDetailsEntry51.tbxEnterValue.text = "";
    this.view.companyDetailsEntry72.tbxEnterValue.text = "";
    this.view.typeHeadCountry.tbxSearchKey.text = "";
    this.view.typeHeadCountry.tbxSearchKey.info.isValid = false;    
    this.view.typeHeadCity.tbxSearchKey.text = "";
    this.view.typeHeadCity.tbxSearchKey.info.isValid = false;
    this.view.typeHeadState.tbxSearchKey.text = "";
    this.view.typeHeadState.tbxSearchKey.info.isValid = false;
    this.clearValidation(this.view.companyDetailsEntry72.flxEnterValue, this.view.companyDetailsEntry72.flxInlineError, 3);
    this.clearValidation(this.view.typeHeadCountry.tbxSearchKey, this.view.flxNoCountry, 1);
    this.clearValidation(this.view.typeHeadCity.tbxSearchKey, this.view.flxNoCity, 1);
    this.clearValidation(this.view.typeHeadState.tbxSearchKey, this.view.flxNoState, 1);
  },
  showCompanyDetailsScreen : function(){
    this.hideAllOptionsButtonImages();
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      this.view.verticalTabsCompany.flxImgArrow2
    );
    this.view.flxSearchCompanies.setVisibility(false);
    this.view.flxAccountsTab.setVisibility(false);
    this.view.flxCompanyDetailsTab.setVisibility(true);
    this.view.flxAssignFeaturesTab.setVisibility(false);
    this.view.flxAssignLimitsTab.setVisibility(false);
    this.view.flxOwnerDetailsTabs.setVisibility(false);
    var widgetArray = [this.view.verticalTabsCompany.btnOption1,this.view.verticalTabsCompany.btnOption2,this.view.verticalTabsCompany.btnOption3,this.view.verticalTabsCompany.btnOption4];
    this.tabUtilVerticleButtonFunction(widgetArray,this.view.verticalTabsCompany.btnOption2);
  },
  getAddressSegmentData : function(){
    var self = this;
    var callBack = function(response){
      kony.print("listboxreponse",response);
      if(response !== "error") {
        self.segCountry = response.countries.reduce(
          function(list, country) {
            return list.concat([{"id":country.id,
                                 "lblAddress":{"text":country.Name,
                                               "left" : "10dp"},
                                 "template":"flxSearchCompanyMap"}]);
          },[]);
        self.segState = response.regions.reduce(
          function(list, region) {
            return list.concat([{"id":region.id,
                                 "lblAddress":{"text":region.Name,
                                               "left" : "10dp"},
                                 "Country_id":region.Country_id,
                                 "template":"flxSearchCompanyMap"}]);
          },[]);
      }
      if(self.action === self.actionConfig.edit){
        self.getAddressCodes([self.view.typeHeadCountry.tbxSearchKey.text,
                              self.view.typeHeadState.tbxSearchKey.text]);
      }
      self.setAddressSegmentData();
    };
    this.presenter.fetchLocationPrefillData(callBack);
  },
  setAddressSegmentData : function(){
    var widgetMap = {"flxSearchCompanyMap":"flxSearchCompanyMap",
                     "lblAddress":"lblAddress",
                     "id":"id",
                     "Region_id":"Region_id",
                     "Country_id":"Country_id"};
    this.view.typeHeadCountry.segSearchResult.widgetDataMap = widgetMap;
    this.view.typeHeadState.segSearchResult.widgetDataMap = widgetMap;
    this.view.typeHeadCountry.segSearchResult.setData(this.segCountry);
    this.view.typeHeadState.segSearchResult.setData(this.segState);
  },
  /*
   * show add accounts screen
   */
  showAddAccountsScreen : function () {
    var self = this;
    this.hideAllOptionsButtonImages();
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      this.view.verticalTabsCompany.flxImgArrow1 
    );
    self.view.flxCompanyDetailsTab.setVisibility(false);
    self.view.flxOwnerDetailsTabs.setVisibility(false);
    self.view.flxAccountsTab.setVisibility(true);
	self.view.flxAssignFeaturesTab.setVisibility(false);
    self.view.flxAssignLimitsTab.setVisibility(false);
    self.view.addAndRemoveAccounts.segAddOptions.setData([]);
    self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
    self.view.addAndRemoveAccounts.tbxSearchBox.text = "";
    var widgetArray = [this.view.verticalTabsCompany.btnOption1,this.view.verticalTabsCompany.btnOption2,this.view.verticalTabsCompany.btnOption3,this.view.verticalTabsCompany.btnOption4];
    self.tabUtilVerticleButtonFunction(widgetArray,this.view.verticalTabsCompany.btnOption1);
    var data = self.view.addAndRemoveAccounts.segSelectedOptions.info.segData || [];
    self.view.addAndRemoveAccounts.segSelectedOptions.setData(data);
    self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_accountNo");
    self.showHideAddRemoveSegment();
    self.adjustAccountsUIBasedOnType();
    self.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
    self.view.forceLayout();
  },
  /*
   * show create owner details screen
   */
  showCreateOwnerDetails: function () {
    var self = this;
    this.hideAllOptionsButtonImages();
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      this.view.verticalTabsCompany.flxImgArrow5
    );
    self.view.flxCompanyDetailsTab.setVisibility(false);
    self.view.flxAccountsTab.setVisibility(false);
    self.view.flxOwnerDetailsTabs.setVisibility(true);
    self.view.flxAssignFeaturesTab.setVisibility(false);
    self.view.flxAssignLimitsTab.setVisibility(false);
    var widgetArray = [this.view.verticalTabsCompany.btnOption1,this.view.verticalTabsCompany.btnOption2,this.view.verticalTabsCompany.btnOption3,this.view.verticalTabsCompany.btnOption4];
    self.tabUtilVerticleButtonFunction(widgetArray,this.view.verticalTabsCompany.btnOption5);
  },
  /*
   * hide the create screen and display search companies screen
   */
  hideCreateScreen : function(){
    var self = this;
    self.view.flxCreateCompany.setVisibility(false);
    self.view.flxSearchCompanies.setVisibility(true);
    self.view.forceLayout();
  },
  /*
   * filters accounts data based on search text
   */
  accountSearch: function (response) {
    var self = this;
    self.hideAccountSearchLoading();
    var filteredData = [],segData =[],searchResult =[];
    if(response && response instanceof Array){
      searchResult = response;
    }else {
      self.view.addAndRemoveAccounts.segAddOptions.setData([]);
    }
    segData = self.mapAvailableSegmentData(searchResult);
    filteredData = self.filterAlreadyAddedData(segData);
    self.view.addAndRemoveAccounts.segAddOptions.setVisibility((filteredData.length > 0));
    if(filteredData.length <= 0) {
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.searchNoResultFoundCompanies");
    } else {
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_accountNo");
    }
    self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility((filteredData.length <= 0));
    self.view.addAndRemoveAccounts.segAddOptions.setData(filteredData);
    self.view.forceLayout();
  },
  /*
    * filters already add data from the search result
    * data resulted from the search
    */
  filterAlreadyAddedData : function(searchResultData){
    var self = this;
    var finalData = [], doesExsist = false;
    var searchDataLen = searchResultData.length || 0;
    var selectedAccounts = self.view.addAndRemoveAccounts.segSelectedOptions.data || [];
    if(searchDataLen > 0){
      finalData = searchResultData.filter(function(rec){
        doesExsist = false;
        for(var i =0; i< selectedAccounts.length;i++){
          if(selectedAccounts[i].Account_id === rec.lblAccountNum.text){ 
            doesExsist =true;
            break;
          }
        }
        if(!doesExsist){
          return rec;
        }
      });
    }
    return finalData;
  },
  /*
   * function to map data to available segment data on search
   * @param: result of search
   */
  mapAvailableSegmentData: function (data) {
    var self = this;
    var result =[];
    var widgetMap ={"lblAccountNum": "lblAccountNum",
                    "lblAccFieldValue1": "lblAccFieldValue1",
                    "lblAccFieldValue2": "lblAccFieldValue2",
                    "lblAccountText":"lblAccountText",
                    "lblAccFieldHeader1":"lblAccFieldHeader1",
                    "lblAccFieldHeader2":"lblAccFieldHeader2",
                    "btnAddAccount": "btnAddAccount" ,
                    "flxSeperator":"flxSeperator",
                    "flxAccField1":"flxAccField1",
                    "flxAccField2":"flxAccField2",
                    "lblLine":"lblLine",
                    "flxAvailableAccounts": "flxAvailableAccounts",
                    "completeRecord" : "completeRecord"
                   };
    self.view.addAndRemoveAccounts.segAddOptions.widgetDataMap = widgetMap;
    result = data.map(function (record) {
      if(record.AccountHolder){
        try{
          record.AccountHolder = JSON.parse(record.AccountHolder);
        }catch(err){
          record.AccountHolder = {username:"",fullname:""};
          kony.print("Invalid JSON format for AccountHolder");
        }
      }else{
        record.AccountHolder = {
          username:"",
          fullname:"" 
        };
      }
      // complete reocrd generation for create / edit payload
      var completeRecord = {};
      completeRecord.Account_id = record.Account_id || "";
      if(record.AccountHolder){
        completeRecord.AccountHolder = JSON.stringify(record.AccountHolder);
      } else{
        completeRecord.AccountHolder = JSON.stringify({username:"",fullname:""});
      }
      completeRecord.Type_id = record.Type_id || "";
      completeRecord.AccountName = record.AccountName || "";
      completeRecord.Membership_id = record.Membership_id;
      completeRecord.TaxId = record.TaxId || "";
      if(record.AccountType || record.accountType){
        completeRecord.AccountType = record.AccountType || record.accountType;
      }
      completeRecord.arrangementId = record.arrangementId ? record.arrangementId : kony.i18n.getLocalizedString("i18n.Applications.NA");
      if(self.isAccountCentricConfig === false){
        completeRecord.MembershipName = record.MembershipName || "";
        completeRecord.Membership_id = record.Membership_id || "";
      }
      return {
        "lblAccountNum": {"text": record.Account_id},
        "lblAccFieldValue1": {"text": record.AccountType },
        "lblAccFieldValue2": (self.isAccountCentricConfig === true) ? 
        {"text": record.AccountHolder.fullname,
         "info":{"value":record.AccountHolder.fullname },
         "tooltip":record.AccountHolder.fullname }:
        {"text":self.AdminConsoleCommonUtils.getTruncatedString(record.Membership_id, 17, 15) ,
         "info":{"value":record.Membership_id},
         "tooltip":record.Membership_id },
        "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT"),
        "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
        "lblAccFieldHeader2":(self.isAccountCentricConfig === true) ?
        kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT_HOLDER_NAME") : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID"),
        "btnAddAccount": {
          "onClick": (self.isAccountCentricConfig === true) ? self.addSelectedAccount : self.addSelectedAccountCustCentric
        },
        "lblLine":{"text": "-"},
        "template": "flxAvailableAccounts",
        "completeRecord" : completeRecord
      };
    });
    return result;
  },
  /*
   * mapping for rows to available accounts segment row
   * @param : row data
   */
  mapRemovedAccountToAvailableAcc : function(selectedRowData){
    var self =this;
    selectedRowData = selectedRowData.completeRecord;
    var accountHolder = JSON.parse(selectedRowData.AccountHolder);
    return {
      "lblAccountNum": {"text": selectedRowData.Account_id},
      "lblAccFieldValue1": {"text": selectedRowData.AccountType},
      "lblAccFieldValue2": (self.isAccountCentricConfig === true) ?
      {"text": accountHolder.fullname || kony.i18n.getLocalizedString("i18n.Applications.NA"),
       "info":{"value":accountHolder.fullname },
       "tooltip":accountHolder.fullname }:
      {"text":self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.Membership_id, 16, 14) ,
       "info":{"value":selectedRowData.Membership_id},
       "tooltip":selectedRowData.Membership_id },
      "lblAccountText": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT"),
      "lblAccFieldHeader1": kony.i18n.getLocalizedString("i18n.Group.TYPE"),
      "lblAccFieldHeader2": (self.isAccountCentricConfig === true) ?
      kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT_HOLDER_NAME") : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID"),
      "btnAddAccount": {
        "onClick": (self.isAccountCentricConfig === true)? self.addSelectedAccount : self.addSelectedAccountCustCentric
      },
      "lblLine":{"text": "-"},
      "template": "flxAvailableAccounts",
      "completeRecord" : selectedRowData
    };
  },
  /*
   * (Add Accounts) add left segment selected row to right segment
   */
  addSelectedAccount: function () {
    var self = this;
    var selectedRowData =[];
    var rowIndex = self.view.addAndRemoveAccounts.segAddOptions.selectedIndex[1];
    selectedRowData = self.view.addAndRemoveAccounts.segAddOptions.selectedRowItems[0];
    var widgetMap = {
      "flxClose":"flxClose",
      "fontIconClose":"fontIconClose",
      "lblOption":"lblOption",
      "flxOptionAdded":"flxOptionAdded",
      "completeRecord" : "completeRecord"
    };
    var recordToRemove = {
      "flxClose": {
        "onClick": function () {
          self.removeSelectedAccount();
        }
      },
      "fontIconClose": {
        "text": "\ue929",
        "tooltip": "Remove Account"
      },
      "lblOption": {"text":"Account  " + self.AdminConsoleCommonUtils.getTruncatedString(selectedRowData.lblAccountNum.text, 15, 12),
                    "tooltip": selectedRowData.lblAccountNum.text},
      "template": "flxOptionAdded",
      "completeRecord" : selectedRowData.completeRecord
    };
    self.assignedAccount.push(recordToRemove);
    self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
    self.view.addAndRemoveAccounts.segSelectedOptions.hasSections =  false;
    self.view.addAndRemoveAccounts.segSelectedOptions.addDataAt(recordToRemove,0);
    self.view.addAndRemoveAccounts.segAddOptions.removeAt(rowIndex);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.showHideAddRemoveSegment();
    if(self.checkAccountValidation()){
        self.view.flxAccountError.setVisibility(false);
        self.view.flxAccountsAddAndRemove.top = "0dp";
    }else{
        self.view.flxAccountError.setVisibility(true);
        self.view.flxAccountsAddAndRemove.top = "60dp";
    }
    self.view.forceLayout();
  },
  /*
   * (Remove Accounts) add right segment selected row to left segment
   */
  removeSelectedAccount: function () {
    var self = this;
    var selectedRowData = [];
    var rowIndex = self.view.addAndRemoveAccounts.segSelectedOptions.selectedIndex[1];
    selectedRowData = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowItems[0];
    var recordToAdd = self.mapRemovedAccountToAvailableAcc(selectedRowData);
    self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd,0);
    self.view.addAndRemoveAccounts.segSelectedOptions.removeAt(rowIndex);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
  * reset the added accounts in selected accounts column
  */
  resetAccounts :  function(){
    var self = this;
    var selectedAcc = [],resetRec = [],exsistingData = [],dataToAdd =[];
    selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    if(self.isAccountCentricConfig === true){ //for account centric 
      resetRec = selectedAcc.map(self.mapRemovedAccountToAvailableAcc);
      exsistingData = self.view.addAndRemoveAccounts.segAddOptions.data;
      dataToAdd = exsistingData.concat(resetRec);
      self.view.addAndRemoveAccounts.segAddOptions.setData(dataToAdd);
    }else{ //for customer centric
      for(var i=0; i<selectedAcc.length; i++){
        var rowsData = self.view.addAndRemoveAccounts.segSelectedOptions.data[i][1];
        for(var j=0;j< rowsData.length;j++){
          var recordToAdd = self.mapRemovedAccountToAvailableAcc(rowsData[j]);
          self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd,0);
        }
      }
    }
    self.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = [];
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
  * validation to check atleast one account is assigned 
  */
  checkAccountValidation : function(){
    var self = this;
    var selectedAcc = [];
    selectedAcc = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    return (selectedAcc.length > 0);
  },
  /*
  validating Input Fields when click on search button
  */
  validateInputFields: function() {
    var validationOfSearch = true;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if((this.view.tbxName.text === null || this.view.tbxName.text === "") && (this.view.tbxEmailId.text === null || this.view.tbxEmailId.text === "") ) {
      this.view.flxNameError.setVisibility(true);
      this.view.flxEmailIdError.setVisibility(true);
      this.view.tbxName.skin = "skinredbg";
      this.view.tbxEmailId.skin = "skinredbg";
      validationOfSearch =  false;
    }
    if(this.view.tbxEmailId.text)
      if(emailRegex.test(this.view.tbxEmailId.text.trim()) === false) {
        this.view.lblEmailIdErrorText.text = kony.i18n.getLocalizedString("i18n.frmUsersController.Enter_a_valid_Email-id");
        this.view.flxError.setVisibility(true);
        this.view.flxEmailIdError.setVisibility(true);
        this.view.tbxEmailId.skin = "skinredbg";
        validationOfSearch = false;
      }

    return validationOfSearch;
  },
  /*
  Companies Search Results
  */
  companiesSearch: function() {
    var self = this;
    var searchNameText = "";
    var searchEmailText = "";
    if(this.view.tbxName.text !== null && this.view.tbxName.text !== "") {
      if(this.view.tbxEmailId.text !== null && this.view.tbxEmailId.text !== "") {
        searchNameText = this.view.tbxName.text;
        searchEmailText = this.view.tbxEmailId.text;
      }
      else {
        searchNameText = this.view.tbxName.text;
      }
    }else if(this.view.tbxEmailId.text !== null && this.view.tbxEmailId.text !== "") {
      searchEmailText = this.view.tbxEmailId.text;
    }
    var payload = {
      "Name" : searchNameText,
      "Email" : searchEmailText
    };
    self.presenter.getCompaniesSearch(payload);
  },

  mapResultsData: function (data) {
    var self = this;
    var widgetMap ={"lblCompanyName": "lblCompanyName",
					"rtxContractCustomers":"rtxContractCustomers",
                    "lblTIN": "lblTIN",
                    "lblEmail": "lblEmail",
                    "lblType": "lblType",
                    "lblCompanyId":"lblCompanyId",
                    "lblSeperator":"lblSeperator",
                    "contractId":"contractId"
                   };
    self.view.addAndRemoveAccounts.segAddOptions.widgetDataMap = widgetMap;
    var rtxContractCustomers="";
    var result = data.map(function (record) {
      rtxContractCustomers="";
      for(var i=0;i<record.contractCustomers.length;i++){
      rtxContractCustomers=rtxContractCustomers+"-"+record.contractCustomers[i].coreCustomerName+"<br/>";
    }
      return {
        "contractId": record.contractId,
        "lblCompanyName": {"text":self.AdminConsoleCommonUtils.getTruncatedString(record.contractName, 30, 27),
                           "info":{"value":record.contractName},
                           "tooltip":record.contractName},
        "rtxContractCustomers":{"text":rtxContractCustomers},
        "lblType": record.serviceDefinitionName || kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblCompanyId":record.contractId,
        "lblEmail": record.email || kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblSeperator":"-"
      };
    });
    return result;
  },
  /*
   * toggle segment and noResults_found based on data's length
   */
  showHideAddRemoveSegment : function(){
    var self = this;
    var availableSegData = [],selectedSegData= [];
    availableSegData = self.view.addAndRemoveAccounts.segAddOptions.data;
    selectedSegData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.view.addAndRemoveAccounts.segAddOptions.setVisibility(availableSegData.length > 0);
    self.view.addAndRemoveAccounts.segSelectedOptions.setVisibility(selectedSegData.length > 0);
    self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.setVisibility(availableSegData.length <= 0);
    self.view.addAndRemoveAccounts.rtxSelectedOptionsMessage.setVisibility(selectedSegData.length <= 0);
    self.view.addAndRemoveAccounts.btnRemoveAll.setVisibility(selectedSegData.length > 0);
    //selected accounts container skin change
    if(selectedSegData.length > 0 && self.isAccountCentricConfig === false){
      self.view.addAndRemoveAccounts.flxSegSelectedOptions.skin = "sknflxffffffBordere1e5ed4Px";
    }else{
      self.view.addAndRemoveAccounts.flxSegSelectedOptions.skin = "sknflxf9f9f9Border1pxRad4px";
    }
    self.view.forceLayout();
  },
  /*
   * search on address fields while typing in textbox
   * @param: textbox path, sement path
   * @param: category ( 1-country, 2- state, 3-city)
   */
  searchForAddress : function(tbxPath,segPath,noResultFlex,category){
    var self = this;
    var searchText = tbxPath.text;
    var sourceData = [],dataToAssign = [];
    if(category === 1){
      sourceData =self.segCountry;
      dataToAssign = sourceData.filter(function(rec){
        var name = (rec.lblAddress.text).toLowerCase();
        return (name.indexOf(searchText.toLowerCase()) > -1);
      });
    }else if(category === 2){
      sourceData =self.segState;
      var country = self.view.typeHeadCountry.tbxSearchKey.info.data;
      dataToAssign = sourceData.filter(function(rec){
        var name = (rec.lblAddress.text).toLowerCase();
        return ((name.indexOf(searchText.toLowerCase()) > -1) && (rec.Country_id === country.id) );
      });

    }else if(category === 3){
      sourceData =self.segLocationCity;
      var state = self.view.typeHeadState.tbxSearchKey.info.data;
      dataToAssign = sourceData.filter(function(rec){
        var name = (rec.lblAddress.text).toLowerCase();
        return ((name.indexOf(searchText.toLowerCase()) > -1)&& (rec.Region_id === state.id));
      });
    }
    if(searchText === "") dataToAssign = [];
    segPath.setData(dataToAssign);
    if(dataToAssign.length > 0){
      segPath.setVisibility(true);
      noResultFlex.setVisibility(false);
      if(noResultFlex === this.view.typeHeadCountry.flxNoResultFound){
        this.view.flxCountry.zIndex = 2;
      }else{
        this.view.flxCountry.zIndex = 1;
      }
    }else{
      segPath.setVisibility(false);
      noResultFlex.setVisibility(true);
      if(noResultFlex === this.view.typeHeadCountry.flxNoResultFound){
        this.view.flxCountry.zIndex = 2;
      }else{
        this.view.flxCountry.zIndex = 1;
      }
    }
    self.view.forceLayout();
  },
  /*
    * displays required main flex(search,details,create) and hides other
    * @param: path of flex to show
    */
  hideRequiredMainScreens : function(reqFlexPath){
    var self = this;
    self.view.flxSearchCompanies.setVisibility(false);
    self.view.flxCreateCompany.setVisibility(false);
    self.view.flxCompanyDetails.setVisibility(false);
    self.view.flxCreateContract.setVisibility(false);
    reqFlexPath.setVisibility(true);
    var screenHeight = kony.os.deviceInfo().screenHeight;
    if(reqFlexPath.id === "flxCompanyDetails"){
      this.view.flxMainContent.height = (screenHeight -115) +"px";
    }else if(reqFlexPath.id === "flxCreateCompany"){
      this.view.flxMainContent.height = (screenHeight -115) +"px";
    } else{
      this.view.flxMainContent.height = (screenHeight -90) +"px";
    }
    self.view.forceLayout();
  },
  /*
   * validate owner detail screen fields
   */
  validateOwnerDetailsScreen: function () {
    var self = this;
    var isValid = true;
    //firstname
    if (self.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text.trim() === "") {
      self.view.textBoxOwnerDetailsEntry11.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxOwnerDetailsEntry11.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry11.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.FirstNameMissing");
      isValid = false;
    }
    //lastname
    if (self.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text.trim() === "") {
      self.view.textBoxOwnerDetailsEntry13.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxOwnerDetailsEntry13.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry13.lblErrorText.text = kony.i18n.getLocalizedString("i18n.Common.frmGuestDashboard.LastNameMissing");
      isValid = false;
    }
    //email-id
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text.trim() === "") {
      self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailId_cannot_be_empty");
      isValid = false;
    } else if (emailRegex.test(self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text.trim()) === false) {
      self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry23.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
      isValid = false;
    }
    //contact num
    var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (!self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text || !self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text.trim()) {
      self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_be_empty");
      isValid = false;
    } else if (self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text.trim().length > 15) {
      self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
      isValid = false;
    } else if (phoneRegex.test(self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text) === false) {
      self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "80dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      isValid = false;
    }
    //ISD code
    var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
    if (!self.view.textBoxOwnerDetailsEntry31.txtISDCode.text ||
        !self.view.textBoxOwnerDetailsEntry31.txtISDCode.text.trim() ||
        (self.view.textBoxOwnerDetailsEntry31.txtISDCode.text === "+")) {
      self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    } else if (self.view.textBoxOwnerDetailsEntry31.txtISDCode.text.trim().length > 4) {
      self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    } else if (ISDRegex.test(self.view.textBoxOwnerDetailsEntry31.txtISDCode.text) === false) {
      self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skinredbg";
      self.view.textBoxOwnerDetailsEntry31.flxError.left = "0dp";
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry31.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      isValid = false;
    }
    //DOB
    if (self.view.customCalOwnerDOB.value === "") {
      self.view.flxCalendarDOB.skin = "sknFlxCalendarError";
      self.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry21.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.DOB_cannot_be_empty");
      isValid = false;
    }
    //SSN
    if (self.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text.trim() === "") {
      self.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueError";
      self.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(true);
      self.view.textBoxOwnerDetailsEntry22.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.SSN_cannot_be_empty");
      isValid = false;
    }
    self.view.forceLayout();
    return isValid;
  },  
  /*
   * clears all fields in create company owner details
   */
  clearDataForOwnerDetails : function(){
    var self =this;
    self.clearValidationsForOwnerDetails();
    self.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry12.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry21.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text = "";
    self.view.textBoxOwnerDetailsEntry31.txtContactNumber.text = "";
    self.view.textBoxOwnerDetailsEntry31.txtISDCode.text = "";
    self.view.customCalOwnerDOB.resetData = kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
    self.view.forceLayout();
  },
  /*
   * clears the inline error for owner details in create company
   */
  clearValidationsForOwnerDetails : function(txtBoxPath,errFlexPath){
    var self = this;
    if(txtBoxPath){
      txtBoxPath.skin = "skntbxLato35475f14px";
      if(errFlexPath) errFlexPath.setVisibility(false);
    } else{
      self.view.textBoxOwnerDetailsEntry11.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxOwnerDetailsEntry13.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.flxCalendarDOB.skin = "sknflxEnterValueNormal";
      self.view.textBoxOwnerDetailsEntry22.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxOwnerDetailsEntry23.flxEnterValue.skin = "sknflxEnterValueNormal";
      self.view.textBoxOwnerDetailsEntry31.txtContactNumber.skin = "skntbxLato35475f14px";
      self.view.textBoxOwnerDetailsEntry31.txtISDCode.skin = "skntbxLato35475f14px";

      self.view.textBoxOwnerDetailsEntry11.flxInlineError.setVisibility(false);
      self.view.textBoxOwnerDetailsEntry13.flxInlineError.setVisibility(false);
      self.view.textBoxOwnerDetailsEntry21.flxInlineError.setVisibility(false);
      self.view.textBoxOwnerDetailsEntry22.flxInlineError.setVisibility(false);
      self.view.textBoxOwnerDetailsEntry23.flxInlineError.setVisibility(false);
      self.view.textBoxOwnerDetailsEntry31.flxError.setVisibility(false);
    }
    self.view.forceLayout();
  },
  /*
   * display the selected type for search
   */
  dispalySelectedType : function(){
    var self = this;
    var selType ="",curType = "",ind = "";
    var selRowData = self.view.addAndRemoveAccounts.segSearchType.selectedRowItems[0];
    ind = self.view.addAndRemoveAccounts.segSearchType.selectedRowIndex[1];
    selType = selRowData.lblValue;
    curType = self.view.addAndRemoveAccounts.lblSelSearchType.text;
    selRowData.lblValue = curType;
    self.view.addAndRemoveAccounts.segSearchType.setDataAt(selRowData, ind);
    self.view.addAndRemoveAccounts.lblSelSearchType.text = selType;
    self.view.addAndRemoveAccounts.flxSegSearchType.setVisibility(false);
    self.view.forceLayout();
  },
  /*
   * show the changes in accounts screen based on type
   */
  adjustAccountsUIBasedOnType : function(){
    var self =this;
    self.view.flxHeading.setVisibility(false);
    self.view.flxAccountsAddAndRemove.top = "0dp";
    self.view.addAndRemoveAccounts.flxFilteredSearch.setVisibility(true);
    self.view.addAndRemoveAccounts.flxSearchContainer.setVisibility(false);
    if(self.isAccountCentricConfig === false){ //customer centric
      self.view.addAndRemoveAccounts.lblSelSearchType.text = kony.i18n.getLocalizedString("i18n.customerSearch.MemberID");
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompaniesController.SearchByCustomerIDToViewResults");
      self.view.addAndRemoveAccounts.tbxFilterSearch.placeholder = kony.i18n.getLocalizedString("i18n.frmCompaniesController.SearchByCustomerID");
    } else{ //account centric
      self.view.addAndRemoveAccounts.lblSelSearchType.text = kony.i18n.getLocalizedString("i18n.frmCompaniesController.AccountID");
      self.view.addAndRemoveAccounts.rtxAvailableOptionsMessage.text = kony.i18n.getLocalizedString("i18n.frmCompanies.SearchByAccountNumToViewResults");
      self.view.addAndRemoveAccounts.tbxFilterSearch.placeholder = kony.i18n.getLocalizedString("i18n.frmCompanies.Search_by_accountNo");
    }
    self.view.forceLayout();
  },
  /*
   *resets search type list box
   */
  resetSearchTypeList : function(){
    var self =this;
    self.view.lblSelSearchType.text ="Membership Id";
    var data =[{"lblValue":"Account Id"},{"lblValue":"Tax Number"}];
    self.view.segSearchType.setData(data);
    self.view.forceLayout();
  },
  /*
   * returns company details information that user entered in create company
   */
  getCreateCompanyDetails:function(){
    var companyDetails = {};
    
    companyDetails.Name = this.view.companyDetailsEntry11.tbxEnterValue.text;
    companyDetails.Type = this.view.lstBoxCompanyDetails12.selectedKeyValue[1];
    companyDetails.FaxId = this.view.companyDetailsEntry31.tbxEnterValue.text;
    companyDetails.Description = "";
    var phn="";
    if(this.view.companyContactNumber.txtISDCode.text && this.view.companyContactNumber.txtContactNumber.text){
      phn = this.view.companyContactNumber.addingPlus(this.view.companyContactNumber.txtISDCode.text)+"-"+this.view.companyContactNumber.txtContactNumber.text;
    } else if(this.view.companyContactNumber.txtISDCode.text || this.view.companyContactNumber.txtContactNumber.text){
      phn = this.view.companyContactNumber.addingPlus(this.view.companyContactNumber.txtISDCode.text) || this.view.companyContactNumber.txtContactNumber.text;
    }
    companyDetails.Communication = [{
      "Phone":phn,
      "Email":this.view.companyDetailsEntry32.tbxEnterValue.text || ""
    }];
    companyDetails.Address = [
      {
        "country": this.view.typeHeadCountry.tbxSearchKey.text,
        "cityName": this.view.typeHeadCity.tbxSearchKey.text,
        "state": this.view.typeHeadState.tbxSearchKey.text,
        "zipCode": this.view.companyDetailsEntry72.tbxEnterValue.text,
        "addressLine1": this.view.companyDetailsEntry41.tbxEnterValue.text,
        "addressLine2": this.view.companyDetailsEntry51.tbxEnterValue.text
      }
    ];
    //companyDetails.Membership = [];
    return companyDetails;
  },
  /*
   * returns owner details information that user entered in create company
   */
  getCreateCompanyOwnerDetails:function(){
    
    var ownerDetails = {};
    ownerDetails.firstName = this.view.textBoxOwnerDetailsEntry11.tbxEnterValue.text;
    ownerDetails.middleName = this.view.textBoxOwnerDetailsEntry12.tbxEnterValue.text;
    ownerDetails.lastName = this.view.textBoxOwnerDetailsEntry13.tbxEnterValue.text;
    ownerDetails.dob = this.getDateFormatYYYYMMDD(this.view.customCalOwnerDOB.value);
    ownerDetails.ssn = this.view.textBoxOwnerDetailsEntry22.tbxEnterValue.text;
    ownerDetails.email = this.view.textBoxOwnerDetailsEntry23.tbxEnterValue.text;
    ownerDetails.contactNumber = this.view.textBoxOwnerDetailsEntry31.txtContactNumber.text;
    ownerDetails.isoCode = this.view.textBoxOwnerDetailsEntry31.txtISDCode.text;
    return {
      "Owner":[
        {
          "FirstName": ownerDetails.firstName,
          "MidleName": ownerDetails.middleName || "",
          "LastName": ownerDetails.lastName,
          "DOB": ownerDetails.dob,
          "EmailId": ownerDetails.email,
          "Ssn": ownerDetails.ssn,
          "PhoneNumber": ownerDetails.isoCode+"-"+ownerDetails.contactNumber,
          "IdType": "",
          "IdValue": ""
        }
      ]
    };
  },
  /*
   * returns selected accounts information that user added in create company
   */
  getCreateCompanySelectedAccounts:function(){
    var selectedAccounts = this.view.addAndRemoveAccounts.segSelectedOptions.data || [];
    var selectedAccountsFinal = [];
    for(var i=0;i<selectedAccounts.length;i++){
      if(this.isAccountCentricConfig === true){ //account centric
        selectedAccounts[i].completeRecord['TaxId'] = this.view.companyDetailsEntry21.tbxEnterValue.text || "";
        selectedAccountsFinal.push(selectedAccounts[i].completeRecord);
      } else{ //customer centric
        var sectionRowsData = this.view.addAndRemoveAccounts.segSelectedOptions.data[i][1];
        for(var j=0;j<sectionRowsData.length;j++){
          selectedAccountsFinal.push(sectionRowsData[j].completeRecord);
        }
      }
    }
    
    
    return {
      "AccountsList":JSON.stringify(selectedAccountsFinal)
    };
  },
  getMandatoryAndSelectedFeatures : function(){
    var selectedData = [];
    var data1 = this.view.segFeatures.data;
    var data2 = this.view.segmandatoryFeatures.data;
    var features;
    for(var i=0;i<data1.length;i++){
      if(data1[i].selected === "true"){
        selectedData.push(data1[i].featureId);
      }
    }
    for(var j=0;j<data2.length;j++){
      selectedData.push(data2[j].featureId);
    }
    features = JSON.stringify(selectedData);
    return {
      "features" : features
    };
  },
  getSelectedFeatureLimits : function(data){
    var actionlimits = [];
    var tempJson = {};
    for(var i=0;i<data.length;i++){
      tempJson = {
        "id": data[i].actionId,
        "limits": [
          {"id": "MAX_TRANSACTION_LIMIT",
           "value": data[i].tbxPerTransactionLimitValue.text},
          {"id": "DAILY_LIMIT",
           "value": data[i].tbxDailyTransactionLimitValue.text},
          {"id": "WEEKLY_LIMIT",
           "value": data[i].tbxWeeklyTransactionLimitValue.text}
        ]
      };
      actionlimits.push(tempJson);
    }
    actionlimits = JSON.stringify(actionlimits);
    return{
      "actionlimits" : actionlimits
    };
  },

  /*
   * creates company with details provided by the user in create company module
   */
  requestCreateUpdateComapny: function(){
    const companyDetails = this.getCreateCompanyDetails();
    const selectedAccounts = this.getCreateCompanySelectedAccounts();
    var payLoad = {
    };
    if(this.action === this.actionConfig.create){
      const selectedFeautres = this.getMandatoryAndSelectedFeatures();
      const selectedFeatureLimits = this.getSelectedFeatureLimits(this.view.segAssignLimits.data);  
      payLoad = Object.assign({},companyDetails,selectedAccounts,selectedFeautres,selectedFeatureLimits);
      this.presenter.createCompany(payLoad);
    }else{
      const addedFeatures = {
        "addedFeatures" :JSON.stringify(this.generateAddedRemovedFeatures().addedIds)
      };

      const removedFeatures = {
        "removedFeatures":JSON.stringify(this.generateAddedRemovedFeatures().removedIds)
      };
      const updatedActionlimits = {
        "updatedActionlimits":this.getSelectedFeatureLimits(this.view.segAssignLimits.data).actionlimits
      };
      payLoad = Object.assign({},companyDetails,selectedAccounts,addedFeatures,removedFeatures, updatedActionlimits);
      payLoad.id = this.completeCompanyDetails.CompanyContext[0].id;
      this.presenter.editCompany(payLoad);
    }
  },
  createCompaySuccess:function(context){
    this.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),"Company Created Successfully");
    this.showCreatedCompanyDetails(context.id, 1);
  },
  createCompanyFailed:function(){

  },
  /*
   * clears all fields in create company details
   */
  clearDataForCreateCompanyDetails : function(){
    this.view.companyContactNumber.txtContactNumber.text = "";
    this.view.companyContactNumber.txtISDCode.text = "";
    this.view.companyDetailsEntry11.tbxEnterValue.text = "";
    this.view.companyDetailsEntry11.tbxEnterValue.setEnabled(true);
    this.view.companyDetailsEntry11.tbxEnterValue.skin = "sknTbx485c75LatoReg13pxNoBor";
    this.view.lstBoxCompanyDetails12.selectedKey = "select";
    this.view.lstBoxCompanyDetails12.skin = "sknLbxborderd7d9e03pxradius";
    this.view.lstBoxCompanyDetails12.setEnabled(true);
    this.view.companyDetailsEntry21.tbxEnterValue.skin = "sknTbx485c75LatoReg13pxNoBor";
    this.view.companyDetailsEntry21.tbxEnterValue.setEnabled(true);
    this.view.companyDetailsEntry21.tbxEnterValue.text = "";
    this.view.companyDetailsEntry31.tbxEnterValue.text = "";
    this.view.companyDetailsEntry32.tbxEnterValue.text = "";
    this.view.companyDetailsEntry41.tbxEnterValue.text = "";
    this.view.companyDetailsEntry51.tbxEnterValue.text = "";
    this.view.companyDetailsEntry72.tbxEnterValue.text = "";
    this.view.typeHeadCountry.tbxSearchKey.text = "";
    this.view.typeHeadCity.tbxSearchKey.text = "";
    this.view.typeHeadState.tbxSearchKey.text = "";
    this.view.typeHeadCountry.tbxSearchKey.info.isValid = false;
    this.view.typeHeadCity.tbxSearchKey.info.isValid = false;
    this.view.typeHeadState.tbxSearchKey.info.isValid = false;

    this.view.addAndRemoveAccounts.tbxFilterSearch.text = "";
    this.view.addAndRemoveAccounts.tbxSearchBox.text = "";
    
    this.clearValidation(this.view.companyDetailsEntry11.flxEnterValue, this.view.companyDetailsEntry11.flxInlineError, 3);
    this.clearValidation(this.view.lstBoxCompanyDetails12, this.view.flxComapnyDetailsEntryError12, 2);
    this.clearValidation(this.view.companyDetailsEntry32.flxEnterValue, this.view.companyDetailsEntry32.flxInlineError, 3);
    this.clearValidation(this.view.companyContactNumber.txtContactNumber, this.view.companyContactNumber.flxError, 1);
    this.clearValidation(this.view.companyContactNumber.txtISDCode, this.view.companyContactNumber.flxError, 1);

    this.clearAllAddressFields();
    this.view.forceLayout();
  },
  /*
   *check  tin validation
   */
  validateTin:function(callback){
    var scopeObj = this;
    var tin = scopeObj.view.companyDetailsEntry21.tbxEnterValue.text;
    if(tin){
      var payLoad = {
        "Tin":tin
      };
      scopeObj.presenter.validateTIN(payLoad,callback);
    }    
  },
  /*
   * update tin validation status based on service response
   */  
/*  updateTINValidationStatus: function(response){
    if(response.status === "success"){
      if(response.hasOwnProperty("isTINExists")){
        if(response.isTINExists == "true"){
          this.view.lblTinCheck.text = kony.i18n.getLocalizedString("i18n.frmCompanies.tin_valid_success");
          this.view.lblTinCheck.skin = "sknlblLato5bc06cBold14px";
          this.view.lblTinCheck.setVisibility(true);
          this.view.lblTinCheck.isValidTin = true;
          this.view.lblTinCheck.left = "0dp";
          this.view.lblTINErrorIcon.setVisibility(false);
        }else{
          this.view.lblTinCheck.text = kony.i18n.getLocalizedString("i18n.frmCompanies.tin_validation_failed");
          this.view.lblTinCheck.skin = "sknlblError";
          this.view.lblTinCheck.setVisibility(true);                
          this.view.lblTinCheck.isValidTin = false;
          this.view.lblTinCheck.left = "16dp";
          this.view.lblTINErrorIcon.setVisibility(true);
        }
      }else{
        this.view.lblTinCheck.text = response.msg;
        this.view.lblTinCheck.skin = "sknlblLato5bc06cBold14px";
        this.view.lblTinCheck.setVisibility(true);
        this.view.lblTinCheck.isValidTin = true;
        this.view.lblTinCheck.left = "0dp";
        this.view.lblTINErrorIcon.setVisibility(false);
      }
    }else{
      this.view.lblTinCheck.text = response.msg;//kony.i18n.getLocalizedString("i18n.frmCompanies.tin_validation_failed");
      this.view.lblTinCheck.skin = "sknlblError";
      this.view.lblTinCheck.setVisibility(true);      
      this.view.lblTinCheck.isValidTin = false;
      this.view.lblTinCheck.left = "16dp";
      this.view.lblTINErrorIcon.setVisibility(true);
    }
    this.view.lblTinCheck.isTinValidated = true;
    this.view.forceLayout();
  },*/
  setDatatoSearchSegment: function(searchResult) {
   
    var self = this;
    
    self.view.flxHeaderPermissions.setVisibility(false);
    self.view.segSearchResults.setVisibility(false);
    if(searchResult && searchResult !== null && searchResult.length > 0) {

      var segData = self.mapResultsData(searchResult);
      var enteredText = self.returnEnteredText(this.view.tbxName.text, this.view.tbxEmailId.text);
      self.view.flxHeaderPermissions.setVisibility(true);
      self.view.segSearchResults.setVisibility(true);
      self.view.lblNoResults.setVisibility(false);
      self.view.flxNoResultsFound.setVisibility(false);
      self.view.flxSearchResultsFor.setVisibility(true);
      self.view.flxNoFilterResults.setVisibility(false);
      self.view.lblEnteredText.text = enteredText;
      self.view.segSearchResults.setVisibility((segData.length > 0));
      if(segData.length > 0){
        if(segData.length === 1){
          var id = segData[0].contractId;
          self.view.segSearchResults.setData(segData);
          self.showCreatedCompanyDetails(id, 1);
        }else{
          self.view.segSearchResults.setData(segData);
        }
        self.setDataToBusinessTypeFilter(searchResult);
        self.view.segSearchResults.info = {"segData":segData};
      }
      self.sortBy = self.getObjectSorter("lblCompanyName");
      self.resetSortImages("searchList");
      var sortedData = segData.sort(self.sortBy.sortData);
      self.view.segSearchResults.setData(sortedData);
      self.view.segSearchResults.height = (self.view.flxSearchResults.frame.height - 100)+"px";
    } else {
      self.view.segSearchResults.info = {"segData":[]};
      self.view.flxNoResultsFound.setVisibility(true);
      self.view.lblNoResults.text = kony.i18n.getLocalizedString("i18n.frmCompanies.searchNoResultFoundCompanies");
      self.view.lblNoResults.setVisibility(true);
      self.view.lblCreateCompanyLink.setVisibility(true);
    }
    self.view.forceLayout();
  },
  /*
   * displays company creation page with company details
   */  
  showEditCompany:function(companyCompleteDetails){
    var scopeObj = this;
    this.hideRequiredMainScreens(scopeObj.view.flxCreateCompany);
    this.view.flxBreadcrumb.setVisibility(true);
    this.view.addAndRemoveAccounts.segSelectedOptions.info = {"segData":[]};
    this.showAddAccountsScreen()
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
    this.view.flxSettings.setVisibility(false);
    this.clearDataForOwnerDetails();
    this.clearDataForCreateCompanyDetails();   
    this.dataForSearch = false;
    this.dataForMandatorySearch = false;
    /*Data population in company details create company*/
    var companyDetails = companyCompleteDetails.CompanyContext[0];
    if(companyDetails){
      /*Data population in accounts create company*/
      var accounts = companyCompleteDetails.accountContext ? companyCompleteDetails.accountContext : [];
      if(this.isAccountCentricConfig){
        this.mapCompanyAccountsToEditCompany(accounts);
      }else{
        this.mapCustCentricAccountsforEdit(accounts);
        var selectedAcc = this.view.addAndRemoveAccounts.segSelectedOptions.data;
        if(selectedAcc.length === 1 ){
          this.view.lblCompanyDetailsHeading.text = kony.i18n.getLocalizedString("i18n.customerSearch.MemberID")+": "+selectedAcc[0][0].CustomerId;
        } else{
          this.view.lblCompanyDetailsHeading.text = kony.i18n.getLocalizedString("i18n.frmCompaniesController.CompanyIsAssociatedWithMultipleCustomers");
        }
      }
      /*********************************************************/
      var selectedType = this.getBusinessTypeKeyBasedOnName(companyDetails.businessType);
      this.view.lstBoxCompanyDetails12.selectedKey = selectedType ? selectedType[0] : "select";
      this.view.breadcrumbs.btnPreviousPage.text = companyDetails.Name;
      this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.SearchContract");
      this.view.companyDetailsEntry11.tbxEnterValue.text = companyDetails.Name || "";
      this.view.companyDetailsEntry32.tbxEnterValue.text = companyDetails.Email || "";
      var phoneNumber = companyDetails.Phone ? companyDetails.Phone.split("-"):["",""];
      this.view.companyContactNumber.txtContactNumber.text = phoneNumber.length>=2 ? phoneNumber[1]:phoneNumber[0];
      this.view.companyContactNumber.txtISDCode.text = phoneNumber.length>=2 ? this.view.companyContactNumber.addingPlus(phoneNumber[0].trim()):"";
      this.view.companyDetailsEntry21.tbxEnterValue.text = companyDetails.taxId || "";
      this.view.companyDetailsEntry31.tbxEnterValue.text = companyDetails.faxId || "";
      this.view.companyDetailsEntry41.tbxEnterValue.text = companyDetails.addressLine1 || "";
      this.view.companyDetailsEntry51.tbxEnterValue.text = companyDetails.addressLine2 || "";
      this.view.typeHeadCountry.tbxSearchKey.text = companyDetails.country || "";
      this.view.companyDetailsEntry72.tbxEnterValue.text = companyDetails.zipCode || "";
      this.view.typeHeadCity.tbxSearchKey.text = companyDetails.cityName || "";
      this.view.typeHeadState.tbxSearchKey.text = companyDetails.state || "";
      this.view.typeHeadCity.tbxSearchKey.info.isValid = true;
      this.view.typeHeadCountry.tbxSearchKey.info.isValid = true;
      this.view.typeHeadState.tbxSearchKey.info.isValid = true;
      //disable TaxId field for edit
      if(companyDetails.taxId){
        this.view.companyDetailsEntry21.tbxEnterValue.skin = "txtD7d9e0disabledf3f3f3NoBorder";
        this.view.companyDetailsEntry21.tbxEnterValue.setEnabled(false);
      }
      //disable business type
      if(this.completeCompanyDetails.customerContext.length > 0){
        this.view.lstBoxCompanyDetails12.skin = "sknLstBxBre1e5edBgf5f6f813pxDisableHover";
        this.view.lstBoxCompanyDetails12.setEnabled(false);
      }
    }
    /*********************************************************/
    /* map feautres and actions fro edit company */
    this.selectedFeaturesforEdit(companyCompleteDetails.featuresContext);    
  },
  updatedAddedRemovedList: function(a1,a2) {
    return a1.filter(function(x) {
      if(a2.indexOf(x) >= 0) return false;
      else return true;
    }); 
  },
  
  generateAddedRemovedFeatures : function(){
    var finalFeatureList = [];
    var mandatoryData = this.view.segmandatoryFeatures.data;
    var additionalData = this.view.segFeatures.data;
    var finalData = [];
    for(var a = 0;a<additionalData.length;a++){
      finalData.push(additionalData[a]);
    }
    for(var i=0;i<finalData.length;i++){
      if(finalData[i].selected === "true"){
        finalFeatureList.push(finalData[i]);
      }
    }
    for(var m = 0;m<mandatoryData.length;m++){
      finalFeatureList.push(mandatoryData[m]);
    }
    var intialList = this.intialFeatureList;
    var finalList = finalFeatureList;
	var intialListID,finalListID;
    intialListID = intialList.map(function(data){
      return data.featureId;
    });
   finalListID = finalList.map(function(data){
      return data.featureId;
    });
    
    var addedIds = this.updatedAddedRemovedList(finalListID,intialListID);
    var removededIds = this.updatedAddedRemovedList(intialListID,finalListID);
    return{
      addedIds : addedIds,
      removedIds : removededIds
    };
  },
  generateAddedRemovedLimits : function(){
    var intialList= this.initialLimitList;
    var finalList = this.view.segAssignLimits.data;
    var intialListID,finalListID;
    var addedData = [];
    intialListID = intialList.map(function(data){
      var id = data.actionId ? data.actionId : data.actions.actionId;
      return id;
    });
    finalListID = finalList.map(function(data){
      return data.actionId;
    });

    var addedIds = this.updatedAddedRemovedList(finalListID,intialListID);
    var removededIds = this.updatedAddedRemovedList(intialListID,finalListID);
    for (var i=0;i<finalList.length;i++){
      for(var j=0;j<addedIds.length;j++){
        if(finalList[i].actionId === addedIds[j]){
          addedData.push(finalList[i]);
        }
      }
    }
    return{
      addedIds : addedIds,
      removedIds : removededIds,
      addedData : addedData
    };
  },
    /*
   * map accounts to create company accounts module right section 
   */
  mapCompanyAccountsToEditCompany: function (accounts) {
    var self = this;
    var widgetMap = {
      "flxClose":"flxClose",
      "fontIconClose":"fontIconClose",
      "lblOption":"lblOption",
      "flxOptionAdded":"flxOptionAdded",
      "completeRecord" : "completeRecord"
    };
    accounts = this.mapAvailableSegmentData(accounts);
    accounts = accounts.map(
      function(account){
        return {
          "flxClose": {
            "onClick": function () {
              self.removeSelectedAccount();
            }
          },
          "fontIconClose": {
            "text": "\ue929",
            "tooltip": "Remove Account"
          },
          "lblOption": {"text":"Account  " + self.AdminConsoleCommonUtils.getTruncatedString(account.lblAccountNum.text, 15, 12),
                        "tooltip": account.lblAccountNum.text},
          "template": "flxOptionAdded",
          "completeRecord" : account.completeRecord
        };
      }
    );
    self.assignedAccount = Array.from(accounts);    
    self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
    self.view.addAndRemoveAccounts.segSelectedOptions.setData(accounts,0);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = accounts;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  /*
   * customercentric - mapaccounts to segments for edit
   */
  mapCustCentricAccountsforEdit: function (accounts) {
    var self = this;
    var widgetMap = {
      "flxCompanySelectedAccountsSec":"flxCompanySelectedAccountsSec",
      "flxAccountHeaderSection":"flxAccountHeaderSection",
      "lblSectionName":"lblSectionName",
      "flxClose":"flxClose",
      "lblSectionLine":"lblSectionLine",
      "fontIconClose":"fontIconClose",
      "flxCompanySelectedAccountsRow":"flxCompanySelectedAccountsRow",
      "flxAccountRowContainer":"flxAccountRowContainer",
      "flxAccountRowRecord":"flxAccountRowRecord",
      "lblRecordName":"lblRecordName",
      "fontIconClose":"fontIconClose",
      "completeRecord":"completeRecord"
    };
    accounts = this.mapAvailableSegmentData(accounts);
    var groupedAccounts = this.groupAccountsByCIF(accounts,1);
    var segData = [];
    var custIdList = Object.keys(groupedAccounts);
    for(var i=0; i< custIdList.length; i++){
      var sectionData = self.mapSelectedAccountsSection(groupedAccounts[custIdList[i]][0]);
      var rows = groupedAccounts[custIdList[i]].map(self.mapSelectedAccountsRow);
      segData.push([sectionData,rows]);
    }
    self.assignedAccount = Array.from(accounts);    
    self.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
    segData = self.setSkinsToSelectedAccountsSection(segData);
    self.view.addAndRemoveAccounts.segSelectedOptions.setData(segData);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = segData;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },
  editCompanySuccess:function(context){
    // create payload
    var self = this;
    var success = function(res){
      self.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),"Company Details Updated Successfully");
      self.showCreatedCompanyDetails(context.id, 1);
    };
    var error = function(err){
      kony.print("could not suspend features");
    };
    var data = this.view.segEditSuspendedFeature.data;
	var finalList = [];
    for(var i = 0;i<data.length;i++){
      if(data[i].switchSuspend.selectedIndex === 1){
        finalList.push(data[i].featureId);
      }
    }
    var payload = {
      "id": context.id,
      "Type": self.completeCompanyDetails.CompanyType,
      "suspendedFeatures": finalList
    };
    
    this.presenter.suspendFeature(payload,success,error);
  },
  changeUIAccessBasedOnEditCreateCompanyFlow:function(){
    if(this.action === this.actionConfig.create){
      this.view.flxOwnerDetailsRow1.setEnabled(true);
      this.view.flxOwnerDetailsRow2.setEnabled(true);
      this.view.customCalOwnerDOB.setEnabled(true);
      this.view.flxDOBDisabled.setVisibility(false);

      this.view.typeHeadState.tbxSearchKey.setEnabled(false);
      this.view.typeHeadCity.tbxSearchKey.setEnabled(false);
      this.view.lstBoxCompanyDetails12.selectedKey = "select";
      this.view.lblCompanyDetailsHeading.text =kony.i18n.getLocalizedString("i18n.frmCompaniesController.CompanyIsAssociatedWithMultipleCustomers");
    }else{
      this.view.flxOwnerDetailsRow1.setEnabled(false);
      this.view.flxOwnerDetailsRow2.setEnabled(false);
      this.view.customCalOwnerDOB.setEnabled(false);
      this.view.flxDOBDisabled.setVisibility(true);
      this.view.flxDOBDisabled.onClick = function(){};

      this.view.breadcrumbs.lblCurrentScreen.text = this.completeCompanyDetails.CompanyContext ? (this.completeCompanyDetails.CompanyContext[0].Name).toUpperCase() : "COMPANY";//kony.i18n.getLocalizedString("i18n.frmCompanies.EditCompany");
    }
    if(this.isAccountCentricConfig === true){
      this.view.lblCompanyDetailsHeading.text =kony.i18n.getLocalizedString("i18n.frmCompaniesController.CompanyIsAssociatedWithMultipleAccounts");
    }
  },
  showAccountSearchLoading: function(){
  },
  hideAccountSearchLoading: function(){
  },
  getTransactionDateCustom: function (Num, target) {
    var date = this.getCustomDate(Num, target);
    return this.getTransactionDateForServiceCall(date);
  },
  setDataForProductTransactionSegment: function (target) {
    var dataMap = {
      "flxCustMangRequestHeader": "flxCustMangRequestHeader",
      "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
      "flxFirstColoum": "flxFirstColoum",
      "lblAmountOriginalSign": "lblAmountOriginalSign",
      "lblAmountOriginalSymbol": "lblAmountOriginalSymbol",
      "lblAmountOriginal": "lblAmountOriginal",
      "lblAmountConvertedSign": "lblAmountConvertedSign",
      "lblAmountConvertedSymbol": "lblAmountConvertedSymbol",
      "lblAmountConverted": "lblAmountConverted",
      "lblDateAndTime": "lblDateAndTime",
      "lblRefNo": "lblRefNo",
      "lblSeperator": "lblSeperator",
      "lblTransctionDescription": "lblTransctionDescription",
      "lblType": "lblType"
    };
    var toAdd;
    var data = [];
    if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful") ||
        target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Pending")) {

      for (var i = 0; i < this.AccountTrasactions.length; i++) {
        var newDate = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[i].transactionDate.replace("T", " ").replace("Z", ""));
        if (this.AccountTrasactions[i].statusDescription &&
            this.AccountTrasactions[i].statusDescription.toUpperCase() === target.toUpperCase()) {
          toAdd = {
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
            "flxFirstColoum": "flxFirstColoum",
            "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
            "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
            "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
            "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ?
            this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
            "lblDateAndTime": this.getLocaleDateAndTime(newDate),
            "lblRefNo": this.AccountTrasactions[i].transactionId,
            "lblSeperator": ".",
            "lblTransctionDescription": this.AccountTrasactions[i].description,
            "lblType": this.AccountTrasactions[i].transactiontype,
            "template": "flxCustMangTransactionHistoryCompanies"
          };
          data.push(toAdd);
        } else if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Successful")) {
          toAdd = {
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
            "flxFirstColoum": "flxFirstColoum",
            "lblAmountOriginalSign": this.AccountTrasactions[i].amount && this.AccountTrasactions[i].amount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountOriginalSymbol": this.AccountTrasactions[i].amount ? this.defaultCurrencyCode(this.AccountTrasactions[i].baseCurrency) : "",
            "lblAmountOriginal": this.AccountTrasactions[i].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].amount)) : "-",
            "lblAmountConvertedSign": this.AccountTrasactions[i].convertedAmount && this.AccountTrasactions[i].convertedAmount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountConvertedSymbol": this.AccountTrasactions[i].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[i].transactionCurrency) : "",
            "lblAmountConverted": this.AccountTrasactions[i].convertedAmount ?
            this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[i].convertedAmount)) : "-",
            "lblDateAndTime": this.getLocaleDateAndTime(newDate),
            "lblRefNo": this.AccountTrasactions[i].transactionId,
            "lblSeperator": ".",
            "lblTransctionDescription": this.AccountTrasactions[i].description,
            "lblType": this.AccountTrasactions[i].fromAccountType,
            "template": "flxCustMangTransactionHistoryCompanies"
          };
          data.push(toAdd);
        }
      }
    } else if (target === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.Scheduled")) {
      for (var j = 0; j < this.AccountTrasactions.length; j++) {
        var newDate1 = this.getFormattedDataTimeFromDBDateTime(this.AccountTrasactions[j].transactionDate.replace("T", " ").replace("Z", ""));
        if (this.AccountTrasactions[j].isScheduled === "true") {
          toAdd = {
            "flxCustMangRequestHeader": "flxCustMangRequestHeader",
            "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
            "flxFirstColoum": "flxFirstColoum",
            "lblAmountOriginalSign": this.AccountTrasactions[j].amount && this.AccountTrasactions[j].amount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountOriginalSymbol": this.AccountTrasactions[j].amount ? this.defaultCurrencyCode(this.AccountTrasactions[j].baseCurrency) : "",
            "lblAmountOriginal": this.AccountTrasactions[j].amount ? this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[j].amount)) : "-",
            "lblAmountConvertedSign": this.AccountTrasactions[j].convertedAmount && this.AccountTrasactions[j].convertedAmount.substring(0, 1) === "-" ? "-" : "",
            "lblAmountConvertedSymbol": this.AccountTrasactions[j].convertedAmount ? this.defaultCurrencyCode(this.AccountTrasactions[j].transactionCurrency) : "",
            "lblAmountConverted": this.AccountTrasactions[j].convertedAmount ?
            this.formatCurrencyByDeletingSign(this.getCurrencyFormat(this.AccountTrasactions[j].convertedAmount)) : "-",
            "lblDateAndTime": this.getLocaleDateAndTime(newDate1),
            "lblRefNo": this.AccountTrasactions[j].transactionId,
            "lblSeperator": ".",
            "lblTransctionDescription": this.AccountTrasactions[j].description,
            "lblType": this.AccountTrasactions[j].transactiontype,
            "template": "flxCustMangTransactionHistoryCompanies"
          };
          data.push(toAdd);
        }
      }
    }
    this.view.transactionHistorySearch.tbxSearchBox.text = "";
    if (data.length > 0) {
      toAdd = {
        "flxCustMangRequestHeader": "flxCustMangRequestHeader",
        "flxCustMangTransactionHistoryCompanies": "flxCustMangTransactionHistoryCompanies",
        "flxFirstColoum": "flxFirstColoum",
        "lblAmountOriginalSign": "",
        "lblAmountOriginalSymbol": "",
        "lblAmountOriginal": "",
        "lblAmountConvertedSign": "",
        "lblAmountConvertedSymbol": "",
        "lblAmountConverted": "",
        "lblDateAndTime": "",
        "lblRefNo": "",
        "lblSeperator": ".",
        "lblTransctionDescription": kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LOAD_MORE"),
        "lblType": "",
        "template": "flxCustMangTransactionHistoryCompanies"
      };
      this.view.segTransactionHistory.widgetDataMap = dataMap;
      this.view.segTransactionHistory.setData(data);
      this.view.segTransactionHistory.info = {
        "data": data,
        "searchAndSortData": data
      };
      this.view.segTransactionHistory.setVisibility(true);

      this.view.flxTransactionHistorySegmentHeader.setVisibility(true);
      this.view.flxSeperator4.setVisibility(true);
      this.view.rtxMsgTransctions.setVisibility(false);
      this.view.transactionHistorySearch.flxDownload.setVisibility(true);
    } else {
      this.view.segTransactionHistory.info = {
        "data": [],
        "searchAndSortData": []
      };
      this.view.transactionHistorySearch.flxDownload.setVisibility(false);
      this.view.segTransactionHistory.setVisibility(false);
      this.view.flxTransactionHistorySegmentHeader.setVisibility(false);
      this.view.flxSeperator4.setVisibility(false);
      this.view.rtxMsgTransctions.setVisibility(true);
    }

    this.showProductTransactionHistory();
  },
  showProductTransactionHistory: function () {
    this.view.flxProductHeaderAndDetails.setVisibility(true);
    this.view.flxProductInfoWrapper.setVisibility(true);
    this.view.flxProductDetails.setVisibility(true);
    this.view.forceLayout();
    var flxScroll = document.getElementById("frmCompanies_flxScrollTransctionsSegment");
    var scrollWidth = flxScroll.offsetWidth - flxScroll.clientWidth;
    this.view.flxTransactionHistorySegmentHeader.right = 35 + scrollWidth + "px";
    this.view.forceLayout();
  },
  getTransactionsBasedOnDate: function () {
    var scopeObj = this;
    var index = scopeObj.view.segCompanyDetailAccount.selectedRowIndex[1];
    var data = scopeObj.view.segCompanyDetailAccount.data;
    var acctNo = data[index].lblAccountNumber.text;
    var StartDate;
    var EndDate;
    var rangeType = scopeObj.view.customCalCreatedDate.value;
    if (rangeType !== "") {
      StartDate = rangeType.substring(0, rangeType.indexOf(" - "));
      var mm = StartDate.substr(0, 2);
      var dd = StartDate.substr(3, 2);
      var yyyy = StartDate.substr(6, 4);
      StartDate = yyyy + "-" + mm + "-" + dd + " 00:00:00";
      EndDate = rangeType.substring(rangeType.indexOf(" - ") + 3);
      var mm1 = EndDate.substr(0, 2);
      var dd1 = EndDate.substr(3, 2);
      var yyyy1 = EndDate.substr(6, 4);
      EndDate = yyyy1 + "-" + mm1 + "-" + dd1 + " 00:00:00";
    }
    kony.adminConsole.utils.showProgressBar(scopeObj.view);
    scopeObj.presenter.getAccountTransactions({
      "AccountNumber": acctNo,
      "StartDate": StartDate,
      "EndDate": EndDate
    });
  },
  setDataForProductDetailsScreen: function (CustomerAccounts) {
    var scopeObj = this;
    var customerDetails = CustomerAccounts[0];

    var status = customerDetails.StatusDesc || kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
    //set data
    this.view.backToAccounts.btnBack.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNTS_BTN_BACK");
    this.view.AccountsHeader.lblProductCardName.text = customerDetails.AccountName?customerDetails.AccountName:kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
    this.view.AccountsHeader.lblCardStatus.text = status;

    var StatusImg;
    //TODO: all images for status
    if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.secureimage.Active").toUpperCase()) {
      StatusImg = "sknFontIconActivate";
    } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Suspended").toUpperCase()) {
      StatusImg = "sknFontIconSuspend";
    } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.Closed").toUpperCase()) {
      StatusImg = "sknfontIconInactive";
    } else if (status.toUpperCase() === kony.i18n.getLocalizedString("i18n.frmCustomers.NA").toUpperCase()) {
      StatusImg = "sknfontIconInactive";
    }

    this.view.AccountsHeader.fontIconCardStatus.skin = StatusImg;
    if (customerDetails.IBAN) {
      this.view.AccountsHeader.lblIBAN.isVisible = true;
      this.view.AccountsHeader.lblIBANLabel.isVisible = true;
      this.view.AccountsHeader.lblIBAN.text = customerDetails.IBAN;
      this.view.AccountsHeader.lblIBAN.width = (8.75 * (customerDetails.IBAN).length) + "px";
      this.view.AccountsHeader.lblIBAN.right = "0px";
      this.view.AccountsHeader.lblIBANLabel.right = ((8.75 * (customerDetails.IBAN).length)) + "px";
    }
    else {
      this.view.AccountsHeader.lblIBAN.isVisible = false;
      this.view.AccountsHeader.lblIBANLabel.isVisible = false;
    }
    this.view.AccountsHeader.flxAccountDetailsColumn2.right = "2px";
    this.view.AccountsHeader.blAccountNumber.left = undefined;
    if (customerDetails.Account_id) {
      this.view.AccountsHeader.blAccountNumber.isVisible = true;
      this.view.AccountsHeader.lblAccountNumberLabel.isVisible = true;
      var cardOrAccountNumber = customerDetails.Account_id;
      this.view.AccountsHeader.blAccountNumber.text = cardOrAccountNumber;
      this.view.AccountsHeader.blAccountNumber.width = (8.75 * cardOrAccountNumber.length) + "px";
      if (customerDetails.IBAN) {
        var accountNumberRight = (8.75 * (customerDetails.IBAN).length) + 50;
        this.view.AccountsHeader.blAccountNumber.right = accountNumberRight + "px";
        this.view.AccountsHeader.lblAccountNumberLabel.right = (accountNumberRight + (8.75 * cardOrAccountNumber.length)) + "px";

      }
      else {
        this.view.AccountsHeader.blAccountNumber.right =  "0px";
        this.view.AccountsHeader.lblAccountNumberLabel.right = ((8.75 * cardOrAccountNumber.length)) + "px";
      }
    } else {
      this.view.AccountsHeader.blAccountNumber.isVisible = false;
      this.view.AccountsHeader.lblAccountNumberLabel.isVisible = false;
    }

    //row1
    this.view.ProductRow1.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CREATED_ON");
    this.view.ProductRow1.lblData1.text = customerDetails.OpeningDate ? scopeObj.getLocaleDate(customerDetails.OpeningDate) : kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    this.view.ProductRow1.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CURRENT_BALANCE");
    this.view.ProductRow1.lblSignData2.left = "-7px";
    if(customerDetails.CurrentBalance){
      this.view.ProductRow1.lblSignData2.text = customerDetails.CurrentBalance.substring(0, 1) === "-" ? "-" : "";
      this.view.ProductRow1.lblIconData2.text = this.defaultCurrencyCode(customerDetails.CurrencyCode);
      this.view.ProductRow1.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.CurrentBalance));
    }else{
      this.view.ProductRow1.lblData2.text = kony.i18n.getLocalizedString("i18n.Applications.NA");
    }

    this.view.ProductRow1.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_RATE");
    this.view.ProductRow1.lblData3.text = customerDetails.DividendRate ? customerDetails.DividendRate : kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    //row 2
    this.view.ProductRow2.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP");
    this.view.ProductRow2.lblData1.text = customerDetails.ownership=="joint" ? kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT") : kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SINGLE");
    this.view.ProductRow2.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.AVAILABLE_BALANCE");
    this.view.ProductRow2.lblSignData2.left = "-7px";

    if(customerDetails.AvailableBalance){
      this.view.ProductRow2.lblSignData2.text = customerDetails.AvailableBalance.substring(0, 1) === "-" ? "-" : "";
      this.view.ProductRow2.lblIconData2.text = this.defaultCurrencyCode(customerDetails.CurrencyCode);
      this.view.ProductRow2.lblData2.text = this.formatCurrencyByDeletingSign(this.getCurrencyFormat(customerDetails.AvailableBalance));
    }else{
      this.view.ProductRow2.lblData2.text = kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
    }

    this.view.ProductRow2.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.DIVIDEND_PAID_(YTD)");
    this.view.ProductRow2.lblData3.text = customerDetails.DividendPaidYTD ? customerDetails.DividendPaidYTD:kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    //row 3
    this.view.ProductRow3.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.JOINT_HOLDER_NAME");

    var accountHolder = customerDetails.AccountHolder;
    if (accountHolder === null || accountHolder === undefined) {
      accountHolder = kony.i18n.getLocalizedString("i18n.frmCustomers.NA")
    } else if (typeof accountHolder === 'string') {
      try {
        accountHolder = JSON.parse(accountHolder);
      } catch (e) {
        accountHolder = kony.i18n.getLocalizedString("i18n.frmCustomers.NA");
        kony.print("Account Holder is not a valid json object " + e);
      }
    }

    var currentUsername = this.view.mainHeader.lblUserName;
    this.view.ProductRow3.lblData1.setVisibility(false);

    if (accountHolder !== kony.i18n.getLocalizedString("i18n.frmCustomers.NA")) {
      if (currentUsername !== accountHolder.username) {
        this.view.ProductRow3.lblData1.setVisibility(true);
        this.view.ProductRow3.lblData1.text = accountHolder.fullname+ " (Primary)";
      } else {
        this.view.ProductRow3.lblData1.setVisibility(true);
        this.view.ProductRow3.lblData1.text = this.view.generalInfoHeader.lblCustomerName.info.FirstName + " " +
          this.view.generalInfoHeader.lblCustomerName.info.LastName + " (Primary)";
      }
    } else {
      this.view.ProductRow3.lblData1.setVisibility(true);
      this.view.ProductRow3.lblData1.text = accountHolder;
    }


    this.view.ProductRow3.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ROUTING_NUMBER");
    this.view.ProductRow3.lblData2.text = customerDetails.RoutingNumber === undefined ? kony.i18n.getLocalizedString("i18n.frmCustomers.NA") : customerDetails.RoutingNumber;

    this.view.ProductRow3.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID");
    this.view.ProductRow3.lblData3.text = customerDetails.LastDividendPaidAmount?customerDetails.LastDividendPaidAmount:kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    //row 4
    this.view.ProductRow4.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_UPDATE_ON");
    this.view.ProductRow4.lblData1.text = customerDetails.LastPaymentDate ? this.getFormattedDataTimeFromDBDateTime(customerDetails.LastPaymentDate) : kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    this.view.ProductRow4.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SWIFT_CODE");
    this.view.ProductRow4.lblData2.text = customerDetails.SwiftCode === undefined ? kony.i18n.getLocalizedString("i18n.frmCustomers.NA") : customerDetails.SwiftCode;

    this.view.ProductRow4.lblHeading3.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.LAST_DIVIDEND_PAID_ON");
    this.view.ProductRow4.lblData3.text = customerDetails.DividendLastPaidDate?scopeObj.getLocaleDate(customerDetails.DividendLastPaidDate):kony.i18n.getLocalizedString("i18n.frmCustomers.NA");

    //row 5
    this.view.ProductRow5.setVisibility(false);

    var eStatementStatus = customerDetails.EStatementmentEnable;

    if (eStatementStatus == "true") {
      this.view.imgRbEstatement.src = "radio_selected.png";
      this.view.imgRbPaper.src = "radio_notselected.png";
      this.view.forceLayout();
    }
    else {
      this.view.imgRbEstatement.src = "radio_notselected.png";
      this.view.imgRbPaper.src = "radio_selected.png";
    }
  },
  fetchAccountsForMicroBusinessBanking: function(){
    var scopeObj = this;
    var payload = {
      "Membership_id":scopeObj.view.lblMembershipValue.text,
      "Taxid":scopeObj.view.lblTinValue.text
    };
    scopeObj.showAccountSearchLoading();
    scopeObj.presenter.getAllAccounts(payload);    
  },
  backToCompanyDetails: function(tabselection){
    this.view.settingsMenu.setVisibility(false);
    this.view.breadcrumbs.btnBackToMain.text = kony.i18n.getLocalizedString("i18n.frmCompanies.SearchContract");
    
    this.view.breadcrumbs.lblCurrentScreen.text = this.completeContractDetails.name ? (this.completeContractDetails.name).toUpperCase() : "COMPANY";
    this.view.flxInlineError.setVisibility(false);
    this.hideRequiredMainScreens(this.view.flxCompanyDetails);
    this.view.flxBreadcrumb.setVisibility(true);
    this.view.mainHeader.btnAddNewOption.setVisibility(false);
    this.view.mainHeader.btnDropdownList.setVisibility(false);
    this.view.flxSettings.setVisibility(false);
    if(tabselection === 1){
      this.view.flxViewTab1.onTouchEnd();
    } else if(tabselection === 4){
      this.view.flxViewTab4.onTouchEnd();    
    }
  },
  /*
   * onclick handler for create company details next
   */  
  createCompanyAccountsNextClickHandler:function(){
    var scopeObj = this;
    var isValidAccounts = scopeObj.checkAccountValidation();
    if(!isValidAccounts){
      this.view.flxAccountError.setVisibility(true);
      this.view.flxAccountsAddAndRemove.top = "60dp";
    }else{
      this.view.flxAccountError.setVisibility(false);
      this.view.flxAccountsAddAndRemove.top = "0dp";
      this.showCompanyDetailsScreen();
      this.setTaxIdInCompanyDetailsScreen();
      //to prefill company details in case of single cust account selected
      if(this.isAccountCentricConfig === false){
        var selectedAcc = this.view.addAndRemoveAccounts.segSelectedOptions.data;
        if(selectedAcc.length === 1 ){
          var custId = this.view.lblCompanyDetailsHeading.text.split(": ");
          if(custId.length === 2 && custId[1] === (selectedAcc[0][0].CustomerId)){
            //selected account is not changed and data for details is already filled
          } else{ //in case selected account is changed
            this.view.lblCompanyDetailsHeading.text = kony.i18n.getLocalizedString("i18n.customerSearch.MemberID")+": "+selectedAcc[0][0].CustomerId;
            this.presenter.getMembershipDetails({"Membership_id": selectedAcc[0][0].CustomerId});
          }
          
        } else{
          this.view.companyDetailsEntry11.tbxEnterValue.setEnabled(true);
          this.view.companyDetailsEntry11.tbxEnterValue.skin = "sknTbx485c75LatoReg13pxNoBor";
          this.view.lblCompanyDetailsHeading.text = kony.i18n.getLocalizedString("i18n.frmCompaniesController.CompanyIsAssociatedWithMultipleCustomers");
        }
      }
    }
  },
  /*
   * Clears all the selected account in accounts tab.
   * 
   */  
  clearCreateCompanySelectedAccounts:function(){
    this.assignedAccount = [];
    this.view.addAndRemoveAccounts.segSelectedOptions.setData([]);
    this.view.addAndRemoveAccounts.segSelectedOptions.info.segData = [];
    this.view.forceLayout();    
  },
  /*
   * Track if there is any change in membership/tin values that previously entered and if there is 
   * any change clears all the previously selected accounts.
   */ 
  /* currently not required
  trackCreateCompanyMembershipIdAndTinChange:function(){
    var companyType = this.view.radioCompanyType.selectedKey;
    if(this.action == this.actionConfig.create){
      if(this.view.radioCompanyType.selectedKey=="TYPE_MICRO_BUSINESS"){ 
        var tin = this.view.companyDetailsEntry21.tbxEnterValue.text;
        if(this.microBusinessInputs.TIN != tin || this.microBusinessInputs.CompanyType != companyType){
          this.microBusinessInputs.TIN = tin;
          this.microBusinessInputs.CompanyType = companyType;
        }
      }else{
        if(this.microBusinessInputs.CompanyType != companyType){
          this.microBusinessInputs.CompanyType = companyType;
        }        
      }
    }
  },*/
  sortIconFor: function(column,iconPath){
    var self =this;
    self.determineSortFontIcon(this.sortBy,column,self.view[iconPath]);
  },
  resetSortImages : function(context) {
    var self = this;
    if (context === "searchList") {
      self.sortIconFor('lblCompanyName.info.value', 'fontIconSortName');
      self.sortIconFor('lblEmail', 'fontIconSortRoles');
      self.sortIconFor('lblCompanyId', 'fontIconSortCompanyId');
      
    } else if(context === "accounts"){
      self.sortIconFor('lblAccountType.text', 'lblAccountTypeSortIcon');
      self.sortIconFor('lblAccountName.text', 'lblAccountNameSortIcon');
      self.sortIconFor('lblAccountNumber.text', 'lblAccountNumberSortIcon');
      self.sortIconFor('lblStatus.text', 'lblStatusSortIcon');
    } else if(context === "businessUsers"){
      self.sortIconFor('lblName.text', 'lblCustomerNameSortIcon');
      self.sortIconFor('lblRole.text', 'lblRoleSortIcon');
      self.sortIconFor('lblUsername.text', 'lblUserNameSortIcon');
      self.sortIconFor('lblEmail.text', 'lblCustomerEmailIDSortIocn');
      self.sortIconFor('lblCustomerStatus.text', 'lblCustomerStatusSortIcon');
    } else if(context === "transactions"){
      self.sortIconFor('lblRefNo','fonticonSortTranasctionRefNo');
      self.sortIconFor('lblDateAndTime','fonticonSortTransactionDateAndTime');
      self.sortIconFor('lblType','fonticonSortTransactionType');
      self.sortIconFor('lblAmountOriginal','fonticonSortTransactionAmountOriginal');
      self.sortIconFor('lblAmountConverted','fonticonSortTransactionAmountConverted');
    }
  },
  sortAndGetData : function(segData, sortColumn, context) {
    var self = this;
    self.sortBy.column(sortColumn);
    self.resetSortImages(context);
    return segData.sort(self.sortBy.sortData);
  },
  hideAllOptionsButtonImages : function(){
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsCompany.flxImgArrow1,
       this.view.verticalTabsCompany.flxImgArrow2,
       this.view.verticalTabsCompany.flxImgArrow3,
       this.view.verticalTabsCompany.flxImgArrow4],
      "" );
  },  
  calWidgetFormatDate :function(dateString) {
    var yyyy = +dateString.substr(0, 4);
    var mm = +dateString.substr(5, 2);
    var dd = +dateString.substr(8, 2);
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    return  mm + "/" + dd + "/" + yyyy;
  },
  sortForTransactions: function (SegmentWidget, columnName, sortImgWidget, defaultScrollWidget, NumberofRecords, LoadMoreRow) {
    var data = SegmentWidget.info.searchAndSortData;
    var sortdata = this.sortAndSetData(data, columnName, "transactions");
    if (NumberofRecords === kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ALL")) {
      SegmentWidget.info.searchAndSortData = sortdata;
      SegmentWidget.setData(sortdata);
    } else if (data.length > NumberofRecords) {
      SegmentWidget.info.searchAndSortData = sortdata;
      var newData = sortdata.slice(0, NumberofRecords);
      if (LoadMoreRow !== null) newData.push(LoadMoreRow);
      SegmentWidget.setData(newData);
    } else {
      SegmentWidget.info.searchAndSortData = sortdata;
      SegmentWidget.setData(sortdata);
    }
    this.sortIconFor(columnName, sortImgWidget);
    this.view.forceLayout();
  },
  hideAddressSegments : function(typeHeadPath){
    this.view.typeHeadCity.segSearchResult.setVisibility(false);
    this.view.typeHeadCountry.segSearchResult.setVisibility(false);
    this.view.typeHeadState.segSearchResult.setVisibility(false);
    if(typeHeadPath){
      typeHeadPath.segSearchResult.setVisibility(true);
    }
    this.view.forceLayout();
  },
  /*
   * function to get address codes of city,state,country populated
   */
  getAddressCodes : function(name){
    var self = this;
    var country = self.segCountry;
    var state = self.segState;
    //country
    var r1 = country.filter(function(rec){
      if(rec.lblAddress.text.indexOf(name[0]) >= 0){
        return rec;
      }
    });
    self.view.typeHeadCountry.tbxSearchKey.info.data = r1[0] ||{};
    //state
    var r2 = state.filter(function(rec){
      if(rec.lblAddress.text.indexOf(name[1]) >= 0){
        return rec;
      }
    });
    self.view.typeHeadState.tbxSearchKey.info.data = r2[0] ||{};
    //city
    self.view.typeHeadCity.tbxSearchKey.info.data = {};
  },
  returnEnteredText : function(name, email) {
    var enteredText = "";
    if(name !== null && name !== "") {
      if(email !== null && email !== "") {
        enteredText = name + " and " + email;
      }
      else {
        enteredText = name;
      }
    }else if(email !== null && email !== "") {
      enteredText = email;
    }
    return enteredText;
  },
  /*
   * function to navigate to customer profile form on click of business user name
   */
  onClickUserNavigateToProfile : function(){
    var self = this;
    var data = self.view.segCompanyDetailCustomer.data;
    var index = self.view.segCompanyDetailCustomer.selectedRowIndex[1];
    var rowData = data[index];
    var param = {
      "Customer_id": rowData.customerEditInfo.id
    };
    
    self.presenter.navigateToCustomerPersonal(param,{"name":"frmCompanies" ,
    'data': { 
      'breadcrumbValue': [kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SEARCHCUSTOMERS"),contractName] , 
      'previousPageCallBack':()=>{
        let x = new kony.mvc.Navigation('frmCompanies');
        x.navigate();  
      }
    }});
  },
  /*
   * function to navigate to customer profile form on click of business user name
   */
  onClickSignUserNavigateToProfile : function(){
    var self = this;
    var data = self.view.segSignatoriesDetail.data;
    var index = self.view.segSignatoriesDetail.selectedRowIndex[1];
    var rowData = data[index];
    var param = {
      "Customer_id": rowData.customerId
    };
    
    kony.adminConsole.utils.showProgressBar(this.view);
    let contractName = this.view.lblCompanyDetailName.text;
    self.presenter.navigateToCustomerPersonal(param,{"name":"frmCompanies" , 'data': { 'breadcrumbValue':contractName}});
  },
   hideAllTabsDetails : function() {
    var self =  this;
    var flexChildrenlength = self.view.flxDetailsContainer.children.length;
    for(var i = 0; i < flexChildrenlength; i++) {
      self.view[self.view.flxDetailsContainer.children[i]].setVisibility(false);
    }
   },
  setCompanyFeatures: function() {
    var self = this;
    var features = self.completeCompanyDetails.featuresContext;
    var dataMap = {
      "lblFeatureName": "lblFeatureName",
      "flxViewDetails": "flxViewDetails",
      "lblViewDetails":"lblViewDetails",
      "statusValue":"statusValue",
      "statusIcon":"statusIcon",
      // this is required for suspend features
      "featureData" : "featureData",
      "featureId"  : "featureId",
      "statusId" : "statusId"
    };
    var data = features.map(function(rec){
      var statusInfo = {};
      var featureStatus = rec.featureStatus;
      statusInfo = self.getFeatureStatusAndSkin(featureStatus);
      return{
        "lblFeatureName":rec.featureName,
        "flxViewDetails":{
          "hoverSkin": "sknFlxPointer",
          "onClick" : function(){
            self.fillFeatureDetails(rec);
          }
        },   
        "statusValue":{
          "text":statusInfo.status
        },
        "statusIcon":{
          "skin":statusInfo.statusIconSkin,
          "text":"\ue921"
        },
        "lblViewDetails" : {
          "text":kony.i18n.getLocalizedString("i18n.frmGroupsController.viewDetails")
        },
        // this is required for suspend features
        "featureData" : rec,
        "statusId" : rec.featureStatus,
        "featureId"  : rec.featureId
      };
    });
    var featuresJSON = self.checkForPrimaryFeatures(data);
    self.view.segCompanyFeatures.widgetDataMap = dataMap;
    if(featuresJSON.additional.length <=0){
      self.view.segCompanyFeatures.setData([]);
      self.view.flxNoAdditionalFeatures.setVisibility(true);
      self.view.segCompanyFeatures.setVisibility(false);
      self.view.btnShowSuspendFeature.setVisibility(false);
    }else{
      self.view.segCompanyFeatures.setData(featuresJSON.additional);
      self.view.flxNoAdditionalFeatures.setVisibility(false);
      self.view.segCompanyFeatures.setVisibility(true);
      self.view.btnShowSuspendFeature.setVisibility(true);
    }
    self.view.segCompanyMandatoryFeatures.setData(featuresJSON.mandatory);
    featuresJSON = {};
    self.view.forceLayout();
  },
  subtract: function(a1,a2) {
    return a1.filter(function(x) {
      if(a2.indexOf(x) >= 0) return false;
      else return true;
    }); 
  },
  checkForPrimaryFeatures : function(data){
    var allMandatoryFeatures = this.requiredFeatures.mandatory;
    var mandatory = [] ,additional = [];
    
    for(var i=0;i<allMandatoryFeatures.length;i++){
      for(var j=0;j<data.length;j++){
    	if(allMandatoryFeatures[i].id === data[j].featureId){
          mandatory.push(data[j]);
        }
      }
    }
    additional = this.subtract(data,mandatory);
    return{
      additional : additional,
      mandatory : mandatory
    };
  },
  setEditSuspendFeatures : function(){
    var self = this;
    var data = this.view.segFeatures.data;
    var filteredData = [];
    for(var i=0;i< data.length;i++){
      if(data[i].selected === "true"){
        filteredData.push(data[i]);
      }
    }
    data = filteredData.map(self.suspendEditFeatureMapping);
    var widgetMap = {
      "flxSuspendFeature" : "flxSuspendFeature",
      "flxFeaturesContainer" : "flxFeaturesContainer",
      "flxFeatureUpper" : "flxFeatureUpper",
      "lblFeature" : "lblFeature",
      "flxFeaturesLower" : "flxFeaturesLower",
      "flxViewDetails" : "flxViewDetails",
      "lblViewFeatureDetails" : "lblViewFeatureDetails",
      "switchSuspend" : "switchSuspend"  ,
      "featureId" : "featureId"
    };
    this.view.segEditSuspendedFeature.widgetDataMap = widgetMap;
    this.view.segEditSuspendedFeature.setData(data);
	this.setEditSuspendCount();
    this.view.forceLayout();
  },
  setSuspendFeatures : function(){
    var self = this;
    var data = this.view.segCompanyFeatures.data;
    data = data.map(self.suspendFeatureMapping);
    var widgetMap = {
      "flxSuspendFeature" : "flxSuspendFeature",
      "flxFeaturesContainer" : "flxFeaturesContainer",
      "flxFeatureUpper" : "flxFeatureUpper",
      "lblFeature" : "lblFeature",
      "flxFeaturesLower" : "flxFeaturesLower",
      "flxViewDetails" : "flxViewDetails",
      "lblViewFeatureDetails" : "lblViewFeatureDetails",
      "switchSuspend" : "switchSuspend"  ,
      "featureId" : "featureId"
    };
    this.view.segSuspendedFeature.widgetDataMap = widgetMap;
    this.view.segSuspendedFeature.setData(data);
    this.setSuspendCount();
    this.view.forceLayout();
  },
  setEditSuspendCount : function(featureId){
    var data = this.view.segEditSuspendedFeature.data;
    var count = 0;
    for(var i=0;i<data.length;i++){
      if(data[i].switchSuspend.selectedIndex === 1){
        count++;
      }
    }
    if(count < 10)
      this.view.lblEditSuspendedFeaturesSelectedCount.text = "0" + count;
    else
      this.view.lblEditSuspendedFeaturesSelectedCount.text = count;    
    this.view.lblEditSuspendedFeaturesTotalCount.text = "of " + data.length;
    this.view.forceLayout();
  },
  changeFeatureStatusLocaly : function(id){
    //featureMapping
    var self = this;
    var statusInfo;
    var selectedIds = [];
    var suspendSegdata = this.view.segEditSuspendedFeature.data;
    var FeatureSegmentdata = this.view.segFeatures.data;
    var count = 0;
    for(var idCount=0;idCount<suspendSegdata.length;idCount++){
      if(suspendSegdata[idCount].switchSuspend.selectedIndex === 1){
        selectedIds.push(suspendSegdata[idCount].featureId);
      }
    }
    for(var x=0; x<FeatureSegmentdata.length;x++){
      for(var y=0;y<self.requiredFeatures.additional.length;y++){
        if(FeatureSegmentdata[x].featureId === self.requiredFeatures.additional[y].id){
          FeatureSegmentdata[x].localStatus = self.requiredFeatures.additional[y].status;
          statusInfo = this.getFeatureStatusAndSkin(FeatureSegmentdata[x].localStatus);
          FeatureSegmentdata[x].lblUsersStatus.text = statusInfo.status;
          FeatureSegmentdata[x].fontIconStatusImg.skin = statusInfo.statusIconSkin;
        }
      }
    }
    for(var i=0;i< FeatureSegmentdata.length;i++){
      for(var j=0;j<selectedIds.length;j++){
        if(FeatureSegmentdata[i].featureId === selectedIds[j]){ //&& FeatureSegmentdata[i].statusId !== "SID_FEATURE_SUSPENDED"){
          FeatureSegmentdata[i].localStatus = "SID_FEATURE_SUSPENDED";
          statusInfo = this.getFeatureStatusAndSkin(FeatureSegmentdata[i].localStatus);
          FeatureSegmentdata[i].lblUsersStatus.text = statusInfo.status;
          FeatureSegmentdata[i].fontIconStatusImg.skin = statusInfo.statusIconSkin;
        }
      }
    }
    this.view.segFeatures.setData(FeatureSegmentdata);
    this.setAdditionfeatureCount();
  },
  changeLimitStatusLocally : function(){
    var featureData = this.view.segFeatures.data;
    var limitsData  = this.view.segAssignLimits.data;
    for(var i=0;i<featureData.length;i++){
      for(var j=0;j<limitsData.length;j++){
        if(featureData[i].featureId === limitsData[j].featureId){
          var statusInfo = this.getFeatureStatusAndSkin(featureData[i].localStatus);
          limitsData[j].lblUsersStatus.text = statusInfo.status;
          limitsData[j].fontIconStatusImg.skin = statusInfo.statusIconSkin;
        }
      }
    }
    this.view.segAssignLimits.setData(limitsData);
  },
  setSuspendCount : function(){
    var data = this.view.segSuspendedFeature.data;
    var count = 0;
    for(var i=0;i<data.length;i++){
      if(data[i].switchSuspend.selectedIndex === 1){
        count++;
      }
    }
    if(count < 10)
      this.view.lblSuspendedFeaturesSelectedCount.text = "0" + count;
    else
      this.view.lblSuspendedFeaturesSelectedCount.text = count;
    this.view.lblSuspendedFeaturesTotalCount.text = "of "+data.length;
    this.view.forceLayout();
  },
  suspendEditFeatureMapping : function(data){
    var self = this;
    var isSuspended;
    if(data.localStatus === "SID_FEATURE_SUSPENDED" ){//|| data.statusId === "SID_FEATURE_SUSPENDED"){
      isSuspended = 1;
    }else{
      isSuspended = 0;
    }
    return{
      "lblFeature" : data.lblFeature,
      "flxViewDetails" : {
        "hoverSkin": "sknFlxPointer",
        "onClick" : function(){
          self.createEditViewFeatureDetail(data.featureData);
        }
      },
      "lblViewFeatureDetails" : "View Details",
      "switchSuspend" : {
        "selectedIndex" : isSuspended,
        "onSlide" : function(){self.setEditSuspendCount(data.featureId);}
      },
      "featureId" : data.featureId
    };
  },
  suspendFeatureMapping : function(data){
    var self = this;
    var isSuspended;
    if(data.statusId === "SID_FEATURE_SUSPENDED"){
      isSuspended = 1;
    }else{
      isSuspended = 0;
    }
    return{
      "lblFeature" : data.lblFeatureName,
      "flxViewDetails" : {
        "hoverSkin": "sknFlxPointer",
        "onClick" : function(){
          self.fillFeatureDetails(data.featureData);
        }
      },
      "lblViewFeatureDetails" : "View Details",
      "switchSuspend" : {
        "selectedIndex" : isSuspended,
        "onSlide" : self.setSuspendCount
      },
      "featureId" : data.featureId
    };
  },
  suspendFeatureRequest : function(){
    var self = this;
    var data = this.view.segSuspendedFeature.data;
	var finalList = [];
    for(var i = 0;i<data.length;i++){
      if(data[i].switchSuspend.selectedIndex === 1){
        finalList.push(data[i].featureId);
      }
    }
    var payload = {
      "id": this.completeCompanyDetails.CompanyContext[0].id,
      "Type": this.completeCompanyDetails.CompanyType,
      "suspendedFeatures": finalList
    };
    var success = function(res){
      var featurePayload = {
        "id": self.completeCompanyDetails.CompanyContext[0].id
      };
      var featureSuccess = function(res){
        self.completeCompanyDetails.featuresContext = res;
        self.suspendFeatureHideShow("additional_feature_screen");
      };
      var featureError = function(err){
        kony.print("failed to get company feature", err);
      };
      self.presenter.getCompanyFeatures(featurePayload,featureSuccess,featureError);
    };
    var error = function(err){
      kony.print("failed to suspend feature", err);
      //show toast
    };
    this.presenter.suspendFeature(payload,success,error);
  },
  setCompanyLimits: function() {
    var self = this;
    var features = self.completeCompanyDetails.featuresContext;
    var dataMap = {
      "lblFeatureName": "lblFeatureName",
      "lblData1": "lblData1",
      "lblData2":"lblData2",
      "lblData3":"lblData3",
      "statusIcon":"statusIcon",
      "statusValue":"statusValue",
      "lblHeading1": "lblHeading1",
      "lblHeading2": "lblHeading2",
      "lblHeading3": "lblHeading3",
    };
    var segFormatData = features.map(function(feature){
      var actions = feature.Actions|| [];
      var monetaryAction = [];
      //filter all monetary actions with limits
      for(var i=0; i<actions.length; i++){
        if(actions[i].actionType === "MONETARY" && actions[i].limits){
          monetaryAction.push(actions[i]);
        }
      }
      var rowData = {"featureName": feature.featureName,
                     "featureStatus":feature.featureStatus,
                     "actionLimits": monetaryAction};
      return rowData;
    });
    var data = segFormatData.map(function(rec){
      var limitsJson = {};
      var statusInfo = {};
      var featureStatus = rec.featureStatus;
      statusInfo = self.getFeatureStatusAndSkin(featureStatus);
      if ((rec.actionLimits.length > 0)) {
        limitsJson = rec.actionLimits[0].limits.reduce(function (mapJson, rec) {
          mapJson[rec.id] = rec.value;
          return mapJson;
        }, {});
      }
      return{
        "lblFeatureName":rec.featureName,        
        "statusValue":{
          "text":statusInfo.status
        },
        "statusIcon":{
          "skin":statusInfo.statusIconSkin,
          "text":"\ue921"
        },
        "lblData1": limitsJson.MAX_TRANSACTION_LIMIT ?  (self.convertToLocaleString(limitsJson.MAX_TRANSACTION_LIMIT)) : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblData2": limitsJson.DAILY_LIMIT ? (self.convertToLocaleString(limitsJson.DAILY_LIMIT)) : kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblData3": limitsJson.WEEKLY_LIMIT ? (self.convertToLocaleString(limitsJson.WEEKLY_LIMIT)) :kony.i18n.getLocalizedString("i18n.Applications.NA"),
        "lblHeading1": kony.i18n.getLocalizedString("i18n.frmServiceManagement.PerTransactionLimitUC"),
        "lblHeading2": kony.i18n.getLocalizedString("i18n.frmServiceManagement.DailyTransactionLimitUC"),
        "lblHeading3": kony.i18n.getLocalizedString("i18n.frmServiceManagement.WeeklyTransLimitUC"),
      };
    });
    self.view.segCompanyLimits.widgetDataMap = dataMap;
    self.view.segCompanyLimits.setData(data);
    //self.view.flxCompanyDetailsLimitsContainer.setVisibility(true);
    self.view.forceLayout();
  },
  setViewFeatureActionsData : function(actions){
    var scopeObj = this;
    var actionNo=1;
    if (actions === undefined||actions.length===0) {
      this.view.segActions.setData([]);
    } else {
      var dataMap = {
        "flxFeatureDetailsActions": "flxFeatureDetailsActions",
        "lblActionName":"lblActionName",
        "lblActionDescription":"lblActionDescription",
        "lblSeparator": "lblSeparator"
      };
      var data = actions.map(function(actionRec){
        return{
          "lblActionName":actionRec.actionName,
          "lblActionDescription":actionRec.actionDescription,
          "lblSeparator":"."
        };
      });
      scopeObj.view.segActions.widgetDataMap = dataMap;
      scopeObj.view.segActions.setData(data);
    }
    scopeObj.view.forceLayout();
  },
  fillFeatureDetails : function(feature){
    var statusInfo = {};
    var featureStatus = feature.status ? feature.status : feature.featureStatus;
    statusInfo = this.getFeatureStatusAndSkin(featureStatus);
    this.view.lblFeatureDetailsHeader2.text=feature.featureName;
    this.view.fontIconActive.skin=statusInfo.statusIconSkin;
    this.view.lblFeatureStatus.text=statusInfo.status;
    this.view.lblFeatureDescriptionValue.text=feature.featureDescription;
    this.setViewFeatureActionsData(feature.Actions);
    this.view.flxFeatureDetails.setVisibility(true);
    this.view.forceLayout();
  },
  populateAccountsSegment: function() {
    var scopeObj = this;
    var accounts = scopeObj.completeCompanyDetails.approvalsContext.cifs[0].accounts;
    var dataMap = {
      "btnOption": "btnOption",
      "lblOptionName": "lblOptionName",
      "flxImgArrow": "flxImgArrow",
      "flxContent": "flxContent",
      "lblIconSelected":"lblIconSelected",
      "lblTabsSeperator": "lblTabsSeperator",
      "flxVerticalTabsRolesAccounts":"flxVerticalTabsRolesAccounts",
      "limitTypes":"limitTypes"
    };
    var data = accounts.map(function(rec) {
      return {
        "flxVerticalTabsRolesAccounts": {"skin":"sknflxBgF9F9F9"},
        "btnOption": {
          "text":rec.accountId,
          "skin":"sknBtnUtilRest73767812pxReg",
          "hoverSkin":"sknBtnUtilHover30353612pxReg"
        },
        "lblOptionName": {"isVisible": rec.accountName ? true : false,
                          "text": rec.accountName},
        "flxImgArrow": {
          "isVisible": false
        },
        "lblIconSelected":{"text":""},
        "lblTabsSeperator": ".",
        "limitTypes": rec.limitTypes,
        "template":"flxVerticalTabsRolesAccounts"
      };
    });
    scopeObj.view.segCompanyAccounts.widgetDataMap = dataMap;
    scopeObj.view.segCompanyAccounts.setData(data);
    scopeObj.view.forceLayout();
  },
  onAccountNumSelectionApprovals: function(onTabClick) { 
    var index =0;
    if(onTabClick){ //select first account initially when clicked on approvals tab
      index=0;
    }else{
      index=(this.view.segCompanyAccounts.selectedIndex)[1];
    }
    var segData = this.view.segCompanyAccounts.data;
    var selectedRowData = segData[index];
    for(var i=0; i<segData.length; i++){
      if(i === index && (segData[i].flxImgArrow.isVisible === false)){
        segData[i].flxImgArrow.isVisible = true;
        segData[i].btnOption.skin= "sknBtnUtilActive485b7512pxBold";
        segData[i].btnOption.hoverSkin= "sknBtnUtilActive485b7512pxBold";
        this.view.segCompanyAccounts.setDataAt(segData[i],i);
      } else if(i !== index && (segData[i].flxImgArrow.isVisible === true)){
        segData[i].flxImgArrow.isVisible = false;
        segData[i].btnOption.skin= "sknBtnUtilRest73767812pxReg";
        segData[i].btnOption.hoverSkin= "sknBtnUtilHover30353612pxReg";
        this.view.segCompanyAccounts.setDataAt(segData[i],i);
      }
    }
    this.view.forceLayout();
    this.showApprovalMatrixList(selectedRowData);
  },
   /*
   * display approval matrix list
   * @param: list of all approvals for selected account
   */
  showApprovalMatrixList : function(approvalsList){
    var flag=false;
    var approvalLimitsJson = approvalsList.limitTypes.reduce(function(mapJson, rec) {
        mapJson[rec.limitTypeId] = rec.actions;
        return mapJson;
      }, {});
    this.view.tabs.btnTab1.info = {"data" : approvalLimitsJson.MAX_TRANSACTION_LIMIT};
    this.view.tabs.btnTab2.info = {"data" : approvalLimitsJson.DAILY_LIMIT};
    this.view.tabs.btnTab3.info = {"data" : approvalLimitsJson.WEEKLY_LIMIT};
    this.view.tabs.btnTab1.onClick();
  },
  /*
  * create rows and display all the actions under per transation limits tab
  */
  showPerTransactionLimitsTab : function(){
    var actions = this.view.tabs.btnTab1.info.data;  
    if(actions.length>0){
      this.view.flxPerTransactionLimitsContent.removeAll();
      this.view.flxPerTransactionLimitsContent.setVisibility(true);
      var screenWidth = kony.os.deviceInfo().screenWidth;
      for (var i = 0; i < actions.length; i++) {
        var approvalSegmentRow = new com.adminConsole.companies.featureLimitsTemplate({
          "id": "approvalMatrixRowPT" + i,
          "isVisible": true,
          "width": "100%",
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "top": "20px"
        }, {}, {});
        this.view.flxPerTransactionLimitsContent.add(approvalSegmentRow);
         approvalSegmentRow.lblFeatureName.text = actions[i].featureName;
        var statusValue = "",statusSkin = "";
        if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Active");
          statusSkin = "sknFontIconActivate";
        } else if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_INACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
          statusSkin = "sknfontIconInactive";
        } else{
          statusValue = kony.i18n.getLocalizedString("i18n.common.Unavailable");
          statusSkin = "sknIconBgE61919S12px";
        }
        approvalSegmentRow.statusValue.text =statusValue;
        approvalSegmentRow.statusIcon.skin = statusSkin;
        this.setApprovalsSubSegmentData(actions[i].limits,approvalSegmentRow);
      }
    }
  },
   /*
  * create rows and display all the actions under daily limits tab
  */
  showDailyLimitsTab : function(){
    var actions = this.view.tabs.btnTab2.info.data;
    if(actions.length>0){
      this.view.flxDailyTransactionLimitsContent.removeAll();
      this.view.flxDailyTransactionLimitsContent.setVisibility(true);
      var screenWidth = kony.os.deviceInfo().screenWidth;
      for (var i = 0; i < actions.length; i++) {
        var approvalSegmentRow = new com.adminConsole.companies.featureLimitsTemplate({
          "id": "approvalMatrixRowDT" + i,
          "isVisible": true,
          "width": "100%",
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "top": "20px"
        }, {}, {});
        this.view.flxDailyTransactionLimitsContent.add(approvalSegmentRow);
         approvalSegmentRow.lblFeatureName.text = actions[i].featureName;
        var statusValue = "",statusSkin = "";
        if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Active");
          statusSkin = "sknFontIconActivate";
        } else if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_INACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
          statusSkin = "sknfontIconInactive";
        } else{
          statusValue = kony.i18n.getLocalizedString("i18n.common.Unavailable");
          statusSkin = "sknIconBgE61919S12px";
        }
        approvalSegmentRow.statusValue.text =statusValue;
        approvalSegmentRow.statusIcon.skin = statusSkin;
        this.setApprovalsSubSegmentData(actions[i].limits,approvalSegmentRow);

      }
    }
  },
   /*
  * create rows and display all the actions under weekly limits tab
  * @param: actionsList for weekly limits
  */
  showWeeklyTransactionLimitsTab : function(){
    var actions = this.view.tabs.btnTab3.info.data;
    if(actions.length>0){
      this.view.flxWeeklyTransactionLimitsContent.removeAll();
      this.view.flxWeeklyTransactionLimitsContent.setVisibility(true);
      var screenWidth = kony.os.deviceInfo().screenWidth;
      for (var i = 0; i < actions.length; i++) {
        var approvalSegmentRow = new com.adminConsole.companies.featureLimitsTemplate({
          "id": "approvalMatrixRowWT" + i,
          "isVisible": true,
          "width": "100%",
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "top": "20px"
        }, {}, {});
        this.view.flxWeeklyTransactionLimitsContent.add(approvalSegmentRow);
        approvalSegmentRow.lblFeatureName.text = actions[i].featureName;
        var statusValue = "",statusSkin = "";
        if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_ACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Active");
          statusSkin = "sknFontIconActivate";
        } else if(actions[i].featureStatus === this.AdminConsoleCommonUtils.constantConfig.FEATURE_INACTIVE){
          statusValue = kony.i18n.getLocalizedString("i18n.secureimage.Inactive");
          statusSkin = "sknfontIconInactive";
        } else{
          statusValue = kony.i18n.getLocalizedString("i18n.common.Unavailable");
          statusSkin = "sknIconBgE61919S12px";
        }
        approvalSegmentRow.statusValue.text =statusValue;
        approvalSegmentRow.statusIcon.skin = statusSkin;
        this.setApprovalsSubSegmentData(actions[i].limits,approvalSegmentRow);

      }
    }
  },
  /*
  * set approval limits and approvers data to segment
  * @param: approvalsList, parent component path
  */
  setApprovalsSubSegmentData : function(approvalsList,componentToAdd){
    var self =this;
    var widgetMap = {
      "lblApprovalLimits":"lblApprovalLimits",
      "lblApprovers":"lblApprovers",
      "lblApprovalRule":"lblApprovalRule",
      "lblSeperator":"lblSeperator",
      "flxFeatureLimits":"flxFeatureLimits"
    };
    var filteredApprovals = [];
    //remove default approvals matrix record if any
    filteredApprovals = approvalsList.filter(function(rec){
      if(rec. numberOfApprovals !== "0")
        return rec;
    });
    if(filteredApprovals.length > 0){
      var segData = filteredApprovals.map(function(rec){
        var lowerLimit,upperLimit,approverNames = "",approvalNum,prefix = "";
        for(var i=0;i< rec.approvers.length;i++){
          approverNames = approverNames + ", " + rec.approvers[i].approverName;
        }
        lowerLimit = (parseInt(rec.lowerlimit) === -1) ? "" : self.convertToLocaleString(rec.lowerlimit);
        upperLimit = (parseInt(rec.upperlimit) === -1) ? "" : self.convertToLocaleString(rec.upperlimit);
        //get the prefix for limit if any
        if(parseInt(rec.lowerlimit) === -1) prefix = kony.i18n.getLocalizedString("i18n.frmCompanies.Upto")+" ";
        else if(parseInt(rec.upperlimit) === -1) prefix = kony.i18n.getLocalizedString("i18n.frmCompanies.Above")+" ";
        else if((parseInt(rec.lowerlimit) !== -1) && (parseInt(rec.upperlimit) !== -1)) lowerLimit = lowerLimit + " - ";
        return {
          "lblApprovalLimits":{"text": prefix+ "" +lowerLimit +""+upperLimit},
          "lblApprovers":{"text":approverNames.substring(2)},
          "lblApprovalRule":{"text": rec.approvalRuleName},
          "lblSeperator":".",
          "template":"flxFeatureLimits"
        };
      });
      componentToAdd.SegActions.widgetDataMap = widgetMap;
      componentToAdd.SegActions.setData(segData);
      componentToAdd.flxNoApprovals.setVisibility(false);
      componentToAdd.flxSegActions.setVisibility(true);
    } else{
      componentToAdd.flxNoApprovals.setVisibility(true);
      componentToAdd.flxSegActions.setVisibility(false);
    }
    
    this.view.forceLayout();
  },
  /*
   * show/hide the accounts list under the approval matrix tab
   */
  toggleApprovalsAccountsTab : function(expandTab){
    if(expandTab){
      this.view.flxhideDisplay.setVisibility(true);
      this.view.btnAccountType.skin = "sknBtnUtilActive485b7512pxBold";
      this.view.btnAccountType.hoverSkin = "sknBtnUtilActive485b7512pxBold";
      this.view.lblDropArrow.text = "\ue920"; //down-arrow
      this.view.lblDropArrow.skin = "sknIcon12pxBlack";
    } else {
      this.view.flxhideDisplay.setVisibility(false);
      this.view.btnAccountType.skin = "sknBtnUtilRest73767812pxReg";
      this.view.btnAccountType.hoverSkin = "sknBtnUtilHover30353612pxReg";
      this.view.lblDropArrow.text = "\ue906"; //rigth-arrow
      this.view.lblDropArrow.skin = "sknicon15pxBlack";
    }
  },
  convertToLocaleString : function(value){
    if(value === undefined)
      return value;
    else
      return (parseFloat(value)).toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
    });
  },
  onHoverSettings : function(widget, context){
    var scopeObj = this;
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER||context.eventType === constants.ONHOVER_MOUSE_MOVE) {
      widget.setVisibility(true);
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      widget.setVisibility(false);
    }
    scopeObj.view.forceLayout();
  },
  /*
  * mapping for section in the selected accounts segment
  * @param : row data
  */
  mapSelectedAccountsSection : function(secData){
    return {
      "completeRecord": secData.completeRecord,
      "CustomerId": secData.completeRecord.Membership_id,
      "flxAccountHeaderSection":"flxAccountHeaderSection",
      "lblSectionName":{"text":kony.i18n.getLocalizedString("i18n.customerSearch.MemberID") +" - "+
                        secData.completeRecord.Membership_id},
      "flxClose":{"onClick":this.removeAccountSectionCustCentric},
      "lblSectionLine":"-",
      "fontIconClose":{"text":"\ue9a5"},
      "template":"flxCompanySelectedAccountsSec",
    };
  },
  /*
  * mapping for rows in the selected accounts segment
  * @param : row data
  */
  mapSelectedAccountsRow : function(rowData){
    var self =this;
    return {
      "completeRecord": rowData.completeRecord,
      "CustomerId": rowData.completeRecord.Membership_id,
      "flxAccountRowContainer":{"skin":"sknFlxBgF9F9F9brD7D9E0r1pxLeftRight"},
      "flxAccountRowRecord":"flxAccountRowRecord",
      "lblRecordName":{"text":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.ACCOUNT") +" "+
                       rowData.completeRecord. Account_id},
      "flxClose":{"onClick": self.removeAccountRowCustCentric},
      "fontIconClose":{"text":"\ue929",
                       "tooltip": kony.i18n.getLocalizedString("i18n.frmCompanies.Remove_account")},
      "template":"flxCompanySelectedAccountsRow",
    };
  },
  /*
  * change skin for last row in selected accounts segment
  * @param: segment data
  * @returns:  updated segment data
  */
  setSkinsToSelectedAccountsSection : function(data){
    for(var i=0;i<data.length; i++){
      var rowLen = data[i][1].length -1;
      if(rowLen >= 0)
        data[i][1][rowLen].flxAccountRowContainer.skin = "sknFlxBgF9F9F9brD7D9E0r3pxBottomRound";
    }
    return data;
  },
  /*
   * groups the accounts based on CIF
   * accounts list
   */
  groupAccountsByCIF : function(accounts,opt){
    var groupedAcc = accounts.reduce(function(group, acc) {
      if(opt === 1){ //for create company accounts
        (group[acc.completeRecord.Membership_id] = group[acc.completeRecord.Membership_id] || []).push(acc);
      } else{ //for company detail accouns
        (group[acc.Membership_id] = group[acc.Membership_id] || []).push(acc);
      }
      return group;
    }, {});
    return groupedAcc;
  },
  /*
  * add accounts to selected section for customer centric
  */
  addSelectedAccountCustCentric : function(){
    var widgetMap = {
      "flxCompanySelectedAccountsSec":"flxCompanySelectedAccountsSec",
      "flxAccountHeaderSection":"flxAccountHeaderSection",
      "lblSectionName":"lblSectionName",
      "flxClose":"flxClose",
      "lblSectionLine":"lblSectionLine",
      "fontIconClose":"fontIconClose",
      "flxCompanySelectedAccountsRow":"flxCompanySelectedAccountsRow",
      "flxAccountRowContainer":"flxAccountRowContainer",
      "flxAccountRowRecord":"flxAccountRowRecord",
      "lblRecordName":"lblRecordName",
      "fontIconClose":"fontIconClose",
      "completeRecord":"completeRecord"
    };
    var selectedRowData = [], accSectionIndex = [], recordToAdd;
    var rowIndex = this.view.addAndRemoveAccounts.segAddOptions.selectedIndex[1];
    selectedRowData = this.view.addAndRemoveAccounts.segAddOptions.selectedRowItems[0];
    var addedAccData = this.view.addAndRemoveAccounts.segSelectedOptions.data;
    this.view.addAndRemoveAccounts.segSelectedOptions.hasSections =  true;
    for(var i=0; i<addedAccData.length; i++){
      if(addedAccData[i][0].completeRecord.Membership_id === selectedRowData.completeRecord.Membership_id){
        accSectionIndex.push(i);
        break;
      }
    }
    if(accSectionIndex.length > 0){ //add to exsisting customerId
      this.assignedAccount.push(selectedRowData);
      recordToAdd = this.mapSelectedAccountsRow(selectedRowData);
      this.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
      this.view.addAndRemoveAccounts.segSelectedOptions.addDataAt(recordToAdd,accSectionIndex[0]);
      this.view.addAndRemoveAccounts.segAddOptions.removeAt(rowIndex);
    } else { //add new customerId
      this.assignedAccount.push(selectedRowData);
      var secToAdd = this.mapSelectedAccountsSection(selectedRowData);
      recordToAdd = this.mapSelectedAccountsRow(selectedRowData);
      this.view.addAndRemoveAccounts.segSelectedOptions.widgetDataMap = widgetMap;
      recordToAdd.flxAccountRowContainer.skin = "sknFlxBgF9F9F9brD7D9E0r3pxBottomRound";
      var segData = [];
      segData.push([secToAdd,[]]);
      if(addedAccData.length > 0){
        this.view.addAndRemoveAccounts.segSelectedOptions.addSectionAt(segData, 0);
        this.view.addAndRemoveAccounts.segSelectedOptions.addDataAt(recordToAdd, 0,0);
      } else{ //first record been added
        segData[0][1].push(recordToAdd);
        this.view.addAndRemoveAccounts.segSelectedOptions.setData(segData);
      }
      this.view.addAndRemoveAccounts.segAddOptions.removeAt(rowIndex);
    }
    this.view.addAndRemoveAccounts.segSelectedOptions.info.segData = this.view.addAndRemoveAccounts.segSelectedOptions.data;
    this.showHideAddRemoveSegment();
    //show/hide error message
    if(this.checkAccountValidation()){
      this.view.flxAccountError.setVisibility(false);
      this.view.flxAccountsAddAndRemove.top = "0dp";
    }else{
      this.view.flxAccountError.setVisibility(true);
      this.view.flxAccountsAddAndRemove.top = "60dp";
    }
    this.view.forceLayout();
  },
  /*
  * remove account under a section
  * @param: row index, section index
  */
  removeAccountRowCustCentric : function(){
    var self = this;
    var selectedRowData = [];
    var secInd = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowIndex[0];
    var rowIndex = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowIndex[1];
    var data = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    selectedRowData = self.view.addAndRemoveAccounts.segSelectedOptions.selectedRowItems[0];
    var recordToAdd = self.mapRemovedAccountToAvailableAcc(selectedRowData);
    self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd,0);
    if(data[secInd][1].length <= 1){ //clicked on last row left
      self.view.addAndRemoveAccounts.segSelectedOptions.removeSectionAt(secInd);
    } else{
      self.view.addAndRemoveAccounts.segSelectedOptions.removeAt(rowIndex, secInd);
      var segData = self.setSkinsToSelectedAccountsSection(self.view.addAndRemoveAccounts.segSelectedOptions.data);
      self.view.addAndRemoveAccounts.segSelectedOptions.setData(segData);
    }
    
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();
  },

  /*
  * remove entire section of accounts
  *@param : section index
  */
  removeAccountSectionCustCentric : function(sectionInd){
    var self = this;
    var selectedRowData = [];
    var sectionIndex = typeof(sectionInd) === "number" ? sectionInd : self.view.addAndRemoveAccounts.segSelectedOptions.selectedsectionindex;
    var rowsData = self.view.addAndRemoveAccounts.segSelectedOptions.data[sectionIndex][1];
    for(var i=0;i< rowsData.length;i++){
      var recordToAdd = self.mapRemovedAccountToAvailableAcc(rowsData[i]);
      self.view.addAndRemoveAccounts.segAddOptions.addDataAt(recordToAdd,0);
    }
    self.view.addAndRemoveAccounts.segSelectedOptions.removeSectionAt(sectionIndex);
    self.view.addAndRemoveAccounts.segSelectedOptions.info.segData = self.view.addAndRemoveAccounts.segSelectedOptions.data;
    self.showHideAddRemoveSegment();
    self.view.forceLayout();

  },
  resetBusinessUsers : function(isAuthorizedSignatoriesTab){
    if(isAuthorizedSignatoriesTab===true){
      this.view.btnAuthSignatories.skin="sknbtnBgffffffLato485c75Radius3Px12Px";
      this.view.btnOtherBusinessUsers.skin="sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
      this.view.btnCreateCustomer.text= kony.i18n.getLocalizedString("i18.authorizedSignatories.createAuthSignatory");
      this.view.btnAddCustomer.text=kony.i18n.getLocalizedString("i18.authorizedSignatories.createAuthSignatory");
      this.view.lblNoCustomers.text=kony.i18n.getLocalizedString("i18.businessUsers.noUsersWarning");
    }else{
      this.view.btnAuthSignatories.skin="sknbtnBgf5f6f8Lato485c75Radius3Px12Px";
      this.view.btnOtherBusinessUsers.skin="sknbtnBgffffffLato485c75Radius3Px12Px";
      this.view.btnCreateCustomer.text=kony.i18n.getLocalizedString("i18n.frmCompanies.Create_Business_User");
      this.view.btnAddCustomer.text=kony.i18n.getLocalizedString("i18n.frmCompanies.Create_Business_User");
      this.view.lblNoCustomers.text=kony.i18n.getLocalizedString("i18.businessUsers.noBusinessUsers");
    }    
    this.view.EditToolTip.setVisibility(false);
    this.setCompanyCustomers();
    this.view.forceLayout();
  },
  setDataForTaxIdHoverTooltip : function(data){
    var tinId = data.split(",");
    var tinText ="";
    var remainingTin = [],tinArr = [];
    for(var i=0;i< tinId.length;i++){
      if(!tinArr.contains(tinId[i])){ //remove repeated tin num
        tinArr.push(tinId[i]);
        if((tinText+ ""+ tinId[i]).length < 35){
          tinText = tinText + ", "+ tinId[i];
        }else{
          remainingTin.push(tinId[i]);
        }
      }
    }
    this.view.lblDetailsValue13.text = tinText.substring(2,tinText.length);
    //set tooltip data
    if(remainingTin.length > 0){
      var widgetMap = {
        "flxMore":"flxMore",
        "lblName":"lblName"
      };
      var segData = remainingTin.map(function(rec){
        return{
          "lblName":rec,
          "template":"flxMore"
        }
      })
      this.view.segTaxId.setData(segData);
      this.view.lblDetailsMore.text = "+"+segData.length+" more";
      this.view.lblDetailsMore.setVisibility(true);
    } else{
      this.view.lblDetailsMore.setVisibility(false);
    }
    this.view.forceLayout();
  },
  /*
   * hover on more text for tax nuber in case of customer centric
   */
  onHoverTaxIdMoreText : function(widget,context){
    if (widget) { 
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        this.view.flxTaxIdTooltip.setVisibility(true);
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        this.view.flxTaxIdTooltip.setVisibility(false);
      }
    }
    this.view.forceLayout();
  },
  /*
  * set filter data in company details accounts tab
  */
  setDataToAccountsStatusFilter : function(){
    var self = this;
    var statusList=[],maxLenText = "";
    var accountsData = self.completeCompanyDetails.accountContext;
    for(var i=0;i<accountsData.length;i++){
      if(!statusList.contains(accountsData[i].StatusDesc))
        statusList.push(accountsData[i].StatusDesc);
    }
    var widgetMap = {
      "Status_id": "Status_id",
      "Type_id": "Type_id",
      "BusinessType_id": "BusinessType_id",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = statusList.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;
      return {
        "Status_id": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": rec
      };
    });
    self.view.accountStatusFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.accountStatusFilterMenu.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<statusList.length;j++){
      selStatusInd.push(j);
    }
    self.view.accountStatusFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    self.view.flxAccountStatusFilter.width = self.AdminConsoleCommonUtils.getLabelWidth(maxLenText)+55+"px";
    self.view.forceLayout();
  },
  /*
  * created by :kaushik mesala
  * set filter data in company details accounts tab
  */
  setDataToContractAccountsFilter : function(filterComponent,headerData, accountsData , colName , accountsFeaturesCard){
    var self = this;
    var statusList=[],maxLenText = "";
    
    for(var i=0;i<accountsData.length;i++){
      if(!statusList.contains(accountsData[i][colName]))
        statusList.push(accountsData[i][colName]);
    }
    var widgetMap = {
      "Status_id": "Status_id",
      "Type_id": "Type_id",
      "BusinessType_id": "BusinessType_id",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = statusList.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;
      return {
        "Status_id": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": rec
      };
    });
    filterComponent.segStatusFilterDropdown.widgetDataMap = widgetMap;
    filterComponent.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<statusList.length;j++){
      selStatusInd.push(j);
    }
    filterComponent.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    filterComponent.segStatusFilterDropdown.onRowClick = function(){
      var segStatusData = filterComponent.segStatusFilterDropdown.data;
      var indices = filterComponent.segStatusFilterDropdown.selectedIndices;
      
      accountsFeaturesCard.reportPagination.setVisibility(false);
      
      if (indices) {
          // selType is to store all the selected values to be matched
          var selType = [];
          
          selInd = indices[0][1]; // all selected indexes in filter
          for (var i = 0; i < selInd.length; i++) {
              selType.push(filterComponent.segStatusFilterDropdown.data[selInd[i]].Status_id);
          }

          if (selInd.length === segStatusData.length) { //all are selected
            
            this.paginationDetails.currSegContractData[1] = accountsData;
            accountsFeaturesCard.segAccountFeatures.setData([headerData].concat(accountsData.slice(0,10)));
            accountsFeaturesCard.flxNoFilterResults.setVisibility(false);
          } else {
              // filtering of data
              dataToShow = accountsData.filter(function(rec) {
                  if (selType.indexOf(rec[colName]) >= 0) {
                      return rec;
                  }
              });
              this.paginationDetails.currSegContractData[1] = dataToShow;
              // should retain header
              if(dataToShow.length > 0) {
                // we only set first 10 values in the segment                
                var sectionData = [ headerData , dataToShow.slice(0,10)];
                
                accountsFeaturesCard.segAccountFeatures.setData([sectionData]);
                accountsFeaturesCard.flxNoFilterResults.setVisibility(false);
              } else {
                this.paginationDetails.currSegContractData[1] = [];
                accountsFeaturesCard.segAccountFeatures.setData([headerData]);
                accountsFeaturesCard.flxNoFilterResults.setVisibility(true);
              }
          }
      } else {
        this.paginationDetails.currSegContractData[1] = [];
        accountsFeaturesCard.segAccountFeatures.setData([headerData]);
        accountsFeaturesCard.flxNoFilterResults.setVisibility(true);
          // no filter
          // filter and set to segment   
      }
      let datLen = this.paginationDetails.currSegContractData[1].length;
      if (datLen > 10) {
        this.paginationActionsForAcct(accountsFeaturesCard, accountsFeaturesCard.segAccountFeatures, datLen);
      }
      this.view.forceLayout();
    }.bind(this);
    self.view.forceLayout();
  },
  /*
  * filter the accounts based on the status selected
  */
  performAccountsStatusFilter : function(){
    var self = this;
    var selStatus = [];
    var selInd;
    var dataToShow = [];
    var allData = self.completeCompanyDetails.accountContext;
    var segStatusData = self.view.accountStatusFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.accountStatusFilterMenu.segStatusFilterDropdown.selectedIndices;
    if(indices){
      selInd = indices[0][1];
      for(var i=0;i<selInd.length;i++){
        selStatus.push(self.view.accountStatusFilterMenu.segStatusFilterDropdown.data[selInd[i]].lblDescription);
      }
      if (selInd.length === segStatusData.length) { //all are selected
        self.setCompanyAccounts(allData);
      } else {
        dataToShow = allData.filter(function(rec){
          if(selStatus.indexOf(rec.StatusDesc) >= 0){
            return rec;
          }
        });
        if (dataToShow.length > 0) {
          self.setCompanyAccounts(dataToShow);
        } else {
          self.setCompanyAccounts([]);
        }
      }
    } else{
      self.setCompanyAccounts([]);
    }
      
  },
   /*
  * set filter data in company details accounts tab
  * 2param: search result
  */
  setDataToBusinessTypeFilter : function(data){
    var self = this;
    var statusList=[],maxLenText = "";

    for(var i=0;i<data.length;i++){
      if(!statusList.contains(data[i].serviceDefinitionName))
        statusList.push(data[i].serviceDefinitionName);
    }
    var widgetMap = {
      "Status_id": "Status_id",
      "Type_id": "Type_id",
      "BusinessType_id": "BusinessType_id",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = statusList.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;     
      return {
        "Type_id": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": rec
      };
    });
    self.view.businessTypeFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.businessTypeFilterMenu.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<statusList.length;j++){
      selStatusInd.push(j);
    }
    self.view.businessTypeFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    //set filter width
    self.view.flxSearchBusinessTypeFilter.width = self.AdminConsoleCommonUtils.getLabelWidth(maxLenText)+55+"px";
    self.view.forceLayout();
  },
  /*
  * filter the company's list based on the business type selected
  */
  performBusinessTypeFilter : function(){
    var self = this;
    var selType = [];
    var selInd;
    var dataToShow = [];
    var allData = self.view.segSearchResults.info.segData;
    var segStatusData = self.view.businessTypeFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.businessTypeFilterMenu.segStatusFilterDropdown.selectedIndices;
    if(indices){
      selInd = indices[0][1];
      for(var i=0;i<selInd.length;i++){
        selType.push(self.view.businessTypeFilterMenu.segStatusFilterDropdown.data[selInd[i]].Type_id);
      }
      if (selInd.length === segStatusData.length) { //all are selected
        self.view.segSearchResults.setData(allData);
        self.view.segSearchResults.setVisibility(true);
        self.view.flxNoFilterResults.setVisibility(false);
      } else {
        dataToShow = allData.filter(function(rec){
          if(selType.indexOf(rec.lblType) >= 0){
            return rec;
          }
        });
        if (dataToShow.length > 0) {
          self.view.segSearchResults.setData(dataToShow);
          self.view.segSearchResults.setVisibility(true);
          self.view.flxNoFilterResults.setVisibility(false);
        } else {
          self.view.segSearchResults.setData([]);
          self.view.flxHeaderPermissions.setVisibility(true);
          self.view.segSearchResults.setVisibility(false);
          self.view.flxNoFilterResults.setVisibility(true);
        }
      }
    } else{
      self.view.segSearchResults.setData([]);
      self.view.flxHeaderPermissions.setVisibility(true);
      self.view.segSearchResults.setVisibility(false);
      self.view.flxNoFilterResults.setVisibility(true);
    }
      
  },
  /*
  * set business types to listbox on create/edit company screen
  */
  setBusinessTypesListBoxData: function (list) {
    var attributeList = [];
    attributeList = list.reduce(
      function (list, record) {
        if(record.Default_group){
          list.push([record.BusinessType_id, record.BusinessType_name]);
        } 
        return list;
      }, [["select", "Select a Business Type"]]);
    this.view.lstBoxCompanyDetails12.masterData = attributeList;
    this.view.lstBoxCompanyDetails12.selectedKey = "select";
    this.view.lstBoxCompanyDetails12.info = {"allData":list};
    this.view.forceLayout();
  },
  editSignatoryOrUser: function () {
    var scopeObj = this;
    var index = scopeObj.view.segCompanyDetailCustomer.selectedRowIndex[1];
    var data = scopeObj.view.segCompanyDetailCustomer.data;
    var customerEditContext = data[index].customerEditInfo;
    customerEditContext["inAuthorizedSignatoriesUi"] = scopeObj.inAuthorizedSignatoriesUi;
    customerEditContext["isAccountCentricConfig"]=scopeObj.isAccountCentricConfig;
    scopeObj.presenter.navigateToEditCustomerScreen(customerEditContext);
  },
  /*
  * fill the company details when only single customer acc is selected
  * @param: company details
  */
  fillDetailsForSelectedCIF : function(companyDetails){
    this.view.companyDetailsEntry11.tbxEnterValue.text = companyDetails.name || "";
    var selectedType = this.getBusinessTypeKeyBasedOnName(companyDetails.businessType);
    this.view.lstBoxCompanyDetails12.selectedKey = selectedType ? selectedType[0] : "select";
    /*//disabling the company name
    if(companyDetails.name){
      this.view.companyDetailsEntry11.tbxEnterValue.skin = "txtD7d9e0disabledf3f3f3NoBorder";
      this.view.companyDetailsEntry11.tbxEnterValue.setEnabled(false);
    }*/
    this.view.companyDetailsEntry32.tbxEnterValue.text = companyDetails.email || "";
    var phoneNumber = companyDetails.phone ? companyDetails.phone.split("-"):["",""];
    this.view.companyContactNumber.txtContactNumber.text = phoneNumber.length>=2 ? phoneNumber[1]:phoneNumber[0];
    this.view.companyContactNumber.txtISDCode.text = phoneNumber.length>=2 ? this.view.companyContactNumber.addingPlus(phoneNumber[0].trim()):"";
    
    this.view.companyDetailsEntry41.tbxEnterValue.text = companyDetails.Address[0].addressLine1 || "";
    this.view.companyDetailsEntry51.tbxEnterValue.text = companyDetails.Address[0].addressLine2 || "";
    this.view.typeHeadCountry.tbxSearchKey.text = companyDetails.Address[0].country || "";
    this.view.companyDetailsEntry72.tbxEnterValue.text = companyDetails.Address[0].zipCode || "";
    this.view.typeHeadCity.tbxSearchKey.text = companyDetails.Address[0].cityName || "";
    this.view.typeHeadState.tbxSearchKey.text = companyDetails.Address[0].state || "";
    this.view.typeHeadCity.tbxSearchKey.info.isValid = true;
    this.view.typeHeadCountry.tbxSearchKey.info.isValid = true;
    this.view.typeHeadState.tbxSearchKey.info.isValid = true;
    this.view.forceLayout();
  },
  /*
  * get business type record in listbox based on typename
  * @param: business type name
  * @return: [businessId,businessTypeName]
  */
  getBusinessTypeKeyBasedOnName : function(businessTypeName){
    var data;
    var businessTypes = this.view.lstBoxCompanyDetails12.info.allData;
    for(var i=0;i<businessTypes.length;i++){
      if(businessTypes[i].BusinessType_name === businessTypeName){
        data = [businessTypes[i].BusinessType_id, businessTypes[i].BusinessType_name];
        break;
      }
    }
    return data;
  },
  /*
  * set tax id UI/data based on account/customer centric config
  */
  setTaxIdInCompanyDetailsScreen : function(){
    if(this.isAccountCentricConfig === true){
      this.view.customListboxTaxid.setVisibility(false);
      this.view.companyDetailsEntry21.flxEnterValue.setVisibility(true);
    } else{ //customer centric
      var selectedAccounts = this.view.addAndRemoveAccounts.segSelectedOptions.data;
      var taxIdArr = [], listBoxData;
      for(var i=0;i<selectedAccounts.length;i++){
        if(selectedAccounts[i][0].completeRecord.TaxId && 
           !taxIdArr.contains(selectedAccounts[i][0].completeRecord.TaxId))
          taxIdArr.push(selectedAccounts[i][0].completeRecord.TaxId);
      }
      
      if(taxIdArr.length > 1){ //for multiple customerId accounts
        this.view.customListboxTaxid.setVisibility(true);
        this.view.companyDetailsEntry21.flxEnterValue.setVisibility(false);        
        var widgetDataMap = {
          "id": "id",
          "lblDescription": "lblDescription",
          "imgCheckBox": "imgCheckBox",
          "flxCheckBox":"flxCheckBox",
          "flxSearchDropDown": "flxSearchDropDown"
        };
        this.view.customListboxTaxid.segList.widgetDataMap = widgetDataMap;
        listBoxData = taxIdArr.map(function(rec) {
          return {
            "id": rec,
            "lblDescription": {"text":rec,"skin":"sknLbl485C75LatoRegular13Px"},
            "imgCheckBox": {"src":"checkboxDisableBg_2x.png"},
            "template": "flxSearchDropDown"
          };
        });
        this.view.customListboxTaxid.segList.setData(listBoxData);
        this.view.customListboxTaxid.lblSelectedValue.text = listBoxData.length + " selected";
      } else if(taxIdArr.length === 1){ //single customer Id accounts
        this.view.customListboxTaxid.setVisibility(false);
        this.view.companyDetailsEntry21.flxEnterValue.setVisibility(true);
        
        var taxId = taxIdArr[0] || "";
        this.view.companyDetailsEntry21.tbxEnterValue.text = taxId;
        this.view.companyDetailsEntry21.tbxEnterValue.skin = "txtD7d9e0disabledf3f3f3NoBorder";
        this.view.companyDetailsEntry21.tbxEnterValue.setEnabled(false); 
      } else{
        this.view.customListboxTaxid.setVisibility(false);
        this.view.companyDetailsEntry21.flxEnterValue.setVisibility(true);
        this.view.companyDetailsEntry21.tbxEnterValue.skin = "sknTbx485c75LatoReg13pxNoBor";
        this.view.companyDetailsEntry21.tbxEnterValue.setEnabled(true);
        this.view.companyDetailsEntry21.tbxEnterValue.text = "";
      }
    }
    this.view.forceLayout();
  },
  saveScreenY:function(widget,context){
    this.mouseYCoordinate=context.screenY;
  },
  showEditTooltip : function(widget,context){
    var self=this;
    if (widget) { 
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER ) {
        self.view.EditToolTip.top=self.mouseYCoordinate-400+self.view.flxCompanyDetails.contentOffsetMeasured.y+"px";
        self.view.EditToolTip.setVisibility(true);
      }else if (context.eventType === constants.ONHOVER_MOUSE_MOVE){
        self.view.EditToolTip.setVisibility(true);
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        self.view.EditToolTip.setVisibility(false);
      }
    }
    self.view.forceLayout();
  },
  setContractServiceCards : function(services,isSearch){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow0,this.view.verticalTabsContract.btnOption0);
    if(isSearch===true)
      this.setDataToServiceTypeFilter(services);
    this.view.flxContractServiceCards.removeAll();
    var screenWidth = kony.os.deviceInfo().screenWidth;
    var left =20, top =20, width = 0;
    for (var i = 0; i < services.length; i++) {
      width = (screenWidth -305-230-35-60-60)/3;
      var alertServiceCard = new com.adminConsole.contracts.serviceCard({
        "id": "serviceCard"+services[i].id,
        "isVisible": true,
        "width": width + "px",
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "top": top+"dp",
        "left": left +"dp"
      }, {}, {});
      left = left + width + 20;
      if(i!== 0 && i%3 === 0){ //new row
        alertServiceCard.left = "20dp";
        left = 20 + width + 20;
        top = top + 80 + 20;
        alertServiceCard.top = top +"dp";
      } 
      this.setServiceCardData(services[i], alertServiceCard,width-40);
    }
    if(this.action === this.actionConfig.edit){
      this.view.flxContractServiceCards.setEnabled(false);
    }else{
      this.view.flxContractServiceCards.setEnabled(true);
    }
  },
  /*
  * set category data to each card id
  * category data, card to add
  */
  setServiceCardData : function(serviceDefinition, serviceCard,width){
    var self = this;
    serviceCard.lblCategoryName.info ={"catId":serviceDefinition.id};
    var labelCharCount=Math.ceil(width/7);
    serviceCard.lblCategoryName.text=this.AdminConsoleCommonUtils.getTruncatedString(serviceDefinition.name, labelCharCount, labelCharCount-3);
    serviceCard.lblCategoryName.tooltip = serviceDefinition.name;
    serviceCard.lblContent1.info={"groupId":serviceDefinition.serviceType};
    if(serviceDefinition.serviceType==="TYPE_ID_RETAIL"){
      serviceCard.lblContent1.text="Retail Banking";
      serviceCard.flxServiceTag.skin="sknflxCustomertagRedRadius4px";
      serviceCard.flxServiceTag.width="100px";
    } else if(serviceDefinition.serviceType==="TYPE_ID_BUSINESS"){
      serviceCard.lblContent1.text="Business Banking";
      serviceCard.flxServiceTag.skin="sknflxCustomertagPurpleRadius4px";
      serviceCard.flxServiceTag.width="115px";
    } else if(serviceDefinition.serviceType==="TYPE_ID_WEALTH"){
      serviceCard.lblContent1.text="Wealth Banking";
      serviceCard.flxServiceTag.skin="sknflxServiceTag0D4CFF";
      serviceCard.flxServiceTag.width="105px";
    }
    if(this.action===this.actionConfig.edit&&this.completeContractDetails.servicedefinitionId===serviceDefinition.id){
      serviceCard.skin="sknFlxbgF8F9FAbdr003E75Shadow";
      this.selectedServiceCard=serviceCard.id;
    }else if(this.selectedServiceCard===serviceCard.id){
      serviceCard.skin="sknFlxbgF8F9FAbdr003E75Shadow";
    }
    serviceCard.onClick = function(){
      //should make all the other cards skins to normal skins
      if(self.selectedServiceCard){
      var cards=self.view.flxContractServiceCards.widgets();
      self.view[self.selectedServiceCard].skin="sknFlxbgFFFFFFbdrD7D9E0rd3px";
        if(self.createContractRequestParam.contractCustomers.length!==0)
          self.presenter.getServiceDefinitionFeaturesAndLimits({"serviceDefinitionId":serviceCard.lblCategoryName.info.catId});
      }
      self.createContractRequestParam.serviceDefinitionId=serviceCard.lblCategoryName.info.catId;
      self.createContractRequestParam.serviceDefinitionName=serviceCard.lblCategoryName.tooltip;
      self.view.lstBoxContractService.masterData=[[serviceCard.lblCategoryName.tooltip,serviceCard.lblCategoryName.tooltip]];
      serviceCard.skin="sknFlxbgF8F9FAbdr003E75Shadow";
      self.AdminConsoleCommonUtils.setEnableDisableSkinForButton(self.view.commonButtonsServiceDetails.btnSave,true,true);
      self.view.verticalTabsContract.flxOption0.setEnabled(true);
      self.view.verticalTabsContract.flxOption1.setEnabled(true);
      self.selectedServiceCard=serviceCard.id;
    }
    this.view.flxContractServiceCards.add(serviceCard);
    this.view.forceLayout();
  },
  hideAllScreens: function(){
    this.view.flxContractServiceTab.setVisibility(false);
    this.view.flxContractCustomersTab.setVisibility(false);
    this.view.flxContractDetailsTab.setVisibility(false);
    this.view.flxContractAccounts.setVisibility(false);
    this.view.flxContractFA.setVisibility(false);
    this.view.flxContractLimits.setVisibility(false);
    this.view.forceLayout();
  },
  toggleContractVerticalTabs: function(imgPath,btnPath){
    this.tabUtilVerticleArrowVisibilityFunction(
      [this.view.verticalTabsContract.flxImgArrow0,
       this.view.verticalTabsContract.flxImgArrow1,
       this.view.verticalTabsContract.flxImgArrow2,
       this.view.verticalTabsContract.flxImgArrow3,
       this.view.verticalTabsContract.flxImgArrow4,
      this.view.verticalTabsContract.flxImgArrow5],imgPath);  
    var widgetArray = [this.view.verticalTabsContract.btnOption0,this.view.verticalTabsContract.btnOption1,this.view.verticalTabsContract.btnOption2,this.view.verticalTabsContract.btnOption3,this.view.verticalTabsContract.btnOption4,this.view.verticalTabsContract.btnOption5];
    this.tabUtilVerticleButtonFunction(widgetArray,btnPath);
  },
  showContractServiceScreen: function(){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow0,this.view.verticalTabsContract.btnOption0);
    this.hideAllScreens();
    if(this.view.tbxRecordsSearch.text.length>0||(this.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices&&this.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices[0][1].length!==this.view.serviceTypeFilterMenu.segStatusFilterDropdown.data.length)){
      this.view.flxClearRecordsSearch.onClick();
    }
    this.view.flxContractServiceTab.setVisibility(true);
    this.view.forceLayout();
  },
  showContractCustomersScreen: function(isNextClick){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow1,this.view.verticalTabsContract.btnOption1);
    if(isNextClick)
      this.view.flxContractServiceTab.setVisibility(false);
    else
      this.hideAllScreens();
    this.view.verticalTabsContract.flxOption0.setEnabled(true);
    this.view.verticalTabsContract.flxOption1.setEnabled(true);
    this.view.flxContractCustomersTab.setVisibility(true);
    this.view.forceLayout();
  },
  showContractDetailsScreen : function(isNextClick){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow2,this.view.verticalTabsContract.btnOption2);
    if(isNextClick)
      this.view.flxContractCustomersTab.setVisibility(false);
    else
      this.hideAllScreens();
    this.view.contractDetailsEntry1.flxInlineError.setVisibility(false);
    this.view.flxContractDetailsTab.setVisibility(true);
    this.view.forceLayout();
  },
  setContractDetails : function(){
    var contractData={};
    var contact="";
    if(this.action===this.actionConfig.create){
      var primaryData=this.primaryContractCustomer;
      contractData.name=primaryData.coreCustomerName;
      contractData.faxId=primaryData.faxId?primaryData.faxId:"";
      contractData.email=primaryData.email?primaryData.email:"";
      contractData.address1=primaryData.addressLine1?primaryData.addressLine1:"";
      contractData.address2=primaryData.addressLine2?primaryData.addressLine2:"";
      contractData.zipCode=primaryData.zipCode?primaryData.zipCode:"";
      contractData.country=primaryData.country?primaryData.country:"";
      contractData.city=primaryData.city?primaryData.city:"";
      contact=primaryData.phone.split("-");
    }else{
      var contractDetails=JSON.parse(JSON.stringify(this.completeContractDetails));
      this.view.lstBoxContractService.masterData=[[contractDetails.servicedefinitionId,contractDetails.servicedefinitionName]];
      contractData.name=contractDetails.name;
      contractData.faxId=contractDetails.faxId?contractDetails.faxId:"";
      contractData.email=contractDetails.communication[0].email?contractDetails.communication[0].email:"";
      contractData.address1=contractDetails.address[0]&&contractDetails.address[0].addressLine1?contractDetails.address[0].addressLine1:"";
      contractData.address2=contractDetails.address[0]&&contractDetails.address[0].addressLine2?contractDetails.address[0].addressLine2:"";
      contractData.zipCode=contractDetails.address[0]&&contractDetails.address[0].zipCode?contractDetails.address[0].zipCode:"";
      contractData.country=contractDetails.address[0]&&contractDetails.address[0].country?contractDetails.address[0].country:"";
      contractData.city=contractDetails.address[0]&&contractDetails.address[0].city?contractDetails.address[0].city:"";
      contact=[contractDetails.communication[0].phoneCountryCode,contractDetails.communication[0].phoneNumber];
    }
    if(this.view.segAddedCustomers.data.length>1)
      this.view.flxContractNotification.setVisibility(true);
    else
      this.view.flxContractNotification.setVisibility(false);
    this.view.lstBoxContractService.selectedKey=this.view.lstBoxContractService.masterData[0][0];
    this.view.lstBoxContractService.setEnabled(false);
    this.view.contractDetailsEntry1.tbxEnterValue.text=contractData.name;
    this.view.contractDetailsEntry2.tbxEnterValue.text=contractData.faxId;
    this.view.contractContactNumber.txtISDCode.text=contact[0]?contact[0].trim():"";
    this.view.contractContactNumber.txtContactNumber.text=contact[1]?contact[1].trim():"";
    this.view.contractDetailsEntry3.tbxEnterValue.text=contractData.email;
    this.view.contractDetailsEntry4.tbxEnterValue.text=contractData.address1;
    this.view.contractDetailsEntry5.tbxEnterValue.text=contractData.address2;
    this.view.contractDetailsEntry6.tbxEnterValue.text=contractData.zipCode;
    //this.view.contractDetailsEntry7.tbxEnterValue.text=contractData.country;
    this.view.typeHeadContractCountry.tbxSearchKey.text = contractData.country;
    this.view.typeHeadContractCountry.tbxSearchKey.info={"isValid": true};
    this.view.contractDetailsEntry8.tbxEnterValue.text=contractData.city;
    if(this.view.contractDetailsEntry1.flxInlineError.isVisible)
      this.view.contractDetailsEntry1.flxInlineError.setVisibility(false);
    this.view.forceLayout();
  },
  showCustomerSearchPopup : function(isFirstTime){
    this.view.flxLoading2.setVisibility(false);
    this.resetCoreCustomerSearch();
    this.setNormalSkinToCoreSearch();
    this.view.flxSearchFilter.setVisibility(true);
    this.view.lblIconArrow.text="";
    this.view.commonButtons.btnNext.setVisibility(false);
    this.view.flxCustomerSearchPopUp.setVisibility(true);
    this.view.flxCustomersBreadcrumb.setVisibility(false);
    this.view.flxAddedCustomerInfo.setVisibility(false);
    this.view.flxCustomerSearchHeader.setVisibility(true);
    this.view.imgCustomerSearchError.setVisibility(false);
    this.view.lblCustomerSearchError.setVisibility(false);
    this.view.fonticonrightarrowSearch.text="";
    this.view.commonButtons.btnCancel.skin="sknbtnffffffLatoRegular4f555dBorder1px485c75";
    this.view.btnShowHideAdvSearch.text=kony.i18n.getLocalizedString("i18n.contracts.showAdvancedSearch");
    this.view.flxRow2.setVisibility(false);
    this.view.flxRow3.setVisibility(false);
    this.view.flxColumn13.setVisibility(false);
    this.view.flxCustomerAdvSearch.setVisibility(true);
    this.view.flxCustomerDetailsContainer.setVisibility(true);
    this.view.flxRelatedCustomerSegment.setVisibility(false);
    this.view.lblNoCustomersSearched.text=kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.NoresultsSearchForaCustomer");
    this.view.flxNoCustomersSearched.setVisibility(true);
    this.view.lblRelatedcustSubHeading.setVisibility(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnSave,true,false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnNext,false,false);
    if(isFirstTime)
      this.selectedCustomers=[];
    this.view.flxSearchBreadcrumb.info={"added":[]};
    this.view.flxSearchFilter.info={"added":[]};
    this.view.segBreadcrumbs.setData([]);
    var i=this.view.flxSearchBreadcrumb.widgets().concat([]);
    this.view.flxSearchFilter.removeAll();
    for(var x=2;x<i.length-1;x++)
    	this.view.flxSearchBreadcrumb.remove(i[x]);
    this.view.forceLayout();
  },
  addCustomer : function(customerInfo){
    this.view.flxCustomerSearchPopUp.info={"action":"SEGMENTCLICK"};
    this.view.commonButtons.btnNext.setVisibility(true);
    this.view.flxCustomerSearchHeader.setVisibility(false);
    this.view.flxCustomersBreadcrumb.setVisibility(true);
    this.view.flxAddedCustomerInfo.setVisibility(true);
    this.view.flxCustomerAdvSearch.setVisibility(false);
    this.view.flxCustomerDetailsContainer.setVisibility(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnNext,false,true);
    if(this.view.flxSearchBreadcrumb.info.added)
    this.addBreadcrumb(customerInfo);
    var isAdded=false;
    for(var i=0;i<this.selectedCustomers.length;i++){
      if(this.selectedCustomers[i].coreCustomerId===customerInfo.coreCustomerId&&this.selectedCustomers[i].isSelected)
        isAdded=true;
    }
    if(isAdded===false)
    	this.addCustomerTag(this.view.flxSearchFilter,customerInfo.coreCustomerName,customerInfo.coreCustomerId);
    this.view.flxLoading2.setVisibility(true);
    this.presenter.getRelatedCoreCustomers({"coreCustomerId" : customerInfo.coreCustomerId});
    this.view.forceLayout();
  },
  addCustomerTag : function(tagParentFlex,customerName,id){
    var self = this;
    var tagsCount=tagParentFlex.widgets().length;
    var newTextTag = self.view.flxFilterTag.clone(tagsCount.toString());
    var lblname = tagsCount + "lblTagName";
    var imgname = tagsCount + "flxCross";
    var flexWidth= this.AdminConsoleCommonUtils.getLabelWidth(customerName,"12px Lato-Regular");
    tagParentFlex.info.added.push([customerName,id]);
    newTextTag[lblname].text = customerName;
    newTextTag[lblname].tooltip = customerName;
    newTextTag.isVisible = true;
    newTextTag.info=id;
    newTextTag.width=flexWidth+10+10+15+"px";//labelwidth+left padding+right padding+ cross image width
    var parentWidth=tagParentFlex.frame.width;
    var leftVal=20;
    var topVal=0;
    var lineCount=1;
    for(var a=tagsCount-1;a>=0;a--){
      var i=tagParentFlex.children[a];
      leftVal=leftVal+(tagParentFlex[i].frame.width+15);
      if((leftVal+flexWidth+50)>parentWidth){
        leftVal=20;
        lineCount=lineCount+1;
      }
    }
    newTextTag.left=leftVal+"px";
    if(lineCount>1){
      newTextTag.top=(lineCount-1)*30+"px";
    }else{
      newTextTag.top=topVal+"px";
    }
    tagParentFlex.addAt(newTextTag, -1);
    newTextTag[imgname].onTouchStart = function (id) {
      self.removeSelectedCustomer(tagParentFlex,id.parent.info);
      if(tagParentFlex.widgets().length===1){
        self.AdminConsoleCommonUtils.setEnableDisableSkinForButton(self.view.commonButtons.btnSave,true,false);
        self.AdminConsoleCommonUtils.setEnableDisableSkinForButton(self.view.commonButtons.btnNext,false,false);
      }
      if(tagParentFlex.id=="flxTagsContainer"&&self.view.flxContractLimits.isVisible&&self.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.length!==0)
        self.showBulkUpdatePopupScreen2();//to refresh features data in the dropdown when any selected customer is removed
      else
      	self.removeTag(tagParentFlex,id.parent.info);
    };
    tagParentFlex.setVisibility(true);
    this.view.forceLayout();
  },
  /*
   * function to remove a selected tag
   * @param : selectedflex id
   */
  removeTag: function(tagParentFlex,id) {
    //remove the flex tag
    for(let x=0;x<tagParentFlex.info.added.length;x++){
      if(tagParentFlex.info.added[x][1]===id)
        tagParentFlex.info.added.splice(x,1);
    }    
    tagParentFlex.removeAll();
    var addedCount=tagParentFlex.info.added.length;
    var addedCustomers=tagParentFlex.info.added;
    tagParentFlex.info.added=[];
    for (var x=0;x<addedCount;x++){
      this.addCustomerTag(tagParentFlex,addedCustomers[x][0],addedCustomers[x][1]);
    }
    this.view.forceLayout();
  },
  removeSelectedCustomer: function(tagParentFlex,id){
    var removedCustId=id;
    if(tagParentFlex.id!="flxTagsContainer"){
      var relativeCustomers=this.view.segRelatedCustomers.data;
      var self=this;
      for(var i=0;i<this.selectedCustomers.length;i++){
        if(this.selectedCustomers[i].coreCustomerId===removedCustId){
          this.selectedCustomers[i].isSelected=false;
          if(this.view.lblDataInfo11.text==removedCustId){//if its the last tag , the current customer checkbox should be unselected
            this.view.imgCheckbox.src="checkboxnormal.png";
          }else{//if that customer id displayed in the current screen
            for(let x=0;x<relativeCustomers.length;x++){
              if(relativeCustomers[x].lblData1.text===removedCustId){
                relativeCustomers[x].imgCheckbox.src="checkboxnormal.png";
                relativeCustomers[x].flxCheckbox.onClick = function(context){self.relatedCustomersCheckboxClick(context,self.selectedCustomers[i])};
                relativeCustomers[x].flxRelatedCustomerRow.onClick = function(){self.addCustomer(self.selectedCustomers[i])};
                this.view.segRelatedCustomers.setData(relativeCustomers);
                break;
              }
            }
          }
          break;
        }        
      }
    }else{
      //to set removed customer checkbox to unselected      
      var segData=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.data;
      for(var i=0;i<segData[0][1].length;i++){
        if(segData[0][1][i].imgCheckBox.src==="checkboxselected.png"&&segData[0][1][i].custInfo.coreCustomerId===removedCustId){        
          segData[0][1][i].imgCheckBox.src="checkboxnormal.png";
          break;
        }
      }
      for(let x=0;x<this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.length;x++){
        if(removedCustId===this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust[x].coreCustomerId){
          this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.splice(x,1);
          break;
        }
      }
      segData[0][0].imgCheckBox.src=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.length===0?"checkboxnormal.png":"checkboxpartial.png"
      this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.setData(segData);
      if(this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.length===0)
        this.showBulkUpdatePopupScreen1(false);
      this.view.forceLayout();      
    }
  },
  showContractAccountsScreen: function(isNextClick){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow3,this.view.verticalTabsContract.btnOption3);
    if(isNextClick)
      this.view.flxContractDetailsTab.setVisibility(false);
    else
      this.hideAllScreens();
    this.view.imgContractAccounts.setVisibility(true);
    this.view.lblNoCustomersSelected.text=kony.i18n.getLocalizedString("i18n.contracts.selectCustomerAccount");
    this.view.flxNoCustomerSelected.setVisibility(true);
    this.view.flxContractAccountsList.setVisibility(false);
    this.view.flxContractAccounts.setVisibility(true);
    this.view.flxContractAccountsSearch.setVisibility(false);
    this.view.tbxAccountsSearch.text="";
    this.view.flxClearAccountsSearch.setVisibility(false);
    this.setCustomersDropDownList("customersDropdown");
    this.view.ContractAccountsList.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmCompanies.Selected_Accounts") + ":";
    this.view.forceLayout();
  },
  showContractFAScreen : function(isNextClick){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow4,this.view.verticalTabsContract.btnOption4);
    if(isNextClick)
      this.view.flxContractAccounts.setVisibility(false);
    else
      this.hideAllScreens();
    this.view.imgContractFA.setVisibility(true);
    this.view.lblNoCustomersSelectedFA.text=kony.i18n.getLocalizedString("i18n.contracts.selectCustomerFeatures");
    this.view.flxNoCustomerSelectedFA.setVisibility(true);
    this.view.flxContractFAList.setVisibility(false);
    this.view.flxContractFA.setVisibility(true);
    this.view.flxContractFASearch.setVisibility(false);
    this.view.tbxContractFASearch.text="";
    this.view.flxClearContractFASearch.setVisibility(false);
    this.setCustomersDropDownList("customersDropdownFA");
    this.view.ContractFAList.lblHeading.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomersController.SelectedFeatures") + ":";
    this.view.forceLayout();
  },
  showContractLimitsScreen: function(isNextClick){
    this.toggleContractVerticalTabs(this.view.verticalTabsContract.flxImgArrow5,this.view.verticalTabsContract.btnOption5);
    if(isNextClick)
      this.view.flxContractFA.setVisibility(false);
    else
      this.hideAllScreens();
    this.view.imgContractLimits.setVisibility(true);
    this.view.lblNoCustomersSelectedLimits.text=kony.i18n.getLocalizedString("i18n.contracts.selectCustomerLimits");
    this.view.flxNoCustomerSelectedLimits.setVisibility(true);
    this.view.flxContractLimitsList.setVisibility(false);
    this.view.flxContractLimits.setVisibility(true);
    this.view.flxContractLimitsSearch.setVisibility(false);
    this.view.tbxContractLimitsSearch.text="";
    this.view.flxClearContractLimitsSearch.setVisibility(false);
    this.setCustomersDropDownList("customersDropdownLimits");
    //to store the limits actual values for customers which is used in reset limits page 
	if(Object.keys(this.limitsActualJSON).length === 0){
    var monetaryLimits={};
    var monetaryActions={};
    var actionsJSON={};
    var limitsJSON={};
    for(let w=0;w<this.createContractRequestParam.contractCustomers.length;w++){
      var featuresList=this.createContractRequestParam.contractCustomers[w].features;
      monetaryLimits={};
      for(let x=0;x<featuresList.length;x++){
        var featureId=featuresList[x].id||featuresList[x].featureId;
        if(featuresList[x].type==="MONETARY"&&featuresList[x].actions.length>0){
          monetaryLimits[featureId]={};
          monetaryActions =featuresList[x].actions.filter(function(item) {
            return item.type=="MONETARY";
          });
          for(let b=0;b<monetaryActions.length;b++){
            actionsJSON={};
            limitsJSON={};
            var actionId=monetaryActions[b].actionId||monetaryActions[b].id;
            actionsJSON[actionId]={};
            limitsJSON[monetaryActions[b].limits[0].id]=monetaryActions[b].limits[0].value;
            limitsJSON[monetaryActions[b].limits[1].id]=monetaryActions[b].limits[1].value;
            limitsJSON[monetaryActions[b].limits[2].id]=monetaryActions[b].limits[2].value;
            actionsJSON[actionId]=limitsJSON;
          }
          monetaryLimits[featureId]=actionsJSON;        
        }
      }
      this.limitsActualJSON[this.createContractRequestParam.contractCustomers[w].coreCustomerId]=monetaryLimits;
    }
    }
    this.view.forceLayout();
  },
  showContractSearch : function(currFlex){
    currFlex.setVisibility(false);
    this.view.flxContractServiceTab.setVisibility(true);
    this.view.flxCreateContract.setVisibility(false);
    this.view.flxSearchCompanies.setVisibility(true);
    this.view.flxNoFilterResults.setVisibility(true);
    this.view.segSearchResults.setVisibility(false);
    this.view.mainHeader.btnAddNewOption.setVisibility(true);
    this.view.mainHeader.btnDropdownList.setVisibility(true);
    this.view.flxSettings.setVisibility(false);
    this.view.flxBreadcrumb.setVisibility(false);
    
    //add user visibilty changes
    this.view.flxAddUser.setVisibility(false);
    
    this.view.flxSearchUser.setVisibility(true);
    this.view.flxSearchInner.setVisibility(true);
    this.view.flxUserAdded.setVisibility(true);
    this.view.flxBottomButtonContianer.setVisibility(true);
    
    this.view.flxEditAddedUserDetails.setVisibility(false);
    this.view.flxEditUserContainer.setVisibility(true);
    this.view.flxAddeUserEditVerticalTabs.setVisibility(true);
    this.view.flxEditUserRightContainer.setVisibility(true);
    this.view.flxAccountTypesFilter.setVisibility(false);
    
    this.view.forceLayout();
  },
  setContractSearchResults : function(){
    
    var selectedInd=this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex?this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex[1]:null;
    var status="" ,desc="";
    if(selectedInd!==undefined&&selectedInd!==null){
      status=this.view.AdvancedSearchDropDownServType.sgmentData.data[selectedInd].customerStatusId
      desc=this.view.AdvancedSearchDropDownServType.sgmentData.data[selectedInd].lblDescription;
    }

    let contractInfo = {
      "contractId": this.view.txtSearchParam2.text.trim(),
      "contractName": this.view.txtSearchParam1.text.trim(),
      "coreCustomerId": this.view.txtSearchParam4.text.trim(),
      "coreCustomerName": this.view.txtSearchParam6.text.trim(),
      "email": this.view.txtSearchParam5.text.trim(),
      "phoneCountryCode": this.view.textBoxSearchContactNumber.txtISDCode.text.trim(),
      "phoneNumber": this.view.textBoxSearchContactNumber.txtContactNumber.text.trim() ,
      "country": this.view.txtSearchParam3.text.trim(),
      "serviceDefinitionId": desc.trim()
      };
    let srchTxt = '';
    let keys = Object.keys(contractInfo);

    keys.forEach(function(key){
      // return in a forEach() callback is equivalent to continue
      if(contractInfo[key] === ""){
         return;
      }

      srchTxt += contractInfo[key]+' , ';
    });
    // removing the last , from the search string
    if(srchTxt.indexOf(',') >=0){
      srchTxt = srchTxt.substring(0, srchTxt.length - 2);
    }    
    this.view.lblResultsFor.text = 'Showing Results for ' + srchTxt;

    contractInfo["serviceDefinitionId"] = status.trim();
    this.presenter.getSearchContract(contractInfo);
  },
  /*
  * create features card at customer level
  */
  setFeaturesDataCustomers : function(featureData){
    this.view.ContractFAList.lblName.setVisibility(true);
    this.view.ContractFAList.toggleCollapseArrow(true);
    this.view.ContractFAList.flxArrow.setVisibility(false);
    this.view.ContractFAList.flxSelectAllOption.setVisibility(true);
    this.view.ContractFAList.flxCardBottomContainer.setVisibility(true);
    this.view.ContractFAList.flxCheckbox.onClick = this.onSelectAllFeaturesClick.bind(this,this.view.ContractFAList);
    this.setFeaturesAtCustomerLevel(this.view.ContractFAList.segAccountFeatures,featureData);
  },
  /*
  * widget map function for edit user features segment
  * @returns: widget data map for features segment
  */
  getWidgetDataMapForFeatures : function(){
    var widgetMap = {
      "id":"id",
      "isRowVisible":"isRowVisible",
      "flxFeatureNameCont":"flxFeatureNameCont",
      "imgCheckbox":"imgCheckbox",
      "flxCheckbox":"flxCheckbox",
      "lblStatus":"lblStatus",
      "flxStatus":"flxStatus",
      "lblIconStatusTop":"lblIconStatusTop",
      "lblIconStatus":"lblIconStatus",
      "flxContractEnrollFeaturesEditRow":"flxContractEnrollFeaturesEditRow",
      "lblTopSeperator":"lblTopSeperator",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxToggleArrow":"flxToggleArrow",
      "lblIconToggleArrow":"lblIconToggleArrow",
      "lblFeatureName":"lblFeatureName",
      "lblStatusValue":"lblStatusValue",
      "lblBottomSeperator":"lblBottomSeperator",
      "lblAvailableActions":"lblAvailableActions",
      "lblCountActions":"lblCountActions",
      "lblTotalActions":"lblTotalActions",
      "flxContractEnrollFeaturesEditSection":"flxContractEnrollFeaturesEditSection",
      "featureData":"featureData"
    };
    return widgetMap;
  },
   /*
  * set features and actions segment data in feature card
  * @param: segment widget path
  */
  setFeaturesAtCustomerLevel : function(segmentPath,featureData){
    var self =this;
    var selectedFeaturesCount=0;
    var featuresSegData = featureData.map(function(rec){
      var segRowData = [];
      var segSecData = {
        "id":rec.id||rec.featureId,
        "lblTopSeperator":{"isVisible":false},
        "flxCheckbox":{"onClick": self.onSectionCheckboxClick.bind(self,segmentPath)},
        "imgSectionCheckbox":{"src":"checkboxselected.png"},
        "lblIconToggleArrow":{"text":"\ue922","skin":"sknfontIconDescRightArrow14px"},
        "flxToggleArrow":{"onClick": self.toggleSegmentSectionArrow.bind(self,segmentPath)},
        "lblFeatureName":rec.name||rec.featureName,
        "lblStatusValue":{"text":rec.status=== "SID_FEATURE_ACTIVE" ||rec.featureStatus=== "SID_FEATURE_ACTIVE" ?kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive")},
        "lblIconStatusTop":{"skin":rec.status === "SID_FEATURE_ACTIVE"||rec.featureStatus=== "SID_FEATURE_ACTIVE" ?"sknFontIconActivate":"sknfontIconInactive","text":"\ue921"},
        "lblBottomSeperator":"-",
        "lblAvailableActions":kony.i18n.getLocalizedString("i18n.contracts.availableActions")+": ",
        "lblCountActions":{"text":rec.actions.length},
        "lblTotalActions":"of "+rec.actions.length,
        "featureData":rec,
        "template":"flxContractEnrollFeaturesEditSection"
      };
      var selectedActionsCount=0;
      var dependentActions=[];
      for(var i=0;i < rec.actions.length; i++){
        dependentActions=[];
        var actionImg="checkboxselected.png";
        if(rec.actions[i].isEnabled){
          actionImg=rec.actions[i].isEnabled==="true"?"checkboxselected.png":"checkboxnormal.png";
        }
        if(actionImg==="checkboxselected.png")
          selectedActionsCount++;
        if(rec.actions[i].dependentActions&&rec.actions[i].dependentActions.length>0){
          if(typeof rec.actions[i].dependentActions==="string")//as we are getting string format in edit flow and object format in create flow
            dependentActions=(rec.actions[i].dependentActions.substring(1,rec.actions[i].dependentActions.length-1)).split(",");
          else
            dependentActions=rec.actions[i].dependentActions.map(function(rec){return rec.id});
        }
        segRowData.push({
          "id":rec.actions[i].id||rec.actions[i].actionId,
          "isRowVisible": false,
          "dependentActions":dependentActions,
          "flxContractEnrollFeaturesEditRow":{"isVisible":false},
          "flxFeatureNameCont":{"isVisible":true},
          "imgCheckbox":{"src":actionImg},
          "flxCheckbox":{"onClick":self.onClickFeaturesRowCheckbox.bind(self,segmentPath)},
          "lblFeatureName":{"text":rec.actions[i].name||rec.actions[i].actionName},
          "lblStatus":{"text":rec.actions[i].actionStatus === "SID_ACTION_ACTIVE" ?kony.i18n.getLocalizedString("i18n.secureimage.Active") : kony.i18n.getLocalizedString("i18n.secureimage.Inactive")},
          "lblIconStatus":{"skin":rec.actions[i].actionStatus === "SID_ACTION_ACTIVE"?"sknFontIconActivate":"sknfontIconInactive"},
          "template":"flxContractEnrollFeaturesEditRow",
        });
      }
      var headerImg=self.getHeaderCheckboxImage(segRowData,true, true);
      segSecData.imgSectionCheckbox.src=headerImg;
      segSecData.lblCountActions.text=selectedActionsCount.toString();
      return [segSecData, segRowData];
    });
    segmentPath.widgetDataMap = this.getWidgetDataMapForFeatures();
    segmentPath.rowTemplate="flxContractEnrollFeaturesEditRow";
    segmentPath.setData(featuresSegData);
    this.view.ContractFAList.lblCount.text= this.getSelectedItemsCount(featuresSegData, false);
    this.view.ContractFAList.lblTotalCount.text= "of " + this.getTwoDigitNumber(featuresSegData.length) ;
    this.view.ContractFAList.imgSectionCheckbox.src=self.getHeaderCheckboxImage(featuresSegData,false, true);
/*
To validate whether atleast one feature is selected or not
    var isValid = this.validateCheckboxSelections(segmentPath,true);
    if(isValid){
      this.view.flxCustomerDropdownFA.setEnabled(true);
      this.enableAllTabs(true);
    }else{
      this.view.flxCustomerDropdownFA.setEnabled(false);
      this.enableAllTabs(false);
    }
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnNext,false,isValid);
*/
    kony.adminConsole.utils.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  onSectionCheckboxClick : function(segmentWidPath,event){
    var selectedCustId=this.view.customersDropdownFA.lblSelectedValue.info.id;
    var selSecInd = event.rowContext.sectionIndex;
    var segData = segmentWidPath.data;
    var isActionEnabled="false";
    var img = (segData[selSecInd][0].imgSectionCheckbox.src === "checkboxnormal.png") ? "checkboxselected.png" : "checkboxnormal.png";
    var actionIds=[];
    segData[selSecInd][0].imgSectionCheckbox.src = img;
    for(var i =0;i<segData[selSecInd][1].length; i++){
      actionIds.push(segData[selSecInd][1][i].id);
      segData[selSecInd][1][i].imgCheckbox.src = img;
    }
    if(img==="checkboxnormal.png"){
        segData[selSecInd][0].lblCountActions.text="0";
      }
      else{
        segData[selSecInd][0].lblCountActions.text=segData[selSecInd][1].length.toString();
      }
    segmentWidPath.setSectionAt(segData[selSecInd],selSecInd);
    isActionEnabled=img==="checkboxnormal.png"?"false":"true";
    for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
      if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
        for(var k=0;k<this.createContractRequestParam.contractCustomers[j].features.length;k++){
          if(this.createContractRequestParam.contractCustomers[j].features[k].featureId===segData[selSecInd][0].id){
            for(var l=0;l<this.createContractRequestParam.contractCustomers[j].features[k].actions.length;l++){
              if(actionIds.includes(this.createContractRequestParam.contractCustomers[j].features[k].actions[l].actionId))
              this.createContractRequestParam.contractCustomers[j].features[k].actions[l].isEnabled=isActionEnabled;
            }
            break;
          }
        }
        break;
      }
    }
    //set image for select all features image
    this.view.ContractFAList.imgSectionCheckbox.src = this.getHeaderCheckboxImage(segData,false,true);
    this.view.ContractFAList.lblCount.text = this.getSelectedItemsCount(segData, false);
    /*
	var isValid = this.validateCheckboxSelections(segmentWidPath,true);
    if(isValid){
      this.view.flxCustomerDropdownFA.setEnabled(true);
      this.enableAllTabs(true);
    }else{
      this.view.flxCustomerDropdownFA.setEnabled(false);
      this.enableAllTabs(false);
    }
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnNext,false,isValid);
    */
    this.view.forceLayout();
  },
    /*
  * select all features click
  * @param: selected card widget path
  */
  onSelectAllFeaturesClick : function(cardWidgetPath){
    var selectedCustId=this.view.customersDropdownFA.lblSelectedValue.info.id;
    var segData = cardWidgetPath.segAccountFeatures.data;
    var img = (cardWidgetPath.imgSectionCheckbox.src === this.AdminConsoleCommonUtils.checkboxnormal) ?
        this.AdminConsoleCommonUtils.checkboxSelected : this.AdminConsoleCommonUtils.checkboxnormal;
    cardWidgetPath.imgSectionCheckbox.src = img;
    cardWidgetPath.lblCount.text = (img === this.AdminConsoleCommonUtils.checkboxnormal ? "0": this.getTwoDigitNumber(segData.length));
    var featureIds=[];
    for(var i=0;i<segData.length; i++){
      segData[i][0].imgSectionCheckbox.src = img;
      segData[i][0].lblCountActions.text=img==="checkboxnormal.png"?"0":segData[i][1].length.toString();
      featureIds.push(segData[i][0].id);
      for(var j=0;j<segData[i][1].length; j++){
        segData[i][1][j].imgCheckbox.src = img;
      }
    }
    var isActionEnabled=img==="checkboxnormal.png"?"false":"true";
    for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
      if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
        for(var k=0;k<this.createContractRequestParam.contractCustomers[j].features.length;k++){
          if(featureIds.includes(this.createContractRequestParam.contractCustomers[j].features[k].featureId)){
            for(var l=0;l<this.createContractRequestParam.contractCustomers[j].features[k].actions.length;l++){
              this.createContractRequestParam.contractCustomers[j].features[k].actions[l].isEnabled=isActionEnabled;
            }
          }
        }
        break;
      }
    }
    cardWidgetPath.segAccountFeatures.setData(segData);
    /*
    //enable/disable save buttons
    var isValid = this.validateSelectionForMultipleCards(cardWidgetPath,"segAccountFeatures",true);
    if(isValid){
      this.view.flxCustomerDropdownFA.setEnabled(true);
      this.enableAllTabs(true);
    }
    else{
      this.view.flxCustomerDropdownFA.setEnabled(false);
      this.enableAllTabs(false);
    }
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnNext,false,isValid);
    */
    this.view.forceLayout();
  },
    /*
  * expand/collapse the rows under a section
  * @param: segment widget path, event
  */
  toggleSegmentSectionArrow : function(segmentWidgetPath,event){
    var segData = segmentWidgetPath.data;
    var selectedSecInd = event.rowContext.sectionIndex;
    //update remaining sections
    for(var i=0;i< segData.length;i++){
      segData[i][0].lblTopSeperator.isVisible = false;
      if(selectedSecInd !== i){
        segData[i][0].lblIconToggleArrow.text = "\ue922";
        segData[i][0].lblIconToggleArrow.skin = "sknfontIconDescRightArrow14px";
        segData[i][1] = this.showHideSegRowFlex(segData[i][1],false);
      }
    }
    //update selected section
    if(segData[selectedSecInd][1][0].isRowVisible === false){
      segData[selectedSecInd][0].lblIconToggleArrow.text = "\ue915";
      segData[selectedSecInd][0].lblIconToggleArrow.skin = "sknfontIconDescDownArrow12px";
      segData[selectedSecInd][1] = this.showHideSegRowFlex(segData[selectedSecInd][1],true);
      if(selectedSecInd < (segData.length-1)){
        segData[selectedSecInd+1][0].lblTopSeperator.isVisible = true;
      }
    } else{
      segData[selectedSecInd][0].lblIconToggleArrow.text = "\ue922";
      segData[selectedSecInd][0].lblIconToggleArrow.skin = "sknfontIconDescRightArrow14px";
      segData[selectedSecInd][1] = this.showHideSegRowFlex(segData[selectedSecInd][1],false);
      if(selectedSecInd < (segData.length-1)){
        segData[selectedSecInd+1][0].lblTopSeperator.isVisible = false;
      }
    }
    segmentWidgetPath.setData(segData);
  },
    /*
  * uncheck/check features row checkbox
  * @param: segment widget path, event
  */
  onClickFeaturesRowCheckbox: function(segmentWidPath,event){
    var selectedCustId=this.view.customersDropdownFA.lblSelectedValue.info.id;
    var selSecInd = event.rowContext.sectionIndex;
    var selRowInd = event.rowContext.rowIndex;
    var segData = segmentWidPath.data;
    var dependentActions=[];
    var updatedActionIds=[];
    var isActionEnabled="true";
    segData[selSecInd][1][selRowInd].imgCheckbox.src = (segData[selSecInd][1][selRowInd].imgCheckbox.src === "checkboxnormal.png") ? "checkboxselected.png" : "checkboxnormal.png";
    updatedActionIds.push(segData[selSecInd][1][selRowInd].id);
    if(segData[selSecInd][1][selRowInd].dependentActions.length>0){
      dependentActions=segData[selSecInd][1][selRowInd].dependentActions;
      for(let x=0;x<dependentActions.length;x++){
        for(let y=0;y<segData[selSecInd][1].length;y++){
          if(segData[selSecInd][1][y].id===dependentActions[x]){
            segData[selSecInd][1][y].imgCheckbox.src =segData[selSecInd][1][selRowInd].imgCheckbox.src;
            updatedActionIds.push(segData[selSecInd][1][y].id);
          }
        }
      }
    }
    isActionEnabled=segData[selSecInd][1][selRowInd].imgCheckbox.src === "checkboxnormal.png"?"false":"true";
    var headerCheckImg = this.getHeaderCheckboxImage(segData[selSecInd][1],true, true);
    segData[selSecInd][0].imgSectionCheckbox.src=headerCheckImg;
    
    //to update the selected actions count (dependent actions also)    
    segData[selSecInd][0].lblCountActions.text=(segData[selSecInd][1][selRowInd].imgCheckbox.src==="checkboxnormal.png")?(parseInt(segData[selSecInd][0].lblCountActions.text)-updatedActionIds.length):(parseInt(segData[selSecInd][0].lblCountActions.text)+updatedActionIds.length);
	segmentWidPath.setSectionAt(segData[selSecInd],selSecInd);
    for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
      if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
        for(var k=0;k<this.createContractRequestParam.contractCustomers[j].features.length;k++){
          for(var l=0;l<this.createContractRequestParam.contractCustomers[j].features[k].actions.length;l++){
            if(updatedActionIds.includes(this.createContractRequestParam.contractCustomers[j].features[k].actions[l].actionId))
              this.createContractRequestParam.contractCustomers[j].features[k].actions[l].isEnabled=isActionEnabled;
          }
        }
        break;
      }
    }
    //set image for select all features image
    var headerCheckImg = this.getHeaderCheckboxImage(segData,false,true);
    this.view.ContractFAList.imgSectionCheckbox.src=headerCheckImg;
    this.view.ContractFAList.lblCount.text= this.getSelectedItemsCount(segData, false);
    /*
    var isValid = this.validateCheckboxSelections(segmentWidPath,true);
    if(isValid){
      this.view.flxCustomerDropdownFA.setEnabled(true);
      this.enableAllTabs(true);
    }else{
      this.view.flxCustomerDropdownFA.setEnabled(false);
      this.enableAllTabs(false);
    }
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnNext,false,isValid);
    */
    this.view.forceLayout();
  },
    /*
  * set segment rows visibility
  * @params: rows array, visibility - true/false
  * @return: updated rows data with visibilty
  */
  showHideSegRowFlex : function(rowsData,visibility){
    for(var i=0;i<rowsData.length;i++){
      if(rowsData[i].flxContractEnrollFeaturesEditRow){
        rowsData[i].flxContractEnrollFeaturesEditRow.isVisible = visibility;
      }else{
        rowsData[i].flxAssignLimits.isVisible =visibility;
      }
      rowsData[i].isRowVisible =visibility;
    }
    return rowsData;
  },
 /*
  * create limits card at customer level
  */
  setLimitsDataForCustomer : function(limitsData){
    this.view.ContractLimitsList.flxCardBottomContainer.setVisibility(true);
    this.setLimitsAtCustomerLevel(limitsData);
  },
   /*
  * widget map function for edit user limits segment at customer level
  * @returns: widget data map for limits segment
  */
  getWidgetMapForLimitsCustLevel : function(){
    var widgetMap = {
      "flxContractsLimitsHeaderCreate":"flxContractsLimitsHeaderCreate",
      "flxHeader":"flxHeader",
      "flxActionDetails":"flxActionDetails",
      "flxRow1":"flxRow1",
      "flxToggle":"flxToggle",
      "lblToggle":"lblToggle",
      "lblFeatureName":"lblFeatureName",
      "flxRow2":"flxRow2",
      "lblMonetaryActions":"lblMonetaryActions",
      "lblCountActions":"lblCountActions",
      "flxViewLimitsHeader":"flxViewLimitsHeader",
      "lblActionHeader":"lblActionHeader",
      "flxPerLimitHeader":"flxPerLimitHeader",
      "lblPerLimitHeader":"lblPerLimitHeader",
      "flxLimitInfo1":"flxLimitInfo1",
      "fontIconInfo1":"fontIconInfo1",
      "flxDailyLimitHeader":"flxDailyLimitHeader",
      "lblDailyLimitHeader":"lblDailyLimitHeader",
      "flxLimitInfo2":"flxLimitInfo2",
      "fontIconInfo2":"fontIconInfo2",
      "flxWeeklyLimitHeader":"flxWeeklyLimitHeader",
      "lblWeeklyLimitHeader":"lblWeeklyLimitHeader",
      "flxLimitInfo3":"flxLimitInfo3",
      "fontIconInfo3":"fontIconInfo3",
      "lblFASeperator2":"lblFASeperator2",
      "lblFASeperator1":"lblFASeperator1",
      "lblFASeperatorTop":"lblFASeperatorTop",
      "flxContractsLimitsBodyCreate":"flxContractsLimitsBodyCreate",
      "flxLimitActionName":"flxLimitActionName",
      "flxRangeIcon":"flxRangeIcon",
      "lblIconRangeInfo":"lblIconRangeInfo",
      "flxViewLimits":"flxViewLimits",
      "lblAction":"lblAction",
      "flxPerLimitTextBox":"flxPerLimitTextBox",
      "tbxPerValue":"tbxPerValue",
      "lblCurrencySymbol1":"lblCurrencySymbol1",
      "flxDailyLimitTextBox":"flxDailyLimitTextBox",
      "tbxDailyValue":"tbxDailyValue",
      "lblCurrencySymbol2":"lblCurrencySymbol2",
      "flxWeeklyLimitTextBox":"flxWeeklyLimitTextBox",
      "tbxWeeklyValue":"tbxWeeklyValue",
      "lblCurrencySymbol3":"lblCurrencySymbol3",
      "flxLimitError1":"flxLimitError1",
      "lblLimitError1":"lblLimitError1",
      "lblLimitErrorIcon1":"lblLimitErrorIcon1",
      "flxLimitError2":"flxLimitError2",
      "lblLimitError2":"lblLimitError2",
      "lblLimitErrorIcon2":"lblLimitErrorIcon2",
      "flxLimitError3":"flxLimitError3",
      "lblLimitError3":"lblLimitError3",
      "lblLimitErrorIcon3":"lblLimitErrorIcon3",
      "lblLimitsSeperator":"lblLimitsSeperator",
      "featureId":"featureId",
      "actionId":"actionId",
      "featuresData":"featuresData"
    };
    return widgetMap;
  },
   /*
  * set limits segment data in limits card at account level
  */
  setLimitsAtCustomerLevel : function(featuresData){
    var self=this;
    var segData=[];
    var segHeaderData=[];
    var segBodyData=[];
    var mappedData=[];
    for(var i=0;i<featuresData.length;i++){
      segBodyData=[];
      segHeaderData=this.mapContractLimitsHeader(featuresData[i],i);
      for(var j=0;j<featuresData[i].actions.length;j++){
        segBodyData.push(this.mapContractLimitsBody(featuresData[i].actions[j],featuresData,featuresData[i].id||featuresData[i].featureId));
      }
      segBodyData[j-1].lblLimitsSeperator.isVisible=false;
      //mappedData.push([segHeaderData,segBodyData]);
      segData.push([segHeaderData,segBodyData]);
    }
    this.view.ContractLimitsList.segAccountFeatures.widgetDataMap = this.getWidgetMapForLimitsCustLevel();
    this.view.ContractLimitsList.segAccountFeatures.rowTemplate = "flxContractsLimitsBodyCreate";
    this.view.ContractLimitsList.segAccountFeatures.setData(segData);
    if(this.view.flxContractLimits.info&&this.view.flxContractLimits.info.isValid===false)
    	this.validateLimits();
    //this.view.ContractLimitsList.segAccountFeatures.info={"data":mappedData};
    this.view.forceLayout();
  },
  mapContractLimitsHeader : function(feature,limitCount){
    var self=this;
    return{
      "template":"flxContractsLimitsHeaderCreate",
      "flxToggle":{"onClick":function(){
        self.toggleLimits();
      }
                  },
      "lblToggle":{"text":"\ue922"},//right arrow
      "flxViewLimitsHeader":{"isVisible":false},
      "lblFASeperatorTop":{"isVisible":true},
      "lblFeatureName":{"text":feature.name||feature.featureName},
      "lblMonetaryActions":{"text":kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.MonetaryActions")+": "},
      "lblCountActions":{"text":feature.actions.length<10?"0"+feature.actions.length:feature.actions.length},
      "lblActionHeader":{"text":kony.i18n.getLocalizedString("i18n.frmMfascenarios.ACTION_CAP")},
      "lblPerLimitHeader":{"text":kony.i18n.getLocalizedString("i18n.contracts.perTransaction_UC")},
      "flxLimitInfo1":{"onHover" : function(widget, context) {
			var info = kony.i18n.getLocalizedString("i18n.contracts.RangePerInfo")+" " + (feature.name||feature.featureName) + " "+kony.i18n.getLocalizedString("i18n.contracts.RangeMsg");
			self.onHoverCallBack(widget, context,info);
		}
                      },
      "fontIconInfo1":{"text":""},
      "lblDailyLimitHeader":{"text":kony.i18n.getLocalizedString("i18n.contracts.dailyTransaction_UC")},
      "flxLimitInfo2":{"onHover" : function(widget, context) {
			var info = kony.i18n.getLocalizedString("i18n.contracts.RangeDailyInfo")+" " + (feature.name||feature.featureName) + " "+kony.i18n.getLocalizedString("i18n.contracts.RangeMsg");
			self.onHoverCallBack(widget, context,info);
		}
                      },
      "fontIconInfo2":{"text":""},
      "lblWeeklyLimitHeader":{"text":kony.i18n.getLocalizedString("i18n.contracts.weeklyTransaction_UC")},
      "flxLimitInfo3":{"onHover" : function(widget, context) {
			var info = kony.i18n.getLocalizedString("i18n.contracts.RangeWeeklyInfo")+" " + (feature.name||feature.featureName) + " "+kony.i18n.getLocalizedString("i18n.contracts.RangeMsg");
			self.onHoverCallBack(widget, context,info);
		}
                      },
      "fontIconInfo3":{"text":""},
      "featureId":feature.id||feature.featureId,
      "lblFASeperator2":"-",
      "lblFASeperator1":"-"
    };
  },
  mapContractLimitsBody : function(action,featuresData,featureId){
    var self=this;
    var perLimit,dailyLimit,weeklyLimit=0;
    for(var x=0;x<action.limits.length;x++){
      if(action.limits[x].id==="DAILY_LIMIT")
        dailyLimit=action.limits[x].value;
      if(action.limits[x].id==="MAX_TRANSACTION_LIMIT")
        perLimit=action.limits[x].value;
      if(action.limits[x].id==="WEEKLY_LIMIT")
        weeklyLimit=action.limits[x].value;
    }
    var range=action.id?this.monetaryLimits[featureId][action.id]:this.monetaryLimits[featureId][action.actionId];
    return{
      "actionId":action.id||action.actionId,
      "featuresData":featuresData,
      "template":"flxContractsLimitsBodyCreate",
      "flxContractsLimitsBodyCreate":{"isVisible":false},
      "lblAction":{"text":action.name||action.actionName},
      "flxRangeIcon":{"onHover" : function(widget,context){self.showRangeTooltip(widget,context,range);},"info":{"range":range}},
      "lblIconRangeInfo":{"text":"\ue9b9"},
      "flxPerLimitTextBox":{"skin":"sknflxEnterValueNormal"},
      "tbxPerValue":{"text":perLimit},
      "lblCurrencySymbol1":{"text":self.defaultCurrencyCodeSymbol},
      "flxDailyLimitTextBox":{"skin":"sknflxEnterValueNormal"},
      "tbxDailyValue":{"text":dailyLimit},
      "lblCurrencySymbol2":{"text":self.defaultCurrencyCodeSymbol},
      "flxWeeklyLimitTextBox":{"skin":"sknflxEnterValueNormal"},
      "tbxWeeklyValue":{"text":weeklyLimit},
      "lblCurrencySymbol3":{"text":self.defaultCurrencyCodeSymbol},
      "flxLimitError1":{"isVisible":false},
      "lblLimitError1":{"text":""},
      "lblLimitErrorIcon1":{"text":""},
      "flxLimitError2":{"isVisible":false},
      "lblLimitError2":{"text":""},
      "lblLimitErrorIcon2":{"text":""},
      "flxLimitError3":{"isVisible":false},
      "lblLimitError3":{"text":""},
      "lblLimitErrorIcon3":{"text":""},
      "lblLimitsSeperator":{"text":"-","isVisible":true}
    }
  },
  updateLimitValues : function(){
    var selectedCustId=this.view.customersDropdownLimits.lblSelectedValue.info.id;
    var isValid=true;
    var segData=this.view.ContractLimitsList.segAccountFeatures.data?this.view.ContractLimitsList.segAccountFeatures.data:[];
    // To update limit values in global variable
      for(let a=0;a<segData.length;a++){
        for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
          if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
            for(let k=0;k<this.createContractRequestParam.contractCustomers[j].features.length;k++){
              if((this.createContractRequestParam.contractCustomers[j].features[k].id&&this.createContractRequestParam.contractCustomers[j].features[k].id===segData[a][0].featureId)||(this.createContractRequestParam.contractCustomers[j].features[k].featureId&&this.createContractRequestParam.contractCustomers[j].features[k].featureId===segData[a][0].featureId)){
                for(var b=0;b<segData[a][1].length;b++){
                  for(let l=0;l<this.createContractRequestParam.contractCustomers[j].features[k].actions.length;l++){
                    if((this.createContractRequestParam.contractCustomers[j].features[k].actions[l].id&&this.createContractRequestParam.contractCustomers[j].features[k].actions[l].id===segData[a][1][b].actionId)||
                       (this.createContractRequestParam.contractCustomers[j].features[k].actions[l].actionId&&this.createContractRequestParam.contractCustomers[j].features[k].actions[l].actionId===segData[a][1][b].actionId)){
                      this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits[1].value=segData[a][1][b].tbxPerValue.text;
                      this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits[2].value=segData[a][1][b].tbxDailyValue.text;
                      this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits[0].value=segData[a][1][b].tbxWeeklyValue.text;
                      break;
                    }
                  }
                }
                break;
              }
            }
            break;
          }
        }
      }
  },
  // To get customer Id's list where there is limit violation
  validateAllLimits : function(){
    var range="";
    var featureId="";
    var actionId="";
    var errorCustIds=[];
    var isRangeValid=true;
    var isValid=true;
    var isEnabled="true";
    this.updateLimitValues();
    for(var x=0;x<this.createContractRequestParam.contractCustomers.length;x++){
      for(let y=0;y<this.createContractRequestParam.contractCustomers[x].features.length;y++){
        if(this.createContractRequestParam.contractCustomers[x].features[y].type==="MONETARY"){
          featureId=this.createContractRequestParam.contractCustomers[x].features[y].featureId;
          for(let z=0;z<this.createContractRequestParam.contractCustomers[x].features[y].actions.length;z++){
            isEnabled=this.createContractRequestParam.contractCustomers[x].features[y].actions[z].isEnabled?this.createContractRequestParam.contractCustomers[x].features[y].actions[z].isEnabled:"true";
            if(this.createContractRequestParam.contractCustomers[x].features[y].actions[z].type==="MONETARY"&&isEnabled==="true"){
              actionId=this.createContractRequestParam.contractCustomers[x].features[y].actions[z].actionId;
              isRangeValid=true;
              range=this.monetaryLimits[featureId][actionId];
              var weekly=parseFloat(this.createContractRequestParam.contractCustomers[x].features[y].actions[z].limits[0].value);
              var daily=parseFloat(this.createContractRequestParam.contractCustomers[x].features[y].actions[z].limits[2].value);
              var per=parseFloat(this.createContractRequestParam.contractCustomers[x].features[y].actions[z].limits[1].value);
              if(weekly>parseFloat(range["WEEKLY_LIMIT"])||weekly<parseFloat(range["MIN_TRANSACTION_LIMIT"])||weekly<daily)
                isRangeValid=false;
              if(per>parseFloat(range["MAX_TRANSACTION_LIMIT"])||per<parseFloat(range["MIN_TRANSACTION_LIMIT"])||per>daily)
                isRangeValid=false;
              if(daily>parseFloat(range["DAILY_LIMIT"])||daily<parseFloat(range["MIN_TRANSACTION_LIMIT"]))
                isRangeValid=false;
              if(isRangeValid===false){
                errorCustIds.push(this.createContractRequestParam.contractCustomers[x].coreCustomerId);
                break;
              }
            }
          }
          //if this customer Id is already pushed
          if(errorCustIds.includes(this.createContractRequestParam.contractCustomers[x].coreCustomerId))
            break;
        }
      }
    }
    if(errorCustIds.length>0){
      isValid=false;
      this.view.flxContractLimits.info={"isValid":false};
      if(errorCustIds.length>1){
      this.view.flxLimitsErrorFlag.setVisibility(true);
      this.view.flxContractLimitsContainer.top="55px";
      }else{
        this.view.flxLimitsErrorFlag.setVisibility(false);
      this.view.flxContractLimitsContainer.top="0px"
      }
      this.view.customersDropdownLimits.lblSelectedValue.info={"id":errorCustIds[0]};
      this.setSelectedText("customersDropdownLimits",errorCustIds[0]);
      this.setCustSelectedData("customersDropdownLimits",false);
    }else{
      this.view.flxContractLimits.info={"isValid":true};
      isValid=true;
      this.view.flxLimitsErrorFlag.setVisibility(false);
      this.view.flxContractLimitsContainer.top="0px";
    }
    this.view.forceLayout();
    return isValid;
  },
  validateLimits : function(){
    var errMsg="";
    var isValid=true;
    var segData=this.view.ContractLimitsList.segAccountFeatures.data;
    var range="";
    var errorIndices=[];
    var isSectionValid=true;
    var errorLeft="75%";
    for(var a=0;a<segData.length;a++){
      isSectionValid=true;
      for(var b=0;b<segData[a][1].length;b++){
        range=segData[a][1][b].flxRangeIcon.info.range;
        //per transaction limit validation
        if(parseFloat(range["MAX_TRANSACTION_LIMIT"])<parseFloat(segData[a][1][b].tbxPerValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxPerValue.text)||
           parseFloat(segData[a][1][b].tbxPerValue.text)>parseFloat(segData[a][1][b].tbxDailyValue.text)){
          isValid=false;
          isSectionValid=false;
          segData[a][1][b].lblLimitError1.skin="sknlblErrorIcon12px";
          if(parseFloat(range["MAX_TRANSACTION_LIMIT"])<parseFloat(segData[a][1][b].tbxPerValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxPerValue.text))
            segData[a][1][b].lblLimitError1.text=kony.i18n.getLocalizedString("i18n.contracts.valueShouldBeWithin")+" "+this.defaultCurrencyCodeSymbol+range["MIN_TRANSACTION_LIMIT"]+" - "+this.defaultCurrencyCodeSymbol+range["MAX_TRANSACTION_LIMIT"];          
          else
            segData[a][1][b].lblLimitError1.text=kony.i18n.getLocalizedString("i18n.contracts.perLimitMsg");
          
          segData[a][1][b].flxLimitError1.isVisible=true;
          segData[a][1][b].flxPerLimitTextBox.skin="sknflxEnterValueError";
        }else{
          segData[a][1][b].flxLimitError1.isVisible=false;
          segData[a][1][b].flxPerLimitTextBox.skin="sknflxEnterValueNormal";
        }
        //daily transaction limit validation
        if(parseFloat(range["DAILY_LIMIT"])<parseFloat(segData[a][1][b].tbxDailyValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxDailyValue.text)||
           parseFloat(segData[a][1][b].tbxDailyValue.text)>parseFloat(segData[a][1][b].tbxWeeklyValue.text)){
          isValid=false;
          isSectionValid=false;
          segData[a][1][b].lblLimitError2.skin="sknlblErrorIcon12px";
          if(parseFloat(range["DAILY_LIMIT"])<parseFloat(segData[a][1][b].tbxDailyValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxDailyValue.text))
            segData[a][1][b].lblLimitError2.text=kony.i18n.getLocalizedString("i18n.contracts.valueShouldBeWithin")+" "+this.defaultCurrencyCodeSymbol+range["MIN_TRANSACTION_LIMIT"]+" - "+this.defaultCurrencyCodeSymbol+range["DAILY_LIMIT"];
          else
            segData[a][1][b].lblLimitError2.text=kony.i18n.getLocalizedString("i18n.contracts.dailyLimitMsg");
          segData[a][1][b].flxLimitError2.isVisible=true;
          segData[a][1][b].flxDailyLimitTextBox.skin="sknflxEnterValueError";
        }else{
          segData[a][1][b].flxLimitError2.isVisible=false;
          segData[a][1][b].flxDailyLimitTextBox.skin="sknflxEnterValueNormal";
        }
		//weekly transaction limit validation
        if(parseFloat(range["WEEKLY_LIMIT"])<parseFloat(segData[a][1][b].tbxWeeklyValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxWeeklyValue.text)||
           parseFloat(segData[a][1][b].tbxDailyValue.text)>parseFloat(segData[a][1][b].tbxWeeklyValue.text)){
          isValid=false;
          isSectionValid=false;
          segData[a][1][b].lblLimitError3.skin="sknlblErrorIcon12px";
          if(parseFloat(range["WEEKLY_LIMIT"])<parseFloat(segData[a][1][b].tbxWeeklyValue.text)||
           parseFloat(range["MIN_TRANSACTION_LIMIT"])>parseFloat(segData[a][1][b].tbxWeeklyValue.text))
            segData[a][1][b].lblLimitError3.text=kony.i18n.getLocalizedString("i18n.contracts.valueShouldBeWithin")+" "+this.defaultCurrencyCodeSymbol+range["MIN_TRANSACTION_LIMIT"]+" - "+this.defaultCurrencyCodeSymbol+range["WEEKLY_LIMIT"];
          else
            segData[a][1][b].lblLimitError3.text=kony.i18n.getLocalizedString("i18n.contracts.weeklyLimitMsg");
          segData[a][1][b].flxLimitError3.isVisible=true;
          segData[a][1][b].flxWeeklyLimitTextBox.skin="sknflxEnterValueError";
        }else{
          segData[a][1][b].flxLimitError3.isVisible=false;
          segData[a][1][b].flxWeeklyLimitTextBox.skin="sknflxEnterValueNormal";
        }
      }
      if(isSectionValid===false)
      	errorIndices.push(a);
    }
    this.view.ContractLimitsList.segAccountFeatures.setData(segData);
    if(isValid===false){
      this.view.flxContractLimits.info={"isValid":false};
      this.toggleLimits(errorIndices);
    }else{
      this.view.flxContractLimits.info={"isValid":true};
    }
    this.view.forceLayout();
    return isValid;
  },
  resetLimitValues : function(){
    var selectedCustId=this.view.customersDropdownLimits.lblSelectedValue.info.id;
    var segData=this.view.ContractLimitsList.segAccountFeatures.data;
    var featureId="";
    var actionId="";
    var limitId="";
    //should update request param with actual values from limitsActualJSON
    for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
      if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
        for(let k=0;k<this.createContractRequestParam.contractCustomers[j].features.length;k++){
          featureId=this.createContractRequestParam.contractCustomers[j].features[k].id||this.createContractRequestParam.contractCustomers[j].features[k].featureId;
          for(let l=0;l<this.createContractRequestParam.contractCustomers[j].features[k].actions.length;l++){
            if(this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits){
              actionId=this.createContractRequestParam.contractCustomers[j].features[k].actions[l].id||this.createContractRequestParam.contractCustomers[j].features[k].actions[l].actionId;
              for(var m=0;m<this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits.length;m++){
                limitId=this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits[m].id;
                this.createContractRequestParam.contractCustomers[j].features[k].actions[l].limits[m].value=this.limitsActualJSON[selectedCustId][featureId][actionId][limitId];
              }
            }
          }
        }
        this.setCustSelectedData("customersDropdownLimits",false);
        break;
      }
    }
    this.view.forceLayout();
  },
  toggleLimits : function(errIndices){
    var self = this;
    var selectedRowData = [];
    var segData=self.view.ContractLimitsList.segAccountFeatures.data;
    var sectionIndex =self.view.ContractLimitsList.segAccountFeatures.selectedsectionindex;
    var isExpanded=false;
    if(sectionIndex!==null&&sectionIndex!==undefined)
      isExpanded=segData[sectionIndex][0].flxViewLimitsHeader.isVisible?true:false;
    for(let a=0;a<segData.length;a++){
      segData[a][0].lblToggle.text="\ue922";
      segData[a][0].flxViewLimitsHeader.isVisible=false;
      for(let b=0;b<segData[a][1].length;b++)
      segData[a][1][b].flxContractsLimitsBodyCreate.isVisible=false;
    }
    if(errIndices){
      for(let i=0;i<errIndices.length;i++){
        segData[errIndices[i]][0].lblToggle.text="\ue915";
        segData[errIndices[i]][0].flxViewLimitsHeader.isVisible=true;
        for(let j=0;j<segData[errIndices[i]][1].length;j++)
        segData[errIndices[i]][1][j].flxContractsLimitsBodyCreate.isVisible=true;
      }
    }else{
      if(!isExpanded){
        segData[sectionIndex][0].lblToggle.text="\ue915";
        segData[sectionIndex][0].flxViewLimitsHeader.isVisible=true;
        for(let c=0;c<segData[sectionIndex][1].length;c++)
        segData[sectionIndex][1][c].flxContractsLimitsBodyCreate.isVisible=true;
      }else{
        segData[sectionIndex][0].lblToggle.text="\ue922";
        segData[sectionIndex][0].flxViewLimitsHeader.isVisible=false;
        for(let c=0;c<segData[sectionIndex][1].length;c++)
        segData[sectionIndex][1][c].flxContractsLimitsBodyCreate.isVisible=false;
      }
    }
    this.view.ContractLimitsList.segAccountFeatures.setData(segData);
    this.view.forceLayout();
  },
  onHoverCallBack:function(widget, context,info , isViewContractFlow = false , accountsFeaturesCard) {
    var scopeObj = this;
    var widGetId = widget.id;
    if (widget) {
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
        if(isViewContractFlow){
          scopeObj.showOnHoverInfoViewContract(context, widGetId, info , accountsFeaturesCard);
        }else{
          scopeObj.showOnHvrInfo(context, widGetId, info);
        }
      }else if (context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        scopeObj.view.ToolTip.setVisibility(true);
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        scopeObj.view.ToolTip.setVisibility(false);
      }
    }
  },
  showOnHvrInfo: function(featureSegment, widGetId, info) {
    var scopeObj = this;
    var leftVal = 0;
    var topVal=0;
    switch (widGetId) {
        case 'flxLimitInfo1':
            leftVal = featureSegment.pageX;
        	topVal = featureSegment.pageY;
            break;
        case 'flxLimitInfo2':
            leftVal = featureSegment.pageX ;
        	topVal = featureSegment.pageY;
            break;
        case 'flxLimitInfo3':
            leftVal = featureSegment.pageX ;
        	topVal = featureSegment.pageY;
            break;
    }
    topVal +=5;
    leftVal = leftVal - 190;
    scopeObj.view.ToolTip.left = leftVal + "px";
    scopeObj.view.ToolTip.top = topVal + "px";
    scopeObj.view.ToolTip.lblNoConcentToolTip.text = info;
    // scopeObj.view.ToolTip.lblarrow.skin =  "sknfonticonCustomersTooltipFFFFFF";
    scopeObj.view.ToolTip.imgUpArrow.setVisibility(true);
    scopeObj.view.ToolTip.lblarrow.setVisibility(false);

    scopeObj.view.ToolTip.flxToolTipMessage.skin ="sknFlxFFFFFF";
    scopeObj.view.ToolTip.lblNoConcentToolTip.skin = "sknLbl485C75LatoRegular13Px";
    scopeObj.view.ToolTip.setVisibility(true);
    scopeObj.view.forceLayout();
  },
  showOnHoverInfoViewContract: function(featureSegment, widGetId, info , accountsFeaturesCard) {
    var scopeObj = this;
    let e = document.getElementById('flxContractsLimitsHeaderView_'+widGetId);
    var rect = e.getBoundingClientRect();
    console.log(rect.top, rect.right, rect.bottom, rect.left);

    // 3 positions based on widget
    scopeObj.view.ToolTip.left = rect.left - 190 + 'px';
    scopeObj.view.ToolTip.top = rect.top +20 + "px";
    scopeObj.view.ToolTip.lblNoConcentToolTip.text = info;
    // scopeObj.view.ToolTip.lblarrow.skin =  "sknfonticonCustomersTooltipFFFFFF";
    scopeObj.view.ToolTip.imgUpArrow.setVisibility(true);
    scopeObj.view.ToolTip.lblarrow.setVisibility(false);  

    scopeObj.view.ToolTip.flxToolTipMessage.skin ="sknFlxFFFFFF";
    scopeObj.view.ToolTip.lblNoConcentToolTip.skin = "sknLbl485C75LatoRegular13Px";
    scopeObj.view.ToolTip.setVisibility(true);
    scopeObj.view.forceLayout();
  },
    /*
  * widget data map for accounts segment
  * @return: widget data map object
  */
  widgetMapForAccounts : function(){
    var widgetMap = {
      "flxHeaderContainer":"flxHeaderContainer",
      "flxAccountNumCont":"flxAccountNumCont",
      "lblAccountNumber":"lblAccountNumber",
      "lblIconSortAccName":"lblIconSortAccName",
      "flxCheckbox":"flxCheckbox",
      "imgSectionCheckbox":"imgSectionCheckbox",
      "flxAccountType":"flxAccountType",
      "lblAccountType":"lblAccountType",
      "lblIconFilterAccType":"lblIconFilterAccType",
      "lblAccountName":"lblAccountName",
      "flxAccountName":"flxAccountName",
      "lblIconAccNameSort":"lblIconAccNameSort",
      "flxAccountHolder":"flxAccountHolder",
      "lblAccountHolder":"lblAccountHolder",
      "lblIconSortAccHolder":"lblIconSortAccHolder",
      "lblSeperator":"lblSeperator",
      "flxContractEnrollAccountsEditSection":"flxContractEnrollAccountsEditSection",
      "imgCheckbox":"imgCheckbox",
      "flxAccFlag":"flxAccFlag",
      "lblNewText":"lblNewText",
      "fontIconFlag":"fontIconFlag",
      "flxContractEnrollAccountsEditRow":"flxContractEnrollAccountsEditRow"
    };
    return widgetMap;
  },
  setAccountsDataCustomers : function(customerAccounts){
    var self=this;
    var secData = {
      "flxCheckbox":{"onClick": this.onCheckAccountsCheckbox.bind(this,true)},
      "flxAccountNumCont":{"onClick":this.sortAndSetData.bind(this,"lblAccountNumber",this.view.ContractAccountsList.segAccountFeatures, 1)},
      "lblIconSortAccName":"\ue92a",
      "lblAccountNumber":kony.i18n.getLocalizedString("i18.authorizedSignatories.AccountNumber_UC"),
      "imgSectionCheckbox":{"src":"checkboxselected.png"},
      "flxAccountType":{"onClick": this.showFilterForAccountsSectionClick.bind(this,1)},
      "lblAccountType":kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTTYPE"),
      "lblIconFilterAccType":"\ue916",
      "lblAccountName":kony.i18n.getLocalizedString("i18n.frmCompanies.ACCOUNTNAME"),
      "lblIconAccNameSort":"\ue92b",
      "flxAccountName":{"onClick": this.sortAndSetData.bind(this,"lblAccountName", this.view.ContractAccountsList.segAccountFeatures, 1)},
      "flxAccountHolder":{"onClick": this.showFilterForAccountsSectionClick.bind(this,2)},
      "lblAccountHolder":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.OWNERSHIP"),
      "lblIconSortAccHolder":"\ue916",
      "lblSeperator":"-",
      "template":"flxContractEnrollAccountsEditSection",
    };
    var selectedAccountsCount=0;
    var imgCheckbox="checkboxnormal.png";
    var rowData = customerAccounts.map(function(account){
        // in edit/create , to check whether has made any selection changes or not
        if(account.isEnabled){
          if(account.isEnabled==="true"||account.isNew==="true"){
            selectedAccountsCount++;
            imgCheckbox= "checkboxselected.png";
          }else
            imgCheckbox="checkboxnormal.png";
        }
        else{
          imgCheckbox="checkboxselected.png";
          selectedAccountsCount++;
        }
      return{
        "flxCheckbox":{"onClick":self.onCheckAccountsCheckbox.bind(self,false)},
        "imgCheckbox":{"src":imgCheckbox},
        "lblAccountNumber":account.accountNumber,
        "lblAccountType":account.accountType,
        "lblAccountName":account.accountName,
        "lblAccountHolder":account.ownerType?account.ownerType:"N/A",
        "flxAccFlag":{"isVisible":account.isNew==="true"?true:false},
        "lblNewText":{"text":"New"},
        "fontIconFlag":{"text":""},
        "lblSeperator":"-",
        "template":"flxContractEnrollAccountsEditRow"
      }
    });
    secData.imgSectionCheckbox.src=this.getHeaderCheckboxImage(rowData,true,true);
    this.sortBy = this.getObjectSorter("lblAccountNumber");
    this.sortBy.inAscendingOrder = true;
    rowData = rowData.sort(this.sortBy.sortData);
    this.view.ContractAccountsList.segAccountFeatures.widgetDataMap = this.widgetMapForAccounts();
    this.view.ContractAccountsList.segAccountFeatures.rowTemplate="flxContractEnrollAccountsEditRow";
    this.view.ContractAccountsList.segAccountFeatures.setData([[secData,rowData]]);
    this.view.ContractAccountsList.segAccountFeatures.info={"selectedAccountsCount":selectedAccountsCount};
    this.view.ContractAccountsList.lblCount.text= this.getSelectedItemsCount(rowData, true);
    this.view.ContractAccountsList.lblTotalCount.text = "of "+ this.getTwoDigitNumber(rowData.length);
    this.view.forceLayout();
  },
  /*
  * check/uncheck checkbox in accounts tab header
  */
  onCheckAccountsCheckbox : function(isHeader) {
    var selectedCustId=this.view.customersDropdown.lblSelectedValue.info.id;
    var segData = this.view.ContractAccountsList.segAccountFeatures.data;
    var segSecData = segData[0][0];
    var rowsData = segData[0][1];
    var updatedAccIds=[];
    var isEnableDisable="true";
    //on section checkbox click
    if(isHeader){
      var img = (segSecData.imgSectionCheckbox.src === "checkboxnormal.png") ? "checkboxselected.png" : "checkboxnormal.png";
      segSecData.imgSectionCheckbox.src = img;
      isEnableDisable=(img === "checkboxnormal.png")?"false":"true";
      for(var i=0; i<rowsData.length; i++){
        rowsData[i].imgCheckbox.src =img;
        updatedAccIds.push(rowsData[i].lblAccountNumber);
      }
      if(img==="checkboxnormal.png")
        this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount=0;
      else
        this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount=rowsData.length;
    } 
    //on row checkbox click
    else{
      var selInd = this.view.ContractAccountsList.segAccountFeatures.selectedRowIndex[1];
      rowsData[selInd].imgCheckbox.src = (rowsData[selInd].imgCheckbox.src === "checkboxnormal.png") ? "checkboxselected.png" : "checkboxnormal.png";
      updatedAccIds.push(rowsData[selInd].lblAccountNumber);
      if(rowsData[selInd].imgCheckbox.src==="checkboxnormal.png"){
        this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount=this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount-1;
        isEnableDisable="false";
      }else{
        this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount=this.view.ContractAccountsList.segAccountFeatures.info.selectedAccountsCount+1;
        isEnableDisable="true";
      }
      segSecData.imgSectionCheckbox.src = this.getHeaderCheckboxImage(rowsData,true,true);
    }
    
    for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
        if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
          for(let k=0;k<this.createContractRequestParam.contractCustomers[j].accounts.length;k++){
            if(updatedAccIds.includes(this.createContractRequestParam.contractCustomers[j].accounts[k].accountNumber))
              this.createContractRequestParam.contractCustomers[j].accounts[k].isEnabled=isEnableDisable;
          }
          break;
        }
    }
    this.view.ContractAccountsList.segAccountFeatures.setData([[segSecData,rowsData]]);
    this.view.ContractAccountsList.lblCount.text= this.getSelectedItemsCount(rowsData, true);
    var isValid = this.validateCheckboxSelections(this.view.ContractAccountsList.segAccountFeatures,true);
    if(isValid){
      this.view.flxCustomerDropdown.setEnabled(true);
      this.enableAllTabs(true);
    }else{
      this.view.flxCustomerDropdown.setEnabled(false);
      this.enableAllTabs(false);
    }
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractAccounts.btnSave,true,isValid);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractAccounts.btnNext,false,isValid);
    this.view.forceLayout();
  },
    /*
  * check if all the rows of segment are selected or not
  * @param: rows array, is partial selection behaviour
  * @return: image to be set (checked/unchecked/partial)
  */
  getHeaderCheckboxImage : function(data,isRowData,hasPartialSelection){
    var img = this.AdminConsoleCommonUtils.checkboxnormal;
    var currImg = (isRowData === true) ? "imgCheckbox" :"imgSectionCheckbox";
    var selCount = 0, partialCount = 0,unselCount = 0;
    for(var i=0; i<data.length; i++){
      var list = (isRowData === true) ? data[i] : data[i][0];
        if(list[currImg].src === this.AdminConsoleCommonUtils.checkboxSelected || list[currImg].src === this.AdminConsoleCommonUtils.checkboxPartial){
          selCount  = selCount +1;
          partialCount = (list[currImg].src === this.AdminConsoleCommonUtils.checkboxPartial) ? partialCount +1 : partialCount;
        }
      }
    if(hasPartialSelection){
      if(selCount !== 0 && selCount === data.length)
        img = partialCount === 0 ? this.AdminConsoleCommonUtils.checkboxSelected: this.AdminConsoleCommonUtils.checkboxPartial;
      else if(selCount !== 0 && selCount < data.length)
        img = this.AdminConsoleCommonUtils.checkboxPartial;
    } else{
      if(selCount === data.length)
        img = this.AdminConsoleCommonUtils.checkboxSelected;
    }
    return img;
      
  },
  setCustomersDropDownList : function(componentName){
    this.view[componentName].btnPrimary.setVisibility(true);//to get its width while calculating its parent flex width
    this.view[componentName].flxNoResult.setVisibility(false);
    var selectedCustomers= this.selectedCustomers;
    var maxLengthText="";
    var data=[];
    var widgetMap={
      "flxCustomerName":"flxCustomerName",
      "flxCustomerNameCont":"flxCustomerNameCont",
      "lblCustomerName":"lblCustomerName",
      "btnPrimaryCustomers":"btnPrimaryCustomers"
    }
    var self=this;
    for(var x=0;x<selectedCustomers.length;x++){
      if(selectedCustomers[x].isSelected!==false){
        if((selectedCustomers[x].coreCustomerName+"("+selectedCustomers[x].coreCustomerId+")").length>maxLengthText.length)
          maxLengthText=selectedCustomers[x].coreCustomerName+"("+selectedCustomers[x].coreCustomerId+")";
        data.push({
          "flxCustomerName":{"onClick":function(){
            if(componentName==="customersDropdownLimits"&&self.view.flxClearContractLimitsSearch.isVisible){
              self.updateLimitValues();
            }
            self.setSelectedText(componentName);
            self.setCustSelectedData(componentName,false);
          }},
        "id":selectedCustomers[x].coreCustomerId,
        "lblCustomerName":{"text":selectedCustomers[x].coreCustomerName+"("+selectedCustomers[x].coreCustomerId+")"},
        "btnPrimaryCustomers":{"isVisible":selectedCustomers[x].isPrimary==="true"?true:false}
      });
      }
    }
    this.view[componentName].segList.widgetDataMap=widgetMap;
    this.view[componentName].segList.setData(data);
    this.view.forceLayout();//to get the primary button width, calling forceLayout
    this.view[componentName].segList.info={"records":data};
    this.view[componentName].lblSelectedValue.text=kony.i18n.getLocalizedString("i18n.contracts.selectACustomer");
    this.view[componentName].tbxSearchBox.text="";
    this.view[componentName].flxClearSearchImage.setVisibility(false);
    var maxTextWidth=this.AdminConsoleCommonUtils.getLabelWidth(maxLengthText,"13px Lato-Regular");
    var dropdownWidth=maxTextWidth+15+15+this.view[componentName].btnPrimary.frame.width+15;
    if(componentName==="customersDropdownLimits")
      this.view.flxCustomerDropdownLimits.width=dropdownWidth>270?dropdownWidth+"px":"270px";
    else if(componentName==="customersDropdown")
      this.view.flxCustomerDropdown.width=dropdownWidth>270?dropdownWidth+"px":"270px";
    else if(componentName==="customersDropdownFA")
      this.view.flxCustomerDropdownFA.width=dropdownWidth>270?dropdownWidth+"px":"270px";
    this.view[componentName].flxSegmentList.setVisibility(false);
    this.view[componentName].btnPrimary.setVisibility(false);
    this.view.forceLayout();
  },
  isValidSelection : function(){
    var isValid=true;
    if(this.view.flxContractDetailsTab.isVisible){
      isValid=this.validateContractDetails();
    }else if(this.view.flxContractAccounts.isVisible&&this.view.flxContractAccountsList.isVisible&&this.view.ContractAccountsList){
      if(this.view.ContractAccountsList.segAccountFeatures.data[0][0].imgSectionCheckbox.src==="checkboxnormal.png")
        isValid=false;
    }else if(this.view.flxContractLimits.isVisible&&this.view.flxContractLimitsSearch.isVisible){//if there are no limits search will be hidden
      isValid=this.validateAllLimits();
    }
    return isValid;  
  },
  searchCustomersDropDownList : function(componentName){
    var segData=this.view[componentName].segList.info.records;
    var searchText=this.view[componentName].tbxSearchBox.text;
    var custName="";
    var searchResults=segData.filter(function(rec){
      custName=rec.lblCustomerName.text.toLowerCase();
      if(custName.indexOf(searchText)>=0)
        return rec;
    });
    if(searchResults.length===0){
      this.view[componentName].flxNoResult.setVisibility(true);
      this.view[componentName].segList.setVisibility(false);
    }else if(this.view[componentName].flxNoResult.isVisible){
      this.view[componentName].flxNoResult.setVisibility(false);
      this.view[componentName].segList.setVisibility(true);
    }
    this.view[componentName].segList.setData(searchResults);
    this.view[componentName].lblSelectedValue.text=kony.i18n.getLocalizedString("i18n.contracts.selectACustomer");
    this.view[componentName].btnPrimary.setVisibility(false);
    this.view.forceLayout();
  },
    //searchExistingUserRelatedCustomer 
  searchExistingUserDropDownList : function(){
    var segData=this.view.segExistingUserListContainer.info.records;
    var searchText= this.view.searchExistingUserRelatedCustomer.tbxSearchBox.text;
    var custName="";
    var custId="";
    var searchResults=segData.filter(function(rec){
      custName=rec.lblExistingUserName.text.toLowerCase();
      custId=rec.lblExistingUserCustId.text.toLowerCase();
      if(custName.indexOf(searchText)>=0 || custId.indexOf(searchText)>=0)
        return rec;
    });    
    this.view.segExistingUserListContainer.setData(searchResults);
    if(searchResults.length===0){
      this.view.segExistingUserListContainer.setVisibility(false);
      this.view.flxNoResultsFoundExisting.setVisibility(true);
    }else if(this.view.flxNoResultsFoundExisting.isVisible){
      this.view.segExistingUserListContainer.setVisibility(true);
      this.view.flxNoResultsFoundExisting.setVisibility(false);
    }
    this.view.forceLayout();
  },

  setSelectedText : function(componentName,selectedId){
    var selIndex="";
    var segData=this.view[componentName].segList.data;
    if(selectedId){
      for(let x=0;x<segData.length;x++){
        if(segData[x].id===selectedId){
          selIndex=x;
          break;
        }
      }
    }else{
      selIndex=this.view[componentName].segList.selectedRowIndex[1];
    }
    var isPrimary=segData[selIndex].btnPrimaryCustomers.isVisible?true:false;
    this.view[componentName].lblSelectedValue.text=this.AdminConsoleCommonUtils.getTruncatedString(segData[selIndex].lblCustomerName.text,isPrimary?25:30,isPrimary?22:27);
    this.view[componentName].lblSelectedValue.tooltip=segData[selIndex].lblCustomerName.text;
    this.view[componentName].lblSelectedValue.info={"id":segData[selIndex].id};
    this.view[componentName].btnPrimary.setVisibility(isPrimary);
    this.view[componentName].flxSegmentList.setVisibility(false);
    this.view.forceLayout();
  },
   /*
  * show bulk update features/limits popup
  */
  showBulkUpdatePopup: function(){
    if(this.view.flxContractFA.isVisible){
      this.view.bulkUpdateFeaturesLimitsPopup.lblDetailsHeading.text=kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UpdatePermissionsInBulk");
      this.view.bulkUpdateFeaturesLimitsPopup.lblTitle.text=kony.i18n.getLocalizedString("i18n.contract.addPermission");
      this.view.bulkUpdateFeaturesLimitsPopup.bulkUpdateAddNewFeatureRow.lblFieldName13.text="Permission Type";
      this.view.bulkUpdateFeaturesLimitsPopup.bulkUpdateAddNewFeatureRow.flxRow2.isVisible=false;
      this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.text=kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.plusAddNewPermissions");
      this.view.bulkUpdateFeaturesLimitsPopup.lblRadioGroupTitle.text=kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.PermissionType")
      this.setRadioGroupData(1);
    }else{
      this.view.bulkUpdateFeaturesLimitsPopup.lblDetailsHeading.text=kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.UpdateLimitsInBulk");
      this.view.bulkUpdateFeaturesLimitsPopup.lblTitle.text=kony.i18n.getLocalizedString("i18n.contracts.addLimits");
      this.view.bulkUpdateFeaturesLimitsPopup.bulkUpdateAddNewFeatureRow.lblFieldName13.text=kony.i18n.getLocalizedString("i18n.konybb.Common.TransactionType");
      this.view.bulkUpdateFeaturesLimitsPopup.bulkUpdateAddNewFeatureRow.flxRow2.isVisible=true;
      this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.text=kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.plusAddNewLimits");
      this.view.bulkUpdateFeaturesLimitsPopup.lblRadioGroupTitle.text=kony.i18n.getLocalizedString("i18n.konybb.Common.TransactionType");
      this.setRadioGroupData(2);
    }
	this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.info={"added":[]};
    this.view.flxBulkUpdateFeaturesPopup.setVisibility(true);
    this.showBulkUpdatePopupScreen1(false);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen1.btnSave,true,false);
  },
  setRadioGroupData : function(opt){
    var radioBtnData=[];
    if(opt===1){
      radioBtnData = [{src:"radio_selected.png", value:"Enable", selectedImg:"radio_selected.png", unselectedImg:"radio_notselected.png"},
                        {src:"radio_notselected.png", value:"Disable", selectedImg:"radio_selected.png", unselectedImg:"radio_notselected.png"}];
      this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.flxRadioButton1.width="100px";
      this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.flxRadioButton2.width="100px";
    }else{
      radioBtnData = [{src:"radio_selected.png", value:"Per Transaction", selectedImg:"radio_selected.png", unselectedImg:"radio_notselected.png"},
                        {src:"radio_notselected.png", value:"Daily Transaction", selectedImg:"radio_selected.png", unselectedImg:"radio_notselected.png"},
                     {src:"radio_notselected.png", value:"Weekly Transaction", selectedImg:"radio_selected.png", unselectedImg:"radio_notselected.png"}];
      this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.flxRadioButton1.width="150px";
      this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.flxRadioButton2.width="150px";
      this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.flxRadioButton3.width="150px";
    }
    this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.setData(radioBtnData);
    this.view.forceLayout();
  },
  /*
  * show bulk update features/limits accounts selection screen
  */
  showBulkUpdatePopupScreen1 : function(isModify){
    if(!isModify)
      this.setBulkUpdateCustomersList();
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateScreen1.setVisibility(true);
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateScreen2.setVisibility(false);
    this.view.forceLayout();
  },
  /*
  * show bulk update features/limits screen in popup
  */
  showBulkUpdatePopupScreen2 : function(){
    this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.setVisibility(true);
    this.view.bulkUpdateFeaturesLimitsPopup.lblIconArrow.text="";
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateScreen1.setVisibility(false);
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateScreen2.setVisibility(true);
    this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.setVisibility(true);
    this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.setVisibility(true);
    this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.info={"added":[]};
    var flxChildren = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.removeAll();
    for(var i=0;i<flxChildren.length;i++){
      this.view.bulkUpdateFeaturesLimitsPopup.remove(this.view.bulkUpdateFeaturesLimitsPopup[flxChildren[i].id]);
    }
    this.addBulkUpdateTags();
    this.view.forceLayout();
    var height = this.view.bulkUpdateFeaturesLimitsPopup.flxContractDetailsPopupContainer.frame.height - (70 + this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateListContainer.frame.y + 80);
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateListContainer.height = height + "dp";
    this.bulkUpdateList=this.getFeaturesForBulkUpdate();
    this.bulkUpdateListboxData = this.getListForListBox(this.bulkUpdateAllFeaturesList);
    this.addNewFeatureRowBulkUpdate();
  },
  resetAddedRows : function(){
    var flxChildren = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.removeAll();
    for(var i=0;i<flxChildren.length;i++){
      this.view.bulkUpdateFeaturesLimitsPopup.remove(this.view.bulkUpdateFeaturesLimitsPopup[flxChildren[i].id]);
    }
    var height = this.view.bulkUpdateFeaturesLimitsPopup.flxContractDetailsPopupContainer.frame.height - (70 + this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateListContainer.frame.y + 80);
    this.view.bulkUpdateFeaturesLimitsPopup.flxBulkUpdateListContainer.height = height + "dp";
    this.bulkUpdateListboxData = this.getListForListBox(this.bulkUpdateAllFeaturesList);
    this.addNewFeatureRowBulkUpdate();
    this.view.forceLayout();
  },
    /*
  * change the fields based on features/limits bulk update
  * @param: opt - feature/limit, row widget path
  */
  setFeatureLimitRowUI : function(opt,widgetPath){
    if(opt === 1){  //for feature
      widgetPath.flxRow2.setVisibility(false);
      widgetPath.lblFieldName13.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.PermissionType");
      widgetPath.lstBoxFieldValue13.masterData = [["select","Select a Permission Type"],["ENABLE","Enable"],["DISABLE","Disable"]];
      widgetPath.lstBoxFieldValue13.selectedKey = "select";
    } else{ //for limits
      widgetPath.flxRow2.setVisibility(false);
      widgetPath.lblFieldName21.text = "Value";
      widgetPath.tbxValue21.text="";
      widgetPath.tbxValue21.onKeyUp = function(){
        widgetPath.flxErrorField21.setVisibility(false);
        widgetPath.flxValue21.skin="sknflxEnterValueNormal";
      };
      widgetPath.lstBoxFieldValue13.masterData = [["select","Select a Transaction Type"],["MAX_TRANSACTION_LIMIT","Per Transaction Limit"],
                                                  ["DAILY_LIMIT","Daily Limit"],
                                                  ["WEEKLY_LIMIT","Weekly Limit"]];
      widgetPath.lstBoxFieldValue13.selectedKey = "select";
    }
  },
  /*
  * add new entry for feature selection in bulk update popup
  */
  addNewFeatureRowBulkUpdate : function(){
    var flxChildren = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    //caluclate the value for id to suffix
    var num = flxChildren.length > 0 ? flxChildren[flxChildren.length-1].id.split("Z")[1] : "0";
    num = parseInt(num,10) +1;
    var id = num > 9 ? ""+num: "0"+num;
    var rowWidth = this.view.bulkUpdateFeaturesLimitsPopup.flxContractDetailsPopupContainer.frame.width - 40;
    var featureRowToAdd = new com.adminConsole.contracts.bulkUpdateAddNewFeatureRow({
        "id": "bulkFeatureRowZ" +id,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"20dp",
        "width":rowWidth + "dp",
        "top": "20dp"
      }, {}, {});
    if(this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.info==="Limits"){
      featureRowToAdd.flxFieldColumn21.isVisible=true;
      //featureRowToAdd.lblFieldName13.text="Transaction Type";
      this.setNewFeatureLimitRowData(featureRowToAdd,2);
    }else{
      featureRowToAdd.flxFieldColumn21.isVisible=false;
      //featureRowToAdd.lblFieldName13.text="Permission Type";
      this.setNewFeatureLimitRowData(featureRowToAdd,1);
    }
    
    //this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.addAt(featureRowToAdd, flxChildren.length);
    
  },
  /*
  * add new entry for limit selection in bulk update popup
  */
  addNewLimitRowBulkUpdate : function(){
    var flxChildren = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    //caluclate the value for id to suffix
    var num = flxChildren.length > 0 ? flxChildren[flxChildren.length-1].id.split("Z")[1] : "0";
    num = parseInt(num,10) +1;
    var id = num > 9 ? ""+num: "0"+num;
    var rowWidth = this.view.bulkUpdateFeaturesLimitsPopup.flxContractDetailsPopupContainer.frame.width - 40;
    var limitRowToAdd = new com.adminConsole.contracts.bulkUpdateAddNewFeatureRow({
        "id": "bulkLimitRowZ" +id,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"20dp",
        "width":rowWidth + "dp",
        "top": "20dp"
      }, {}, {});
    limitRowToAdd.flxRow2.isVisible = false;
    limitRowToAdd.flxFieldColumn21.isVisible=true;
    //this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.addAt(limitRowToAdd, flxChildren.length);
    this.setNewFeatureLimitRowData(limitRowToAdd,2);
  },
  /*
  * set data and assign actions to the newly created row
  * @param: new row widget path, option (features:1,limits:2)
  */
  setNewFeatureLimitRowData : function(newRowWidget, option){
    var self=this;
    var allRows = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    if(allRows.length === 0){
      newRowWidget.flxDelete.isVisible = false;
    } else{
      allRows[0].flxDelete.isVisible = true;
      newRowWidget.flxDelete.isVisible = true;
    }
    var listboxData = this.getUnselectedFeaturesList();
    newRowWidget.lstBoxFieldValue11.masterData = listboxData;
    newRowWidget.lstBoxFieldValue11.selectedKey = listboxData[0][0];
    newRowWidget.zindex=listboxData.length;
    newRowWidget.lstBoxFieldValue11.onSelection = function(){
      newRowWidget.lstBoxFieldValue11.skin="sknLbxborderd7d9e03pxradius";
      newRowWidget.flxErrorField11.setVisibility(false);
      self.updateExistingRowsListboxData(newRowWidget);
      //disable action when feature not selected
      if(newRowWidget.lstBoxFieldValue11.selectedKey === "select"){
        newRowWidget.flxFieldValueContainer12.setEnabled(false);
        newRowWidget.lblFieldValue12.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.SelectAnAction");
      } else{
        self.setActionsListboxData(newRowWidget);
      }
    }
    if(listboxData.length<3)//if there is only on feature to be added , that would be added in this widget row so no new row is needed
      this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.setVisibility(false);
    else
      this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.setVisibility(true);
    newRowWidget.lblFieldValue12.text="Select an action";
    newRowWidget.flxDelete.onClick = this.deleteAddNewFeatureRow.bind(this,newRowWidget);
    this.setFeatureLimitRowUI(option,newRowWidget);
    this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.addAt(newRowWidget, allRows.length);
    this.updateExistingRowsListboxData(newRowWidget);
    this.view.forceLayout();
  },
  setActionsListboxData : function(widgetPath){
    var self=this;
    var allBulkFeatures=this.bulkUpdateList;
    var selectedFeatureId=widgetPath.lstBoxFieldValue11.selectedKey;
    var featureActions=[{"id":"select","name":"Select"}];
    var actionListData=[["select","Select"]];
     var selectionProp = {
      imageIdentifier: "imgCheckBox",
      selectedStateImage: "checkboxselected.png",
      unselectedStateImage: "checkboxnormal.png"
    };
    if(selectedFeatureId!=="select"){
      if(this.view.flxContractFA.isVisible){
        featureActions=allBulkFeatures[selectedFeatureId].actions;
      }else{
        featureActions = allBulkFeatures[selectedFeatureId].actions.filter(function(item) {
          return item.type=="MONETARY";
        });
      }
      widgetPath.lblFieldValue12.text=featureActions.length+ " Selected";
      widgetPath.flxFieldValueContainer12.setEnabled(true);
      
      widgetPath.flxFieldValueContainer12.onClick = function(){
        widgetPath.flxFieldValueContainer12.skin="sknFlxbgFFFFFFbdrD7D9E0rd3px";
          widgetPath.flxErrorField12.setVisibility(false);
        if(widgetPath.flxDropdownField12.isVisible)
          widgetPath.flxDropdownField12.setVisibility(false);
        else{
          widgetPath.productTypeFilterMenu.tbxSearchBox.text="";
          var selInd=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices;
          widgetPath.productTypeFilterMenu.flxClearSearchImage.setVisibility(false);
          var totalRecords=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.data;
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(totalRecords);
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehaviorConfig = selectionProp;
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices=[[0,selInd]];
          widgetPath.productTypeFilterMenu.flxNoResultFound.setVisibility(false);
          widgetPath.flxDropdownField12.setVisibility(true);
          self.view.forceLayout();
        }
      }
    }
    var widgetMap = {
      "serviceType": "serviceType",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription",
      "dependentActions": "dependentActions"
    };
    var i=0;
    var maxText="";
    var actionsSegData=featureActions.map(function(action){
      self.bulkUpdateList[selectedFeatureId].actions[i].isChecked=true;
      maxText=action.actionName.length>maxText.length?action.actionName:maxText;
      i=i+1;
      return{
        "actionId": action.actionId,
        "flxSearchDropDown": "flxSearchDropDown",
        "imgCheckBox":{"src":"checkboxselected.png"},
        "lblDescription": action.actionName,
        "dependentActions":action.dependentActions
      }
    });
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.widgetDataMap=widgetMap;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(actionsSegData);
    var maxTextWidth=this.AdminConsoleCommonUtils.getLabelWidth(maxText,"13px Lato-Regular");
    var dropDownWidth=maxTextWidth+15+15+20;
    widgetPath.flxDropdownField12.width=dropDownWidth>widgetPath.flxFieldValueContainer12.frame.width?dropDownWidth+"px":widgetPath.flxFieldValueContainer12.frame.width+"px";
    //to select all the actions by default
    var selectInd=[];
    for(let x=0;x<actionsSegData.length;x++){
      selectInd.push(x);
    }
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectionBehaviorConfig = selectionProp;
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices = [[0,selectInd]];
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info={"data":actionsSegData,"selectedIndices":selectInd};
    
    widgetPath.productTypeFilterMenu.segStatusFilterDropdown.onRowClick = function(){
      self.bulkActionsRowClick(widgetPath);
    };
    
    widgetPath.productTypeFilterMenu.tbxSearchBox.onKeyUp = function(){
      if(widgetPath.productTypeFilterMenu.tbxSearchBox.text.trim().length>0){
        self.searchBulkActions(widgetPath);
      }else{
        var selInd=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices;
        widgetPath.productTypeFilterMenu.flxNoResultFound.setVisibility(false);
        widgetPath.productTypeFilterMenu.flxClearSearchImage.setVisibility(false);
        var totalRecords=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.data;
        widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(totalRecords);
        widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices=[[0,selInd]];
      }
      self.view.forceLayout();
    };
    
    widgetPath.productTypeFilterMenu.flxClearSearchImage.onClick = function(){
      widgetPath.productTypeFilterMenu.tbxSearchBox.text="";
      widgetPath.productTypeFilterMenu.flxClearSearchImage.setVisibility(false);
      widgetPath.productTypeFilterMenu.flxNoResultFound.setVisibility(false);
      var totalRecords=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.data;
      widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(totalRecords);
      var selInd=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices;
      widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices=[[0,selInd]];
      self.view.forceLayout();
    };
    
    widgetPath.flxDropdownField12.onHover = this.onHoverSettings;
    this.view.forceLayout();
  },
  // function to search actions in bulk update popup
  searchBulkActions : function(widgetPath){
    widgetPath.productTypeFilterMenu.flxClearSearchImage.setVisibility(true);
        var segData=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.data;
        var searchText=widgetPath.productTypeFilterMenu.tbxSearchBox.text;
        var actionName="";
        var selIndices=[];
        var selectedRowIndices=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices;
        var actualData=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.data;
        var selectedActionsIds=[];
        for(let z=0;z<selectedRowIndices.length;z++)
        	selectedActionsIds.push(actualData[z].actionId);
        var filteredData=segData.filter(function(rec){
          actionName=rec.lblDescription.toLowerCase();
          if(actionName.indexOf(searchText)>=0)
            return rec;
        });
        if(filteredData.length===0){
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setVisibility(false);
          widgetPath.productTypeFilterMenu.flxNoResultFound.setVisibility(true);
        }else{
          for(let x=0;x<filteredData.length;x++){
            if(selectedActionsIds.includes(filteredData[x].actionId))
              selIndices.push(x);
          }
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setData(filteredData);
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices=[[0,selIndices]];
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.setVisibility(true);
          widgetPath.productTypeFilterMenu.flxNoResultFound.setVisibility(false);
          widgetPath.forceLayout();
        }
    this.view.forceLayout();
  },
  //function to retain and set selected data on row click of actions segment in bulk update popup
  bulkActionsRowClick: function(widgetPath){
    var self=this;
    var segData=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.data;
      var selInd=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices?widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndices[0][1]:[];
      var selectedIndex=widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndex?widgetPath.productTypeFilterMenu.segStatusFilterDropdown.selectedRowIndex[1]:null;
      if(selInd.length===0&&widgetPath.productTypeFilterMenu.tbxSearchBox.text.length===0)
        widgetPath.lblFieldValue12.text="Select an action";
      else{
        if(widgetPath.productTypeFilterMenu.tbxSearchBox.text.length===0)
        widgetPath.lblFieldValue12.text=selInd.length==1?segData[selInd[0]].lblDescription:selInd.length+" Selected";
        if(widgetPath.flxErrorField12.isVisible){
          widgetPath.flxErrorField12.isVisible=false;
          widgetPath.flxFieldValueContainer12.skin="sknFlxbgFFFFFFbdrD7D9E0rd3px";
        }
      }
      if(widgetPath.productTypeFilterMenu.tbxSearchBox.text.length===0)
        widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices=selInd;
      else{//to update slected indices in info which would be used in retaining the selected indices in search
        var index = widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices.indexOf(selectedIndex);
        if (index > -1) 
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices.splice(index, 1);
        else
          widgetPath.productTypeFilterMenu.segStatusFilterDropdown.info.selectedIndices.push(selectedIndex);
      }
      var feature=self.bulkUpdateList[widgetPath.lstBoxFieldValue11.selectedKey];
      for(let i=0;i<feature.actions.length;i++){
        if(feature.actions[i].actionId===segData[selectedIndex].actionId)
          self.bulkUpdateList[widgetPath.lstBoxFieldValue11.selectedKey].actions[i].isChecked=(segData[selectedIndex].imgCheckBox.src==="checkboxnormal.png")?false:true;
      }      
      self.view.forceLayout();
  },
   /*
  * remove the new row added in bulk update for features
  * @param: widget to remove path
  */
  deleteAddNewFeatureRow : function(widgetPath){
    var delRowSelection = widgetPath.lstBoxFieldValue11.selectedKeyValue;
    this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.remove(this.view.bulkUpdateFeaturesLimitsPopup[widgetPath.id]);
    this.view.bulkUpdateFeaturesLimitsPopup.remove(this.view.bulkUpdateFeaturesLimitsPopup[widgetPath.id]);
    var addedRows = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    if(addedRows.length === 1){
      addedRows[0].flxDelete.isVisible = false;
    }
    this.updateExistingRowsListboxData({"id":""});
    this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.setVisibility(true);
  },
  /*
  * update previous rows listbox data on addition of new row
  * @param: current row path
  */
  updateExistingRowsListboxData : function(currRowPath){
    var currRowData;
    var allRows = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    var updatedList = this.getUnselectedFeaturesList();
    for(var i=0;i<allRows.length;i++){
      if(currRowPath.id !== allRows[i].id){
        currRowData = [];
        currRowData.push(allRows[i].lstBoxFieldValue11.selectedKeyValue);
        var lstMasterData = currRowData.concat(updatedList);
        allRows[i].lstBoxFieldValue11.masterData = lstMasterData;
      }
    }
    if(updatedList.length === 0){
      this.view.bulkUpdateFeaturesLimitsPopup.btnAddNewRow.setVisibility(false);
    }
  },
  /*
  * get unselected features list to set it in remaining rows
  * @returns: unselected features array formated for listbox masterdata
  */
  getUnselectedFeaturesList: function(option) {
    var assignedData = [],
        allFeaturesId = [],diffList = [],commonList = [];
    var rowsList = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    //get all assigned feature id's
    if(this.view.flxContractFA.isVisible){//features bulk update list
    for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select") assignedData.push(rowsList[i].lstBoxFieldValue11.selectedKey);
    }   
    }else{//limits bulk update list
      for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select"){
        if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems&&rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems.length===rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.data.length)
        	assignedData.push(rowsList[i].lstBoxFieldValue11.selectedKey);
      }
    }
    }
     //all exsisting feature id's
    allFeaturesId = this.bulkUpdateAllFeaturesList.map(function(rec) {
      return rec.featureId;
    });
    //differentiate common and diff id's
    for (var j = 0; j < allFeaturesId.length; j++) {
      if (assignedData.contains(allFeaturesId[j])) {
        commonList.push(allFeaturesId[j]);
      } else {
        diffList.push(allFeaturesId[j]);
      }
    }
    var finalList = this.getListForListBox(diffList);
    return finalList;
  },
  /* 
  * function to return the required features in listbox masterdata format
  * @param: unselected features id's list
  * @retrun: masterdata with given features id's
  */
  getListForListBox: function(data) {
    var self = this;
    var finalList = [["select","Select a Feature"]];
    if(this.view.flxContractFA.isVisible){
    for (var i = 0; i < self.bulkUpdateAllFeaturesList.length; i++) {
      if (data.contains(self.bulkUpdateAllFeaturesList[i].featureId)) {
        finalList.push([self.bulkUpdateAllFeaturesList[i].featureId,self.bulkUpdateAllFeaturesList[i].featureName]);
      }
    }
    }else{
      var limitFeatures=[];
      var limitActions=[];
      var dataToSet=[];
      for(var a=0;a<self.bulkUpdateAllFeaturesList.length;a++){
        if(self.bulkUpdateAllFeaturesList[a].type==="MONETARY"&&self.bulkUpdateAllFeaturesList[a].actions.length>0&&this.bulkUpdateList[self.bulkUpdateAllFeaturesList[a].featureId]){
          limitFeatures=JSON.parse(JSON.stringify(self.bulkUpdateAllFeaturesList[a]));
          limitActions = self.bulkUpdateAllFeaturesList[a].actions.filter(function(item) {
            return item.type=="MONETARY";
          });
          if(limitActions.length>0){
            limitFeatures.actions=limitActions;
            dataToSet.push(limitFeatures);
          }
        }
      }
      for (var i = 0; i < dataToSet.length; i++) {
      if (data.contains(dataToSet[i].featureId)) {
        finalList.push([dataToSet[i].featureId,dataToSet[i].featureName]);
      }
    }
    }
    return finalList;
  },
  /*
  * to set customers list data to segment
  */
  setBulkUpdateCustomersList : function(customers){
    var customers=this.selectedCustomers;
    var self=this;
    var rowsData=[];
    var dataMap={
      "flxsegCustomersList":"flxsegCustomersList",
      "flxCheckBox":"flxCheckBox",
      "imgCheckBox":"imgCheckBox",
      "lblCustomerId":"lblCustomerId",
      "lblCustomerName":"lblCustomerName",
      "lblSeparator":"lblSeparator",
      "custInfo":"custInfo"
    };
    var secData={
      "template":"flxsegCustomersList",
      "flxCheckBox":{"onClick":function(){self.toggleBulkCheckbox();}},
      "imgCheckBox":{"src":"checkboxnormal.png"},
      "lblCustomerId":{"text":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID")},
      "lblCustomerName":{"text":kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.CustomerName_UC")},
      "lblSeparator":{"skin":"sknLblSeparator696C73","isVisible":true}
    }
    for (var x = 0; x < customers.length; x++) {
      if (customers[x].isSelected === true) {
        rowsData.push({
          "template": "flxsegCustomersList",
          "flxCheckBox": { "onClick": function () { self.toggleCustomerCheckbox(); } },
          "imgCheckBox": { "src": "checkboxnormal.png" },
          "lblCustomerId": { "text": customers[x].coreCustomerId },
          "lblCustomerName": { "text": customers[x].coreCustomerName },
          "lblSeparator": { "skin": "sknLblSeparatore7e7e7", "isVisible": true },
          "custInfo": customers[x]
        });
      }
    }
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.widgetDataMap=dataMap;
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.setData([[secData,rowsData]]);
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info={"selectedCust":[]};
    this.view.forceLayout();
  },
  toggleBulkCheckbox : function(){
    var imgSrc="checkboxnormal.png";
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust=[];
    var segData=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.data;
    if(segData[0][0].imgCheckBox.src==="checkboxnormal.png"){
      segData[0][0].imgCheckBox.src="checkboxselected.png";
      imgSrc="checkboxselected.png";
    }else
      segData[0][0].imgCheckBox.src="checkboxnormal.png";
    for(var i=0;i<segData[0][1].length;i++){
      segData[0][1][i].imgCheckBox.src=imgSrc;
      if(imgSrc==="checkboxnormal.png")
        this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.push(segData[0][1][i].custInfo);
    }
    var isValid = segData[0][0].imgCheckBox.src==="checkboxnormal.png"?false:true;
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen1.btnSave,true,isValid);
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.setData(segData);
    this.view.forceLayout();
  },
  toggleCustomerCheckbox : function(){
    var selCount=0;
    var segData=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.data;
    var selIndex=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.selectedRowIndex[1];
    for(var i=0;i<segData[0][1].length;i++){
      if(segData[0][1][i].imgCheckBox.src==="checkboxselected.png")
        selCount=selCount+1;
    }
    if(segData[0][1][selIndex].imgCheckBox.src==="checkboxnormal.png"){
      segData[0][0].imgCheckBox.src=(selCount+1===segData[0][1].length)?"checkboxselected.png":"checkboxpartial.png";
      segData[0][1][selIndex].imgCheckBox.src="checkboxselected.png";
      this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.push(segData[0][1][selIndex].custInfo);
    }else{
      segData[0][0].imgCheckBox.src=(selCount===1)?"checkboxnormal.png":"checkboxpartial.png";
      segData[0][1][selIndex].imgCheckBox.src="checkboxnormal.png";
      for(let x=0;x<this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.length;x++){
        if(segData[0][1][selIndex].custInfo.coreCustomerId===this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust[x].coreCustomerId)
          this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.info.selectedCust.splice(x,1);
      }
    }
    this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.setData(segData);
    var isValid = segData[0][0].imgCheckBox.src==="checkboxnormal.png"?false:true;
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.bulkUpdateFeaturesLimitsPopup.commonButtonsScreen1.btnSave,true,isValid);
    this.view.forceLayout();
  },
  addBreadcrumb : function(customerInfo){
    var self=this;
    var i=this.view.flxSearchBreadcrumb.widgets();
    var tagsCount=i.length;    
    var id=customerInfo.coreCustomerId;
    var name=customerInfo.coreCustomerName.toUpperCase();
    if(this.view.flxSearchBreadcrumb.info.added.length===0){
      this.view.lblCurrentScreen.info=customerInfo;
      this.view.lblCurrentScreen.text=name;
      this.view.flxSearchBreadcrumb.info.added.push([id,customerInfo.coreCustomerName]);
    }else{
      var newButton = this.view.btnBackToMain.clone(tagsCount.toString());
      var newArrow=this.view.fontIconBreadcrumbsRight.clone(tagsCount.toString());
      var flexWidth= this.AdminConsoleCommonUtils.getLabelWidth(name,"12px Lato-Regular");
      newButton.text = JSON.parse(JSON.stringify(this.view.lblCurrentScreen.text));
      newArrow.text="";
      newButton.isVisible = true;
      newArrow.isVisible=true;
      newButton.info=JSON.parse(JSON.stringify(this.view.lblCurrentScreen.info));
      newButton.onClick = function(){
        self.onBreadcrumbClick(newButton.info.coreCustomerId);
      }
      this.view.flxSearchBreadcrumb.info.added.push([this.view.lblCurrentScreen.info,this.view.lblCurrentScreen.text]);
      this.view.lblCurrentScreen.info=customerInfo;
      this.view.lblCurrentScreen.text=name;
      var width=0;
      var parentWidth=this.view.flxPopupHeader.frame.width/2;
      if(this.view.segBreadcrumbs.data.length===0){
        for(var a=tagsCount-1;a>=0;a--){
          width=width+(i[a].frame.width);
          if(i.indexOf("fontIconBreadcrumbsRight")){
            width=width+12;
          }
        }
        if(flexWidth+width>parentWidth){
          if(this.view["3btnBackToMain"]){
            this.setBreadcrumbSegData(JSON.parse(JSON.stringify(this.view["3btnBackToMain"].info)));
          }
          for(var x=5;x<tagsCount;x+2){//as the buttons are created as 3btnBackToMain,3fontIconBreadcrumbsRight,5btnBackToMainbtn...
            this.view[x+"btnBackToMain"].text=this.view[x+1+"btnBackToMain"]?this.view[x+1+"btnBackToMain"].text:newButton.text;
            this.view[x+"btnBackToMain"].info=this.view[x+1+"btnBackToMain"]?this.view[x+1+"btnBackToMain"].info:newButton.info;
          }
          //if only once contract is selected and the width exceeds half width
          if(this.view["3btnBackToMain"]){
            this.view["3btnBackToMain"].text="...";
            this.view["3btnBackToMain"].onClick =function(){
              self.view.flxSegMore.left=self.view["3btnBackToMain"].frame.x-120+"px";
              self.view.flxSegMore.setVisibility(!self.view.flxSegMore.isVisible);
            }
          }
            this.view.flxSearchBreadcrumb.addAt(newButton,tagsCount-1);
            this.view.flxSearchBreadcrumb.addAt(newArrow,tagsCount);
        }else{
          this.view.flxSearchBreadcrumb.addAt(newButton,tagsCount-1);
          this.view.flxSearchBreadcrumb.addAt(newArrow,tagsCount);
        }
      }else{
        var custId=JSON.parse(JSON.stringify(self.view["5btnBackToMain"].info.coreCustomerId));
        var rowData={
          "flxMore":{"onClick":function(){
            self.onBreadcrumbClick(custId);
            self.view.flxSegMore.setVisibility(false);}},
          "lblName":{"text":self.view["5btnBackToMain"].info.coreCustomerName,"skin":"sknLblLatoReg117eb013px","info":self.view["5btnBackToMain"].info},
          "template":"flxMore"
        }
        this.view.segBreadcrumbs.addDataAt(rowData,0);
        for(var x=5;x<tagsCount;x=x+2){//as the buttons are created as 3btnBackToMain,3fontIconBreadcrumbsRight,5btnBackToMainbtn...
            this.view[x+"btnBackToMain"].text=this.view[x+1+"btnBackToMain"]?this.view[x+1+"btnBackToMain"].text:newButton.text;
            this.view[x+"btnBackToMain"].info=this.view[x+1+"btnBackToMain"]?this.view[x+1+"btnBackToMain"].info:newButton.info;
          }
      }
    }
    this.view.forceLayout();
  },
  setBreadcrumbSegData: function(customerInfo){
    var self=this;
    var dataMap={
      "flxMore":"flxMore",
      "lblName":"lblName"
    };
    var segData=[{
        "flxMore":{"onClick":function(){
          self.onBreadcrumbClick(customerInfo.coreCustomerId);
          self.view.flxSegMore.setVisibility(false);
          }},
        "lblName":{"text":customerInfo.coreCustomerName,"skin":"sknLblLatoReg117eb013px","info":customerInfo},
        "template":"flxMore"
      }]
    this.view.segBreadcrumbs.widgetDataMap=dataMap;
    this.view.segBreadcrumbs.setData(segData);
    this.view.forceLayout();
  },
  getCustomersDataMap : function(){
    return{
      "flxRelatedCustomerList":"flxRelatedCustomerList",
      "flxRelatedCustomerRow":"flxRelatedCustomerRow",
      "flxLeftDetailsCont":"flxLeftDetailsCont",
      "flxCheckbox":"flxCheckbox",
      "imgCheckbox":"imgCheckbox",
      "flxContractDetails":"flxContractDetails",
      "flxContractName":"flxContractName",
      "lblContractName":"lblContractName",
      "lblContractId":"lblContractId",
      "flxDetails":"flxDetails",
      "flxColumn1":"flxColumn1",
      "lblHeading1":"lblHeading1",
      "lblData1":"lblData1",
      "flxColumn2":"flxColumn2",
      "lblHeading2":"lblHeading2",
      "lblData2":"lblData2",
      "flxColumn3":"flxColumn3",
      "lblHeading3":"lblHeading3",
      "lblData3":"lblData3",
      "flxRightDetailsCont":"flxRightDetailsCont",
      "flxContractRelation":"flxContractRelation",
      "btnRelation":"btnRelation",
      "lblContractInfo":"lblContractInfo",
      "flxRightActionCont":"flxRightActionCont",
      "flxRemoveContract":"flxRemoveContract",
      "lblIconRemove":"lblIconRemove",
      "lblLine":"lblLine",
      "lblPrimaryText":"lblPrimaryText",
      "flxPrimaryBtn":"flxPrimaryBtn",
      "imgRadioButton":"imgRadioButton",
      "customerInfo":"customerInfo"
    };
  },
  setCustomerSearchData : function(customers){
    var dataMap=this.getCustomersDataMap();
    var self=this;
    this.view.lblRelatedcustSubHeading.setVisibility(true);
    this.view.lblRelatedcustSubHeading.text=kony.i18n.getLocalizedString("i18n.contracts.searchResults")+" ("+customers.length+")";
    var details={};
    var address="";
    var data=customers.map(function(customer){
      if(customer.city!==undefined||customer.country!==undefined)
        address=self.AdminConsoleCommonUtils.getAddressText(customer.city,customer.country);
      else
        address="N/A";
      details ={
        "id": customer.coreCustomerId,
        "name": customer.coreCustomerName,
        "industry":customer.industry?customer.industry:"N/A",
        "email": customer.email,
        "phone":customer.phone,
        "address": address
      };
      return{
        "template":"flxRelatedCustomerList",
        "flxCheckbox":{"isVisible":false},
        "imgCheckbox":{"src":"checkboxnormal.png"},
        "flxRelatedCustomerRow":{"skin":customer.isAssociated==="true"?"sknFlxbgF3F3F3bdrd7d9e0":"sknFlxbgFFFFFFbdrD7D9E0rd4px",
                                 "hoverSkin":customer.isAssociated==="true"?"sknFlxbgF3F3F3bdrd7d9e0":"sknFlxbgFFFFFFbdrd7d9e0Hover",
                                 "onClick":customer.isAssociated==="true"?function(){}:function(){
                                   self.addCustomer(customer);
                                 }},
        "lblContractName":{"onTouchStart":self.showCustomerDetailsPopup.bind(self,details,true),"text":customer.coreCustomerName},
        "lblContractId":customer.coreCustomerId,
        "lblHeading1":{"text":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID")},
        "lblData1":{"text":customer.coreCustomerId},
        "flxColumn2":{"left":"36%"},
        "lblHeading2":{"text":kony.i18n.getLocalizedString("i18n.View.ADDRESS")},
        "lblData2":{"text":address},
        "flxColumn3":{"isVisible":false},
        "flxRightDetailsCont":{"isVisible":customer.isAssociated==="true"?true:false},
        "lblContractInfo":{"text":"Part of another contract","isVisible":customer.isAssociated==="true"?true:false},
        "btnRelation":{"isVisible":false},
        "flxRightActionCont":{"isVisible":false},
        "flxPrimaryBtn":{"isVisible":false},
        "customerInfo":customer
      }
    });
    this.view.segRelatedCustomers.widgetDataMap=dataMap;
    this.view.segRelatedCustomers.setData(data);
    this.view.flxRelatedCustomerSegment.setVisibility(true);
    this.view.forceLayout();
  },
  setSelectedCustomerData : function(relatedCustomers,coreCustomerId){
    var selectedCustIndex=this.view.segRelatedCustomers.selectedRowIndex[1];
    var customerInfo;
    var address="N/A";
    if(this.view.flxCustomerSearchPopUp.info.action!=="BREADCRUMBCLICK"){
      customerInfo=this.view.segRelatedCustomers.data[selectedCustIndex].customerInfo;
      var isAdded=false;
      for(var i=0;i<this.selectedCustomers.length;i++){
        if(this.selectedCustomers[i].coreCustomerId===customerInfo.coreCustomerId){
          isAdded=true;
          if(this.selectedCustomers[i].isSelected===false)
            this.selectedCustomers[i].isSelected=true;
        }
      }
      if(isAdded===false){
        customerInfo.isSelected=true;
        this.selectedCustomers.push(customerInfo);
      }
    }else{
      for(var i=0;i<this.selectedCustomers.length;i++){
        if(coreCustomerId===this.selectedCustomers[i].coreCustomerId){
          customerInfo=this.selectedCustomers[i];
        }
      }
    }
    if(customerInfo.city!==undefined||customerInfo.country!==undefined)
      address=this.AdminConsoleCommonUtils.getAddressText(customerInfo.city,customerInfo.country);
    else
      address="N/A";
    this.view.imgCheckbox.src=customerInfo.isSelected!==false?"checkboxselected.png":"checkboxnormal.png";
    this.view.lblContractName.text=customerInfo.coreCustomerName;
    this.view.lblContractId.text=customerInfo.coreCustomerId;
    this.view.lblDataInfo11.text=customerInfo.coreCustomerId;
    this.view.lblDataInfo12.text=address.trim().length==0?"N/A":address;
    this.view.lblDataInfo13.text=customerInfo.industry?customerInfo.industry:"N/A";
    this.view.lblDataInfo21.text=customerInfo.email?customerInfo.email:"N/A";
    this.view.lblDataInfo22.text=customerInfo.phone?customerInfo.phone:"N/A";
    if(relatedCustomers.length>0){
      this.view.lblRelatedcustSubHeading.text=kony.i18n.getLocalizedString("i18n.contracts.relatedCustomers")+" ("+relatedCustomers.length+")";
      var self=this;
      var isAdded=false;
      var details={};
      var addressText="";
      var relatedCustData=relatedCustomers.map(function(customer){
        isAdded=false;
        for(var i=0;i<self.selectedCustomers.length;i++){
          if(self.selectedCustomers[i].coreCustomerId===customer.coreCustomerId&&self.selectedCustomers[i].isSelected)
            isAdded=true;
        }
        if(customer.city!==undefined||customer.country!==undefined)
          addressText=self.AdminConsoleCommonUtils.getAddressText(customer.city,customer.country);
        else
          addressText="N/A";
        details ={
          "id": customer.coreCustomerId,
          "name": customer.coreCustomerName,
        "industry":customer.industry?customer.industry:"N/A",
        "email": customer.email,
        "phone":customer.phone,
        "address": addressText
      };
        return{
          "template":"flxRelatedCustomerList",
          "flxCheckbox":{"isVisible":true,"onClick":(customer.isAssociated==="true")?function(){}:function(context){self.relatedCustomersCheckboxClick(context,customer)}},
          "imgCheckbox":{"src":isAdded?"checkboxselected.png":"checkboxnormal.png"},
          "flxRelatedCustomerRow":{"skin":customer.isAssociated==="true"?"sknFlxbgF3F3F3bdrd7d9e0":"sknFlxbgFFFFFFbdrD7D9E0rd4px",
                                   "hoverSkin":customer.isAssociated==="true"?"sknFlxbgF3F3F3bdrd7d9e0":"sknFlxbgFFFFFFbdrd7d9e0Hover",
                                   "onClick":(customer.isAssociated==="true")?function(){}:function(){
                                     var isBreadcrumb=false;
                                     for(let x=0;x<self.view.flxSearchBreadcrumb.info.added.length;x++){
                                       if(self.view.flxSearchBreadcrumb.info.added[x][0]===customer.coreCustomerId){
                                         isBreadcrumb=true;
                                       }
                                     }
                                     if(isBreadcrumb===true)
                                       self.onBreadcrumbClick(customer.coreCustomerId);           
                                     else
                                       self.addCustomer(customer);
                                   }
                                  },
          "lblContractName":{"onTouchStart":self.showCustomerDetailsPopup.bind(self,details,true),"text":customer.coreCustomerName},
          "lblContractId":customer.coreCustomerId,
          "lblHeading1":{"text":"CUSTOMER ID"},
          "lblData1":{"text":customer.coreCustomerId},
          "flxColumn2":{"left":"37%"},
          "lblHeading2":{"text":"ADDRESS"},
          "lblData2":{"text":addressText},
          "flxColumn3":{"isVisible":false},
          "flxContractRelation":{"isVisible":false},
          "flxRightDetailsCont":{"isVisible":true},
          "btnRelation":{"isVisible":true,"text":customer.relationshipName},
          "lblContractInfo":{"text":kony.i18n.getLocalizedString("i18n.contracts.partOfAnotherContract"),"isVisible":customer.isAssociated==="true"?true:false},
          "flxRightActionCont":{"isVisible":true},
          "customerInfo":customer
        }
      });
      this.view.segRelatedCustomers.setData(relatedCustData);
      this.view.flxNoCustomersSearched.setVisibility(false);
      this.view.lblRelatedcustSubHeading.setVisibility(true);
      this.view.flxRelatedCustomerSegment.setVisibility(true);
    }else{
      this.view.lblNoCustomersSearched.text=kony.i18n.getLocalizedString("i18n.contracts.noRelatedCustomers");
      this.view.flxNoCustomersSearched.setVisibility(true);
      this.view.lblRelatedcustSubHeading.setVisibility(true);
      this.view.lblRelatedcustSubHeading.text=kony.i18n.getLocalizedString("i18n.contracts.relatedCustomers")+" (0)";
      this.view.flxRelatedCustomerSegment.setVisibility(false);
    }
    this.view.flxSelectedCustomerInfo.setVisibility(true);
    this.view.flxLoading2.setVisibility(false);
    this.view.forceLayout();
  },
  relatedCustomersCheckboxClick : function(context){
    var selIndex=context.rowContext.rowIndex;
    var segData=this.view.segRelatedCustomers.data;
    var customer=segData[selIndex].customerInfo;
    var tags=this.view.flxSearchFilter.widgets();
    var isAdded=false;
    for(var x=0;x<this.selectedCustomers.length;x++){
        if(this.selectedCustomers[x].coreCustomerId===customer.coreCustomerId){
          isAdded=true;
          if(this.selectedCustomers[x].isSelected===false)
            this.selectedCustomers[x].isSelected=true;
          break;
        }
      }
    if(segData[selIndex].imgCheckbox.src==="checkboxnormal.png"){
      if(isAdded){
        this.selectedCustomers[x].isSelected=true;
      }else{
        customer.isSelected=true;
        this.selectedCustomers.push(customer);
      }
      this.addCustomerTag(this.view.flxSearchFilter,this.selectedCustomers[x].coreCustomerName,this.selectedCustomers[x].coreCustomerId);
      segData[selIndex].imgCheckbox.src="checkboxselected.png";
    }else{
      segData[selIndex].imgCheckbox.src="checkboxnormal.png";
      this.selectedCustomers[x].isSelected=false;
      this.removeTag(this.view.flxSearchFilter,this.selectedCustomers[x].coreCustomerId);
        if(tags.length===1){
          this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnSave,true,false);
          this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtons.btnNext,false,false);
        }
    }
    this.view.segRelatedCustomers.setData(segData);
    this.view.forceLayout();
  },
  setSelectedCustomersData: function(){
    var customersData=this.selectedCustomers;
    var dataMap=this.getCustomersDataMap();
    var self=this;
    var details={};
    var relatedCustData=[];
    var primaryIndex="";
    var address="";
    for(var x=0;x<customersData.length;x++){
      if(customersData[x].isSelected!==false){
        if(customersData[x].city||customersData[x].country)
        address=this.AdminConsoleCommonUtils.getAddressText(customersData[x].city,customersData[x].country);
        else
          address="N/A";
        details ={
        "id": customersData[x].coreCustomerId,
        "name": customersData[x].coreCustomerName,
        "industry":customersData[x].industry?customersData[x].industry:"N/A",
        "email": customersData[x].email,
        "phone":customersData[x].phone,
        "address": address
      };
        if(this.action===this.actionConfig.edit&&customersData[x].isPrimary==="true")
          primaryIndex=x;
      relatedCustData.push({
        "template":"flxRelatedCustomerList",
        "lblContractName":{"onTouchStart":self.showCustomerDetailsPopup.bind(self,details,false),"text":customersData[x].coreCustomerName},
        "lblContractId":customersData[x].coreCustomerId,
        "lblHeading1":{"text":kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID")},
        "lblData1":{"text":customersData[x].coreCustomerId},
        "flxColumn2":{"left":"37%"},
        "lblHeading2":{"text":kony.i18n.getLocalizedString("i18n.View.ADDRESS")},
        "lblData2":{"text":address},
        "flxColumn3":{"isVisible":false},
        "flxContractRelation":{"isVisible":false},
        "flxRightActionCont":{"isVisible":true},
        "flxRemoveContract":{"enable":true,"isVisible":true,"onClick":function(context){self.deleteCoreCustomer(context)},"onHover":function(){}},
        "lblIconRemove":{"text":"","skin":"sknIcon00000015px"},
        "lblLine":{"isVisible":true},
        "lblPrimaryText":{"text":kony.i18n.getLocalizedString("i18n.userwidgetmodel.lblprimary1")},
        "flxPrimaryBtn":{"onClick":function(context){self.setCustomerAsPrimary(context)}},
        "imgRadioButton":{"src":"radio_notselected.png"},
        "customerInfo":customersData[x]
      });
      };
    }
    this.view.segAddedCustomers.widgetDataMap=dataMap;
    if(relatedCustData.length===1){
      relatedCustData[0].flxRemoveContract.isVisible=false;
      relatedCustData[0].lblLine.isVisible=false;
    }
    this.view.segAddedCustomers.setData(relatedCustData);
    this.view.lblContractCustomersHeader.text=kony.i18n.getLocalizedString("i18n.contracts.summarySelectedCustomers")+" ("+relatedCustData.length+")";
    if(this.action===this.actionConfig.edit)
      relatedCustData[0].flxPrimaryBtn.onClick({rowContext:{rowIndex:primaryIndex}});
    else
      relatedCustData[0].flxPrimaryBtn.onClick({rowContext:{rowIndex:0}});
    this.view.forceLayout();
  },
  deleteCoreCustomer : function(context){
    var selIndex=context.rowContext.rowIndex;
    var segData=this.view.segAddedCustomers.data;
    var customer=segData[selIndex].customerInfo;
    var isDeleted=false;
    var self=this;
    for(let x=0;x<this.selectedCustomers.length;x++){
      if(this.selectedCustomers[x].coreCustomerId===customer.coreCustomerId){
          this.selectedCustomers.splice(x,1);
          this.view.segAddedCustomers.removeAt(selIndex,0);
          isDeleted=true;
          this.view.lblContractCustomersHeader.text=kony.i18n.getLocalizedString("i18n.contracts.summarySelectedCustomers")+" ("+this.view.segAddedCustomers.data.length+")";
          break;
      }
    }
    for(let y=0;y<this.createContractRequestParam.contractCustomers.length;y++){
      if(this.createContractRequestParam.contractCustomers[y].coreCustomerId===customer.coreCustomerId){
        this.createContractRequestParam.contractCustomers.splice(y,1);
        break;
      }
    }
    if(isDeleted){
      for(let x=0;x<this.view.segAddedCustomers.data.length;x++){
        if(this.view.segAddedCustomers.data[x].imgRadioButton.src==="radio_selected.png"){
          this.view.segAddedCustomers.data[x].flxPrimaryBtn.onClick({rowContext:{rowIndex:x}});
          break;
        }
      }
    }
    
    if(this.view.segAddedCustomers.data.length===1){
      segData=this.view.segAddedCustomers.data[0];
      segData.flxRemoveContract.isVisible=false;
      segData.lblLine.isVisible=false;
      this.view.segAddedCustomers.setDataAt(segData,0);
    }
    this.view.forceLayout();
  },
  showPrimaryDeleteHover : function(widget,context){
    var scopeObj = this;
    var widGetId = widget.id;
    var topVal=0;
    if (widget) {
      if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
          topVal=context.screenY-125;
        scopeObj.view.ToolTipDelete.top=topVal+"px";
        scopeObj.view.ToolTipDelete.setVisibility(true)
      }else if (context.eventType === constants.ONHOVER_MOUSE_MOVE) {
        scopeObj.view.ToolTipDelete.setVisibility(true);
      } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
        scopeObj.view.ToolTipDelete.setVisibility(false);
      }
    }
    scopeObj.view.forceLayout();
  },
  setCustomerAsPrimary : function(contextObj){
    var self=this;
    var segData=this.view.segAddedCustomers.data;
    for(var x=0;x<this.selectedCustomers.length;x++)
      this.selectedCustomers[x].isPrimary="false";
    this.selectedCustomers[contextObj.rowContext.rowIndex].isPrimary="true";
    for(let i=0;i<this.createContractRequestParam.contractCustomers.length;i++){
      if(this.createContractRequestParam.contractCustomers[i].coreCustomerId===this.selectedCustomers[contextObj.rowContext.rowIndex].coreCustomerId)
        this.createContractRequestParam.contractCustomers[i].isPrimary="true";
      else
        this.createContractRequestParam.contractCustomers[i].isPrimary="false";
    }
    for(let a=0;a<segData.length;a++){
      segData[a].imgRadioButton.src="radio_notselected.png";
      segData[a].flxRemoveContract.onClick=function(context){self.deleteCoreCustomer(context)};
      segData[a].flxRemoveContract.onHover=function(){};
      segData[a].lblIconRemove.skin="sknIcon00000018px";
    }
    segData[contextObj.rowContext.rowIndex].imgRadioButton.src="radio_selected.png";
    segData[contextObj.rowContext.rowIndex].flxRemoveContract.onClick=function(){};
    segData[contextObj.rowContext.rowIndex].lblIconRemove.skin="sknIconb2b2b218px";
    segData[contextObj.rowContext.rowIndex].flxRemoveContract.onHover=function(widget,context){
      context.rowIndex=contextObj.rowContext.rowIndex;
      self.showPrimaryDeleteHover(widget,context);
    };
    this.primaryContractCustomer=segData[contextObj.rowContext.rowIndex].customerInfo;
    this.view.segAddedCustomers.setData(segData);
    this.setCustomersDropDownList("customersDropdown");
    this.setCustomersDropDownList("customersDropdownFA");
    this.setCustomersDropDownList("customersDropdownLimits");
    if(this.action===this.actionConfig.create&&this.createContractRequestParam.contractCustomers.length===0){
      var coreCustomerIdList=[];
      for(var x=0;x<this.selectedCustomers.length;x++){
        if(this.selectedCustomers[x].isSelected!==false){
          coreCustomerIdList.push(this.selectedCustomers[x].coreCustomerId);
          this.createContractRequestParam.contractCustomers.push({
            "isPrimary": this.selectedCustomers[x].isPrimary?this.selectedCustomers[x].isPrimary:"false",
            "coreCustomerId": this.selectedCustomers[x].coreCustomerId,
            "coreCustomerName": this.selectedCustomers[x].coreCustomerName,
            "isBusiness": this.selectedCustomers[x].isBusiness,
            "accounts":[],
            "features":[],
          });
        }
      }
    this.presenter.getCoreCustomerAccounts({"coreCustomerIdList":coreCustomerIdList});
      var serviceDefId=this.view[this.selectedServiceCard].lblCategoryName.info.catId;
      this.presenter.getServiceDefinitionFeaturesAndLimits({"serviceDefinitionId":serviceDefId});
    }else if(this.action===this.actionConfig.edit){
      if(this.createContractRequestParam.contractCustomers.length===0){
        var coreCustomerIdList=[];
        for(var x=0;x<this.selectedCustomers.length;x++){
          if(this.selectedCustomers[x].isSelected!==false){
            coreCustomerIdList.push(this.selectedCustomers[x].coreCustomerId);
            this.createContractRequestParam.contractCustomers.push({
              "isPrimary": this.selectedCustomers[x].isPrimary,
              "coreCustomerId": this.selectedCustomers[x].coreCustomerId,
              "coreCustomerName": this.selectedCustomers[x].coreCustomerName,
              "accounts":[],
              "features":[],
            });
          }
        }
        this.presenter.getCoreCustomerAccounts({"coreCustomerIdList":coreCustomerIdList});
      }
      if(this.completeCompanyDetails.accountsFeatures===null)
        this.presenter.getContractFeatureActionLimits(this.completeContractDetails.id,"edit");
      else
        this.setEditContractFeaturesLimits();
    }
    this.setContractDetails();
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsCustomers.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsCustomers.btnNext,false,true);
    this.enableAllTabs(true);
    this.view.forceLayout();
  },
  enableAllTabs : function(isEnable){
    for (let x=0;x<6;x++){
      this.view.verticalTabsContract["flxOption"+x].setEnabled(isEnable);
    }
    this.view.forceLayout();
  },
  addBulkUpdateTags : function(){
    var selectedCustData=this.view.bulkUpdateFeaturesLimitsPopup.segSelectionList.data;
    this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.removeAll();
    var customers=[];
    for (var i=0;i<selectedCustData[0][1].length;i++){
      if(selectedCustData[0][1][i].imgCheckBox.src==="checkboxselected.png")
      	this.addCustomerTag(this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer,selectedCustData[0][1][i].lblCustomerName.text,selectedCustData[0][1][i].lblCustomerId.text);
    }
    this.view.forceLayout();
  },
  //to set error skin for all the search fields
    setErrorSkinToAllFeilds: function(){
    this.view.txtSearchParam1.skin = "skntbxBordereb30173px";
    this.view.txtSearchParam2.skin = "skntbxBordereb30173px";
    this.view.txtSearchParam3.skin = "skntbxBordereb30173px";
    this.view.txtSearchParam4.skin = "skntbxBordereb30173px";
    this.view.txtSearchParam5.skin = "skntbxBordereb30173px";
    this.view.txtSearchParam6.skin = "skntbxBordereb30173px";
    this.view.flxDropDownServType.skin = "sknflxffffffoptemplateop3pxBordee32416";
    this.view.textBoxSearchContactNumber.txtISDCode.skin = "skntbxBordereb30173px";
    this.view.textBoxSearchContactNumber.txtContactNumber.skin = "skntbxBordereb30173px";
    this.view.lstbxSearchServiceDef.skin = "sknlbxError";
  },
  setNormalSkinToAllFeilds: function(){
    this.view.txtSearchParam1.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.txtSearchParam2.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.txtSearchParam3.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.txtSearchParam4.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.txtSearchParam5.skin = "sknTbx485c75Reg13pxBRe1e5edR1pxTBRSide";
    this.view.txtSearchParam6.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.flxDropDownServType.skin = "sknflxffffffoptemplateop3px";
    this.view.textBoxSearchContactNumber.txtISDCode.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.textBoxSearchContactNumber.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.lstbxSearchServiceDef.skin = "sknLbxborderd7d9e03pxradius";
  },
  //to validate whether atleast one search parameter is entered or not
  validateSearchFields: function () {
    if (this.view.txtSearchParam1.text.trim() !== "") return true;
    if (this.view.txtSearchParam3.text.trim() !== "") return true;
    if (this.view.txtSearchParam2.text.trim() !== "") return true;
    if (this.view.txtSearchParam6.text.trim() !== "") return true;
    if (this.view.textBoxSearchContactNumber.txtContactNumber.text.trim() !== ""&&this.view.textBoxSearchContactNumber.txtISDCode.text.trim()!=="") return true;
    if (this.view.txtSearchParam4.text.trim() !== "") return true;
    if (this.view.txtSearchParam5.text.trim() !== "") return true;
    
    var selectedInd=this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex?this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex[1]:null;
    var status="";
    if(selectedInd!==undefined&&selectedInd!==null)
      status=this.view.AdvancedSearchDropDownServType.sgmentData.data[selectedInd].customerStatusId;
      
    if (status !== "") return true;
    return false;
  },
    clearSearchFeilds: function(){
    this.view.txtSearchParam1.text = "";
    this.view.txtSearchParam3.info = null;
    this.view.txtSearchParam2.text = "";
    this.view.txtSearchParam3.text = "";
    this.view.txtSearchParam4.text = "";
    this.view.txtSearchParam5.text = "";
    this.view.txtSearchParam6.text = "";
    this.view.textBoxSearchContactNumber.txtISDCode.text="";
    this.view.textBoxSearchContactNumber.txtContactNumber.text = "";
    // this.view.lstbxSearchServiceDef.selectedKey = "SELECT";
    this.view.AdvancedSearchDropDownServType.sgmentData.setData([]); 
    this.view.flxDropDownDetailServType.setVisibility(false);
    this.view.lblSelectedRowsServType.text="Select";
    if(this.view.AdvancedSearchDropDownServType.sgmentData.info && this.view.AdvancedSearchDropDownServType.sgmentData.info.data){
      let segData = this.view.AdvancedSearchDropDownServType.sgmentData.info.data;
      this.view.AdvancedSearchDropDownServType.sgmentData.setData(segData); 
      this.view.AdvancedSearchDropDownServType.sgmentData.selectedRowIndex = null;
    }
  },
  searchForContracts: function(){
    if(this.validateSearchFields()){
      this.setContractSearchResults();
    }else{
      this.view.lblSearchError.setVisibility(true);
      this.view.imgSearchError.setVisibility(true);
      this.setErrorSkinToAllFeilds();
    }
    this.view.forceLayout();
  },
  setContractButtonsSkin : function(isEdit){
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsServiceDetails.btnSave,true,isEdit);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsCustomers.btnSave,true,isEdit);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsCustomers.btnNext,false,isEdit);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.contractDetailsCommonButtons.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.contractDetailsCommonButtons.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractAccounts.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractAccounts.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractFA.btnNext,false,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractLimits.btnSave,true,true);
    this.AdminConsoleCommonUtils.setEnableDisableSkinForButton(this.view.commonButtonsContractLimits.btnNext,false,true);
    this.view.forceLayout();
  },
  validateCoreCustSearch : function(){
    if (this.view.textBoxEntry11.tbxEnterValue.text.trim() !== "") return true;
    if (this.view.textBoxEntry12.tbxEnterValue.text.trim() !== "") return true;
    if (this.view.textBoxEntry13.tbxEnterValue.text.trim() !== "") return true;
    if (this.view.textBoxEntry21.txtContactNumber.text.trim() !== ""&&this.view.textBoxEntry21.txtISDCode.text.trim()!=="") return true;
    if (this.view.customCalCustomerDOB.value !== "") return true;
    if (this.view.lblSelectedRows.text !== kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SelectAStatus")) return true;
    if (this.view.textBoxEntry31.tbxEnterValue.text.trim() !== "") return true;
    if (this.view.textBoxEntry32.tbxEnterValue.text.trim() !== "") return true;
    if (this.view.textBoxEntry33.tbxEnterValue.text.trim() !== "") return true;
    this.setErrorSkinsToCoreSearch();
    return false;
  },
  //to set error skin for all core customer search fields
  setErrorSkinsToCoreSearch: function(){
    this.view.textBoxEntry11.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.textBoxEntry12.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.textBoxEntry13.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.textBoxEntry21.txtContactNumber.skin = "skntbxBordereb30173px";
    this.view.textBoxEntry21.txtISDCode.skin = "skntbxBordereb30173px";
    this.view.flxCalendarDOB22.skin="sknflxEnterValueError";
    this.view.flxDropDown23.skin= "sknflxEnterValueError";
    this.view.textBoxEntry31.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.textBoxEntry32.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.textBoxEntry33.flxEnterValue.skin = "sknflxEnterValueError";
    this.view.imgCustomerSearchError.setVisibility(true);
    this.view.lblCustomerSearchError.setVisibility(true);
  },
  setNormalSkinToCoreSearch: function(){
    this.view.textBoxEntry11.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.textBoxEntry12.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.textBoxEntry13.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.textBoxEntry21.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.textBoxEntry21.txtISDCode.skin = "skntxtbxDetails0bbf1235271384a";
    this.view.flxCalendarDOB22.skin="sknflxEnterValueNormal";
    this.view.flxDropDown23.skin= "sknflxEnterValueNormal";
    this.view.textBoxEntry31.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.textBoxEntry32.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.textBoxEntry33.flxEnterValue.skin = "sknflxEnterValueNormal";
    this.view.imgCustomerSearchError.setVisibility(false);
    this.view.lblCustomerSearchError.setVisibility(false);
  },
  resetCoreCustomerSearch : function(){
    this.view.textBoxEntry11.tbxEnterValue.text = "";
    this.view.textBoxEntry12.tbxEnterValue.text= "";
    this.view.textBoxEntry13.tbxEnterValue.text= "";
    this.view.textBoxEntry21.txtContactNumber.text= "";
    this.view.textBoxEntry21.txtISDCode.text="";
    this.view.customCalCustomerDOB.resetData=kony.i18n.getLocalizedString("i18n.frmLogsController.Select_Date");
    this.view.customCalCustomerDOB.value="";
    this.view.lblSelectedRows.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.SelectAStatus");
    this.view.textBoxEntry31.tbxEnterValue.text= "";
    this.view.textBoxEntry32.tbxEnterValue.text= "";
    this.view.textBoxEntry33.tbxEnterValue.text= "";
    this.view.AdvancedSearchDropDown01.sgmentData.selectedRowIndex=null;
    this.view.forceLayout();
  },
  searchCoreCustomers : function(){
    var dob=this.getDateFormatYYYYMMDD(this.view.customCalCustomerDOB.value);
    var selectedInd=this.view.AdvancedSearchDropDown01.sgmentData.selectedRowIndex?this.view.AdvancedSearchDropDown01.sgmentData.selectedRowIndex[1]:null;
    var status="";
    if(selectedInd!==undefined&&selectedInd!==null)
      status=this.view.AdvancedSearchDropDown01.sgmentData.data[selectedInd].customerStatusId;
    var searchRequest={
      "id":this.view.textBoxEntry11.tbxEnterValue.text,
      "name":this.view.textBoxEntry12.tbxEnterValue.text,
      "email":this.view.textBoxEntry13.tbxEnterValue.text,
      "phoneNumber":this.view.textBoxEntry21.txtContactNumber.text,
      "phoneCountryCode":this.view.textBoxEntry21.txtISDCode.text,
      "dob":dob==="NaN-NaN-NaN"?"":dob,
      "customerStatus":status,
      "country":this.view.textBoxEntry31.tbxEnterValue.text,
      "town":this.view.textBoxEntry32.tbxEnterValue.text,
      "zipcode":this.view.textBoxEntry33.tbxEnterValue.text
    };
    this.view.flxLoading2.setVisibility(true);
    this.presenter.searchCoreCustomers(searchRequest);
  },
  setCoreCustomersList : function(customers){
    if(this.view.flxRow2.isVisible){
      this.view.flxColumn13.setVisibility(false);
      this.view.flxRow2.setVisibility(false);
      this.view.flxRow3.setVisibility(false);
      this.view.fonticonrightarrowSearch.text="";
      this.view.btnShowHideAdvSearch.text=kony.i18n.getLocalizedString("i18n.contracts.showAdvancedSearch");
    }
    if(customers.length>0){
      this.view.flxNoCustomersSearched.setVisibility(false);
      this.setCustomerSearchData(customers);
    }else{
      this.view.lblNoCustomersSearched.text=kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found");
      this.view.flxNoCustomersSearched.setVisibility(true);
      this.view.lblRelatedcustSubHeading.setVisibility(false);
      this.view.flxRelatedCustomerSegment.setVisibility(false);
    }
    this.view.flxLoading2.setVisibility(false);
    this.view.forceLayout();
  },
  onBreadcrumbClick : function(id){
    var self=this;
    this.view.flxCustomerSearchPopUp.info={"action":"BREADCRUMBCLICK"};
    var currBreadcrumb;
    for(var i=0;i<this.selectedCustomers.length;i++){
      if(this.selectedCustomers[i].coreCustomerId===id){
        currBreadcrumb=this.selectedCustomers.slice(0,i+1);
        break;
      }
    }
    this.presenter.getRelatedCoreCustomers({"coreCustomerId" : id});
    this.view.flxSearchBreadcrumb.info.added=[];
    var i=this.view.flxSearchBreadcrumb.widgets().concat([]);
    this.view.segBreadcrumbs.setData([]);
    for(var x=2;x<i.length-1;x++)
    	this.view.flxSearchBreadcrumb.remove(i[x]);    
    for(var k=0;k<currBreadcrumb.length;k++){
      if(currBreadcrumb[k].isSelected!==false)
      	this.addBreadcrumb(currBreadcrumb[k]);
    }
    this.view.forceLayout();
  },
  setServiceDefinitionFAData : function(featuresLimits){
    var limits=featuresLimits.limits;
    var features=featuresLimits.features;

    for(let p=0;p<limits.length;p++){
      for(let q=0;q<features.length;q++){
        if(features[q].featureId===limits[p].featureId){
          features[q]=this.getCombinedFeature(features[q],limits[p]);
        }
      }
    }
    for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++)
      this.createContractRequestParam.contractCustomers[j].features=JSON.parse(JSON.stringify(features));
    this.bulkUpdateAllFeaturesList=JSON.parse(JSON.stringify(features));//used for bulk update popup data
    this.setGlobalMonetaryActions(limits);
    this.view.forceLayout();
  },
  setContractAccountsData : function(customerAccounts){
    if(this.action===this.actionConfig.create){
      for(var i=0;i<customerAccounts.length;i++){
        for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
          if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===customerAccounts[i].coreCustomerId)
            this.createContractRequestParam.contractCustomers[j].accounts=customerAccounts[i].accounts;
        }
      }
    }else{
      var selectedAccIds={};
      var accIds=[];
      var contractDetails=JSON.parse(JSON.stringify(this.completeContractDetails));
      for(let x=0;x<contractDetails.contractCustomers.length;x++){
        if(contractDetails.contractCustomers[x].accounts){
        for(let y=0;y<contractDetails.contractCustomers[x].accounts.length;y++){
			accIds.push(contractDetails.contractCustomers[x].accounts[y].accountId);
        }
        }
        selectedAccIds[contractDetails.contractCustomers[x].coreCustomerId]=accIds;
      }
      for(var i=0;i<customerAccounts.length;i++){
        for(var j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
          if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===customerAccounts[i].coreCustomerId&&customerAccounts[i].accounts){
            for(var k=0;k<customerAccounts[i].accounts.length;k++){
              if(selectedAccIds[customerAccounts[i].coreCustomerId]){
              if(selectedAccIds[customerAccounts[i].coreCustomerId].includes(customerAccounts[i].accounts[k].accountNumber)||customerAccounts[i].accounts[k].isNew==="true")
                customerAccounts[i].accounts[k].isEnabled="true";
              else
                customerAccounts[i].accounts[k].isEnabled="false";
            }
            }
            this.createContractRequestParam.contractCustomers[j].accounts=customerAccounts[i].accounts;
          }
        }
      }
    }
  },
  // To set Features& actions or Limits Data based on selected customer Id from dropdown
  setCustSelectedData : function(component,isSearch){
    var selectedCustId=this.view[component].lblSelectedValue.info?this.view[component].lblSelectedValue.info.id:this.view[component].segList.data[0].id;
    var dataToSet=[];
    var customerDetails={};
    var address="N/A";
    for(var i=0;i<this.selectedCustomers.length;i++){
      if(this.selectedCustomers[i].coreCustomerId===selectedCustId){
        customerDetails=this.selectedCustomers[i];
        break;
      }
    }
    if(customerDetails.city!==undefined||customerDetails.country!==undefined)
      address=this.AdminConsoleCommonUtils.getAddressText(customerDetails.city,customerDetails.country);
    else
      address="N/A";
    var details = {
        "id": customerDetails.coreCustomerId,
        "name": customerDetails.coreCustomerName,
        "industry":customerDetails.industry?customerDetails.industry:"N/A",
        "email": customerDetails.email,
        "phone":customerDetails.phone,
        "address": address
      };
    if(component==="customersDropdown"){
      for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
        if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
          dataToSet=JSON.parse(JSON.stringify(this.createContractRequestParam.contractCustomers[j].accounts));
          break;
        }
      }
      if(isSearch===false){
        this.view.tbxAccountsSearch.text="";
        this.view.flxClearAccountsSearch.setVisibility(false);
        this.view.ContractAccountsList.lblName.text=customerDetails.coreCustomerName;
        this.view.ContractAccountsList.lblName.onTouchStart = this.showCustomerDetailsPopup.bind(this,details,false);
        this.view.ContractAccountsList.lblData1.text=selectedCustId;
        this.view.ContractAccountsList.lblData2.text=customerDetails.taxId?customerDetails.taxId:"N/A";
        this.view.ContractAccountsList.lblData3.text=address;
        this.view.ContractAccountsList.flxPrimary.setVisibility(customerDetails.isPrimary==="true"?true:false);
        this.view.lblSeperatorAccounts.setVisibility(false);
        this.view.flxNoCustomerSelected.setVisibility(false);
        this.view.flxContractAccountsList.setVisibility(true);
        this.view.flxContractAccountsSearch.setVisibility(true);      
        this.setAccountsDataCustomers(dataToSet.concat([]));
        this.view.flxContractAccountsList.info={"totalRecords":dataToSet};
        this.setAccountTypeFilterData(dataToSet);
        this.setOwnerShipFilterData(dataToSet);
      }else{
        this.searchAccounts(dataToSet.concat([]));
      }
    }else if(component==="customersDropdownLimits"){
      var features=[];
      var limitFeatures=[];
      var limitActions=[]
      for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
        if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
          features=JSON.parse(JSON.stringify(this.createContractRequestParam.contractCustomers[j].features));
          break;
        }
      }
      var isSelected=true;
      for(var a=0;a<features.length;a++){
        if(features[a].type==="MONETARY"&&features[a].actions.length>0){
          limitFeatures=JSON.parse(JSON.stringify(features[a]));
          limitActions = features[a].actions.filter(function(item) {
            isSelected=true;
            //to check whether this action is checked in features and actions tab or not
            if(item.isEnabled)
              isSelected=item.isEnabled==="true"?true:false;
            return item.type=="MONETARY"&&isSelected;
          });
          if(limitActions.length>0){
            limitFeatures.actions=limitActions;
            dataToSet.push(limitFeatures);
          }
        }
      }
      if(isSearch===false){
        this.view.lblSeperatorLimits.setVisibility(false);
        this.view.flxContractLimitsList.setVisibility(true);
        this.view.flxContractLimits.setVisibility(true);
        this.view.flxNoCustomerSelectedLimits.setVisibility(false);
        this.view.ContractLimitsList.lblName.text=customerDetails.coreCustomerName;
        this.view.ContractLimitsList.lblName.onTouchStart = this.showCustomerDetailsPopup.bind(this,details,false);
        this.view.ContractLimitsList.flxPrimary.setVisibility(customerDetails.isPrimary==="true"?true:false);
        this.view.ContractLimitsList.lblData1.text=selectedCustId;
        this.view.ContractLimitsList.lblData2.text=customerDetails.taxId?customerDetails.taxId:"N/A";
        this.view.ContractLimitsList.lblData3.text=address;
        if(dataToSet.length>0){
          this.view.tbxContractLimitsSearch.text="";
          this.view.flxClearContractLimitsSearch.setVisibility(false);
          this.view.ContractLimitsList.flxNoFilterResults.setVisibility(false);
          this.view.ContractLimitsList.segAccountFeatures.setVisibility(true);
          this.view.flxContractLimitsSearch.setVisibility(true);
          this.view.ContractLimitsList.btnReset.setVisibility(true);
          this.view.btnUpdateInBulkLimits.setVisibility(true);
          this.setLimitsDataForCustomer(JSON.parse(JSON.stringify(dataToSet)));
        }else{
          this.view.ContractLimitsList.lblNoFilterResults.text="There are no limits associated with the selected features.";
          this.view.ContractLimitsList.flxNoFilterResults.setVisibility(true);
          this.view.ContractLimitsList.segAccountFeatures.setVisibility(false);
          this.view.flxContractLimitsSearch.setVisibility(false);
          this.view.ContractLimitsList.btnReset.setVisibility(false);
          this.view.btnUpdateInBulkLimits.setVisibility(false);
        }
      }else{
        this.searchLimits(JSON.parse(JSON.stringify(dataToSet)));
      }
    }else if(component==="customersDropdownFA"){
      for(let j=0;j<this.createContractRequestParam.contractCustomers.length;j++){
        if(this.createContractRequestParam.contractCustomers[j].coreCustomerId===selectedCustId){
          dataToSet=JSON.parse(JSON.stringify(this.createContractRequestParam.contractCustomers[j].features));
          break;
        }
      }
      if(isSearch===false){
        this.view.tbxContractFASearch.text="";
        this.view.flxClearContractFASearch.setVisibility(false);
        this.view.lblSeperatorFA.setVisibility(false);
        this.view.flxNoCustomerSelectedFA.setVisibility(false);
        this.view.flxContractFAList.setVisibility(true);
        this.view.flxContractFASearch.setVisibility(true);
        this.view.ContractFAList.lblName.text=customerDetails.coreCustomerName;
        this.view.ContractFAList.lblName.onTouchStart = this.showCustomerDetailsPopup.bind(this,details,false);
        this.view.ContractFAList.flxPrimary.setVisibility(customerDetails.isPrimary==="true"?true:false);
        this.view.ContractFAList.lblData1.text=selectedCustId;
        this.view.ContractFAList.lblData2.text=customerDetails.taxId?customerDetails.taxId:"N/A";
        this.view.ContractFAList.lblData3.text=address;
        this.setFeaturesDataCustomers(JSON.parse(JSON.stringify(dataToSet)));
      }else{
        this.searchFA(JSON.parse(JSON.stringify(dataToSet)));
      }
    }
  },
  createContract : function(){
    var requestParam=JSON.parse(JSON.stringify(this.createContractRequestParam));
	var featureActions=[];
    if(this.action===this.actionConfig.edit)
      requestParam.contractId=this.createContractRequestParam.contractId;
    requestParam.contractName=this.view.contractDetailsEntry1.tbxEnterValue.text;
    requestParam.serviceDefinitionName=this.view[this.selectedServiceCard].lblCategoryName.tooltip;
    requestParam.serviceDefinitionId=this.view[this.selectedServiceCard].lblCategoryName.info.catId;
    requestParam.faxId="abcd";//this.view.contractDetailsEntry2.tbxEnterValue.text
    requestParam.communication=[{"phoneNumber":this.view.contractContactNumber.txtContactNumber.text,"phoneCountryCode":this.view.contractContactNumber.txtISDCode.text,"email":this.view.contractDetailsEntry3.tbxEnterValue.text}];
    requestParam.address=[{"country":this.view.typeHeadContractCountry.tbxSearchKey.text,"cityName":this.view.contractDetailsEntry8.tbxEnterValue.text,
                           "state":this.view.contractDetailsEntry8.tbxEnterValue.text,"zipCode":this.view.contractDetailsEntry6.tbxEnterValue.text,
                           "addressLine1":this.view.contractDetailsEntry4.tbxEnterValue.text,"addressLine2":this.view.contractDetailsEntry5.tbxEnterValue.text}]

    for(var a=0;a<this.createContractRequestParam.contractCustomers.length;a++){
      requestParam.contractCustomers[a].isBusiness=this.createContractRequestParam.contractCustomers[a].isBusiness;
      requestParam.contractCustomers[a].accounts=[];
      requestParam.contractCustomers[a].excludedAccounts=[];
      for(var b=0;b<this.createContractRequestParam.contractCustomers[a].accounts.length;b++){
        if(this.createContractRequestParam.contractCustomers[a].accounts[b].isEnabled){
          //to check if the user has selected this account or not
          if(this.createContractRequestParam.contractCustomers[a].accounts[b].isEnabled==="true"){
            requestParam.contractCustomers[a].accounts.push({
              "accountId":this.createContractRequestParam.contractCustomers[a].accounts[b].accountNumber||this.createContractRequestParam.contractCustomers[a].accounts[b].accountId,
              "accountType":this.createContractRequestParam.contractCustomers[a].accounts[b].accountType,
              "accountName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountName,
              "typeId":this.createContractRequestParam.contractCustomers[a].accounts[b].typeId,
              "ownerType":this.createContractRequestParam.contractCustomers[a].accounts[b].ownerType,
              "accountHolderName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountHolderName,
              "accountStatus":this.createContractRequestParam.contractCustomers[a].accounts[b].accountStatus,
              "arrangementId":this.createContractRequestParam.contractCustomers[a].accounts[b].arrangementId
            });
          }else{
            requestParam.contractCustomers[a].excludedAccounts.push({
              "accountId":this.createContractRequestParam.contractCustomers[a].accounts[b].accountNumber||this.createContractRequestParam.contractCustomers[a].accounts[b].accountId,
              "accountType":this.createContractRequestParam.contractCustomers[a].accounts[b].accountType,
              "accountName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountName,
              "typeId":this.createContractRequestParam.contractCustomers[a].accounts[b].typeId,
              "ownerType":this.createContractRequestParam.contractCustomers[a].accounts[b].ownerType,
              "accountHolderName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountHolderName,
              "accountStatus":this.createContractRequestParam.contractCustomers[a].accounts[b].accountStatus,
              "arrangementId":this.createContractRequestParam.contractCustomers[a].accounts[b].arrangementId
            });
          }
        }else{//if the user has neither selected nor unselected this account
          requestParam.contractCustomers[a].accounts.push({
            "accountId":this.createContractRequestParam.contractCustomers[a].accounts[b].accountNumber||this.createContractRequestParam.contractCustomers[a].accounts[b].accountId,
            "accountType":this.createContractRequestParam.contractCustomers[a].accounts[b].accountType,
            "accountName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountName,
            "typeId":this.createContractRequestParam.contractCustomers[a].accounts[b].typeId,
            "ownerType":this.createContractRequestParam.contractCustomers[a].accounts[b].ownerType,
            "accountHolderName":this.createContractRequestParam.contractCustomers[a].accounts[b].accountHolderName,
            "accountStatus":this.createContractRequestParam.contractCustomers[a].accounts[b].accountStatus,
            "arrangementId":this.createContractRequestParam.contractCustomers[a].accounts[b].arrangementId
          });
        }
      }
      requestParam.contractCustomers[a].features=[];
      for(var b=0;b<this.createContractRequestParam.contractCustomers[a].features.length;b++){
        featureActions=[];
        for(var c=0;c<this.createContractRequestParam.contractCustomers[a].features[b].actions.length;c++){
          if(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled===undefined||this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled===null){
            //if the user has neither selected nor unselected this actions
            this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled="true";
          }
          featureActions[c]={
            "actionId":this.createContractRequestParam.contractCustomers[a].features[b].actions[c].id||this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionId,
            "actionName":this.createContractRequestParam.contractCustomers[a].features[b].actions[c].name||this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionsName,
            "actionDescription":this.createContractRequestParam.contractCustomers[a].features[b].actions[c].description||this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionDescription,
            "isAllowed":this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled,
          };
          if(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].limits)
            featureActions[c].limits=this.createContractRequestParam.contractCustomers[a].features[b].actions[c].limits;          
        }
        requestParam.contractCustomers[a].features[b]={
          "featureName":this.createContractRequestParam.contractCustomers[a].features[b].name||this.createContractRequestParam.contractCustomers[a].features[b].featureName,
          "featureId":this.createContractRequestParam.contractCustomers[a].features[b].id||this.createContractRequestParam.contractCustomers[a].features[b].featureId,
          "featureDescription":this.createContractRequestParam.contractCustomers[a].features[b].description||this.createContractRequestParam.contractCustomers[a].features[b].featureDescription,
          "actions":featureActions
        };        
      }
    }
    if(this.action===this.actionConfig.create)
      this.presenter.createContract(requestParam);
    else
      this.presenter.editContract(requestParam);
  },
  createEditContractSuccess:function(context){
    this.presenter.showToastMessage(kony.i18n.getLocalizedString("i18n.frmCustomerCareController.success"),context.message);
    this.getCompanyAllDetails(context.contractId, 1);
  },
  searchServiceCards : function(){
    var searchText=this.view.tbxRecordsSearch.text.toLowerCase();
    var segStatusData = this.view.serviceTypeFilterMenu.segStatusFilterDropdown.data;
    var indices = this.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices?this.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices[0][1]:[];
    var serviceDef=segStatusData.length===indices.length?this.view.flxContractServiceCards.info.totalRecords:this.view.flxContractServiceCards.info.filteredRecords;
    var serviceName="";
    var searchResults=serviceDef.filter(function(service){
      serviceName=service.name.toLowerCase();
      if(serviceName.indexOf(searchText)>=0)
        return service;
    });
    if(searchResults.length>0){
      this.view.flxContractServiceCards.info.filteredRecords=searchResults;
      this.setContractServiceCards(searchResults,true);
      this.view.flxNoServiceSearchResults.setVisibility(false);
      this.view.flxContractServiceCards.setVisibility(true);
    }
    else{
      this.view.lblNoServiceSearchResults.text="No records found with keyword \""+searchText+"\". Please try again.";
      this.view.flxNoServiceSearchResults.setVisibility(true);
      this.view.flxContractServiceCards.setVisibility(false);
    }
    this.view.forceLayout();
  },
  searchAccounts : function(accounts){
    var searchText=this.view.tbxAccountsSearch.text.toLowerCase();
    var accountName="";
    var searchResults=accounts.filter(function(account){
      accountName=account.accountName.toLowerCase();
      if(accountName.indexOf(searchText)>=0||account.accountNumber.indexOf(searchText)>=0)
        return account;
    });
    if(searchResults.length>0){
      this.setAccountsDataCustomers(searchResults);
      this.view.flxNoCustomerSelected.setVisibility(false);
      this.view.flxContractAccountsList.setVisibility(true);
    }
    else{
      this.view.lblNoCustomersSelected.text=kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found");
      this.view.imgContractAccounts.setVisibility(false);
      this.view.flxNoCustomerSelected.setVisibility(true);
      this.view.flxContractAccountsList.setVisibility(false);
    }
  },
  searchFA : function(features){
    var searchText=this.view.tbxContractFASearch.text.toLowerCase();
    var searchedActions=[];
    var featureName="";
    var actionName="";
    var searchResults=features.filter(function(feature){
      featureName=feature.name?feature.name.toLowerCase():feature.featureName.toLowerCase();
      if(featureName.indexOf(searchText)>=0)
        return feature;
      else{
          for(let x=0;x<feature.actions.length;x++){
            actionName=feature.actions[x].name?feature.actions[x].name.toLowerCase():feature.actions[x].actionName.toLowerCase();
            if(actionName.indexOf(searchText)>=0)
              searchedActions.push(feature.actions[x]);
          }
          if(searchedActions.length>0){
            feature.actions=searchedActions;
            return feature;
          }
        }
    });
    if(searchResults.length>0){
      this.setFeaturesAtCustomerLevel(this.view.ContractFAList.segAccountFeatures,searchResults);
      this.view.flxNoCustomerSelectedFA.setVisibility(false);
      this.view.flxContractFAList.setVisibility(true);
    }
    else{
      this.view.lblNoCustomersSelectedFA.text=kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found");
      this.view.imgContractFA.setVisibility(false);
      this.view.flxNoCustomerSelectedFA.setVisibility(true);
      this.view.flxContractFAList.setVisibility(false);
    }
  },
  searchLimits : function(features){
    var searchText=this.view.tbxContractLimitsSearch.text.toLowerCase();
    var searchedActions=[];
    var featureName="";
    var actionName="";
    var searchResults=features.filter(function(feature){
      featureName=feature.name?feature.name.toLowerCase():feature.featureName.toLowerCase();
      if(featureName.indexOf(searchText)>=0)
        return feature;
      else{
        for(let x=0;x<feature.actions.length;x++){
          actionName=feature.actions[x].name?feature.actions[x].name.toLowerCase():feature.actions[x].actionName.toLowerCase();
          if(actionName.indexOf(searchText)>=0)
            searchedActions.push(feature.actions[x]);
        }
        if(searchedActions.length>0){
          feature.actions=searchedActions;
          return feature;
        }
      }
    });
    if(searchResults.length>0){
      this.setLimitsAtCustomerLevel(searchResults);
      this.view.ContractLimitsList.segAccountFeatures.setVisibility(true);
      this.view.ContractLimitsList.flxNoFilterResults.setVisibility(false);
    }
    else{
      this.view.ContractLimitsList.lblNoFilterResults.text=kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.No_results_found");
      this.view.ContractLimitsList.flxNoFilterResults.setVisibility(true);
      this.view.ContractLimitsList.segAccountFeatures.setVisibility(false);
    }
    this.view.forceLayout();
  },
     /*
  * set filter data in contract services tab
  * 2param: search result
  */
  setDataToServiceTypeFilter : function(data){
    var self = this;
    var statusList=[],maxLenText = "";
    var serviceType="";
    for(var i=0;i<data.length;i++){
      if(!statusList.contains(data[i].serviceType))
        statusList.push(data[i].serviceType);
    }
    var widgetMap = {
      "serviceType": "serviceType",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = statusList.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;
      if(rec==="TYPE_ID_WEALTH")
        serviceType="Wealth Banking";
      else
        serviceType=rec==="TYPE_ID_RETAIL"?"Retail Banking":rec==="TYPE_ID_BUSINESS"?"Business Banking":"Retail & Business Banking";
      return {
        "serviceType": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": serviceType
      };
    });
    self.view.serviceTypeFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.serviceTypeFilterMenu.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<statusList.length;j++){
      selStatusInd.push(j);
    }
    self.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    //set filter width
    self.view.flxServiceTypeFilter.width = self.AdminConsoleCommonUtils.getLabelWidth(maxLenText)+35+"px";
    self.view.forceLayout();
  },
  /*
  * filter the company's list based on the business type selected
  */
  performServiceTypeFilter : function(){
    var self = this;
    var selType = [];
    var selInd;
    var dataToShow = [];
    var allData = self.view.tbxRecordsSearch.text.trim().length===0?self.view.flxContractServiceCards.info.totalRecords:self.view.flxContractServiceCards.info.filteredRecords;
    var segStatusData = self.view.serviceTypeFilterMenu.segStatusFilterDropdown.data;
    var indices = self.view.serviceTypeFilterMenu.segStatusFilterDropdown.selectedIndices;
    if(indices){
      selInd = indices[0][1];
      for(var i=0;i<selInd.length;i++){
        selType.push(self.view.serviceTypeFilterMenu.segStatusFilterDropdown.data[selInd[i]].serviceType);
      }
      if (selInd.length === segStatusData.length) { //all are selected
        self.setContractServiceCards(allData,false);
        self.view.flxContractServiceCards.setVisibility(true);
        self.view.flxNoServiceSearchResults.setVisibility(false);
      } else {
        dataToShow = allData.filter(function(rec){
          if(selType.indexOf(rec.serviceType) >= 0){
            return rec;
          }
        });
        if (dataToShow.length > 0) {
          self.view.flxContractServiceCards.info.filteredRecords=dataToShow;
          self.setContractServiceCards(dataToShow,false);
          self.view.flxContractServiceCards.setVisibility(true);
          self.view.flxNoServiceSearchResults.setVisibility(false);
        } else {
          self.view.lblNoServiceSearchResults.text="No records found with given filters. Please try again.";
          self.view.flxContractServiceCards.setVisibility(false);
          self.view.flxNoServiceSearchResults.setVisibility(true);
        }
      }
    } else{
      self.view.lblNoServiceSearchResults.text="No records found with given filters. Please try again.";
      self.view.flxContractServiceCards.setVisibility(false);
      self.view.flxNoServiceSearchResults.setVisibility(true);
    }
  },
  fetchCustomerStatusList : function(){
    var requestParam={
      "bundle_name" : "C360",
      "config_key": "CUSTOMER_STATUS"
    }
    this.presenter.getCustomerStatusList(requestParam);
  },
  setCustomerStatusData : function(statusListString){
    var statusList=JSON.parse(statusListString);
    this.view.lblSelectedRows.text="Select a Status";
    var statusData=[];
    var widgetMap = {
      "customerStatusId": "customerStatusId",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = statusList.code.map(function(rec){     
      return {
        "customerStatusId": rec.codeId,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": {"isVisible":false},
        "lblDescription": rec.codeName
      };
    });
    this.view.AdvancedSearchDropDown01.sgmentData.widgetDataMap = widgetMap;
    this.view.AdvancedSearchDropDown01.sgmentData.setData(statusData);
    this.view.AdvancedSearchDropDown01.sgmentData.info={"data":statusData};
    this.view.AdvancedSearchDropDown01.sgmentData.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
    this.view.forceLayout();
  },
  setServiceTypeStatusData : function(records){
    // setting the x & y position for drop down
    
    // this.view.txtSearchParam3 for getting the top position
    // this.view.lblSearchParam8 for getting left position

    // this.view.flxColumnServType.top = '0dp';
    // this.view.flxColumnServType.left = '0dp';
    // this.view.forceLayout();

    // let e = document.getElementById('frmCompanies_flxColumnServType');
    // var rect1 = e.getBoundingClientRect(); 

    //  // for left position
    //  e = document.getElementById('frmCompanies_lblSearchParam8');
    //  // Header icon
    //  var rect = e.getBoundingClientRect(); 
    //  console.log(rect.top, rect.right, rect.bottom, rect.left);
    //  let diff = rect.left - rect1.left 
    //  this.view.flxColumnServType.left = diff +'dp';

    //  // for top position
    //  e = document.getElementById('frmCompanies_txtSearchParam3');
    //  // Header icon
    //  var rect = e.getBoundingClientRect(); 
    //  console.log(rect.top, rect.right, rect.bottom, rect.left);
    //  let diff = rect.top - rect1.top;
    //  this.view.flxColumnServType.left = diff +'dp';

    this.view.lblSelectedRowsServType.text="Select";
    var statusData=[];
    var widgetMap = {
      "customerStatusId": "customerStatusId",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = records.map(function(rec){     
      return {
        "customerStatusId": rec.id,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": {"isVisible":false},
        "lblDescription": rec.name
      };
    });
    this.view.AdvancedSearchDropDownServType.sgmentData.widgetDataMap = widgetMap;
    this.view.AdvancedSearchDropDownServType.sgmentData.setData(statusData);
    this.view.AdvancedSearchDropDownServType.sgmentData.info={"data":statusData};
    this.view.AdvancedSearchDropDownServType.sgmentData.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
    this.view.forceLayout();
  },
  /*
  * sort based on selected segment column
  * @param: column to sort, segment path, segment category
  */
  sortAndSetData : function(columnName,segmentPath,category){
    var segData = segmentPath.data;
    var secInd = segmentPath.selectedsectionindex?segmentPath.selectedsectionindex:0;
    this.sortBy.column(columnName);
    if(category === 1){ //edit accounts screen
      segData[secInd][1] = segData[secInd][1].sort(this.sortBy.sortData);
      segData[secInd][0].lblIconSortAccName = this.determineSortIconForSeg(this.sortBy,"lblAccountNumber");
      segData[secInd][0].lblIconAccNameSort = this.determineSortIconForSeg(this.sortBy,"lblAccountName");
      segmentPath.setSectionAt(segData[secInd],secInd);
    }
  },
  /*
  * show filter for account types in account tab of edit user access
  *@param: option-(account type:1, ownership:2), event object, context object
  */
  showFilterForAccountsSectionClick : function(option,event,context){
    if(option === 1)
      this.view.flxOwnershipFilter.setVisibility(false);
    else if(option === 2)
      this.view.flxAccountsFilter.setVisibility(false);
    var filterWidget = (option === 1) ? this.view.flxAccountsFilter :this.view.flxOwnershipFilter;
    var filterIcon = (option === 1) ? "lblIconFilterAccType":"lblIconSortAccHolder" ;
    
    var flxRight = context.widgetInfo.frame.width - event.frame.x - event.frame.width;
    var iconRight = event.frame.width - event[filterIcon].frame.x;
    filterWidget.right = (flxRight + iconRight - 7) + "dp";
    filterWidget.top =(this.view.ContractAccountsList.flxCardBottomContainer.frame.y + 40+110) +"dp";
    if(filterWidget.isVisible){
      filterWidget.setVisibility(false);
    } else{
      filterWidget.setVisibility(true);
    }
  },
    /*
  * set accounts types filter data
  * @param: option -1/2
  */
  setAccountTypeFilterData : function(data){
    var self = this;
    var accTypes=[],maxLenText = "";
    for(var i=0;i<data.length;i++){
      if(!accTypes.contains(data[i].accountType))
        accTypes.push(data[i].accountType);
    }
    var widgetMap = {
      "accountType": "accountType",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = accTypes.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;     
      return {
        "accountType": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": rec
      };
    });
    self.view.accountTypesFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.accountTypesFilterMenu.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<accTypes.length;j++){
      selStatusInd.push(j);
    }
    self.view.accountTypesFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    //set filter width
    self.view.flxAccountsFilter.width = self.AdminConsoleCommonUtils.getLabelWidth(maxLenText)+55+"px";
    self.view.forceLayout();
  },
  setOwnerShipFilterData: function(data){
    var self = this;
    var accTypes=[],maxLenText = "";
    var ownerType="N/A";
    for(var i=0;i<data.length;i++){
      ownerType=data[i].ownerType?data[i].ownerType:"N/A";
      if(!accTypes.contains(ownerType))
        accTypes.push(ownerType);
    }
    var widgetMap = {
      "ownerType": "ownerType",
      "flxSearchDropDown": "flxSearchDropDown",
      "flxCheckBox": "flxCheckBox",
      "imgCheckBox": "imgCheckBox",
      "lblDescription": "lblDescription"
    };
    var statusData = accTypes.map(function(rec){
      maxLenText = (rec.length > maxLenText.length) ? rec : maxLenText;     
      return {
        "ownerType": rec,
        "flxSearchDropDown": "flxSearchDropDown",
        "flxCheckBox": "flxCheckBox",
        "imgCheckBox": "checkbox.png",
        "lblDescription": rec
      };
    });
    self.view.ownershipFilterMenu.segStatusFilterDropdown.widgetDataMap = widgetMap;
    self.view.ownershipFilterMenu.segStatusFilterDropdown.setData(statusData);
    var selStatusInd = [];
    for(var j=0;j<accTypes.length;j++){
      selStatusInd.push(j);
    }
    self.view.ownershipFilterMenu.segStatusFilterDropdown.selectedIndices = [[0,selStatusInd]];
    //set filter width
    self.view.flxOwnershipFilter.width = self.AdminConsoleCommonUtils.getLabelWidth(maxLenText)+55+"px";
    self.view.forceLayout();
  },
  performAccountOwnerFilters : function(){
    var self = this;
    var selFilter = [[]];
    var dataToShow = [];
    var accountsList = self.view.flxContractAccountsList.info.totalRecords;
    var ownershipIndices = self.view.ownershipFilterMenu.segStatusFilterDropdown.selectedIndices;
    var typeIndices = self.view.accountTypesFilterMenu.segStatusFilterDropdown.selectedIndices;
    selFilter[0][1] = [];
    selFilter[0][0] = [];
    var selOwnershipInd = null;
    var selTypeInd = null;
    //get selected account types
    selTypeInd = typeIndices ? typeIndices[0][1] : [];
    for (var i = 0; i < selTypeInd.length; i++) {
      selFilter[0][0].push(self.view.accountTypesFilterMenu.segStatusFilterDropdown.data[selTypeInd[i]].accountType);
    }
    //get selected ownerships
    selOwnershipInd = ownershipIndices ? ownershipIndices[0][1] : [];
    for (var j = 0; j < selOwnershipInd.length; j++) {
      selFilter[0][1].push(self.view.ownershipFilterMenu.segStatusFilterDropdown.data[selOwnershipInd[j]].ownerType);
    }

    if (selFilter[0][0].length === 0 || selFilter[0][1].length === 0) { //none selected - show no results
      dataToShow = [];
    } else if (selFilter[0][0].length > 0 && selFilter[0][1].length > 0) {//both filters selected
      dataToShow = accountsList.filter(function (rec) {  
        if (selFilter[0][1].indexOf(rec.ownerType?rec.ownerType:"N/A") >= 0&&selFilter[0][0].indexOf(rec.accountType) >= 0) {
          return rec;
        }
      });
    } else { //single filter selected
    }
    return dataToShow;
  },
  /*
  * validation to check if all rows are selected
  * @param: segment path, section/row level
  */
  validateCheckboxSelections : function(segmentPath, isSection){
    var isValid = true, count =0;
    var segData = segmentPath.data;
    var imgName = isSection === true ? "imgSectionCheckbox" : "imgCheckBox";
    for(var i=0;i<segData.length;i++){
      var data = isSection === true ? segData[i][0] : segData[i];
      if(data[imgName].src !== this.AdminConsoleCommonUtils.checkboxnormal){
        count = count +1;
      }
    }
    isValid = count === 0 ? false: true;
    return isValid;
  },
  /*
  * check for checkbox selection for all the cards of container
  * @param: cards container path, segment name, is section level
  */
  validateSelectionForMultipleCards : function(flexPath,segmentName,isSection){
    var isValid = true;
    isValid = this.validateCheckboxSelections(flexPath[segmentName],isSection);
    return isValid;
  },
  /*
  * show the range tooltip for limits
  */
  showRangeTooltip : function(widget,context,range){
    if (context.eventType === constants.ONHOVER_MOUSE_ENTER) {
      this.view.flxRangeTooltip.top = (context.pageY -120)+"dp";
      this.view.flxRangeTooltip.left = (context.pageX - 305 -230 -70) +"dp"; //(pageX -leftmenu-verticaltabs- left,right padding)
      if(this.view.flxRangeTooltip.isVisible === false){
        this.view.lblIconRangeCurr11.text=this.currencyValue;
        this.view.lblRangeValue11.text=range["MIN_TRANSACTION_LIMIT"];
        this.view.lblIconRangeCurr12.text=this.currencyValue;
        this.view.lblRangeValue12.text=range["MAX_TRANSACTION_LIMIT"];
        this.view.lblIconRangeCurr21.text=this.currencyValue;
        this.view.lblRangeValue21.text=range["MIN_TRANSACTION_LIMIT"];
        this.view.lblIconRangeCurr22.text=this.currencyValue;
        this.view.lblRangeValue22.text=range["DAILY_LIMIT"];
        this.view.lblIconRangeCurr31.text=this.currencyValue;
        this.view.lblRangeValue31.text=range["MIN_TRANSACTION_LIMIT"];
        this.view.lblIconRangeCurr32.text=this.currencyValue;
        this.view.lblRangeValue32.text=range["WEEKLY_LIMIT"];
        this.view.flxRangeTooltip.setVisibility(true);
        this.view.forceLayout();
      }    
    } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
      if(this.view.flxRangeTooltip.isVisible === true)
        this.view.flxRangeTooltip.setVisibility(false);
    }
  },
    /*
  * show customer's details in the popup
  * @param: customer details json
  */
  showCustomerDetailsPopup : function(details,showBackButton){
    if(this.view.flxCustomerSearchPopUp.isVisible){
      this.view.flxCustomerSearchPopUp.setVisibility(false);
    }
    
    this.view.flxContractDetailsPopup.setVisibility(true);
    this.view.contractDetailsPopup.showBackButton(showBackButton);
    this.view.contractDetailsPopup.setDataForPopup(details);
    this.view.forceLayout();

  },
  updateFeatureLimitsBulkChanges : function(){
    var rowsList = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    var featureId="";
    var actionIds=[];
    var isEnable=false;
    var bulkUpdateList=[];
    var typeValue=this.getSelectedType();
    var selectedCust=this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.info.added;
    var limitVal=this.view.tbxValue21
    var selectedCustIds=[];
    var isFeatures=this.view.flxContractFA.isVisible;
    var dependentActions=[];
    for(let a=0;a<selectedCust.length;a++)
      selectedCustIds.push(selectedCust[a][1]);
    //get all assigned feature id's
    for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select")
        featureId=rowsList[i].lstBoxFieldValue11.selectedKey;
      actionIds=[];
      if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems){
        var selItems=rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems;
        for(let i=0;i<selItems.length;i++){
          if(!actionIds.includes(selItems[i].actionId)){
            actionIds.push(selItems[i].actionId);
            if(isFeatures){
              //To check dependent actions in bulk update permissions
              for(let j=0;j<selItems[i].dependentActions.length;j++){
                if(!actionIds.includes(selItems[i].dependentActions[j].trim()))
                  actionIds.push(selItems[i].dependentActions[j].trim());
              }
            }
          }
        }
      }
      if(actionIds.length>0){
      if(isFeatures){
        bulkUpdateList.push({"featureId":featureId,"actionIds":actionIds,"isEnable":typeValue==="enable"?"true":"false"});
      }else{
        bulkUpdateList.push({"featureId":featureId,"actionIds":actionIds,"limitId":typeValue,"limitVal":rowsList[i].tbxValue21.text});
      }
      }
    }
    for(var a=0;a<this.createContractRequestParam.contractCustomers.length;a++){
      if(selectedCustIds.includes(this.createContractRequestParam.contractCustomers[a].coreCustomerId)){
        for(var b=0;b<this.createContractRequestParam.contractCustomers[a].features.length;b++){
          for(let x=0;x<bulkUpdateList.length;x++){
            if(this.createContractRequestParam.contractCustomers[a].features[b].featureId===bulkUpdateList[x].featureId){
              for(var c=0;c<this.createContractRequestParam.contractCustomers[a].features[b].actions.length;c++){
                if(bulkUpdateList[x].actionIds.includes(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionId)){
                  this.updateBulkRequestParam(a,b,c,isFeatures,bulkUpdateList[x]);
                }
              }
            }
          }
        }
      }
    }
    this.view.flxBulkUpdateFeaturesPopup.setVisibility(false);
    if(isFeatures){
      this.view.customersDropdownLimits.lblSelectedValue.info={"id":selectedCustIds[0]};
      this.setSelectedText("customersDropdownFA",selectedCustIds[0]);
      this.setCustSelectedData("customersDropdownFA",false);
      this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.contracts.FeatureBulkUpdateMsg"), this);
    }
    else{
      this.view.customersDropdownLimits.lblSelectedValue.info={"id":selectedCustIds[0]};
      this.setSelectedText("customersDropdownLimits",selectedCustIds[0]);
      this.setCustSelectedData("customersDropdownLimits",false);
      this.view.toastMessage.showToastMessage(kony.i18n.getLocalizedString("i18n.contracts.LimitsBulkUpdateMsg"), this);
    }
},
  updateBulkRequestParam : function(custInd,featureInd,actionInd,isFA,data){
    if(isFA){
      this.createContractRequestParam.contractCustomers[custInd].features[featureInd].actions[actionInd].isEnabled=data.isEnable;
    }else{
      for(let x=0;x<3;x++){
      if(this.createContractRequestParam.contractCustomers[custInd].features[featureInd].actions[actionInd].limits[x].id===data.limitId)
        this.createContractRequestParam.contractCustomers[custInd].features[featureInd].actions[actionInd].limits[x].value=data.limitVal;
      }
    }
  },
  getSelectedType : function() {
    var radBtn = this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton1.src;
    var radBtn1 = this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton2.src;
    var radBtn2 = this.view.bulkUpdateFeaturesLimitsPopup.customRadioButtonGroup.imgRadioButton3.src;
    if(this.view.flxContractFA.isVisible){
      if(radBtn === "radio_selected.png") 
        return "enable";
      else
        return "disable";
    }else{
      if(radBtn === "radio_selected.png") 
        return "MAX_TRANSACTION_LIMIT";
      if( radBtn1 === "radio_selected.png") 
        return "DAILY_LIMIT";
      if(radBtn2 === "radio_selected.png")
        return "WEEKLY_LIMIT";
    }
  },
  
  getFeaturesForBulkUpdate : function(){
    var bulkUpdateFeatures={};
    var featureJSON={};
    var actionsJSON={};
    var actions
    for(var i=0;i<this.bulkUpdateAllFeaturesList.length;i++){
      featureJSON={};
      featureJSON.featreName=this.bulkUpdateAllFeaturesList[i].featureName;
      featureJSON.actions=[];
      for(var j=0;j<this.bulkUpdateAllFeaturesList[i].actions.length;j++){
        actionsJSON={};
        actionsJSON.actionName=this.bulkUpdateAllFeaturesList[i].actions[j].actionName;
        actionsJSON.actionId=this.bulkUpdateAllFeaturesList[i].actions[j].actionId;
        actionsJSON.isChecked=true;
        actionsJSON.type=this.bulkUpdateAllFeaturesList[i].actions[j].type;
        actionsJSON.limitVal="0";
        actionsJSON.dependentActions=[];
        if(this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions&&this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions.length>0){
          if(typeof this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions==="string")//as we are getting string format in edit flow and object format in create flow
            actionsJSON.dependentActions=(this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions.substring(1,this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions.length-1)).split(",");
          else
            actionsJSON.dependentActions=this.bulkUpdateAllFeaturesList[i].actions[j].dependentActions.map(function(rec){return rec.id});
        }
        featureJSON.actions.push(actionsJSON);
      }
      bulkUpdateFeatures[this.bulkUpdateAllFeaturesList[i].featureId]=featureJSON;
    }
    if(this.view.flxContractFA.isVisible)
      return bulkUpdateFeatures;
    else{
      var filteredFeatures=this.filterBulkFeatures(bulkUpdateFeatures);
      return filteredFeatures;
    }
  },
  filterBulkFeatures : function(bulkFeatures){
    var selectedCust=this.view.bulkUpdateFeaturesLimitsPopup.flxTagsContainer.info.added;
    var selectedCustIds=[];
    var monetaryActions=[];
    var selectedActionIds=[];
    var isEnabled="true;"
    for(let a=0;a<selectedCust.length;a++)
      selectedCustIds.push(selectedCust[a][1]);
    
        for(var a=0;a<this.createContractRequestParam.contractCustomers.length;a++){
          if(selectedCustIds.includes(this.createContractRequestParam.contractCustomers[a].coreCustomerId)){
            for(var b=0;b<this.createContractRequestParam.contractCustomers[a].features.length;b++){
              if(this.createContractRequestParam.contractCustomers[a].features[b].type==="MONETARY"){
                for(var c=0;c<this.createContractRequestParam.contractCustomers[a].features[b].actions.length;c++){
                  isEnabled=this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled?this.createContractRequestParam.contractCustomers[a].features[b].actions[c].isEnabled:"true";
                  if(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].type==="MONETARY"&&isEnabled==="true"&&!selectedActionIds.includes(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionId)){
                    selectedActionIds.push(this.createContractRequestParam.contractCustomers[a].features[b].actions[c].actionId);
                  }
                }
              }
            }
          }
        }
    for(var i in bulkFeatures){
      for (var j=0;j<bulkFeatures[i].actions.length;j++){
        if(!selectedActionIds.includes(bulkFeatures[i].actions[j].actionId)){
          bulkFeatures[i].actions.splice(j,1);
          j--;
        }
      }
      if(bulkFeatures[i].actions.length===0)
        delete bulkFeatures[i];
        }
    return bulkFeatures;
  },
  setEditContractCustomersData : function(){
    var contractDetails=JSON.parse(JSON.stringify(this.completeContractDetails));
    this.selectedCustomers=[];
    for(let x=0;x<contractDetails.contractCustomers.length;x++){
        this.selectedCustomers.push({
          "addressLine1": contractDetails.contractCustomers[x].addressLine1,
          "addressLine2": contractDetails.contractCustomers[x].addressLine2,
          "city": contractDetails.contractCustomers[x].cityName?contractDetails.contractCustomers[x].cityName:"",
          "coreCustomerId": contractDetails.contractCustomers[x].coreCustomerId,
          "coreCustomerName": contractDetails.contractCustomers[x].coreCustomerName,
          "country": contractDetails.contractCustomers[x].country,
          "isBusiness": contractDetails.contractCustomers[x].isBusiness,
          "isPrimary": contractDetails.contractCustomers[x].isPrimary,
          "state": contractDetails.contractCustomers[x].state,
          "taxId": contractDetails.contractCustomers[x].taxId,
          "zipCode": contractDetails.contractCustomers[x].zipCode,
          "isSelected": true,
          "email":contractDetails.contractCustomers[x].email,
          "phone":contractDetails.contractCustomers[x].phone?contractDetails.contractCustomers[x].phone:"N/A",
          "industry":contractDetails.contractCustomers[x].industry
      });
    }
    this.setSelectedCustomersData();
  },
  setEditContractFeaturesLimits : function(){
    var features=this.completeCompanyDetails.accountsFeatures.features;
    var limits=this.completeCompanyDetails.accountsFeatures.limits;
    var selectedCustIds=[];
    var isNewlyAdded=true;
    var combinedFeature={};
    for(let p=0;p<limits.length;p++){
        for(let x=0;x<limits[p].contractCustomerLimits.length;x++){
          for(let y=0;y<features[p].contractCustomerFeatures.length;y++){
            if(features[p].contractCustomerFeatures[y].featureId===limits[p].contractCustomerLimits[x].featureId){
              combinedFeature={};
              combinedFeature=this.getCombinedFeature(features[p].contractCustomerFeatures[y],limits[p].contractCustomerLimits[x]);
              features[p].contractCustomerFeatures[y]=combinedFeature;
            }
          }
        }
    }
    for(let m=0;m<this.createContractRequestParam.contractCustomers.length;m++){
      isNewlyAdded=true;
      for(let n=0;n<features.length;n++){
        if(features[n].coreCustomerId===this.createContractRequestParam.contractCustomers[m].coreCustomerId){
          if(this.createContractRequestParam.contractCustomers[m].features.length===0)
          	this.createContractRequestParam.contractCustomers[m].features=features[n].contractCustomerFeatures;        
          isNewlyAdded=false;
        }
      }
      if(isNewlyAdded===true)
        this.setAllFeatures(m,features[0].contractCustomerFeatures);
    }
	this.bulkUpdateAllFeaturesList=JSON.parse(JSON.stringify(features[0].contractCustomerFeatures));//used for bulk update popup data
  },
  setAllFeatures : function(index,features){
    for(let a=0;a<features.length;a++){
      for(let b=0;b<features[a].actions.length;b++){
        features[a].actions[b].isEnabled="true";
      }
    }
    this.createContractRequestParam.contractCustomers[index].features=features;
  },
  getCombinedFeature : function(feature,limits){
    for(let a=0;a<limits.actions.length;a++){
      for(let b=0;b<feature.actions.length;b++){
        if(limits.actions[a].actionId===feature.actions[b].actionId){
          feature.actions[b].limits=limits.actions[a].limits;
          feature.actions[b].type="MONETARY";
          feature.type="MONETARY";
        }
      }
    }
    return feature;
  },
  updateButtonsText : function(isEdit){
    var btnText=isEdit?kony.i18n.getLocalizedString("i18n.contracts.updateContract_UC"):kony.i18n.getLocalizedString("i18n.Contracts.createContract");
    this.view.commonButtonsCustomers.btnSave.text=btnText;
    this.view.contractDetailsCommonButtons.btnSave.text=btnText;
    this.view.commonButtonsContractAccounts.btnSave.text=btnText;
    this.view.commonButtonsContractFA.btnSave.text=btnText;
    this.view.commonButtonsContractLimits.btnSave.text=btnText;
    this.view.forceLayout();
  },
  setAddedCustDataInRequest : function(){
    var customersData=this.selectedCustomers;
    var newlyAddedIds=[];
    var isNewlyAdded=true;
    for(var a=0;a<customersData.length;a++){
      isNewlyAdded=true;
      for(var b=0;b<this.createContractRequestParam.contractCustomers.length;b++){
        if(customersData[a].coreCustomerId===this.createContractRequestParam.contractCustomers[b].coreCustomerId){
          isNewlyAdded=false;
          break;
        }
      }
      if(isNewlyAdded){
        newlyAddedIds.push(customersData[a].coreCustomerId);
        this.createContractRequestParam.contractCustomers.push({
          "isPrimary": customersData[a].isPrimary?customersData[a].isPrimary:false,
          "coreCustomerId": customersData[a].coreCustomerId,
          "coreCustomerName": customersData[a].coreCustomerName,
          "isBusiness": customersData[a].isBusiness,
          "accounts":[],
          "features":[],
        });
      }
    }
    if(newlyAddedIds.length>0)
      this.presenter.getCoreCustomerAccounts({"coreCustomerIdList":newlyAddedIds});
    this.setAllFeatures(this.createContractRequestParam.contractCustomers.length-1,JSON.parse(JSON.stringify(this.createContractRequestParam.contractCustomers[0].features)));
  },
  setGlobalMonetaryActions : function(limits){
    var monetaryActions=[];
    var monetaryLimits={};
    var actionsJSON={};
    var limitsJSON={};
    for(var p=0;p<limits.length;p++){
      actionsJSON={};
      for(let b=0;b<limits[p].actions.length;b++){
        limitsJSON={};
        actionsJSON[limits[p].actions[b].actionId]={};
        limitsJSON[limits[p].actions[b].limits[0].id]=limits[p].actions[b].limits[0].value;
        limitsJSON[limits[p].actions[b].limits[1].id]=limits[p].actions[b].limits[1].value;
        limitsJSON[limits[p].actions[b].limits[2].id]=limits[p].actions[b].limits[2].value;
        if(limits[p].actions[b].limits[3])
        limitsJSON[limits[p].actions[b].limits[3].id]=limits[p].actions[b].limits[3].value;
        else
          limitsJSON["MIN_TRANSACTION_LIMIT"]="1.0";
        actionsJSON[limits[p].actions[b].actionId]=limitsJSON;
      }
      monetaryLimits[limits[p].featureId]=actionsJSON;
    }
    this.monetaryLimits=monetaryLimits;
  },
    showResetAllLimitsPopup: function() {
    var scopeObj = this;
    scopeObj.view.suspendFeaturePopup.lblPopUpMainMessage.text = kony.i18n.getLocalizedString("i18n.common.ResetLimits");
    scopeObj.view.suspendFeaturePopup.rtxPopUpDisclaimer.text = kony.i18n.getLocalizedString("i18n.common.Disclaimer");
    scopeObj.view.suspendFeaturePopup.btnPopUpCancel.text = kony.i18n.getLocalizedString("i18n.frmCustomers.CANCEL");
    scopeObj.view.suspendFeaturePopup.btnPopUpDelete.text = kony.i18n.getLocalizedString("i18n.common.RESET");
    scopeObj.view.suspendFeaturePopup.btnPopUpDelete.width = "116dp";
    scopeObj.view.suspendFeaturePopup.rtxPopUpDisclaimer.width = "80%";
    scopeObj.view.flxSuspendFeaturePopup.setVisibility(true);
    scopeObj.view.suspendFeaturePopup.btnPopUpDelete.onClick = function() {
        scopeObj.view.flxSuspendFeaturePopup.setVisibility(false);
        scopeObj.resetLimitValues();
    }
    scopeObj.view.forceLayout();
  },
  revertAddedCustomers : function(){
    if(this.view.segAddedCustomers.isVisible){
    var segAddedData=this.view.segAddedCustomers.data;
      var custIds=[];
      for(let x=0;x<segAddedData.length;x++){
        custIds.push(segAddedData[x].customerInfo.coreCustomerId);
      }
      for(let y=0;y<this.selectedCustomers.length;y++){
        if(!custIds.includes(this.selectedCustomers[y].coreCustomerId))
          this.selectedCustomers[y].isSelected=false;
      }
    }else{
      this.selectedCustomers=[];
    }
  },
  validateContractDetails : function(){
    // contract name
    var validation = true
    if(this.view.contractDetailsEntry1.tbxEnterValue.text.trim() === ""){
      this.view.contractDetailsEntry1.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmLocationsController.Enter_Company_Name");
      this.view.contractDetailsEntry1.flxInlineError.isVisible = true;
      this.view.contractDetailsEntry1.flxEnterValue.skin = "sknFlxCalendarError";
      validation = false;
    }
    // contact number
    var phoneRegex=/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    var phn = this.view.contractContactNumber.txtContactNumber.text.trim();
    if (phn && this.view.contractContactNumber.txtContactNumber.text.trim().length > 15) {
      this.view.contractContactNumber.flxError.width = "61%";
      this.view.contractContactNumber.flxError.isVisible = true;
      this.view.contractContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_cannot_exceed");
      this.view.contractContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if (phn && phoneRegex.test(this.view.contractContactNumber.txtContactNumber.text) === false) {
      this.view.contractContactNumber.flxError.width = "61%";
      this.view.contractContactNumber.flxError.isVisible = true;
      this.view.contractContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      this.view.contractContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if(phn === "" && this.view.contractContactNumber.txtISDCode.text.trim()){
      this.view.contractContactNumber.flxError.width = "61%";
      this.view.contractContactNumber.flxError.isVisible = true;
      this.view.contractContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ContactNo_not_valid");
      this.view.contractContactNumber.txtContactNumber.skin = "skinredbg";
      validation = false;
    } else if(phn && (!this.view.contractContactNumber.txtISDCode.text.trim() ||(this.view.contractContactNumber.txtISDCode.text === "+"))){
      this.view.contractContactNumber.flxError.width = "98%";
      this.view.contractContactNumber.flxError.isVisible = true;
      this.view.contractContactNumber.txtISDCode.skin = "skinredbg";
      this.view.contractContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      validation = false;
    }

    //ISD code
    var ISDRegex = /^\+(\d{1,3}|\d{1,3})$/;
    if(this.view.contractContactNumber.txtISDCode.text.trim() && (this.view.contractContactNumber.txtISDCode.text.trim().length > 4 || ISDRegex.test(this.view.contractContactNumber.txtISDCode.text) === false)){
      this.view.contractContactNumber.flxError.width = "98%";
      this.view.contractContactNumber.flxError.isVisible = true;
      this.view.contractContactNumber.lblErrorText.text = kony.i18n.getLocalizedString("i18n.common.Enter_valid_ISD_code");
      this.view.contractContactNumber.txtISDCode.skin = "skinredbg";
      validation = false;
    }

    // email id
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.view.contractDetailsEntry3.tbxEnterValue.text.trim() && emailRegex.test(this.view.contractDetailsEntry3.tbxEnterValue.text.trim()) === false) {
      this.view.contractDetailsEntry3.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.EmailID_not_valid");
      this.view.contractDetailsEntry3.flxInlineError.isVisible = true;
      this.view.contractDetailsEntry3.flxEnterValue.skin = "sknFlxCalendarError";
      validation = false;
    }

    //ZipCode
    if (this.view.contractDetailsEntry6.tbxEnterValue.text.trim() && /^([a-zA-Z0-9_]+)$/.test(this.view.contractDetailsEntry6.tbxEnterValue.text) === false  ) {
      this.view.contractDetailsEntry6.lblErrorText.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorZipCode");
      this.view.contractDetailsEntry6.flxInlineError.setVisibility(true);
      this.view.contractDetailsEntry6.flxEnterValue.skin = "sknFlxCalendarError";
      validation = false;
    }

    //Country
    if (this.view.typeHeadContractCountry.tbxSearchKey.text&&this.view.typeHeadContractCountry.tbxSearchKey.text.trim() === "" && (this.view.typeHeadContractCountry.segSearchResult.isVisible || this.view.typeHeadContractCountry.flxNoResultFound.isVisible)) {
      this.view.lblNoContractCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorSelectCountry"); 
      this.view.flxNoContractCountry.isVisible = true;
      this.view.typeHeadContractCountry.tbxSearchKey.skin = "skinredbg";
      validation = false;
    } else if(this.view.typeHeadContractCountry.tbxSearchKey.text && this.view.typeHeadContractCountry.tbxSearchKey.info.isValid === false){
      this.view.lblNoContractCountryError.text = kony.i18n.getLocalizedString("i18n.frmCompanies.ErrorCountry");
      this.view.flxNoContractCountry.isVisible = true;
      this.view.typeHeadContractCountry.tbxSearchKey.skin = "skinredbg";
      validation = false;
    }
    if(validation===true){
      this.view.contractDetailsEntry3.flxEnterValue.skin = "sknflxEnterValueNormal";
      this.view.contractDetailsEntry3.flxInlineError.isVisible = false;
      this.view.contractContactNumber.flxError.isVisible = false;
      this.view.contractContactNumber.txtISDCode.skin = "skntxtbxDetails0bbf1235271384a";
      this.view.contractContactNumber.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
      this.view.contractDetailsEntry6.flxInlineError.setVisibility(false);
      this.view.contractDetailsEntry6.flxEnterValue.skin = "sknflxEnterValueNormal";
      this.view.flxNoContractCountry.isVisible = false;
      this.view.typeHeadContractCountry.tbxSearchKey.skin = "skntbxLato35475f14px";
      this.view.contractDetailsEntry1.flxInlineError.isVisible = false;
      this.view.contractDetailsEntry1.flxEnterValue.skin = "sknflxEnterValueNormal";
    }
    return validation;
  },
  getCountrySegmentData : function(){
    var self = this;
    var callBack = function(response){
      kony.print("listboxreponse",response);
      if(response !== "error") {
        self.segCountry = response.countries.reduce(
          function(list, country) {
            return list.concat([{"id":country.id,
                                 "lblAddress":{"text":country.Name,
                                               "left" : "10dp"},
                                 "template":"flxSearchCompanyMap"}]);
          },[]);
      }
      var widgetMap = {"flxSearchCompanyMap":"flxSearchCompanyMap",
                       "lblAddress":"lblAddress",
                       "id":"id",
                       "Region_id":"Region_id",
                       "Country_id":"Country_id"};
      self.view.typeHeadContractCountry.segSearchResult.widgetDataMap = widgetMap;
      self.view.typeHeadContractCountry.segSearchResult.setData(self.segCountry);
      self.view.forceLayout();
    };
    this.presenter.fetchLocationPrefillData(callBack);
  },
  validateBulkSelection : function(){
    var rowsList = this.view.bulkUpdateFeaturesLimitsPopup.flxAddNewRowListCont.widgets();
    var isValid=true;
    var selCount=0;
    for (var i = 0; i < rowsList.length; i++) {
      if (rowsList[i].lstBoxFieldValue11.selectedKey !== "select"){
        selCount=selCount+1;
        if(rowsList[i].productTypeFilterMenu.segStatusFilterDropdown.selectedRowItems===null){
          isValid=false;
          rowsList[i].flxFieldValueContainer12.skin="sknFlxCalendarError";
          rowsList[i].lblErrorMsg12.text="Select atleast one action";
          rowsList[i].flxErrorField12.setVisibility(true);
        }
        if(this.view.flxContractLimits.isVisible===true&&rowsList[i].tbxValue21.text.trim().length===0){
          isValid=false;
          rowsList[i].lblErrorMsg21.text="Value cannot be empty";
          rowsList[i].flxErrorField21.setVisibility(true);
          rowsList[i].flxValue21.skin="sknFlxCalendarError";
        }
      }
    }
    if(selCount===0){
      isValid=false;
      rowsList[0].lstBoxFieldValue11.skin="sknlbxError";
      rowsList[0].lblErrorMsg11.text="Select atleast one feature";
      rowsList[0].flxErrorField11.setVisibility(true);
    }
    this.view.forceLayout();
    return isValid;
  },
  fetchAutoSyncAccountsFlag : function(){
    var requestParam={
      "bundle_name" : "C360",
      "config_key": "DEFAULT_ACCOUNTS_ACCESS_TYPE"
    }
    this.presenter.getAutoSyncAccountsFlag(requestParam);
  },
  /*
  * get 2 digit string number
  * @param: number
  * @return: 2-digit sting number
  */
  getTwoDigitNumber : function(count){
    var updatedCount =0;
    count = parseInt(count,10);
    if(count > 9 || count === 0){
      updatedCount = count;
    } else{
      updatedCount = "0"+count;
    }
    return updatedCount;
  },
  /*
  * get count of selected items
  * @param: data,isRow
  * @return: count of selected items
  */
  getSelectedItemsCount : function(data,isRow){
    var selCount = 0;
    var srcImg = (isRow === true) ? "imgCheckbox" :"imgSectionCheckbox";
    for(var i=0; i<data.length; i++){
      var list = (isRow === true) ? data[i] : data[i][0];
      if(list[srcImg].src === this.AdminConsoleCommonUtils.checkboxSelected || list[srcImg].src === this.AdminConsoleCommonUtils.checkboxPartial){
        selCount  = selCount +1;
      }
    }
    if(selCount > 9 || selCount === 0)
      return selCount;
    else
      return "0"+selCount;
  },
  /*
  * view approval matrix of customer 
  * @param: create dynamic features(true/false)
  */
  showApprovalMatrixNew : function(createCard){
    this.showFourColumnsInCardAM();
    this.view.searchBoxAM.tbxSearchBox.text = "";
    this.view.searchBoxAM.flxClearSearch.setVisibility(false);
    this.view.flxCompanyDetailsApprovalMatrix.setVisibility(true);
    this.view.flxDynamicWidCustLevelAM.setVisibility(true);
    this.view.flxDynamicWidAccLevelAM.setVisibility(false);
    this.view.flxAccountsBackAM.setVisibility(false);
    this.view.lblCardNameAM.skin = "sknLbl117EB0LatoReg14px";
    this.view.lblCardNameAM.hoverSkin = "sknLbl117EB0LatoReg14pxHov";
    if(createCard === true){
      this.createDynamicFeatureCardsCustAM();
    }
    this.setCustomerDropdownAM();
    this.view.customerDropdownAM.flxSelectedText.skin = "sknflxffffffBorderE1E5EERadius3pxPointer";
    this.view.customerDropdownAM.flxSelectedText.hoverSkin = "sknFlxBorder117eb0radius3pxbgfff";
    this.view.forceLayout();
  },
  /*
  * view approval matrix of customer at account level
  * @param: create dynamic features(true/false)
  */
  showApprovalMatrixAccountLevel : function(createCard){
    this.showThreeColumnsInCardAM();
    this.view.searchBoxAM.tbxSearchBox.text = "";
    this.view.searchBoxAM.flxClearSearch.setVisibility(false);
    this.view.flxDynamicWidCustLevelAM.setVisibility(false);
    this.view.flxDynamicWidAccLevelAM.setVisibility(true);
    this.view.flxAccountsBackAM.setVisibility(true);
    this.view.lblCardNameAM.skin = "sknLbl192B45LatoRegular14px";
    this.view.lblCardNameAM.hoverSkin = "sknLbl192B45LatoRegular14px";
    this.setAccountDropdownAM();
    if(createCard === true){
      this.createDynamicFeatureCardsAccAM();
    }
    this.view.forceLayout();
  },
  /*
   * show 3 column division for details part
   */
  showThreeColumnsInCardAM : function(){
    this.view.flxAMColumn4.setVisibility(false);
    this.view.flxAMColumn1.width = "29%";
    this.view.flxAMColumn2.width = "29%";
    this.view.flxAMColumn3.width = "29%";
  },
  /*
   * show 4 column division for details part
   */
  showFourColumnsInCardAM : function(){
    this.view.flxAMColumn4.setVisibility(true);
    this.view.flxAMColumn1.width = "22.5%";
    this.view.flxAMColumn2.width = "22.5%";
    this.view.flxAMColumn3.width = "22.5%";
    this.view.flxAMColumn4.width = "22.5%";
  },
  /*
  * set data to customers dropdown in approval matrix
  */
  setCustomerDropdownAM : function(){
    var widgetMap={
      "flxCustomerName":"flxCustomerName",
      "flxCustomerNameCont":"flxCustomerNameCont",
      "lblCustomerName":"lblCustomerName",
      "btnPrimaryCustomers":"btnPrimaryCustomers"
    };  
    this.view.customerDropdownAM.segList.widgetDataMap=widgetMap;
    //this.view.customerDropdownAM.segList.setData(dropdownData);
    //this.view.customerDropdownAM.segList.info={"records":dropdownData};
  },
  /*
  * set data to acounts dropdown in account view of approval matrix
  */
  setAccountDropdownAM : function(){
     var widgetMap={
      "flxCustomerName":"flxCustomerName",
      "flxCustomerNameCont":"flxCustomerNameCont",
      "lblCustomerName":"lblCustomerName",
      "btnPrimaryCustomers":"btnPrimaryCustomers"
    };  
    this.view.customerDropdownAM.segList.widgetDataMap=widgetMap;
    //this.view.customerDropdownAM.segList.setData(dropdownData);
    //this.view.customerDropdownAM.segList.info={"records":dropdownData};
  },
  /*
  * create features card in approval matrix card for selected customer
  */
  createDynamicFeatureCardsCustAM : function(){
    this.view.flxDynamicWidCustLevelAM.removeAll();
    for(var i=0;i<3;i++){
      var num = i>10 ? i : "0"+i;
      var featureCardToAdd = new com.adminConsole.contracts.approvalMatrixFeatureCard({
        "id": "featureCardCustAM" +num,
		"isVisible": true,
		"masterType": constants.MASTER_TYPE_DEFAULT,
		"left":"0dp",
		"width":"100%",
		"top": "0dp",
      }, {}, {});
      this.view.flxDynamicWidCustLevelAM.add(featureCardToAdd);
      featureCardToAdd.flxToggle.onClick = this.toggleFeatureCardRowAM.bind(this,featureCardToAdd,1);
      this.setFeatureCardDataAM(featureCardToAdd);
      this.setActionsSegDataAM(featureCardToAdd);
    }
    this.view.flxDynamicWidCustLevelAM.setVisibility(true);
    this.view.flxCardBottomContainerAM.setVisibility(true);
    this.view.forceLayout();
  },
  /*
  * create features card in approval matrix card for selected account
  */
  createDynamicFeatureCardsAccAM : function(){
    this.view.flxDynamicWidAccLevelAM.removeAll();
    for(var i=0;i<3;i++){
      var num = i>10 ? i : "0"+i;
      var featureCardToAdd = new com.adminConsole.contracts.approvalMatrixFeatureCard({
        "id": "featureCardAccAM" +num,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"0dp",
        "top": "0dp"
      }, {}, {});
      this.view.flxDynamicWidAccLevelAM.add(featureCardToAdd);

      featureCardToAdd.flxToggle.onClick = this.toggleFeatureCardRowAM.bind(this,featureCardToAdd,2);
      this.setFeatureCardDataAM(featureCardToAdd); 
      this.setActionsSegDataAM(featureCardToAdd);
    }
    this.view.flxDynamicWidAccLevelAM.setVisibility(true);
    this.view.flxCardBottomContainerAM.setVisibility(true);
    this.view.forceLayout();
  },
  /*
  * set date to the feature card
  * @param: featureCard to add
  */
  setFeatureCardDataAM : function(featureCardToAdd){
    featureCardToAdd.toggleCollapseArrow(false);
    
  },
  /*
  * toggle feature card to show/hide actions
  * @param: current featureCard, category(1:cust,2:acc)
  */
  toggleFeatureCardRowAM : function(currFeatureCard, category){
    var parentFlex = category === 1 ? this.view.flxDynamicWidCustLevelAM :
                                 this.view.flxDynamicWidAccLevelAM;
    var featureCards = parentFlex.widgets();
    for(var i=0; i<featureCards.length; i++){
      if(featureCards[i].id === currFeatureCard.id){ //toggle selected card
        var flxVisiblity = currFeatureCard.flxCardBottomContainer.isVisible;
        currFeatureCard.toggleCollapseArrow(!flxVisiblity);
      } else{ //toggle any remaining cards
        if(featureCards[i].flxCardBottomContainer.isVisible === true){
          featureCards[i].toggleCollapseArrow(false);
        }
      }
    }
  },
  /*
  * widget data map fro actions listing segment inside feature card
  */
  getWidgetMapForActionSegAM : function(){
    return {
      //section header
      "flxApprovalMatrixActionHeader":"flxApprovalMatrixActionHeader",
      "lblFASeperatorTop":"lblFASeperatorTop",
      "flxActionName":"flxActionName",
      "lblIconAction":"lblIconAction",
      "lblActionName":"lblActionName",
      "lbxTransactionType":"lbxTransactionType",
      "flxViewApprovalsHeader":"flxViewApprovalsHeader",
      "lblRangeHeader":"lblRangeHeader",
      "lblApprovalRequiredHeader":"lblApprovalRequiredHeader",
      "lblApproversHeader":"lblApproversHeader",
      "lblFASeperator2":"lblFASeperator2",
      "lblFASeperator1":"lblFASeperator1",
      //ranges row template
      "flxApprovalMatrixRangeRow":"flxApprovalMatrixRangeRow",
      "flxApprovalRangeCont":"flxApprovalRangeCont",
      "lblApprovalValue1":"lblApprovalValue1",
      "lblApprovalValue2":"lblApprovalValue2",
      "lblApprovalView":"lblApprovalView",
      "lblApprovalNA":"lblApprovalNA",
      "lblLine":"lblLine",
      "flxApprovalEdit":"flxApprovalEdit",
      "lblIconAction":"lblIconAction",
      //no ranges available template
      "flxApprovalMatrixNoRangeRow":"flxApprovalMatrixNoRangeRow",
      "flxNoRangesCont":"flxNoRangesCont",
      "lblNoApprovalText":"lblNoApprovalText",
      "btnAddApproval":"btnAddApproval"
    };
  },
  /*
  * set data to actions segment under each feature
  *
  */
  setActionsSegDataAM : function(featureCard){
    var sampledata = [{"actionName":"Action1","actionId":"A1","type":"MONETARY","isCustom":false,"approvalRange":[{"rangeId":"R1","value":"Upto $100"}, {"rangeId":"R2","value":"$101 - $500"}]},
                      {"actionName":"Action2","actionId":"A2","type":"MONETARY","isCustom":true,"approvalRange":[]},
                      {"actionName":"Action3","actionId":"A3","type":"NON-MONETARY","isCustom":false,"approvalRange":[]}];
    var segData = [];
    for(var i=0; i< sampledata.length; i++){
      var sectionData = {
        "lblFASeperatorTop":"-",
        "lblIconAction":"lblIconAction",
        "lblActionName":sampledata[i].actionName,
        "lbxTransactionType":{"masterData":"","selectedKey":""},
        "flxViewApprovalsHeader":{"isVisible":true},
        "lblRangeHeader":"RANGE",
        "lblApprovalRequiredHeader":"APPROVAL REQUIRED",
        "lblApproversHeader":"APPROVERS",
        "lblFASeperator2":"-",
        "lblFASeperator1":"-",
        "template":"flxApprovalMatrixActionHeader"
      };
      var rowsData = [];
      rowsData = this.getApprovalRangesRowData(sampledata[i]);
      segData.push([sectionData,rowsData])
    }
    featureCard.segFeatureActions.widgetDataMap = this.getWidgetMapForActionSegAM();
    featureCard.segFeatureActions.setData(segData);
    this.view.forceLayout();
  },
  /*
  * set actions segment rows
  *
  */
  getApprovalRangesRowData : function(actionData){
    var rowsData = [];
    var approvalData = actionData.approvalRange;
    if(approvalData.length > 0){
      for(var i=0;i<approvalData.length; i++){
        var row = {
          "lblApprovalValue1":{"text":approvalData[i].value},
          "lblApprovalValue2":{"text":approvalData[i].rangeId},
          "lblApprovalView":{"text":"View","isVisible":true},
          "lblApprovalNA":{"isVisible":false},
          "lblLine": {"text":"-","bottom":"0dp"},
          "flxApprovalEdit":{"isVisible":false,"onClick":""},
          "lblIconAction":{"text":"\ue91e"},
          "template":"flxApprovalMatrixRangeRow",
        };
        rowsData.push(row);
      }
      //adjust edit icon and line
      var index = (rowsData.length/2 === 0) ? (rowsData.length/2)-1 : Math.floor(rowsData.length/2);
      rowsData[index].flxApprovalEdit.isVisible = true;
      rowsData[rowsData.length-1].lblLine.bottom = "10dp";
    } else{
      var row = {};
      // no approval ranges configured
      if(actionData.isCustom === false){
        row = {
          "lblNoApprovalText": {"text":"No approval ranges have been added"},
          "btnAddApproval":{"text":"Add","isVisible":true,"onClick":""},
          "flxApprovalEdit":{"isVisible":false},
          "lblLine": {"isVisible":false},
          "template":"flxApprovalMatrixNoRangeRow",
        };
      } // custom changes made at account level
      else{
        row = {
          "lblNoApprovalText": {"text":"Please refer the custom changes at account level"},
          "btnAddApproval":{"isVisible":false,"onClick":""},
          "lblLine": {"text":"-","isVisible":true,"bottom":"0dp"},
          "flxApprovalEdit":{"isVisible":true,"onClick":""},
          "lblIconAction":{"text":"\ue91e"},
          "template":"flxApprovalMatrixNoRangeRow",
        };
      }
      rowsData.push(row);
    }
  return rowsData;
  },
  showSignatoryGroups : function(){
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName6);
    this.hideAllTabsDetails();
    this.view.flxContractSignatoryContainer.setVisibility(true);
    var signatoryGroups={"822334":[{
      "signatoryGroupId" : "1234",
      "signatoryGroupName": "ABC",
      "signatoryGroupDescription":  "ABC Description"
    },{
      "signatoryGroupId" : "2345",
      "signatoryGroupName": "ABCD",
      "signatoryGroupDescription":  "ABCD Description"
    },{
      "signatoryGroupId" : "3456",
      "signatoryGroupName": "ABCDE",
      "signatoryGroupDescription":  "ABCDE Description"
    }],
                         "2":[{
                           "signatoryGroupId" : "aaa",
                           "signatoryGroupName": "ABC",
                           "signatoryGroupDescription":  "ABC Description"
                         },{
                           "signatoryGroupId" : "bbb",
                           "signatoryGroupName": "ABCD",
                           "signatoryGroupDescription":  "ABCD Description"
                         },{
                           "signatoryGroupId" : "ccc",
                           "signatoryGroupName": "ABCDE",
                           "signatoryGroupDescription":  "ABCDE Description"
                         }]};
    this.setSignatoryGroups(signatoryGroups);
    //this.presenter.getContractSignatoryGroups(this.recentContractDetails.contractId);
    this.view.forceLayout();
  },
  setSignatoryGroups : function(signGroups){
    var self=this;
    self.view.flxContractGroupsContainer.removeAll();
    var customers=this.completeContractDetails.contractCustomers;
    for (var x=0;x<customers.length;x++){
      var num = x>10 ? x : "0"+x;
      var featureCardToAdd = new com.adminConsole.contracts.accountsFeaturesCard({
        "id": "featureCard" +num,
		"isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"0dp",
        "width":"100%",
        "top": "15dp"
      }, {}, {});
      i=i+1;
      featureCardToAdd.flxArrow.onClick = self.toggleGroupCardVisibility.bind(self,featureCardToAdd,self.view.flxContractGroupsContainer);
      featureCardToAdd.flxHeadingRightContainer.isVisible=true;
      featureCardToAdd.btnEdit.text="Add";
      featureCardToAdd.btnEdit.onClick = function(){
        self.addNewGroupClick(featureCardToAdd);
      };
      featureCardToAdd.btnView.isVisible=false;
      self.view.flxContractGroupsContainer.add(featureCardToAdd);
      featureCardToAdd.segAccountFeatures.info = {"parentId":featureCardToAdd.id,"segData":[], "segDataJSON":{}};
      self.setsignGroupsCardData(featureCardToAdd, signGroups[customers[x].coreCustomerId],customers[x]);
    }
  },
  setsignGroupsCardData :function(featureCardToAdd,signatoryData,customerData){
    //featureCardToAdd.info = {"accDetails":accLevelFeaturesMap.accountDetails};
    var address="N/A";
    if(customerData.city!==undefined||customerData.country!==undefined)
      address=this.AdminConsoleCommonUtils.getAddressText(customerData.city,customerData.country);
    else
      address="N/A";
    var details = {
        "id": customerData.coreCustomerId,
        "name": customerData.coreCustomerName,
        "industry":customerData.industry?customerData.industry:"N/A",
        "email": customerData.email,
        "phone":customerData.phone,
        "address": address
      };
    featureCardToAdd.lblName.text = customerData.coreCustomerName;
    this.view.ContractFAList.lblName.onTouchStart = this.showCustomerDetailsPopup.bind(this,details,false);
    featureCardToAdd.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID");
    featureCardToAdd.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.TaxId_UC");
    featureCardToAdd.lblHeading3.text = kony.i18n.getLocalizedString("i18n.View.ADDRESS");
    featureCardToAdd.lblData1.text = customerData.coreCustomerId;
    featureCardToAdd.lblData2.text = customerData.taxId;
    featureCardToAdd.lblData3.text = address;
    if(customerData.isPrimary === "true"){
      featureCardToAdd.flxPrimary.setVisibility(true);
    }
    featureCardToAdd.lblHeading.text =  kony.i18n.getLocalizedString("i18n.frmEnrollCustomersController.SelectedFeatures")+":";
    featureCardToAdd.lblTotalCount.setVisibility(true);
    var count =Object.keys(signatoryData).length;
    featureCardToAdd.lblTotalCount.text =  "of "+ this.getTwoDigitNumber(count);
    featureCardToAdd.toggleCollapseArrow(false);
    featureCardToAdd.flxArrow.isVisible = true;
    this.setGroupCardSegmentData(featureCardToAdd.segAccountFeatures, signatoryData);
  },
  setGroupCardSegmentData : function(segmentPath,data){
    var existingGroupNames=[];
    var widgetDataMap={
      "flxSegSignatoryGroupHeader":"flxSegSignatoryGroupHeader",
      "flxHeaderContainer":"flxHeaderContainer",
      "flxGroupName":"flxGroupName",
      "lblGroupName":"lblGroupName",
      "lblIconGroupNameSort":"lblIconGroupNameSort",
      "flxGroupDescription":"flxGroupDescription",
      "lblGroupDescription":"lblGroupDescription",
      "flxNumberOfUsers":"flxNumberOfUsers",
      "lblNumberOfUsers":"lblNumberOfUsers",
      "lblIconSortNumberOfUsers":"lblIconSortNumberOfUsers",
      "lblSeperator":"lblSeperator",
      "flxSegSignatoryGroupContent":"flxSegSignatoryGroupContent",
      "flxSegHeader":"flxSegHeader",
      "lblGroupName":"lblGroupName",
      "lblGroupDescription":"lblGroupDescription",
      "flxOptions":"flxOptions",
      "lblOptions":"lblOptions",
      "flxDelete":"flxDelete",
      "lblIconDeleteLang":"lblIconDeleteLang",
      "lblSeperator":"lblSeperator"
    };
    var secData={
      "flxGroupName":{"onClick":function(){}},
      "lblGroupName":"GROUP NAME",
      "lblIconGroupNameSort":"\ue92a",
      "lblGroupDescription":"GROUP DESCRIPTION",
      "flxNumberOfUsers":{"onClick":function(){}},
      "lblNumberOfUsers":"NUMBER OF USERS",
      "lblIconSortNumberOfUsers":"\ue92b",
      "template":"flxSegSignatoryGroupHeader"
    };
    var segData= data.map(function(rec){
      existingGroupNames.push(rec.signatoryGroupName);
      return{
        "template":"flxSegSignatoryGroupContent",
        "lblGroupName":rec.signatoryGroupName,
        "lblGroupDescription":rec.signatoryGroupDescription,
        "flxOptions":{"onClick":function(){}},
        "lblOptions":{"text":kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit")},
        "flxDelete":{"onClick":function(){}},
        "lblIconDeleteLang":{"text":"","skin":"sknIcon00000015px"},
      }
    });
    segmentPath.widgetDataMap = widgetDataMap;
    segmentPath.rowTemplate="flxSegSignatoryGroupContent";
    segmentPath.info={"existingNames":existingGroupNames};
    segmentPath.setData([[secData,segData]]);
    this.view.forceLayout();
  },
  toggleGroupCardVisibility : function(cardPath,parentFlex){
    var listArr = parentFlex.widgets();
    for(var i=0; i<listArr.length; i++){
      if(listArr[i].id === cardPath.id){
        var visibilityCheck = cardPath.flxCardBottomContainer.isVisible;
        cardPath.toggleCollapseArrow(!visibilityCheck);
      }
      else{
        this.view[listArr[i].id].toggleCollapseArrow(false);
      }
    }
  },
  toggleAddGroupVerticalTabs: function(imgPath,btnPath){
    this.tabUtilVerticleArrowVisibilityFunction(
      [//this.view.verticalTabs.flxImgArrow0,
       this.view.verticalTabs.flxImgArrow1,
       this.view.verticalTabs.flxImgArrow2,
       //this.view.verticalTabs.flxImgArrow3,
       //this.view.verticalTabs.flxImgArrow4,
       //this.view.verticalTabs.flxImgArrow5
      ],imgPath);  
    var widgetArray = [
      //this.view.verticalTabs.btnOption0,
      this.view.verticalTabs.btnOption1,this.view.verticalTabs.btnOption2,
      //this.view.verticalTabs.btnOption3,this.view.verticalTabs.btnOption4,this.view.verticalTabs.btnOption5
    ];
    this.tabUtilVerticleButtonFunction(widgetArray,btnPath);
  },
  addNewGroupClick : function(featuresCard){
    var existingGroupNames=featuresCard.segAccountFeatures.info.existingNames;
    var coreCustId=featuresCard.lblData1.text;
    this.view.flxCompanyDetails.setVisibility(false);
    this.view.tbxGroupNameValue.text="";
    this.view.flxNoGroupNameError.setVisibility(false);
    this.view.txtGroupDescription.text="";
    this.toggleAddGroupVerticalTabs(this.view.verticalTabs.flxImgArrow1,this.view.verticalTabs.btnOption1);
    this.view.flxSelectedUsers.setVisibility(false);
    this.view.flxGroupDetails.setVisibility(true);
    this.view.flxAddGroups.setVisibility(true);
    this.view.forceLayout();
  },
    showSignatoryGroups : function(){
    this.tabUtilLabelFunction([ this.view.lblTabName1,this.view.lblTabName2,this.view.lblTabName3,
                               this.view.lblTabName4,this.view.lblTabName5,this.view.lblTabName6],this.view.lblTabName6);
    this.hideAllTabsDetails();
    this.view.flxContractSignatoryContainer.setVisibility(true);
    var signatoryGroups={"822334":[{
      "signatoryGroupId" : "1234",
      "signatoryGroupName": "ABC",
      "signatoryGroupDescription":  "ABC Description"
    },{
      "signatoryGroupId" : "2345",
      "signatoryGroupName": "ABCD",
      "signatoryGroupDescription":  "ABCD Description"
    },{
      "signatoryGroupId" : "3456",
      "signatoryGroupName": "ABCDE",
      "signatoryGroupDescription":  "ABCDE Description"
    }],
                         "2":[{
                           "signatoryGroupId" : "aaa",
                           "signatoryGroupName": "ABC",
                           "signatoryGroupDescription":  "ABC Description"
                         },{
                           "signatoryGroupId" : "bbb",
                           "signatoryGroupName": "ABCD",
                           "signatoryGroupDescription":  "ABCD Description"
                         },{
                           "signatoryGroupId" : "ccc",
                           "signatoryGroupName": "ABCDE",
                           "signatoryGroupDescription":  "ABCDE Description"
                         }]};
    this.setSignatoryGroups(signatoryGroups);
    //this.presenter.getContractSignatoryGroups(this.recentContractDetails.contractId);
    this.view.forceLayout();
  },
setSignatoryGroups : function(signGroups){
    var self=this;
    self.view.flxContractGroupsContainer.removeAll();
    var customers=this.completeContractDetails.contractCustomers;
    for (var x=0;x<customers.length;x++){
      var num = x>10 ? x : "0"+x;
      var featureCardToAdd = new com.adminConsole.contracts.accountsFeaturesCard({
        "id": "featureCard" +num,
        "isVisible": true,
        "masterType": constants.MASTER_TYPE_DEFAULT,
        "left":"0dp",
        "width":"100%",
        "top": "15dp"
      }, {}, {});
      i=i+1;
      featureCardToAdd.flxArrow.onClick = self.toggleGroupCardVisibility.bind(self,featureCardToAdd,self.view.flxContractGroupsContainer);
      featureCardToAdd.flxHeadingRightContainer.isVisible=true;
      featureCardToAdd.btnEdit.text="Add";
      featureCardToAdd.btnEdit.onClick = function(){
        self.addNewGroupClick(featureCardToAdd);
      };
      featureCardToAdd.btnView.isVisible=false;
      self.view.flxContractGroupsContainer.add(featureCardToAdd);
      featureCardToAdd.segAccountFeatures.info = {"parentId":featureCardToAdd.id,"segData":[], "segDataJSON":{}};
      self.setsignGroupsCardData(featureCardToAdd, signGroups[customers[x].coreCustomerId],customers[x]);
    }
  },
  setsignGroupsCardData :function(featureCardToAdd,signatoryData,customerData){
    //featureCardToAdd.info = {"accDetails":accLevelFeaturesMap.accountDetails};
    var address="N/A";
    if(customerData.city!==undefined||customerData.country!==undefined)
      address=this.AdminConsoleCommonUtils.getAddressText(customerData.city,customerData.country);
    else
      address="N/A";
    var details = {
        "id": customerData.coreCustomerId,
        "name": customerData.coreCustomerName,
        "industry":customerData.industry?customerData.industry:"N/A",
        "email": customerData.email,
        "phone":customerData.phone,
        "address": address
      };
    featureCardToAdd.lblName.text = customerData.coreCustomerName;
    this.view.ContractFAList.lblName.onTouchStart = this.showCustomerDetailsPopup.bind(this,details,false);
    featureCardToAdd.lblHeading1.text = kony.i18n.getLocalizedString("i18n.frmCustomerManagementController.CUSTOMER_ID");
    featureCardToAdd.lblHeading2.text = kony.i18n.getLocalizedString("i18n.frmEnrollCustomer.TaxId_UC");
    featureCardToAdd.lblHeading3.text = kony.i18n.getLocalizedString("i18n.View.ADDRESS");
    featureCardToAdd.lblData1.text = customerData.coreCustomerId;
    featureCardToAdd.lblData2.text = customerData.taxId;
    featureCardToAdd.lblData3.text = address;
    if(customerData.isPrimary === "true"){
      featureCardToAdd.flxPrimary.setVisibility(true);
    }
    featureCardToAdd.lblHeading.text =  kony.i18n.getLocalizedString("i18n.frmEnrollCustomersController.SelectedFeatures")+":";
    featureCardToAdd.lblTotalCount.setVisibility(true);
    var count =Object.keys(signatoryData).length;
    featureCardToAdd.lblTotalCount.text =  "of "+ this.getTwoDigitNumber(count);
    featureCardToAdd.toggleCollapseArrow(false);
    featureCardToAdd.flxArrow.isVisible = true;
    this.setGroupCardSegmentData(featureCardToAdd.segAccountFeatures, signatoryData);
  },
  setGroupCardSegmentData : function(segmentPath,data){
    var existingGroupNames=[];
    var widgetDataMap={
      "flxSegSignatoryGroupHeader":"flxSegSignatoryGroupHeader",
      "flxHeaderContainer":"flxHeaderContainer",
      "flxGroupName":"flxGroupName",
      "lblGroupName":"lblGroupName",
      "lblIconGroupNameSort":"lblIconGroupNameSort",
      "flxGroupDescription":"flxGroupDescription",
      "lblGroupDescription":"lblGroupDescription",
      "flxNumberOfUsers":"flxNumberOfUsers",
      "lblNumberOfUsers":"lblNumberOfUsers",
      "lblIconSortNumberOfUsers":"lblIconSortNumberOfUsers",
      "lblSeperator":"lblSeperator",
      "flxSegSignatoryGroupContent":"flxSegSignatoryGroupContent",
      "flxSegHeader":"flxSegHeader",
      "lblGroupName":"lblGroupName",
      "lblGroupDescription":"lblGroupDescription",
      "flxOptions":"flxOptions",
      "lblOptions":"lblOptions",
      "flxDelete":"flxDelete",
      "lblIconDeleteLang":"lblIconDeleteLang",
      "lblSeperator":"lblSeperator"
    };
    var secData={
      "flxGroupName":{"onClick":function(){}},
      "lblGroupName":"GROUP NAME",
      "lblIconGroupNameSort":"\ue92a",
      "lblGroupDescription":"GROUP DESCRIPTION",
      "flxNumberOfUsers":{"onClick":function(){}},
      "lblNumberOfUsers":"NUMBER OF USERS",
      "lblIconSortNumberOfUsers":"\ue92b",
      "template":"flxSegSignatoryGroupHeader"
    };
    var segData= data.map(function(rec){
      existingGroupNames.push(rec.signatoryGroupName);
      return{
        "template":"flxSegSignatoryGroupContent",
        "lblGroupName":rec.signatoryGroupName,
        "lblGroupDescription":rec.signatoryGroupDescription,
        "flxOptions":{"onClick":function(){}},
        "lblOptions":{"text":kony.i18n.getLocalizedString("i18n.frmFAQ.fonticonEdit")},
        "flxDelete":{"onClick":function(){}},
        "lblIconDeleteLang":{"text":"","skin":"sknIcon00000015px"},
      }
    });
    segmentPath.widgetDataMap = widgetDataMap;
    segmentPath.rowTemplate="flxSegSignatoryGroupContent";
    segmentPath.info={"existingNames":existingGroupNames};
    segmentPath.setData([[secData,segData]]);
    this.view.forceLayout();
  },
  toggleGroupCardVisibility : function(cardPath,parentFlex){
    var listArr = parentFlex.widgets();
    for(var i=0; i<listArr.length; i++){
      if(listArr[i].id === cardPath.id){
        var visibilityCheck = cardPath.flxCardBottomContainer.isVisible;
        cardPath.toggleCollapseArrow(!visibilityCheck);
      }
      else{
        this.view[listArr[i].id].toggleCollapseArrow(false);
      }
    }
  },
  toggleAddGroupVerticalTabs: function(imgPath,btnPath){
    this.tabUtilVerticleArrowVisibilityFunction(
      [//this.view.verticalTabs.flxImgArrow0,
       this.view.verticalTabs.flxImgArrow1,
       this.view.verticalTabs.flxImgArrow2,
       //this.view.verticalTabs.flxImgArrow3,
       //this.view.verticalTabs.flxImgArrow4,
       //this.view.verticalTabs.flxImgArrow5
      ],imgPath);  
    var widgetArray = [
      //this.view.verticalTabs.btnOption0,
      this.view.verticalTabs.btnOption1,this.view.verticalTabs.btnOption2,
      //this.view.verticalTabs.btnOption3,this.view.verticalTabs.btnOption4,this.view.verticalTabs.btnOption5
    ];
    this.tabUtilVerticleButtonFunction(widgetArray,btnPath);
  },
  addNewGroupClick : function(featuresCard){
    var existingGroupNames=featuresCard.segAccountFeatures.info.existingNames;
    var coreCustId=featuresCard.lblData1.text;
    this.view.flxCompanyDetails.setVisibility(false);
    this.view.tbxGroupNameValue.text="";
    this.view.flxNoGroupNameError.setVisibility(false);
    this.view.txtGroupDescription.text="";
    this.toggleAddGroupVerticalTabs(this.view.verticalTabs.flxImgArrow1,this.view.verticalTabs.btnOption1);
    this.view.flxSelectedUsers.setVisibility(false);
    this.view.flxGroupDetails.setVisibility(true);
    this.view.flxAddGroups.setVisibility(true);
    this.view.forceLayout();
  },
});